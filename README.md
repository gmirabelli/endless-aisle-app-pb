# Endless Aisle - App Code
Endless Aisle is a Salesforce Commerce Cloud in-store reference app, an Appcelerator Mobile application written to enable brick-and-mortar stores to leverage the Salesforce Commerce Cloud platform for in-store sales. 

More information on getting started is in [Getting Started with EA development](https://xchange.demandware.com/docs/DOC-23020).

## Software Compatability

This version of Endless Aisle has been tested against the following versions of software:

| Software                                  | Version         |
| ------------------------------------------|-----------------|
| Salesforce Commerce Cloud OCAPI           | 18.2            |
| Salesforce Commerce Cloud Site Genesis    | 15.1            |
| Salesforce Commerce Cloud Server API      | 15.5            |
| Mac OS X                                  | 10.13.x         |
| Appcelerator Studio                       | 5.0.0           |
| - Appcelerator CLI                        | 7.0.2           |
| - Titanium SDK                            | 7.0.2.GA        |
| - Alloy                                   | 1.11.0          |
| - Backbone.js                             | 0.9.10          |
| - Node                                    | 8.9.3           |
| Xcode                                     | 9.2             |
| - iOS SDK                                 | 11.2.x          |
| - iOS Simulator                           | 11.2.x          |
| iPad iOS Version                          | 11.2.x          |
| Verifone iOS Framework Library            | 1.0.6.334       |
| Verifone XPI                              | 12.08.77        |
| Adyen iOS Framework Library               | 1-22p0          |
| Adyen Firmware Application Versions Tested| 1_29.2          |

License and Attribution guide is available at https://xchange.demandware.com/docs/DOC-31150.

## How to get the latest version of the Endless Aisle app code

1. Fork the 'master' branch to your account.
2. Clone the fork to your laptop.

  * Note: this automatically sets the remove name to 'origin'.
  * On the command line, this can be accomplished by:
    
     ```
     git clone https://<YOUR_ACCOUNT_HERE>@bitbucket.org/<YOUR_ACCOUNT_HERE>/digital-store-app
     ```

3. Set up the ability to pull updates from the digital-store-app repo via 'upstream' remote.

  * On the command line, this can be accomplished by running the following in the digital-store-app directory created in the above step.

    ```
    git remote add upstream https://<YOUR_ACCOUNT_HERE>@bitbucket.org/demandware/digital-store-app
    ```

### Notes

* Only the 'master' branch is available in the digital-store-app repository. For your own code authoring purposes, you might want to create other branches.
* When you want to incorporate the latest version of Endless Aisle, you can pull into a new branch in your local repository. You then resolve conflicts and test the merged code before merging into your main branch.

  ```
  git checkout -b <new-branch>
  ```
  
  You are now in the new branch in your forked project.
  
  ```
  git pull upstream master
  ```
  This pulls the latest EA code from the master branch into your new branch. You can now resolve conflicts and test the new merged code.
