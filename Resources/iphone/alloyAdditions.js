

var Backbone = require('alloy/backbone');

Alloy.M = function (name, modelDesc, migrations) {
    var config = modelDesc.config,
        type = (config.adapter ? config.adapter.type : null) || 'localDefault';

    type === 'localDefault' && (type = 'sql');
    var adapter = require('alloy/sync/' + type);
    var extendObj = {
        defaults: config.defaults,
        sync: function (method, model, opts) {
            var config = model.config || {},
                adapterObj = config.adapter || {},
                type = (config.adapter ? config.adapter.type : null) || 'localDefault';
            type === 'localDefault' && (type = 'sql');

            return require('alloy/sync/' + type).sync(method, model, opts);
        }
    };
    var extendClass = {};
    migrations && (extendClass.migrations = migrations);
    _.isFunction(adapter.beforeModelCreate) && (config = adapter.beforeModelCreate(config, name) || config);

    var superClass = config.superClass || Backbone.Model;
    var Model = superClass.extend(extendObj, extendClass);
    Model.prototype.config = config;
    _.isFunction(modelDesc.extendModel) && (Model = modelDesc.extendModel(Model) || Model);
    _.isFunction(adapter.afterModelCreate) && adapter.afterModelCreate(Model, name);
    return Model;
};

Alloy.C = function (name, modelDesc, model) {
    modelDesc.config = modelDesc.config || {};
    var superClass = Backbone.Collection;
    var extendObj = {
        model: model,
        sync: function (method, model, opts) {
            return this.adapter.sync(method, model, opts);
        }
    };
    var Collection = superClass.extend(extendObj),
        config = Collection.prototype.config = model.prototype.config;

    var _adapter = config.collection_adapter || config.adapter || {
        type: 'localDefault'
    };
    var type = _adapter.type;
    Collection.prototype.adapter = require('alloy/sync/' + type);
    var adapter = require('alloy/sync/' + type);

    _.isFunction(adapter.afterCollectionCreate) && adapter.afterCollectionCreate(Collection);
    _.isFunction(modelDesc.extendCollection) && (Collection = modelDesc.extendCollection(Collection) || Collection);

    return Collection;
};

var AssociatedModel = require('backbone-associations').AssociatedModel;
var BackboneModel = Backbone.Model;
Backbone.Model = AssociatedModel;

Alloy._.mixin(require('underscore.deferred'));

Alloy._.mixin(require('underscore.string'));

var makePrintable = function (inString) {
    inString = inString || '';

    return '_' + inString.replace(/[^a-z0-9_]/gi, '_');
};

_L = function (locale_string) {
    if (locale_string == '$') {
        locale_string = 'CurrencySymbol';
    }
    return L(makePrintable(locale_string));
};