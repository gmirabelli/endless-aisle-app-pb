

var logger = require('logging')('ocapi:ocapi_methods', 'app/lib/ocapi_methods');

function mixinApiMethods(modelClass, metadata) {
    modelClass.prototype.modelHash = modelClass.prototype.modelHash || {};

    if (modelClass.prototype._get) {
        return;
    }

    modelClass.prototype._get = modelClass.prototype.get;
    modelClass.prototype.get = function (key) {
        if (!key) {
            return null;
        }

        var camelKey = _.capitalize(_.camelize(key));
        var getMethod = 'get' + camelKey;

        var relations = this.relations || [],
            relation;

        for (var i = 0; i < relations.length; i++) {
            relation = relations[i];
            if (relation.key == key) {
                if (relation.type == Backbone.Many) {
                    getMethod += 'Collection';
                }
                break;
            }
        }
        if (this[getMethod]) {
            return this[getMethod]();
        }

        return this._get(key);
    };

    modelClass.prototype.modelHash[metadata.name] = modelClass;
    logger.info('mixinApiMethods adding to modelHash ' + metadata.name);

    var metaprops = metadata.properties;

    var properties = _.keys(metaprops);
    _.each(properties, function (property) {
        var classDescription = metaprops[property];
        if (classDescription.type == 'string') {
            ensureAPIStringProperty(modelClass, classDescription);
        } else if (classDescription.type == 'number') {
            ensureAPINumberProperty(modelClass, classDescription);
        } else if (classDescription.type == 'integer') {
            ensureAPIIntegerProperty(modelClass, classDescription);
        } else if (classDescription.type == 'boolean') {
            ensureAPIBooleanProperty(modelClass, classDescription);
        } else if (classDescription.type == 'object' && classDescription.format == 'map') {
            ensureAPIMapProperty(modelClass, classDescription);
        } else if (classDescription.type == 'object') {
            ensureAPIToOneRelationship(modelClass, classDescription);
        } else if (classDescription.type == 'array') {
            ensureAPIToManyRelationship(modelClass, classDescription);
        }
    });
}

function ensureAPIStringProperty(classObject, property) {
    var method = property.label;
    var camelMethod = _.capitalize(_.camelize(method));
    var getMethod = 'get' + camelMethod;
    var setMethod = 'set' + camelMethod;
    var hasMethod = 'has' + camelMethod;
    if (!classObject.prototype[getMethod]) {
        classObject.prototype[getMethod] = function () {
            return this._get(method);
        };
    }
    if (!classObject.prototype[setMethod]) {
        classObject.prototype[setMethod] = function (value, options) {
            return this.set(method, value, options);
        };
    }
    if (!classObject.prototype[hasMethod]) {
        classObject.prototype[hasMethod] = function () {
            return this.has(method);
        };
    }
}

function ensureAPINumberProperty(classObject, property) {
    var method = property.label;
    var camelMethod = _.capitalize(_.camelize(method));
    var getMethod = 'get' + camelMethod;
    var setMethod = 'set' + camelMethod;
    var hasMethod = 'has' + camelMethod;
    if (!classObject.prototype[getMethod]) {
        classObject.prototype[getMethod] = function () {
            return this._get(method);
        };
    }
    if (!classObject.prototype[setMethod]) {
        classObject.prototype[setMethod] = function (value, options) {
            return this.set(method, value, options);
        };
    }
    if (!classObject.prototype[hasMethod]) {
        classObject.prototype[hasMethod] = function () {
            return this.has(method);
        };
    }
}

function ensureAPIIntegerProperty(classObject, property) {
    var method = property.label;
    var camelMethod = _.capitalize(_.camelize(method));
    var getMethod = 'get' + camelMethod;
    var setMethod = 'set' + camelMethod;
    var hasMethod = 'has' + camelMethod;
    if (!classObject.prototype[getMethod]) {
        classObject.prototype[getMethod] = function () {
            return this._get(method);
        };
    }
    if (!classObject.prototype[setMethod]) {
        classObject.prototype[setMethod] = function (value, options) {
            return this.set(method, value, options);
        };
    }
    if (!classObject.prototype[hasMethod]) {
        classObject.prototype[hasMethod] = function () {
            return this.has(method);
        };
    }
}

function ensureAPIBooleanProperty(classObject, property) {
    var method = property.label;
    var camelMethod = _.capitalize(_.camelize(method));
    var getMethod = 'is' + camelMethod;
    var setMethod = 'setIs' + camelMethod;
    var hasMethod = 'has' + camelMethod;
    if (!classObject.prototype[getMethod]) {
        classObject.prototype[getMethod] = function () {
            return this._get(method);
        };
    }
    if (!classObject.prototype[setMethod]) {
        classObject.prototype[setMethod] = function (value, options) {
            return this.set(method, value, options);
        };
    }
    if (!classObject.prototype[hasMethod]) {
        classObject.prototype[hasMethod] = function () {
            return this.has(method);
        };
    }
}

function ensureAPIToOneRelationship(classObject, property) {
    var method = property.label;
    var camelMethod = _.capitalize(_.camelize(method));
    var getMethod = 'get' + camelMethod;
    var setMethod = 'set' + camelMethod;
    var hasMethod = 'has' + camelMethod;
    if (!classObject.prototype[getMethod]) {
        classObject.prototype[getMethod] = function () {
            return this._get(method);
        };
    }
    if (!classObject.prototype[setMethod]) {
        classObject.prototype[setMethod] = function (value, options) {
            return this.set(method, value, options);
        };
    }
    if (!classObject.prototype[hasMethod]) {
        classObject.prototype[hasMethod] = function () {
            return this.has(method);
        };
    }

    var nestedObjectClass = classObject.prototype.modelHash[property.name];
    if (!nestedObjectClass) {
        nestedObjectClass = Backbone.Model.extend({});

        mixinApiMethods(nestedObjectClass, property);

        classObject.prototype.modelHash[property.label] = nestedObjectClass;
        logger.info('ensureAPIToOneRelationship adding to modelHash ' + property.label);
    } else {
        logger.info('ensureAPIToOneRelationship SKIPPING adding to modelHash ' + property.label + ' obtained with ' + property.name);
    }
    var className = _.capitalize(_.camelize(property.label));
    if (!classObject[className]) {
        classObject[className] = nestedObjectClass;
    }

    classObject.prototype.relations = classObject.prototype.relations || [];
    classObject.prototype.relations.push({
        type: Backbone.One,
        key: property.label,
        relatedModel: nestedObjectClass
    });
    logger.info('Relation Count: ' + classObject.prototype.relations.length);
}

function ensureAPIToManyRelationship(classObject, property) {
    var method = property.label;
    var camelMethod = _.capitalize(_.camelize(method));
    var getMethod = 'get' + camelMethod + 'Collection';
    var getArrayMethod = 'get' + camelMethod;
    var setMethod = 'set' + camelMethod;
    var hasMethod = 'has' + camelMethod;
    if (!classObject.prototype[getArrayMethod]) {
        classObject.prototype[getArrayMethod] = function () {
            var collection = this._get(method);
            return collection ? collection.models : [];
        };
    }
    if (!classObject.prototype[getMethod]) {
        classObject.prototype[getMethod] = function () {
            var collection = this._get(method);
            return collection ? collection : new Backbone.Collection([], {
                model: classObject
            });
        };
    }
    if (!classObject.prototype[setMethod]) {
        classObject.prototype[setMethod] = function (value, options) {
            return this.set(method, value, options);
        };
    }
    if (!classObject.prototype[hasMethod]) {
        classObject.prototype[hasMethod] = function () {
            return this._get(method) != null;
        };
    }

    var nestedObjectClass = classObject.prototype.modelHash[property.items.name];
    if (!nestedObjectClass) {
        nestedObjectClass = Backbone.Model.extend({});

        mixinApiMethods(nestedObjectClass, property.items);

        classObject.prototype.modelHash[property.items.name] = nestedObjectClass;
        logger.info('ensureAPIToManyRelationship adding to modelHash ' + property.items.name);
    } else {
        logger.info('ensureAPIToManyRelationship SKIPPING adding to modelHash ' + property.items.name);
    }
    var className = _.capitalize(_.camelize(property.items.name));
    if (!classObject[className]) {
        classObject[className] = nestedObjectClass;
    }
    classObject.prototype.relations = classObject.prototype.relations || [];
    classObject.prototype.relations.push({
        type: Backbone.Many,
        key: property.label,
        relatedModel: nestedObjectClass
    });
    logger.info('Relation Count: ' + classObject.prototype.relations.length);
}

function ensureAPIMapProperty(classObject, property) {
    var method = property.label;
    var camelMethod = _.capitalize(_.camelize(method));
    var getMethod = 'get' + camelMethod;
    var setMethod = 'set' + camelMethod;
    var hasMethod = 'has' + camelMethod;
    if (!classObject.prototype[getMethod]) {
        classObject.prototype[getMethod] = function () {
            return this._get(method);
        };
    }
    if (!classObject.prototype[setMethod]) {
        classObject.prototype[setMethod] = function (value, options) {
            return this.set(method, value, options);
        };
    }
    if (!classObject.prototype[hasMethod]) {
        classObject.prototype[hasMethod] = function () {
            return this.has(method);
        };
    }
}

module.exports = mixinApiMethods;