

var logger = require('logging')('images:imageUtils', 'app/lib/imageUtils');

(function () {
    Alloy.Globals.getImageViewImage = function (imageView, url) {
        if (!url) {
            logger.error('Trying to lookup non-existent image.');
            url = '';
        }
        if (!Alloy.CFG.is_live) {
            url = url.replace('https', 'http');
        }

        if (decodeURI(url) == url) {
            imageView.image = encodeURI(url);
        } else {
            imageView.image = url;
        }
    };

    Alloy.Globals.getImageForObjectProperty = function (object, url, property) {
        if (!url) {
            return;
        }

        if (decodeURI(url) == url) {
            object[property] = encodeURI(url);
        } else {
            object[property] = url;
        }
        return;
    };
})();