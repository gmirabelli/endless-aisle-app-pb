

var logger = require('logging')('images:dwImageServiceMethods', 'app/lib/dwImageServiceMethods');

exports.mixinImageServiceMethods = mixinImageServiceMethods;

function mixinImageServiceMethods(model_class) {
    model_class.prototype.getImageForContext = function (context, data) {
        if (!context) {
            return null;
        }

        var imageRef = null;
        switch (context) {
            case 'cart':
                imageRef = getCartImage(this, data);
                break;

            case 'productTile':
                imageRef = getSearchHitImage(this, data);
                break;

            case 'heroImage':
                imageRef = getImageFromImageGroup(this, data);
                break;

            case 'categoryTile':
                imageRef = getImageForCategoryTile(this, context);
                break;

            case 'imageSwatchLink':
                imageRef = getColorSwatchImage(this, context);
                break;
        }

        if (imageRef || imageRef != '') {
            return imageRef;
        }
        return Alloy.CFG.image_service.placeholderImageURL;

        function getSearchHitImage(hit, data) {
            var imageObj = hit.get('image');
            var variationAttributes;
            var variationValues;

            if (!imageObj && hit.getVariationAttributes()) {
                variationAttributes = hit.getVariationAttributes();
                outer: for (var i = 0; i < variationAttributes.length; i++) {
                    variationValues = variationAttributes[i].getValues();
                    for (var j = 0; j < variationValues.length; j++) {
                        if (variationValues[j].getImage()) {
                            imageObj = variationValues[j].getImage();
                            break outer;
                        }
                    }
                }
            }

            var link = imageObj ? imageObj.get('link') : null;
            return link;
        }

        function getCartImage(product, data) {
            var images,
                vvs = product.getVariationValues();
            if (vvs && vvs[Alloy.CFG.product.color_attribute]) {
                images = product.getImages('cart', vvs[Alloy.CFG.product.color_attribute]);
                if (!images) {
                    images = product.getImages('cart');
                }
            } else {
                images = product.getImages('cart');
            }
            if (images && images.length > 0) {
                return images[0].get('link');
            }
            return null;
        }

        function getImageFromImageGroup(product, imageGroup) {
            return imageGroup.get('images').at(0).get('link');
        }

        function getImageForCategoryTile(category, context) {
            return category.get('thumbnail');
        }

        function getColorSwatchImage(color, context) {
            return color.get('image_swatch').get('link');
        }
    };

    model_class.prototype.getImagesForContext = function (context, data) {
        if (!context) {
            return null;
        }

        var imageRef = null;
        switch (context) {
            case 'altImages':
            case 'altZoomImages':
            case 'largeAltZoomImages':
            case 'bundleProductImages':
            case 'setProductImages':
            case 'swatchImages':
                imageRef = getProductImages(this, context, data);
                break;
        }

        if (imageRef || imageRef != '') {
            return imageRef;
        }
        return Alloy.CFG.image_service.placeholderImageURL;

        function getProductImages(product, context, variationValue) {
            return product.getImages(context, variationValue) || [];
        }
    };

    model_class.prototype.getImageGroupsForContext = function (context, data) {
        var imageGroups;

        if (this.isMaster() || this.isVariant()) {
            imageGroups = product.getOrderableColorImageGroups(context);
        }

        if (!imageGroups) {
            imageGroups = product.getImageGroupsForViewType(context);
        }

        return imageGroups;
    };

    model_class.prototype.getImageViewType = function (context) {
        return Alloy.CFG.image_service.view_type[context];
    };

    model_class.prototype.transformImageGroupImageURLs = function (imageGroupJSON, view_type) {
        logger.info('image link: ' + JSON.stringify(imageGroupJSON));
        return imageGroupJSON;
    };

    var imageServiceUtilityMethods = require('imageServiceUtilityMethods');
    imageServiceUtilityMethods.mixinImageServiceUtilityMethods(model_class);
}