

var showError = require('EAUtils').showError;
var clearError = require('EAUtils').clearError;
var removeAllViews = require('EAUtils').removeAllViews;
var getUIObjectType = require('EAUtils').getUIObjectType;
var logger = require('logging')('addressFormManager', 'app/lib/addressFormManager');

function addressFormManager(args) {
    if (!args || !args.viewLayoutData || !args.$) {
        logger.error('Invalid arguments passed to form builder');
        return;
    }

    var getCountry;
    var getState;
    var $ = args.$;
    var viewLayoutData = args.viewLayoutData;
    var errorMsgs = viewLayoutData.addressForm.error_messages;
    var initializeCountryPicker = args.initializeCountryPicker;
    var initializeStatePicker = args.initializeStatePicker;
    var textFields = [];
    var textFieldNames = [];
    var formFields = [];
    var formFieldNames = [];

    if (args.getterFunctionsForCustomFields) {
        if (_.isFunction(args.getterFunctionsForCustomFields.getCountry)) {
            getCountry = args.getterFunctionsForCustomFields.getCountry;
        }
        if (_.isFunction(args.getterFunctionsForCustomFields.getState)) {
            getState = args.getterFunctionsForCustomFields.getState;
        }
    }

    this.renderAddressViewInContainer = renderAddressViewInContainer;
    this.deinit = deinit;
    this.closeKeyboard = closeKeyboard;
    this.getPhone = getPhone;
    this.getPostalCode = getPostalCode;
    this.getAddress2 = getAddress2;
    this.getAddress1 = getAddress1;
    this.getLastName = getLastName;
    this.getFirstName = getFirstName;
    this.getCity = getCity;
    this.validatePhoneField = validatePhoneField;
    this.validatePostalCodeField = validatePostalCodeField;
    this.validateEmailAddressField = validateEmailAddressField;
    this.getAllTextFields = getAllTextFields;
    this.getAllTextFieldNames = getAllTextFieldNames;
    this.getAllFormFields = getAllFormFields;
    this.getAllFormFieldNames = getAllFormFieldNames;
    this.clearAllErrors = clearAllErrors;
    this.setAllTextFieldsValues = setAllTextFieldsValues;
    this.isPostalCodeValid = isPostalCodeValid;
    this.isPhoneValid = isPhoneValid;
    this.showHideError = showHideError;
    this.isEmailAddressValid = isEmailAddressValid;
    this.getEmailAddress = getEmailAddress;
    this.getAllFieldValues = getAllFieldValues;
    this.hasAnythingChanged = hasAnythingChanged;

    function deinit() {
        unsetTextFieldsBlurEventHandlers();
        unsetOnReturnEventsHandlers();
        viewLayoutData = null;
        errorMsgs = null;
        $ = null;
        initializeCountryPicker = null;
        initializeStatePicker = null;
        textFields = null;
        textFieldNames = null;
        formFields = null;
        formFieldNames = null;
        getCountry = null;
        getState = null;
    }

    function setAllTextFieldsValues(addressModel) {
        if (addressModel instanceof Backbone.Model) {
            _.each(textFieldNames, function (name) {
                if (addressModel.get(name)) {
                    $[name].setValue(addressModel.get(name));
                } else {
                    $[name].setValue('');
                }
            });
        }
    }

    function clearAllErrors() {
        _.each(formFieldNames, function (currentfield) {
            if ($[currentfield + '_error']) {
                clearError($[currentfield], $[currentfield + '_error'], $[currentfield].originalBorderColor || Alloy.Styles.color.text.lightest, $[currentfield].originalBackgroundColor || Alloy.Styles.color.text.lightest, true);
            }
        });
    }

    function getAllTextFields() {
        return textFields;
    }

    function getAllTextFieldNames() {
        return textFieldNames;
    }

    function getAllFormFields() {
        return formFields;
    }

    function getAllFormFieldNames() {
        return formFieldNames;
    }

    function saveFormElement(elementName, elementType) {
        if (elementType == 'TextField') {
            textFields.push($[elementName]);
            textFieldNames.push(elementName);
        }
        if (elementType == 'TextField' || elementType == 'View') {
            formFields.push($[elementName]);
            formFieldNames.push(elementName);
        }
    }

    function applyClass(UIObjectName, classArray) {
        if (Object.prototype.toString.apply(classArray) === '[object Array]') {
            _.each(classArray, function (className, index, list) {
                if (viewLayoutData.addressForm.styles.class[className]) {
                    _.extend($[UIObjectName], viewLayoutData.addressForm.styles.class[className]);
                }
            });
        }
    }

    function applySingleStyle(UIObjectName) {
        if (viewLayoutData.addressForm.styles.id[UIObjectName]) {
            _.extend($[UIObjectName], viewLayoutData.addressForm.styles.id[UIObjectName]);
        }
    }

    function createAddressLineContainerView(viewObj) {
        $[viewObj.name] = Ti.UI['create' + viewObj.type]();
        applyClass(viewObj.name, viewObj.class);
        applySingleStyle(viewObj.name);
        if (Object.prototype.toString.apply(viewObj.content) === '[object Array]') {
            _.each(viewObj.content, function (currentContent, index, list) {
                switch (currentContent.type) {
                    case 'View':
                        createAddressLineContainerView(currentContent);
                        $[viewObj.name].add($[currentContent.name]);
                        break;
                    case 'CountryPicker':
                        if (_.isFunction(initializeCountryPicker)) {
                            initializeCountryPicker(true);
                            saveFormElement(viewObj.name, viewObj.type);
                        }

                        break;
                    case 'StatePicker':
                        if (_.isFunction(initializeStatePicker)) {
                            initializeStatePicker(true);
                            saveFormElement(viewObj.name, viewObj.type);
                        }
                        break;
                    default:
                        $[currentContent.name] = Ti.UI['create' + currentContent.type]();
                        saveFormElement(currentContent.name, currentContent.type);
                        applyClass(currentContent.name, currentContent.class);
                        applySingleStyle(currentContent.name);
                        $[currentContent.name].name = currentContent.name;
                        $[viewObj.name].add($[currentContent.name]);
                        break;
                }
            });
        }
    }

    function renderAddressViewInContainer(containerViewId) {
        if (containerViewId) {
            _.each(viewLayoutData.addressForm.lines, function (currentLine, index, list) {
                createAddressLineContainerView(currentLine);
                $[containerViewId].add($[currentLine.name]);
            });
            setTextFieldsBlurEventHandlers();
            setOnReturnEventsHandlers();
        }
    }

    function setTextFieldsBlurEventHandlers() {
        _.each(textFieldNames, function (currentTextField) {
            if ($[currentTextField].required && !$[currentTextField].hasSpecificOnBlurValidationFunction) {
                $[currentTextField].addEventListener('blur', onTextFieldBlurEvent);
            }
        });

        if ($.postal_code) {
            $.postal_code.addEventListener('blur', onPostalCodeBlur);
        }
        if ($.phone) {
            $.phone.addEventListener('blur', onPhoneBlur);
        }
        if ($.email_address) {
            $.email_address.addEventListener('blur', onEmailAddressBlur);
        }
    }

    function unsetTextFieldsBlurEventHandlers() {
        _.each(textFieldNames, function (currentTextField) {
            if ($[currentTextField].required && !$[currentTextField].hasSpecificOnBlurValidationFunction) {
                $[currentTextField].removeEventListener('blur', onTextFieldBlurEvent);
            }
        });

        if ($.postal_code) {
            $.postal_code.removeEventListener('blur', onPostalCodeBlur);
        }
        if ($.phone) {
            $.phone.removeEventListener('blur', onPhoneBlur);
        }
        if ($.email_address) {
            $.email_address.removeEventListener('blur', onEmailAddressBlur);
        }
    }

    function onTextFieldBlurEvent(evt) {
        var currentTextField = evt.source.name;

        if (!$[currentTextField] || !$[currentTextField + '_error']) {
            return;
        }
        if (evt.source.getValue().trim() === '') {
            showError($[currentTextField], $[currentTextField + '_error'], errorMsgs[currentTextField + '_error'] ? errorMsgs[currentTextField + '_error'] : errorMsgs.required_field_error, true);
        } else {
            clearError($[currentTextField], $[currentTextField + '_error'], Alloy.Styles.color.text.lightest, Alloy.Styles.color.text.lightest, true);
        }
    }

    function onPostalCodeBlur() {
        validatePostalCodeField();
    }

    function onEmailAddressBlur() {
        validateEmailAddressField();
    }

    function onPhoneBlur() {
        validatePhoneField();
    }

    function isPostalCodeValid() {
        if (!_.isFunction(getCountry)) {
            return;
        }

        var zip = getPostalCode();

        var country_code = getCountry();
        var regex = Alloy.CFG.regexes.postal_code[country_code];
        var postal_regex = new RegExp(regex, 'i');

        if (postal_regex && zip && zip !== '' && postal_regex.test(zip) || !postal_regex) {
            return true;
        } else {
            return false;
        }
    }

    function showHideZipError(isValid) {
        if (!$.postal_code || !$.postal_code_error) {
            return;
        }
        if (isValid) {
            clearError($.postal_code, $.postal_code_error, $.postal_code.originalBorderColor || Alloy.Styles.color.text.lightest, $.postal_code.originalBackgroundColor || Alloy.Styles.color.text.lightest, true);
        } else {
            showError($.postal_code, $.postal_code_error, errorMsgs.postal_code_error || errorMsgs.required_field_error, true);
        }
    }

    function showHideEmailError(isValid) {
        if (!$.email_address || !$.email_address_error) {
            return;
        }
        if (isValid) {
            clearError($.email_address, $.email_address_error, $.email_address.originalBorderColor || Alloy.Styles.color.text.lightest, $.email_address.originalBackgroundColor || Alloy.Styles.color.text.lightest, true);
        } else {
            showError($.email_address, $.email_address_error, errorMsgs.email_address_error || errorMsgs.required_field_error, true);
        }
    }

    function isPhoneValid() {
        if (!_.isFunction(getCountry)) {
            return false;
        }
        var phoneRegex = new RegExp(Alloy.CFG.regexes.phone[getCountry()], 'i');
        var phoneNumber = getPhone();
        if (phoneNumber && phoneRegex && phoneNumber !== '' && phoneRegex.test(phoneNumber)) {
            return true;
        } else {
            return false;
        }
    }

    function isEmailAddressValid() {
        var emailRegex = new RegExp(Alloy.CFG.regexes.email, 'i');
        var email = getEmailAddress();
        if (email && emailRegex && email !== '' && emailRegex.test(email)) {
            return true;
        } else {
            return false;
        }
    }

    function validateEmailAddressField() {
        var isValid = isEmailAddressValid();
        showHideEmailError(isValid);
        return isValid;
    }

    function validatePostalCodeField() {
        var isValid = isPostalCodeValid();
        showHideZipError(isValid);
        return isValid;
    }

    function validatePhoneField() {
        var isValid = isPhoneValid();
        showHidePhoneError(isValid);
        return isValid;
    }

    function showHidePhoneError(isValid) {
        if (!$.phone || !$.phone_error) {
            return;
        }
        if (isValid) {
            clearError($.phone, $.phone_error, $.phone_error.originalBorderColor || Alloy.Styles.color.text.lightest, $.phone_error.originalBackgroundColor || Alloy.Styles.color.text.lightest, true);
        } else {
            showError($.phone, $.phone_error, errorMsgs.phone_error || errorMsgs.required_field_error, true);
        }
    }

    function showHideError(fieldId, isValid, message) {
        if (!fieldId || !_.isString(fieldId) || !$[fieldId] || !$[fieldId + '_error']) {
            return false;
        }
        if (isValid) {
            clearError($[fieldId], $[fieldId + '_error'], $[fieldId].originalBorderColor || Alloy.Styles.color.text.lightest, $[fieldId].originalBackgroundColor || Alloy.Styles.color.text.lightest, true);
        } else {
            showError($[fieldId], $[fieldId + '_error'], message || errorMsgs[fieldId + '_error'] || errorMsgs.required_field_error, true);
        }
        return isValid;
    }

    function getFirstName() {
        if ($.first_name) {
            return $.first_name.getValue().trim();
        }
    }

    function getLastName() {
        if ($.last_name) {
            return $.last_name.getValue().trim();
        }
    }

    function getAddress1() {
        if ($.address1) {
            return $.address1.getValue().trim();
        }
    }

    function getAddress2() {
        if ($.address2) {
            return $.address2.getValue().trim();
        }
    }

    function getCity() {
        if ($.city) {
            return $.city.getValue().trim();
        }
    }

    function getPostalCode() {
        if ($.postal_code) {
            return $.postal_code.getValue().trim();
        }
    }

    function getPhone() {
        if ($.phone) {
            return $.phone.getValue().trim();
        }
    }

    function getEmailAddress() {
        if ($.email_address) {
            return $.email_address.getValue().trim();
        }
    }

    function closeKeyboard() {
        _.each(textFields, function (textField) {
            textField.blur();
        });
    }

    function handleReturn(event) {
        if (event.source.name) {
            var index = _.indexOf(formFieldNames, event.source.name);
            index++;
            if (index <= formFields.length - 1) {
                if (getUIObjectType(formFields[index]) === 'TextField') {
                    formFields[index].focus();
                } else {
                    formFields[index].fireEvent('autoFocus');
                }
            }
        }
    }

    function setOnReturnEventsHandlers() {
        _.each(formFields, function (field, index) {
            var nextIdx = index + 1;
            if (nextIdx <= formFields.length - 1 && !field.noGenericReturnEvent && getUIObjectType(field) === 'TextField') {
                field.addEventListener('return', handleReturn);
            }
        });
    }

    function unsetOnReturnEventsHandlers() {
        _.each(formFields, function (field, index) {
            var nextIdx = index + 1;
            if (nextIdx <= formFields.length - 1 && !field.noGenericReturnEvent && getUIObjectType(field) === 'TextField') {
                field.removeEventListener('return', handleReturn);
            }
        });
    }

    function hasAnythingChanged(tfields, previousValues, params) {
        var currentValues = getAllFieldValues(tfields, params);
        return previousValues.trim() !== currentValues.trim();
    }

    function getAllFieldValues(tfields, params) {
        var currentValues = '';
        _.each(tfields != null ? tfields : getAllTextFields(), function (field) {
            if (tfields != null) {
                currentValues = currentValues + $[field].getValue();
            } else {
                currentValues = currentValues + field.getValue();
            }
        });
        _.each(params, function (parameter) {
            currentValues = currentValues + parameter;
        });
        return currentValues;
    }
}

module.exports = function (args) {
    return new addressFormManager(args);
};