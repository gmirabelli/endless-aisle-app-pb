

var animation = require('alloy/animation');
var logger = require('logging')('utils:DialogMgr', 'app/lib/DialogMgr');

function DialogMgr(mainWindow, options) {
    var options = null,
        dialogView = null,
        window = null,
        backdrop = null;

    this.showNotifyGrowl = require('dialogUtils').showNotifyGrowl;

    this.removeNotifyGrowl = require('dialogUtils').removeNotifyGrowl;

    this.showCustomDialog = require('dialogUtils').showCustomDialog;

    this.showConfirmationDialog = require('dialogUtils').showConfirmationDialog;

    this.showAlertDialog = require('dialogUtils').showAlertDialog;

    this.presentDialog = function (newDialogView, options) {
        logger.info('presentDialog called');

        this.dismissDialog();
        options = options || {};

        var properties;

        if (options.parent) {
            if (options.preferredSide == 'bottom') {
                var parentBounds = options.parent.rect,
                    origin = {
                    x: 0,
                    y: 0
                };
                origin.y += parentBounds.height + this.options.distance;
                var topLeft = options.parent.convertPointToView(origin, this.window);

                logger.info('parentBounds height: ' + parentBounds.height);
                logger.info('parentBounds width: ' + parentBounds.width);
                logger.info('topLeft x: ' + topLeft.x);
                logger.info('topLeft y: ' + topLeft.y);

                var availableSpaceBelow = this.window.size.height - topLeft.y - this.options.minMargin;
                var availableSpaceAbove = topLeft.y - this.options.minMargin - parentBounds.height;

                var effectiveHeight;

                if (availableSpaceBelow < options.preferredHeight && availableSpaceAbove > availableSpaceBelow) {
                    effectiveHeight = options.preferredHeight > availableSpaceAbove ? availableSpaceAbove : options.preferredHeight;

                    properties = {
                        top: topLeft.y - parentBounds.height - effectiveHeight,
                        left: topLeft.x,
                        width: parentBounds.width,
                        height: effectiveHeight
                    };
                } else {
                    effectiveHeight = options.preferredHeight > availableSpaceBelow ? availableSpaceBelow : options.preferredHeight;

                    properties = {
                        top: topLeft.y,
                        left: topLeft.x,
                        width: parentBounds.width,
                        height: effectiveHeight
                    };
                }
            }
        } else {
            logger.info('NOT ATTACHED TO PARENT');
        }

        if (properties) {
            newDialogView.applyProperties(properties);
        }

        this.window.add(this.backdrop);
        this.window.add(newDialogView);
        newDialogView.setZIndex(999);
        this.dialogView = newDialogView;
    };

    this.dismissDialog = function () {
        if (this.dialogView) {
            logger.info('dismissDialog removing view ' + JSON.stringify(this.dialogView, null, 4));
            this.window.remove(this.dialogView);
            this.window.remove(this.backdrop);
            this.dialogView = null;
        }
    };

    this.presentModalDialog = function (view) {
        logger.info('presentModalDialog called for view ' + JSON.stringify(view, null, 4));
        this.dismissDialog();
        view.setZIndex(101);
        this.window.add(view);
    };

    this.setWindow = function (mainWindow) {
        this.window = mainWindow;
    };

    this.getWindow = function () {
        return this.window;
    };

    this.dismissModalDialog = function (view, fadeOut) {
        logger.info('dismissModalDialog called for view ' + JSON.stringify(view, null, 4));
        if (fadeOut) {
            animation.fadeAndRemove(view, 500, this.window);
        } else {
            this.window.remove(view);
        }
    };

    this.options = _.extend({
        opacity: 0.02,
        minMargin: 10,
        distance: 0,
        backgroundColor: Alloy.Styles.color.background.white
    }, options);
    this.window = mainWindow;
    this.backdrop = Ti.UI.createView({
        backgroundColor: this.options.backgroundColor,
        opacity: this.options.opacity,
        left: 0,
        right: 0,
        top: 0,
        bottom: 0
    });

    this.backdrop.setZIndex(200);
    logger.info('adding click listener');
    var self = this;
    this.backdrop.addEventListener('click', function () {
        self.dismissDialog();
    });
}

module.exports = DialogMgr;