
function mixinImageServiceMethods(model_class) {
    if (!model_class.prototype.getHeroImage && !model_class.prototype.getCartImage && !model_class.prototype.getProductTileImage) {
        require(Alloy.CFG.image_service.type + 'Methods').mixinImageServiceMethods(model_class);
    }
}

module.exports = mixinImageServiceMethods;