

var logger = require('logging')('utils:dialogUtils', 'app/lib/dialogUtils');

var notifyGrowl = null;

exports.showActivityIndicator = showActivityIndicator;
exports.showNotifyGrowl = showNotifyGrowl;
exports.removeNotifyGrowl = removeNotifyGrowl;
exports.showAlertDialog = showAlertDialog;
exports.showConfirmationDialog = showConfirmationDialog;
exports.showCustomDialog = showCustomDialog;

function showActivityIndicator(deferred) {
    var win = Titanium.UI.createWindow({
        fullScreen: true,
        touchEnabled: true
    });
    var activityIndicator = Ti.UI.createActivityIndicator({
        style: Ti.UI.ActivityIndicatorStyle.BIG,
        height: 100,
        width: 100,
        backgroundColor: Alloy.Styles.color.background.darkest,
        borderColor: Alloy.Styles.color.border.white,
        borderRadius: 20,
        opacity: 0.8
    });
    var dismiss = function () {
        Alloy.eventDispatcher.stopListening(Alloy.eventDispatcher, 'hideAuxillaryViews', dismiss);
        if (win) {
            win.close();
            win.remove(activityIndicator);
            win = null;
            activityIndicator.hide();
            activityIndicator = null;
        }
    };
    Alloy.eventDispatcher.listenTo(Alloy.eventDispatcher, 'hideAuxillaryViews', dismiss);

    activityIndicator.addEventListener('doubletap', dismiss);

    activityIndicator.show();
    win.add(activityIndicator);
    win.open();

    deferred.always(dismiss);
}

function showNotifyGrowl(args) {
    if (notifyGrowl && !notifyGrowl.getPreventAutoClose()) {
        notifyGrowl.setMessage(args.label);
        if (args.preventAutoClose) {
            notifyGrowl.setPreventAutoClose(true);
        }
    } else {
        if (!notifyGrowl || notifyGrowl && notifyGrowl.getPreventAutoClose() && notifyGrowl.getMessage() != args.label) {
            removeNotifyGrowl(null, true).always(function () {
                notifyGrowl = showCustomDialog({
                    controllerPath: 'components/notifyGrowl',
                    initOptions: args,
                    continueEvent: 'notifyGrowl:dismiss',
                    continueFunction: function () {
                        notifyGrowl = null;
                    },
                    ignoreHideAuxillary: true,
                    fadeOut: true
                });
            });
        }
    }
}

function removeNotifyGrowl(message, wait) {
    var deferred = new _.Deferred();
    if (notifyGrowl) {
        if (message) {
            if (notifyGrowl.getMessage() == message) {
                notifyGrowl.dismiss();
            }
            deferred.resolve();
        } else if (wait && notifyGrowl.getPreventAutoClose()) {
            notifyGrowl.once('notifyGrowl:dismiss', function () {
                deferred.resolve();
            });
        } else {
            notifyGrowl.dismiss();
            deferred.resolve();
        }
    } else {
        deferred.resolve();
    }
    return deferred.promise();
}

function showAlertDialog(args) {
    showConfirmationDialog(_.extend({
        hideCancel: true,
        titleString: _L('Alert')
    }, args));
}

function showConfirmationDialog(args) {
    if (!args.messageString) {
        logger.error('showConfirmationDialog messageString was not specified');
        return;
    }
    var controllerPath = 'components/ConfirmationDialog';
    var controllerArgs = {
        okButtonString: args.okButtonString,
        cancelButtonString: args.cancelButtonString,
        hideCancel: args.hideCancel,
        icon: args.icon,
        messageString: args.messageString,
        titleString: args.titleString
    };
    showCustomDialog({
        controllerPath: controllerPath,
        options: controllerArgs,
        cancelEvent: 'confirmation_dialog:dismiss',
        cancelFunction: function () {
            if (_.isFunction(args.cancelFunction)) {
                args.cancelFunction();
            }
        },
        continueEvent: 'confirmation_dialog:continue',
        continueFunction: function () {
            if (_.isFunction(args.okFunction)) {
                args.okFunction();
            }
        }
    });
}

function showCustomDialog(args) {

    if (!args.controllerPath) {
        logger.error('showCustomDialog controllerPath was not specified');
        return;
    }
    if (!args.continueEvent) {
        logger.error('showCustomDialog continueEvent was not specified');
        return;
    }

    if (args.fullScreen) {
        Alloy.eventDispatcher.trigger('app:navigation', {
            view: args.viewName
        });
    } else {
        Alloy.eventDispatcher.trigger('app:dialog_displayed', {
            view: args.viewName
        });
    }
    var customDialog = Alloy.createController(args.controllerPath, args.options);
    var drawer = customDialog.getView();
    var ignoreHideAuxillary = args.ignoreHideAuxillary || false;
    var fadeOut = args.fadeOut || false;

    var closeDialog = function (event) {
        if (customDialog) {
            logger.info('closeDialog called as an external hide occurred');

            customDialog.trigger(args.continueEvent, event);
        }
    };

    var createListener = function (eventName, functionName) {
        if (!eventName) {
            return;
        }
        customDialog.once(eventName, function (event) {
            logger.info('customDialog event called ' + eventName + ' for ' + args.controllerPath);
            if (customDialog) {
                if (!ignoreHideAuxillary) {
                    Alloy.eventDispatcher.stopListening(Alloy.eventDispatcher, 'hideAuxillaryViews', closeDialog);
                } else {
                    Alloy.eventDispatcher.stopListening(Alloy.eventDispatcher, 'associate_logout', closeDialog);
                }
                Alloy.Dialog.dismissModalDialog(drawer, fadeOut);
                logger.info('customDialog calling DEINIT for ' + args.controllerPath);
                customDialog.stopListening();
                customDialog.deinit && customDialog.deinit();
                customDialog = null;

                if (_.isFunction(functionName)) {
                    setTimeout(function () {
                        functionName(event);
                    }, 100);
                }
            }
        });
    };
    createListener(args.cancelEvent, args.cancelFunction);
    createListener(args.continueEvent, args.continueFunction);
    if (!ignoreHideAuxillary) {
        Alloy.eventDispatcher.listenTo(Alloy.eventDispatcher, 'hideAuxillaryViews', closeDialog);
    } else {
        Alloy.eventDispatcher.listenTo(Alloy.eventDispatcher, 'associate_logout', closeDialog);
    }

    if (customDialog.init) {
        logger.info('customDialog calling INIT for ' + args.controllerPath);
        var promise = customDialog.init(args.initOptions);
        if (promise && _.isFunction(promise.always)) {
            var deferred = new _.Deferred();
            showActivityIndicator(deferred);
            promise.always(function () {
                deferred.resolve();
                Alloy.Dialog.presentModalDialog(drawer);
            });
        } else {
            Alloy.Dialog.presentModalDialog(drawer);
        }
    } else {
        Alloy.Dialog.presentModalDialog(drawer);
    }
    return customDialog;
}