

var logger = require('logging')('geolocation', 'app/lib/geolocation');

exports.getCurrentLocation = getCurrentLocation;

function getCurrentLocation(callback) {
    if (Ti.Geolocation.locationServicesEnabled) {
        Ti.Geolocation.accuracy = Ti.Geolocation.ACCURACY_BEST;
        Ti.Geolocation.distanceFilter = 10;
        Ti.Geolocation.preferredProvider = Ti.Geolocation.PROVIDER_GPS;

        Ti.Geolocation.getCurrentPosition(function (event) {
            if (event.error) {
                logger.error('Error: ' + event.error);
            } else {
                callback(event.coords);
            }
        });
    } else {
        logger.error('Geo Location Is NOT Active');
    }
};