(function (root) {
    var breaker = {},
        AP = Array.prototype,
        OP = Object.prototype,
        hasOwn = OP.hasOwnProperty,
        toString = OP.toString,
        forEach = AP.forEach,
        indexOf = AP.indexOf,
        slice = AP.slice;

    var _each = function (obj, iterator, context) {
        var key, i, l;

        if (!obj) {
            return;
        }
        if (forEach && obj.forEach === forEach) {
            obj.forEach(iterator, context);
        } else if (obj.length === +obj.length) {
            for (i = 0, l = obj.length; i < l; i++) {
                if (i in obj && iterator.call(context, obj[i], i, obj) === breaker) {
                    return;
                }
            }
        } else {
            for (key in obj) {
                if (hasOwn.call(obj, key)) {
                    if (iterator.call(context, obj[key], key, obj) === breaker) {
                        return;
                    }
                }
            }
        }
    };

    var _isFunction = function (obj) {
        return !!(obj && obj.constructor && obj.call && obj.apply);
    };

    var _extend = function (obj) {

        _each(slice.call(arguments, 1), function (source) {
            var prop;

            for (prop in source) {
                if (source[prop] !== void 0) {
                    obj[prop] = source[prop];
                }
            }
        });
        return obj;
    };

    var _inArray = function (elem, arr, i) {
        var len;

        if (arr) {
            if (indexOf) {
                return indexOf.call(arr, elem, i);
            }

            len = arr.length;
            i = i ? i < 0 ? Math.max(0, len + i) : i : 0;

            for (; i < len; i++) {
                if (i in arr && arr[i] === elem) {
                    return i;
                }
            }
        }

        return -1;
    };

    var class2type = {};

    _each("Boolean Number String Function Array Date RegExp Object".split(" "), function (name, i) {
        class2type["[object " + name + "]"] = name.toLowerCase();
    });

    var _type = function (obj) {
        return obj == null ? String(obj) : class2type[toString.call(obj)] || "object";
    };

    var _d = {};

    var optionsCache = {};

    function createOptions(options) {
        var object = optionsCache[options] = {};
        _each(options.split(/\s+/), function (flag) {
            object[flag] = true;
        });
        return object;
    }

    _d.Callbacks = function (options) {
        options = typeof options === "string" ? optionsCache[options] || createOptions(options) : _extend({}, options);

        var memory,
            fired,
            firing,
            firingStart,
            firingLength,
            firingIndex,
            list = [],
            stack = !options.once && [],
            fire = function (data) {
            memory = options.memory && data;
            fired = true;
            firingIndex = firingStart || 0;
            firingStart = 0;
            firingLength = list.length;
            firing = true;
            for (; list && firingIndex < firingLength; firingIndex++) {
                if (list[firingIndex].apply(data[0], data[1]) === false && options.stopOnFalse) {
                    memory = false;

                    break;
                }
            }
            firing = false;
            if (list) {
                if (stack) {
                    if (stack.length) {
                        fire(stack.shift());
                    }
                } else if (memory) {
                    list = [];
                } else {
                    self.disable();
                }
            }
        },
            self = {
            add: function () {
                if (list) {
                    var start = list.length;
                    (function add(args) {
                        _each(args, function (arg) {
                            var type = _type(arg);
                            if (type === "function") {
                                if (!options.unique || !self.has(arg)) {
                                    list.push(arg);
                                }
                            } else if (arg && arg.length && type !== "string") {
                                add(arg);
                            }
                        });
                    })(arguments);

                    if (firing) {
                        firingLength = list.length;
                    } else if (memory) {
                        firingStart = start;
                        fire(memory);
                    }
                }
                return this;
            },

            remove: function () {
                if (list) {
                    _each(arguments, function (arg) {
                        var index;
                        while ((index = _inArray(arg, list, index)) > -1) {
                            list.splice(index, 1);

                            if (firing) {
                                if (index <= firingLength) {
                                    firingLength--;
                                }
                                if (index <= firingIndex) {
                                    firingIndex--;
                                }
                            }
                        }
                    });
                }
                return this;
            },

            has: function (fn) {
                return _inArray(fn, list) > -1;
            },

            empty: function () {
                list = [];
                return this;
            },

            disable: function () {
                list = stack = memory = undefined;
                return this;
            },

            disabled: function () {
                return !list;
            },

            lock: function () {
                stack = undefined;
                if (!memory) {
                    self.disable();
                }
                return this;
            },

            locked: function () {
                return !stack;
            },

            fireWith: function (context, args) {
                args = args || [];
                args = [context, args.slice ? args.slice() : args];
                if (list && (!fired || stack)) {
                    if (firing) {
                        stack.push(args);
                    } else {
                        fire(args);
                    }
                }
                return this;
            },

            fire: function () {
                self.fireWith(this, arguments);
                return this;
            },

            fired: function () {
                return !!fired;
            }
        };

        return self;
    };

    _d.Deferred = function (func) {

        var tuples = [["resolve", "done", _d.Callbacks("once memory"), "resolved"], ["reject", "fail", _d.Callbacks("once memory"), "rejected"], ["notify", "progress", _d.Callbacks("memory")]],
            state = "pending",
            promise = {
            state: function () {
                return state;
            },
            always: function () {
                deferred.done(arguments).fail(arguments);
                return this;
            },
            then: function () {
                var fns = arguments;

                return _d.Deferred(function (newDefer) {

                    _each(tuples, function (tuple, i) {
                        var action = tuple[0],
                            fn = fns[i];

                        deferred[tuple[1]](_isFunction(fn) ? function () {
                            var returned;
                            try {
                                returned = fn.apply(this, arguments);
                            } catch (e) {
                                newDefer.reject(e);
                                return;
                            }

                            if (returned && _isFunction(returned.promise)) {
                                returned.promise().done(newDefer.resolve).fail(newDefer.reject).progress(newDefer.notify);
                            } else {
                                newDefer[action !== "notify" ? 'resolveWith' : action + 'With'](this === deferred ? newDefer : this, [returned]);
                            }
                        } : newDefer[action]);
                    });

                    fns = null;
                }).promise();
            },

            promise: function (obj) {
                return obj != null ? _extend(obj, promise) : promise;
            }
        },
            deferred = {};

        promise.pipe = promise.then;

        _each(tuples, function (tuple, i) {
            var list = tuple[2],
                stateString = tuple[3];

            promise[tuple[1]] = list.add;

            if (stateString) {
                list.add(function () {
                    state = stateString;
                }, tuples[i ^ 1][2].disable, tuples[2][2].lock);
            }

            deferred[tuple[0]] = list.fire;
            deferred[tuple[0] + "With"] = list.fireWith;
        });

        promise.promise(deferred);

        if (func) {
            func.call(deferred, deferred);
        }

        return deferred;
    };

    _d.when = function (subordinate) {
        var i = 0,
            resolveValues = _type(subordinate) === 'array' && arguments.length === 1 ? subordinate : slice.call(arguments),
            length = resolveValues.length,
            remaining = length !== 1 || subordinate && _isFunction(subordinate.promise) ? length : 0,
            deferred = remaining === 1 ? subordinate : _d.Deferred(),
            updateFunc = function (i, contexts, values) {
            return function (value) {
                contexts[i] = this;
                values[i] = arguments.length > 1 ? slice.call(arguments) : value;
                if (values === progressValues) {
                    deferred.notifyWith(contexts, values);
                } else if (! --remaining) {
                    deferred.resolveWith(contexts, values);
                }
            };
        },
            progressValues,
            progressContexts,
            resolveContexts;

        if (length > 1) {
            progressValues = new Array(length);
            progressContexts = new Array(length);
            resolveContexts = new Array(length);
            for (; i < length; i++) {
                if (resolveValues[i] && _isFunction(resolveValues[i].promise)) {
                    resolveValues[i].promise().done(updateFunc(i, resolveContexts, resolveValues)).fail(deferred.reject).progress(updateFunc(i, progressContexts, progressValues));
                } else {
                    --remaining;
                }
            }
        }

        if (!remaining) {
            deferred.resolveWith(resolveContexts, resolveValues);
        }

        return deferred.promise();
    };

    if (typeof module !== "undefined" && module.exports) {
        module.exports = _d;
    } else if (typeof root._ !== "undefined") {
        root._.mixin(_d);
    } else {
        root._ = _d;
    }
})(this);