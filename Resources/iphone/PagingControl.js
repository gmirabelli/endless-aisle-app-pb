

var logger = require('logging')('application:PagingControl', 'app/lib/PagingControl');

function PagingControl(scrollableView) {
    var viewNumber = 0;

    this.scrollEventHandler = function (event) {
        var currentPage = event.currentPage;
        if (currentPage >= 0 && viewNumber != currentPage) {
            logger.trace('***firing pagechanged ' + currentPage);
            scrollableView.fireEvent('pagechanged', event);
            viewNumber = currentPage;
        }
    };
    scrollableView.addEventListener('scroll', this.scrollEventHandler);

    this.pageChangedEventHandler = function (event) {
        for (var i = 0; i < numberOfPages; i++) {
            pages[i].setOpacity(0.5);
        }

        if (event.currentPage < pages.length) {
            pages[event.currentPage].setOpacity(1);
        } else {
            logger.error('The event.currentPage was greater than the number of pages?');
        }
    };
    scrollableView.addEventListener('pagechanged', this.pageChangedEventHandler);

    var numberOfPages = scrollableView.getViews().length;

    this.container = Titanium.UI.createView({
        height: 60,
        width: Ti.UI.SIZE,
        top: 595
    });

    var pages = [];

    var pageClickEventHandlers = [];

    for (var i = 0; i < numberOfPages; i++) {
        var page = Titanium.UI.createView({
            borderRadius: 4,
            width: 8,
            height: 8,
            left: 15 * i,
            backgroundColor: Alloy.Styles.accentColor,
            opacity: 0.5,
            pageIndex: i,
            accessibilityLabel: 'page_' + i
        });
        pageClickEventHandlers.push(function (event) {
            var halfWay = scrollableView.views.length / 2,
                curPage = scrollableView.currentPage;
            if ('pageIndex' in event.source) {
                if (event.source.pageIndex >= halfWay) {
                    if (curPage + 1 < scrollableView.views.length) {
                        scrollableView.scrollToView(curPage + 1);
                        scrollableView.fireEvent('pagechanged', {
                            currentPage: curPage + 1
                        });
                    }
                } else if (curPage >= 1) {
                    scrollableView.scrollToView(curPage - 1);
                    scrollableView.fireEvent('pagechanged', {
                        currentPage: curPage - 1
                    });
                }
            }
        });
        page.addEventListener('click', pageClickEventHandlers[pageClickEventHandlers.length - 1]);

        pages.push(page);

        this.container.add(page);
    }

    pages[0].setOpacity(1);
    this.pages = pages;
    this.pageClickEventHandlers = pageClickEventHandlers;
    this.scrollableView = scrollableView;
};

PagingControl.prototype.deinit = function () {
    logger.info('DEINIT called');
    this.scrollableView.removeEventListener('scroll', this.scrollEventHandler);
    this.scrollableView.removeEventListener('pagechanged', this.pageChangedEventHandler);
    var pageClickEventHandlers = this.pageClickEventHandlers;
    _.each(this.pages, function (currentPage, index) {
        currentPage.removeEventListener('click', pageClickEventHandlers[index]);
    });
    _.each(this.container.views, function (view) {
        this.container.removeView(view);
        view = null;
    });
    this.container = null;
    this.pages = [];
    this.pageClickEventHandlers = pageClickEventHandlers = [];
    this.scrollEventHandler = null;
    this.pageChangedEventHandler = null;
    this.scrollableView = null;
};

PagingControl.prototype.getView = function () {
    return this.container;
};

module.exports = PagingControl;