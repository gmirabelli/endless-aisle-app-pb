

var ga = require('ti.ga');
var logger = require('logging')('analytics:googleAnalytics', 'app/lib/googleAnalytics');

var initialized = false;
var tracker = null;
var trackingId = null;

exports.startAnalytics = startAnalytics;
exports.stopAnalytics = stopAnalytics;
exports.dispatchEvents = dispatchEvents;
exports.fireAnalyticsEvent = fireAnalyticsEvent;
exports.fireUserEvent = fireUserEvent;
exports.fireScreenEvent = fireScreenEvent;
exports.fireTransactionEvent = fireTransactionEvent;
exports.fireTransactionItemEvent = fireTransactionItemEvent;
exports.fireExceptionEvent = fireExceptionEvent;

function init() {
    if (Alloy.CFG.analytics.google.tracker) {
        tracker = ga.createTracker({
            trackingId: Alloy.CFG.analytics.google.tracker,
            useSecure: true,
            debug: Ti.App.deployType === 'development'
        });
        trackingId = tracker.trackingId;
        ga.setDispatchInterval(Alloy.CFG.analytics.dispatch_interval);
        initialized = true;
    }
}

function startAnalytics(event) {}

function stopAnalytics(event) {}

function dispatchEvents() {
    if (initialized) {
        var time = new Date().getTime();
        tracker.dispatchAnalyticsEvents({});
        var totalTime = new Date().getTime() - time;
        logger.info('Dispatched google analytics events in ' + totalTime + 'ms');
    }
}

function fireAnalyticsEvent(data) {
    logger.info('sending ' + data.category + ' analytics event with action ' + data.action);

    if (tracker) {
        tracker.trackEvent({
            category: data.category,
            action: data.action,
            label: data.label,
            value: data.value
        });
    } else {
        logger.info('could not send ' + data.category + ' analytics event with action ' + data.action);
    }
}

function fireUserEvent(data) {
    logger.info('sending ' + data.category + ' analytics user event with action ' + data.action);

    if (tracker && data.userId) {
        tracker.setUser({
            userId: data.userId,
            category: data.category,
            action: data.action
        });
    } else {
        logger.info('could not send ' + data.category + ' analytics user event with action ' + data.action);
    }
}

function fireScreenEvent(data) {
    logger.info('sending analytics screen event for screen ' + data.screen);

    if (tracker) {
        tracker.trackScreen({
            screenName: data.screen
        });
    } else {
        logger.info('could not send analytics screen event for screen ' + data.screen);
    }
}

function fireTransactionEvent(data) {
    logger.info('sending analytics transaction event with id ' + data.orderId);

    if (tracker) {
        tracker.trackTransaction({
            transactionId: data.orderId,
            affiliation: data.storeId,
            revenue: data.total,
            tax: data.tax,
            shipping: data.shipping,
            currency: data.currency
        });
    } else {
        logger.info('could not send analytics transaction event with id ' + data.orderId);
    }
}

function fireTransactionItemEvent(data) {
    logger.info('sending analytics transaction item event with id ' + data.productId + ' for order ' + data.orderId);

    if (tracker) {
        tracker.trackTransactionItem({
            transactionId: data.orderId,
            name: data.name,
            sku: data.productId,
            price: data.price,
            quantity: data.quantity,
            currency: data.currency
        });
    } else {
        logger.info('could not send analytics transaction item event with id ' + data.productId + ' for order ' + data.orderId);
    }
}

function fireExceptionEvent(data) {
    logger.info('sending analytics exception event with description ' + data.description);

    if (tracker) {
        tracker.trackException({
            description: data.description,
            fatal: data.fatal
        });
    } else {
        logger.info('could not send analytics exception event with description ' + data.description);
    }
}

init();