

module.exports = {
    devices: {
        payment_terminal_module: 'verifoneDevice',

        barcode_scanner_module: 'barcodeScanner',

        printer_module: 'epsonPrinter',

        validate_url: 'EAStore-ValidateDevice',

        bluetoothDevicePicker: false,

        verifone: {
            card_swipe_timeout: 120,
            registart_url: 'Verifone-ActivateDevice'
        },

        adyen: {
            paymentStrings: {
                mc: {
                    default: "Mastercard",
                    de: "MasterCard",
                    fr: "MasterCard",
                    ja: "マスターカード",
                    "zh-CN": "万事达"
                },
                visa: {
                    default: "Visa",
                    ja: "ビザ",
                    "zh-CN": "签证"
                },

                alipay: "AliPay",
                wechatpay_pos: "WeChat",
                amex: "Amex",
                discover: "Discover",
                diners: "Discover",
                maestro: "Mastercard",
                jcb: "JCB",
                cup: "China UnionPay",
                vpay: "Visa"
            }
        }
    }
};