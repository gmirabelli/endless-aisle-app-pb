

module.exports = {
    countryConfig: {
        US: {
            ocapi: {
                site_url: '/s/SiteGenesis'
            },
            storefront: {
                site_url: '/on/demandware.store/Sites-SiteGenesis-Site'
            },
            languagesSupported: ['en', 'fr', 'zh-CN', 'ja', 'de'],
            displayName: 'United States',
            value: 'US',
            appCurrency: 'USD' }
    },
    languageConfig: [{
        displayName: 'English',
        ocapi_locale: 'en-US',
        storefront_locale: '/en_US',
        value: 'en'
    }, {
        displayName: 'French',
        ocapi_locale: 'fr-FR',
        storefront_locale: '/fr_FR',
        value: 'fr'
    }, {
        displayName: 'Chinese',
        ocapi_locale: 'zh-CN',
        storefront_locale: '/zh_CN',
        value: 'zh-CN'
    }, {
        displayName: 'Japanese',
        ocapi_locale: 'ja-JP',
        storefront_locale: '/ja_JP',
        value: 'ja'
    }, {
        displayName: 'German',
        ocapi_locale: 'de-DE',
        storefront_locale: '/de_DE',
        value: 'de'
    }],
    currencyConfig: {
        USD: {
            currencyFormat: '$0,0.00',
            currencyLocale: 'en',
            delimiters: {
                thousands: ',',
                decimal: '.' },
            currencySymbol: '$' },
        EUR: {
            currencyFormat: '0,0.00$',
            currencyLocale: 'en',
            delimiters: {
                thousands: '.',
                decimal: ',' },
            currencySymbol: '€' },
        JPY: {
            currencyFormat: '$0,0',
            currencyLocale: 'en',
            delimiters: {
                thousands: ',',
                decimal: '.' },
            currencySymbol: '¥' }
    }
};