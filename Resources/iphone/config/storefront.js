

module.exports = {
    storefront: {
        enable_proxy_cache: true,

        enable_http_cache: true,

        validate_secure_cert: false,

        site_url: '/on/demandware.store/Sites-SiteGenesis-Site',

        locale_url: '/default'
    }
};