

module.exports = {
    local_address: {
        US: 'config/address/addressForm_NA',
        IT: 'config/address/addressForm_EU',
        CA: 'config/address/addressForm_NA',
        FR: 'config/address/addressForm_EU',
        NL: 'config/address/addressForm_EU',
        GB: 'config/address/addressForm_EU',
        ES: 'config/address/addressForm_EU',
        DE: 'config/address/addressForm_EU',
        JP: 'config/address/addressForm_Asia',
        CN: 'config/address/addressForm_Asia',
        default: 'config/address/addressForm_NA'
    }
};