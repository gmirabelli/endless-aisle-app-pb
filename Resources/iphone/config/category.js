

module.exports = {
    category_grid: {
        aspect: {
            width: 544,
            height: 544
        },

        tablet: {
            landscape: {
                map_to_height: true,

                spacing: {
                    width: 20,
                    height: 20
                },

                min_padding: {
                    width: 20,
                    height: 20
                },

                max_cols: 4,

                max_bounds: {
                    width: 1024,
                    height: 643
                }
            }
        }
    },

    navigation: {
        use_mega_menu: false
    }
};