

var analytics = require('analyticsBase');
var logger = require('logging')('utils:EAUtils', 'app/lib/EAUtils');
var numeral = require('alloy/numeral.min');
var countryConfig = require('config/countries').countryConfig;
var currencyConfig = require('config/countries').currencyConfig;
var loadConfigurations = require('appConfiguration').loadConfigurations;
var _s = require('underscore.string');

var _errorFieldHeight = 20;
var _textFieldHeight = 50;

exports.toCurrency = toCurrency;
exports.buildURLParams = buildURLParams;
exports.returnToShopping = returnToShopping;
exports.returnToShoppingOrCart = returnToShoppingOrCart;
exports.buildStorefrontURL = buildStorefrontURL;
exports.returnToLastSearchOrProduct = returnToLastSearchOrProduct;
exports.history = history;
exports.getConfigValue = getConfigValue;
exports.setConfigValue = setConfigValue;
exports.showError = showError;
exports.showErrorLabelOnly = showErrorLabelOnly;
exports.clearError = clearError;
exports.clearErrorLabelOnly = clearErrorLabelOnly;
exports.removeAllViews = removeAllViews;
exports.countryCodeToCountryName = countryCodeToCountryName;
exports.countryNameToCountryCode = countryNameToCountryCode;
exports.strikeThrough = strikeThrough;
exports.zero = zero;
exports.emailLogs = emailLogs;
exports.sendErrorToServer = sendErrorToServer;
exports.sendSignatureToServer = sendSignatureToServer;
exports.doProductSearch = doProductSearch;
exports.getUIObjectType = getUIObjectType;
exports.showCustomerAddressAlert = showCustomerAddressAlert;
exports.addressVerification = addressVerification;
exports.formatDate = formatDate;
exports.updateLocaleGlobalVariables = updateLocaleGlobalVariables;
exports.verifyAddressEditBeforeNavigation = verifyAddressEditBeforeNavigation;
exports.isSymbolBasedLanguage = isSymbolBasedLanguage;
exports.isLatinBasedLanguage = isLatinBasedLanguage;
exports.appendURL = appendURL;
exports.fetchImagesForProducts = fetchImagesForProducts;
exports.getAddressNickname = getAddressNickname;
exports.getVisibleViewFromContainerView = getVisibleViewFromContainerView;
exports.getConsoleFile = getConsoleFile;
exports.uploadFileToServer = uploadFileToServer;
exports.uploadLogsToServer = uploadLogsToServer;
exports.getAddressStringFromAddressDataOrderAndType = getAddressStringFromAddressDataOrderAndType;
exports.buildRequestUrl = buildRequestUrl;
exports.getCurrencyConfiguration = getCurrencyConfiguration;

function toCurrency(num, currencyCode) {
    var countryConfig = require('config/countries').countryConfig[Alloy.CFG.countrySelected];
    if (!currencyCode) {
        currencyCode = countryConfig.appCurrency;
    }
    numeral.language(currencyConfig[currencyCode].currencyLocale, {
        delimiters: {
            thousands: currencyConfig[currencyCode].delimiters.thousands,
            decimal: currencyConfig[currencyCode].delimiters.decimal
        },
        currency: {
            symbol: currencyConfig[currencyCode].currencySymbol
        }
    });
    numeral.language(currencyConfig[currencyCode].currencyLocale);
    return numeral(num).format(currencyConfig[currencyCode].currencyFormat);
}

function buildURLParams(params) {
    if (!params) {
        return '';
    }
    try {
        var attrs = JSON.parse(params),
            values = [],
            urlParams = '?';
        for (var property in attrs) {
            values.push(property + '=' + attrs[property]);
        }
        urlParams += values.join('&');
        return urlParams;
    } catch (ex) {
        logger.error('buildURLParams: Unable to parse: "' + params + '"');
        return '';
    }
}

function returnToShopping() {
    var currentHistory = Alloy.Collections.history;
    var historyLength = currentHistory.length;

    var info = null,
        h,
        route;

    for (var i = historyLength - 1; i > -1; i--) {
        h = currentHistory.at(i);

        route = h.get('route');

        if (route == 'product_search_result') {
            info = {
                route: route,
                switch_only: true
            };
            break;
        }
        if (route == 'product_detail') {
            info = {
                route: route,
                switch_only: true
            };
            break;
        }
    }

    info = info || {
        route: 'product_search_result',
        switch_only: true
    };
    Alloy.Router.navigate(info);
}

function returnToShoppingOrCart() {
    var currentHistory = Alloy.Collections.history;
    var historyLength = currentHistory.length;

    var info = null,
        h,
        route;

    for (var i = historyLength - 1; i > -1; i--) {
        h = currentHistory.at(i);

        route = h.get('route');

        if (route == 'product_search_result') {
            info = {
                route: route,
                switch_only: true
            };
            break;
        }
        if (route == 'product_detail') {
            info = {
                route: route,
                switch_only: true
            };
            break;
        }
        if (route == 'cart') {
            info = {
                route: route
            };
            break;
        }
    }

    info = info || {
        route: 'home'
    };
    Alloy.Router.navigate(info);
}

function buildStorefrontURL(scheme, pipeline) {
    return scheme + '://' + Alloy.CFG.storefront_host + Alloy.CFG.storefront.site_url + Alloy.CFG.storefront.locale_url + '/' + pipeline;
}

function returnToLastSearchOrProduct(historyCursor, currentProductId) {
    var currentHistory = Alloy.Collections.history;
    var historyLength = currentHistory.length;
    historyCursor = historyCursor || historyLength - 1;

    var info = null,
        h,
        route,
        switch_only,
        product_id;

    for (var i = historyCursor - 1; i > -1; i--) {
        h = currentHistory.at(i);

        var detailsJSON = h.get('details') || '{}';
        var details = JSON.parse(detailsJSON);

        if (details.route == 'product_search_result') {
            if (!details.single_hit) {
                info = _.extend({}, details, {
                    historyCursor: i
                });
                break;
            }
        }
        if (details.route == 'product_detail') {
            if (details.product_id) {
                if (currentProductId && currentProductId == details.product_id) {
                    continue;
                }
                info = {
                    route: details.route,
                    product_id: details.product_id,
                    historyCursor: i
                };
                break;
            }
        }
    }

    if (info && info.route == 'product_search_result') {
        Alloy.Router.navigateToProductSearch({
            switch_only: true
        });
        return;
    }

    info = info || {
        route: 'home'
    };
    Alloy.Router.navigate(info);
}

function history(steps) {
    setps = steps || 0;
    steps = Math.abs(steps);

    var currentHistory = Alloy.Collections.history;
    var historyLength = currentHistory.length;

    if (steps > historyLength) {
        steps = historyLength;
    }

    if (!steps) {
        return;
    }

    var entry = currentHistory.at(historyLength - steps - 1).toJSON();
    Alloy.Router.navigate(entry);
}

function getConfigValue(path, defaultValue) {
    if (!path) {
        return;
    };
    var cfgValue = Alloy.CFG,
        segments = path.split('.'),
        defaultValue = defaultValue || null;
    logger.info('getConfigValue for ' + path + ' defaultValue ' + defaultValue);
    _.each(segments, function (key) {
        if (!(key in cfgValue)) {
            cfgValue = defaultValue;
            return cfgValue;
        }
        cfgValue = cfgValue[key];
    });
    logger.info('getConfigValue is ' + cfgValue);
    return cfgValue;
}

function setConfigValue(path, value) {
    if (!path) {
        return;
    };
    logger.info('setConfigValue for ' + path + ' to ' + value);
    var segments = path.split('.');
    if (segments.length > 1) {
        var configValue = Alloy.CFG;
        for (var i = 0; i < segments.length - 1; i++) {
            configValue = configValue[segments[i]];
        }
        configValue[segments[i++]] = value;
    } else {
        Alloy.CFG[path] = value;
    }
}

function showError(textFieldComponent, labelComponent, errorMessage, adjustGrandParentHeight) {
    if (labelComponent && labelComponent.getText() !== errorMessage) {
        textFieldComponent.setBorderColor(Alloy.Styles.color.border.red);
        textFieldComponent.setBackgroundColor(Alloy.Styles.color.background.pink);

        labelComponent.setText(errorMessage);
        labelComponent.setVisible(true);
        labelComponent.setHeight(_errorFieldHeight);

        if (adjustGrandParentHeight) {
            var grandParent = textFieldComponent.getParent().getParent();
            if (grandParent.height && grandParent.height != _textFieldHeight + _errorFieldHeight) {
                grandParent.setHeight(_textFieldHeight + _errorFieldHeight);
            }
        }
    }
}

function showErrorLabelOnly(labelComponent, errorMessage) {
    if (labelComponent.text !== errorMessage) {
        labelComponent.setText(errorMessage);
        labelComponent.setVisible(true);
        labelComponent.setHeight(_errorFieldHeight);
    }
}

function clearError(textFieldComponent, labelComponent, borderColor, backgroundColor, adjustGrandParentHeight) {
    if (labelComponent && !_.isEmpty(labelComponent.text)) {
        textFieldComponent.setBorderColor(borderColor ? borderColor : Alloy.Styles.color.border.dark);
        textFieldComponent.setBackgroundColor(backgroundColor ? backgroundColor : Alloy.Styles.color.text.white);

        labelComponent.setText('');
        labelComponent.setVisible(false);
        labelComponent.setHeight(0);

        if (adjustGrandParentHeight) {
            var grandParent = textFieldComponent.getParent().getParent();
            var errorFlags = 0;

            _.each(grandParent.getChildren(), function (child) {
                if (child.getChildren()[1] && child.getChildren()[1].text && child.getChildren()[1].text != '') {
                    ++errorFlags;
                }
            });

            if (errorFlags == 0) {
                grandParent.setHeight(_textFieldHeight);
            }
        }
    }
}

function clearErrorLabelOnly(labelComponent) {
    if (!_.isEmpty(labelComponent.text)) {
        labelComponent.setText('');
        labelComponent.setVisible(false);
        labelComponent.setHeight(0);
    }
}

function removeAllViews(parentView) {
    _.each(parentView.getChildren(), function (child) {
        parentView.remove(child);
    });
}

function countryCodeToCountryName(country_code, addressType) {
    var countries = getGlobalCountries(addressType);
    var result = _.find(countries, function (c) {
        return c.countryCode == country_code;
    });

    return result ? result.countryName : country_code;
}

function countryNameToCountryCode(country_name, addressType) {
    var countries = getGlobalCountries(addressType);
    var result = _.find(countries, function (c) {
        return c.countryName == country_name;
    });

    return result ? result.countryCode : country_name;
}

function getGlobalCountries(addressType) {
    var countries;
    if (addressType == 'billing') {
        countries = Alloy.Globals.billingGlobalCountries;
    } else if (addressType == 'shipping') {
        countries = Alloy.Globals.shippingGlobalCountries;
    } else {
        countries = Alloy.Globals.customerGlobalCountries;
    }
    return countries;
}

function strikeThrough(label, text) {
    var attr = Ti.UI.createAttributedString({
        text: text,
        attributes: [{
            type: Ti.UI.ATTRIBUTE_STRIKETHROUGH_STYLE,
            value: Ti.UI.ATTRIBUTE_UNDERLINE_STYLE_SINGLE,
            range: [0, text.length]
        }]
    });
    label.setAttributedString(attr);
}

function zero(x) {
    return x ? x : 0;
}

function emailLogs(body, subject) {
    var deferred = new _.Deferred();
    var url = buildStorefrontURL('https', 'EAUtils-EmailConsoleLog');
    var xhr = Ti.Network.createHTTPClient({
        timeout: Alloy.CFG.storefront.timeout,
        validatesSecureCertificate: Alloy.CFG.storefront.validate_secure_cert
    });

    xhr.onload = function (eResp) {

        try {
            var response = JSON.parse(this.responseText);
            if (response.httpStatus == 200) {
                logger.log('request-success', 'emailed logs through server');
                deferred.resolve();
            } else {
                xhr.onerror(eResp);
            }
        } catch (ex) {
            xhr.onerror(eResp);
        }
    };

    xhr.onerror = function (eResp) {
        logger.error('emailLogs error!\n url: ' + url + '\n status: [' + xhr.status + ']\n response: [' + xhr.responseText + ']\n exception: [' + JSON.stringify(eResp, null, 2) + ']');
        deferred.reject();
    };

    logger.log('request', 'emailLogs POST ' + url);
    xhr.open('POST', url);
    var data = {
        log: body
    };

    if (subject) {
        data.subject = subject;
    }

    logger.secureLog('emailLogs sending ' + JSON.stringify(data, null, 2), 'request-response');
    xhr.send(data);

    return deferred.promise();
}

function sendErrorToServer(errorText) {
    var url = buildStorefrontURL('https', 'EAUtils-LogAndEmailError');
    var xhr = Ti.Network.createHTTPClient({
        timeout: Alloy.CFG.storefront.timeout,
        validatesSecureCertificate: Alloy.CFG.storefront.validate_secure_cert
    });

    xhr.onload = function () {
        logger.info('emailed error to admin');
    };

    xhr.onerror = function (eResp) {
        logger.error('sendErrorToServer error!\n url: ' + url + '\n status: [' + xhr.status + ']\n response: [' + xhr.responseText + ']\n exception: [' + JSON.stringify(eResp, null, 2) + ']');
    };
    logger.log('request', 'sendErrorToServer POST ' + url);
    xhr.open('POST', url);
    var data = {
        errorText: errorText
    };
    logger.secureLog('sendErrorToServer sending ' + JSON.stringify(data, null, 2), 'request-response');
    logger.error('Reporting Error:\n ' + errorText);
    xhr.send(data);

    analytics.fireExceptionEvent({
        description: errorText && errorText.length <= 100 ? errorText : errorText.substring(0, 100),
        fatal: false
    });
}

function sendSignatureToServer(sigImg, filename) {
    uploadFileToServer(sigImg, filename, 'signature').fail(function () {
        var img = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, filename);
        img.write(sigImg);
        logger.error('image ' + filename + ' saved to: ' + img.nativePath);
    });
}

function doProductSearch(options) {
    var deferred = new _.Deferred();
    Alloy.Router.showActivityIndicator(deferred);
    Alloy.Models.productSearch.fetch(options).always(function () {
        deferred.resolve();
    });
}

function getUIObjectType(UIObject) {
    if (UIObject.getApiName) {
        var UI_ObjectType = UIObject.getApiName().split('.');
        return UI_ObjectType[UI_ObjectType.length - 1];
    } else {
        return typeof UIObject;
    }
}

function showCustomerAddressAlert(changePage) {
    var deferred = new _.Deferred();
    var warningMessage = null;
    if (Alloy.Models.customerAddress.isModifyingCustomerAddress()) {
        warningMessage = _L('You are editing an address. Are you sure you want to discard the changes?');
    } else {
        warningMessage = _L('You are adding an address. Are you sure you want to discard the changes?');
    }
    Alloy.Dialog.showConfirmationDialog({
        messageString: warningMessage,
        titleString: _L('Discard Changes'),
        okFunction: function () {
            if (changePage) {
                Alloy.Models.customerAddress.setCurrentPage(null);
                Alloy.Models.customerAddress.setModifyingCurrentAddress(false);
            }
            deferred.resolve();
        },
        cancelFunction: function () {
            deferred.reject();
        }
    });
    return deferred.promise();
}

function addressVerification(fault, originalAddress, updateFunction, optionalData) {
    logger.info('AVS verifying address save results');
    var faultHandled = false;
    if (fault && fault.arguments && fault.arguments.statusCode == 'AddressVerificationError') {
        var response = JSON.parse(fault.arguments.statusMessage);
        switch (response.verificationLevel) {
            case 'Recommended':
            case 'InvalidAddress':
                createAVSPopOver(originalAddress, response, updateFunction, optionalData);
                faultHandled = true;
                break;
            default:
                logger.info('AVS invalid response.');
                break;
        }
    }
    return faultHandled;
}

function createAVSPopOver(originalAddress, addressVerificationResults, updateFunction, optionalData) {
    logger.info('creating AVS popover');

    Alloy.Dialog.showCustomDialog({
        controllerPath: 'components/avsPopover',
        cancelEvent: 'avsPopover:dismiss',
        continueEvent: 'avsPopover:continue',
        initOptions: {
            givenAddress: addressVerificationResults.givenAddress,
            pickListDisplay: addressVerificationResults.pickListDisplay
        },
        continueFunction: function (args) {
            if (args && args.selectedAddress) {
                if (args.selectedAddress.address1) {
                    var newAddress = _.extend(originalAddress, {
                        address1: args.selectedAddress.address1,
                        address2: args.selectedAddress.address2 ? args.selectedAddress.address2 : '',
                        city: args.selectedAddress.city,
                        state_code: args.selectedAddress.state,
                        postal_code: args.selectedAddress.postalCode
                    });
                    logger.info('AVS selected new address, calling update function');
                    updateFunction ? updateFunction(newAddress, optionalData) : null;
                } else {
                    logger.info('AVS continue with existing address, calling continue function');
                    updateFunction ? updateFunction(originalAddress, optionalData) : null;
                }
            }
        }
    });
}

function formatDate(date, time) {
    if (!time) {
        time = 'T23:59:59';
    }
    var d = date,
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) {
        month = '0' + month;
    }
    if (day.length < 2) {
        day = '0' + day;
    }
    return [year, month, day].join('-') + time;
}

function getVisibleViewFromContainerView(containerView) {
    var visibleView = _.find(containerView.getChildren(), function (view) {
        return view.getVisible() == true;
    });
    return visibleView;
}

function updateLocaleGlobalVariables(countrySelected) {
    var deferred = new _.Deferred();
    var appSettings = require('appSettings');
    var loadConfigurationPromise = new _.Deferred();

    var oldOcapiSite = Alloy.CFG.ocapi.site_url;
    var oldStorefrontSite = Alloy.CFG.storefront.site_url;
    var countryConfigurations = countryConfig[countrySelected];
    var failure = false;

    appSettings.setSetting('ocapi.site_url', countryConfigurations.ocapi.site_url);
    appSettings.setSetting('storefront.site_url', countryConfigurations.storefront.site_url);

    var addressform = require('config/address/addressConfig').local_address[countrySelected];

    if (Alloy.CFG.country_configuration && !Alloy.CFG.country_configuration[countrySelected]) {
        loadConfigurations().done(function () {
            if (!Alloy.CFG.country_configuration[countrySelected] || !Alloy.CFG.country_configuration[countrySelected].list_price_book) {
                loadConfigurationPromise.reject();
                Alloy.Dialog.showAlertDialog({
                    messageString: String.format(_L('Price book mapping missing from Endless Aisle Preferences for the value \'%s\'.'), countrySelected),
                    titleString: _L('Configuration Error')
                });
            } else {
                loadConfigurationPromise.resolve();
            }
        }).fail(function () {
            notify(_L('Unable to load the configurations from the server'), {
                preventAutoClose: true
            });
            loadConfigurationPromise.reject();
        });
    } else {
        loadConfigurationPromise.resolve();
    }

    loadConfigurationPromise.done(function () {
        if (addressform == null) {
            failure = true;
            Alloy.Dialog.showAlertDialog({
                messageString: String.format(_L('Please add the address configuration for the %s in addressConfig.js'), countryConfigurations.value),
                titleString: _L('Configuration Error')
            });
        }

        if (currencyConfig[countryConfigurations.appCurrency] === undefined) {
            failure = true;
            Alloy.Dialog.showAlertDialog({
                messageString: String.format(_L('Please add the currency configuration for the \'%s\' in currencyConfig'), countryConfigurations.appCurrency),
                titleString: _L('Configuration Error')
            });
        }

        if (failure) {
            appSettings.setSetting('ocapi.site_url', oldOcapiSite);
            appSettings.setSetting('storefront.site_url', oldStorefrontSite);
            deferred.reject();
        } else {
            appSettings.setSetting('addressform', addressform);
            appSettings.setSetting('appCurrency', countryConfigurations.appCurrency);

            appSettings.setSetting('country', countryConfigurations.value);

            deferred.resolve();
        }
    }).fail(function () {
        appSettings.setSetting('ocapi.site_url', oldOcapiSite);
        appSettings.setSetting('storefront.site_url', oldStorefrontSite);
        deferred.reject();
    });

    return deferred.promise();
}

function verifyAddressEditBeforeNavigation(navigate) {
    if (Alloy.Models.customer.isLoggedIn() && Alloy.Models.customerAddress.isCustomerAddressPage()) {
        logger.info('Customer is logged in and customer is adding/modifying address');
        showCustomerAddressAlert(true).done(function () {
            if (_.isFunction(navigate)) {
                navigate();
            }
        });
    } else {
        logger.info('Customer is not logged in');
        if (_.isFunction(navigate)) {
            navigate();
        }
    }
}

function buildRequestUrl(endPointName, params) {
    var url = endPointName;
    _.each(params, function (value, key) {
        if (url.indexOf('?') == -1) {
            url += '?' + key + '=' + value;
        } else {
            url += '&' + key + '=' + value;
        }
    });
    return url;
}

function isSymbolBasedLanguage() {
    var locale = Alloy.CFG.ocapi.default_locale;
    if (_s.startsWith(locale, 'ja') || _s.startsWith(locale, 'zh')) {
        return true;
    } else {
        return false;
    }
}

function isLatinBasedLanguage() {
    var locale = Alloy.CFG.ocapi.default_locale;
    if (_s.startsWith(locale, 'de') || _s.startsWith(locale, 'fr')) {
        return true;
    } else {
        return false;
    }
}

function appendURL(url, key, value) {
    if (url.indexOf(key + '=') < 0) {
        if (url.indexOf('?') > -1) {
            url += '&';
        } else {
            url += '?';
        }
        url += key + '=' + encodeURIComponent(value);
    }
    return url;
}

function fetchImagesForProducts(plis, table, hideButtons, order) {
    var orderCurrency;
    if (order) {
        orderCurrency = {
            currency: order.getCurrency()
        };
    }
    var products = Alloy.createCollection('product');
    var ids = [];
    var deferred = new _.Deferred();
    _.each(plis, function (pli) {
        ids.push(pli.getProductId());
    });
    products.setIds(ids);
    products.fetchModels(orderCurrency).done(function () {
        _.each(plis, function (pli, index) {
            var product = _.find(products.models, function (product) {
                return product.getId() == pli.getProductId();
            });
            var productItemRowUpdate = function () {
                table.getSections()[0].getRows()[index].update.apply(this, [product, pli, index < plis.length - 1, hideButtons, order ? order : '']);
                if (index == plis.length - 1) {
                    deferred.resolve();
                }
            };

            if (product) {
                product.ensureImagesLoaded('cart').done(function () {
                    productItemRowUpdate();
                });
            } else {
                productItemRowUpdate();
            }
        });
    }).fail(function () {
        deferred.reject();
    });
    return deferred.promise();
}

function getAddressNickname(city, stateCode) {
    return city + (stateCode ? ' ' + stateCode : '');
}

function getConsoleFile() {
    return Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'console.log');
}

function uploadFileToServer(blob, filename, filetype) {

    var deferred = new _.Deferred();
    var url = buildStorefrontURL('https', 'EAUtils-SaveFile?filename=' + filename + '&filetype=' + filetype);
    var xhr = Ti.Network.createHTTPClient({
        timeout: Alloy.CFG.storefront.timeout
    });

    xhr.onload = function (eResp) {
        try {
            var response = JSON.parse(this.responseText);
            if (response.httpStatus == 200) {
                logger.log('request-success', 'uploading ' + filename + ' loaded to ' + url);
                deferred.resolve();
            } else {
                xhr.onerror(eResp);
            }
        } catch (ex) {
            xhr.onerror(eResp);
        }
    };

    xhr.onerror = function (eResp) {
        logger.error('uploadFileToServer error!\n url: ' + url + '\n status: [' + xhr.status + ']\n response: [' + xhr.responseText + ']\n exception: [' + JSON.stringify(eResp, null, 2) + ']');
        deferred.reject();
    };

    logger.log('request', 'uploadFileToServer POST ' + url);
    xhr.open('POST', url);
    var data = {
        file: blob
    };
    xhr.send(data);

    return deferred.promise();
}

function getCurrencyConfiguration() {
    return {
        currencyFormat: currencyConfig[Alloy.CFG.appCurrency].currencyFormat,
        currencyLocale: currencyConfig[Alloy.CFG.appCurrency].currencyLocale,
        thousands: currencyConfig[Alloy.CFG.appCurrency].delimiters.thousands,
        decimal: currencyConfig[Alloy.CFG.appCurrency].delimiters.decimal,
        country: countryConfig[Alloy.CFG.countrySelected].value
    };
}

function uploadLogsToServer() {
    var dir = Ti.Filesystem.getFile(Ti.Filesystem.getApplicationDataDirectory(), 'logUploads');
    if (dir.isDirectory()) {
        var fileList = dir.getDirectoryListing();
        _.each(fileList, function (fName) {
            var cFile = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'logUploads/' + fName);

            if (cFile.isFile() && fName.split('.').pop() == 'log') {
                var blob = cFile.read();
                if (blob) {
                    uploadFileToServer(blob, fName, 'log').done(function () {
                        cFile.deleteFile();
                    });
                }
            }
        });
    }
}

function getAddressStringFromAddressDataOrderAndType(addressData, addressOrder, addressType) {
    var addressLabel = '';
    _.each(addressOrder, function (addressLine, index) {
        var label = '';
        _.each(addressLine, function (addressItem, addressItemIndex) {
            if (typeof addressItem === 'object') {
                if (addressItem.format && addressItem.field && addressData[addressItem.field]) {
                    label = label + ' ' + String.format(addressItem.format, addressData[addressItem.field]);
                }
            } else {
                if (addressItem === 'country_code') {
                    var countryCode = addressData[addressItem];
                    if (countryCode && typeof countryCode === 'string') {
                        countryCode = countryCode.toUpperCase();
                        if (countryCode.length == 2) {
                            label = label + ' ' + countryCodeToCountryName(countryCode, addressType);
                        } else {
                            label = label + ' ' + addressData[addressItem];
                        }
                    }
                } else {
                    label = addressData[addressItem] && addressData[addressItem] != 'null' ? label + ' ' + addressData[addressItem] : '';
                }
            }
        });
        if (label.trim() != '') {
            if (addressLabel != '') {
                addressLabel = addressLabel + '\n' + label.trim();
            } else {
                addressLabel = addressLabel + label.trim();
            }
        } else {
            label = '';
        }
    });
    return addressLabel;
}