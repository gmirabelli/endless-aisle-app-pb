

var logger = require('logging')('analytics', 'app/lib/analytics');

var analyticsTimer = null;
var eventCount = 0;
var analyticsModules = [];

Alloy.eventDispatcher.listenTo(Alloy.eventDispatcher, 'configurations:prelogin', function (event) {
    if (isAnalyticsEnabled()) {
        _.each(Alloy.CFG.analytics.modules, function (module) {
            logger.info('adding and starting analyticsModule ' + module);
            var analyticsModule = require(module);
            analyticsModules.push(analyticsModule);
            analyticsModule.startAnalytics(event);
        });
    }
});

Alloy.eventDispatcher.listenTo(Alloy.eventDispatcher, 'configurations:postlogin', function (event) {
    if (isAnalyticsEnabled()) {
        startTimer();
    }
});

Alloy.eventDispatcher.listenTo(Alloy.eventDispatcher, 'configurations:unload', function (event) {
    if (isAnalyticsEnabled()) {
        stopTimer();
        var dispatch_type = Alloy.CFG.analytics.dispatch_type;
        if (dispatch_type == 'on_logout' || dispatch_type == 'low_net_traffic') {
            dispatchEvents();
        }
        _.each(analyticsModules, function (analyticsModule) {
            logger.info('stopping analyticsModule ' + analyticsModule);
            analyticsModule.stopAnalytics(event);
        });
        analyticsModules = [];
    }
});

exports.fireAnalyticsEvent = fireAnalyticsEvent;
exports.fireUserEvent = fireUserEvent;
exports.fireScreenEvent = fireScreenEvent;
exports.fireTransactionEvent = fireTransactionEvent;
exports.fireTransactionItemEvent = fireTransactionItemEvent;
exports.fireExceptionEvent = fireExceptionEvent;

function isAnalyticsEnabled() {
    return Alloy.CFG.analytics.enabled;
}

function dispatchEvents() {
    var dispatch_type = Alloy.CFG.analytics.dispatch_type;
    if (isAnalyticsEnabled() && eventCount && (dispatch_type == 'low_net_traffic' || dispatch_type == 'on_logout')) {
        var time = new Date().getTime();
        _.each(analyticsModules, function (analyticsModule) {
            analyticsModule.dispatchEvents();
        });
        var totalTime = new Date().getTime() - time;
        logger.info('Dispatched ' + eventCount + ' analytics events for ' + analyticsModules.length + ' analytics modules in ' + totalTime + 'ms');
        eventCount = 0;
    }
}

function startTimer() {
    if (Alloy.CFG.analytics.dispatch_type == 'low_net_traffic') {
        analyticsTimer = setInterval(dispatchEvents, Alloy.CFG.analytics.event_dispatch_delay * 1000);
        logger.info('starting timer for \'low_net_traffic\' dispatchEvents id ' + analyticsTimer);
    }
}

function stopTimer() {
    if (analyticsTimer) {
        logger.info('clearing timer for \'low_net_traffic\' dispatchEvents id ' + analyticsTimer);
        clearInterval(analyticsTimer);
        analyticsTimer = null;
    }
}

function fireAnalyticsEvent(data) {
    if (isAnalyticsEnabled()) {
        logger.info('fireAnalyticsEvent ' + JSON.stringify(data, null, 4));
        _.each(analyticsModules, function (analyticsModule) {
            analyticsModule.fireAnalyticsEvent(data);
        });
        eventCount++;
    }
}

function fireUserEvent(data) {
    if (isAnalyticsEnabled()) {
        logger.info('fireUserEvent ' + JSON.stringify(data, null, 4));
        _.each(analyticsModules, function (analyticsModule) {
            analyticsModule.fireUserEvent(data);
        });
        eventCount++;
    }
}

function fireScreenEvent(data) {
    if (isAnalyticsEnabled()) {
        logger.info('fireScreenEvent ' + JSON.stringify(data, null, 4));
        _.each(analyticsModules, function (analyticsModule) {
            analyticsModule.fireScreenEvent(data);
        });
        eventCount++;
    }
}

function fireTransactionEvent(data) {
    if (isAnalyticsEnabled()) {
        logger.info('fireTransactionEvent ' + JSON.stringify(data, null, 4));
        _.each(analyticsModules, function (analyticsModule) {
            analyticsModule.fireTransactionEvent(data);
        });
        eventCount++;
    }
}

function fireTransactionItemEvent(data) {
    if (isAnalyticsEnabled()) {
        logger.info('fireTransactionItemEvent ' + JSON.stringify(data, null, 4));
        _.each(analyticsModules, function (analyticsModule) {
            analyticsModule.fireTransactionItemEvent(data);
        });
        eventCount++;
    }
}

function fireExceptionEvent(data) {
    if (isAnalyticsEnabled()) {
        logger.info('fireExceptionEvent ' + JSON.stringify(data, null, 4));
        _.each(analyticsModules, function (analyticsModule) {
            analyticsModule.fireExceptionEvent(data);
        });
        eventCount++;
    }
}