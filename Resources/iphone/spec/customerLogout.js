
require('behave').andSetup(this);
var helper = require('testhelper');
var baskethelper = require('spec/baskethelper');

exports.define = function () {
    describe('Customer Logout tests', function () {

        it.eventually('execute customer logout model tests', function (done) {
            var associateId = Alloy.CFG.modelTestsConfiguration.associateId;
            var associatePasscode = Alloy.CFG.modelTestsConfiguration.associatePasscode;
            baskethelper.loginAssociateWithCredentials(associateId, associatePasscode, false, function (associate) {
                expect(associate.isLoggedIn()).toBe(true);

                var productId = Alloy.CFG.modelTestsConfiguration.productId;
                var quantity = 1;
                baskethelper.newBasketAddProduct(productId, quantity, associateId, Alloy.CFG.modelTestsConfiguration.storeId, false, function (basket) {
                    var email = Alloy.CFG.modelTestsConfiguration.customerEmail;
                    baskethelper.loginCustomerWithEmail(email, basket, false, function (customer) {
                        helper.isTrue(customer.isLoggedIn());
                        customer.syncSavedProducts(basket, {
                            c_employee_id: associateId
                        }).done(function () {
                            baskethelper.logoutCustomer(customer, basket, associateId, function () {
                                baskethelper.newBasket(false, associateId, function (basket) {
                                    var email = Alloy.CFG.modelTestsConfiguration.customerEmail;
                                    baskethelper.loginCustomerWithEmail(email, basket, false, function (customer) {
                                        helper.isTrue(customer.isLoggedIn());

                                        helper.equals(customer.getSavedProducts()[0].getProductId(), Alloy.CFG.modelTestsConfiguration.productId);

                                        baskethelper.removeSavedProduct(customer, customer.getSavedProducts()[0]).done(function () {
                                            helper.equals(customer.getSavedProducts().length, 0);
                                            baskethelper.logoutCustomer(customer, basket, associateId, function () {});
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });
};