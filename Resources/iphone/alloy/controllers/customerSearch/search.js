var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'customerSearch/search';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.search = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.color.background.transparent, id: "search" });

  $.__views.search && $.addTopLevelView($.__views.search);
  $.__views.backdrop = Ti.UI.createView(
  { top: 0, left: 0, width: 1024, height: 768, opacity: 0.25, backgroundColor: Alloy.Styles.color.background.black, zIndex: 100, id: "backdrop" });

  $.__views.search.add($.__views.backdrop);
  $.__views.search_contents = Ti.UI.createView(
  { left: 94, right: 94, top: 76, layout: "vertical", height: 250, opacity: 0.95, backgroundColor: Alloy.Styles.color.background.white, zIndex: 101, id: "search_contents" });

  $.__views.search.add($.__views.search_contents);
  $.__views.customer_header_container = Ti.UI.createView(
  { layout: "horizontal", top: 50, left: 89, height: Ti.UI.SIZE, id: "customer_header_container" });

  $.__views.search_contents.add($.__views.customer_header_container);
  $.__views.customer_lookup_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.calloutCopyFont, textid: "_Customer_Look_up", id: "customer_lookup_label", accessibilityValue: "customer_lookup_label" });

  $.__views.customer_header_container.add($.__views.customer_lookup_label);
  $.__views.search_form_container = Ti.UI.createView(
  { height: Ti.UI.SIZE, layout: "horizontal", left: 89, id: "search_form_container" });

  $.__views.search_contents.add($.__views.search_form_container);
  $.__views.search_textfield = Ti.UI.createTextField(
  { borderColor: Alloy.Styles.color.border.dark, borderWidth: 1, color: Alloy.Styles.color.text.dark, backgroundColor: Alloy.Styles.color.background.white, backgroundImage: Alloy.Styles.magnifierImage, width: 480, top: 10, font: Alloy.Styles.dialogFieldFont, padding: { left: 50 }, height: 55, clearButtonMode: Ti.UI.INPUT_BUTTONMODE_ALWAYS, keyboardType: Ti.UI.KEYBOARD_TYPE_EMAIL, autocapitalization: Ti.UI.TEXT_AUTOCAPITALIZATION_NONE, returnKeyType: Ti.UI.RETURNKEY_SEARCH, autocorrect: false, hintText: L('_Enter_First_and_Last_Name_or_Email'), id: "search_textfield", accessibilityLabel: "search_textfield_drawer" });

  $.__views.search_form_container.add($.__views.search_textfield);
  $.__views.search_button = Ti.UI.createButton(
  { width: 156, right: 0, left: 15, top: 10, height: 55, padding: { left: 20 }, backgroundImage: Alloy.Styles.primaryButtonImage, font: Alloy.Styles.bigButtonFont, color: Alloy.Styles.buttons.primary.color, id: "search_button", titleid: "_Search", accessibilityValue: "search_button_drawer" });

  $.__views.search_form_container.add($.__views.search_button);
  $.__views.search_results_container = Ti.UI.createView(
  { layout: "vertical", left: 50, right: 50, top: 10, height: Ti.UI.FILL, id: "search_results_container", visible: false });

  $.__views.search_contents.add($.__views.search_results_container);
  $.__views.results_error = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.red, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.headlineFont, id: "results_error", accessibilityValue: "results_error" });

  $.__views.search_results_container.add($.__views.results_error);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var logger = require('logging')('customerSearch:search', getFullControllerPath($.__controllerPath));




  $.search_textfield.addEventListener('return', handlePerformSearch);

  $.search_button.addEventListener('click', handlePerformSearch);

  $.backdrop.addEventListener('click', dismiss);




  exports.init = init;
  exports.deinit = deinit;
  exports.focusSearchField = focusSearchField;
  exports.showErrorMessage = showErrorMessage;
  exports.hideErrorMessage = hideErrorMessage;









  function init() {
    logger.info('INIT called');
  }






  function deinit() {
    logger.info('DEINIT called, removing listeners');
    $.search_textfield.removeEventListener('return', handlePerformSearch);
    $.search_button.removeEventListener('click', handlePerformSearch);
    $.backdrop.removeEventListener('click', dismiss);
    $.stopListening();
    $.destroy();
  }





  function focusSearchField() {
    logger.info('focus search field');
    $.search_textfield.focus();
  }






  function showErrorMessage(text) {

    $.results_error.setText(text);
    $.search_results_container.setVisible(true);
  }





  function hideErrorMessage() {

    $.results_error.setText('');
    $.search_results_container.setVisible(false);
  }








  function handlePerformSearch() {
    logger.info('handle perform search');

    var search_text = $.search_textfield.getValue().trim();
    if (!search_text) {
      $.results_error.setText(_L('You must provide either a first/last name or an email for a customer search.'));
      $.search_results_container.setVisible(true);
      return;
    }
    Alloy.Router.navigateToCustomerSearchResult({
      customer_query: search_text });

    dismiss();
  }





  function dismiss() {
    $.trigger('customer_search:dismiss');
  }









  _.extend($, exports);
}

module.exports = Controller;