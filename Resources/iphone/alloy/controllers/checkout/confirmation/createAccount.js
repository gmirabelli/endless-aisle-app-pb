var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'checkout/confirmation/createAccount';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.create_account_window = Ti.UI.createScrollView(
  { backgroundColor: Alloy.Styles.color.background.transparent, id: "create_account_window" });

  $.__views.create_account_window && $.addTopLevelView($.__views.create_account_window);
  $.__views.backdrop = Ti.UI.createView(
  { top: 0, left: 0, width: 1024, height: 768, opacity: 0.55, backgroundColor: Alloy.Styles.color.background.black, id: "backdrop" });

  $.__views.create_account_window.add($.__views.backdrop);
  $.__views.create_account_container = Ti.UI.createView(
  { layout: "vertical", width: 675, left: 175, height: 414, top: 75, backgroundColor: Alloy.Styles.color.background.white, id: "create_account_container" });

  $.__views.create_account_window.add($.__views.create_account_container);
  $.__views.create_account_header = Ti.UI.createView(
  { layout: "horizontal", height: Ti.UI.SIZE, width: "100%", top: 13, id: "create_account_header" });

  $.__views.create_account_container.add($.__views.create_account_header);
  $.__views.skip_button = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, backgroundImage: Alloy.Styles.buttons.secondary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.secondary.backgroundDisabledImage, color: Alloy.Styles.buttons.secondary.color, disabledColor: Alloy.Styles.buttons.secondary.disabledColor, height: 35, width: 146, left: 15, titleid: "_Skip", id: "skip_button", accessibilityValue: "skip_button" });

  $.__views.create_account_header.add($.__views.skip_button);
  $.__views.create_account_header_label = Ti.UI.createLabel(
  { width: 200, height: Ti.UI.SIZE, color: Alloy.Styles.accentColor, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.headlineFont, left: 95, top: 3, textid: "_Create_Account", id: "create_account_header_label", accessibilityValue: "create_account_header_label" });

  $.__views.create_account_header.add($.__views.create_account_header_label);
  $.__views.create_button = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.buttons.primary.color, disabledColor: Alloy.Styles.buttons.primary.disabledColor, backgroundImage: Alloy.Styles.buttons.primary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.primary.backgroundDisabledImage, height: 35, width: 146, left: 55, titleid: "_Create", id: "create_button", accessibilityValue: "create_button" });

  $.__views.create_account_header.add($.__views.create_button);
  $.__views.create_account_details_container = Ti.UI.createView(
  { layout: "vertical", top: 15, id: "create_account_details_container" });

  $.__views.create_account_container.add($.__views.create_account_details_container);
  $.__views.create_account_question_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.headlineFont, textid: "_Would_you_like_to_create_a_new_account_", id: "create_account_question_label", accessibilityValue: "create_account_question_label" });

  $.__views.create_account_details_container.add($.__views.create_account_question_label);
  $.__views.first_name = Ti.UI.createTextField(
  { height: 55, width: 445, font: Alloy.Styles.textFieldFont, borderColor: Alloy.Styles.color.border.light, borderWidth: 1, backgroundColor: Alloy.Styles.color.background.white, autocapitalization: Ti.UI.TEXT_AUTOCAPITALIZATION_WORDS, autocorrect: false, padding: { left: 18 }, clearButtonMode: Ti.UI.INPUT_BUTTONMODE_ALWAYS, top: 15, hintText: L('_First_Name_'), id: "first_name", accessibilityLabel: "first_name" });

  $.__views.create_account_details_container.add($.__views.first_name);
  $.__views.last_name = Ti.UI.createTextField(
  { height: 55, width: 445, font: Alloy.Styles.textFieldFont, borderColor: Alloy.Styles.color.border.light, borderWidth: 1, backgroundColor: Alloy.Styles.color.background.white, autocapitalization: Ti.UI.TEXT_AUTOCAPITALIZATION_WORDS, autocorrect: false, padding: { left: 18 }, clearButtonMode: Ti.UI.INPUT_BUTTONMODE_ALWAYS, top: 10, hintText: L('_Last_Name_'), id: "last_name", accessibilityLabel: "last_name" });

  $.__views.create_account_details_container.add($.__views.last_name);
  $.__views.email = Ti.UI.createTextField(
  { height: 55, width: 445, font: Alloy.Styles.textFieldFont, borderColor: Alloy.Styles.color.border.light, borderWidth: 1, backgroundColor: Alloy.Styles.color.background.white, autocapitalization: Ti.UI.TEXT_AUTOCAPITALIZATION_NONE, autocorrect: false, padding: { left: 18 }, clearButtonMode: Ti.UI.INPUT_BUTTONMODE_ALWAYS, top: 10, keyboardType: Ti.UI.KEYBOARD_TYPE_EMAIL, hintText: L('_Email_Address_'), id: "email", accessibilityLabel: "email" });

  $.__views.create_account_details_container.add($.__views.email);
  $.__views.save_address_container = Ti.UI.createView(
  { layout: "horizontal", width: 275, height: Ti.UI.SIZE, left: 115, top: 10, id: "save_address_container" });

  $.__views.create_account_details_container.add($.__views.save_address_container);
  $.__views.save_address_switch = Ti.UI.createSwitch(
  { width: 85, value: false, id: "save_address_switch", accessibilityLabel: "login_customer_switch" });

  $.__views.save_address_container.add($.__views.save_address_switch);
  $.__views.save_address_label = Ti.UI.createLabel(
  { width: 175, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.switchLabelFont, left: 0, textid: "_Save_Shipping_Address", id: "save_address_label", accessibilityValue: "save_address_label" });

  $.__views.save_address_container.add($.__views.save_address_label);
  $.__views.error_message = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.red, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.calloutCopyFont, visible: false, top: 20, id: "error_message", accessibilityValue: "error_message" });

  $.__views.create_account_details_container.add($.__views.error_message);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var showError = require('EAUtils').showError;
  var clearError = require('EAUtils').clearError;
  var showErrorLabelOnly = require('EAUtils').showErrorLabelOnly;
  var EAUtils = require('EAUtils');
  var logger = require('logging')('checkout:confirmation:createAccount', getFullControllerPath($.__controllerPath));
  var email_regex = new RegExp(Alloy.CFG.regexes.email, 'i');
  var address;
  var originalEmailBorderColor = $.email.getBorderColor();
  var originalEmailBackgroundColor = $.email.getBackgroundColor();




  $.skip_button.addEventListener('click', dismiss);

  $.create_button.addEventListener('click', onCreateClick);

  $.email.addEventListener('change', validate);
  $.email.addEventListener('blur', onBlur);

  $.first_name.addEventListener('change', validate);
  $.last_name.addEventListener('change', validate);

  $.backdrop.addEventListener('click', dismiss);




  exports.deinit = deinit;
  exports.init = init;
  exports.showErrorMessage = showErrorMessage;
  exports.focusFirstField = focusFirstField;
  exports.dismiss = dismiss;









  function init(args) {
    logger.info('INIT called');
    clearError($.email, $.error_message, originalEmailBorderColor, originalEmailBackgroundColor, false);
    if (args && args.order) {
      var basket = Alloy.createModel('baskets', args.order.toJSON());
      address = basket.getShippingAddress();
      if (address) {
        $.first_name.setValue(address.getFirstName());
        $.last_name.setValue(address.getLastName());
        $.email.setValue(basket.getCustomerEmail());
      }
      validate();
    } else {
      $.skip_button.setTitle(_L('Cancel'));
      if (EAUtils.isSymbolBasedLanguage()) {
        $.save_address_container.setWidth(400);
        $.save_address_label.setWidth(300);
        $.create_account_header_label.setLeft(70);
        $.create_button.setLeft(80);
      } else {
        $.save_address_container.setWidth(300);
        $.save_address_label.setWidth(210);
      }
      $.save_address_label.setText(_L('Login and Shop For Customer'));
      $.save_address_label.setAccessibilityLabel('login_customer_label');
      $.save_address_container.setTop(20);
      $.create_button.setEnabled(false);
      $.save_address_switch.setAccessibilityLabel('login_customer_switch');
    }
    $.toolbar = Alloy.createController('components/nextPreviousToolbar');
    $.toolbar.setTextFields([$.first_name, $.last_name, $.email]);
  }






  function deinit() {
    logger.info('DEINIT called');
    $.skip_button.removeEventListener('click', dismiss);
    $.create_button.removeEventListener('click', onCreateClick);
    $.email.removeEventListener('change', validate);
    $.email.removeEventListener('blur', onBlur);
    $.first_name.removeEventListener('change', validate);
    $.last_name.removeEventListener('change', validate);
    $.backdrop.removeEventListener('click', dismiss);
    $.toolbar && $.toolbar.deinit();
    $.stopListening();
    $.destroy();
  }










  function showErrorMessage(error) {
    var errorMessage;
    if (!error) {
      errorMessage = _L('Error creating account');
    } else {
      switch (Object.prototype.toString.apply(error)) {
        case '[object String]':
          errorMessage = error;
          break;
        case '[object Object]':
          if (error.type === 'InvalidEmailException') {
            showError($.email, $.error_message, _L('Please enter a valid email address.'), false);
            return;
          } else if (error.type) {
            errorMessage = _L(error.type);
          } else if (error.details) {
            errorMessage = error.details.StatusMsg;
          } else {
            errorMessage = error.message;
          }
          break;}

    }
    showErrorLabelOnly($.error_message, errorMessage);
  }






  function focusFirstField() {
    $.first_name.focus();
  }






  function dismiss() {
    $.trigger('createAccount:dismiss');
  }






  function validateEmailAddress() {
    var email = $.email.getValue().trim();
    if (email_regex.test(email)) {
      return true;
    } else {
      return false;
    }
  }






  function validate() {
    logger.info('validate called');
    var firstName = $.first_name.getValue().trim();
    var lastName = $.last_name.getValue().trim();
    var emailValid = validateEmailAddress();
    $.create_button.setEnabled(emailValid && firstName != '' && lastName != '');
    return emailValid;
  }









  function onCreateClick() {
    var valid = validate();
    $.first_name.blur();
    $.last_name.blur();
    $.email.blur();

    if (valid) {
      var customer_info = {
        customer: {
          first_name: $.first_name.getValue(),
          last_name: $.last_name.getValue(),
          email: $.email.getValue(),
          login: $.email.getValue() } };


      $.trigger('createAccount:create', customer_info, address, $.save_address_switch.getValue());
    }
  }






  function onBlur() {
    if (validateEmailAddress()) {
      clearError($.email, $.error_message, originalEmailBorderColor, originalEmailBackgroundColor, false);
    } else {
      showError($.email, $.error_message, _L('Please enter a valid email address.'), false);
    }
  }









  _.extend($, exports);
}

module.exports = Controller;