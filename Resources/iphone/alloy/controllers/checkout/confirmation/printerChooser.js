var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'checkout/confirmation/printerChooser';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};




  $.printerList = Alloy.createCollection('printer');


  $.__views.printer_chooser_container = Ti.UI.createView(
  { layout: "vertical", top: 10, left: 0, height: Ti.UI.SIZE, width: Ti.UI.SIZE, id: "printer_chooser_container" });

  $.__views.printer_chooser_container && $.addTopLevelView($.__views.printer_chooser_container);
  $.__views.printer_label_container = Ti.UI.createView(
  { layout: "horizontal", left: 0, height: Ti.UI.SIZE, width: Ti.UI.SIZE, id: "printer_label_container" });

  $.__views.printer_chooser_container.add($.__views.printer_label_container);
  $.__views.print_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailTextFont, left: 0, textid: "_Printer_", id: "print_label" });

  $.__views.printer_label_container.add($.__views.print_label);
  $.__views.chosen_printer_label = Ti.UI.createLabel(
  { width: Ti.UI.FILL, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailTextFont, left: 10, id: "chosen_printer_label" });

  $.__views.printer_label_container.add($.__views.chosen_printer_label);
  $.__views.printer_table = Ti.UI.createTableView(
  { top: 10, separatorStyle: "transparent", borderWidth: 1, height: 150, width: Ti.UI.FILL, borderColor: Alloy.Styles.color.border.dark, id: "printer_table" });

  $.__views.printer_chooser_container.add($.__views.printer_table);
  var __alloyId64 = Alloy.Collections['$.printerList'] || $.printerList;function __alloyId65(e) {if (e && e.fromAdapter) {return;}var opts = __alloyId65.opts || {};var models = __alloyId64.models;var len = models.length;var rows = [];for (var i = 0; i < len; i++) {var __alloyId61 = models[i];__alloyId61.__transform = _.isFunction(__alloyId61.transform) ? __alloyId61.transform() : __alloyId61.toJSON();var __alloyId62 = Ti.UI.createTableViewRow(
      { selectionStyle: Ti.UI.iOS.TableViewCellSelectionStyle.NONE, width: "100%", layout: "vertical" });

      rows.push(__alloyId62);
      var __alloyId63 = Ti.UI.createLabel(
      { width: Ti.UI.FILL, height: 42, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 5, font: Alloy.Styles.detailValueFont, text: __alloyId61.__transform.ip });

      __alloyId62.add(__alloyId63);
    }$.__views.printer_table.setData(rows);};__alloyId64.on('fetch destroy change add remove reset', __alloyId65);exports.destroy = function () {__alloyId64 && __alloyId64.off('fetch destroy change add remove reset', __alloyId65);};




  _.extend($, $.__views);










  var logger = require('logging')('checkout:confirmation:printerChooser', getFullControllerPath($.__controllerPath));
  var printerCollection = new Backbone.Collection();
  var printerAvailability = Alloy.CFG.printer_availability;





  $.listenTo(Alloy.eventDispatcher, 'printerlist:change', onPrinterListChange);




  $.printer_table.addEventListener('click', onPrinterTableClick);




  exports.init = init;
  exports.deinit = deinit;









  function init() {
    if (printerAvailability) {
      logger.info('INIT called');
      printerCollection = new Backbone.Collection();
      if (Alloy.last_selected_printer) {
        $.chosen_printer_label.setText(Alloy.last_selected_printer);
      } else {
        $.chosen_printer_label.setText(_L('<select printer>'));
      }
      $.printerList.reset([]);

      Alloy.eventDispatcher.trigger('printerlist:startdiscovery', {});
    }
  }






  function deinit() {
    logger.info('DEINIT called');
    $.stopListening(Alloy.eventDispatcher, 'printerlist:change', onPrinterListChange);
    $.printer_table.removeEventListener('click', onPrinterTableClick);
    $.stopListening();
    $.destroy();
  }










  function onPrinterListChange(event) {
    printerCollection.reset([]);

    _.each(event.printerList, function (string) {
      var model = Alloy.createModel('printer');
      model.set('ip', string);
      printerCollection.add(model);
    });


    $.printerList.once('reset', function () {
      var rows = $.printer_table.getSections()[0].getRows();
      var row = _.find(rows, function (row) {
        return row.getChildren()[0].text == Alloy.last_selected_printer;
      });

      if (row) {
        row.setBackgroundColor(Alloy.Styles.color.background.medium);
        $.trigger('printer_chosen', {
          enabled: true });

      }
    });

    $.printerList.reset(printerCollection.models);
  }







  function onPrinterTableClick(event) {

    if (Alloy.last_selected_printer === printerCollection.at(event.index).get('ip')) {
      Alloy.last_selected_printer = null;
      _.each($.printer_table.getSections()[0].getRows(), function (row) {
        row.setBackgroundColor(Alloy.Styles.color.background.white);
      });
      $.chosen_printer_label.setText(_L('<select printer>'));
      $.trigger('printer_chosen', {
        enabled: false });

    } else {

      Alloy.last_selected_printer = printerCollection.at(event.index).get('ip');
      $.chosen_printer_label.setText(Alloy.last_selected_printer);
      _.each($.printer_table.getSections()[0].getRows(), function (row) {
        row.setBackgroundColor(Alloy.Styles.color.background.white);
      });
      $.printer_table.getSections()[0].getRows()[event.index].setBackgroundColor(Alloy.Styles.color.background.medium);
      $.trigger('printer_chosen', {
        enabled: true });

      Alloy.eventDispatcher.trigger('printer:select', {
        selectedPrinter: Alloy.last_selected_printer });

    }
  }









  _.extend($, exports);
}

module.exports = Controller;