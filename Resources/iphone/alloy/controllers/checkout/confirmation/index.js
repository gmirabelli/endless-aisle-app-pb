var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
		delete obj[key];
	}
	return arg;
}

function Controller() {

	require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'checkout/confirmation/index';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};







	$.__views.confirmation_container = Ti.UI.createView(
	{ layout: "vertical", bottom: 0, height: "100%", id: "confirmation_container" });

	$.__views.confirmation_container && $.addTopLevelView($.__views.confirmation_container);
	$.__views.order_header = Alloy.createController('components/orders/orderHeader', { id: "order_header", __parentSymbol: $.__views.confirmation_container });
	$.__views.order_header.setParent($.__views.confirmation_container);
	$.__views.separator = Ti.UI.createView(
	{ top: 33, width: "100%", height: 2, backgroundColor: Alloy.Styles.color.background.medium, id: "separator" });

	$.__views.confirmation_container.add($.__views.separator);
	$.__views.order_product_details = Alloy.createController('components/orders/orderProductDetails', { id: "order_product_details", __parentSymbol: $.__views.confirmation_container });
	$.__views.order_product_details.setParent($.__views.confirmation_container);
	exports.destroy = function () {};




	_.extend($, $.__views);










	var logger = require('logging')('checkout:confirmation:index', getFullControllerPath($.__controllerPath));




	moment.locale(Alloy.CFG.languageSelected);
	$.listenTo($.order_header, 'orderAbandoned', function () {
		$.trigger('orderAbandoned');
	});



	exports.render = render;
	exports.deinit = deinit;









	function render(order) {
		logger.secureLog('Basket: ' + JSON.stringify(order.toJSON()));
		$.order_header.render(order, true);
		order.justCreated = true;
		$.order_product_details.render(order);
	}






	function deinit() {
		logger.info('DEINIT called');
		$.stopListening();
		$.order_header.deinit();
		$.order_product_details.deinit();
		$.destroy();
	}









	_.extend($, exports);
}

module.exports = Controller;