var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'checkout/payments/noPaymentTerminal';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.no_payment_terminal_window = Ti.UI.createView(
  { left: 0, top: 0, width: 1024, height: 768, backgroundColor: Alloy.Styles.color.background.transparent, id: "no_payment_terminal_window" });

  $.__views.no_payment_terminal_window && $.addTopLevelView($.__views.no_payment_terminal_window);
  $.__views.backdrop = Ti.UI.createView(
  { top: 0, left: 0, width: 1024, height: 768, opacity: 0.55, backgroundColor: Alloy.Styles.color.background.black, id: "backdrop" });

  $.__views.no_payment_terminal_window.add($.__views.backdrop);
  $.__views.no_payment_terminal_container = Ti.UI.createView(
  { layout: "vertical", left: 112, top: 84, width: 800, height: 600, backgroundColor: Alloy.Styles.color.background.white, id: "no_payment_terminal_container" });

  $.__views.no_payment_terminal_window.add($.__views.no_payment_terminal_container);
  $.__views.title = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.accentColor, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.titleFont, top: 20, textid: "_No_Connection_To_Payment_Device", id: "title", accessibilityValue: "no_payment_terminal_title" });

  $.__views.no_payment_terminal_container.add($.__views.title);
  $.__views.message = Ti.UI.createLabel(
  { width: 740, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.appFont, top: 20, id: "message", accessibilityValue: "no_payment_terminal_message" });

  $.__views.no_payment_terminal_container.add($.__views.message);
  $.__views.image = Ti.UI.createImageView(
  { top: 20, height: 363, id: "image", accessibilityValue: "no_payment_terminal_image" });

  $.__views.no_payment_terminal_container.add($.__views.image);
  $.__views.button_container = Ti.UI.createView(
  { layout: "horizontal", width: Ti.UI.SIZE, height: Ti.UI.SIZE, bottom: 20, id: "button_container" });

  $.__views.no_payment_terminal_container.add($.__views.button_container);
  $.__views.test_connection_button = Ti.UI.createButton(
  { backgroundImage: Alloy.Styles.primaryButtonImage, color: Alloy.Styles.buttons.primary.color, width: 150, height: 40, top: 10, left: 15, titleid: "_Test_Payment_Device_Connection", id: "test_connection_button", accessibilityValue: "no_payment_terminal_test_conn_button" });

  $.__views.button_container.add($.__views.test_connection_button);
  $.__views.ok_button = Ti.UI.createButton(
  { backgroundImage: Alloy.Styles.primaryButtonImage, color: Alloy.Styles.buttons.primary.color, width: 150, height: 40, top: 10, left: 15, titleid: "_OK", id: "ok_button", accessibilityValue: "no_payment_terminal_ok_button" });

  $.__views.button_container.add($.__views.ok_button);
  if (true && Alloy.CFG.devices.bluetoothDevicePicker) {
    $.__views.bluetoothDevicePicker = Ti.UI.createButton(
    function () {
      var o = {};
      if (Alloy.CFG.devices.bluetoothDevicePicker) Alloy.deepExtend(true, o, { backgroundImage: Alloy.Styles.primaryButtonImage, color: Alloy.Styles.buttons.primary.color, width: 150, height: 40, top: 10, left: 15, titleid: "_Scan_for_Devices" });
      Alloy.deepExtend(true, o, { id: "bluetoothDevicePicker", accessibilityValue: "bluetooth_button" });
      return o;
    }());

    $.__views.button_container.add($.__views.bluetoothDevicePicker);
  }
  exports.destroy = function () {};




  _.extend($, $.__views);










  var paymentTerminal = require(Alloy.CFG.devices.payment_terminal_module);
  var logger = require('logging')('checkout:payments:noPaymentTerminal', getFullControllerPath($.__controllerPath));
  var bluetoothDevicePickerModule = undefined;


  var latinButtonTextLength = 16;




  $.ok_button.addEventListener('click', dismiss);

  $.test_connection_button.addEventListener('click', verifyDeviceConnection);




  exports.deinit = deinit;
  exports.init = init;
  exports.updateDeviceStatus = updateDeviceStatus;









  function init() {
    logger.info('init called');
    $.message.setText(paymentTerminal.getNoDeviceConnectionMessage());
    $.image.setImage(paymentTerminal.getNoDeviceConnectionImage());
  }






  function deinit() {
    logger.info('deinit called');
    $.ok_button.removeEventListener('click', dismiss);
    $.test_connection_button.removeEventListener('click', verifyDeviceConnection);
    if (Alloy.CFG.devices.bluetoothDevicePicker) {
      $.bluetoothDevicePicker.removeEventListener('click', showBluetoothDevicePicker);
      bluetoothDevicePickerModule.removeEventListener('deviceConnected', dismiss);
    }
    $.stopListening();
    $.destroy();
  }










  function updateDeviceStatus(connected) {
    if (connected) {
      dismiss();
    }
  }









  function dismiss() {
    $.trigger('noPaymentTerminal:dismiss', {});
  }






  function verifyDeviceConnection() {
    var message = '';
    var title = _L('Payment Terminal');

    var deferred = new _.Deferred();
    Alloy.Router.showActivityIndicator(deferred);

    logger.info('checking payment device');
    if (paymentTerminal.verifyDeviceConnection()) {
      message = _L('Connection to payment device was successful.');
    } else {
      message = _L('Unable to connect to payment device.');
      title = _L('Error');
    }

    deferred.resolve();

    Alloy.Dialog.showConfirmationDialog({
      messageString: message,
      titleString: title,
      hideCancel: true });

  }






  function showBluetoothDevicePicker() {
    bluetoothDevicePickerModule.showBluetoothDevicePicker();
  }




  if (Alloy.CFG.devices.bluetoothDevicePicker) {
    bluetoothDevicePickerModule = require('com.demandware.ios.bluetoothDevicePicker');
    bluetoothDevicePickerModule.addEventListener('deviceConnected', dismiss);
    $.bluetoothDevicePicker.addEventListener('click', showBluetoothDevicePicker);
  }

  if ($.test_connection_button.getTitle().length > latinButtonTextLength) {
    $.test_connection_button.setWidth(180);
    $.ok_button.setWidth(180);
  }









  _.extend($, exports);
}

module.exports = Controller;