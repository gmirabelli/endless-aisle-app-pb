var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'checkout/payments/promptSignature';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.contents = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.color.background.white, height: Ti.UI.SIZE, width: Ti.UI.SIZE, layout: "vertical", id: "contents" });

  $.__views.contents && $.addTopLevelView($.__views.contents);
  $.__views.signature_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.accentColor, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.calloutFont, top: 20, textid: "_Signature", id: "signature_label", accessibilityValue: "signature_label" });

  $.__views.contents.add($.__views.signature_label);
  $.__views.signature_explanation_label = Ti.UI.createLabel(
  { width: 615, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.textFieldFont, top: 36, textid: "_Please_provide_your_signature_to_complete_your_transaction", id: "signature_explanation_label", accessibilityValue: "signature_explanation_label" });

  $.__views.contents.add($.__views.signature_explanation_label);
  $.__views.signature_view = Ti.UI.createView(
  { height: 150, width: 615, id: "signature_view" });

  $.__views.contents.add($.__views.signature_view);
  $.__views.signature_line = Ti.UI.createView(
  { top: 0, width: 615, height: 2, backgroundColor: Alloy.Styles.tabs.backgroundColorDisabled, id: "signature_line" });

  $.__views.contents.add($.__views.signature_line);
  $.__views.signature_button_container = Ti.UI.createView(
  { layout: "horizontal", height: Ti.UI.SIZE, width: Ti.UI.SIZE, top: 40, bottom: 20, id: "signature_button_container" });

  $.__views.contents.add($.__views.signature_button_container);
  $.__views.clear_button = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, backgroundImage: Alloy.Styles.buttons.secondary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.secondary.backgroundDisabledImage, color: Alloy.Styles.buttons.secondary.color, disabledColor: Alloy.Styles.buttons.secondary.disabledColor, height: 40, width: 200, left: 0, titleid: "_Clear", id: "clear_button", accessibilityValue: "clear_button" });

  $.__views.signature_button_container.add($.__views.clear_button);
  $.__views.accept_button = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.buttons.primary.color, disabledColor: Alloy.Styles.buttons.primary.disabledColor, backgroundImage: Alloy.Styles.buttons.primary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.primary.backgroundDisabledImage, height: 40, width: 200, left: 20, titleid: "_Accept", id: "accept_button", accessibilityValue: "accept_button" });

  $.__views.signature_button_container.add($.__views.accept_button);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var logger = require('logging')('checkout:payments:promptSignature', getFullControllerPath($.__controllerPath));

  var args = arguments[0] || {};

  var paintView = null;





  $.accept_button.addEventListener('click', onAcceptClick);


  $.clear_button.addEventListener('click', onClearClick);




  exports.init = init;
  exports.deinit = deinit;









  function init(args) {
    logger.info('INIT called');
    var Paint = require('ti.paint');
    paintView = Paint.createPaintView({
      top: 0,
      right: 0,
      bottom: 0,
      left: 0,
      strokeColor: Alloy.Styles.color.text.black,
      strokeAlpha: 255,
      strokeWidth: 3,
      eraseMode: false });


    $.signature_view.add(paintView);
    paintView.clear();
  }






  function deinit() {
    logger.info('DEINIT called');
    removeAllChildren($.signature_view);
    paintView = null;
    $.accept_button.removeEventListener('click', onAcceptClick);
    $.clear_button.removeEventListener('click', onClearClick);
    $.stopListening();
    $.destroy();
  }









  function onAcceptClick() {
    var sigImg = paintView.toImage();
    $.trigger('promptSignature:accept_signature', {
      image: sigImg });

  }






  function onClearClick() {
    paintView.clear();
  }









  _.extend($, exports);
}

module.exports = Controller;