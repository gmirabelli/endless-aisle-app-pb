var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
		delete obj[key];
	}
	return arg;
}

function Controller() {

	require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'checkout/payments/gcBalanceDetails';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};







	$.__views.payment_failure_window = Ti.UI.createView(
	{ backgroundColor: Alloy.Styles.color.background.transparent, id: "payment_failure_window" });

	$.__views.payment_failure_window && $.addTopLevelView($.__views.payment_failure_window);
	$.__views.backdrop = Ti.UI.createView(
	{ top: 0, left: 0, width: 1024, height: 768, opacity: 0.55, backgroundColor: Alloy.Styles.color.background.black, id: "backdrop" });

	$.__views.payment_failure_window.add($.__views.backdrop);
	$.__views.payment_failure_container = Ti.UI.createView(
	{ layout: "vertical", width: 450, left: 287, height: 300, top: 94, backgroundColor: Alloy.Styles.color.background.white, id: "payment_failure_container" });

	$.__views.payment_failure_window.add($.__views.payment_failure_container);
	$.__views.title = Ti.UI.createLabel(
	{ width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.accentColor, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.titleFont, top: 20, textid: "_Payment_Status", id: "title", accessibilityValue: "balance_title" });

	$.__views.payment_failure_container.add($.__views.title);
	$.__views.message = Ti.UI.createLabel(
	{ width: 400, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.appFont, top: 20, id: "message", accessibilityValue: "balance_message" });

	$.__views.payment_failure_container.add($.__views.message);
	$.__views.current_balance_container = Ti.UI.createView(
	{ layout: "horizontal", left: 45, height: Ti.UI.SIZE, top: 20, id: "current_balance_container" });

	$.__views.payment_failure_container.add($.__views.current_balance_container);
	$.__views.current_balance_label = Ti.UI.createLabel(
	{ width: "60%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailValueFont, textid: "_Gift_card_balance_", id: "current_balance_label", accessibilityValue: "current_balance_label" });

	$.__views.current_balance_container.add($.__views.current_balance_label);
	$.__views.current_balance_value = Ti.UI.createLabel(
	{ width: "30%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT, font: Alloy.Styles.detailValueFont, id: "current_balance_value", accessibilityValue: "current_balance_value" });

	$.__views.current_balance_container.add($.__views.current_balance_value);
	$.__views.amount_applied_container = Ti.UI.createView(
	{ layout: "horizontal", left: 45, height: Ti.UI.SIZE, top: 10, id: "amount_applied_container" });

	$.__views.payment_failure_container.add($.__views.amount_applied_container);
	$.__views.amount_applied_label = Ti.UI.createLabel(
	{ width: "60%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailValueFont, textid: "_Amount_to_be_applied_to_order_", id: "amount_applied_label", accessibilityValue: "amount_applied_label" });

	$.__views.amount_applied_container.add($.__views.amount_applied_label);
	$.__views.amount_applied_value = Ti.UI.createLabel(
	{ width: "30%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT, font: Alloy.Styles.detailValueFont, id: "amount_applied_value", accessibilityValue: "amount_applied_value" });

	$.__views.amount_applied_container.add($.__views.amount_applied_value);
	$.__views.remaining_balance_container = Ti.UI.createView(
	{ layout: "horizontal", left: 45, height: Ti.UI.SIZE, top: 10, id: "remaining_balance_container" });

	$.__views.payment_failure_container.add($.__views.remaining_balance_container);
	$.__views.remaining_balance_label = Ti.UI.createLabel(
	{ width: "60%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailValueFont, textid: "_Remaining_gift_card_balance_", id: "remaining_balance_label", accessibilityValue: "remaining_balance_label" });

	$.__views.remaining_balance_container.add($.__views.remaining_balance_label);
	$.__views.remaining_balance_value = Ti.UI.createLabel(
	{ width: "30%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT, font: Alloy.Styles.detailValueFont, id: "remaining_balance_value", accessibilityValue: "remaining_balance_value" });

	$.__views.remaining_balance_container.add($.__views.remaining_balance_value);
	$.__views.button_container = Ti.UI.createView(
	{ top: 30, layout: "horizontal", left: 80, id: "button_container" });

	$.__views.payment_failure_container.add($.__views.button_container);
	$.__views.cancel_button = Ti.UI.createButton(
	{ backgroundImage: Alloy.Styles.secondaryButtonImage, color: Alloy.Styles.buttons.secondary.color, width: 150, height: 40, id: "cancel_button", accessibilityValue: "balance_cancel_button", titleid: "_Cancel" });

	$.__views.button_container.add($.__views.cancel_button);
	$.__views.ok_button = Ti.UI.createButton(
	{ backgroundImage: Alloy.Styles.buttons.primary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.primary.backgroundDisabledImage, color: Alloy.Styles.buttons.primary.color, width: 150, height: 40, left: 15, id: "ok_button", accessibilityValue: "balance_ok_button", titleid: "_Apply" });

	$.__views.button_container.add($.__views.ok_button);
	exports.destroy = function () {};




	_.extend($, $.__views);










	var toCurrency = require('EAUtils').toCurrency;
	var logger = require('logging')('checkout:payments:gcBalanceDetails', getFullControllerPath($.__controllerPath));




	$.ok_button.addEventListener('click', onOKClick);

	$.cancel_button.addEventListener('click', dismiss);




	exports.init = init;
	exports.deinit = deinit;











	function init(details) {
		logger.info('init');
		$.message.setText(details.message);
		$.current_balance_value.setText(toCurrency(details.currentBalance));
		$.amount_applied_value.setText(toCurrency(details.toApply));
		$.remaining_balance_value.setText(toCurrency(details.remainingBalance));

		$.ok_button.setEnabled(details.currentBalance != 0);
	}






	function deinit() {
		$.ok_button.removeEventListener('click', onOKClick);
		$.cancel_button.removeEventListener('click', dismiss);
		$.stopListening();
		$.destroy();
	}









	function dismiss() {
		$.trigger('gc_balance:dismiss');
	}






	function onOKClick() {
		$.trigger('gc_balance:continue');
	}









	_.extend($, exports);
}

module.exports = Controller;