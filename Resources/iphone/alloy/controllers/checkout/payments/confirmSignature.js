var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'checkout/payments/confirmSignature';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.confirm_signature_window = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.color.background.transparent, id: "confirm_signature_window" });

  $.__views.confirm_signature_window && $.addTopLevelView($.__views.confirm_signature_window);
  $.__views.backdrop = Ti.UI.createView(
  { top: 0, left: 0, width: 1024, height: 768, id: "backdrop" });

  $.__views.confirm_signature_window.add($.__views.backdrop);
  $.__views.signature_container = Ti.UI.createView(
  { layout: "vertical", width: Ti.UI.SIZE, height: Ti.UI.SIZE, backgroundColor: Alloy.Styles.color.background.white, id: "signature_container" });

  $.__views.confirm_signature_window.add($.__views.signature_container);
  $.__views.padding_container = Ti.UI.createView(
  { layout: "vertical", width: Ti.UI.SIZE, height: Ti.UI.SIZE, left: 50, right: 50, id: "padding_container" });

  $.__views.signature_container.add($.__views.padding_container);
  $.__views.signature_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.accentColor, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.calloutFont, top: 20, textid: "_Approve_Signature", id: "signature_label", accessibilityValue: "signature_label" });

  $.__views.padding_container.add($.__views.signature_label);
  $.__views.signature_explanation_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.textFieldFont, top: 36, textid: "_Please_approve_this_signature_to_complete_the_transaction", id: "signature_explanation_label", accessibilityValue: "signature_explanation_label" });

  $.__views.padding_container.add($.__views.signature_explanation_label);
  $.__views.signature_view = Ti.UI.createImageView(
  { top: 20, height: 150, width: Ti.UI.SIZE, borderWidth: 1, borderColor: Alloy.Styles.color.border.darker, id: "signature_view", accessibilityValue: "signature_view" });

  $.__views.padding_container.add($.__views.signature_view);
  $.__views.signature_button_container = Ti.UI.createView(
  { layout: "horizontal", height: Ti.UI.SIZE, width: Ti.UI.SIZE, top: 20, bottom: 20, id: "signature_button_container" });

  $.__views.padding_container.add($.__views.signature_button_container);
  $.__views.decline_button = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, backgroundImage: Alloy.Styles.buttons.secondary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.secondary.backgroundDisabledImage, color: Alloy.Styles.buttons.secondary.color, disabledColor: Alloy.Styles.buttons.secondary.disabledColor, height: 40, width: 200, titleid: "_Decline", id: "decline_button", accessibilityValue: "decline_button" });

  $.__views.signature_button_container.add($.__views.decline_button);
  $.__views.accept_button = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.buttons.primary.color, disabledColor: Alloy.Styles.buttons.primary.disabledColor, backgroundImage: Alloy.Styles.buttons.primary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.primary.backgroundDisabledImage, height: 40, width: 200, titleid: "_Approve", left: 20, id: "accept_button", accessibilityValue: "accept_button" });

  $.__views.signature_button_container.add($.__views.accept_button);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var logger = require('logging')('checkout:payments:confirmSignature', getFullControllerPath($.__controllerPath));

  var paymentTerminal = require(Alloy.CFG.devices.payment_terminal_module);

  var signatureImage = null;





  $.accept_button.addEventListener('click', onAcceptClick);


  $.decline_button.addEventListener('click', onDeclineClick);

  paymentTerminal.addEventListener('transactionComplete', dismiss);




  exports.init = init;
  exports.deinit = deinit;
  exports.dismiss = dismiss;










  function init(image) {
    logger.info('INIT called');
    $.signature_view.setImage(image);
    signatureImage = image;
  }






  function deinit() {
    logger.info('DEINIT called');
    $.accept_button.removeEventListener('click', onAcceptClick);
    $.decline_button.removeEventListener('click', onDeclineClick);
    paymentTerminal.removeEventListener('transactionComplete', dismiss);
    $.stopListening();
    $.destroy();
  }









  function onAcceptClick() {
    logger.info('onAcceptClick called');
    $.trigger('confirmSignature:accepted', {
      image: signatureImage });

  }






  function onDeclineClick() {
    logger.info('onDeclineClick called');
    dismiss({
      declined: true });

  }







  function dismiss(info) {
    $.trigger('confirmSignature:dismiss', info);
  }









  _.extend($, exports);
}

module.exports = Controller;