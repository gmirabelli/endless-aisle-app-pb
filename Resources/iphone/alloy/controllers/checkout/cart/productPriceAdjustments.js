var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'checkout/cart/productPriceAdjustments';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.product_price_adjustment_window = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.color.background.transparent, id: "product_price_adjustment_window" });

  $.__views.product_price_adjustment_window && $.addTopLevelView($.__views.product_price_adjustment_window);
  $.__views.backdrop = Ti.UI.createView(
  { top: 0, left: 0, width: 1024, height: 768, opacity: 0.25, backgroundColor: Alloy.Styles.color.background.black, id: "backdrop" });

  $.__views.product_price_adjustment_window.add($.__views.backdrop);
  $.__views.product_overrides_container = Ti.UI.createView(
  { layout: "vertical", width: 995, left: 15, height: 507, top: 47, backgroundColor: Alloy.Styles.color.background.white, id: "product_overrides_container" });

  $.__views.product_price_adjustment_window.add($.__views.product_overrides_container);
  $.__views.product_overrides_header = Ti.UI.createView(
  { layout: "horizontal", height: Ti.UI.SIZE, width: "100%", top: 13, id: "product_overrides_header" });

  $.__views.product_overrides_container.add($.__views.product_overrides_header);
  $.__views.cancel_button = Ti.UI.createButton(
  { backgroundImage: Alloy.Styles.secondaryButtonImage, color: Alloy.Styles.buttons.secondary.color, width: 146, height: 35, font: Alloy.Styles.buttonFont, left: 15, id: "cancel_button", titleid: "_Cancel", accessibilityValue: "cancel_button_price_override" });

  $.__views.product_overrides_header.add($.__views.cancel_button);
  $.__views.product_overrides_label = Ti.UI.createLabel(
  { width: 250, height: Ti.UI.SIZE, color: Alloy.Styles.accentColor, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.headlineFont, left: 255, top: 0, textid: "_Price_Overrides", id: "product_overrides_label", accessibilityValue: "product_overrides_label" });

  $.__views.product_overrides_header.add($.__views.product_overrides_label);
  $.__views.apply_button = Ti.UI.createButton(
  { backgroundImage: Alloy.Styles.primaryButtonImage, color: Alloy.Styles.buttons.primary.color, width: 146, height: 35, font: Alloy.Styles.buttonFont, left: 170, id: "apply_button", titleid: "_Apply", accessibilityValue: "apply_button_price_override" });

  $.__views.product_overrides_header.add($.__views.apply_button);
  $.__views.overrides_container = Ti.UI.createView(
  { layout: "horizontal", width: "100%", id: "overrides_container" });

  $.__views.product_overrides_container.add($.__views.overrides_container);
  $.__views.product_container = Ti.UI.createView(
  { layout: "vertical", width: 175, left: 41, top: 48, id: "product_container" });

  $.__views.overrides_container.add($.__views.product_container);
  $.__views.product_name_label = Ti.UI.createLabel(
  { width: "100%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.productFont, left: 0, id: "product_name_label", accessibilityValue: "product_name_label" });

  $.__views.product_container.add($.__views.product_name_label);
  $.__views.price_container = Ti.UI.createView(
  { layout: "horizontal", height: 40, width: "100%", left: 0, id: "price_container" });

  $.__views.product_container.add($.__views.price_container);
  $.__views.original_price_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.appFont, id: "original_price_label", accessibilityValue: "original_price_label" });

  $.__views.price_container.add($.__views.original_price_label);
  $.__views.product_image = Ti.UI.createImageView(
  { width: 175, height: Ti.UI.SIZE, left: 0, id: "product_image", accessibilityValue: "product_image" });

  $.__views.product_container.add($.__views.product_image);
  $.__views.override_container = Ti.UI.createView(
  { left: 74, id: "override_container" });

  $.__views.overrides_container.add($.__views.override_container);
  $.__views.show_overrides = Ti.UI.createView(
  { id: "show_overrides", visible: false });

  $.__views.override_container.add($.__views.show_overrides);
  $.__views.overrides = Alloy.createController('checkout/cart/productPriceOverrides', { id: "overrides", __parentSymbol: $.__views.show_overrides });
  $.__views.overrides.setParent($.__views.show_overrides);
  $.__views.manager_override_already_exists = Ti.UI.createView(
  { top: 100, id: "manager_override_already_exists", visible: false });

  $.__views.override_container.add($.__views.manager_override_already_exists);
  $.__views.manager_override_label = Ti.UI.createLabel(
  { width: 550, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.subtitleFont, top: 0, textid: "_A_manager_override_has_already_been_applied__Click_Remove_to_remove_the_override_and_continue_to_the_override_screen_", id: "manager_override_label", accessibilityValue: "manager_override_label" });

  $.__views.manager_override_already_exists.add($.__views.manager_override_label);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var currentAssociate = Alloy.Models.associate;
  var currentBasket = Alloy.Models.basket;
  var showActivityIndicator = require('dialogUtils').showActivityIndicator;
  var toCurrency = require('EAUtils').toCurrency;
  var doingManagerAuthorization = false;
  var associatePermissions = null;
  var isRemovingOverride = false;
  var selectedOverride = null;
  var index = null;
  var pli = null;




  $.apply_button.addEventListener('click', onApplyClick);

  $.cancel_button.addEventListener('click', onCancelClick);

  $.listenTo($.overrides, 'enableApply', onEnableApply);


  $.listenTo($.overrides, 'managerLoggedIn', onManagerLoggedIn);


  $.listenTo($.overrides, 'needManagerAuthorization', onNeedManagerAuthorization);




  exports.init = init;
  exports.deinit = deinit;











  function init(args) {
    var deferred = new _.Deferred();
    pli = args.pli;
    index = args.index;
    $.show_overrides.hide();

    if (pli && pli.price_override === 'true' && pli.manager_employee_id) {
      $.manager_override_already_exists.show();
      $.apply_button.setTitle(_L('Remove'));
      isRemovingOverride = true;
      $.apply_button.setEnabled(true);
      deferred.resolve();
    } else {

      $.manager_override_already_exists.hide();
      isRemovingOverride = false;
      resetToApplyButton();
      initOverrides(deferred);
    }

    if (pli) {
      $.product_name_label.setText(pli.product_name);
      $.original_price_label.setText(toCurrency(pli.base_price));
      Alloy.Globals.getImageViewImage($.product_image, pli.image);
    }
    return deferred.promise();
  }






  function deinit() {
    $.apply_button.removeEventListener('click', onApplyClick);
    $.cancel_button.removeEventListener('click', onCancelClick);
    $.stopListening($.overrides, 'enableApply', onEnableApply);
    $.stopListening($.overrides, 'managerLoggedIn', onManagerLoggedIn);
    $.stopListening($.overrides, 'needManagerAuthorization', onNeedManagerAuthorization);
    $.overrides.deinit();
    $.stopListening();
    $.destroy();
  }










  function initOverrides(deferred) {
    var assoc = currentAssociate;
    if (isKioskManagerLoggedIn()) {
      assoc = Alloy.Kiosk.manager;
    }

    var permissions = {
      allowByAmount: assoc.getPermissions().allowItemPriceOverrideByAmount,
      allowByPercent: assoc.getPermissions().allowItemPriceOverrideByPercent,
      allowFixed: assoc.getPermissions().allowItemPriceOverrideFixedPrice };


    var selectedOverride = {
      product_id: pli.product_id,
      price_override_type: 'none',
      price_override_value: 0,
      price_override_reason_code: null };

    if (pli && pli.price_override === 'true') {
      selectedOverride = {
        price_override_type: pli.price_override_type,
        price_override_value: pli.price_override_value,
        price_override_reason_code: pli.price_override_reason_code,
        manager_employee_id: pli.manager_employee_id };

    }


    $.overrides.init({
      basePrice: pli.base_price,
      permissions: permissions,
      maxPercentOff: assoc.getPermissions().itemPriceOverrideMaxPercent,
      selectedOverride: selectedOverride,
      adjustmentReasons: Alloy.CFG.product_override_reasons,
      leftJustifyManager: true });

    $.show_overrides.show();
    if (deferred) {
      deferred.resolve();
    }
  }






  function resetToApplyButton() {
    $.apply_button.setTitle(_L('Apply'));
    $.apply_button.setBackgroundImage(Alloy.Styles.primaryButtonImage);
    $.apply_button.setColor(Alloy.Styles.color.text.white);
  }






  function dismiss() {
    $.trigger('productOverride:dismiss');
  }









  function onApplyClick() {
    var deferred = new _.Deferred();
    showActivityIndicator(deferred);
    $.overrides.hideDropdown();

    if (isRemovingOverride) {
      if (pli.manager_employee_id) {
        var overrideToSet = {
          lineItem_id: pli.item_id,
          product_id: pli.product_id,
          price_override_type: 'none',
          index: index,
          kiosk_mode: isKioskMode() };

        currentBasket.setProductPriceOverride(overrideToSet, {
          c_employee_id: Alloy.Models.associate.getEmployeeId() }).
        done(function () {
          var plis = currentBasket.getProductItems();
          if (plis.length > 0) {
            pli = plis[index].attributes;
            initOverrides();
            $.manager_override_already_exists.hide();
            isRemovingOverride = false;
            resetToApplyButton();
          }
        }).fail(function () {
          notify(_L('Unable to remove product price override'), {
            preventAutoClose: true });

        }).always(function () {
          deferred.resolve();
        });
      } else {
        dismiss();
        deferred.resolve();
      }
    } else if (doingManagerAuthorization) {

      $.overrides.doManagerAuthorization().always(function () {
        deferred.resolve();
      });
    } else {

      var override = $.overrides.getOverride();
      if (override) {
        var overrideToSet = {
          lineItem_id: pli.item_id,
          product_id: pli.product_id,
          price_override_value: override.price_override_value,
          price_override_type: override.price_override_type,
          price_override_reason_code: override.price_override_reason_code,
          index: index,
          kiosk_mode: isKioskMode(),
          employee_id: Alloy.Models.associate.getEmployeeId(),
          employee_passcode: Alloy.Models.associate.getPasscode(),
          store_id: Alloy.CFG.store_id };





        if (override.manager_employee_id) {
          overrideToSet.manager_employee_id = override.manager_employee_id;
          overrideToSet.manager_employee_passcode = override.manager_employee_passcode;
          overrideToSet.manager_allowLOBO = override.manager_allowLOBO;
        } else if (isKioskManagerLoggedIn()) {
          overrideToSet.manager_employee_id = getKioskManager().getEmployeeId();
          overrideToSet.manager_employee_passcode = getKioskManager().getPasscode();
          overrideToSet.manager_allowLOBO = getKioskManager().getPermissions().allowLOBO;
        }
        currentBasket.setProductPriceOverride(overrideToSet, {
          c_employee_id: Alloy.Models.associate.getEmployeeId() }).
        done(function (model, params, options) {
          dismiss();
        }).fail(function () {
          notify(_L('Unable to apply a product price override'), {
            preventAutoClose: true });

        }).always(function () {
          deferred.resolve();
        });
      } else {
        dismiss();
        deferred.resolve();
      }
    }
  }






  function onCancelClick() {
    $.overrides.hideDropdown();
    if (doingManagerAuthorization) {

      if (associatePermissions && associatePermissions.permissions === 'none') {
        dismiss();
      }
      $.product_overrides_label.setText(_L('Price Overrides'));

      $.overrides.cancelOverride();
      doingManagerAuthorization = false;
    } else {
      dismiss();
    }
  }







  function onEnableApply(enable) {
    if (!isRemovingOverride) {
      $.apply_button.setEnabled(enable.valid);
    }
  }







  function onManagerLoggedIn(manager) {
    var permissions = {
      allowByAmount: manager.getPermissions().allowItemPriceOverrideByAmount,
      allowByPercent: manager.getPermissions().allowItemPriceOverrideByPercent,
      allowFixed: manager.getPermissions().allowItemPriceOverrideFixedPrice,
      maxPercentOff: manager.getPermissions().itemPriceOverrideMaxPercent };

    doingManagerAuthorization = false;
    $.overrides.resetPermissions(permissions);
    $.product_overrides_label.setText(_L('Price Overrides'));
  }







  function onNeedManagerAuthorization(permissions) {
    $.product_overrides_label.setText(_L('Manager Authorization'));
    doingManagerAuthorization = true;
    associatePermissions = permissions;
    $.apply_button.setEnabled(true);
  }









  _.extend($, exports);
}

module.exports = Controller;