var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'checkout/cart/productItemRow';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.productItemRowTable = Ti.UI.createTableViewRow(
  { layout: "vertical", selectionStyle: Ti.UI.iOS.TableViewCellSelectionStyle.NONE, height: Ti.UI.SIZE, id: "productItemRowTable", contains_product_image: true, product_id: $model.__transform.row_id });

  $.__views.productItemRowTable && $.addTopLevelView($.__views.productItemRowTable);
  $.__views.product_line_item_container = Ti.UI.createView(
  { top: 20, height: 195, width: 650, layout: "horizontal", left: 32, backgroundImage: Alloy.Styles.customerBorderImage, id: "product_line_item_container", contains_product_image: true });

  $.__views.productItemRowTable.add($.__views.product_line_item_container);
  $.__views.product_item = Alloy.createController('checkout/cart/productItem', { id: "product_item", __parentSymbol: $.__views.product_line_item_container });
  $.__views.product_item.setParent($.__views.product_line_item_container);
  $.__views.button_container = Ti.UI.createView(
  { layout: "vertical", left: 20, top: 20, height: 180, id: "button_container" });

  $.__views.product_line_item_container.add($.__views.button_container);
  $.__views.edit_delete_button_container = Ti.UI.createView(
  { top: 10, left: 0, height: Ti.UI.SIZE, width: "100%", id: "edit_delete_button_container" });

  $.__views.button_container.add($.__views.edit_delete_button_container);
  $.__views.edit_button = Ti.UI.createButton(
  { height: 55, width: 55, backgroundImage: Alloy.Styles.editButtonIcon, left: 10, id: "edit_button", accessibilityLabel: "edit_button" });

  $.__views.edit_delete_button_container.add($.__views.edit_button);
  $.__views.delete_button = Ti.UI.createButton(
  { height: 55, width: 55, backgroundImage: Alloy.Styles.deleteButtonIcon, right: 10, id: "delete_button", accessibilityLabel: "delete_button" });

  $.__views.edit_delete_button_container.add($.__views.delete_button);
  $.__views.override_button = Ti.UI.createButton(
  { backgroundImage: Alloy.Styles.secondaryButtonImage, width: 140, height: 28, color: Alloy.Styles.buttons.primary.color, font: Alloy.Styles.switchLabelFont, top: 9, titleid: "_Override", id: "override_button", accessibilityValue: "override_button" });

  $.__views.button_container.add($.__views.override_button);
  $.__views.more_menu_button = Ti.UI.createButton(
  { backgroundImage: Alloy.Styles.secondaryButtonImage, width: 140, height: 28, color: Alloy.Styles.color.text.white, font: Alloy.Styles.threeDotsButtonFont, top: 9, title: "• • •", id: "more_menu_button", accessibilityValue: "menu_button" });

  $.__views.button_container.add($.__views.more_menu_button);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var productOverridesAllowed = Alloy.CFG.overrides.product_price_overrides;
  var logger = require('logging')('checkout:cart:productItemRow', getFullControllerPath($.__controllerPath));


  var buttonTextLength = 18;




  $.listenTo(Alloy.Models.customer, 'change:login', adjustButtons);














  $.productItemRowTable.update = function (product, pli, showDivider, hideButtons, order) {
    logger.info('update called');
    $.product_item.init(product, pli, pli.currencyCode, order);
    if (!showDivider) {
      $.product_line_item_container.setBackgroundImage(null);
    }
    if (hideButtons) {
      $.button_container.hide();
    }
  };




  $.productItemRowTable.showOverrideButton = function () {
    if ($.override_button) {
      if (!isKioskMode() || isKioskManagerLoggedIn()) {
        $.override_button.show();
      } else {
        $.override_button.hide();
      }
    }
  };






  $.productItemRowTable.deinit = function () {
    logger.info('DEINIT called');
    $.product_item.deinit();

    $.stopListening();
    $.destroy();
  };









  function adjustButtons() {
    if (!Alloy.Models.customer.isLoggedIn()) {

      $.more_menu_button.hide();
      $.more_menu_button.setHeight(0);
      if (productOverridesAllowed && !isKioskMode()) {
        $.override_button.show();
        $.override_button.setHeight(28);
        $.button_container.show($.more_menu_button);
      }
    } else {

      $.override_button.hide();
      $.override_button.setHeight(0);
      $.more_menu_button.setHeight(28);
      $.more_menu_button.show();
    }
  }




  if (isKioskMode() && productOverridesAllowed) {
    $.listenTo(Alloy.eventDispatcher, 'kiosk:manager_login_change', function () {
      isKioskManagerLoggedIn() ? $.override_button.show() : $.override_button.hide();
    });
  }

  if ($model.getPriceOverride() === 'true' || productOverridesAllowed) {
    $.override_button.show();
  } else {
    $.override_button.hide();
    $.override_button.setWidth(0);
    $.override_button.setHeight(0);
  }

  if (productOverridesAllowed) {
    !isKioskMode() || isKioskManagerLoggedIn() ? $.override_button.show() : $.override_button.hide();
  }

  adjustButtons();

  if ($.override_button && $.override_button.getTitle().length > buttonTextLength) {
    $.override_button.setFont(Alloy.Styles.detailInfoBoldFont);
  }









  _.extend($, exports);
}

module.exports = Controller;