var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'checkout/cart/productPriceOverrides';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.overrides_details_container = Ti.UI.createView(
  { id: "overrides_details_container" });

  $.__views.overrides_details_container && $.addTopLevelView($.__views.overrides_details_container);
  $.__views.overrides_container = Ti.UI.createView(
  { layout: "horizontal", top: 48, id: "overrides_container" });

  $.__views.overrides_details_container.add($.__views.overrides_container);
  $.__views.overrides_left_column = Ti.UI.createView(
  { layout: "vertical", width: "35%", id: "overrides_left_column" });

  $.__views.overrides_container.add($.__views.overrides_left_column);
  $.__views.override_types = Alloy.createController('checkout/components/allOverrideTypes', { id: "override_types", __parentSymbol: $.__views.overrides_left_column });
  $.__views.override_types.setParent($.__views.overrides_left_column);
  $.__views.overrides_right_column = Ti.UI.createView(
  { layout: "vertical", width: "65%", disableBounce: "true", id: "overrides_right_column" });

  $.__views.overrides_container.add($.__views.overrides_right_column);
  $.__views.price_container = Ti.UI.createView(
  { layout: "horizontal", width: 410, height: 100, left: 0, id: "price_container" });

  $.__views.overrides_right_column.add($.__views.price_container);
  $.__views.amount_off_container = Ti.UI.createView(
  { width: 180, layout: "vertical", id: "amount_off_container" });

  $.__views.price_container.add($.__views.amount_off_container);
  $.__views.amount_off_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.calloutCopyFont, left: 0, id: "amount_off_label", accessibilityValue: "amount_off_label" });

  $.__views.amount_off_container.add($.__views.amount_off_label);
  $.__views.amount_off_text = Ti.UI.createTextField(
  { width: 169, height: 55, borderStyle: Ti.UI.INPUT_BORDERSTYLE_LINE, left: 0, keyboardType: Ti.UI.KEYBOARD_TYPE_NUMBERS_PUNCTUATION, font: Alloy.Styles.textFieldFont, borderColor: Alloy.Styles.color.border.light, borderWidth: 1, backgroundColor: Alloy.Styles.color.background.white, autocapitalization: Ti.UI.TEXT_AUTOCAPITALIZATION_NONE, autocorrect: false, padding: { left: 14 }, clearButtonMode: Ti.UI.INPUT_BUTTONMODE_ALWAYS, id: "amount_off_text", accessibilityLabel: "amount_off_text" });

  $.__views.amount_off_container.add($.__views.amount_off_text);
  $.__views.new_price_container = Ti.UI.createView(
  { width: 230, layout: "vertical", left: 0, id: "new_price_container" });

  $.__views.price_container.add($.__views.new_price_container);
  $.__views.new_price = Ti.UI.createView(
  { layout: "horizontal", height: Ti.UI.SIZE, width: "100%", id: "new_price" });

  $.__views.new_price_container.add($.__views.new_price);
  $.__views.new_price_label = Ti.UI.createLabel(
  { width: "50%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.calloutCopyFont, left: 0, textid: "_New_Price", id: "new_price_label", accessibilityValue: "new_price_label" });

  $.__views.new_price.add($.__views.new_price_label);
  $.__views.new_price_calculated = Ti.UI.createLabel(
  { width: "50%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT, font: Alloy.Styles.calloutFont, right: 0, id: "new_price_calculated", accessibilityValue: "new_price_calculated" });

  $.__views.new_price.add($.__views.new_price_calculated);
  $.__views.surcharge = Ti.UI.createView(
  { layout: "horizontal", height: Ti.UI.SIZE, width: "100%", top: 10, id: "surcharge", visible: false });

  $.__views.new_price_container.add($.__views.surcharge);
  $.__views.surcharge_label = Ti.UI.createLabel(
  { width: "50%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.calloutCopyFont, left: 0, textid: "_Surcharge", id: "surcharge_label", accessibilityValue: "surcharge_label" });

  $.__views.surcharge.add($.__views.surcharge_label);
  $.__views.surcharge_price = Ti.UI.createLabel(
  { width: "50%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT, font: Alloy.Styles.calloutCopyFont, right: 0, id: "surcharge_price", accessibilityValue: "surcharge_price" });

  $.__views.surcharge.add($.__views.surcharge_price);
  $.__views.adjustment_reason_container = Ti.UI.createView(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, layout: "horizontal", left: 0, id: "adjustment_reason_container" });

  $.__views.overrides_right_column.add($.__views.adjustment_reason_container);
  $.__views.get_manager_authorization_container = Ti.UI.createView(
  { layout: "vertical", left: 0, width: "100%", height: Ti.UI.SIZE, top: 29, id: "get_manager_authorization_container" });

  $.__views.overrides_right_column.add($.__views.get_manager_authorization_container);
  $.__views.manager_authorization_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.calloutCopyFont, left: 0, textid: "_Manager_Authorization", id: "manager_authorization_label", accessibilityValue: "manager_authorization_label" });

  $.__views.get_manager_authorization_container.add($.__views.manager_authorization_label);
  $.__views.has_manager_approval_container = Ti.UI.createView(
  { layout: "horizontal", left: 0, height: 0, id: "has_manager_approval_container", visible: false });

  $.__views.overrides_right_column.add($.__views.has_manager_approval_container);
  $.__views.manager_approval_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailValueFont, textid: "_Manager_Approval_", id: "manager_approval_label", accessibilityValue: "manager_approval_label" });

  $.__views.has_manager_approval_container.add($.__views.manager_approval_label);
  $.__views.manager_name_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.accentColor, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailLabelFont, left: 5, id: "manager_name_label", accessibilityValue: "manager_name_label" });

  $.__views.has_manager_approval_container.add($.__views.manager_name_label);
  $.__views.error_message_label = Ti.UI.createLabel(
  { width: 400, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.red, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.errorMessageFont, left: 0, top: 25, id: "error_message_label", accessibilityValue: "error_message_label", visible: false });

  $.__views.overrides_right_column.add($.__views.error_message_label);
  $.__views.enter_manager_authorization = Ti.UI.createView(
  { layout: "vertical", height: Ti.UI.SIZE, width: Ti.UI.SIZE, top: 29, id: "enter_manager_authorization", visible: false });

  $.__views.enter_manager_authorization && $.addTopLevelView($.__views.enter_manager_authorization);
  $.__views.manager_id = Ti.UI.createTextField(
  { color: Alloy.Styles.color.text.light, font: Alloy.Styles.dialogFieldFont, width: 455, height: 55, borderColor: Alloy.Styles.color.border.light, top: 13, passwordMask: true, padding: { left: 18 }, keyboardType: Ti.UI.KEYBOARD_TYPE_NUMBERS_PUNCTUATION, left: 0, hintText: L('_Manager_ID'), id: "manager_id", accessibilityLabel: "manager_id" });

  $.__views.enter_manager_authorization.add($.__views.manager_id);
  $.__views.manager_password = Ti.UI.createTextField(
  { color: Alloy.Styles.color.text.light, font: Alloy.Styles.dialogFieldFont, width: 455, height: 55, borderColor: Alloy.Styles.color.border.light, top: 13, passwordMask: true, padding: { left: 18 }, keyboardType: Ti.UI.KEYBOARD_TYPE_NUMBERS_PUNCTUATION, left: 0, hintText: L('_Password'), id: "manager_password", accessibilityLabel: "manager_password" });

  $.__views.enter_manager_authorization.add($.__views.manager_password);
  $.__views.manager_error = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: 80, color: Alloy.Styles.color.text.red, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.calloutCopyFont, visible: true, top: 13, left: 0, id: "manager_error", accessibilityValue: "manager_error" });

  $.__views.enter_manager_authorization.add($.__views.manager_error);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var currentBasket = Alloy.Models.basket;
  var currentAssociate = Alloy.Models.associate;

  var price_override_type = 'none';
  var price_override_value = null;
  var toCurrency = require('EAUtils').toCurrency;
  var EAUtils = require('EAUtils');

  var overrideReason = null;

  var isValid = false;
  var authorizedManager = null;
  var selectedOverride = null;
  var basePrice;





  $.amount_off_text.addEventListener('change', onAmountOffChange);

  $.amount_off_text.addEventListener('focus', onAmountOffFocus);


  $.listenTo($.override_types, 'overridetype', onOverrideType);


  $.manager_authorization_label.addEventListener('click', onManagerClick);




  exports.init = init;
  exports.deinit = deinit;
  exports.resetPermissions = resetPermissions;
  exports.doManagerAuthorization = doManagerAuthorization;
  exports.cancelOverride = cancelOverride;
  exports.hideDropdown = hideDropdown;
  exports.getOverride = getOverride;
  exports.doManagerAuthorization = doManagerAuthorization;









  function init(config) {
    authorizedManager = null;

    if (config.permissions) {

      if (!config.permissions.alloyByAmount && !config.permissions.allowByPercent && !config.permissions.allowFixed) {
        setupManagerOverrides();
        $.trigger('needManagerAuthorization', {
          permissions: 'none' });

      }

      $.override_types.hideOverrideByAmount(!config.permissions.allowByAmount);

      $.override_types.hideOverrideByPercent(!config.permissions.allowByPercent);

      $.override_types.hideOverrideFixedPrice(!config.permissions.allowFixed);
    }

    maxPercent = config.maxPercentOff - 0;
    basePrice = config.basePrice;
    $.new_price_calculated.setText(toCurrency(basePrice));


    if (config.selectedOverride) {
      selectPreviousOverride(config.selectedOverride);
      selectedOverride = config.selectedOverride;
    } else {
      selectPreviousOverride({
        price_override_type: 'none',
        price_override_value: 0,
        price_override_reason_code: null });

    }


    initializeAdjustmentReasonPicker(config.adjustmentReasons, config.selectedOverride);


    if (currentAssociate.getPermissions().allowManagerOverrides || isKioskManagerLoggedIn() && getKioskManager().canDoManagerOverrides()) {
      $.get_manager_authorization_container.hide();
    } else {
      $.get_manager_authorization_container.show();
      $.get_manager_authorization_container.setHeight(Ti.UI.SIZE);
      $.has_manager_approval_container.hide();
      $.has_manager_approval_container.setHeight(0);
      $.toolbar = Alloy.createController('components/nextPreviousToolbar');
      $.toolbar.setTextFields([$.manager_id, $.manager_password]);
    }

    if (config.leftJustifyManager) {
      $.enter_manager_authorization.setLeft(0);
    }


    if (config.surcharge) {
      $.surcharge.show();
      $.surcharge_price.setText(toCurrency(config.surcharge - 0));
    } else {
      $.surcharge.hide();
    }
    clearErrorMessage();
    enableApplyButton();
    if (EAUtils.isSymbolBasedLanguage()) {
      $.new_price_calculated.setFont(Alloy.Styles.headlineFont);
    }
  }






  function deinit() {
    $.override_types.deinit();
    if ($.toolbar) {
      $.toolbar.deinit();
    }
    $.amount_off_text.removeEventListener('change', onAmountOffChange);
    $.amount_off_text.removeEventListener('focus', onAmountOffFocus);
    $.manager_authorization_label.removeEventListener('click', onManagerClick);
    if ($.override_select_list) {
      removeAllChildren($.adjustment_reason_container);
      $.override_select_list.deinit();
    }
    $.stopListening();
    $.destroy();
  }










  function resetPermissions(permissions) {
    $.override_types.hideOverrideByAmount(!permissions.allowByAmount);
    $.override_types.hideOverrideByPercent(!permissions.allowByPercent);
    $.override_types.hideOverrideFixedPrice(!permissions.allowFixed);

    maxPercent = permissions.maxPercentOff;
    calculateOverride(price_override_type);
  }







  function doManagerAuthorization() {
    var deferred = new _.Deferred();
    $.manager_password.blur();
    $.manager_id.blur();
    var employee_id = $.manager_id.getValue();
    var passcode = $.manager_password.getValue();
    if (!employee_id || !passcode) {
      $.manager_error.setText(_L('You must provide manager code and pin.'));
      $.manager_error.show();
      deferred.resolve();
      return deferred.promise();
    }

    var manager = Alloy.createModel('associate');
    manager.fetchPermissions({
      employee_id: employee_id,
      passcode: passcode }).
    done(function (model) {

      if (manager.canDoManagerOverrides()) {
        $.trigger('managerLoggedIn', manager);
        $.enter_manager_authorization.hide();
        $.overrides_container.show();
        $.get_manager_authorization_container.hide();
        $.get_manager_authorization_container.setHeight(0);
        $.has_manager_approval_container.show();
        $.has_manager_approval_container.setHeight(Ti.UI.SIZE);
        $.manager_name_label.setText(manager.getFirstName() + ' ' + manager.getLastName());
        authorizedManager = manager;
        if (selectedOverride) {
          selectedOverride.manager_employee_id = authorizedManager.getEmployeeId();
          selectedOverride.manager_employee_passcode = authorizedManager.getPasscode();
          selectedOverride.manager_allowLOBO = authorizedManager.getPermissions().allowLOBO;
        }
      } else {
        $.manager_error.setText(_L('This manager is not allowed to do overrides'));
        $.manager_error.show();
      }
    }).fail(function (model) {
      if (model.get('httpStatus') != 200 && model.get('fault')) {
        $.manager_error.setText(model.get('fault').message);
      } else {
        $.manager_error.setText(_L('Error logging in manager'));
      }
      $.manager_error.show();
      $.manager_password.setValue('');
    }).always(function () {
      deferred.resolve();
    });
    return deferred.promise();
  }






  function cancelOverride() {
    $.enter_manager_authorization.hide();
    $.overrides_container.show();
    $.get_manager_authorization_container.show();
    $.get_manager_authorization_container.setHeight(Ti.UI.SIZE);
    $.has_manager_approval_container.hide();
    $.has_manager_approval_container.setHeight(0);
    enableApplyButton();
  }






  function getOverride() {
    return selectedOverride;
  }






  function hideDropdown() {
    $.override_select_list && $.override_select_list.dismiss();
  }








  function initializeAdjustmentReasonPicker(adjustmentReasons, selectedOverride) {
    var options = [];

    $.override_select_list = Alloy.createController('components/selectWidget', {
      values: adjustmentReasons,
      messageWhenNoSelection: _L('Adjustment Reason'),
      selectListTitleStyle: {
        accessibilityValue: 'adjustment_reason',
        width: 410,
        height: 55,
        left: 15,
        color: Alloy.Styles.color.text.black },

      selectListStyle: {
        width: 410,
        height: 55,
        top: 0,
        color: Alloy.Styles.color.text.black },

      selectedItem: selectedOverride ? selectedOverride.price_override_reason_code : null });

    $.override_select_list.setEnabled(selectedOverride && selectedOverride.price_override_type != 'none');
    $.adjustment_reason_container.add($.override_select_list.getView());

    $.listenTo($.override_select_list, 'itemSelected', onItemSelected);


    $.listenTo($.override_select_list, 'dropdownSelected', onDropdownSelected);
  }







  function selectPreviousOverride(selectedOverride) {
    var type = selectedOverride.price_override_type;
    $.override_types.setSelection(type);
    var price = basePrice;
    switch (type) {
      case 'percent':
        $.amount_off_text.setValue((100 * selectedOverride.price_override_value).toFixed(2));
        handlePercentOff();
        $.amount_off_text.setEnabled(true);
        price = price - price * selectedOverride.price_override_value;
        $.amount_off_label.setText(_L('Percentage Off'));
        break;
      case 'amount':
        $.amount_off_text.setValue((selectedOverride.price_override_value - 0).toFixed(2));
        handleAmountOff();
        $.amount_off_text.setEnabled(true);
        price = price - selectedOverride.price_override_value;
        $.amount_off_label.setText(_L('Amount Off'));
        break;
      case 'fixedPrice':
        handleFixedPrice();
        $.amount_off_text.setValue((selectedOverride.price_override_value - 0).toFixed(2));
        $.amount_off_text.setEnabled(true);
        price = selectedOverride.price_override_value - 0;
        $.amount_off_label.setText(_L('Price'));
        break;
      case 'none':
        $.amount_off_label.setText(' ');
        $.amount_off_text.setValue('');
        $.amount_off_text.setEnabled(false);
        $.get_manager_authorization_container.show();
        $.get_manager_authorization_container.setHeight(Ti.UI.SIZE);
        $.has_manager_approval_container.hide();
        $.has_manager_approval_container.setHeight(0);
        break;}

    price_override_type = type;
    price_override_value = selectedOverride.price_override_value;
    $.new_price_calculated.setText(toCurrency(price));
    setOverrideReason(selectedOverride.price_override_reason_code);
  }







  function setOverrideReason(reason) {
    overrideReason = reason;
  }






  function setOverride() {
    selectedOverride = {
      price_override_type: price_override_type,
      price_override_reason_code: overrideReason };

    if (price_override_value != undefined) {
      selectedOverride.price_override_value = price_override_value;
    }
    if (authorizedManager) {
      selectedOverride.manager_employee_id = authorizedManager.getEmployeeId();
      selectedOverride.manager_employee_passcode = authorizedManager.getPasscode();
      selectedOverride.manager_allowLOBO = authorizedManager.getPermissions().allowLOBO;
    }
    if ((price_override_type === 'amount' || price_override_type === 'percent') && price_override_value == 0) {
      selectedOverride.price_override_type = 'none';
    }
  }







  function calculateOverride(overrideType) {
    var price = basePrice;
    if (overrideType === 'none') {
      price_override_type = 'none';
      $.amount_off_text.setEnabled(false);
      $.override_select_list.setEnabled(false);
      $.amount_off_label.setText(' ');
    } else {
      $.amount_off_text.setEnabled(true);
      var value;
      switch (overrideType) {
        case 'amount':
          value = handleAmountOff();
          price = price - value;
          $.amount_off_label.setText(_L('Amount Off'));
          break;
        case 'percent':
          value = handlePercentOff();
          price = price - price * value;
          $.amount_off_label.setText(_L('Percentage Off'));
          break;
        case 'fixedPrice':
          value = handleFixedPrice();
          price = value;
          $.amount_off_label.setText(_L('Price'));
          break;}

      price_override_type = overrideType;
      price_override_value = value;
    }
    if (isNaN(price)) {
      $.new_price_calculated.setText('');
    } else {
      $.new_price_calculated.setText(toCurrency(price));
    }
    setOverride();
    enableApplyButton();
  }






  function clearErrorMessage() {
    $.error_message_label.setVisible(false);
    $.error_message_label.setText('');
  }






  function handleAmountOff() {
    var value = $.amount_off_text.getValue().trim();
    value = value.replace(',', '.');
    var amount = value - 0;
    if (value == '') {
      amount = 0;
    }
    isValid = validateAmountOff(value);
    return amount;
  }







  function validateAmountOff(value) {
    clearErrorMessage();
    if (isNaN(value)) {
      $.error_message_label.setVisible(true);
      $.error_message_label.setText(_L('Amount off must be a number'));
      return false;
    } else {
      if (parseInt(value) > parseInt(basePrice)) {
        $.error_message_label.setVisible(true);
        $.error_message_label.setText(_L('Amount off cannot be greater than the base price'));
        return false;
      } else if (value / basePrice > maxPercent) {
        $.error_message_label.setVisible(true);
        $.error_message_label.setText(String.format(_L('Percentage off cannot be greater than'), maxPercent * 100));
        return false;
      } else if (value < 0) {
        $.error_message_label.setVisible(true);
        $.error_message_label.setText(_L('Please enter a number greater than 0'));
        return false;
      } else {
        return true;
      }
    }
  }






  function enableApplyButton() {
    if (price_override_type != 'none') {
      $.trigger('enableApply', {
        valid: isValid && $.amount_off_text.getValue().trim() != '' && overrideReason != null && overrideReason != 'null' });

    } else {
      $.trigger('enableApply', {
        valid: true });

    }
  }






  function handlePercentOff() {
    var value = $.amount_off_text.getValue().trim();
    value = value.replace(',', '.');
    var percent = (value - 0) / 100;
    if (value == '') {
      percent = 0;
    }
    isValid = validatePercentOff(percent);
    return percent;
  }







  function validatePercentOff(value) {
    clearErrorMessage();
    if (isNaN(value)) {
      $.error_message_label.setVisible(true);
      $.error_message_label.setText(_L('Percentage off must be a number'));
      return false;
    } else {
      if (value > 1) {
        $.error_message_label.setVisible(true);
        $.error_message_label.setText(_L('Percentage off cannot be greater than 100%'));
        return false;
      } else if (maxPercent && value > maxPercent) {
        $.error_message_label.setVisible(true);
        $.error_message_label.setText(String.format(_L('Percentage off cannot be greater than'), maxPercent * 100));
        return false;
      } else if (value < 0) {
        $.error_message_label.setVisible(true);
        $.error_message_label.setText(_L('Please enter a number greater than 0'));
        return false;
      } else {
        return true;
      }
    }
  }






  function handleFixedPrice() {
    var value = $.amount_off_text.getValue().trim();
    value = value.replace(',', '.');
    var price = value - 0;
    if (value == '') {
      price = basePrice;
    }
    isValid = validateFixedPrice(price);
    return price;
  }






  function validateFixedPrice(price) {
    clearErrorMessage();
    if (isNaN(price)) {
      $.error_message_label.setVisible(true);
      $.error_message_label.setText(_L('Fixed price must be a number'));
      return false;
    } else if (price < 0) {
      $.error_message_label.setVisible(true);
      $.error_message_label.setText(_L('Please enter a number greater than 0'));
      return false;
    } else {
      return true;
    }
  }






  function setupManagerOverrides() {
    $.manager_id.setValue('');
    $.manager_password.setValue('');
    $.manager_error.hide();
    $.enter_manager_authorization.show();
    $.overrides_container.hide();
  }









  function onAmountOffChange() {
    calculateOverride(price_override_type);
  }






  function onAmountOffFocus() {
    $.override_select_list.dismiss();
  }







  function onOverrideType(event) {
    calculateOverride(event.overrideType);
    $.override_select_list.setEnabled(event.overrideType != 'none');
    $.override_select_list.dismiss();
    if (event.overrideType != 'none') {
      $.amount_off_text.focus();
    }
  }






  function onManagerClick() {
    setupManagerOverrides();
    $.trigger('needManagerAuthorization');
  }







  function onItemSelected(event) {
    setOverrideReason(event.item.value);
    setOverride();
    enableApplyButton();
  }






  function onDropdownSelected() {
    $.amount_off_text.blur();
  }









  _.extend($, exports);
}

module.exports = Controller;