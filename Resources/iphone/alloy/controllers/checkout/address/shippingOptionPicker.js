var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'checkout/address/shippingOptionPicker';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.container = Ti.UI.createView(
  { height: 60, width: "100%", backgroundColor: Alloy.Styles.color.background.white, id: "container" });

  $.__views.container && $.addTopLevelView($.__views.container);
  $.__views.picker_container = Ti.UI.createView(
  { height: "100%", width: "100%", layout: "horizontal", backgroundColor: Alloy.Styles.color.background.white, id: "picker_container" });

  $.__views.container.add($.__views.picker_container);
  $.__views.ship_to_address_wrapper = Ti.UI.createView(
  { left: 0, width: "33.3%", height: "100%", backgroundColor: Alloy.Styles.color.background.white, layout: "horizontal", bubbleParent: false, id: "ship_to_address_wrapper" });

  $.__views.picker_container.add($.__views.ship_to_address_wrapper);
  $.__views.ship_to_address_checkbox = Ti.UI.createImageView(
  { left: 10, top: 15, height: 25, width: 25, image: Alloy.Styles.roundCheckboxDarkGray, checked: true, id: "ship_to_address_checkbox", accessibilityValue: "ship_to_address_checkbox" });

  $.__views.ship_to_address_wrapper.add($.__views.ship_to_address_checkbox);
  $.__views.ship_to_address_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 10, top: 15, font: Alloy.Styles.unselectedTabFont, textid: "_Ship_to_address", id: "ship_to_address_label", accessibilityValue: "ship_to_address_label" });

  $.__views.ship_to_address_wrapper.add($.__views.ship_to_address_label);
  if (isShipToCurrentStoreEnabled()) {
    $.__views.ship_to_current_store_wrapper = Ti.UI.createView(
    { left: 0, width: "33.3%", height: "100%", backgroundColor: Alloy.Styles.color.background.white, layout: "horizontal", bubbleParent: false, id: "ship_to_current_store_wrapper" });

    $.__views.picker_container.add($.__views.ship_to_current_store_wrapper);
    $.__views.ship_to_current_store_checkbox = Ti.UI.createImageView(
    { left: 10, top: 15, height: 25, width: 25, image: Alloy.Styles.unselectedRoundCheckboxDarkGray, checked: false, id: "ship_to_current_store_checkbox", accessibilityValue: "ship_to_current_store_checkbox" });

    $.__views.ship_to_current_store_wrapper.add($.__views.ship_to_current_store_checkbox);
    $.__views.ship_to_current_store_label = Ti.UI.createLabel(
    { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 10, top: 15, font: Alloy.Styles.unselectedTabFont, textid: "_Ship_here", id: "ship_to_current_store_label", accessibilityValue: "ship_to_current_store_label" });

    $.__views.ship_to_current_store_wrapper.add($.__views.ship_to_current_store_label);
  }
  if (isDifferentStorePickupEnabled()) {
    $.__views.dif_store_pickup_wrapper = Ti.UI.createView(
    { left: 0, width: "33.3%", height: "100%", backgroundColor: Alloy.Styles.color.background.white, layout: "horizontal", bubbleParent: false, id: "dif_store_pickup_wrapper" });

    $.__views.picker_container.add($.__views.dif_store_pickup_wrapper);
    $.__views.dif_store_pickup_checkbox = Ti.UI.createImageView(
    { left: 10, top: 15, height: 25, width: 25, image: Alloy.Styles.unselectedRoundCheckboxDarkGray, checked: false, id: "dif_store_pickup_checkbox", accessibilityValue: "dif_store_pickup_checkbox" });

    $.__views.dif_store_pickup_wrapper.add($.__views.dif_store_pickup_checkbox);
    $.__views.dif_store_pickup_label = Ti.UI.createLabel(
    { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 10, top: 15, font: Alloy.Styles.unselectedTabFont, textid: "_Pick_up_elsewhere", id: "dif_store_pickup_label", accessibilityValue: "dif_store_pickup_label" });

    $.__views.dif_store_pickup_wrapper.add($.__views.dif_store_pickup_label);
  }
  $.__views.separator = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.color.border.medium, width: "95%", height: 1, bottom: 0, id: "separator" });

  $.__views.container.add($.__views.separator);
  exports.destroy = function () {};




  _.extend($, $.__views);









  var args = $.args;


  var logger = require('logging')('checkout:address:shippingOptionPicker', getFullControllerPath($.__controllerPath));




  $.listenTo(Alloy.eventDispatcher, 'order_just_created', handleShipToAddressSelected);
  $.listenTo(Alloy.eventDispatcher, 'cart_cleared', handleShipToAddressSelected);




  $.ship_to_address_wrapper.addEventListener('click', handleShipToAddressSelected);

  if (isShipToCurrentStoreEnabled()) {
    $.ship_to_current_store_wrapper.addEventListener('click', handleShipToCurrentStoreSelected);
  }

  if (isDifferentStorePickupEnabled()) {
    $.dif_store_pickup_wrapper.addEventListener('click', handleDifferentStorePickupSelected);
  }




  exports.init = init;
  exports.deinit = deinit;
  exports.isShipToAddressChecked = isShipToAddressChecked;
  exports.isShipToCurrentStoreChecked = isShipToCurrentStoreChecked;
  exports.isDifferentStorePickupChecked = isDifferentStorePickupChecked;
  exports.checkDifferentStorePickup = checkDifferentStorePickup;
  exports.checkShipToAddress = checkShipToAddress;
  exports.checkShipToStore = checkShipToStore;









  function init() {
    logger.info('Calling INIT');
    checkShipToAddress();
  }






  function deinit() {
    logger.info('Calling DEINIT');
    $.ship_to_address_wrapper.removeEventListener('click', handleShipToAddressSelected);

    if (isShipToCurrentStoreEnabled()) {
      $.ship_to_current_store_wrapper.removeEventListener('click', handleShipToCurrentStoreSelected);
    }
    if (isDifferentStorePickupEnabled()) {
      $.dif_store_pickup_wrapper.removeEventListener('click', handleDifferentStorePickupSelected);
    }

    if (_.isFunction($.hasAnythingChanged)) {
      $.hasAnythingChanged = null;
    }
    $.stopListening();
    $.destroy();
  }









  function updateSelectedCheckboxView(imageView) {
    var checkboxes = [$.ship_to_address_checkbox];
    if (isShipToCurrentStoreEnabled()) {
      checkboxes.push($.ship_to_current_store_checkbox);
    }
    if (isDifferentStorePickupEnabled()) {
      checkboxes.push($.dif_store_pickup_checkbox);
    }
    _.each(checkboxes, function (checkbox) {
      if (checkbox === imageView) {
        $.resetClass(imageView, ['shipping_option_checkbox', 'checked_option']);
      } else {
        $.resetClass(checkbox, ['shipping_option_checkbox', 'unchecked_option']);
      }
    });
  }






  function isShipToCurrentStoreEnabled() {
    return Alloy.CFG.alternate_shipping_options.order_pickup_current_store;
  }






  function isDifferentStorePickupEnabled() {
    return Alloy.CFG.alternate_shipping_options.order_pickup_different_store;
  }






  function checkShipToAddress() {
    updateSelectedCheckboxView($.ship_to_address_checkbox);
  }






  function checkShipToStore() {
    updateSelectedCheckboxView($.ship_to_current_store_checkbox);
  }






  function checkDifferentStorePickup() {
    updateSelectedCheckboxView($.dif_store_pickup_checkbox);
  }







  function isShipToAddressChecked() {
    return $.ship_to_address_checkbox.checked;
  }







  function isShipToCurrentStoreChecked() {
    return isShipToCurrentStoreEnabled() && $.ship_to_current_store_checkbox && $.ship_to_current_store_checkbox.checked;
  }







  function isDifferentStorePickupChecked() {
    return isDifferentStorePickupEnabled() && $.dif_store_pickup_checkbox && $.dif_store_pickup_checkbox.checked;
  }










  function handleShipToAddressSelected(event) {
    if (!$.ship_to_address_checkbox.checked) {
      if (_.isFunction($.hasAnythingChanged)) {
        confirmCurrentAddressFormCleanup($.hasAnythingChanged()).done(function () {
          checkShipToAddress();
          Alloy.eventDispatcher.trigger('shippingOption:shipToAddress');
        });
      } else {
        checkShipToAddress();
        Alloy.eventDispatcher.trigger('shippingOption:shipToAddress');
      }
    }
  }







  function handleShipToCurrentStoreSelected(event) {
    if (!$.ship_to_current_store_checkbox.checked) {
      if (_.isFunction($.hasAnythingChanged)) {
        confirmCurrentAddressFormCleanup($.hasAnythingChanged()).done(function () {
          checkShipToStore();
          Alloy.eventDispatcher.trigger('shippingOption:shipToCurrentStore');
        });
      } else {
        checkShipToStore();
        Alloy.eventDispatcher.trigger('shippingOption:shipToCurrentStore');
      }
    }
  }







  function handleDifferentStorePickupSelected(event) {
    if (!$.dif_store_pickup_checkbox.checked) {
      if (_.isFunction($.hasAnythingChanged)) {
        confirmCurrentAddressFormCleanup($.hasAnythingChanged()).done(function () {
          checkDifferentStorePickup();
          Alloy.eventDispatcher.trigger('shippingOption:differentStorePickupAddress');
        });
      } else {
        checkDifferentStorePickup();
        Alloy.eventDispatcher.trigger('shippingOption:differentStorePickupAddress');
      }
    }
  }








  function confirmCurrentAddressFormCleanup(hasFormChanged) {
    var deferred = new _.Deferred();
    if (hasFormChanged) {
      Alloy.Dialog.showConfirmationDialog({
        messageString: _L('Are you sure you want to leave, you might lose the data filled in the form if you continue!'),
        titleString: _L('Address Form'),
        okButtonString: _L('Continue'),
        okFunction: function () {
          deferred.resolve();
        },
        cancelFunction: function () {
          deferred.reject();
        } });

    } else {
      deferred.resolve();
    }
    return deferred.promise();
  }












  _.extend($, exports);
}

module.exports = Controller;