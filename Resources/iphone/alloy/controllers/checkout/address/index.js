var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'checkout/address/index';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.container = Ti.UI.createView(
  { id: "container", layout: "vertical" });

  $.__views.container && $.addTopLevelView($.__views.container);
  if (shouldDisplayShippingOptionPicker()) {
    $.__views.shipping_option_picker = Alloy.createController('checkout/address/shippingOptionPicker', { id: "shipping_option_picker", __parentSymbol: $.__views.container });
    $.__views.shipping_option_picker.setParent($.__views.container);
  }
  $.__views.choose_address = Alloy.createController('checkout/address/chooseAddress', { id: "choose_address", __parentSymbol: $.__views.container });
  $.__views.choose_address.setParent($.__views.container);
  $.__views.enter_address = Alloy.createController('checkout/address/enterAddress', { id: "enter_address", __parentSymbol: $.__views.container });
  $.__views.enter_address.setParent($.__views.container);
  if (shouldDisplayDifferentStorePickupAddress()) {
    $.__views.different_store_pickup_address = Alloy.createController('checkout/address/differentStorePickupAddress', { id: "different_store_pickup_address", __parentSymbol: $.__views.container });
    $.__views.different_store_pickup_address.setParent($.__views.container);
  }
  exports.destroy = function () {};




  _.extend($, $.__views);










  var currentCustomer = Alloy.Models.customer;
  var currentBasket = Alloy.Models.basket;
  var args = $.args;
  var addressType = args.addressType || 'shipping';
  var addressVerification = require('EAUtils').addressVerification;
  var logger = require('logging')('checkout:address:index', getFullControllerPath($.__controllerPath));




  if (shouldDisplayShippingOptionPicker()) {
    $.listenTo(Alloy.eventDispatcher, 'shippingOption:shipToCurrentStore', handleShipToStore);
    $.listenTo(Alloy.eventDispatcher, 'shippingOption:shipToAddress', handleShipToAddress);
    $.listenTo(Alloy.eventDispatcher, 'shippingOption:differentStorePickupAddress', handleDifferentStorePickupAddress);
  }




  $.listenTo($.enter_address, 'addressEntered', addressEntered);

  $.listenTo($.choose_address, 'addressEntered', addressEntered);

  if (shouldDisplayDifferentStorePickupAddress()) {
    $.listenTo($.different_store_pickup_address, 'addressEntered', addressEntered);
  }


  $.listenTo($.enter_address, 'chooseAddress', function () {
    hideEnterAddress();
    showChooseAddress();
  });


  $.listenTo($.choose_address, 'customer:address', function (address) {
    var model = Alloy.createModel('customerAddress');
    model.set(address);
    if (address.newAddress) {
      model.set('first_name', currentCustomer.getFirstName());
      model.set('last_name', currentCustomer.getLastName());
    }
    var email;


    if (address.email) {
      email = address.email;
    } else if (currentBasket.getCustomerEmail()) {

      email = currentBasket.getCustomerEmail();
    } else if (currentCustomer.getEmail()) {

      email = currentCustomer.getEmail();
    }

    $.enter_address.render(model, email, addressType);
    hideChooseAddress();
    showEnterAddress();
  });




  exports.init = init;
  exports.render = render;
  exports.deinit = deinit;
  exports.setAddress = setAddress;









  function init() {
    logger.info(addressType + ' init called');
    $.choose_address.init(addressType);
    $.enter_address.init(addressType);
  }








  function render(initShippingOptionPicker) {
    if (shouldDisplayShippingOptionPicker()) {


      if (!currentBasket.getShippingAddress() || currentBasket.getShippingAddress() && $.shipping_option_picker.isDifferentStorePickupChecked()) {


        if (initShippingOptionPicker && currentBasket.getLastCheckoutStatus() !== 'shippingAddress' && !$.shipping_option_picker.isDifferentStorePickupChecked() && !$.shipping_option_picker.isShipToCurrentStoreChecked()) {
          $.shipping_option_picker.init();
        }

        if ($.shipping_option_picker.isShipToAddressChecked()) {
          return renderChooseAddressOrEnterAddress();
        } else if ($.shipping_option_picker.isShipToCurrentStoreChecked()) {
          return handleShipToStore();
        } else if ($.shipping_option_picker.isDifferentStorePickupChecked()) {
          return setDifferentStorePickupAddress();
        }
      } else {
        if (currentBasket.getShipToStore()) {
          return handleShipToStore();
        } else if (currentBasket.getDifferentStorePickup()) {
          return handleDifferentStorePickupAddress();
        } else {
          return renderChooseAddressOrEnterAddress();
        }
      }
    } else {
      return renderChooseAddressOrEnterAddress();
    }
  }






  function deinit() {
    logger.info(addressType + ' deinit called');
    $.choose_address.deinit();
    $.enter_address.deinit();
    if (shouldDisplayShippingOptionPicker()) {
      $.shipping_option_picker.deinit();
    }
    if (shouldDisplayDifferentStorePickupAddress()) {
      $.different_store_pickup_address.deinit();
    }
    $.stopListening();
    $.destroy();
  }










  function renderChooseAddressOrEnterAddress() {
    logger.info(addressType + ' render called');
    var isLoggedIn = currentCustomer.isLoggedIn();
    var deferred;
    hideDifferentStorePickupAddress();


    if (isLoggedIn) {
      hideEnterAddress();
      deferred = $.choose_address.render();
      deferred.done(function () {
        if (shouldDisplayShippingOptionPicker()) {
          $.shipping_option_picker.checkShipToAddress();
        }
        showChooseAddress();
      });
      return deferred;
    } else {
      hideChooseAddress();
      $.enter_address.render();
      showEnterAddress();
      deferred = new _.Deferred();

      setTimeout(function () {
        if (shouldDisplayShippingOptionPicker()) {
          $.shipping_option_picker.checkShipToAddress();
        }
        deferred.resolve();
      }, 200);
      return deferred.promise();
    }
  }










  function setAddress(address, emailAddress, verify) {
    var promise, errorMessage;
    logger.info(addressType + ' setAddress called');
    logger.secureLog(addressType + ' setAddress called with ' + JSON.stringify(address) + ' emailAddress ' + emailAddress + ' verify ' + verify);
    var deferred = new _.Deferred();
    verify = verify != null ? verify : true;
    Alloy.Router.showActivityIndicator(deferred);
    address = address.toJSON ? address.toJSON() : address;

    var newAddress = {
      first_name: address.first_name,
      last_name: address.last_name,
      address1: address.address1,
      address2: address.address2 ? address.address2 : '',
      city: address.city,
      state_code: address.state_code,
      postal_code: address.postal_code,
      country_code: address.country_code,
      phone: address.phone };

    if (addressType == 'shipping') {
      promise = currentBasket.setShippingAddressAndEmail(newAddress, {
        email: emailAddress },
      verify, {
        c_employee_id: Alloy.Models.associate.getEmployeeId() });

      errorMessage = _L('Cannot save shipping address or email.');
    } else {
      promise = currentBasket.setBillingAddress(newAddress, verify, {
        c_employee_id: Alloy.Models.associate.getEmployeeId() });

      errorMessage = _L('Cannot save billing address.');
    }
    promise.done(function () {
      deferred.resolve();
    }).fail(function (model, params) {

      var fault = model ? model.get('fault') : null;
      var faultHandled = false;

      if (verify) {
        faultHandled = addressVerification(fault, address, updateAddress, emailAddress);
      }

      if (faultHandled === false && params) {

        var paramDataObject = JSON.parse(params.data);
        if (_.has(paramDataObject, 'email') && $.enter_address.getView().getVisible()) {
          $.enter_address.emailError();
        } else {
          if (fault) {
            errorMessage = fault.message;
            notify(errorMessage, {
              preventAutoClose: true });

          }
        }
      }


      if (faultHandled === false && fault) {
        errorMessage = fault.message;
        notify(errorMessage, {
          preventAutoClose: true });

      }

      deferred.reject();
    });
    return deferred.promise();
  }








  function updateAddress(newAddress, email) {
    logger.info(addressType + ' updateAddress called address: ' + JSON.stringify(newAddress) + ' email: ' + email);
    if ($.enter_address.getView().getVisible()) {
      $.enter_address.updateAddress(newAddress);
    }
    setAddress(newAddress, email, false).done(continueCheckout);
  }








  function getStorePickupShippingMethod(methods) {
    var storePickupMethodIds = Alloy.CFG.ship_to_store.store_pickup_ids;
    if (storePickupMethodIds) {
      for (var i = 0; i < methods.length; i++) {
        if (_.contains(storePickupMethodIds, methods[i].getID())) {
          return methods[i];
        }
      }
    }
    return undefined;
  }








  function setShippingMethodForShipToDifferentStore(data) {
    var deferred = new _.Deferred();
    var promise = currentBasket.getShippingMethods({
      c_employee_id: Alloy.Models.associate.getEmployeeId() });

    Alloy.Router.showActivityIndicator(deferred);
    promise.done(function () {
      var methods = currentBasket.getAvailableShippingMethods();
      if (methods && methods.length > 0) {
        var storePickupShippingMethod = getStorePickupShippingMethod(methods);
        if (!storePickupShippingMethod) {
          Alloy.Dialog.showAlertDialog({
            messageString: _L('Store pickup shipping methods have not been configured'),
            titleString: _L('Shipping Method Error') });

          deferred.reject();
          return;
        }
        var method = {
          id: storePickupShippingMethod.getID() };

        var customData = {
          c_employee_id: Alloy.Models.associate.getEmployeeId(),
          c_isDifferentStorePickup: data.isDifferentStorePickup,
          c_pickupFromStoreId: data.pickupFromStoreId,
          c_message: data.message };

        currentBasket.setDifferentStorePickupMessage(data.message);
        currentBasket.setShippingMethod(method, undefined, customData).done(function () {

          removeShippingOverride().done(function () {
            var state = currentBasket.getNextCheckoutState();
            if (state == 'shippingMethod') {
              state = currentBasket.checkoutStates[currentBasket.checkoutStates.indexOf(currentBasket.getCheckoutStatus()) + 2];
            }
            logger.info(addressType + ' moving to ' + state);
            currentBasket.setCheckoutStatus(state);
            deferred.resolve();
          }).fail(function (model) {
            var fault = model.get('fault');
            if (fault && fault.message && fault.message != '') {
              notify(fault.message, {
                preventAutoClose: true });

            }
            deferred.reject();
          });
        }).fail(function (model) {
          var fault = model.get('fault');
          if (fault && fault.message && fault.message != '') {
            notify(fault.message, {
              preventAutoClose: true });

          }
          deferred.reject();
        });
      } else {
        deferred.resolve();
      }
    });

    return deferred.promise();
  }







  function removeShippingOverride() {
    var override = {
      shipping_method_id: currentBasket.getShippingMethod().getID(),
      price_override_type: 'none',
      employee_id: Alloy.Models.associate.getEmployeeId(),
      employee_passcode: Alloy.Models.associate.getPasscode(),
      store_id: Alloy.CFG.store_id,
      kiosk_mode: isKioskMode(),
      ignore_permissions: isKioskMode() };


    if (isKioskManagerLoggedIn()) {
      override.manager_employee_id = getKioskManager().getEmployeeId();
      override.manager_employee_passcode = getKioskManager().getPasscode();
      override.manager_allowLOBO = getKioskManager().getPermissions().allowLOBO;
    }

    var promise = currentBasket.setShippingPriceOverride(override, {
      c_employee_id: Alloy.Models.associate.getEmployeeId() },
    {
      silent: true });

    promise.fail(function () {
      Alloy.Dialog.showAlertDialog({
        messageString: _L('Unable to remove shipping override'),
        titleString: _L('Basket Error') });

    });
    return promise;
  }







  function continueCheckout(data) {
    logger.info(addressType + ' continueCheckout called ');

    if ($.enter_address.getView().getVisible()) {
      var newAddress = currentBasket.getAddress(addressType).toJSON();

      delete newAddress.id;
      $.enter_address.saveNewAddress(newAddress);
    }

    if (currentBasket.canEnableCheckout()) {
      var state = currentBasket.getNextCheckoutState();

      if (data && data.hasOwnProperty('isDifferentStorePickup') && data.isDifferentStorePickup && (state == 'shippingMethod' || state == 'askBillingAddress')) {
        if (addressType == 'shipping') {
          setShippingMethodForShipToDifferentStore(data);
        } else if (addressType == 'billing') {
          if (state == 'shippingMethod') {
            state = currentBasket.checkoutStates[currentBasket.checkoutStates.indexOf(currentBasket.getCheckoutStatus()) + 2];
          }
          logger.info(addressType + ' moving to ' + state);
          currentBasket.setCheckoutStatus(state);
        }
      } else {
        logger.info(addressType + ' moving to ' + state);
        currentBasket.setCheckoutStatus(state);
      }
    } else {
      $.trigger('checkoutDisabled');
    }
  }







  function resolvedAPromise() {
    var deferred = new _.Deferred();
    deferred.resolve();
    return deferred.promise();
  }






  function hideEnterAddress() {
    $.enter_address.getView().hide();
    $.enter_address.getView().setHeight(0);
    $.enter_address.getView().setWidth(0);
    $.enter_address.closeKeyboard();
  }






  function showEnterAddress() {
    $.enter_address.getView().show();
    $.enter_address.getView().setHeight(Ti.UI.SIZE);
    $.enter_address.getView().setWidth(Ti.UI.SIZE);
  }






  function hideChooseAddress() {
    $.choose_address.getView().hide();
    $.choose_address.getView().setHeight(0);
    $.choose_address.getView().setWidth(0);
  }






  function showChooseAddress() {
    $.choose_address.getView().show();
    $.choose_address.getView().setHeight(Ti.UI.SIZE);
    $.choose_address.getView().setWidth(Ti.UI.SIZE);
  }






  function showDifferentStorePickupAddress() {
    if (shouldDisplayDifferentStorePickupAddress()) {
      $.different_store_pickup_address.getView().show();
      $.different_store_pickup_address.getView().setHeight(Ti.UI.SIZE);
      $.different_store_pickup_address.getView().setWidth(Ti.UI.SIZE);
    }
  }






  function hideDifferentStorePickupAddress() {
    if (shouldDisplayDifferentStorePickupAddress()) {
      $.different_store_pickup_address.getView().hide();
      $.different_store_pickup_address.getView().setHeight(0);
      $.different_store_pickup_address.getView().setWidth(0);
      $.different_store_pickup_address.closeKeyboard();
    }
  }







  function getThisStoreAddressModel() {
    var basketAddress = currentBasket.getAddress(addressType);
    var storeAddress = Alloy.Models.storeInfo.constructStoreAddress();
    var origAddress1 = storeAddress.address1;
    storeAddress.address1 = storeAddress.last_name;
    storeAddress.first_name = currentCustomer.getFirstName();
    storeAddress.last_name = currentCustomer.getLastName();
    storeAddress.address2 = origAddress1 + (storeAddress.address2 ? ', ' + storeAddress.address2 : '');
    if (currentCustomer.getPhone()) {
      storeAddress.phone = currentCustomer.getPhone();
    } else {
      storeAddress.phone = '';

    }

    if (basketAddress) {
      storeAddress.first_name = basketAddress.getFirstName();
      storeAddress.last_name = basketAddress.getLastName();
      storeAddress.phone = basketAddress.getPhone();
    }

    storeAddress.shipToStore = true;

    return Alloy.createModel('customerAddress', storeAddress);
  }










  function addressEntered(data) {
    logger.info(addressType + ' addressEntered called');
    logger.secureLog(JSON.stringify(data));


    if (addressType === 'shipping') {
      currentBasket.setShipToStore(data.isShipToStore);
      if (data.hasOwnProperty('isDifferentStorePickup')) {
        currentBasket.setDifferentStorePickup(data.isDifferentStorePickup);
      } else {
        currentBasket.setDifferentStorePickup(false);
      }
    }
    setAddress(data.address, data.email).done(function () {
      if (!data.hasOwnProperty('isDifferentStorePickup')) {
        data.isDifferentStorePickup = false;
      }
      continueCheckout(data);
    });

  }







  function handleShipToStore() {

    var model = getThisStoreAddressModel();
    var email = currentBasket.getCustomerEmail() || currentCustomer.getEmail();
    currentCustomer.isLoggedIn() && $.choose_address.render();
    $.enter_address.render(model, email, true);
    hideChooseAddress();
    hideDifferentStorePickupAddress();
    showEnterAddress();
    if (shouldDisplayShippingOptionPicker()) {
      $.shipping_option_picker.checkShipToStore();
    }
    return resolvedAPromise();
  }







  function setDifferentStorePickupAddress() {
    if (Alloy.CFG.alternate_shipping_options.order_pickup_different_store) {
      currentCustomer.isLoggedIn() && $.choose_address.render();

      var address = {};
      if (currentBasket.getShippingAddress()) {
        address = {
          first_name: currentBasket.getShippingAddress().getFirstName(),
          last_name: currentBasket.getShippingAddress().getLastName(),
          phone: currentBasket.getShippingAddress().getPhone(),
          email_address: currentBasket.getCustomerEmail(),
          message: currentBasket.getDifferentStorePickupMessage() || '' };

      } else {
        address = {
          first_name: currentCustomer.getFirstName(),
          last_name: currentCustomer.getLastName(),
          phone: currentCustomer.getPhone(),
          email_address: currentCustomer.getEmail() || currentBasket.getCustomerEmail() || '',
          message: currentBasket.getDifferentStorePickupMessage() || '' };

      }
      $.different_store_pickup_address.setAddress(address);
    }
    return resolvedAPromise();
  }







  function handleDifferentStorePickupAddress() {
    if (shouldDisplayDifferentStorePickupAddress()) {
      hideChooseAddress();
      hideEnterAddress();
      showDifferentStorePickupAddress();
      setDifferentStorePickupAddress();
      if (shouldDisplayShippingOptionPicker()) {
        $.shipping_option_picker.checkDifferentStorePickup();
      }
    }
    return resolvedAPromise();
  }






  function handleShipToAddress() {
    hideDifferentStorePickupAddress();
    if (currentCustomer.isLoggedIn()) {
      hideEnterAddress();
      showChooseAddress();
    } else {
      $.enter_address.render();
      hideChooseAddress();
      showEnterAddress();
    }
  }







  function shouldDisplayShippingOptionPicker() {
    return $.args.addressType === 'shipping' && (Alloy.CFG.alternate_shipping_options.order_pickup_current_store || Alloy.CFG.alternate_shipping_options.order_pickup_different_store);
  }







  function shouldDisplayDifferentStorePickupAddress() {
    return $.args.addressType === 'shipping' && Alloy.CFG.alternate_shipping_options.order_pickup_different_store;
  }







  function hasAnythingChanged() {
    if (currentBasket.getCheckoutStatus() === 'shippingAddress') {
      if (($.shipping_option_picker.isShipToAddressChecked() || $.shipping_option_picker.isShipToCurrentStoreChecked()) && $.enter_address.getView().getVisible()) {
        return $.enter_address.hasAnythingChanged();
      } else if ($.shipping_option_picker.isDifferentStorePickupChecked() && $.different_store_pickup_address.getView().getVisible()) {
        if ($.different_store_pickup_address.isAddressFormVisible()) {
          return $.different_store_pickup_address.hasAnythingChanged();
        } else {
          return false;
        }
      }
    }
    return false;
  }




  if (shouldDisplayShippingOptionPicker()) {

    $.shipping_option_picker.hasAnythingChanged = hasAnythingChanged;
  }









  _.extend($, exports);
}

module.exports = Controller;