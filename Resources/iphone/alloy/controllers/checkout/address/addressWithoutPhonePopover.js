var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'checkout/address/addressWithoutPhonePopover';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.popover_window = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.color.background.transparent, id: "popover_window" });

  $.__views.popover_window && $.addTopLevelView($.__views.popover_window);
  $.__views.backdrop = Ti.UI.createView(
  { top: 0, left: 0, width: 1024, height: 768, opacity: 0.25, backgroundColor: Alloy.Styles.color.background.black, id: "backdrop" });

  $.__views.popover_window.add($.__views.backdrop);
  $.__views.popover_container = Ti.UI.createView(
  { layout: "vertical", width: "50%", top: 75, height: Ti.UI.SIZE, backgroundColor: Alloy.Styles.color.background.white, id: "popover_container" });

  $.__views.popover_window.add($.__views.popover_container);
  $.__views.confirm_title = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.accentColor, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.titleFont, top: 20, textid: "_Missing_Phone_Number", id: "confirm_title", accessibilityValue: "phone_popover_confirm_title" });

  $.__views.popover_container.add($.__views.confirm_title);
  $.__views.message = Ti.UI.createLabel(
  { width: Ti.UI.FILL, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.messageFont, top: 50, left: 50, right: 50, textid: "_Provide_a_phone_number_for_the_shipping_address_", id: "message", accessibilityValue: "phone_popover_confirmation_message" });

  $.__views.popover_container.add($.__views.message);
  $.__views.phone_number_container = Ti.UI.createView(
  { layout: "vertical", width: Ti.UI.SIZE, height: Ti.UI.SIZE, top: 20, id: "phone_number_container" });

  $.__views.popover_container.add($.__views.phone_number_container);
  $.__views.phone_number = Ti.UI.createTextField(
  { width: Ti.UI.FILL, autocorrect: false, top: 10, left: 50, right: 50, maxLength: 50, backgroundColor: Alloy.Styles.color.background.medium, hintText: L('Phone*'), height: 50, color: Alloy.Styles.color.text.darkest, font: Alloy.Styles.textFieldFont, returnKeyType: Ti.UI.RETURNKEY_GO, keyboardType: Ti.UI.KEYBOARD_TYPE_NUMBERS_PUNCTUATION, padding: { left: 18 }, id: "phone_number", accessibilityLabel: "phone_popover_number" });

  $.__views.phone_number_container.add($.__views.phone_number);
  $.__views.phone_error = Ti.UI.createLabel(
  { width: Ti.UI.FILL, height: 0, color: Alloy.Styles.color.text.red, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, left: 50, right: 50, top: 2, font: Alloy.Styles.errorMessageFont, id: "phone_error", accessibilityValue: "phone_popover_error" });

  $.__views.phone_number_container.add($.__views.phone_error);
  $.__views.buttons_container = Ti.UI.createView(
  { layout: "horizontal", height: Ti.UI.SIZE, width: Ti.UI.SIZE, top: 20, bottom: 20, id: "buttons_container" });

  $.__views.popover_container.add($.__views.buttons_container);
  $.__views.cancel_button = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, backgroundImage: Alloy.Styles.buttons.secondary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.secondary.backgroundDisabledImage, color: Alloy.Styles.buttons.secondary.color, disabledColor: Alloy.Styles.buttons.secondary.disabledColor, height: 40, width: 200, titleid: "_Cancel", id: "cancel_button", accessibilityValue: "phone_popover_cancel_button" });

  $.__views.buttons_container.add($.__views.cancel_button);
  $.__views.confirm_button = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.buttons.primary.color, disabledColor: Alloy.Styles.buttons.primary.disabledColor, backgroundImage: Alloy.Styles.buttons.primary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.primary.backgroundDisabledImage, height: 40, width: 200, left: 20, titleid: "_Confirm", id: "confirm_button", accessibilityValue: "phone_popover_confirm_button" });

  $.__views.buttons_container.add($.__views.confirm_button);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var args = arguments[0] || {};
  var countryCode = args.countryCode;
  var message = args.message;
  var phoneNumber = args.phoneNumber;
  var title = args.title;




  $.phone_number.addEventListener('return', continueSubmit);

  $.confirm_button.addEventListener('click', continueSubmit);

  $.cancel_button.addEventListener('click', dismiss);




  exports.init = init;
  exports.deinit = deinit;









  function init() {

    if (message) {
      $.message.setText(message);
    }
    if (phoneNumber) {
      $.phone_number.setValue(phoneNumber);
    }
    if (title) {
      $.confirm_title.setText(title);
    }
  }






  function deinit() {
    $.stopListening();
    $.phone_number.removeEventListener('return', continueSubmit);
    $.confirm_button.removeEventListener('click', continueSubmit);
    $.cancel_button.removeEventListener('click', dismiss);
    $.destroy();
  }









  function dismiss() {
    $.trigger('addressWithoutPhoneNumber:dismiss');
  }






  function continueSubmit() {
    var phoneNumber = $.phone_number.getValue().trim();
    var phone_regex;
    var regex = Alloy.CFG.regexes.phone[countryCode];
    phone_regex = new RegExp(regex, 'i');

    if (phoneNumber && phoneNumber != '' && phone_regex && phone_regex.test(phoneNumber)) {
      $.phone_error.setText('');
      $.phone_error.setHeight(0);
      $.phone_number_container.setHeight(60);
      $.trigger('addressWithoutPhoneNumber:confirm', {
        phoneNumber: phoneNumber });

    } else {
      $.phone_number_container.setHeight(80);
      $.phone_error.setHeight(20);
      $.phone_error.setText(_L('Please provide a valid phone number.'));
    }
  }









  _.extend($, exports);
}

module.exports = Controller;