var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'checkout/address/differentStorePickupAddress';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};




  $.address = Alloy.createModel('shippingAddress');$.basketInventory = Alloy.createCollection('product');$.unavailableInventory = Alloy.createCollection('productItem');$.nearestStores = Alloy.createCollection('store');


  $.__views.container = Ti.UI.createView(
  { top: 0, width: Ti.UI.SIZE, height: Ti.UI.SIZE, id: "container" });

  $.__views.container && $.addTopLevelView($.__views.container);
  $.__views.store_selector_container = Ti.UI.createView(
  { width: Ti.UI.SIZE, layout: "vertical", top: 15, height: "96%", id: "store_selector_container" });

  $.__views.container.add($.__views.store_selector_container);
  $.__views.postal_code_form_container = Ti.UI.createView(
  { top: 5, layout: "vertical", height: Ti.UI.SIZE, width: 532, id: "postal_code_form_container" });

  $.__views.store_selector_container.add($.__views.postal_code_form_container);
  $.__views.postal_code_title_label = Ti.UI.createLabel(
  { width: "100%", height: 20, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailTextFont, left: 0, top: 5, textid: "_Search_stores_by_zip_code_below", id: "postal_code_title_label", accessibilityLabel: "postal_code_title_label" });

  $.__views.postal_code_form_container.add($.__views.postal_code_title_label);
  $.__views.postal_code_fields_container = Ti.UI.createView(
  { left: 0, top: 0, height: 50, width: "100%", layout: "horizontal", id: "postal_code_fields_container" });

  $.__views.postal_code_form_container.add($.__views.postal_code_fields_container);
  $.__views.postal_code_fields = Ti.UI.createView(
  { left: 0, top: 0, height: Ti.UI.SiZE, width: "66%", layout: "vertical", id: "postal_code_fields" });

  $.__views.postal_code_fields_container.add($.__views.postal_code_fields);
  $.__views.postal_code = Ti.UI.createTextField(
  { left: 0, width: "100%", required: true, hasSpecificOnBlurValidationFunction: true, hintTextid: "_Enter_zip_code_for_store_pickup", autocorrect: false, clearButtonMode: Ti.UI.INPUT_BUTTONMODE_ALWAYS, returnKeyType: Ti.UI.RETURNKEY_SEARCH, originalBorderColor: Alloy.Styles.color.border.dark, originalBackgroundColor: Alloy.Styles.color.background.white, borderColor: Alloy.Styles.color.border.dark, backgroundColor: Alloy.Styles.color.background.white, borderWidth: 1, borderRadius: 2, font: Alloy.Styles.textFieldFont, padding: { left: 18 }, maxLength: 50, color: Alloy.Styles.color.text.darkest, height: 50, id: "postal_code", accessibilityLabel: "input_postal_code" });

  $.__views.postal_code_fields.add($.__views.postal_code);
  $.__views.postal_code_error = Ti.UI.createLabel(
  { width: "100%", height: 0, color: Alloy.Styles.color.text.red, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, top: 2, font: Alloy.Styles.errorMessageFont, id: "postal_code_error", accessibilityValue: "postal_code_error" });

  $.__views.postal_code_fields.add($.__views.postal_code_error);
  $.__views.search_fields = Ti.UI.createView(
  { left: "2%", top: 0, height: 50, width: "30%", layout: null, id: "search_fields" });

  $.__views.postal_code_fields_container.add($.__views.search_fields);
  $.__views.search_button = Ti.UI.createButton(
  { titleid: "_Search", textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.buttons.primary.color, disabledColor: Alloy.Styles.buttons.primary.disabledColor, backgroundImage: Alloy.Styles.buttons.primary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.primary.backgroundDisabledImage, height: 40, width: "96%", left: "2%", id: "search_button", accessibilityValue: "search_button" });

  $.__views.search_fields.add($.__views.search_button);
  $.__views.no_result_container = Ti.UI.createView(
  { top: 10, width: 532, height: 0, visible: false, id: "no_result_container" });

  $.__views.store_selector_container.add($.__views.no_result_container);
  $.__views.no_result = Ti.UI.createLabel(
  { width: "100%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: "center", textid: "_No_stores_found", font: Alloy.Styles.messageFont, id: "no_result", accessibilityValue: "no_result" });

  $.__views.no_result_container.add($.__views.no_result);
  $.__views.store_picker_container = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.color.background.white, width: "100%", top: 10, height: Ti.UI.SIZE, layout: "vertical", id: "store_picker_container" });

  $.__views.store_selector_container.add($.__views.store_picker_container);
  $.__views.footer_indicator_wrapper = Ti.UI.createView(
  { height: 50, width: 490, backgroundColor: Alloy.Styles.color.background.white, id: "footer_indicator_wrapper" });

  $.__views.footer_indicator = Ti.UI.createActivityIndicator(
  { style: Ti.UI.ActivityIndicatorStyle.DARK, id: "footer_indicator", accessibilityLabel: "footer_indicator" });

  $.__views.footer_indicator_wrapper.add($.__views.footer_indicator);
  $.__views.store_list_container = Ti.UI.createTableView(
  { top: 0, width: 490, height: 380, backgroundColor: Alloy.Styles.color.background.white, footerView: $.__views.footer_indicator_wrapper, id: "store_list_container" });

  $.__views.store_picker_container.add($.__views.store_list_container);
  var __alloyId21 = Alloy.Collections['$.nearestStores'] || $.nearestStores;function __alloyId22(e) {if (e && e.fromAdapter) {return;}var opts = __alloyId22.opts || {};var models = __alloyId21.models;var len = models.length;var rows = [];for (var i = 0; i < len; i++) {var __alloyId18 = models[i];__alloyId18.__transform = transformStoreDetail(__alloyId18);var __alloyId20 = Alloy.createController('checkout/address/storePickupSelectRow', { $model: __alloyId18, __parentSymbol: __parentSymbol });
      rows.push(__alloyId20.getViewEx({ recurse: true }));
    }$.__views.store_list_container.setData(rows);};__alloyId21.on('fetch destroy change add remove reset', __alloyId22);$.__views.next_button_container = Ti.UI.createView(
  { top: 10, height: 60, width: 532, id: "next_button_container" });

  $.__views.store_picker_container.add($.__views.next_button_container);
  $.__views.next_button_wrapper = Ti.UI.createView(
  { layout: "vertical", right: 0, top: 0, height: Ti.UI.SIZE, width: Ti.UI.SIZE, id: "next_button_wrapper" });

  $.__views.next_button_container.add($.__views.next_button_wrapper);
  $.__views.store_selected_next_button = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.buttons.primary.color, disabledColor: Alloy.Styles.buttons.primary.disabledColor, backgroundImage: Alloy.Styles.buttons.primary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.primary.backgroundDisabledImage, height: 40, width: 200, top: 3, left: 0, enabled: true, titleid: "_Next", id: "store_selected_next_button", accessibilityValue: "store_selected_next_button" });

  $.__views.next_button_wrapper.add($.__views.store_selected_next_button);
  $.__views.address_form_container = Ti.UI.createScrollView(
  { showVerticalScrollIndicator: true, width: "100%", layout: "vertical", top: 15, disableBounce: true, height: 0, visible: false, id: "address_form_container" });

  $.__views.container.add($.__views.address_form_container);
  $.__views.enter_address_container = Ti.UI.createView(
  { top: 20, layout: "vertical", height: Ti.UI.SIZE, width: 532, id: "enter_address_container" });

  $.__views.address_form_container.add($.__views.enter_address_container);
  $.__views.address_form_button_container = Ti.UI.createView(
  { top: 25, height: 60, width: 532, id: "address_form_button_container" });

  $.__views.address_form_container.add($.__views.address_form_button_container);
  $.__views.address_form_button_wrapper = Ti.UI.createView(
  { top: 0, height: Ti.UI.SIZE, width: "100%", id: "address_form_button_wrapper" });

  $.__views.address_form_button_container.add($.__views.address_form_button_wrapper);
  $.__views.address_form_previous_button = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, backgroundImage: Alloy.Styles.buttons.secondary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.secondary.backgroundDisabledImage, color: Alloy.Styles.buttons.secondary.color, disabledColor: Alloy.Styles.buttons.secondary.disabledColor, height: 40, width: 160, top: 3, left: 0, titleid: "_Go_Back", id: "address_form_previous_button", accessibilityValue: "address_form_previous_button" });

  $.__views.address_form_button_wrapper.add($.__views.address_form_previous_button);
  $.__views.address_form_next_button = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.buttons.primary.color, disabledColor: Alloy.Styles.buttons.primary.disabledColor, backgroundImage: Alloy.Styles.buttons.primary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.primary.backgroundDisabledImage, height: 40, width: 200, top: 3, right: 0, titleid: "_Next", id: "address_form_next_button", accessibilityValue: "address_form_next_button" });

  $.__views.address_form_button_wrapper.add($.__views.address_form_next_button);
  if (true) {
    $.__views.unavailable_items_popover = Ti.UI.iPad.createPopover(
    { backgroundColor: Alloy.Styles.color.background.white, arrowDirection: Ti.UI.iPad.POPOVER_ARROW_DIRECTION_LEFT, id: "unavailable_items_popover" });

    $.__views.unavailable_items_popover && $.addTopLevelView($.__views.unavailable_items_popover);
    $.__views.__alloyId23 = Ti.UI.createWindow(
    { tabBarHidden: 1, navBarHidden: 1, backgroundColor: Alloy.Styles.color.background.white, statusBarStyle: Ti.UI.iOS.StatusBar.LIGHT_CONTENT, id: "__alloyId23" });

    $.__views.unavailable_items_list = Ti.UI.createTableView(
    { backgroundColor: Alloy.Styles.color.background.white, rowSeparatorInsets: { left: 1, right: 1 }, top: 0, height: "100%", width: "100%", separatorColor: Alloy.Styles.color.background.darkGray, id: "unavailable_items_list" });

    $.__views.__alloyId23.add($.__views.unavailable_items_list);
    var __alloyId27 = Alloy.Collections['$.unavailableInventory'] || $.unavailableInventory;function __alloyId28(e) {if (e && e.fromAdapter) {return;}var opts = __alloyId28.opts || {};var models = __alloyId27.models;var len = models.length;var rows = [];for (var i = 0; i < len; i++) {var __alloyId24 = models[i];__alloyId24.__transform = transformUnavailableInventory(__alloyId24);var __alloyId26 = Alloy.createController('checkout/address/unavailableProductItemRow', { $model: __alloyId24, __parentSymbol: __parentSymbol });
        rows.push(__alloyId26.getViewEx({ recurse: true }));
      }$.__views.unavailable_items_list.setData(rows);};__alloyId27.on('fetch destroy change add remove reset', __alloyId28);$.__views.unavailable_nav_window = Ti.UI.iOS.createNavigationWindow(
    { height: 300, width: 400, window: $.__views.__alloyId23, id: "unavailable_nav_window" });

    $.__views.unavailable_items_popover.contentView = $.__views.unavailable_nav_window;}
  exports.destroy = function () {__alloyId21 && __alloyId21.off('fetch destroy change add remove reset', __alloyId22);__alloyId27 && __alloyId27.off('fetch destroy change add remove reset', __alloyId28);};




  _.extend($, $.__views);









  var args = $.args;
  var currentBasket = Alloy.Models.basket;
  var storeListPosition;
  var enteredValidPostalCode;
  var loadMoreStores = true;
  var handlingInfiniteScroll = false;
  var lastCountedNumberOfStores = 0;
  var paginationStep = 5;
  var currentBasket = Alloy.Models.basket;
  var addressFormVisible = null;
  var tfields = ['first_name', 'last_name', 'phone', 'email_address', 'message'];
  var storePickupAddressForm = require('config/address/storePickupConfig').local_address[Alloy.CFG.countrySelected];
  var defaultStorePickupAddressForm = require('config/address/storePickupConfig').local_address['default'];
  var viewLayoutData;

  if (storePickupAddressForm) {
    viewLayoutData = require(storePickupAddressForm).getLayout();
  } else {
    viewLayoutData = require(defaultStorePickupAddressForm).getLayout();
  }

  var formMgr = require('addressFormManager')({
    viewLayoutData: viewLayoutData,
    $: $,
    getterFunctionsForCustomFields: {
      getCountry: getCurrentStoreCountryCode } });


  var fetchImages = require('EAUtils').fetchImagesForProducts;


  var getPostalCode = formMgr.getPostalCode;
  var isPostalCodeValid = formMgr.isPostalCodeValid;
  var validatePostalCodeField = formMgr.validatePostalCodeField;



  $.listenTo(Alloy.eventDispatcher, 'hideAuxillaryViews', hidePopover);
  $.listenTo(Alloy.eventDispatcher, 'app:dialog_displayed', hidePopover);
  $.listenTo(Alloy.eventDispatcher, 'app:navigation', hidePopover);
  $.listenTo(Alloy.eventDispatcher, 'cart_cleared', clearPreviousSelections);
  $.listenTo(Alloy.eventDispatcher, 'order_just_created', clearPreviousSelections);




  $.store_list_container.addEventListener('click', handleStoreListClick);
  $.store_list_container.addEventListener('scroll', handleInfiniteScroll);
  $.unavailable_items_list.addEventListener('click', handleUnavailableItemsListClick);
  $.store_selected_next_button.addEventListener('click', handleStoreSelectedNextClick);
  $.address_form_previous_button.addEventListener('click', handleGoBack);
  $.address_form_next_button.addEventListener('click', handleAddressFormNextClick);



  $.listenTo(currentBasket, 'change:product_items', handleBasketChange);
  $.listenTo($.nearestStores, 'change', enableOrDisableStoreSelectedNextButton);
  $.listenTo(Alloy.Models.customer, 'customer:clear', clearPreviousSelections);




  exports.closeKeyboard = closeKeyboard;
  exports.setAddress = setAddress;
  exports.isAddressFormVisible = isAddressFormVisible;
  exports.hasAnythingChanged = hasAnythingChanged;
  exports.deinit = deinit;
  var previousValues = '';









  function render() {
    formMgr.renderAddressViewInContainer('enter_address_container');
    $.toolbar = Alloy.createController('components/nextPreviousToolbar');
    var formFields = formMgr.getAllFormFields();
    formFields.push($.message);
    $.toolbar.setTextFields(formFields);
    $.postal_code.addEventListener('return', handlePostalCodeReturn);
    $.search_button.addEventListener('click', handleSearchClick);

    $.first_name.addEventListener('change', enableAddressFormNextClick);
    $.last_name.addEventListener('change', enableAddressFormNextClick);
    $.phone.addEventListener('change', enableAddressFormNextClick);
    $.email_address.addEventListener('change', enableAddressFormNextClick);

    $.address_form_next_button.setTitle(Alloy.CFG.collect_billing_address ? _L('Next') : _L('Create Order'));
  }






  function deinit() {
    formMgr.deinit();
    $.toolbar.deinit();
    $.postal_code.removeEventListener('return', handlePostalCodeReturn);
    $.search_button.removeEventListener('click', handleSearchClick);
    $.store_list_container.removeEventListener('click', handleStoreListClick);
    $.store_list_container.removeEventListener('scroll', handleInfiniteScroll);
    $.unavailable_items_list.removeEventListener('click', handleUnavailableItemsListClick);
    $.store_selected_next_button.removeEventListener('click', handleStoreSelectedNextClick);
    $.address_form_previous_button.removeEventListener('click', handleGoBack);
    $.address_form_next_button.removeEventListener('click', handleAddressFormNextClick);

    $.first_name.removeEventListener('change', enableAddressFormNextClick);
    $.last_name.removeEventListener('change', enableAddressFormNextClick);
    $.phone.removeEventListener('change', enableAddressFormNextClick);
    $.email_address.removeEventListener('change', enableAddressFormNextClick);

    if (_.isArray($.store_list_container.getData()) && $.store_list_container.getData().length > 0) {
      _.each($.store_list_container.getData()[0].rows, function (row) {
        row.deinit();
      });
    }
    if (_.isArray($.unavailable_items_list.getData()) && $.unavailable_items_list.getData().length > 0) {
      _.each($.unavailable_items_list.getData()[0].rows, function (row) {
        row.deinit();
      });
    }

    $.stopListening();
    $.destroy();
  }









  function handlePostalCodeFocus() {
    setTimeout(function () {
      $.address_form_container.scrollTo(0, 590);
    }, 100);
  }






  function handlePostalCodeReturn() {
    if (validatePostalCodeField() && enteredValidPostalCode !== getPostalCode()) {
      lastCountedNumberOfStores = 0;
      loadMoreStores = true;
      var deferred = new _.Deferred();
      Alloy.Router.showActivityIndicator(deferred);
      fetchNearestStores(0).done(function () {
        enteredValidPostalCode = getPostalCode();
        fetchNearestStoresAvailability($.nearestStores.start, $.nearestStores.start + paginationStep - 1).always(function () {
          $.store_list_container.scrollToTop();
          deferred.resolve();
        });
        showHideNoStoresFoundLabel($.nearestStores.length <= 0);
      }).fail(function () {
        deferred.resolve();
      });
    }
  }






  function closeKeyboard() {
    formMgr.closeKeyboard();
    $.postal_code.blur();
  }






  function handleSearchClick() {
    closeKeyboard();
    handlePostalCodeReturn();
  }







  function setAddress(addressData) {
    previousValues = '';
    formMgr.clearAllErrors();
    $.address.set(addressData);
    formMgr.setAllTextFieldsValues($.address);
    $.message.setValue(addressData.message || '');
    previousValues = formMgr.getAllFieldValues(tfields, []);
    if (currentBasket.getDifferentStorePickup() && currentBasket.getShippingAddress() && enteredValidPostalCode !== '') {
      return;
    }

    $.postal_code.setValue(Alloy.Models.storeInfo.getPostalCode() || '');
    handlePostalCodeReturn();
    enableAddressFormNextClick();
    handleGoBack();
  }







  function getCurrentStoreCountryCode() {
    return Alloy.Models.storeInfo.getCountryCode();
  }







  function getBasketProductIds() {
    return currentBasket.getAllProductItemIds();
  }







  function getBasketProductIdsAndQuantities() {
    return currentBasket.getAllProductItemsIdsAndQuantities();
  }








  function filterOutUnnavailableBasketItems(ids) {
    return currentBasket.filterProductItemsByIds(ids);
  }








  function transformStoreDetail(model) {
    var message = '';
    var unavailableItemCount = model.getAllUnavailableBasketItems().length;
    if (unavailableItemCount === currentBasket.getProductItems().length) {
      message = _L('All items are unavailable');
    } else if (unavailableItemCount === 1) {
      message = String.format(_L('%d item is unavailable'), unavailableItemCount);
    } else if (unavailableItemCount > 1) {
      message = String.format(_L('%d items are unavailable'), unavailableItemCount);
    }

    return {
      id: model.getId(),
      display_name: model.getAddressDisplay(),
      distance: model.getTextualDistance(),
      unavailability_message: message };

  }








  function fetchNearestStores(start) {
    start = _.isNumber(start) ? start : $.nearestStores.length;
    return $.nearestStores.getStoresWithPagination({
      ids: [],
      country_code: getCurrentStoreCountryCode(),
      postal_code: getPostalCode(),
      max_distance: Alloy.CFG.store_availability.max_distance_search,
      count: paginationStep,
      start: start },
    start === 0 ? false : true, true);
  }








  function fetchNearestStoresAvailability(startIndex, endIndex) {

    var promise = $.basketInventory.fetchModels(null, {
      ids: getBasketProductIds(),

      inventory_ids: $.nearestStores.getInventoryIdsByStartEndIndex(startIndex, endIndex) });

    promise.done(function () {
      $.basketInventory.setQuantities(getBasketProductIdsAndQuantities());
      $.nearestStores.setBasketInventoryAvailabilty($.basketInventory, startIndex, endIndex);
    });
    promise.fail(function (failModel) {
      var fault = failModel ? failModel.get('fault') : null;
      var errorMsg = _L('Unable to load store availability.');
      if (fault && fault.message && fault.message != '') {
        errorMsg = fault.message;
      }
      notify(errorMsg, {
        preventAutoClose: true });

    });
    return promise;
  }







  function handleStoreListClick(event) {
    var sections = $.store_list_container.getData();
    var selectedStoreModel = $.nearestStores.at(event.index);
    if (sections && _.isArray(sections) && sections.length > 0) {
      _.each(sections[0].rows, function (row, idx) {
        if (selectedStoreModel && selectedStoreModel.isBasketAvailable()) {
          if (idx === event.index) {
            $.nearestStores.setSelectedStore(event.index);
            row.checkSelectedCheckbox();
            enableOrDisableStoreSelectedNextButton();
          } else {
            row.uncheckCheckbox();
          }
        } else if (idx === event.index && selectedStoreModel && !selectedStoreModel.isBasketAvailable()) {
          $.unavailableInventory.reset(filterOutUnnavailableBasketItems(selectedStoreModel.getAllUnavailableBasketItems()));
          fetchImages($.unavailableInventory.models, $.unavailable_items_list).done(function () {

            if ($.unavailableInventory.models.length === 1) {
              $.unavailable_items_list.setSeparatorColor(Alloy.Styles.color.background.transparent);
            } else {
              $.unavailable_items_list.setSeparatorColor(Alloy.Styles.color.background.darkGray);
            }

            $.unavailable_items_popover.show({
              view: row.getUnavailableButton() });

          }).fail(function (failModel) {
            var fault = failModel ? failModel.get('fault') : null;
            var errorMsg = _L('Unable to load images.');
            if (fault && fault.message && fault.message != '') {
              errorMsg = fault.message;
            }
            notify(errorMsg, {
              preventAutoClose: true });

          });
        }
      });
    }
  }







  function hasAnythingChanged() {
    return formMgr.hasAnythingChanged(tfields, previousValues, []);
  }







  function handleInfiniteScroll(event) {
    var shouldLoad = storeListPosition && event.contentOffset.y > storeListPosition && event.contentOffset.y + event.size.height > event.contentSize.height;

    storeListPosition = event.contentOffset.y;

    if (!handlingInfiniteScroll && shouldLoad && enteredValidPostalCode === getPostalCode() && loadMoreStores) {
      handlingInfiniteScroll = true;
      showTableActivityIndicator();
      fetchNearestStores().done(function () {
        if ($.nearestStores.length > lastCountedNumberOfStores) {
          lastCountedNumberOfStores = $.nearestStores.length;
          showTableActivityIndicator();
          fetchNearestStoresAvailability($.nearestStores.start, $.nearestStores.start + paginationStep - 1).always(function () {
            hideTableActivityIndicator();
          });
        } else {
          hideTableActivityIndicator();
          loadMoreStores = false;
          notify(_L('No more stores available'));
        }
      }).fail(function (failModel) {
        var fault = failModel ? failModel.get('fault') : null;
        var errorMsg = _L('Unable to get nearest stores.');
        if (fault && fault.message && fault.message != '') {
          errorMsg = fault.message;
        }
        notify(errorMsg, {
          preventAutoClose: true });

      }).always(function () {
        hideTableActivityIndicator();
        handlingInfiniteScroll = false;
      });
    }
  }







  function showHideNoStoresFoundLabel(show) {
    if (show) {
      $.store_picker_container.hide();
      $.store_picker_container.setHeight(0);
      $.no_result_container.setHeight(50);
      $.no_result_container.show();
    } else {
      $.no_result_container.hide();
      $.no_result_container.setHeight(0);
      $.store_picker_container.setHeight(Ti.UI.SIZE);
      $.store_picker_container.show();
    }
  }








  function isNotNullOrEmpty(val) {
    return !_.isNull(val) && val !== '';
  }






  function showTableActivityIndicator() {
    $.footer_indicator_wrapper.setHeight(50);
    $.footer_indicator.show();
    $.footer_indicator_wrapper.show();
  }






  function hideTableActivityIndicator() {
    $.footer_indicator.hide();
    $.footer_indicator_wrapper.hide();
    $.footer_indicator_wrapper.setHeight(0);
  }







  function isAddressFormValid() {
    var phoneValid = formMgr.validatePhoneField();
    var emailValid = formMgr.validateEmailAddressField();
    var fnameValid = formMgr.showHideError('first_name', isNotNullOrEmpty(formMgr.getFirstName()));
    var lnameValid = formMgr.showHideError('last_name', isNotNullOrEmpty(formMgr.getLastName()));
    return phoneValid && emailValid && fnameValid && lnameValid;
  }







  function isAddressFormVisible() {
    return addressFormVisible;
  }







  function transformUnavailableInventory(model) {
    return {
      stock_availability_message: model.getCurrentStockLevel() > 0 ? String.format(_L('Only %d left in stock'), model.getCurrentStockLevel()) : _L('Out of stock') };

  }







  function confirmDeleteItem(item) {

    hidePopover();


    Alloy.Dialog.showConfirmationDialog({
      messageString: String.format(_L('Do you really want to delete this item?'), item.getProductName()),
      titleString: _L('Delete Item'),
      okButtonString: _L('Delete'),
      okFunction: function () {
        currentBasket.setLastCheckoutStatusSilently('shippingAddress');
        var promise = currentBasket.removeItem(item.getItemId());
        Alloy.Router.showActivityIndicator(promise);
        promise.done(function () {
          if (currentBasket.getProductItems().length === 0) {
            currentBasket.setCheckoutStatus('cart');
          } else {
            handlePostalCodeReturn();
          }
        }).fail(function (failModel) {
          var fault = failModel ? failModel.get('fault') : null;
          var errorMsg = _L('Unable to delete item.');
          if (fault && fault.message && fault.message != '') {
            errorMsg = fault.message;
          }
          notify(errorMsg, {
            preventAutoClose: true });

        });
      } });

  }









  function hidePopover() {
    $.unavailable_items_popover.hide();
  }






  function switchToStoreSelector() {
    addressFormVisible = false;
    $.address_form_container.hide();
    $.address_form_container.setHeight(0);
    $.store_selector_container.setHeight('96%');
    $.store_selector_container.show();
  }






  function switchToAddressForm() {
    addressFormVisible = true;
    $.store_selector_container.hide();
    $.store_selector_container.setHeight(0);
    $.address_form_container.setHeight('96%');
    $.address_form_container.show();
  }






  function handleBasketChange() {
    var scheduleStoresRequest = false;
    var basketPIdsAndQuants = getBasketProductIdsAndQuantities();

    if (basketPIdsAndQuants.length === $.basketInventory.length) {
      for (var idx = 0; idx < basketPIdsAndQuants.length; idx++) {
        var prod = $.basketInventory.get(basketPIdsAndQuants[idx].id);
        if (!prod || prod.getPreviouslySetQuantity() !== basketPIdsAndQuants[idx].quantity) {
          scheduleStoresRequest = true;
          break;
        }
      }
    } else {
      scheduleStoresRequest = true;
    }

    if (scheduleStoresRequest) {
      enteredValidPostalCode = '';
      $.nearestStores.reset([]);
      $.store_selected_next_button.setEnabled(false);
      $.address_form_next_button.setEnabled(false);
      switchToStoreSelector();
      if (currentBasket.getShippingAddress() && currentBasket.getDifferentStorePickup()) {
        Alloy.eventDispatcher.trigger('differentStorePickupAddress:reconfirmAddress');
      }
    }
  }






  function enableOrDisableStoreSelectedNextButton() {

    var selectedStoreModel = $.nearestStores.getSelectedStore();

    if (selectedStoreModel && selectedStoreModel.isSelected()) {
      $.store_selected_next_button.setEnabled(true);
    } else {
      $.store_selected_next_button.setEnabled(false);
    }
  }










  function handleUnavailableItemsListClick(event) {
    if (event.source.id === 'delete_button') {
      confirmDeleteItem($.unavailableInventory.at(event.index));
    }
  }






  function handleStoreSelectedNextClick() {
    var selectedStoreModel = $.nearestStores.getSelectedStore();

    if (selectedStoreModel && selectedStoreModel.isSelected()) {
      switchToAddressForm();
    }
  }






  function handleGoBack() {
    if (addressFormVisible) {
      if (!hasAnythingChanged()) {
        closeKeyboard();
        switchToStoreSelector();
      } else {
        Alloy.Dialog.showConfirmationDialog({
          messageString: _L('Are you sure you want to leave, you might lose the data filled in the form if you continue!'),
          titleString: _L('Address Form'),
          okButtonString: _L('Continue'),
          okFunction: function () {
            closeKeyboard();
            switchToStoreSelector();
          } });

      }
    }
  }






  function enableAddressFormNextClick() {
    if (isNotNullOrEmpty(formMgr.getLastName()) && isNotNullOrEmpty(formMgr.getFirstName()) && formMgr.isPhoneValid() && formMgr.isEmailAddressValid()) {
      $.address_form_next_button.setEnabled(true);
    } else {
      $.address_form_next_button.setEnabled(false);
    }
  }






  function handleAddressFormNextClick() {
    closeKeyboard();
    var storeAddressModel = $.nearestStores.getSelectedStore();
    if (isAddressFormValid() && storeAddressModel) {
      var address = storeAddressModel.constructStoreAddressForDifferentStorePickup(formMgr.getFirstName(), formMgr.getLastName());
      address.phone = formMgr.getPhone();
      $.trigger('addressEntered', {
        address: address,
        email: $.email_address.getValue(),
        isShipToStore: false,
        isDifferentStorePickup: true,
        pickupFromStoreId: storeAddressModel.getId(),
        message: $.message.getValue() });

    }
  }






  function clearPreviousSelections() {
    logger.info('clearPreviousSelections called');
    switchToStoreSelector();
    var sections = $.store_list_container.getData();
    if (sections && _.isArray(sections) && sections.length > 0) {
      _.each(sections[0].rows, function (row, idx) {
        row.uncheckCheckbox();
      });
    }
    $.nearestStores.setSelectedStore(-1);
    enteredValidPostalCode = '';
    $.nearestStores.reset([]);
    currentBasket.setDifferentStorePickupMessage('');
    enableOrDisableStoreSelectedNextButton();
  }




  render();









  _.extend($, exports);
}

module.exports = Controller;