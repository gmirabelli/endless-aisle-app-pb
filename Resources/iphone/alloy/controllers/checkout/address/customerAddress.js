var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
		delete obj[key];
	}
	return arg;
}

function Controller() {

	require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'checkout/address/customerAddress';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};







	$.__views.customer_address = Ti.UI.createTableViewRow(
	{ selectionStyle: Ti.UI.iOS.TableViewCellSelectionStyle.NONE, backgroundImage: Alloy.Styles.customerBorderImage, layout: "vertical", address_name: $model.__transform.address_name, id: "customer_address" });

	$.__views.customer_address && $.addTopLevelView($.__views.customer_address);
	$.__views.address_container = Ti.UI.createView(
	{ layout: "vertical", height: Ti.UI.SIZE, top: 10, id: "address_container" });

	$.__views.customer_address.add($.__views.address_container);
	$.__views.address_tile = Ti.UI.createView(
	{ layout: "horizontal", height: 120, width: "100%", left: 0, top: 0, id: "address_tile" });

	$.__views.address_container.add($.__views.address_tile);
	$.__views.radio_button_container = Ti.UI.createView(
	{ left: 0, width: 20, height: Ti.UI.SIZE, top: 10, id: "radio_button_container" });

	$.__views.address_tile.add($.__views.radio_button_container);
	$.__views.radio_button = Ti.UI.createImageView(
	{ image: Alloy.Styles.radioButtonOffImage, id: "radio_button", accessibilityLabel: "radio_button" });

	$.__views.radio_button_container.add($.__views.radio_button);
	$.__views.address_column = Ti.UI.createView(
	{ left: 20, layout: "vertical", width: 400, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, top: 0, id: "address_column" });

	$.__views.address_tile.add($.__views.address_column);
	$.__views.address_name = Ti.UI.createLabel(
	{ width: 400, height: 25, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, ellipsize: true, wordWrap: false, font: Alloy.Styles.calloutCopyFont, text: $model.__transform.address_name, id: "address_name", accessibilityValue: "address_name" });

	$.__views.address_column.add($.__views.address_name);
	$.__views.address_display = Ti.UI.createLabel(
	{ width: 400, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, ellipsize: true, wordWrap: false, font: Alloy.Styles.detailValueFont, id: "address_display", accessibilityValue: "address_display" });

	$.__views.address_column.add($.__views.address_display);
	$.__views.button_column = Ti.UI.createView(
	{ layout: "vertical", width: 150, id: "button_column" });

	$.__views.address_tile.add($.__views.button_column);
	$.__views.edit_button = Ti.UI.createButton(
	{ textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, backgroundImage: Alloy.Styles.buttons.secondary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.secondary.backgroundDisabledImage, color: Alloy.Styles.buttons.secondary.color, disabledColor: Alloy.Styles.buttons.secondary.disabledColor, height: 28, width: 100, titleid: "_Edit", top: 10, id: "edit_button", accessibilityValue: "edit_button" });

	$.__views.button_column.add($.__views.edit_button);
	exports.destroy = function () {};




	_.extend($, $.__views);










	var args = arguments[0] || {};
	var addressType = args.addressType;




	$.customer_address.select = function (selected) {
		selected ? $.radio_button.setImage(Alloy.Styles.radioButtonOnImage) : $.radio_button.setImage(Alloy.Styles.radioButtonOffImage);
	};

	$.customer_address.getAddress = function () {
		return $model;
	};

	var addressLabel = $model.getAddressDisplay(addressType);
	$.address_display.setText(addressLabel);









	_.extend($, exports);
}

module.exports = Controller;