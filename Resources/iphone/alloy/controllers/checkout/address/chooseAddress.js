var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'checkout/address/chooseAddress';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};




  $.addresses = Alloy.createCollection('customerAddress');


  $.__views.shipping_window = Ti.UI.createView(
  { layout: "vertical", top: 0, width: Ti.UI.SIZE, height: Ti.UI.SIZE, id: "shipping_window" });

  $.__views.shipping_window && $.addTopLevelView($.__views.shipping_window);
  $.__views.address_contents = Ti.UI.createView(
  { layout: "vertical", height: 460, width: 600, top: 30, left: 78, id: "address_contents" });

  $.__views.shipping_window.add($.__views.address_contents);
  $.__views.address_container = Ti.UI.createTableView(
  { layout: "vertical", height: Ti.UI.FILL, id: "address_container", separatorStyle: "transparent" });

  $.__views.address_contents.add($.__views.address_container);
  var __alloyId16 = Alloy.Collections['$.addresses'] || $.addresses;function __alloyId17(e) {if (e && e.fromAdapter) {return;}var opts = __alloyId17.opts || {};var models = __alloyId16.models;var len = models.length;var rows = [];for (var i = 0; i < len; i++) {var __alloyId13 = models[i];__alloyId13.__transform = transformAddress(__alloyId13);var __alloyId15 = Alloy.createController('checkout/address/customerAddress', { $model: __alloyId13, __parentSymbol: __parentSymbol });
      rows.push(__alloyId15.getViewEx({ recurse: true }));
    }$.__views.address_container.setData(rows);};__alloyId16.on('fetch destroy change add remove reset', __alloyId17);$.__views.addresses_msg = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, id: "addresses_msg", accessibilityValue: "addresses_msg" });

  $.__views.address_contents.add($.__views.addresses_msg);
  $.__views.button_container = Ti.UI.createView(
  { layout: "horizontal", height: 50, left: 78, top: 20, id: "button_container" });

  $.__views.shipping_window.add($.__views.button_container);
  $.__views.add_address_button = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, backgroundImage: Alloy.Styles.buttons.secondary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.secondary.backgroundDisabledImage, color: Alloy.Styles.buttons.secondary.color, disabledColor: Alloy.Styles.buttons.secondary.disabledColor, height: 42, width: 228, titleid: "_Add_New_Address", id: "add_address_button", accessibilityValue: "add_address_button" });

  $.__views.button_container.add($.__views.add_address_button);
  $.__views.next_button = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.buttons.primary.color, disabledColor: Alloy.Styles.buttons.primary.disabledColor, backgroundImage: Alloy.Styles.buttons.primary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.primary.backgroundDisabledImage, height: 42, width: 228, left: 26, titleid: "_Next", id: "next_button", enabled: false });

  $.__views.button_container.add($.__views.next_button);
  exports.destroy = function () {__alloyId16 && __alloyId16.off('fetch destroy change add remove reset', __alloyId17);};




  _.extend($, $.__views);










  var currentCustomer = Alloy.Models.customer;
  var currentBasket = Alloy.Models.basket;
  var currentAddresses = currentCustomer.addresses;
  var logger = require('logging')('checkout:address:chooseAddress', getFullControllerPath($.__controllerPath));
  var addressType = '';

  var _controllersToAddresses = {};
  var selectedAddress = null;
  var _controllersById = {};
  var _addressesById = {};
  var preferred_address_id = null;
  var addressesToRows = {};
  var usingTemporaryAddress = false;
  var tempAddressIndex = -1;
  var oldShipToStore = false;
  var differentStorePickup = false;

  var toCountryName = require('EAUtils').countryCodeToCountryName;
  var EAUtils = require('EAUtils');




  $.listenTo(Alloy.eventDispatcher, 'shippingOption:shipToCurrentStore', handleShipToStore);
  $.listenTo(Alloy.eventDispatcher, 'shippingOption:shipToAddress', handleShipToAddress);
  $.listenTo(Alloy.eventDispatcher, 'shippingOption:differentStorePickupAddress', handleDifferentStorePickupAddress);





  $.address_container.addEventListener('click', onAddressContainerClick);

  $.next_button.addEventListener('click', onNextButtonClick);

  $.add_address_button.addEventListener('click', onAddAddressButtonClick);




  exports.init = init;
  exports.render = render;
  exports.deinit = deinit;










  function init(type) {
    logger.info(type + ' init called ');
    addressType = type;

    $.next_button.setAccessibilityValue(addressType + '_choose_address_next_btn');
  }







  function render() {
    logger.info(addressType + ' render called');
    var deferred = new _.Deferred();

    if (addressType === 'shipping') {
      oldShipToStore = currentBasket.getShipToStore() || false;
      logger.info(addressType + ' oldShipToStore ' + oldShipToStore);
    }
    currentAddresses.fetchAddresses(currentCustomer.getCustomerId()).done(function () {
      if (currentCustomer.isLoggedIn()) {
        var addresses = currentAddresses.getAddressesOfType(addressType);
        if (addresses.length == 0 && !currentBasket.getAddress(addressType) || addresses.length == 0 && oldShipToStore) {
          $.next_button.setEnabled(false);
          $.address_container.hide();
          $.address_container.setHeight(0);
          $.addresses_msg.setHeight(Ti.UI.SIZE);
          $.addresses_msg.setText(_L('There are no saved addresses.'));
          $.addresses_msg.show();
          deferred.resolve();
        } else {
          $.addresses_msg.hide();
          $.addresses_msg.setHeight(0);
          preferred_address_id = currentAddresses.getPreferredID();

          $.addresses.once('reset', function () {
            selectAddress(addresses).done(function () {
              deferred.resolve();
            });
          });
          $.addresses.reset(addresses);
          $.address_container.setHeight(Ti.UI.FILL);
          $.address_container.show();
        }
      } else {
        deferred.resolve();
      }
    }).fail(function () {
      deferred.reject();
    });
    return deferred.promise();
  }






  function deinit() {
    logger.info(addressType + ' deinit called');
    $.stopListening();
    $.address_container.removeEventListener('click', onAddressContainerClick);
    $.next_button.removeEventListener('click', onNextButtonClick);
    $.add_address_button.removeEventListener('click', onAddAddressButtonClick);
    $.destroy();
  }












  function selectAddress(addresses) {
    logger.info(addressType + ' selectAddress called');
    var deferred = new _.Deferred();
    usingTemporaryAddress = false;
    tempAddressIndex = -1;
    var differentStorePickup = currentBasket.getDifferentStorePickup();

    var chosenAddress = currentBasket.getAddress(addressType);
    $.next_button.setEnabled(false);

    addressesToRows = {};


    if ($.address_container.getSections().length > 0) {
      var children = $.address_container.getSections()[0].getRows();
      if (children) {
        _.each(children, function (child) {
          addressesToRows[child.address_name] = child;
        });
      }
    }

    var foundChosenAddress = false;
    _.each(addresses, function (address) {

      if (chosenAddress && !(oldShipToStore || differentStorePickup)) {
        if (isAddressEqual(address, chosenAddress)) {
          addressesToRows[address.getAddressId()].select.call(this, true);
          selectedAddress = chosenAddress;
          $.next_button.setEnabled(true);
          foundChosenAddress = true;
        }
      } else if (preferred_address_id && address.getAddressId() === preferred_address_id) {

        addressesToRows[address.getAddressId()].select.call(this, true);
        selectedAddress = address;
        $.next_button.setEnabled(true);
        foundChosenAddress = true;
      }
    });

    var addedNewRow = false;



    if (!foundChosenAddress && chosenAddress && !(oldShipToStore || differentStorePickup)) {
      preferred_address_id = null;

      var address = _.extend({}, chosenAddress);
      var address1 = getValue(address.getAddress1());
      var address2 = getValue(address.getAddress2());
      var full_name = getValue(address.getFullName());
      address.__transform = {
        address_name: '',
        address1: address1 ? address1 : '',
        address2: address2 ? address2 : '',
        full_name: full_name ? full_name : '',
        city_state_zip: address.getStateCode() ? address.getCity() + ', ' + address.getStateCode() + ' ' + address.getPostalCode() : address.getCity() + ' ' + address.getPostalCode() };

      var controller = createNewRow(address, addresses.length > 0);
      $.address_container.scrollToTop();
      controller.getView().select.call(this, true);
      selectedAddress = chosenAddress;
      $.next_button.setEnabled(true);
      usingTemporaryAddress = true;
      addedNewRow = true;
      tempAddressIndex = 0;
    }


    setTimeout(function () {
      deferred.resolve();
    }, addedNewRow ? 300 : 0);
    return deferred.promise();
  }








  function createNewRow(address, anyAddresses) {
    var row = Titanium.UI.createTableViewRow({
      selectionStyle: Titanium.UI.iOS.TableViewCellSelectionStyle.NONE });

    var controller = Alloy.createController('checkout/address/customerAddress', {
      $model: address,
      addressType: addressType });

    row.add(controller.getView().getChildren()[0]);
    row.setBackgroundImage(Alloy.Styles.customerBorderImage);
    row.setHeight('auto');
    anyAddresses ? $.address_container.insertRowBefore(0, row) : $.address_container.appendRow(row);
    row.select = function (selected) {
      controller.getView().select.call(this, selected);
    };
    return controller;
  }








  function isAddressEqual(customerAddress, address) {
    logger.info(addressType + ' isAddressEqual called, compare customerAddress ' + JSON.stringify(customerAddress, null, 4) + ' with address ' + JSON.stringify(address, null, 4));
    if (customerAddress.getAddressId() && address.getAddressId() && customerAddress.getAddressId() === address.getAddressId()) {
      return true;
    }
    return customerAddress.getFirstName() === address.getFirstName() && customerAddress.getLastName() === address.getLastName() && customerAddress.getAddress1() === address.getAddress1() && customerAddress.getAddress2() === address.getAddress2() && customerAddress.getPostalCode() === address.getPostalCode() && customerAddress.getStateCode() === address.getStateCode() && customerAddress.getCity() === address.getCity() && customerAddress.getCountryCode() === address.getCountryCode();
  }







  function getValue(value) {
    if (value === null) {
      return null;
    } else if (value === 'undefined') {
      return null;
    } else if (value === undefined) {
      return null;
    } else if (value === 'null') {
      return null;
    }
    return value;
  }







  function transformAddress(address) {
    var country_code = getValue(address.getCountryCode()).toUpperCase();
    var country_name = toCountryName(country_code);
    var address1 = getValue(address.getAddress1());
    var address2 = getValue(address.getAddress2());
    var full_name = getValue(address.getFullName());
    return {
      address_name: address.getAddressId(),
      address1: address1 ? address1 : '',
      address2: address2 ? address2 : '',
      full_name: full_name ? full_name : '',
      city_state_zip: address.getStateCode() ? address.getCity() + ', ' + address.getStateCode() + ' ' + address.getPostalCode() : address.getCity() + ' ' + address.getPostalCode(),
      country: country_name };

  }







  function onAddressContainerClick(event) {
    var address;
    var editAddress;


    if (event.index == tempAddressIndex) {
      address = currentBasket.getAddress(addressType);
      editAddress = address.toJSON();
    } else {

      address = event.row.getAddress.call(this);
      editAddress = address.toJSON();
    }
    if (event.source.id === 'edit_button') {

      $.trigger('customer:address', editAddress);
    } else {

      var children = $.address_container.getSections()[0].getRows();
      for (var i = 0; i < children.length; ++i) {
        var child = children[i];
        child.select.call(this, event.index == i);
      }
      $.next_button.setEnabled(true);
      selectedAddress = address;
    }
  }






  function onNextButtonClick() {
    var address = selectedAddress.toJSON();
    logger.info(addressType + ' onNextButtonClick');
    logger.secureLog('selectedAddress ' + JSON.stringify(address, null, 4));
    $.trigger('addressEntered', {
      address: address,
      email: currentCustomer.getEmail(),
      isDifferentStorePickup: differentStorePickup });

  }






  function onAddAddressButtonClick() {
    $.trigger('customer:address', {
      newAddress: true });

  }






  function handleDifferentStorePickupAddress() {
    differentStorePickup = true;
    $.next_button.setTitle(_L('Create Order'));
  }






  function handleShipToStore() {
    differentStorePickup = false;
    $.next_button.setTitle(_L('Next'));
  }






  function handleShipToAddress() {
    differentStorePickup = false;
    $.next_button.setTitle(_L('Next'));
  }






  if (EAUtils.isLatinBasedLanguage()) {
    $.add_address_button.setWidth(250);
    $.next_button.setWidth(250);
  }









  _.extend($, exports);
}

module.exports = Controller;