var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'checkout/address/storePickupSelectRow';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.store_row = Ti.UI.createTableViewRow(
  { height: 120, width: "100%", borderWidth: 1, touchEnabled: false, selectionStyle: Ti.UI.iOS.TableViewCellSelectionStyle.NONE, id: "store_row" });

  $.__views.store_row && $.addTopLevelView($.__views.store_row);
  $.__views.store_detail_content = Ti.UI.createView(
  { height: 120, width: "50%", layout: "vertical", left: 0, id: "store_detail_content" });

  $.__views.store_row.add($.__views.store_detail_content);
  $.__views.store_address = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Titanium.UI.TEXT_ALIGNMENT_LEFT, left: 20, top: 8, font: Alloy.Styles.sectionTitleFont, id: "store_address", text: $model.__transform.display_name, accessibilityValue: "store_address" });

  $.__views.store_detail_content.add($.__views.store_address);
  $.__views.store_distance = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.green, textAlign: Titanium.UI.TEXT_ALIGNMENT_LEFT, left: 20, top: 5, font: Alloy.Styles.sectionTitleFont, id: "store_distance", text: $model.__transform.distance, accessibilityValue: "store_distance" });

  $.__views.store_detail_content.add($.__views.store_distance);
  $.__views.basket_availability_wrapper = Ti.UI.createView(
  { height: 120, width: "50%", right: 0, id: "basket_availability_wrapper" });

  $.__views.store_row.add($.__views.basket_availability_wrapper);
  if (!isBasketAvailable()) {
    $.__views.unavailability_message = Ti.UI.createLabel(
    { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.red, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.appBoldFont, right: 40, id: "unavailability_message", text: $model.__transform.unavailability_message, accessibilityValue: "unavailability_message" });

    $.__views.basket_availability_wrapper.add($.__views.unavailability_message);
  }
  if (!isBasketAvailable()) {
    $.__views.unavailable_basket_button = Ti.UI.createButton(
    { right: 10, style: Ti.UI.iOS.SystemButton.DISCLOSURE, tintColor: Alloy.Styles.color.border.red, borderColor: Alloy.Styles.color.background.transparent, backgroundColor: Alloy.Styles.color.background.transparent, id: "unavailable_basket_button", accessibilityLabel: "unavailable_basket_button" });

    $.__views.basket_availability_wrapper.add($.__views.unavailable_basket_button);
  }
  if (isBasketAvailable()) {
    $.__views.store_checkbox = Ti.UI.createImageView(
    { height: 25, width: 25, right: 65, image: Alloy.Styles.unselectedRoundCheckboxDarkGray, checked: false, id: "store_checkbox", accessibilityValue: "select_store_checkbox" });

    $.__views.basket_availability_wrapper.add($.__views.store_checkbox);
  }
  exports.destroy = function () {};




  _.extend($, $.__views);









  var args = $.args;






  $.store_row.deinit = function () {
    $.store_row.getUnavailableButton = null;
    $.store_row.uncheckCheckbox = null;
    $.store_row.checkSelectedCheckbox = null;
    $.store_row.deinit = null;
    $.stopListening();
    $.destroy();
  };








  $.store_row.checkSelectedCheckbox = function () {
    if ($.store_checkbox) {
      $.resetClass($.store_checkbox, ['checkbox', 'checked_option']);
    }
  };






  $.store_row.uncheckCheckbox = function () {
    if ($.store_checkbox) {
      $.resetClass($.store_checkbox, ['checkbox', 'unchecked_option']);
    }
  };







  $.store_row.getUnavailableButton = function () {
    if ($.unavailable_basket_button) {
      return $.unavailable_basket_button;
    }
  };







  function isBasketAvailable() {
    return $model.isBasketAvailable();
  }







  function isSelected() {
    return $model.isSelected();
  }




  if (isBasketAvailable() && isSelected() && $.store_checkbox) {

    $.resetClass($.store_checkbox, ['checkbox', 'checked_option']);
  }









  _.extend($, exports);
}

module.exports = Controller;