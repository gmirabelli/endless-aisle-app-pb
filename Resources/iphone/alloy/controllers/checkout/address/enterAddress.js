var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'checkout/address/enterAddress';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.address_window = Ti.UI.createView(
  { top: 0, width: Ti.UI.SIZE, height: Ti.UI.SIZE, id: "address_window" });

  $.__views.address_window && $.addTopLevelView($.__views.address_window);
  $.__views.address_container = Ti.UI.createScrollView(
  { width: "100%", layout: "vertical", textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 75, top: 20, disableBounce: "true", showVerticalScrollIndicator: true, id: "address_container" });

  $.__views.address_window.add($.__views.address_container);
  $.__views.address_error = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: 0, color: Alloy.Styles.color.text.red, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.textFieldFont, visible: false, left: 0, bottom: 5, id: "address_error", accessibilityLabel: "address_error" });

  $.__views.address_container.add($.__views.address_error);
  $.__views.nickname_text_field = Ti.UI.createTextField(
  { backgroundColor: Alloy.Styles.color.background.medium, height: 50, maxLength: 20, padding: { left: 18 }, width: 532, color: Alloy.Styles.color.text.darkest, font: Alloy.Styles.textFieldFont, left: 0, autocorrect: false, hintText: L('_Nickname'), id: "nickname_text_field", accessibilityLabel: "nickname_text_field" });

  $.__views.address_container.add($.__views.nickname_text_field);
  $.__views.enter_address_container = Ti.UI.createView(
  { top: 15, layout: "vertical", height: Ti.UI.SIZE, id: "enter_address_container" });

  $.__views.address_container.add($.__views.enter_address_container);
  $.__views.enter_address_dynamic = Alloy.createController('components/enterAddressDynamic', { id: "enter_address_dynamic", __parentSymbol: $.__views.enter_address_container });
  $.__views.enter_address_dynamic.setParent($.__views.enter_address_container);
  $.__views.email_address = Ti.UI.createTextField(
  { backgroundColor: Alloy.Styles.color.background.medium, height: 50, hintText: L('_Email_Address_'), width: 532, top: 15, left: 0, keyboardType: Ti.UI.KEYBOARD_TYPE_EMAIL, padding: { left: 18 }, autocorrect: false, autocapitalization: Ti.UI.TEXT_AUTOCAPITALIZATION_NONE, font: Alloy.Styles.textFieldFont, returnKeyType: Ti.UI.RETURNKEY_DONE, id: "email_address", accessibilityLabel: "email_address" });

  $.__views.address_container.add($.__views.email_address);
  $.__views.email_error = Ti.UI.createLabel(
  { width: "100%", height: 18, color: Alloy.Styles.color.text.red, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, top: 2, font: Alloy.Styles.errorMessageFont, id: "email_error", accessibilityLabel: "email_error" });

  $.__views.address_container.add($.__views.email_error);
  $.__views.next_container = Ti.UI.createView(
  { layout: "horizontal", width: 532, top: 7, left: 0, height: 100, id: "next_container" });

  $.__views.address_container.add($.__views.next_container);
  $.__views.customer_action_container = Ti.UI.createView(
  { layout: "vertical", top: 5, width: 275, height: 200, id: "customer_action_container" });

  $.__views.next_container.add($.__views.customer_action_container);
  $.__views.save_address_book_container = Ti.UI.createView(
  { layout: "horizontal", width: 275, height: Ti.UI.SIZE, id: "save_address_book_container" });

  $.__views.customer_action_container.add($.__views.save_address_book_container);
  $.__views.address_book_switch = Ti.UI.createSwitch(
  { width: 85, value: false, id: "address_book_switch", accessibilityLabel: "address_book_switch" });

  $.__views.save_address_book_container.add($.__views.address_book_switch);
  $.__views.save_address_label = Ti.UI.createLabel(
  { width: 175, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.medium, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.switchLabelFont, left: 0, textid: "_Save_Address_to_Profile", id: "save_address_label", accessibilityValue: "save_address_label" });

  $.__views.save_address_book_container.add($.__views.save_address_label);
  $.__views.back_to_address_button = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, backgroundImage: Alloy.Styles.buttons.secondary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.secondary.backgroundDisabledImage, color: Alloy.Styles.buttons.secondary.color, disabledColor: Alloy.Styles.buttons.secondary.disabledColor, height: 42, width: 228, top: 20, left: 0, id: "back_to_address_button", accessibilityValue: "back_to_address_button", titleid: "_Cancel" });

  $.__views.customer_action_container.add($.__views.back_to_address_button);
  $.__views.next_button = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.buttons.primary.color, disabledColor: Alloy.Styles.buttons.primary.disabledColor, backgroundImage: Alloy.Styles.buttons.primary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.primary.backgroundDisabledImage, height: 42, width: 228, left: 29, top: 0, id: "next_button", titleid: "_Next" });

  $.__views.next_container.add($.__views.next_button);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var EAUtils = require('EAUtils');
  var showError = EAUtils.showError;
  var clearError = EAUtils.clearError;
  var getAddressNickname = EAUtils.getAddressNickname;
  var logger = require('logging')('checkout:address:enterAddress', getFullControllerPath($.__controllerPath));

  var currentBasket = Alloy.Models.basket;
  var currentCustomer = Alloy.Models.customer;
  var shoppingAsCustomer = false;
  var originalAddressName = null;
  var addressType = '';
  var differentStorePickup = false;

  var email_regex = new RegExp(Alloy.CFG.regexes.email, 'i');
  var textFields = [];
  var isShipToStore = false;
  var editingAddress = false;




  $.listenTo(Alloy.eventDispatcher, 'shippingOption:shipToCurrentStore', handleShipToStore);
  $.listenTo(Alloy.eventDispatcher, 'shippingOption:shipToAddress', handleShipToAddress);
  $.listenTo(Alloy.eventDispatcher, 'shippingOption:differentStorePickupAddress', handleDifferentStorePickupAddress);
  $.listenTo(Alloy.eventDispatcher, 'cart_cleared', clearPreviousSelections);




  $.next_button.addEventListener('click', onNextButtonClick);

  $.email_address.addEventListener('return', nextButtonValidation);

  $.enter_address_dynamic.getView().addEventListener('validation', nextButtonValidation);

  $.email_address.addEventListener('blur', nextButtonValidation);

  $.email_address.addEventListener('change', onEmailAddressChange);

  $.nickname_text_field.addEventListener('change', nextButtonValidation);

  $.address_book_switch.addEventListener('change', nextButtonValidation);

  $.back_to_address_button.addEventListener('click', onBackClick);




  $.listenTo(currentCustomer, 'change', function () {
    var customer_no = currentCustomer.getCustomerNumber();


    initView(customer_no);
    if (!customer_no) {

      setEmailValue('');
      $.enter_address_dynamic.setAddress(null);
    }
  });




  exports.init = init;
  exports.render = render;
  exports.deinit = deinit;
  exports.emailError = emailError;
  exports.updateAddress = updateAddress;
  exports.saveNewAddress = saveNewAddress;
  exports.closeKeyboard = closeKeyboard;
  exports.hasAnythingChanged = hasAnythingChanged;
  var emailValue = '';









  function init(type) {
    logger.info(type + ' init called');
    addressType = type;
    $.toolbar = Alloy.createController('components/nextPreviousToolbar');

    $.enter_address_dynamic.init(addressType + '_address_', addressType);
    $.next_button.setAccessibilityValue(addressType + '_enter_address_next_btn');

    if (addressType === 'billing') {
      $.email_address.setHeight(0);
      $.email_address.hide();
      $.email_error.setHeight(0);
      $.email_error.hide();
    }
  }









  function render(address, email, isShipToStoreSelected) {
    logger.info(addressType + ' render called');
    clearErrors();
    initView(currentCustomer.getCustomerNumber());


    if (address) {
      initWithAddress(address, email);
    } else {
      initWithoutAddress(isShipToStoreSelected);
    }
    resetToolbar();
    $.next_button.setEnabled(isValid());
    $.enter_address_dynamic.resetAddressSuggestionDismissedFlag();
  }






  function deinit() {
    logger.info(addressType + ' deinit called');
    $.toolbar && $.toolbar.deinit();
    $.enter_address_dynamic.deinit();
    $.next_button.removeEventListener('click', onNextButtonClick);
    $.email_address.removeEventListener('return', nextButtonValidation);
    $.email_address.removeEventListener('blur', nextButtonValidation);
    $.enter_address_dynamic.getView().removeEventListener('validation', nextButtonValidation);
    $.email_address.removeEventListener('change', onEmailAddressChange);
    $.nickname_text_field.removeEventListener('change', nextButtonValidation);
    $.address_book_switch.removeEventListener('change', nextButtonValidation);
    $.back_to_address_button.removeEventListener('click', onBackClick);
    textFields = [];
    $.stopListening();
    $.destroy();
  }









  function emailError() {
    showError($.email_address, $.email_error, _L('Invalid email address.'), false);
  }







  function updateAddress(newAddress) {
    logger.info(addressType + ' updateAddress called address: ' + JSON.stringify(newAddress));
    $.enter_address_dynamic.setAddress(newAddress);
  }







  function saveNewAddress(newAddress) {
    if ($.customer_action_container.getVisible() && $.address_book_switch.getValue()) {
      logger.info(addressType + ' saving customer address');
      var currentAddress = currentCustomer.addresses;
      var nickname = getNickname(newAddress.city, newAddress.state_code);
      newAddress.address_id = nickname;
      newAddress.original_id = nickname != originalAddressName ? originalAddressName : nickname;
      if (editingAddress) {
        currentAddress.updateAddress(newAddress, currentCustomer.getCustomerId(), {
          c_employee_id: Alloy.Models.associate.getEmployeeId() },
        {}, false).fail(function () {
          notify(_L('Could not save customer address data!'), {
            preventAutoClose: true });

        });
      } else {
        currentAddress.createAddress(newAddress, currentCustomer.getCustomerId(), {
          c_employee_id: Alloy.Models.associate.getEmployeeId() },
        {}, false).fail(function () {
          notify(_L('Could not save customer address data!'), {
            preventAutoClose: true });

        });
      }
    }
  }






  function closeKeyboard() {
    $.enter_address_dynamic.closeKeyboard();
    $.email_address.blur();
    $.nickname_text_field.blur();
  }







  function initView(isCustomer) {
    logger.info(addressType + ' initView');
    if (isCustomer) {
      showSaveCustomerAddress();
      shoppingAsCustomer = true;
    } else {
      hideSaveCustomerAddress(true);
      shoppingAsCustomer = false;
    }
  }







  function initWithoutAddress(isShipToStoreSelected) {
    logger.info(addressType + ' Entered initWithoutAddress()');
    isShipToStore = isShipToStoreSelected;
    var address = currentBasket.getAddress(addressType);

    if (!isShipToStoreSelected && address) {
      if (addressType === 'shipping' && (currentBasket.getShipToStore() || currentBasket.getDifferentStorePickup())) {

        $.enter_address_dynamic.setAddress({
          first_name: address.getFirstName(),
          last_name: address.getLastName(),
          phone: address.getPhone() });

      } else {
        $.enter_address_dynamic.setAddress(address);
      }
    } else {
      $.enter_address_dynamic.setAddress(address);
    }



    if ($.email_address.getVisible()) {
      if (currentBasket.getCustomerInfo()) {
        setEmailValue(currentBasket.getCustomerEmail());
      } else {
        setEmailValue('');
      }


      getEmailAddress();
      emailValue = $.email_address.getValue();
    }

    $.next_button.show();

    if (addressType === 'shipping') {
      if (currentBasket.getShipToStore() && isShipToStoreSelected) {
        $.enter_address_dynamic.makeFieldsReadOnly(['address1', 'address2', 'postal_code', 'city', 'countrySelectList', 'stateSelectList', 'state_picker_container', 'country_picker_container'], true);
      } else {
        $.enter_address_dynamic.makeFieldsReadOnly(['address1', 'address2', 'postal_code', 'city', 'countrySelectList', 'stateSelectList', 'state_picker_container', 'country_picker_container'], false);
        if (!address) {

          $.enter_address_dynamic.makeFieldsReadOnly(['stateSelectList'], true);
        }
      }
    }
  }








  function initWithAddress(address, email) {
    logger.info(addressType + ' init with address');
    $.enter_address_dynamic.setAddress(address);
    if ($.email_address.getVisible()) {
      setEmailValue(email);
    }

    var addressName = address.getAddressId();
    if (addressName) {
      editingAddress = true;
      $.nickname_text_field.setValue(addressName);
      $.save_address_label.setText(_L('Update Address in Profile'));
      originalAddressName = addressName;
    } else {
      originalAddressName = null;
      editingAddress = false;
      $.nickname_text_field.setValue('');
      $.save_address_label.setText(_L('Save Address to Profile'));
    }

    if (address.isShipToStoreAddress()) {
      hideSaveCustomerAddress(true);
      $.enter_address_dynamic.makeFieldsReadOnly(['address1', 'address2', 'postal_code', 'city', 'countrySelectList', 'stateSelectList', 'state_picker_container', 'country_picker_container'], true);
      isShipToStore = true;
    } else {
      $.enter_address_dynamic.makeFieldsReadOnly(['address1', 'address2', 'postal_code', 'city', 'countrySelectList', 'stateSelectList', 'state_picker_container', 'country_picker_container'], false);
      if (!address || !address.getStateCode()) {

        $.enter_address_dynamic.makeFieldsReadOnly(['stateSelectList'], true);
      }
      isShipToStore = false;
      emailValue = $.email_address.getValue();
    }
  }






  function getCurrentAddress() {
    return $.enter_address_dynamic.getAddress();
  }







  function isValid() {
    var addressIsValid = $.enter_address_dynamic.isValid();
    var emailIsValid = true;
    if ($.email_address.getVisible()) {
      emailIsValid = getEmailAddress() !== null;
    }
    return addressIsValid && emailIsValid;
  }






  function getEmailAddress() {
    if ($.email_address.value) {
      if (email_regex.test($.email_address.value)) {
        clearError($.email_address, $.email_error, Alloy.Styles.color.text.lightest, Alloy.Styles.color.text.lightest, false);
        return $.email_address.getValue().trim();
      } else {
        emailError();
        return null;
      }
    }
    clearError($.email_address, $.email_error, Alloy.Styles.color.text.lightest, Alloy.Styles.color.text.lightest, false);
    return null;
  }








  function getNickname(city, stateCode) {
    var nickname_value = $.nickname_text_field.getValue().trim();
    return nickname_value != '' ? nickname_value : getAddressNickname(city, stateCode);
  }






  function resetToolbar() {
    textFields = [];
    textFields = $.enter_address_dynamic.getAllFormFieldsInOrder().slice();
    textFields[textFields.length - 1].addEventListener('return', onResetToolbar);
    textFields.push($.email_address);

    if ($.nickname_text_field.visible) {
      textFields.unshift($.nickname_text_field);
    }
    $.toolbar.setTextFields(textFields);
  }






  function onResetToolbar() {
    $.email_address.focus();
  }







  function hideSaveCustomerAddress(hideCancel) {
    $.nickname_text_field.hide();
    if (hideCancel) {
      $.customer_action_container.hide();
    }

    $.customer_action_container.setTop(0);
    $.nickname_text_field.setHeight(0);
    $.save_address_book_container.hide();
    $.save_address_book_container.setHeight(0);
    $.back_to_address_button.setTop(0);
  }






  function showSaveCustomerAddress() {
    $.nickname_text_field.show();
    $.customer_action_container.show();
    $.customer_action_container.setTop(5);
    $.nickname_text_field.setHeight(50);
    $.save_address_book_container.show();
    $.save_address_book_container.setHeight(Ti.UI.SIZE);
    $.back_to_address_button.setTop(20);
  }






  function onNextButtonClick() {
    logger.info(addressType + ' next button click');

    var nickname = getNickname(getCurrentAddress().city, getCurrentAddress().state_code);


    if ($.address_book_switch.getValue() && currentCustomer.addresses.nicknameExists(nickname) && originalAddressName != nickname) {
      showErrorMessage(_L('Error: ') + String.format(_L('Customer address name \'%s\' is already in use.'), nickname));
      $.nickname_text_field.focus();
      return;
    }

    if (isValid()) {
      var address = getCurrentAddress();
      closeKeyboard();
      $.trigger('addressEntered', {
        address: address,
        email: getEmailAddress(),
        isShipToStore: isShipToStore,
        isDifferentStorePickup: differentStorePickup });

    }
  }






  function nextButtonValidation() {
    $.next_button.setEnabled(isValid());
  }






  function onEmailAddressChange() {
    if ($.email_address.getVisible()) {
      $.next_button.setEnabled($.enter_address_dynamic.isValid() && email_regex.test($.email_address.value));
    }
  }







  function setEmailValue(newEmailValue) {
    emailValue = newEmailValue;
    $.email_address.setValue(emailValue);
  }






  function clearErrors() {

    $.address_error.hide();
    $.address_error.setHeight(0);
    $.address_error.setText('');
  }







  function showErrorMessage(errorMessage) {
    $.address_error.setText(errorMessage);
    $.address_error.show();
    $.address_error.setHeight(Ti.UI.SIZE);
  }






  function onBackClick() {
    logger.info(addressType + ' leaving enter address form, going back to choose an address');
    Alloy.eventDispatcher.trigger('hideAuxillaryViews');
    $.trigger('chooseAddress');
    clearErrors();
  }







  function hasAnythingChanged() {
    var result = $.enter_address_dynamic.hasAnythingChanged();
    var emailV = $.email_address.getValue() !== emailValue;
    return result || emailV;
  }






  function handleDifferentStorePickupAddress() {
    differentStorePickup = true;
    $.next_button.setTitle(_L('Create Order'));
  }






  function handleShipToStore() {
    differentStorePickup = false;
    $.next_button.setTitle(_L('Next'));
  }






  function handleShipToAddress() {
    differentStorePickup = false;
    $.next_button.setTitle(_L('Next'));
  }






  function clearPreviousSelections() {
    logger.info('clearPreviousSelections called');
    handleShipToAddress();
  }









  _.extend($, exports);
}

module.exports = Controller;