var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'checkout/components/promotionSummary';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};




  $.activeCoupons = Alloy.createCollection('couponItem');


  $.__views.promotions = Ti.UI.createView(
  { layout: "vertical", height: Ti.UI.SIZE, id: "promotions" });

  $.__views.promotions && $.addTopLevelView($.__views.promotions);
  $.__views.coupon_form = Ti.UI.createView(
  { layout: "horizontal", height: 35, top: 22, left: 22, bottom: 22, id: "coupon_form" });

  $.__views.promotions.add($.__views.coupon_form);
  $.__views.coupon = Ti.UI.createTextField(
  { padding: { left: 10 }, color: Alloy.Styles.color.text.black, width: 170, height: 35, font: Alloy.Styles.textFieldFont, borderColor: Alloy.Styles.color.border.dark, borderWidth: 1, backgroundColor: Alloy.Styles.color.background.white, autocapitalization: Ti.UI.TEXT_AUTOCAPITALIZATION_NONE, autocorrect: false, id: "coupon", accessibilityLabel: "coupon" });

  $.__views.coupon_form.add($.__views.coupon);
  $.__views.coupon_apply_button = Ti.UI.createButton(
  { left: 10, color: Alloy.Styles.buttons.secondary.color, backgroundImage: Alloy.Styles.buttons.secondary.backgroundImage, width: 110, height: 35, font: Alloy.Styles.buttonFont, titleid: "_Apply", id: "coupon_apply_button", accessibilityValue: "coupon_apply_button" });

  $.__views.coupon_form.add($.__views.coupon_apply_button);
  $.__views.instruction_view = Ti.UI.createView(
  { height: 40, id: "instruction_view" });

  $.__views.promotions.add($.__views.instruction_view);
  $.__views.instruction_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.medium, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, top: 0, font: Alloy.Styles.detailLabelFont, textid: "_Enter_Promo_Code_for_additional_discounts_", id: "instruction_label", accessibilityValue: "instruction_label" });

  $.__views.instruction_view.add($.__views.instruction_label);
  $.__views.coupon_table = Ti.UI.createTableView(
  { left: 10, top: 0, height: 340, width: 320, id: "coupon_table", separatorStyle: "transparent" });

  $.__views.promotions.add($.__views.coupon_table);
  var __alloyId56 = Alloy.Collections['$.activeCoupons'] || $.activeCoupons;function __alloyId57(e) {if (e && e.fromAdapter) {return;}var opts = __alloyId57.opts || {};var models = __alloyId56.models;var len = models.length;var rows = [];for (var i = 0; i < len; i++) {var __alloyId53 = models[i];__alloyId53.__transform = transformCoupons(__alloyId53);var __alloyId55 = Alloy.createController('checkout/components/couponLineItem', { $model: __alloyId53, __parentSymbol: __parentSymbol });
      rows.push(__alloyId55.getViewEx({ recurse: true }));
    }$.__views.coupon_table.setData(rows);};__alloyId56.on('fetch destroy change add remove reset', __alloyId57);exports.destroy = function () {__alloyId56 && __alloyId56.off('fetch destroy change add remove reset', __alloyId57);};




  _.extend($, $.__views);










  var currentBasket = Alloy.Models.basket;

  var logger = require('logging')('checkout:components:promotionSummary', getFullControllerPath($.__controllerPath));




  $.listenTo(Alloy.eventDispatcher, 'hideAuxillaryViews', onHideAuxViews);

  $.listenTo(Alloy.eventDispatcher, 'order_just_created', initCouponLineItems);




  $.coupon.addEventListener('return', handleApplyCoupon);
  $.coupon_apply_button.addEventListener('click', handleApplyCoupon);
  $.coupon_table.addEventListener('click', handleDeleteCoupon);





  $.listenTo(currentBasket, 'sync change:coupon_items', initCouponLineItems);

  $.listenTo(currentBasket, 'change:checkout_status', onCheckoutStatusChange);



  exports.deinit = deinit;









  function deinit() {
    logger.info('DEINIT called');
    $.coupon.removeEventListener('return', handleApplyCoupon);
    $.coupon_apply_button.removeEventListener('click', handleApplyCoupon);
    $.coupon_table.removeEventListener('click', handleDeleteCoupon);
    $.stopListening();
    $.destroy();
  }









  function initCouponLineItems() {
    var coupon_items = currentBasket.getCouponItems() || {};
    var clis = coupon_items;


    if (clis && clis.length > 0) {
      $.activeCoupons.reset(clis);
      $.instruction_view.hide();
      $.instruction_view.setHeight(0);
      $.coupon_table.show();
      $.coupon_table.setHeight(340);
    } else {
      $.activeCoupons.reset([]);
      $.instruction_view.show();
      $.instruction_view.setHeight('auto');
      $.coupon_table.setHeight('0');
      $.coupon_table.hide();
    }
  }






  function transformCoupons(model) {
    logger.info('transformCoupons');
    var description = currentBasket.getCouponDescription(model.getCode());
    return {
      title: 'Coupon',
      coupon_code: model.getCode(),
      description: description ? description : '' };

  }









  function handleApplyCoupon() {
    var couponCode = $.coupon.value;
    if (couponCode != '') {
      $.coupon.blur();
      if (currentBasket.hasCoupon(couponCode)) {
        notify(String.format(_L('Coupon \'%s\' is already in your cart.'), couponCode));
      } else {

        var promise = currentBasket.addCoupon(couponCode);
        Alloy.Router.showActivityIndicator(promise);
        promise.fail(function (model) {
          var message;
          var faultType = model.getFaultType();
          if (faultType === 'InvalidCouponItemException' || faultType === 'InvalidCouponCodeException') {
            message = String.format(_L('Coupon \'%s\' is invalid.'), couponCode);
          } else {
            message = model.getFaultMessage();
            if (!message) {
              message = String.format(_L('Coupon \'%s\' could not be applied.'), couponCode);
            }
          }
          notify(String.format(message, couponCode), {
            preventAutoClose: true });

        });
      }
      $.coupon.setValue('');
    }
  }






  function handleDeleteCoupon(event) {
    if (event.source.id === 'delete_coupon_button') {
      $.coupon.blur();
      var coupon = $.activeCoupons.at(event.index);
      var coupon_code = coupon.getCode();
      Alloy.Dialog.showConfirmationDialog({
        messageString: String.format(_L('Do you really want to delete this coupon?'), coupon_code),
        titleString: _L('Delete Coupon'),
        okButtonString: _L('Delete'),
        okFunction: function () {
          logger.info('removing coupon ' + coupon_code);
          var promise = currentBasket.removeCoupon(coupon_code);
          Alloy.Router.showActivityIndicator(promise);
          promise.done(function () {
            $.coupon_table.removeAllChildren();
            initCouponLineItems();
          }).fail(function () {
            notify(_L('Could not remove coupon.'), {
              preventAutoClose: true });

          });
        } });

    }
  }






  function onHideAuxViews() {
    $.coupon.blur();
  }









  function onCheckoutStatusChange() {
    if (currentBasket.getCheckoutStatus() == 'cart') {
      this.getView().show();
      this.getView().setHeight('auto');
    } else {
      this.getView().hide();
      this.getView().setHeight(0);
    }
  }









  _.extend($, exports);
}

module.exports = Controller;