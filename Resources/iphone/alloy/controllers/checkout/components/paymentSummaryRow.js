var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
		delete obj[key];
	}
	return arg;
}

function Controller() {

	require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'checkout/components/paymentSummaryRow';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};







	$.__views.payment_table = Ti.UI.createTableViewRow(
	{ id: "payment_table" });

	$.__views.payment_table && $.addTopLevelView($.__views.payment_table);
	$.__views.payment_container = Ti.UI.createView(
	{ layout: "horizontal", height: Ti.UI.SIZE, width: "100%", textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, right: 0, id: "payment_container" });

	$.__views.payment_table.add($.__views.payment_container);
	$.__views.gift_card_label = Ti.UI.createLabel(
	{ width: "50%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 20, font: Alloy.Styles.appFont, id: "gift_card_label", text: $model.__transform.payment, accessibilityValue: "gift_card_label" });

	$.__views.payment_container.add($.__views.gift_card_label);
	$.__views.gift_card_amount_label = Ti.UI.createLabel(
	{ width: "25%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT, right: 0, font: Alloy.Styles.appFont, top: 0, id: "gift_card_amount_label", text: $model.__transform.amount, accessibilityValue: "gift_card_amount_label" });

	$.__views.payment_container.add($.__views.gift_card_amount_label);
	$.__views.delete_button = Ti.UI.createButton(
	{ backgroundImage: Alloy.Styles.removeImage, right: 0, top: 0, width: "15%", height: 24, left: 5, id: "delete_button", accessibilityValue: "delete_payment_button" });

	$.__views.payment_container.add($.__views.delete_button);
	exports.destroy = function () {};




	_.extend($, $.__views);










	var currentBasket = Alloy.Models.basket;
	var logger = require('logging')('checkout:components:paymentSummaryRow', getFullControllerPath($.__controllerPath));





	$.delete_button.addEventListener('click', onDeleteClick);









	$.payment_table.deinit = function () {
		logger.info('DEINIT called');
		$.delete_button.removeEventListener('click', onDeleteClick);
		$.destroy();
	};








	function onDeleteClick() {
		if ($model.isCreditCard()) {
			Alloy.Router.showActivityIndicator(currentBasket.removeCreditCard({
				order_no: currentBasket.getOrderNumber(),
				credit_card_last_four: $model.getLastFourDigits() }));

		} else if ($model.isGiftCard()) {
			Alloy.Router.showActivityIndicator(currentBasket.removeGiftCard({
				order_no: currentBasket.getOrderNumber(),
				gift_card_last_four: $model.getLastFourDigits() }));

		}
	}





	if ($model.getCanDelete()) {
		$.delete_button.show();
		$.delete_button.setWidth(24);
		$.gift_card_amount_label.setWidth('35%');
	} else {
		$.delete_button.hide();
		$.delete_button.setWidth(0);
		$.gift_card_amount_label.setWidth('35%');
	}









	_.extend($, exports);
}

module.exports = Controller;