var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
		delete obj[key];
	}
	return arg;
}

function Controller() {

	require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'checkout/components/couponLineItem';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};







	$.__views.coupon_row = Ti.UI.createTableViewRow(
	{ height: Ti.UI.SIZE, right: 0, top: 0, width: 320, layout: "vertical", selectionStyle: Ti.UI.iOS.TableViewCellSelectionStyle.NONE, id: "coupon_row" });

	$.__views.coupon_row && $.addTopLevelView($.__views.coupon_row);
	$.__views.coupon_line_item_container = Ti.UI.createView(
	{ layout: "horizontal", left: 0, top: 0, height: 65, width: 320, id: "coupon_line_item_container" });

	$.__views.coupon_row.add($.__views.coupon_line_item_container);
	$.__views.image_column = Ti.UI.createView(
	{ left: 0, top: 0, height: 40, width: 60, layout: "horizontal", id: "image_column" });

	$.__views.coupon_line_item_container.add($.__views.image_column);
	$.__views.delete_coupon_button = Ti.UI.createButton(
	{ backgroundImage: Alloy.Styles.closeButtonGrayImage, top: 2, left: 10, width: 32, height: 32, id: "delete_coupon_button", accessibilityValue: "delete_coupon_button" });

	$.__views.image_column.add($.__views.delete_coupon_button);
	$.__views.right_column = Ti.UI.createView(
	{ left: 0, top: 0, width: 260, layout: "vertical", id: "right_column" });

	$.__views.coupon_line_item_container.add($.__views.right_column);
	$.__views.title_code_container = Ti.UI.createView(
	{ layout: "horizontal", height: Ti.UI.SIZE, id: "title_code_container" });

	$.__views.right_column.add($.__views.title_code_container);
	$.__views.title_label = Ti.UI.createLabel(
	{ width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, top: 0, font: Alloy.Styles.detailLabelFont, text: $model.__transform.title, id: "title_label", accessibilityValue: "title_label" });

	$.__views.title_code_container.add($.__views.title_label);
	$.__views.coupon_code_label = Ti.UI.createLabel(
	{ width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, top: 0, left: 5, font: Alloy.Styles.detailValueFont, text: $model.__transform.coupon_code, id: "coupon_code_label", accessibilityValue: "coupon_code_label" });

	$.__views.title_code_container.add($.__views.coupon_code_label);
	$.__views.description_container = Ti.UI.createView(
	{ layout: "horizontal", height: Ti.UI.SIZE, id: "description_container" });

	$.__views.right_column.add($.__views.description_container);
	$.__views.description_label = Ti.UI.createLabel(
	{ width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, top: 0, left: 5, font: Alloy.Styles.detailValueFont, text: $model.__transform.description, id: "description_label", accessibilityValue: "description_label" });

	$.__views.description_container.add($.__views.description_label);
	exports.destroy = function () {};




	_.extend($, $.__views);












	_.extend($, exports);
}

module.exports = Controller;