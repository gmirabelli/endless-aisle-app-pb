var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'checkout/components/orderTotalSummary';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};




  $.basket = Alloy.createModel('baskets');


  $.__views.order_totals_container = Ti.UI.createView(
  { layout: "vertical", top: 10, height: Ti.UI.SIZE, id: "order_totals_container" });

  $.__views.order_totals_container && $.addTopLevelView($.__views.order_totals_container);
  $.__views.order_subtotal_details = Ti.UI.createView(
  { layout: "horizontal", height: Ti.UI.SIZE, left: 20, id: "order_subtotal_details" });

  $.__views.order_totals_container.add($.__views.order_subtotal_details);
  $.__views.subtotal_label = Ti.UI.createLabel(
  { width: "60%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.sectionTitleFont, textid: "_Subtotal", accessibilityValue: "subtotal_label", id: "subtotal_label" });

  $.__views.order_subtotal_details.add($.__views.subtotal_label);
  $.__views.subtotal_value = Ti.UI.createLabel(
  { width: "35%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT, font: Alloy.Styles.sectionTitleFont, right: 5, accessibilityValue: "subtotal_value", id: "subtotal_value" });

  $.__views.order_subtotal_details.add($.__views.subtotal_value);
  $.__views.order_discount_details = Ti.UI.createView(
  { layout: "horizontal", height: Ti.UI.SIZE, left: 20, id: "order_discount_details" });

  $.__views.order_totals_container.add($.__views.order_discount_details);
  $.__views.discounts_label = Ti.UI.createLabel(
  { width: "60%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.sectionTitleFont, textid: "_Discounts", id: "discounts_label", accessibilityValue: "discounts_label" });

  $.__views.order_discount_details.add($.__views.discounts_label);
  $.__views.discounts_value = Ti.UI.createLabel(
  { width: "35%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.medium, textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT, font: Alloy.Styles.sectionTitleFont, right: 5, id: "discounts_value", accessibilityValue: "discounts_value" });

  $.__views.order_discount_details.add($.__views.discounts_value);
  $.__views.shipping_details = Ti.UI.createView(
  { layout: "horizontal", height: Ti.UI.SIZE, left: 20, id: "shipping_details" });

  $.__views.order_totals_container.add($.__views.shipping_details);
  $.__views.shipping_label = Ti.UI.createLabel(
  { width: "60%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.sectionTitleFont, textid: "_Shipping", accessibilityValue: "shipping_label", id: "shipping_label" });

  $.__views.shipping_details.add($.__views.shipping_label);
  $.__views.shipping_total = Ti.UI.createLabel(
  { width: "35%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT, font: Alloy.Styles.sectionTitleFont, right: 5, id: "shipping_total", accessibilityValue: "shipping_total" });

  $.__views.shipping_details.add($.__views.shipping_total);
  $.__views.shipping_discount_details = Ti.UI.createView(
  { layout: "horizontal", height: Ti.UI.SIZE, left: 20, id: "shipping_discount_details" });

  $.__views.order_totals_container.add($.__views.shipping_discount_details);
  $.__views.shipping_discount_label = Ti.UI.createLabel(
  { width: "60%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.sectionTitleFont, textid: "_Shipping_Discount", id: "shipping_discount_label", accessibilityValue: "shipping_discount_label" });

  $.__views.shipping_discount_details.add($.__views.shipping_discount_label);
  $.__views.shipping_discount = Ti.UI.createLabel(
  { width: "35%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.medium, textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT, font: Alloy.Styles.sectionTitleFont, right: 5, id: "shipping_discount", accessibilityValue: "shipping_discount" });

  $.__views.shipping_discount_details.add($.__views.shipping_discount);
  $.__views.sales_tax_details = Ti.UI.createView(
  { layout: "horizontal", height: Ti.UI.SIZE, left: 20, id: "sales_tax_details" });

  $.__views.order_totals_container.add($.__views.sales_tax_details);
  $.__views.sales_tax_label = Ti.UI.createLabel(
  { width: "60%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.sectionTitleFont, textid: "_Sales_Tax", accessibilityValue: "sales_tax_label", id: "sales_tax_label" });

  $.__views.sales_tax_details.add($.__views.sales_tax_label);
  $.__views.sales_tax_total = Ti.UI.createLabel(
  { width: "35%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT, font: Alloy.Styles.sectionTitleFont, right: 5, accessibilityValue: "sales_tax_total", id: "sales_tax_total" });

  $.__views.sales_tax_details.add($.__views.sales_tax_total);
  $.__views.order_total_details = Ti.UI.createView(
  { layout: "horizontal", height: 40, backgroundColor: Alloy.Styles.color.background.dark, top: 15, id: "order_total_details" });

  $.__views.order_totals_container.add($.__views.order_total_details);
  $.__views.order_total_label = Ti.UI.createLabel(
  { width: "60%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.white, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.calloutCopyFont, left: 20, top: 7, textid: "_Order_Total", id: "order_total_label", accessibilityValue: "order_total_label" });

  $.__views.order_total_details.add($.__views.order_total_label);
  $.__views.order_total_value = Ti.UI.createLabel(
  { width: "30%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.white, textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT, font: Alloy.Styles.calloutCopyFont, right: 5, top: 7, accessibilityValue: "order_total_value", id: "order_total_value" });

  $.__views.order_total_details.add($.__views.order_total_value);
  exports.destroy = function () {};




  _.extend($, $.__views);









  var args = arguments[0] || {};

  var currentBasket = Alloy.Models.basket;
  var EAUtils = require('EAUtils');
  var toCurrency = require('EAUtils').toCurrency;
  var zero = require('EAUtils').zero;
  var logger = require('logging')('checkout:components:orderTotalSummary', getFullControllerPath($.__controllerPath));
  var oldOrderTotal,
  oldShippingTotal,
  oldProductSubtotal,
  oldOrderDiscount,
  oldTaxTotal,
  oldShippingDiscount = null;





  $.listenTo(Alloy.eventDispatcher, 'order_just_created', function (event) {
    logger.info('Showing order totals for the newly fufilled order');
    oldOrderTotal = null;
    oldShippingTotal = null;
    oldProductSubtotal = null;
    oldOrderDiscount = null;
    oldTaxTotal = null;
    oldShippingDiscount = null;
    render(event.toJSON());
  });




  $.listenTo(currentBasket, 'basket_sync change:order_price_adjustments basket_shipping_methods', function (type, model, something, options) {
    logger.info('Event fired: ' + type);
    render(currentBasket.toJSON(), true);
  });

  $.listenTo(currentBasket, 'change:checkout_status', function () {
    if (currentBasket.getCheckoutStatus() == 'cart') {
      render(currentBasket.toJSON(), true);
    }
  });

  if (EAUtils.isLatinBasedLanguage()) {
    $.order_total_label.setFont(Alloy.Styles.detailTextFont);
  } else {
    $.order_total_label.setFont(Alloy.Styles.sectionTitleFont);
  }




  exports.render = render;
  exports.deinit = deinit;











  function render(basket, hasShippingMethods) {
    logger.info('render called');
    $.basket.clear();
    $.basket.set(basket);

    $.basket.shippingMethods = currentBasket.shippingMethods;
    setProductSubtotal();
    setOrderDiscount();
    setOrderTotal();
    setShippingDiscount(hasShippingMethods);
    setShippingTotal(hasShippingMethods);
    setTaxTotal();
  }






  function deinit() {
    logger.info('DEINIT called');

    $.stopListening();
    $.destroy();
  }









  function setProductSubtotal() {
    var productSubtotal = zero(parseFloat($.basket.getProductSubtotal()));
    if (oldProductSubtotal != productSubtotal) {
      $.subtotal_value.setText(productSubtotal != 0 ? toCurrency(productSubtotal, $.basket.getCurrencyCode()) : _L('No Value'));
      oldProductSubtotal = productSubtotal;
    }
  }






  function setOrderDiscount() {
    var orderDiscount = $.basket.calculateOrderPriceAdjustments();
    if (oldOrderDiscount != orderDiscount) {
      $.discounts_value.setText(orderDiscount != 0 ? toCurrency(orderDiscount, $.basket.getCurrencyCode()) : _L('No Value'));
      oldOrderDiscount = orderDiscount;
    }
  }







  function setShippingTotal(hasShippingMethods) {
    var shippingMethod = $.basket.getShippingMethod();
    var shippingTotal = zero(parseFloat($.basket.getShippingTotal()));
    if (shippingMethod && $.basket.shippingMethods && $.basket.hasProductItems()) {
      shippingTotal = $.basket.getShippingMethodPrice(shippingMethod.getID());
    }

    if (!$.basket.shippingMethods && hasShippingMethods) {
      shippingTotal = 0;
    }
    if (oldShippingTotal != shippingTotal) {
      $.shipping_total.setText(shippingTotal != 0 ? toCurrency(shippingTotal, $.basket.getCurrencyCode()) : _L('No Value'));
      oldShippingTotal = shippingTotal;
    }
  }







  function setShippingDiscount(hasShippingMethods) {
    var shippingMethod = $.basket.getShippingMethod();
    var shippingCost = zero(parseFloat($.basket.getShippingTotal()));
    if (shippingMethod && $.basket.shippingMethods && $.basket.hasProductItems()) {
      shippingCost = $.basket.getShippingMethodPrice(shippingMethod.getID());
    }

    if (!$.basket.shippingMethods && hasShippingMethods) {
      shippingCost = 0;
      shippingDiscount = 0;
    } else {

      if ($.basket.getShippingTotalBasePrice()) {
        shippingCost = $.basket.getShippingTotalBasePrice();
      }
      shippingDiscount = shippingMethod ? $.basket.getShippingTotal() - shippingCost : 0;
    }
    if (oldShippingDiscount != shippingDiscount) {
      $.shipping_discount.setText(shippingDiscount != 0 ? toCurrency(shippingDiscount, $.basket.getCurrencyCode()) : _L('No Value'));
      oldShippingDiscount = shippingDiscount;
    }
  }





  function setTaxTotal() {
    var taxTotal = zero(parseFloat($.basket.getTaxTotal()));
    if (oldTaxTotal != taxTotal) {
      $.sales_tax_total.setText(taxTotal != 0 ? toCurrency(taxTotal, $.basket.getCurrencyCode()) : _L('No Value'));
      oldTaxTotal = taxTotal;
    }
  }





  function setOrderTotal() {
    var orderTotal = zero(parseFloat($.basket.getOrderTotal() || $.basket.getProductTotal()));
    if (oldOrderTotal != orderTotal) {
      $.order_total_value.setText(orderTotal != 0 ? toCurrency(orderTotal, $.basket.getCurrencyCode()) : _L('No Value'));
      oldOrderTotal = orderTotal;
    }
  }









  _.extend($, exports);
}

module.exports = Controller;