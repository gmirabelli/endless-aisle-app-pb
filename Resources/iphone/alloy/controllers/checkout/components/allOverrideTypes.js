var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'checkout/components/allOverrideTypes';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.all_override_types = Ti.UI.createView(
  { layout: "vertical", id: "all_override_types" });

  $.__views.all_override_types && $.addTopLevelView($.__views.all_override_types);
  $.__views.percent_off_container = Ti.UI.createView(
  { layout: "horizontal", width: "100%", height: 60, id: "percent_off_container" });

  $.__views.all_override_types.add($.__views.percent_off_container);
  $.__views.percent_radio_image = Ti.UI.createImageView(
  { image: Alloy.Styles.radioButtonOffImage, id: "percent_radio_image", accessibilityValue: "percent_radio_image" });

  $.__views.percent_off_container.add($.__views.percent_radio_image);
  $.__views.percent_off_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.radioButtonFont, textid: "_Percentage_Off", accessibilityValue: "percent_off_radio_label", id: "percent_off_label" });

  $.__views.percent_off_container.add($.__views.percent_off_label);
  $.__views.amount_off_container = Ti.UI.createView(
  { layout: "horizontal", width: "100%", height: 60, id: "amount_off_container" });

  $.__views.all_override_types.add($.__views.amount_off_container);
  $.__views.amount_radio_image = Ti.UI.createImageView(
  { image: Alloy.Styles.radioButtonOffImage, id: "amount_radio_image", accessibilityValue: "amount_radio_image" });

  $.__views.amount_off_container.add($.__views.amount_radio_image);
  $.__views.amount_off_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.radioButtonFont, textid: "_Amount_Off", accessibilityValue: "amount_off_radio_label", id: "amount_off_label" });

  $.__views.amount_off_container.add($.__views.amount_off_label);
  $.__views.fixed_price_container = Ti.UI.createView(
  { layout: "horizontal", width: "100%", height: 60, id: "fixed_price_container" });

  $.__views.all_override_types.add($.__views.fixed_price_container);
  $.__views.fixed_price_radio_image = Ti.UI.createImageView(
  { image: Alloy.Styles.radioButtonOffImage, id: "fixed_price_radio_image", accessibilityValue: "fixed_price_radio_image" });

  $.__views.fixed_price_container.add($.__views.fixed_price_radio_image);
  $.__views.fixed_price_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.radioButtonFont, textid: "_Fixed_Price", accessibilityValue: "fixed_price_radio_label", id: "fixed_price_label" });

  $.__views.fixed_price_container.add($.__views.fixed_price_label);
  $.__views.none_container = Ti.UI.createView(
  { layout: "horizontal", width: "100%", height: 60, id: "none_container" });

  $.__views.all_override_types.add($.__views.none_container);
  $.__views.none_radio_image = Ti.UI.createImageView(
  { image: Alloy.Styles.radioButtonOnImage, id: "none_radio_image", accessibilityValue: "none_radio_image" });

  $.__views.none_container.add($.__views.none_radio_image);
  $.__views.none_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.radioButtonFont, textid: "_None", id: "none_label", accessibilityValue: "none_radio_label" });

  $.__views.none_container.add($.__views.none_label);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var selectedType = 'none';
  var isValid = true;





  $.none_container.addEventListener('click', onNoneClick);


  $.amount_off_container.addEventListener('click', onAmountOffClick);


  $.percent_off_container.addEventListener('click', onPercentOffClick);


  $.fixed_price_container.addEventListener('click', onFixedPriceClick);




  exports.deinit = deinit;
  exports.hideOverrideFixedPrice = hideOverrideFixedPrice;
  exports.hideOverrideByPercent = hideOverrideByPercent;
  exports.hideOverrideByAmount = hideOverrideByAmount;
  exports.setSelection = setSelection;









  function deinit() {
    $.none_container.removeEventListener('click', onNoneClick);
    $.amount_off_container.removeEventListener('click', onAmountOffClick);
    $.percent_off_container.removeEventListener('click', onPercentOffClick);
    $.fixed_price_container.removeEventListener('click', onFixedPriceClick);
    $.stopListening();
    $.destroy();
  }










  function hideOverrideFixedPrice(hide) {
    if (hide) {
      $.fixed_price_container.setHeight(0);
      $.fixed_price_container.hide();
    } else {
      $.fixed_price_container.setHeight(60);
      $.fixed_price_container.show();
    }
  }







  function hideOverrideByPercent(hide) {
    if (hide) {
      $.percent_off_container.setHeight(0);
      $.percent_off_container.hide();
    } else {
      $.percent_off_container.setHeight(60);
      $.percent_off_container.show();
    }
  }







  function hideOverrideByAmount(hide) {
    if (hide) {
      $.amount_off_container.setHeight(0);
      $.amount_off_container.hide();
    } else {
      $.amount_off_container.setHeight(60);
      $.amount_off_container.show();
    }
  }








  function setSelection(type, amount) {
    switch (type) {
      case 'percent':
        select($.percent_off_container, [$.amount_off_container, $.fixed_price_container]);
        break;
      case 'amount':
        select($.amount_off_container, [$.percent_off_container, $.fixed_price_container]);
        break;
      case 'fixedPrice':
        select($.fixed_price_container, [$.percent_off_container, $.amount_off_container]);
        break;}

    selectNone(type === 'none');
  }








  function select(selected, unselected) {
    if (selected) {
      selected.getChildren()[0].image = Alloy.Styles.radioButtonOnImage;
    }
    _.each(unselected, function (one) {
      one.getChildren()[0].image = Alloy.Styles.radioButtonOffImage;
    });
  }







  function selectNone(selected) {
    $.none_container.getChildren()[0].setImage(selected ? Alloy.Styles.radioButtonOnImage : Alloy.Styles.radioButtonOffImage);
    if (selected) {
      select(null, [$.fixed_price_container, $.percent_off_container, $.amount_off_container]);
      selectedType = 'none';
      isValid = true;
    }
  }









  function onNoneClick() {
    selectNone(true);
    select(null, [$.percent_off_container, $.amount_off_container, $.fixed_price_container]);

    $.trigger('overridetype', {
      valid: true,
      overrideType: 'none' });

  }






  function onAmountOffClick() {
    select($.amount_off_container, [$.percent_off_container, $.fixed_price_container]);
    selectNone(false);

    $.trigger('overridetype', {
      overrideType: 'amount' });

  }






  function onPercentOffClick() {
    select($.percent_off_container, [$.amount_off_container, $.fixed_price_container]);
    selectNone(false);

    $.trigger('overridetype', {
      overrideType: 'percent' });

  }






  function onFixedPriceClick() {
    select($.fixed_price_container, [$.percent_off_container, $.amount_off_container]);
    selectNone(false);

    $.trigger('overridetype', {
      overrideType: 'fixedPrice' });

  }









  _.extend($, exports);
}

module.exports = Controller;