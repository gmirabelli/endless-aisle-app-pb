var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
		delete obj[key];
	}
	return arg;
}

function Controller() {

	require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'checkout/shippingMethod/singleShippingMethod';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};







	$.__views.singleShippingMethod = Ti.UI.createTableViewRow(
	{ selectionStyle: Ti.UI.iOS.TableViewCellSelectionStyle.NONE, method_id: $model.__transform.method_id, id: "singleShippingMethod" });

	$.__views.singleShippingMethod && $.addTopLevelView($.__views.singleShippingMethod);
	$.__views.single_shipping_method = Ti.UI.createView(
	{ layout: "vertical", height: 90, id: "single_shipping_method" });

	$.__views.singleShippingMethod.add($.__views.single_shipping_method);
	$.__views.standard_shipping_container = Ti.UI.createView(
	{ layout: "horizontal", width: "100%", height: 90, id: "standard_shipping_container" });

	$.__views.single_shipping_method.add($.__views.standard_shipping_container);
	$.__views.radio_button = Ti.UI.createImageView(
	{ image: Alloy.Styles.radioButtonOffImage, id: "radio_button", accessibilityValue: "radio_button" });

	$.__views.standard_shipping_container.add($.__views.radio_button);
	$.__views.method_container = Ti.UI.createView(
	{ layout: "vertical", top: 25, id: "method_container" });

	$.__views.standard_shipping_container.add($.__views.method_container);
	$.__views.method_name_and_price = Ti.UI.createView(
	{ layout: "horizontal", height: Ti.UI.SIZE, id: "method_name_and_price" });

	$.__views.method_container.add($.__views.method_name_and_price);
	$.__views.method_name = Ti.UI.createLabel(
	{ width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.radioButtonLabel, left: 10, id: "method_name", text: $model.__transform.name, accessibilityValue: "method_name" });

	$.__views.method_name_and_price.add($.__views.method_name);
	$.__views.method_price = Ti.UI.createLabel(
	{ width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.appFont, id: "method_price", text: $model.__transform.price, accessibilityValue: "method_price" });

	$.__views.method_name_and_price.add($.__views.method_price);
	$.__views.method_description = Ti.UI.createLabel(
	{ width: 500, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.appFont, left: 7, id: "method_description", text: $model.__transform.description, accessibilityValue: "method_description" });

	$.__views.method_container.add($.__views.method_description);
	exports.destroy = function () {};




	_.extend($, $.__views);











	$.singleShippingMethod.select = function (selected) {
		selected ? $.radio_button.setImage(Alloy.Styles.radioButtonOnImage) : $.radio_button.setImage(Alloy.Styles.radioButtonOffImage);
	};





	var description = $model.getDescription();
	if (description && description.length > 60) {
		$.single_shipping_method.setHeight(130);
		$.standard_shipping_container.setHeight(130);
		$.method_container.setTop(50);
	}









	_.extend($, exports);
}

module.exports = Controller;