var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'checkout/shippingMethod/index';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};




  $.shippingMethods = Alloy.createCollection('availableShippingMethod');


  $.__views.shipping_methods = Ti.UI.createView(
  { layout: "vertical", disableBounce: true, contentWidth: Ti.UI.SIZE, id: "shipping_methods" });

  $.__views.shipping_methods && $.addTopLevelView($.__views.shipping_methods);
  $.__views.inner_container = Ti.UI.createScrollView(
  { width: "100%", layout: "vertical", left: 0, height: Ti.UI.SIZE, horizontalBounce: false, id: "inner_container" });

  $.__views.shipping_methods.add($.__views.inner_container);
  $.__views.shipping_methods_container = Ti.UI.createView(
  { layout: "vertical", width: 580, left: 100, top: 15, height: 350, id: "shipping_methods_container" });

  $.__views.inner_container.add($.__views.shipping_methods_container);
  $.__views.shipping_method_table = Ti.UI.createTableView(
  { separatorStyle: "transparent", top: 5, id: "shipping_method_table" });

  $.__views.shipping_methods_container.add($.__views.shipping_method_table);
  var __alloyId69 = Alloy.Collections['$.shippingMethods'] || $.shippingMethods;function __alloyId70(e) {if (e && e.fromAdapter) {return;}var opts = __alloyId70.opts || {};var models = __alloyId69.models;var len = models.length;var rows = [];for (var i = 0; i < len; i++) {var __alloyId66 = models[i];__alloyId66.__transform = transformShippingMethod(__alloyId66);var __alloyId68 = Alloy.createController('checkout/shippingMethod/singleShippingMethod', { $model: __alloyId66 });
      rows.push(__alloyId68.getViewEx({ recurse: true }));
    }$.__views.shipping_method_table.setData(rows);};__alloyId69.on('fetch destroy change add remove reset', __alloyId70);$.__views.gift_container = Ti.UI.createView(
  { layout: "horizontal", left: 100, height: 40, top: 10, width: 580, id: "gift_container" });

  $.__views.inner_container.add($.__views.gift_container);
  $.__views.gift_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.appFont, left: 0, textid: "_Is_this_a_gift_", accessibilityLabel: "gift_label", top: 0, id: "gift_label", accessibilityValue: "gift_label" });

  $.__views.gift_container.add($.__views.gift_label);
  $.__views.gift_switch = Ti.UI.createSwitch(
  { width: 85, value: false, top: 0, id: "gift_switch", accessibilityLabel: "gift_switch" });

  $.__views.gift_container.add($.__views.gift_switch);
  $.__views.message_container = Ti.UI.createView(
  { layout: "vertical", visible: false, left: 100, top: 0, height: 105, width: 580, id: "message_container" });

  $.__views.inner_container.add($.__views.message_container);
  $.__views.gift_text = Ti.UI.createTextArea(
  { width: 300, height: 80, color: Alloy.Styles.color.text.dark, borderColor: Alloy.Styles.color.border.light, borderWidth: 1, font: Alloy.Styles.detailTextFont, padding: { left: 8 }, left: 0, id: "gift_text", accessibilityLabel: "gift_text" });

  $.__views.message_container.add($.__views.gift_text);
  $.__views.character_left_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.medium, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailTextFont, left: 0, top: 4, id: "character_left_label", accessibilityValue: "character_left_label" });

  $.__views.message_container.add($.__views.character_left_label);
  $.__views.button_container = Ti.UI.createView(
  { layout: "horizontal", left: 100, top: 10, height: 80, id: "button_container" });

  $.__views.shipping_methods.add($.__views.button_container);
  $.__views.override_button = Ti.UI.createButton(
  { backgroundImage: Alloy.Styles.secondaryButtonImage, color: Alloy.Styles.buttons.secondary.color, width: 228, height: 42, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, top: 0, id: "override_button", titleid: "_Price_Override", accessibilityValue: "shipping_override_button", visible: false });

  $.__views.button_container.add($.__views.override_button);
  $.__views.create_order_container = Ti.UI.createView(
  { layout: "vertical", left: 26, top: 0, id: "create_order_container" });

  $.__views.button_container.add($.__views.create_order_container);
  $.__views.create_order_button = Ti.UI.createButton(
  { backgroundImage: Alloy.Styles.primaryButtonImage, backgroundDisabledColor: Alloy.Styles.color.background.medium, color: Alloy.Styles.buttons.primary.color, width: 228, height: 42, left: 0, id: "create_order_button", titleid: "_Create_Order", accessibilityValue: "create_order_button" });

  $.__views.create_order_container.add($.__views.create_order_button);
  $.__views.create_order_label_container = Ti.UI.createView(
  { top: 5, left: 0, layout: "vertical", width: 228, id: "create_order_label_container" });

  $.__views.create_order_container.add($.__views.create_order_label_container);
  $.__views.reserve_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.medium, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.lineItemFont, textid: "_Selecting_Create_Order_will_reserve_your_inventory", id: "reserve_label", accessibilityValue: "create_order_label_1" });

  $.__views.create_order_label_container.add($.__views.reserve_label);
  $.__views.price_override_summary = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.blue, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailLabelFont, textid: "_A_shipping_override_has_been_applied", top: 0, id: "price_override_summary", visible: false, accessibilityValue: "price_override_summary" });

  $.__views.shipping_methods.add($.__views.price_override_summary);
  exports.destroy = function () {__alloyId69 && __alloyId69.off('fetch destroy change add remove reset', __alloyId70);};




  _.extend($, $.__views);










  var currentBasket = Alloy.Models.basket;
  var currentAssociate = Alloy.Models.associate;
  var currentCustomer = Alloy.Models.customer;

  var selectedShippingOverride = null;
  var removeAllViews = require('EAUtils').removeAllViews;
  var toCurrency = require('EAUtils').toCurrency;
  var zero = require('EAUtils').zero;
  var logger = require('logging')('checkout:shippingMethod:index', getFullControllerPath($.__controllerPath));

  var shippingMethods = [];
  var methodsToRows = {};
  var methodsToCount = {};
  var shippingMethodsById = {};
  var selectedShippingMethod = null;
  var lastSelectedIndex = -1;
  var shippingOverrideAllowed = Alloy.CFG.overrides.shipping_price_overrides;






  $.listenTo(Alloy.eventDispatcher, 'order_just_created', function () {
    selectedShippingOverride = null;
    turnOffGiftSwitch();
  });

  $.listenTo(Alloy.eventDispatcher, 'associate_logout', turnOffGiftSwitch);

  $.listenTo(Alloy.eventDispatcher, 'cart_cleared', turnOffGiftSwitch);

  $.listenTo(Alloy.eventDispatcher, 'configurations:unload', function () {
    if (isKioskMode() && shippingOverrideAllowed) {
      $.stopListening(Alloy.eventDispatcher, 'kiosk:manager_login_change', handleKioskManagerLogin);
    }
  });

  $.listenTo(Alloy.eventDispatcher, 'configurations:postlogin', function () {

    shippingOverrideAllowed = Alloy.CFG.overrides.shipping_price_overrides;

    if (shippingOverrideAllowed && !isKioskMode()) {
      displayOverrideButton(true);
    } else {
      displayOverrideButton(false);
    }


    $.shipping_methods.removeEventListener('doubletap', onDoubleTap);
    if (!shippingOverrideAllowed && !isKioskMode()) {
      $.shipping_methods.addEventListener('doubletap', onDoubleTap);
    }

    if (!Alloy.CFG.allow_gift_message) {
      $.gift_container.hide();
    } else {
      $.gift_container.show();
    }

    if (isKioskMode() && shippingOverrideAllowed) {
      $.listenTo(Alloy.eventDispatcher, 'kiosk:manager_login_change', handleKioskManagerLogin);
    }
  });




  $.create_order_button.addEventListener('click', onCreateOrderButton);

  $.override_button.addEventListener('click', onOverrideButton);

  $.shipping_method_table.addEventListener('click', onShippingMethodsTable);

  $.gift_switch.addEventListener('change', giftSwitchChange);

  $.gift_text.addEventListener('change', setCharacterLeftText);






  $.listenTo(currentBasket, 'change:shipments reset:shipments', function (type, model, something, options) {
    logger.info('basket event fired ' + type);
    if (currentBasket.getShippingAddress()) {
      updateOverride();
    }
    if (currentBasket.getShippingMethod()) {
      $.create_order_button.setEnabled(true);
    } else {
      $.create_order_button.setEnabled(false);
    }
  });


  $.listenTo(currentCustomer, 'change:login', function () {
    if (!currentCustomer.isLoggedIn()) {
      turnOffGiftSwitch();
    }
  });




  exports.render = render;
  exports.deinit = deinit;
  exports.chooseShippingMethod = chooseShippingMethod;










  function render() {

    if (!shippingOverrideAllowed && !isKioskMode()) {
      displayOverrideButton(false);
    }
    if (currentBasket.getIsGift()) {
      $.gift_switch.setValue(true);
      $.message_container.show();
      $.gift_text.setValue(currentBasket.getGiftMessage());
    } else {
      turnOffGiftSwitch();
      $.message_container.hide();
      $.gift_text.setValue('');
    }
    var deferred = _.Deferred();

    if (!currentBasket.getShipToStore()) {
      var currentShippingMethod = currentBasket.getShippingMethod();
      if (currentShippingMethod && currentShippingMethod.hasPriceOverride() && currentShippingMethod.getPriceOverrideReasonCode() === _L('Free Shipping to the Store')) {
        removeOverride().always(function () {
          drawShippingMethods().always(function () {
            enableOverrideButton();
            deferred.resolve();
          });
        });
      } else {
        drawShippingMethods().always(function () {
          enableOverrideButton();
          deferred.resolve();
        });
      }
    } else {
      drawShippingMethods().always(function () {
        enableOverrideButton();
        deferred.resolve();
      });
    }
    return deferred.promise();
  }







  function deinit() {
    $.create_order_button.removeEventListener('click', onCreateOrderButton);
    $.override_button.removeEventListener('click', onOverrideButton);
    $.shipping_method_table.removeEventListener('click', onShippingMethodsTable);
    $.gift_switch.removeEventListener('change', giftSwitchChange);
    $.gift_text.removeEventListener('change', setCharacterLeftText);
    $.stopListening();
    $.destroy();
  }










  function chooseShippingMethod(index) {
    var children = $.shipping_method_table.getSections()[0].getRows();
    _.each(children, function (child, i) {
      child.select.call(this, i === index);
    });
    selectedShippingMethod = shippingMethods[index];
    lastSelectedIndex = index;

    setShippingMethod();
  }






  function updateOverride() {
    var currentShippingMethod = currentBasket.getShippingMethod();
    if (currentShippingMethod && currentShippingMethod.hasPriceOverride()) {
      selectedShippingOverride = {
        price_override_type: currentShippingMethod.getPriceOverrideType(),
        price_override_value: currentShippingMethod.getPriceOverrideValue(),
        price_override_reason_code: currentShippingMethod.getPriceOverrideReasonCode(),
        manager_employee_id: currentShippingMethod.getPriceOverrideManagerId() };

    } else {
      selectedShippingOverride = null;
    }
    enableOverrideButton();
  }







  function drawShippingMethods() {
    var promise = currentBasket.getShippingMethods({
      c_employee_id: Alloy.Models.associate.getEmployeeId() });

    promise.done(function () {
      var methods = currentBasket.getAvailableShippingMethods();
      var filteredShippingMethods = [];
      filteredShippingMethods = _.filter(methods, function (shippingMethod) {
        if (_.contains(Alloy.CFG.ship_to_store.store_pickup_ids, shippingMethod.getID())) {
          return false;
        } else {
          return true;
        }
      });
      if (filteredShippingMethods && filteredShippingMethods.length > 0) {

        $.shippingMethods.once('reset', function () {
          selectMethod(filteredShippingMethods);
        });
        $.shippingMethods.reset(filteredShippingMethods);
      } else {
        $.shippingMethods.reset([]);
      }
    });
    return promise;
  }







  function selectMethod(methods) {
    shippingMethods = methods;
    methodsToRows = {};
    methodsToCount = {};
    var chosenMethod = null;


    if ($.shipping_method_table.getSections().length > 0) {
      var children = $.shipping_method_table.getSections()[0].getRows();
      if (children) {
        _.each(children, function (child, index) {
          methodsToRows[child.method_id] = child;
          methodsToCount[child.method_id] = index;
        });
      }
    }


    _.each(methods, function (method) {
      shippingMethodsById[method.getID()] = method;
    });


    if (currentBasket.getShippingMethod() && !_.contains(Alloy.CFG.ship_to_store.store_pickup_ids, currentBasket.getShippingMethod().getID())) {
      chosenMethod = currentBasket.getShippingMethod();
      updateFromSelectedShippingMethod();
    } else {

      preselectShippingMethod();
    }
  }







  function transformShippingMethod(shipment) {
    var price = toCurrency(shipment.getBasePrice() - 0);
    if (shipment.getSurcharge()) {
      price = String.format(_L('%s + %s Surcharge'), price, toCurrency(shipment.getSurcharge()));
    }

    var desc = shipment.getDescription();

    desc = _.isUndefined(desc) ? '' : '(' + desc + ')';

    return {
      name: String.format(_L('%s: '), shipment.getName()),
      description: desc,
      price: price,
      method_id: shipment.getID() };

  }






  function onCreateOrderButton() {
    logger.info('create order button clicked');

    var promise = setGiftMessage();
    Alloy.Router.showActivityIndicator(promise);
    promise.done(function () {
      if (currentBasket.canEnableCheckout()) {
        currentBasket.setCheckoutStatus(currentBasket.getNextCheckoutState());
      } else {
        currentBasket.setCheckoutStatus('cart');
      }
    });
  }





  function onOverrideButton() {
    logger.info('shipping override button clicked, firing \'shippingOverride\' event');
    $.trigger('shippingOverride', {
      selectedShippingMethod: selectedShippingMethod,
      selectedShippingOverride: selectedShippingOverride });

  }







  function onShippingMethodsTable(event) {
    if (event.index != lastSelectedIndex) {

      if (currentBasket.hasShippingPriceOverride()) {
        removeShippingOverride(event.index);
      } else {

        chooseShippingMethod(event.index);
      }
    }
  }






  function updateFromSelectedShippingMethod() {
    var currentShippingMethod = currentBasket.getShippingMethod();

    if (currentShippingMethod) {
      var id = currentShippingMethod.getID();
      if (id && methodsToRows[id]) {
        methodsToRows[id].select.call(this, true);
        lastSelectedIndex = methodsToCount[id];
        selectedShippingMethod = shippingMethodsById[id];
        $.create_order_button.setEnabled(true);
        $.override_button.setEnabled(true);
        updateOverride();
        if (freeShippingToStore()) {
          var deferred = new _.Deferred();
          Alloy.Router.showActivityIndicator(deferred);
          setFreeShippingOverride(deferred);
        }
        Alloy.eventDispatcher.trigger('shipping_method:selected');
      } else {

        preselectShippingMethod();
      }
    }
  }







  function preselectShippingMethod() {
    var foundMethod = false;

    if ($.shipping_method_table.getSections().length > 0) {
      var children = $.shipping_method_table.getSections()[0].getRows();
      if (children) {
        var matchingChild = _.find(children, function (child, index) {
          var methodId = child.method_id;
          var shippingMethod = shippingMethodsById[methodId];
          if (shippingMethod.getDefaultMethod() == true && shippingMethod.getID() == methodId) {
            lastSelectedIndex = index;
            return true;
          }
          return false;
        });

        if (matchingChild) {
          matchingChild.select.call(this, true);
          selectedShippingMethod = shippingMethodsById[matchingChild.method_id];
          foundMethod = true;
          setShippingMethod();
        } else {
          var firstShippingMethod = children[0];
          firstShippingMethod.select.call(this, true);
          selectedShippingMethod = shippingMethodsById[firstShippingMethod.method_id];
          setShippingMethod();
        }
      }
    }
    selectedShippingOverride = null;

    if (!foundMethod) {
      $.create_order_button.setEnabled(false);
      $.override_button.setEnabled(false);
    }
    updateShippingOverrideLabel();
  }







  function setGiftMessage() {
    var promise = currentBasket.updateShipment({
      gift: $.gift_switch.getValue(),
      gift_message: $.gift_text.getValue() });

    Alloy.Router.showActivityIndicator(promise);
    promise.fail(function () {
      notify(_L('Unable to set the gift message.'), {
        preventAutoClose: true });

    });
    return promise;
  }








  function setShippingMethod(patch) {

    var method = {
      id: selectedShippingMethod.getID() };


    var deferred = new _.Deferred();
    var promise = currentBasket.setShippingMethod(method, patch, {
      c_employee_id: Alloy.Models.associate.getEmployeeId() });

    Alloy.Router.showActivityIndicator(deferred);
    promise.done(function () {

      if (currentBasket.getShipToStore() && Alloy.CFG.ship_to_store.free_shipping_ids.indexOf(selectedShippingMethod.getID()) >= 0) {
        var override = currentBasket.getShippingPriceOverride();

        if (override && override.price_override_type.toLowerCase() == 'fixed price' && override.price_override_value == 0) {
          enableOverrideButton();
          deferred.resolve();
        } else {

          setFreeShippingOverride(deferred);
        }
      } else {
        enableOverrideButton();
        deferred.resolve();
      }
    }).fail(function (model) {
      var fault = model.get('fault');
      if (fault && fault.message && fault.message != '') {
        notify(fault.message, {
          preventAutoClose: true });

      }
      deferred.reject();
    });
    return deferred.promise();
  }







  function setFreeShippingOverride(deferred) {
    var override = {
      shipping_method_id: selectedShippingMethod.shipping_method_id,
      price_override_type: 'FixedPrice',
      price_override_value: 0,
      price_override_reason_code: _L('Free Shipping to the Store'),
      ignore_permissions: true };

    currentBasket.setShippingPriceOverride(override, {
      c_employee_id: Alloy.Models.associate.getEmployeeId() }).
    done(function () {
      enableOverrideButton();
      deferred.resolve();
    }).fail(function () {
      deferred.reject();
    });
  }







  function updateShippingOverrideLabel(message) {
    if (selectedShippingOverride == null || selectedShippingOverride.price_override_type === 'none') {
      $.price_override_summary.hide();
    } else {
      $.price_override_summary.show();
      $.price_override_summary.setText(_L(message));
    }
  }







  function displayOverrideButton(show) {
    if (show) {
      $.override_button.setWidth(228);
      $.override_button.setHeight(42);
      $.override_button.setVisible(true);

      $.create_order_button.setLeft(0);
      $.create_order_label_container.setLeft(0);
    } else {
      $.override_button.setVisible(false);
      $.override_button.setWidth(0);
      $.override_button.setHeight(0);

      $.create_order_button.setLeft(130);
      $.create_order_label_container.setLeft(130);
    }
  }






  function enableOverrideButton() {

    if (currentBasket.hasShippingPriceOverride()) {
      if (freeShippingToStore()) {
        $.override_button.setEnabled(false);
        updateShippingOverrideLabel('Free Shipping to this Store');
      } else {
        $.override_button.setEnabled(true);
        updateShippingOverrideLabel('A shipping override has been applied');
      }
    } else {

      var shippingMethod = currentBasket.getShippingMethod();
      var shippingCost = zero(parseFloat(currentBasket.getShippingTotal()));
      var basePrice = 0;
      if (shippingMethod && currentBasket.shippingMethods && currentBasket.hasProductItems()) {
        shippingCost = currentBasket.getShippingMethodPrice(shippingMethod.getID());
        basePrice = currentBasket.getShippingMethodBasePrice(shippingMethod.getID());
      }

      if (!currentBasket.shippingMethods) {
        shippingCost = 0;
        shippingDiscount = 0;
      } else {

        shippingDiscount = shippingMethod ? shippingCost - currentBasket.getShippingTotal() : 0;
      }


      if (basePrice.toFixed(2) == shippingDiscount.toFixed(2)) {
        $.override_button.setEnabled(false);
        $.price_override_summary.hide();
      } else {
        $.override_button.setEnabled(true);
        updateShippingOverrideLabel();
      }
    }
  }







  function removeShippingOverride(index) {
    var okFunction = function () {
      removeOverride(true).done(function () {
        chooseShippingMethod(index);
      }).fail(function () {
        notify(_L('Could not remove override.'), {
          preventAutoClose: true });

      });
    };



    if (freeShippingToStore()) {
      var id = shippingMethods[index].shipping_method_id;
      if (Alloy.CFG.ship_to_store.free_shipping_ids.indexOf(id) < 0) {
        okFunction.call(this);
      } else {
        chooseShippingMethod(index);
      }
    } else {
      Alloy.Dialog.showConfirmationDialog({
        messageString: _L('A shipping override has been applied. Changing shipping methods will remove the override.'),
        okButtonString: _L('Confirm'),
        okFunction: okFunction });

    }
  }







  function removeOverride() {
    var override = {
      shipping_method_id: currentBasket.getShippingMethod().getID(),
      price_override_type: 'none',
      employee_id: Alloy.Models.associate.getEmployeeId(),
      employee_passcode: Alloy.Models.associate.getPasscode(),
      store_id: Alloy.CFG.store_id,
      kiosk_mode: isKioskMode(),
      ignore_permissions: isKioskMode() };

    if (isKioskManagerLoggedIn()) {
      override.manager_employee_id = getKioskManager().getEmployeeId();
      override.manager_employee_passcode = getKioskManager().getPasscode();
      override.manager_allowLOBO = getKioskManager().getPermissions().allowLOBO;
    }
    var promise = currentBasket.setShippingPriceOverride(override, {
      c_employee_id: Alloy.Models.associate.getEmployeeId() },
    {
      silent: true });

    Alloy.Router.showActivityIndicator(promise);
    promise.done(function () {
      selectedShippingOverride = override;
    });
    return promise;
  }






  function setCharacterLeftText() {
    $.character_left_label.setText(String.format(_L('You have %d characters left out of 250'), 250 - $.gift_text.getValue().length));
  }






  function giftSwitchChange() {
    if ($.gift_switch.value) {
      $.message_container.show();
      setCharacterLeftText();
    } else {
      $.message_container.hide();
    }
  }






  function turnOffGiftSwitch() {
    $.gift_switch.setValue(false);
    giftSwitchChange();
  }






  function handleKioskManagerLogin() {
    displayOverrideButton(isKioskManagerLoggedIn());
  }






  function freeShippingToStore() {
    return currentBasket.getShipToStore() && Alloy.CFG.ship_to_store.free_shipping_ids.indexOf(currentBasket.getShippingMethod().getID()) >= 0;
  }






  function onDoubleTap() {
    displayOverrideButton(true);
  }









  _.extend($, exports);
}

module.exports = Controller;