var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'components/hamburgerMenu';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.container = Ti.UI.createView(
  { top: 20, width: 0, height: 0, backgroundColor: Alloy.Styles.color.background.transparentBlack30Percent, visible: false, id: "container" });

  $.__views.container && $.addTopLevelView($.__views.container);
  showHideHamburgerMenu ? $.addListener($.__views.container, 'click', showHideHamburgerMenu) : __defers['$.__views.container!click!showHideHamburgerMenu'] = true;$.__views.side_bar_wrapper = Ti.UI.createView(
  { width: "30%", left: "-30%", top: 0, backgroundColor: Alloy.Styles.color.background.white, layout: "vertical", height: "100%", id: "side_bar_wrapper" });

  $.__views.container.add($.__views.side_bar_wrapper);
  $.__views.menu_header = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.color.background.lightGray, height: 170, layout: "vertical", id: "menu_header" });

  $.__views.associate_salute_msg = Ti.UI.createLabel(
  { width: "80%", height: 80, color: Alloy.Styles.color.text.black, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, top: 10, font: Alloy.Styles.messageFont, id: "associate_salute_msg", accessibilityValue: "associate_salute_msg" });

  $.__views.menu_header.add($.__views.associate_salute_msg);
  $.__views.associate_logout = Ti.UI.createButton(
  { backgroundImage: Alloy.Styles.primaryButtonImage, color: Alloy.Styles.buttons.primary.color, width: "80%", height: 50, top: 10, titleid: "_Logout", bottom: 20, bubbleParent: false, id: "associate_logout", accessibilityValue: "associate_logout" });

  $.__views.menu_header.add($.__views.associate_logout);
  onAssociateLogoutClick ? $.addListener($.__views.associate_logout, 'singletap', onAssociateLogoutClick) : __defers['$.__views.associate_logout!singletap!onAssociateLogoutClick'] = true;$.__views.footer_view = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.color.background.white, height: 0, id: "footer_view" });

  $.__views.side_bar = Ti.UI.createTableView(
  { selectedBackgroundColor: Alloy.Styles.color.background.lightGray, color: Alloy.Styles.color.text.black, separatorColor: Alloy.Styles.accentColor, top: 0, height: 685, bubbleParent: false, headerView: $.__views.menu_header, footerView: $.__views.footer_view, id: "side_bar" });

  $.__views.side_bar_wrapper.add($.__views.side_bar);
  handleMenuSelect ? $.addListener($.__views.side_bar, 'singletap', handleMenuSelect) : __defers['$.__views.side_bar!singletap!handleMenuSelect'] = true;$.__views.admin_dashboard_wrapper = Ti.UI.createView(
  { top: 1, height: 62, layout: "vertical", bubbleParent: false, id: "admin_dashboard_wrapper" });

  $.__views.side_bar_wrapper.add($.__views.admin_dashboard_wrapper);
  $.__views.__alloyId87 = Ti.UI.createView(
  { height: 1, backgroundColor: Alloy.Styles.accentColor, top: 0, id: "__alloyId87" });

  $.__views.admin_dashboard_wrapper.add($.__views.__alloyId87);
  $.__views.admin_dashboard = Ti.UI.createButton(
  { top: 0, width: "100%", height: 60, color: Alloy.Styles.color.text.black, font: Alloy.Styles.buttonFont, titleid: "_Admin_Dashboard", backgroundSelectedImage: Alloy.Styles.primaryButtonImage, id: "admin_dashboard", accessibilityValue: "admin_dashboard" });

  $.__views.admin_dashboard_wrapper.add($.__views.admin_dashboard);
  handleAdminDashboardButtonClick ? $.addListener($.__views.admin_dashboard, 'singletap', handleAdminDashboardButtonClick) : __defers['$.__views.admin_dashboard!singletap!handleAdminDashboardButtonClick'] = true;$.__views.__alloyId88 = Ti.UI.createView(
  { height: 1, backgroundColor: Alloy.Styles.accentColor, top: 0, id: "__alloyId88" });

  $.__views.admin_dashboard_wrapper.add($.__views.__alloyId88);
  exports.destroy = function () {};




  _.extend($, $.__views);











  var args = $.args;
  var currentCustomer = Alloy.Models.customer;
  var customerAddress = Alloy.Models.customerAddress;
  var currentBasket = Alloy.Models.basket;
  var currentAssociate = Alloy.Models.associate;
  var storePasswordHelpers = require('storePasswordHelpers');
  var logger = require('logging')('components:hamburgerMenu', getFullControllerPath($.__controllerPath));
  var tableRowStyle = $.createStyle({
    classes: ['table_view_row'],
    apiName: 'Ti.UI.TableRowView' });

  var verifyAddressEditBeforeNavigation = require('EAUtils').verifyAddressEditBeforeNavigation;




  var allTableData = [{
    id: 'sales_dashboard',
    accessibilityValue: 'sales_dashboard',
    title: _L('Sales Dashboard') },
  {
    id: 'product_search',
    accessibilityValue: 'product_search',
    title: _L('Search Button') },
  {
    id: 'customer_search',
    accessibilityValue: 'customer_search',
    title: _L('Customer Search Button') },
  {
    id: 'new_customer',
    accessibilityValue: 'new_customer',
    title: _L('Create Customer') },
  {
    id: 'wish_list',
    accessibilityValue: 'wish_list',
    title: _L('Wish List Button') },
  {
    id: 'clear_cart',
    accessibilityValue: 'clear_cart',
    title: _L('Clear Cart') },
  {
    id: 'order_search',
    accessibilityValue: 'order_search',
    title: _L('Order Search Button') },
  {
    id: 'customer_logout',
    accessibilityValue: 'customer_logout',
    title: _L('Logout Customer') }];



  var isKioskModeMenuBlackList = ['customer_search', 'sales_dashboard', 'new_customer', 'wish_list'];
  var isCustomerLogoutMenuBlackList = ['wish_list', 'customer_logout'];
  var isCustomerLoginMenuBlackList = ['clear_cart'];
  var isWishListDisabledBlackList = ['wish_list'];




  exports.deinit = deinit;
  exports.showHideHamburgerMenu = showHideHamburgerMenu;
  exports.hideHamburgerMenu = hideHamburgerMenu;









  function deinit() {
    logger.info('DEINIT called');
    $.container.removeEventListener('click', showHideHamburgerMenu);
    $.associate_logout.removeEventListener('singletap', onAssociateLogoutClick);
    $.admin_dashboard.removeEventListener('singletap', handleAdminDashboardButtonClick);
    $.side_bar.removeEventListener('singletap', handleMenuSelect);
    $.stopListening();
    $.destroy();
  }











  function hideHamburgerMenu(deferred) {
    if ($.container.getVisible()) {
      $.side_bar_wrapper.animate({
        duration: 300,
        left: '-30%' },
      function () {
        $.container.setVisible(false);
        $.container.setWidth(0);
        $.container.setHeight(0);
        $.container.setZIndex(null);
        if (deferred && _.isFunction(deferred.resolve)) {
          deferred.resolve();
        }
      });
    }
  }








  function showHamburgerMenu(deferred) {
    if (!$.container.getVisible()) {
      updateAssociateName();
      renderMenuItems();
      $.container.setWidth('100%');
      $.container.setHeight('100%');
      $.container.setZIndex(100);
      $.container.setVisible(true);
      $.side_bar_wrapper.animate({
        duration: 300,
        left: 0 },
      function () {
        if (deferred && _.isFunction(deferred.resolve)) {
          deferred.resolve();
        }
      });
    }
  }









  function filterMenuItems(filter, listToFilter) {
    var filteredData;
    var tableData = listToFilter || allTableData;
    if (Object.prototype.toString.apply(filter) === '[object Array]') {
      filteredData = tableData.filter(function (row) {
        return filter.indexOf(row.id) == -1;
      });
    } else {
      filteredData = tableData;
    }
    return filteredData;
  }








  function generateMenuRows(filteredData) {
    var renderRows = [];
    _.each(filteredData, function (rowData) {
      if (rowData.id == 'sales_dashboard') {

        if (!currentAssociate.hasStoreLevelSalesReportsPrivileges() && !currentAssociate.hasSalesReportsPrivileges()) {
          return;
        }
      }
      var row;
      if (rowData.image) {
        var imageRow = Alloy.createController('components/menuImageRow', {
          submenuLabel: rowData.submenuLabel,
          image: rowData.image,
          properties: tableRowStyle,
          label: rowData.label });

        row = imageRow.getView();
        row.id = rowData.id;
        row.accessibilityValue = rowData.accessibilityValue;
      } else {
        row = Ti.UI.createTableViewRow(tableRowStyle);
        row = _.extend(row, rowData);
      }
      renderRows.push(row);
    });
    return renderRows;
  }








  function executeMenuAction(selectedMenuItemId) {
    switch (selectedMenuItemId) {
      case 'sales_dashboard':
        showSalesDashboard();
        break;
      case 'new_customer':
        showNewCustomerDialog();
        break;

      case 'customer_logout':
        onCustomerLogoutClick();
        break;

      case 'order_search':

      case 'product_search':

      case 'customer_search':
        showSearchDialog(selectedMenuItemId);
        break;

      case 'wish_list':
        verifyAddressEditBeforeNavigation(showCurrentCustomerWishList);
        break;
      case 'clear_cart':
        handleClearCart();
        break;
      case 'change_store_password':
        handleChangeStorePassword();
        break;
      default:
        logger.error('Case for ' + selectedMenuItemId + ' is not handled.');
        break;}

  }






  function showSalesDashboard() {
    Alloy.CFG.sales_reports.enabled_charts = {};
    if (currentAssociate.hasStoreLevelSalesReportsPrivileges()) {
      Alloy.CFG.sales_reports.enabled_charts = Alloy.CFG.sales_reports.charts.store_level_privileges;
    } else {
      if (currentAssociate.hasSalesReportsPrivileges()) {
        Alloy.CFG.sales_reports.enabled_charts = Alloy.CFG.sales_reports.charts.associate_level_privileges;
      }
    }
    Alloy.Dialog.showCustomDialog({
      fullScreen: true,
      viewName: 'associate_profile',
      controllerPath: 'reports/index',
      continueEvent: 'salesReport:dismiss' });

  }








  function showSearchDialog(searchTabId) {
    switch (searchTabId) {
      case 'product_search':
        verifyAddressEditBeforeNavigation(Alloy.Router.presentProductSearchDrawer);
        break;

      case 'customer_search':
        verifyAddressEditBeforeNavigation(Alloy.Router.presentCustomerSearchDrawer);
        break;

      case 'order_search':
        verifyAddressEditBeforeNavigation(Alloy.Router.presentOrderSearchDrawer);
        break;}

  }







  function showNewCustomerDialog() {
    if (currentCustomer.isLoggedIn()) {
      Alloy.Dialog.showConfirmationDialog({
        messageString: _L('The current customer needs to be logged out before creating a new one. Tap on "Continue" to logout the customer and create a new customer.'),
        titleString: _L('Create Customer Title'),
        okButtonString: _L('Continue'),
        okFunction: function () {
          Alloy.Router.customerLogout().done(function () {
            Alloy.Router.presentCreateAccountDrawer();
          });
        } });

    } else {
      Alloy.Router.presentCreateAccountDrawer();
    }
  }






  function showCurrentCustomerWishList() {
    if (currentCustomer.isLoggedIn()) {
      Alloy.Router.navigateToCart({
        switchToWishList: true });

    }
  }







  function handleChangeStorePassword() {
    logger.info('handleChangeStorePassword called');
    Alloy.Router.presentChangeStorePasswordDrawer();
  }







  function handleClearCart() {
    var employee_code = currentAssociate.getEmployeeId();
    var employee_pin = currentAssociate.getPasscode();


    Alloy.Globals.resetCookies();

    var deferred = new _.Deferred();



    var retryFailure = function () {
      Alloy.Dialog.showConfirmationDialog({
        messageString: _L('There was a problem clearing the cart. Please try again.'),
        titleString: _L('Unable to clear the cart.'),
        okButtonString: _L('Retry'),
        hideCancel: true,
        okFunction: function () {
          removeNotify();
          handleClearCart();
        } });

    };


    Alloy.Router.showActivityIndicator(deferred);
    currentAssociate.loginAssociate({
      employee_id: employee_code,
      passcode: employee_pin }).
    done(function () {
      notify(_L('Cart Cleared'));
      currentBasket.deleteBasket().done(function () {
        currentBasket.getBasket({
          c_eaEmployeeId: employee_code }).
        done(function () {
          currentBasket.trigger('basket_sync');
          Alloy.eventDispatcher.trigger('cart_cleared');
          notify(_L('Cart Cleared'));
          deferred.resolve();
        }).fail(function () {
          deferred.reject();
          retryFailure();
        });
      }).fail(function () {
        deferred.reject();
        retryFailure();
      });
    }).fail(function () {
      deferred.reject();
      retryFailure();
    });
  }






  function openAdminDashboardWindow() {
    logger.info('Opening Admin Dashboard Window');
    Alloy.Dialog.showCustomDialog({
      fullScreen: true,
      viewName: 'admin_dashbord',
      controllerPath: 'support/index',
      continueEvent: 'dashboard:dismiss',
      continueFunction: function () {
        Alloy.eventDispatcher.trigger('dashboard:dismiss');
      } });

  }








  function showAdminDashboard(requireAuth) {
    logger.info('Showing Admin Dashboard');
    allowAppSleep(false);
    if (requireAuth) {
      var associate = Alloy.createModel('associate');
      Alloy.Dialog.showCustomDialog({
        controllerPath: 'associate/authorization',
        initOptions: {
          associate: associate,
          subTitleText: _L('Enter Manager Credentials Admin'),
          successMessage: _L('Manager Credentials Accepted') },

        continueEvent: 'authorization:dismiss',
        continueFunction: function (result) {
          if (result && result.result) {
            openAdminDashboardWindow();
          }
        } });

    } else {
      openAdminDashboardWindow();
    }
  }






  function renderMenuItems() {
    logger.info('rendering menu items');
    var menuData;
    if (!currentCustomer.isLoggedIn()) {
      menuData = filterMenuItems(isCustomerLogoutMenuBlackList);
    }

    if (isKioskMode()) {
      menuData = filterMenuItems(isKioskModeMenuBlackList, menuData);

      if (isKioskManagerLoggedIn()) {
        $.admin_dashboard_wrapper.show();
        $.admin_dashboard_wrapper.setHeight(62);
        $.admin_dashboard_wrapper.setEnable(true);
      } else {
        $.admin_dashboard_wrapper.hide();
        $.admin_dashboard_wrapper.setHeight(0);
        $.admin_dashboard_wrapper.setEnable(false);
      }
    }

    if (!isKioskMode() && currentCustomer.isLoggedIn()) {
      menuData = filterMenuItems(isCustomerLoginMenuBlackList);
    }

    if (!Alloy.CFG.enable_wish_list) {
      menuData = filterMenuItems(isWishListDisabledBlackList, menuData);
    }

    if (storePasswordHelpers.isStorePasswordExpiring()) {
      var expirationDays = Alloy.Models.storeUser.getExpirationDays();
      menuData.push({
        id: 'change_store_password',
        accessibilityValue: 'change_store_password',
        submenuLabel: expirationDays == 1 ? String.format(_L('Expires in %d Day'), expirationDays) : String.format(_L('Expires in %d Days'), expirationDays),
        label: _L('Change Store Password Menu'),
        image: Alloy.Styles.warningIcon });

    }

    $.side_bar.setData(generateMenuRows(menuData));
  }






  function handleAdminDashboardAction() {
    if (!isKioskMode() && currentAssociate.hasAdminPrivileges() || isKioskManagerLoggedIn() && getKioskManager().hasAdminPrivileges()) {
      showAdminDashboard();
    } else {
      logger.info('Ask Manager credentials');
      showAdminDashboard(true);
    }
  }






  function updateAssociateName() {
    if (isKioskMode() && isKioskManagerLoggedIn()) {
      $.associate_salute_msg.setText(String.format(_L('Hi, %s'), getKioskManager().getFullName()));
    } else {
      $.associate_salute_msg.setText(String.format(_L('Hi, %s'), currentAssociate.getFullName()));
    }
  }










  function showHideHamburgerMenu() {
    var deferred = new _.Deferred();

    if (!$.container.getVisible()) {
      showHamburgerMenu(deferred);
    } else {
      hideHamburgerMenu(deferred);
    }

    return deferred.promise();
  }








  function handleMenuSelect(event) {
    if (event.row && event.row.id) {
      showHideHamburgerMenu().done(function () {
        executeMenuAction(event.row.id);
      });
    }
  }






  function handleAdminDashboardButtonClick() {
    showHideHamburgerMenu().done(handleAdminDashboardAction);
  }






  function onCustomerLogoutClick() {
    verifyAddressEditBeforeNavigation(Alloy.Router.customerLogout);
  }






  function onAssociateLogoutClick() {
    showHideHamburgerMenu().done(function () {
      verifyAddressEditBeforeNavigation(function () {

        if (isKioskManagerLoggedIn()) {
          Alloy.Kiosk.manager = null;
          Alloy.eventDispatcher.trigger('kiosk:manager_login_change');
          notify(_L('Associate Logged Out'));
        } else {
          Alloy.Router.associateLogout();
        }
      });
    });
  }





  __defers['$.__views.container!click!showHideHamburgerMenu'] && $.addListener($.__views.container, 'click', showHideHamburgerMenu);__defers['$.__views.associate_logout!singletap!onAssociateLogoutClick'] && $.addListener($.__views.associate_logout, 'singletap', onAssociateLogoutClick);__defers['$.__views.side_bar!singletap!handleMenuSelect'] && $.addListener($.__views.side_bar, 'singletap', handleMenuSelect);__defers['$.__views.admin_dashboard!singletap!handleAdminDashboardButtonClick'] && $.addListener($.__views.admin_dashboard, 'singletap', handleAdminDashboardButtonClick);



  _.extend($, exports);
}

module.exports = Controller;