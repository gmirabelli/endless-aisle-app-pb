var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'components/errorPopover';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.popover_window = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.color.background.transparent, id: "popover_window" });

  $.__views.popover_window && $.addTopLevelView($.__views.popover_window);
  $.__views.backdrop = Ti.UI.createView(
  { top: 0, left: 0, width: 1024, height: 768, opacity: 0.25, backgroundColor: Alloy.Styles.color.background.black, id: "backdrop" });

  $.__views.popover_window.add($.__views.backdrop);
  $.__views.popover_container = Ti.UI.createView(
  { layout: "vertical", height: Ti.UI.SIZE, top: 76, width: 650, backgroundColor: Alloy.Styles.color.background.white, borderWidth: 2, borderColor: Alloy.Styles.color.border.lightest, id: "popover_container" });

  $.__views.popover_window.add($.__views.popover_container);
  $.__views.error_title = Ti.UI.createLabel(
  { width: Ti.UI.FILL, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.titleFont, top: 20, left: 20, right: 20, textid: "_Error_in_application", id: "error_title", accessibilityValue: "error_confirm_title" });

  $.__views.popover_container.add($.__views.error_title);
  $.__views.describe_text_heading = Ti.UI.createLabel(
  { width: Ti.UI.FILL, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.appFont, left: 20, right: 20, textid: "_Please_help_us_resolve_the_problem_by_telling_us_what_you_were_doing_", top: 20, id: "describe_text_heading", accessibilityValue: "describe_text_heading" });

  $.__views.popover_container.add($.__views.describe_text_heading);
  $.__views.describe_text_message = Ti.UI.createTextArea(
  { width: Ti.UI.FILL, color: Alloy.Styles.color.text.dark, font: Alloy.Styles.detailTextFont, padding: { left: 8 }, borderColor: Alloy.Styles.color.border.light, borderWidth: 1, height: 80, top: 10, left: 20, right: 20, id: "describe_text_message", accessibilityValue: "error_describe_text" });

  $.__views.popover_container.add($.__views.describe_text_message);
  $.__views.error_details = Ti.UI.createView(
  { top: 0, layout: "vertical", width: Ti.UI.FILL, height: 0, visible: false, id: "error_details" });

  $.__views.popover_container.add($.__views.error_details);
  $.__views.error_text_heading = Ti.UI.createLabel(
  { width: Ti.UI.FILL, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.appFont, left: 20, right: 20, textid: "_Error_details_heading", top: 20, id: "error_text_heading", accessibilityValue: "error_text_heading" });

  $.__views.error_details.add($.__views.error_text_heading);
  $.__views.error_scroll_container = Ti.UI.createScrollView(
  { borderColor: Alloy.Styles.color.border.light, borderWidth: 1, left: 20, right: 20, top: 10, height: 200, width: Ti.UI.FILL, id: "error_scroll_container" });

  $.__views.error_details.add($.__views.error_scroll_container);
  $.__views.error_text_message = Ti.UI.createLabel(
  { width: Ti.UI.FILL, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailTextFont, padding: { left: 8 }, top: 10, left: 8, id: "error_text_message", accessibilityValue: "error_message" });

  $.__views.error_scroll_container.add($.__views.error_text_message);
  $.__views.button_container = Ti.UI.createView(
  { layout: "horizontal", height: Ti.UI.SIZE, width: Ti.UI.SIZE, top: 40, bottom: 20, id: "button_container" });

  $.__views.popover_container.add($.__views.button_container);
  $.__views.details_button = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, backgroundImage: Alloy.Styles.buttons.secondary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.secondary.backgroundDisabledImage, color: Alloy.Styles.buttons.secondary.color, disabledColor: Alloy.Styles.buttons.secondary.disabledColor, height: 40, width: 200, titleid: "_Show_Details", id: "details_button", accessibilityValue: "error_details_button" });

  $.__views.button_container.add($.__views.details_button);
  $.__views.ok_button = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.buttons.primary.color, disabledColor: Alloy.Styles.buttons.primary.disabledColor, backgroundImage: Alloy.Styles.buttons.primary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.primary.backgroundDisabledImage, height: 40, width: 200, titleid: "_OK", left: 20, id: "ok_button", accessibilityValue: "error_ok_button" });

  $.__views.button_container.add($.__views.ok_button);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var logger = require('logging')('components:errorPopover', getFullControllerPath($.__controllerPath));




  $.ok_button.addEventListener('click', dismiss);
  $.details_button.addEventListener('click', onDetailsClick);




  exports.init = init;
  exports.deinit = deinit;











  function init(args) {
    logger.info('init called');
    $.error_text_message.setText(args);
  }






  function deinit() {
    logger.info('deinit called');
    $.ok_button.removeEventListener('click', dismiss);
    $.details_button.removeEventListener('click', onDetailsClick);
    $.destroy();
  }









  function dismiss() {
    $.trigger('errorPopover:dismiss', {
      text: $.describe_text_message.getValue() });

  }






  function onDetailsClick() {
    if ($.error_details.getVisible()) {

      $.error_details.setHeight(0);
      $.error_details.setVisible(false);
      $.details_button.setTitle(_L('Show Details'));
    } else {

      $.error_details.setHeight(Ti.UI.SIZE);
      $.error_details.setVisible(true);
      $.details_button.setTitle(_L('Hide Details'));
    }
  }









  _.extend($, exports);
}

module.exports = Controller;