var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'components/startupPopover';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.popover_window = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.color.background.transparent, id: "popover_window" });

  $.__views.popover_window && $.addTopLevelView($.__views.popover_window);
  $.__views.backdrop = Ti.UI.createView(
  { top: 0, left: 0, width: 1024, height: 768, opacity: 0.55, backgroundColor: Alloy.Styles.color.background.black, id: "backdrop" });

  $.__views.popover_window.add($.__views.backdrop);
  $.__views.popover_container = Ti.UI.createView(
  { layout: "vertical", height: Ti.UI.SIZE, width: "50%", backgroundColor: Alloy.Styles.color.background.white, id: "popover_container" });

  $.__views.popover_window.add($.__views.popover_container);
  $.__views.startup_title = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.accentColor, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.titleFont, top: 20, textid: "_Unable_to_start_the_application", id: "startup_title", accessibilityValue: "startup_confirm_title" });

  $.__views.popover_container.add($.__views.startup_title);
  $.__views.startup_message = Ti.UI.createLabel(
  { width: Ti.UI.FILL, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.messageFont, left: 50, right: 50, top: 50, id: "startup_message", accessibilityValue: "startup_message" });

  $.__views.popover_container.add($.__views.startup_message);
  $.__views.startup_fault = Ti.UI.createLabel(
  { width: Ti.UI.FILL, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.messageFont, left: 50, right: 50, top: 50, id: "startup_fault", accessibilityValue: "startup_fault" });

  $.__views.popover_container.add($.__views.startup_fault);
  $.__views.store_input = Ti.UI.createView(
  { top: 20, left: 50, right: 50, layout: "horizontal", width: Ti.UI.FILL, height: 42, id: "store_input" });

  $.__views.popover_container.add($.__views.store_input);
  $.__views.store_id_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.appFont, padding: { left: 4 }, textid: "_Store_ID_", id: "store_id_label", accessibilityValue: "store_id_label" });

  $.__views.store_input.add($.__views.store_id_label);
  $.__views.store_selection = Ti.UI.createView(
  { left: 10, top: 4, width: Ti.UI.FILL, id: "store_selection" });

  $.__views.store_input.add($.__views.store_selection);
  $.__views.buttons_container = Ti.UI.createView(
  { top: 50, bottom: 20, layout: "horizontal", width: 450, height: 50, id: "buttons_container" });

  $.__views.popover_container.add($.__views.buttons_container);
  $.__views.app_info_button = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, backgroundImage: Alloy.Styles.buttons.secondary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.secondary.backgroundDisabledImage, color: Alloy.Styles.buttons.secondary.color, disabledColor: Alloy.Styles.buttons.secondary.disabledColor, height: 40, width: 200, titleid: "_Configuration", left: 0, id: "app_info_button", accessibilityValue: "startup_config_button" });

  $.__views.buttons_container.add($.__views.app_info_button);
  $.__views.ok_button = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.buttons.primary.color, disabledColor: Alloy.Styles.buttons.primary.disabledColor, backgroundImage: Alloy.Styles.buttons.primary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.primary.backgroundDisabledImage, height: 40, width: 200, titleid: "_Retry", left: 50, id: "ok_button", accessibilityValue: "startup_ok_button" });

  $.__views.buttons_container.add($.__views.ok_button);
  exports.destroy = function () {};




  _.extend($, $.__views);











  var logger = require('logging')('components:startupPopover', getFullControllerPath($.__controllerPath));





  $.ok_button.addEventListener('click', dismiss);
  $.app_info_button.addEventListener('click', showAppConfigurationDialog);




  exports.init = init;
  exports.deinit = deinit;









  function init(args) {
    logger.info('init called');
    var title = args.message;
    var fault = args.fault;
    $.startup_message.setText(title);
    $.ok_button.setEnabled(false);
    var message;
    if (fault.stores && fault.stores.stores && fault.stores.stores.length > 0) {
      message = _L('Select Store ID to connect to and then retry.');
      initializeStores(fault.stores.stores);

      $.popover_container.setWidth('65%');
    } else {
      $.store_input.setHeight(0);
      $.store_input.hide();
      message = _L('No Digital Stores found.');
    }
    $.startup_fault.setText(message);
  }






  function deinit() {
    logger.info('deinit called');
    $.stopListening($.store_select, 'itemSelected', onStoreSelected);
    $.ok_button.removeEventListener('click', dismiss);
    $.app_info_button.removeEventListener('click', showAppConfigurationDialog);
    $.destroy();
  }











  function initializeStores(stores) {
    $.store_select = Alloy.createController('components/selectWidget', {
      valueField: 'value',
      textField: 'label',
      values: stores,
      messageWhenNoSelection: _L('Select Store ID'),
      selectListTitleStyle: {
        accessibilityValue: 'store_id_dropdown',
        width: Ti.UI.FILL,
        left: 10 },

      selectListStyle: {
        width: Ti.UI.FILL,
        top: 0 },

      selectedItem: Alloy.CFG.store_id });

    $.store_selection.add($.store_select.getView());

    $.listenTo($.store_select, 'itemSelected', onStoreSelected);
  }









  function dismiss() {
    $.trigger('startupPopover:dismiss');
  }






  function showAppConfigurationDialog() {
    $.trigger('startupPopover:appConfiguration');
  }








  function onStoreSelected(event) {
    logger.info('before Alloy.CFG.store_id ' + Alloy.CFG.store_id);
    var settings = require('appSettings');
    settings.setSetting('store_id', event.item.value, true);
    logger.info('after Alloy.CFG.store_id ' + Alloy.CFG.store_id);
    $.ok_button.setEnabled(true);
  }









  _.extend($, exports);
}

module.exports = Controller;