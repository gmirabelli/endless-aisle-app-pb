var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
		delete obj[key];
	}
	return arg;
}

function Controller() {

	require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'components/associatePopover';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};







	$.__views.popover_window = Ti.UI.createView(
	{ backgroundColor: Alloy.Styles.color.background.transparent, id: "popover_window" });

	$.__views.popover_window && $.addTopLevelView($.__views.popover_window);
	$.__views.backdrop = Ti.UI.createView(
	{ top: 0, left: 0, width: 1024, height: 768, opacity: 0.25, backgroundColor: Alloy.Styles.color.background.black, id: "backdrop" });

	$.__views.popover_window.add($.__views.backdrop);
	$.__views.popover_container = Ti.UI.createView(
	{ layout: "vertical", height: 80, width: 250, backgroundColor: Alloy.Styles.color.background.white, left: "38%", top: 75, borderWidth: 2, borderColor: Alloy.Styles.color.border.lightest, id: "popover_container" });

	$.__views.popover_window.add($.__views.popover_container);
	$.__views.logout_button = Ti.UI.createButton(
	{ backgroundImage: Alloy.Styles.primaryButtonImage, color: Alloy.Styles.buttons.primary.color, width: "80%", height: 50, top: 10, titleid: "_Logout_Associate", id: "logout_button", accessibilityValue: "logout_button" });

	$.__views.popover_container.add($.__views.logout_button);
	exports.destroy = function () {};




	_.extend($, $.__views);










	var customerAddress = Alloy.Models.customerAddress;
	var currentCustomer = Alloy.Models.customer;
	var showCustomerAddressAlert = require('EAUtils').showCustomerAddressAlert;





	$.logout_button.addEventListener('click', onLogoutClick);


	$.backdrop.addEventListener('click', dismiss);




	exports.init = init;
	exports.deinit = deinit;









	function init() {}








	function deinit() {
		$.logout_button.removeEventListener('click', onLogoutClick);
		$.backdrop.removeEventListener('click', dismiss);
		$.stopListening();
		$.destroy();
	}









	function dismiss() {
		$.trigger('associatePopover:dismiss');
	}






	function onLogoutClick() {
		if (currentCustomer.isLoggedIn()) {
			if (customerAddress.isCustomerAddressPage()) {
				showCustomerAddressAlert(true).done(function () {
					Alloy.Router.associateLogout();
				});
			} else {
				Alloy.Router.associateLogout();
			}
		} else {
			Alloy.Router.associateLogout();
		}
		dismiss();
	}









	_.extend($, exports);
}

module.exports = Controller;