var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'components/nextPreviousToolbar';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  exports.destroy = function () {};




  _.extend($, $.__views);










  var logger = require('logging')('components:nextPreviousToolbar', getFullControllerPath($.__controllerPath));

  var args = arguments[0] || {};
  var supportReturn = args.supportReturn || false;
  var getUIObjectType = require('EAUtils').getUIObjectType;
  var textFields = [];
  var selectedField;

  var previous = Titanium.UI.createButton({
    title: _L('Previous'),
    style: Ti.UI.iOS.SystemButtonStyle.BORDERED });


  var next = Titanium.UI.createButton({
    title: _L('Next'),
    style: Ti.UI.iOS.SystemButtonStyle.BORDERED,
    left: 0 });


  var toolbar = Titanium.UI.createToolbar({
    items: [previous, next],
    borderTop: false,
    borderBottom: false });






  next.addEventListener('click', onNextClick);


  previous.addEventListener('click', onPreviousClick);




  exports.deinit = deinit;
  exports.setTextFields = setTextFields;









  function deinit() {
    logger.info('DEINIT called');
    next.removeEventListener('click', onNextClick);
    previous.removeEventListener('click', onPreviousClick);
    removeAllChildren(toolbar);
    selectedField = null;
    if (toolbar) {
      toolbar.hide();
    }
    toolbar = null;
    _.each(textFields, function (textField) {
      logger.info('removing ' + textField);
      textField.removeEventListener('focus', onFieldFocus);
      if (supportReturn) {
        textField.removeEventListener('return', onNextClick);
      }
      textField.setKeyboardToolbar(toolbar);

      if (getUIObjectType(textField) === 'TextField' || getUIObjectType(textField) === 'TextArea') {
        textField.blur();
      }
    });
    textFields = [];
    $.stopListening();
    $.destroy();
  }










  function setTextFields(fields) {
    logger.info('setTextFields called ' + fields);
    textFields = fields.slice();
    _.each(textFields, function (textField) {
      if (getUIObjectType(textField) === 'TextField' || getUIObjectType(textField) === 'TextArea') {
        textField.setKeyboardToolbar(toolbar);
        textField.addEventListener('focus', onFieldFocus);
        if (supportReturn) {
          textField.addEventListener('return', onNextClick);
        }
      }
    });
  }






  function enableNext(enable) {
    next.setEnabled(enable);
  }






  function enablePrevious(enable) {
    previous.setEnabled(enable);
  }









  function onNextClick() {
    logger.info('onNextClick called');
    if (selectedField + 1 < textFields.length) {
      if (getUIObjectType(textFields[selectedField + 1]) === 'TextField' || getUIObjectType(textFields[selectedField + 1]) === 'TextArea') {
        textFields[selectedField + 1].focus();
      } else {
        textFields[selectedField + 1].fireEvent('autoFocus');
      }
    }
  }






  function onPreviousClick() {
    logger.info('onPreviousClick called');
    if (selectedField - 1 >= 0) {
      var UI_ObjectType = textFields[selectedField - 1].getApiName().split('.');
      UI_ObjectType = UI_ObjectType[UI_ObjectType.length - 1];
      if (UI_ObjectType === 'TextField' || UI_ObjectType === "TextArea") {
        textFields[selectedField - 1].focus();
      } else {
        textFields[selectedField - 1].fireEvent('autoFocus');
      }
    }
  }








  function onFieldFocus(event) {
    logger.info('onFieldFocus called');
    selectedField = _.indexOf(textFields, event.source);
    enableNext(selectedField < textFields.length - 1);
    enablePrevious(selectedField != 0);
    Alloy.eventDispatcher.trigger('session:renew');
  }









  _.extend($, exports);
}

module.exports = Controller;