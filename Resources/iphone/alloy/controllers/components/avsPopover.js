var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'components/avsPopover';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};




  $.addresses = Alloy.createCollection('recommendedAddress');$.givenAddress = Alloy.createModel('recommendedAddress');


  $.__views.popover_window = Ti.UI.createView(
  { backgroundColor: "transparent", id: "popover_window" });

  $.__views.popover_window && $.addTopLevelView($.__views.popover_window);
  $.__views.backdrop = Ti.UI.createView(
  { top: 20, left: 0, width: 1024, height: 768, opacity: 0.25, backgroundColor: Alloy.Styles.color.background.black, id: "backdrop" });

  $.__views.popover_window.add($.__views.backdrop);
  $.__views.popover_container = Ti.UI.createView(
  { layout: "vertical", height: Ti.UI.SIZE, width: 750, backgroundColor: Alloy.Styles.color.background.white, left: 127, top: 85, borderWidth: 2, borderColor: Alloy.Styles.color.border.lightest, id: "popover_container" });

  $.__views.popover_window.add($.__views.popover_container);
  $.__views.header_container = Ti.UI.createView(
  { layout: "horizontal", height: 40, top: 20, left: 60, width: "100%", id: "header_container" });

  $.__views.popover_container.add($.__views.header_container);
  $.__views.main_title = Ti.UI.createLabel(
  { width: "85%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, textid: "_VERIFY_YOUR_ADDRESS", font: Alloy.Styles.titleFont, id: "main_title", accessibilityValue: "main_title_avs" });

  $.__views.header_container.add($.__views.main_title);
  $.__views.__alloyId77 = Ti.UI.createView(
  { width: "100%", backgroundColor: Alloy.Styles.color.background.dark, height: 1, left: 0, top: 10, id: "__alloyId77" });

  $.__views.popover_container.add($.__views.__alloyId77);
  $.__views.column_container = Ti.UI.createView(
  { width: 630, height: 439, left: 60, bottom: 20, layout: "horizontal", id: "column_container" });

  $.__views.popover_container.add($.__views.column_container);
  $.__views.subtitle = Ti.UI.createLabel(
  { width: "100%", height: 150, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailValueFont, id: "subtitle", accessibilityValue: "subtitle_avs" });

  $.__views.column_container.add($.__views.subtitle);
  $.__views.__alloyId78 = Ti.UI.createView(
  { width: "100%", backgroundColor: Alloy.Styles.color.background.dark, height: 1, left: 0, top: 10, id: "__alloyId78" });

  $.__views.column_container.add($.__views.__alloyId78);
  $.__views.suggested_addresses_column_container = Ti.UI.createView(
  { layout: "vertical", top: 5, bottom: 0, width: "50%", id: "suggested_addresses_column_container" });

  $.__views.column_container.add($.__views.suggested_addresses_column_container);
  $.__views.suggested_addresses_title = Ti.UI.createLabel(
  { width: "100%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailValueFont, textid: "_Recommended_Address", id: "suggested_addresses_title", accessibilityValue: "suggested_addresses_title" });

  $.__views.suggested_addresses_column_container.add($.__views.suggested_addresses_title);
  $.__views.suggested_addresses_table = Ti.UI.createTableView(
  { layout: "vertical", top: 10, bottom: 0, id: "suggested_addresses_table", separatorStyle: "transparent" });

  $.__views.suggested_addresses_column_container.add($.__views.suggested_addresses_table);
  var __alloyId82 = Alloy.Collections['$.addresses'] || $.addresses;function __alloyId83(e) {if (e && e.fromAdapter) {return;}var opts = __alloyId83.opts || {};var models = __alloyId82.models;var len = models.length;var rows = [];for (var i = 0; i < len; i++) {var __alloyId79 = models[i];__alloyId79.__transform = transformAddress(__alloyId79);var __alloyId81 = Alloy.createController('components/recommendedShippingAddress', { $model: __alloyId79, __parentSymbol: __parentSymbol });
      rows.push(__alloyId81.getViewEx({ recurse: true }));
    }$.__views.suggested_addresses_table.setData(rows);};__alloyId82.on('fetch destroy change add remove reset', __alloyId83);$.__views.entered_address_column_container = Ti.UI.createView(
  { layout: "vertical", top: 5, width: "50%", height: Ti.UI.SIZE, id: "entered_address_column_container" });

  $.__views.column_container.add($.__views.entered_address_column_container);
  $.__views.entered_address_title = Ti.UI.createLabel(
  { width: "100%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailValueFont, textid: "_Address_You_Entered", id: "entered_address_title", accessibilityValue: "entered_addresses_title" });

  $.__views.entered_address_column_container.add($.__views.entered_address_title);
  $.__views.entered_address_tile = Ti.UI.createView(
  { layout: "horizontal", height: 60, width: "100%", left: 0, top: 20, id: "entered_address_tile" });

  $.__views.entered_address_column_container.add($.__views.entered_address_tile);
  $.__views.radio_button_container = Ti.UI.createView(
  { left: 0, width: 20, height: Ti.UI.SIZE, top: 2, id: "radio_button_container" });

  $.__views.entered_address_tile.add($.__views.radio_button_container);
  $.__views.radio_button = Ti.UI.createImageView(
  { image: Alloy.Styles.radioButtonOffImage, id: "radio_button", accessibilityValue: "radio_button" });

  $.__views.radio_button_container.add($.__views.radio_button);
  $.__views.entered_address_column = Ti.UI.createView(
  { left: 20, layout: "vertical", width: Ti.UI.SIZE, height: Ti.UI.SIZE, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, top: 0, id: "entered_address_column" });

  $.__views.entered_address_tile.add($.__views.entered_address_column);
  $.__views.given_address1 = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: 20, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, ellipsize: true, wordWrap: false, font: Alloy.Styles.detailValueFont, id: "given_address1", accessibilityValue: "given_address1" });

  $.__views.entered_address_column.add($.__views.given_address1);
  $.__views.given_address2 = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: 20, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, ellipsize: true, wordWrap: false, font: Alloy.Styles.detailValueFont, id: "given_address2", accessibilityValue: "given_address2" });

  $.__views.entered_address_column.add($.__views.given_address2);
  $.__views.given_city_state_zip = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: 20, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, ellipsize: true, wordWrap: false, font: Alloy.Styles.detailValueFont, id: "given_city_state_zip", accessibilityValue: "given_city_state_zip" });

  $.__views.entered_address_column.add($.__views.given_city_state_zip);
  $.__views.button_container = Ti.UI.createView(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, top: 20, bottom: 20, layout: "horizontal", id: "button_container" });

  $.__views.popover_container.add($.__views.button_container);
  $.__views.cancel_button = Ti.UI.createButton(
  { backgroundImage: Alloy.Styles.secondaryButtonImage, backgroundDisabledColor: Alloy.Styles.color.background.medium, color: Alloy.Styles.buttons.secondary.color, font: Alloy.Styles.buttonMediumFont, width: 228, height: 42, id: "cancel_button", titleid: "_Cancel", enabled: true, visible: true, accessibilityValue: "cancel_button_avs" });

  $.__views.button_container.add($.__views.cancel_button);
  $.__views.continue_button = Ti.UI.createButton(
  { backgroundImage: Alloy.Styles.primaryButtonImage, backgroundDisabledColor: Alloy.Styles.color.background.medium, color: Alloy.Styles.buttons.primary.color, font: Alloy.Styles.buttonMediumFont, width: 228, left: 10, height: 42, id: "continue_button", titleid: "_Continue", enabled: true, visible: true, accessibilityValue: "continue_button_avs" });

  $.__views.button_container.add($.__views.continue_button);
  var __alloyId84 = function () {$['givenAddress'].__transform = _.isFunction($['givenAddress'].transform) ? $['givenAddress'].transform() : $['givenAddress'].toJSON();$.given_address1.text = $['givenAddress']['__transform']['address1'];$.given_address2.text = $['givenAddress']['__transform']['address2'];$.given_city_state_zip.text = $['givenAddress']['__transform']['city_state_zip'];};$['givenAddress'].on('fetch change destroy', __alloyId84);exports.destroy = function () {__alloyId82 && __alloyId82.off('fetch destroy change add remove reset', __alloyId83);$['givenAddress'] && $['givenAddress'].off('fetch change destroy', __alloyId84);};




  _.extend($, $.__views);











  var logger = require('logging')('components:avsPopover', getFullControllerPath($.__controllerPath));
  var selectedAddress = null;




  $.cancel_button.addEventListener('click', dismiss);

  $.continue_button.addEventListener('click', continueCheckout);

  $.suggested_addresses_table.addEventListener('click', selectSuggestedAddress);

  $.entered_address_tile.addEventListener('click', selectEnteredAddress);




  exports.init = init;
  exports.deinit = deinit;









  function init(args) {
    logger.info('init called');
    var givenAddress = args.givenAddress;
    var pickListDisplay = args.pickListDisplay;

    $.givenAddress.clear();
    if (givenAddress) {
      $.givenAddress.set(givenAddress);
    }

    if ($.givenAddress.getAddress2()) {
      $.entered_address_tile.setHeight(60);
      $.entered_address_column.setHeight(60);
      $.given_address2.setHeight(20);
    } else {
      $.entered_address_tile.setHeight(40);
      $.entered_address_column.setHeight(40);
      $.given_address2.setHeight(0);
    }
    removeAllChildren($.suggested_addresses_table);
    $.radio_button.setImage(Alloy.Styles.radioButtonOffImage);

    if (pickListDisplay) {
      $.addresses.reset(pickListDisplay);
      $.suggested_addresses_column_container.show();
      $.suggested_addresses_column_container.setWidth('50%');
      $.entered_address_column_container.setWidth('50%');
      $.subtitle.setText(_L('According to the USPS database...'));
      $.radio_button.show();

      if ($.suggested_addresses_table.getSections().length > 0) {
        var children = $.suggested_addresses_table.getSections()[0].getRows();
        if (children) {
          var rowSelected = children[0];
          rowSelected.select.call(this, true);
          selectedAddress = rowSelected.getAddress.call(this);
          $.continue_button.setEnabled(true);
        }
      }
    } else {
      $.addresses.reset();
      $.suggested_addresses_column_container.hide();
      $.suggested_addresses_column_container.setWidth(0);
      $.entered_address_column_container.setWidth('100%');
      $.subtitle.setText(_L('We were unable to validate your address...'));
      selectedAddress = null;
      $.radio_button.hide();
    }
  }






  function deinit() {
    logger.info('deinit called');
    $.cancel_button.removeEventListener('click', dismiss);
    $.continue_button.removeEventListener('click', continueCheckout);
    $.suggested_addresses_table.removeEventListener('click', selectSuggestedAddress);
    removeAllChildren($.suggested_addresses_table);
    $.entered_address_tile.removeEventListener('click', selectEnteredAddress);
    $.destroy();
  }












  function transformAddress(model) {
    return {
      address1: model.getAddress1(),
      address2: model.getAddress2(),
      city_state_zip: model.getCityStateZip() };

  }









  function dismiss() {
    $.trigger('avsPopover:dismiss');
  }






  function continueCheckout() {
    var data = {};
    if (selectedAddress) {
      data = {
        address1: selectedAddress.get('Address1'),
        address2: selectedAddress.get('Address2'),
        city: selectedAddress.get('city'),
        state: selectedAddress.get('stateCode'),
        postalCode: selectedAddress.get('postalCode') };

    }
    $.trigger('avsPopover:continue', {
      selectedAddress: data });

  }







  function selectSuggestedAddress(event) {
    var address = event.row.getAddress.call(this);
    if ($.suggested_addresses_table.getSections().length > 0) {
      var children = $.suggested_addresses_table.getSections()[0].getRows();
      for (var i = 0; i < children.length; ++i) {
        var child = children[i];
        child.select.call(this, child.getAddress.call(this) === address);
      }
    }
    selectedAddress = address;
    $.radio_button.setImage(Alloy.Styles.radioButtonOffImage);
    $.continue_button.setEnabled(true);
  }






  function selectEnteredAddress() {
    if ($.suggested_addresses_table.getSections().length > 0) {
      var children = $.suggested_addresses_table.getSections()[0].getRows();
      for (var i = 0; i < children.length; ++i) {
        var child = children[i];
        child.select.call(this, false);
      }
    }
    selectedAddress = null;
    $.radio_button.setImage(Alloy.Styles.radioButtonOnImage);
    $.continue_button.setEnabled(true);
  }









  _.extend($, exports);
}

module.exports = Controller;