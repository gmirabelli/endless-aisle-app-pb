var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'components/megaMenu';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};




  $.topCategories = Alloy.createCollection('category');


  $.__views.megaMenu = Ti.UI.createView(
  { top: 105, height: 643, zIndex: 10000, backgroundColor: Alloy.Styles.megaMenu.menuPage.backgroundColor, opacity: 0.95, id: "megaMenu" });

  $.__views.megaMenu && $.addTopLevelView($.__views.megaMenu);
  $.__views.top_nav_area = Ti.UI.createView(
  { layout: "horizontal", top: 0, height: 50, backgroundGradient: { type: "linear", startPoint: { x: "0%", y: "0%" }, endPoint: { x: "0%", y: "100%" }, colors: [{ color: Alloy.Styles.accentColor, offset: 1 }, { color: Alloy.Styles.accentColor, offset: 0 }] }, id: "top_nav_area", dataTransform: "transformCategory" });

  $.__views.megaMenu.add($.__views.top_nav_area);
  var __alloyId97 = Alloy.Collections['$.topCategories'] || $.topCategories;function __alloyId98(e) {if (e && e.fromAdapter) {return;}var opts = __alloyId98.opts || {};var models = __alloyId97.models;var len = models.length;var children = $.__views.top_nav_area.children;for (var d = children.length - 1; d >= 0; d--) {$.__views.top_nav_area.remove(children[d]);}for (var i = 0; i < len; i++) {var __alloyId94 = models[i];__alloyId94.__transform = transformCategory(__alloyId94);var __alloyId96 = Ti.UI.createButton(
      { left: 45, top: 5, color: Alloy.Styles.megaMenu.menu.color, font: Alloy.Styles.megaMenu.menu.font, category_id: __alloyId94.__transform.safeID, title: __alloyId94.__transform.name, accessibilityValue: "menu_title_btn" });

      $.__views.top_nav_area.add(__alloyId96);
    }};__alloyId97.on('fetch destroy change add remove reset', __alloyId98);var __alloyId99 = [];
  $.__views.menu_pages = Ti.UI.createScrollableView(
  { top: 50, scrollingEnabled: false, views: __alloyId99, id: "menu_pages" });

  $.__views.megaMenu.add($.__views.menu_pages);
  var __alloyId107 = Alloy.Collections['$.topCategories'] || $.topCategories;function __alloyId108(e) {if (e && e.fromAdapter) {return;}var opts = __alloyId108.opts || {};var models = __alloyId107.models;var len = models.length;var views = [];for (var i = 0; i < len; i++) {var __alloyId100 = models[i];__alloyId100.__transform = transformCategory(__alloyId100);var __alloyId102 = Ti.UI.createView(
      { width: Ti.UI.FILL, layout: "horizontal", views: __alloyId99, category_id: __alloyId100.__transform.safePageID });

      views.push(__alloyId102);
      var __alloyId104 = Ti.UI.createView(
      { width: 1024, category_id: __alloyId100.__transform.safeSectionID });

      __alloyId102.add(__alloyId104);
      var __alloyId106 = Alloy.createController('components/megaMenuPage', { category_id: __alloyId100.__transform.safeMenuPageID, $model: __alloyId100, __parentSymbol: __alloyId104 });
      __alloyId106.setParent(__alloyId104);
    }$.__views.menu_pages.views = views;};__alloyId107.on('fetch destroy change add remove reset', __alloyId108);exports.destroy = function () {__alloyId97 && __alloyId97.off('fetch destroy change add remove reset', __alloyId98);__alloyId107 && __alloyId107.off('fetch destroy change add remove reset', __alloyId108);};




  _.extend($, $.__views);









  var logger = require('logging')('components:megaMenu', getFullControllerPath($.__controllerPath));

  var current_category_id = null;
  Alloy.Globals.megaMenu = {
    current_dept_id: null };





  exports.init = init;
  exports.deinit = deinit;









  function init() {
    logger.info('INIT called');




    $.listenTo(Alloy.Models.productSearch, 'change:selected_refinements', onChangeRefinements);


    $.top_nav_area.addEventListener('click', onTopNavClick);


    $.menu_pages.addEventListener('click', onMenuPagesClick);


    $.menu_pages.addEventListener('scrollend', onMenuPagesScrollEnd);


    $.listenTo(Alloy.eventDispatcher, 'site_map:show', onSiteMapShow);

    $.listenTo(Alloy.eventDispatcher, 'site_map:hide', hideView);

    $.listenTo(Alloy.eventDispatcher, 'hideAuxillaryViews', hideView);
  }






  function deinit() {
    logger.info('DEINIT called');
    $.top_nav_area.removeEventListener('click', onTopNavClick);
    $.menu_pages.removeEventListener('click', onMenuPagesClick);
    $.menu_pages.removeEventListener('scrollend', onMenuPagesScrollEnd);
    removeAllChildren($.menu_pages);
    $.stopListening();
    $.destroy();
  }












  function getDepartmentLabelByID(cat_id) {
    var matches = _.filter($.top_nav_area.children, function (el) {
      return el.category_id == cat_id;
    });

    return matches ? matches[0] : null;
  }






  function getLeafCategoryLabelByID(cat_id) {}











  function selectDepartment(dept_id) {
    if (dept_id && dept_id != Alloy.Globals.megaMenu.current_dept_id) {
      var deptLabel;
      var newDeptLabel = getDepartmentLabelByID(dept_id);
      if (Alloy.Globals.megaMenu.current_dept_id && newDeptLabel) {
        deptLabel = getDepartmentLabelByID(Alloy.Globals.megaMenu.current_dept_id);
        if (deptLabel) {
          deptLabel.backgroundImage = '';
        }
      }

      deptLabel = newDeptLabel;
      if (deptLabel) {
        deptLabel.backgroundImage = Alloy.Styles.megaMenuDepartmentUnderline;
        Alloy.Globals.megaMenu.current_dept_id = dept_id;
      } else {
        Alloy.Router.navigateToCategory({
          category_id: decodeID(dept_id.substring(1)) }).
        always(function () {
          $.megaMenu.hide();
        });
      }
    }
  }








  function getTopLevelIDs() {
    return _.map($.top_nav_area.children, function (el) {
      return el.category_id;
    });
  }








  function handleDepartmentSelection(dept_id) {
    dept_id = encodeID(dept_id);


    if (dept_id == 'root') {
      dept_id = $.top_nav_area.children[0].id;
    }


    selectDepartment(dept_id);


    var ids = getTopLevelIDs();
    var newIndex = _.indexOf(ids, dept_id);

    if (newIndex > -1) {
      $.menu_pages.setCurrentPage(newIndex);
    }
  }








  function encodeID(id) {
    var encodedID = null;
    if (id) {
      encodedID = id.replace(/-/g, '__');
    }
    return encodedID;
  }








  function decodeID(id) {
    id = '' + id;
    var decodedID = null;
    if (id) {
      decodedID = id.replace(/__/g, '-');
    }
    return decodedID;
  }






  function deselectCurrentLeafCategory() {
    if (current_category_id) {
      var associatedLabel = getLeafCategoryLabelByID(category_id);
      if (associatedLabel) {
        associatedLabel.backgroundImage = '';
      }
    }
  }






  function selectLeafCategory(category_id) {
    if (category_id) {
      var associatedLabel = getLeafCategoryLabelByID(category_id);
      if (associatedLabel) {
        deselectCurrentLeafCategory();
        current_category_id = category_id;
      }
    }
  }








  function handleLeafCategorySelection(category_id) {
    if (category_id) {
      var associatedLabel = getLeafCategoryLabelByID(category_id);
      if (associatedLabel) {
        associatedLabel.animate(Alloy.Animations.bounce);
      }

      Alloy.Router.navigateToCategory({
        category_id: decodeID(category_id) }).
      always(function () {
        $.megaMenu.hide();
      });
    }
  }








  function transformCategory(model) {
    var in_json = model.toJSON();

    in_json.safeID = '_' + in_json.id;
    in_json.safePageID = '_' + in_json.id + 'page';
    in_json.safeSectionID = '_' + in_json.id + 'section';

    return in_json;
  }









  function onChangeRefinements() {
    var selRefs = Alloy.Models.productSearch.get('selected_refinements');
    var category_id = selRefs.cgid;

    selectLeafCategory(category_id);
  }








  function onTopNavClick(event) {
    handleDepartmentSelection(event.source.category_id);
  }








  function onMenuPagesClick(event) {
    event.cancelBubble = true;

    logger.info('event:\n' + JSON.stringify(event));

    handleLeafCategorySelection(event.source.categoryid);
  }








  function onMenuPagesScrollEnd(event) {

    var dept_id = $.top_nav_area.children[event.currentPage].category_id;
    handleDepartmentSelection(dept_id);
  }






  function hideView() {
    if ($.megaMenu.visible) {
      $.megaMenu.hide();
    }
  }








  function onSiteMapShow(event) {
    if ($.megaMenu.visible) {
      $.megaMenu.hide();
      return;
    }
    if ($.topCategories.size() === 0) {
      $.topCategories.reset(Alloy.Globals.categoryTree.getVisibleCategories());
    }
    var dept_id = event.category_id || Alloy.Globals.megaMenu.current_dept_id || $.top_nav_area.children[$.menu_pages.currentPage].category_id;
    if (dept_id) {
      handleDepartmentSelection(dept_id);
    }

    $.megaMenu.show();
  }

  $.megaMenu.hide();









  _.extend($, exports);
}

module.exports = Controller;