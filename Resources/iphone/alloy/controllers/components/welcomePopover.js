var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'components/welcomePopover';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.popover_window = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.color.background.transparent, id: "popover_window" });

  $.__views.popover_window && $.addTopLevelView($.__views.popover_window);
  $.__views.backdrop = Ti.UI.createView(
  { top: 0, left: 0, width: 1024, height: 768, opacity: 0.25, backgroundColor: Alloy.Styles.color.background.black, id: "backdrop" });

  $.__views.popover_window.add($.__views.backdrop);
  $.__views.popover_container = Ti.UI.createView(
  { layout: "vertical", height: Ti.UI.SIZE, top: 200, width: "65%", backgroundColor: Alloy.Styles.color.background.white, id: "popover_container" });

  $.__views.popover_window.add($.__views.popover_container);
  $.__views.welcome_title = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.accentColor, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.titleFont, top: 20, id: "welcome_title", accessibilityValue: "welcome_confirm_title" });

  $.__views.popover_container.add($.__views.welcome_title);
  $.__views.welcome_message = Ti.UI.createLabel(
  { width: Ti.UI.FILL, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.dialogFont, top: 30, left: 50, right: 50, id: "welcome_message", accessibilityValue: "welcome_message" });

  $.__views.popover_container.add($.__views.welcome_message);
  $.__views.country_message = Ti.UI.createLabel(
  { width: Ti.UI.FILL, height: 50, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.dialogFont, verticalAlign: Ti.UI.TEXT_VERTICAL_ALIGNMENT_TOP, textid: "_Country_Message_", bottom: 1, id: "country_message" });

  $.__views.popover_container.add($.__views.country_message);
  $.__views.startup_config = Ti.UI.createView(
  { layout: "vertical", top: 0, left: 0, right: 0, width: Ti.UI.FILL, height: Ti.UI.SIZE, id: "startup_config" });

  $.__views.popover_container.add($.__views.startup_config);
  $.__views.app_settings_view = Alloy.createController('components/appSettingsView', { id: "app_settings_view", __parentSymbol: $.__views.startup_config });
  $.__views.app_settings_view.setParent($.__views.startup_config);
  $.__views.kiosk_message = Ti.UI.createLabel(
  { width: Ti.UI.FILL, height: 0, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.dialogFont, top: 0, left: 50, right: 50, visible: false, id: "kiosk_message", accessibilityValue: "kiosk_message" });

  $.__views.startup_config.add($.__views.kiosk_message);
  $.__views.kiosk_config = Ti.UI.createView(
  { layout: "vertical", top: 0, visible: false, width: Ti.UI.SIZE, height: 0, id: "kiosk_config" });

  $.__views.startup_config.add($.__views.kiosk_config);
  $.__views.change_settings = Ti.UI.createLabel(
  { width: Ti.UI.FILL, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.dialogFont, top: 0, bottom: 30, left: 50, right: 50, id: "change_settings", accessibilityValue: "change_settings" });

  $.__views.startup_config.add($.__views.change_settings);
  $.__views.welcome_button_container = Ti.UI.createView(
  { layout: "horizontal", height: Ti.UI.SIZE, width: Ti.UI.SIZE, bottom: 20, id: "welcome_button_container" });

  $.__views.startup_config.add($.__views.welcome_button_container);
  $.__views.back_button = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, backgroundImage: Alloy.Styles.buttons.secondary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.secondary.backgroundDisabledImage, color: Alloy.Styles.buttons.secondary.color, disabledColor: Alloy.Styles.buttons.secondary.disabledColor, height: 40, width: 200, titleid: "_Back", id: "back_button", accessibilityValue: "welcome_back_button" });

  $.__views.welcome_button_container.add($.__views.back_button);
  $.__views.ok_button = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.buttons.primary.color, disabledColor: Alloy.Styles.buttons.primary.disabledColor, backgroundImage: Alloy.Styles.buttons.primary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.primary.backgroundDisabledImage, height: 40, width: 200, titleid: "_Continue", left: 20, id: "ok_button", accessibilityValue: "welcome_ok_button" });

  $.__views.welcome_button_container.add($.__views.ok_button);
  $.__views.country_config = Ti.UI.createView(
  { layout: "vertical", top: 0, left: 0, right: 0, width: Ti.UI.FILL, height: Ti.UI.SIZE, id: "country_config" });

  $.__views.popover_container.add($.__views.country_config);
  $.__views.country_dropdown = Ti.UI.createView(
  { left: 50, right: 70, layout: "horizontal", width: Ti.UI.FILL, height: 42, id: "country_dropdown" });

  $.__views.country_config.add($.__views.country_dropdown);
  $.__views.country_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, textid: "_Country__", bottom: 14, id: "country_label", accessibilityValue: "country_label" });

  $.__views.country_dropdown.add($.__views.country_label);
  $.__views.country_view = Ti.UI.createView(
  { left: 13, id: "country_view" });

  $.__views.country_dropdown.add($.__views.country_view);
  $.__views.country_dropdown_view = Alloy.createController('components/countryDropDown', { id: "country_dropdown_view", __parentSymbol: $.__views.country_view });
  $.__views.country_dropdown_view.setParent($.__views.country_view);
  $.__views.language_config = Ti.UI.createView(
  { layout: "vertical", top: 0, left: 0, right: 0, width: Ti.UI.FILL, height: Ti.UI.SIZE, id: "language_config" });

  $.__views.popover_container.add($.__views.language_config);
  $.__views.language_dropdown = Ti.UI.createView(
  { top: 10, left: 50, right: 70, layout: "horizontal", width: Ti.UI.FILL, height: 42, id: "language_dropdown" });

  $.__views.language_config.add($.__views.language_dropdown);
  $.__views.language_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, textid: "_Language", bottom: 14, id: "language_label", accessibilityValue: "country_label" });

  $.__views.language_dropdown.add($.__views.language_label);
  $.__views.language_view = Ti.UI.createView(
  { id: "language_view" });

  $.__views.language_dropdown.add($.__views.language_view);
  $.__views.language_dropdown_view = Alloy.createController('components/languageDropDown', { id: "language_dropdown_view", __parentSymbol: $.__views.language_view });
  $.__views.language_dropdown_view.setParent($.__views.language_view);
  $.__views.next_button = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.buttons.primary.color, disabledColor: Alloy.Styles.buttons.primary.disabledColor, backgroundImage: Alloy.Styles.buttons.primary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.primary.backgroundDisabledImage, height: 40, width: 200, top: 17, bottom: 17, titleid: "_Next", id: "next_button", accessibilityValue: "next_button" });

  $.__views.language_config.add($.__views.next_button);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var appSettings = require('appSettings');
  var loadConfigurations = require('appConfiguration').loadConfigurations;
  var showActivityIndicator = require('dialogUtils').showActivityIndicator;
  var EAUtils = require('EAUtils');
  var countryConfig = require('config/countries').countryConfig;
  var logger = require('logging')('components:welcomePopover', getFullControllerPath($.__controllerPath));
  var changesMade = [];


  Alloy.CFG.store_id = '';





  $.ok_button.addEventListener('click', onOKClick);


  $.back_button.addEventListener('click', onBackClick);


  $.next_button.addEventListener('click', onNextClick);




  exports.init = init;
  exports.deinit = deinit;









  function init() {
    logger.info('INIT called');
    $.app_settings_view.init(true);
    $.app_settings_view.setScrollingEnabled(false);
    $.app_settings_view.render();
    $.welcome_title.setText(L('_Welcome_to_Endless_Aisle'));
    $.startup_config.hide();
    $.startup_config.setHeight(0);
    $.welcome_message.hide();
    $.welcome_message.setHeight(0);
    $.next_button.show();
    $.ok_button.setEnabled(false);
    $.country_dropdown_view.init();
    $.language_dropdown_view.init();
    $.next_button.setEnabled(false);
    $.listenTo($.country_dropdown_view, 'country:change', onCountryChange);
    $.listenTo($.language_dropdown_view, 'language:change', onLanguageChange);
  }






  function deinit() {
    logger.info('DEINIT called');
    $.app_settings_view.deinit();
    $.country_dropdown_view.deinit();
    $.language_dropdown_view.deinit();
    $.ok_button.removeEventListener('click', onOKClick);
    $.back_button.removeEventListener('click', onBackClick);
    $.next_button.removeEventListener('click', onNextClick);
    changesMade = [];
    removeAllChildren($.startup_config);
    kioskSettingsDeinit();
    $.stopListening();
    $.destroy();
  }









  function kioskSettingsDeinit() {
    if ($.kiosk_settings) {
      removeAllChildren($.kiosk_config);
      $.kiosk_settings.deinit();
      $.stopListening($.kiosk_settings, 'setting:change', handleChange);
      $.kiosk_settings = null;
    }
  }






  function loadKioskMode() {
    var deferred = new _.Deferred();
    showActivityIndicator(deferred);
    loadConfigurations().done(function () {
      kioskSettingsDeinit();
      if (Alloy.CFG.kiosk_mode.hasOwnProperty('has_credentials') && Alloy.CFG.kiosk_mode.has_credentials) {
        Alloy.CFG.kiosk_mode.username = '<hidden>';
        $.kiosk_settings = Alloy.createController('components/appSettingsView');
        $.kiosk_config.add($.kiosk_settings.getView());
        $.kiosk_settings.init(true, ['kiosk_mode.enabled']);
        $.kiosk_settings.setScrollingEnabled(false);
        $.listenTo($.kiosk_settings, 'setting:change', handleChange);
        $.kiosk_settings.render().always(function () {
          deferred.resolve();
          $.kiosk_config.setVisible(true);
          $.kiosk_message.setVisible(true);
          $.kiosk_config.setHeight(Ti.UI.SIZE);
          $.kiosk_message.setHeight(Ti.UI.SIZE);
        });
      } else {
        $.kiosk_message.setVisible(false);
        $.kiosk_config.setVisible(false);
        $.kiosk_config.setHeight(0);
        deferred.resolve();
      }
    }).fail(function () {
      deferred.resolve();
      var messageString = _L('Reason: ') + _L('Unable to load application settings.');
      Alloy.Dialog.showConfirmationDialog({
        messageString: messageString,
        titleString: _L('Unable to start the application'),
        okButtonString: _L('Retry'),
        hideCancel: true,
        okFunction: function () {
          removeNotify();
          loadKioskMode();
        } });

    });
  }









  function onOKClick() {
    _.each(changesMade, function (change) {
      appSettings.setSetting(change.configName, change.newValue);
    });
    $.trigger('welcomePopover:dismiss');
  }






  function onBackClick() {
    $.welcome_message.setHeight(0);
    $.welcome_message.hide();
    $.startup_config.setHeight(0);
    $.startup_config.hide();
    $.country_message.setHeight(50);
    $.country_message.show();
    $.country_config.setHeight(Ti.UI.SIZE);
    $.country_config.show();
    $.language_config.setHeight(Ti.UI.SIZE);
    $.language_config.show();
    $.country_dropdown_view.updateCountrySelectedItem(Alloy.CFG.countrySelected);
    $.language_dropdown_view.updateLanguageSelectedItem(Alloy.CFG.languageSelected);
  }









  function onCountryChange(event) {
    appSettings.setSetting('countrySelected', event.selectedCountry);
    if (countryConfig[Alloy.CFG.countrySelected]) {
      $.language_dropdown_view.populateLanguages(Alloy.CFG.countrySelected);
      $.language_dropdown_view.updateLanguageSelectedValue(Alloy.CFG.languageSelected);
      $.next_button.setEnabled(true);
    } else {
      Alloy.Dialog.showConfirmationDialog({
        messageString: String.format(_L('There is no corresponding key for the value \'%s\' in countryConfig'), Alloy.CFG.countrySelected),
        titleString: _L('Configuration Error'),
        okButtonString: _L('OK'),
        hideCancel: true,
        okFunction: function () {
          return false;
        } });

      $.language_dropdown_view.populateLanguages();
      $.next_button.setEnabled(false);
    }
  }









  function onLanguageChange(event) {
    if (event) {
      Alloy.CFG.languageSelected = event.selectedLanguage;
      Alloy.CFG.language_ocapi_locale = event.ocapiLocale;
      Alloy.CFG.language_storefront_locale = event.storefrontLocale;
      $.next_button.setEnabled(true);
    } else {
      $.next_button.setEnabled(false);
    }
  }






  function onNextClick() {
    var promise = EAUtils.updateLocaleGlobalVariables(Alloy.CFG.countrySelected);
    showActivityIndicator(promise);
    promise.done(function () {
      $.welcome_message.show();
      $.welcome_message.setHeight(Ti.UI.SIZE);
      $.startup_config.show();
      $.startup_config.setHeight(Ti.UI.SIZE);
      $.country_message.hide();
      $.country_message.setHeight(0);
      $.country_config.hide();
      $.country_config.setHeight(0);
      $.country_message.hide();
      $.language_config.hide();
      $.language_config.setHeight(0);
      appSettings.setSetting('languageSelected', Alloy.CFG.languageSelected);

      Ti.Locale.setLanguage(Alloy.CFG.languageSelected);
      appSettings.setSetting('ocapi.default_locale', Alloy.CFG.language_ocapi_locale);
      appSettings.setSetting('storefront.locale_url', Alloy.CFG.language_storefront_locale);
      $.welcome_title.setText(L('_Welcome_to_Endless_Aisle'));
      $.welcome_message.setText(L('_Welcome_Message_'));
      $.change_settings.setText(L('_Welcome_Message_change_later'));
      $.kiosk_message.setText(L('_Welcome_Kiosk_Message_'));
      $.ok_button.setTitle(L('_Continue'));
      $.language_label.setText(L('_Language'));
      $.country_label.setText(L('_Country__'));
      $.country_message.setText(L('_Country_Message_'));
      $.back_button.setTitle(L('_Back'));
      Alloy.eventDispatcher.trigger('countryChange:selected');
      $.listenTo($.app_settings_view, 'setting:change', handleChange);
    });
  }








  function handleChange(event) {
    logger.info('Change for configName: ' + event.configName + ' new value: ' + event.value);
    var found = _.find(changesMade, function (change) {
      return change.configName == event.configName;
    });
    if (found) {
      var index = changesMade.indexOf(found);
      changesMade.splice(index, 1);
    }
    changesMade.push({
      configName: event.configName,
      newValue: event.value,
      restart: event.restart });


    if (event.configName == 'store_id') {
      Alloy.CFG.store_id = event.value;



      delete Alloy.CFG.kiosk_mode.username;
      loadKioskMode();
      $.ok_button.setEnabled(true);
    }
  }









  _.extend($, exports);
}

module.exports = Controller;