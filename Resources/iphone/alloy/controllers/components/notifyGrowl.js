var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'components/notifyGrowl';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.growl_popover_container = Ti.UI.createView(
  { layout: "absolute", height: 100, width: 500, bottom: 70, borderRadius: 10, borderColor: Alloy.Styles.notifyGrowl.borderColor, touchEnabled: true, opacity: 0.7, backgroundColor: Alloy.Styles.notifyGrowl.backgroundColor, id: "growl_popover_container" });

  $.__views.growl_popover_container && $.addTopLevelView($.__views.growl_popover_container);
  $.__views.growl_label = Ti.UI.createLabel(
  { width: Ti.UI.FILL, height: Ti.UI.FILL, color: Alloy.Styles.notifyGrowl.color, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.notifyGrowlFont, top: 0, bottom: 0, left: 20, right: 20, id: "growl_label", accessibilityValue: "growl_label" });

  $.__views.growl_popover_container.add($.__views.growl_label);
  $.__views.close_button = Ti.UI.createView(
  { width: 32, height: 32, right: 2, top: 2, zIndex: 1, visible: false, backgroundImage: Alloy.Styles.closeButtonClearImage, id: "close_button" });

  $.__views.growl_popover_container.add($.__views.close_button);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var logger = require('logging')('components:notifyGrowl', getFullControllerPath($.__controllerPath));
  var preventAutoClose = false;
  var timeout = Alloy.CFG.notification_display_timeout;
  var dismissTimer = null;




  $.growl_popover_container.addEventListener('click', dismiss);




  exports.init = init;
  exports.deinit = deinit;
  exports.getPreventAutoClose = getPreventAutoClose;
  exports.setPreventAutoClose = setPreventAutoClose;
  exports.setMessage = setMessage;
  exports.getMessage = getMessage;
  exports.dismiss = dismiss;














  function init(args) {
    logger.info('init called');
    var label = args.label || 'No growl message set.';
    $.growl_label.setText(label);

    args.timeout && (timeout = args.timeout);
    preventAutoClose = args.preventAutoClose;

    if (!preventAutoClose) {
      createTimeout(label);
    } else {
      $.close_button.setVisible(true);
    }
  }






  function deinit() {
    logger.info('deinit called ' + $.growl_label.getText());
    dismissTimer && clearTimeout(dismissTimer);
    $.growl_popover_container.removeEventListener('click', dismiss);
    $.stopListening();
    $.destroy();
  }









  function getPreventAutoClose() {
    return preventAutoClose;
  }






  function setPreventAutoClose(enabled) {
    if (enabled) {
      dismissTimer && clearTimeout(dismissTimer);
      $.close_button.setVisible(true);
    }
  }








  function setMessage(message) {
    logger.info('setMessage called ' + message);

    if (message != $.growl_label.getText()) {
      clearTimeout(dismissTimer);
      $.growl_label.setText(message);
      createTimeout(message);
    }
  }






  function getMessage() {
    return $.growl_label.getText();
  }








  function createTimeout(label) {
    logger.info('createTimeout called ' + label);
    dismissTimer = setTimeout(function () {
      logger.info('timeout called ' + label);
      dismiss();
    }, timeout);
  }









  function dismiss() {
    $.trigger('notifyGrowl:dismiss');
  }









  _.extend($, exports);
}

module.exports = Controller;