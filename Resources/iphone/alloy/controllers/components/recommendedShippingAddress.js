var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
		delete obj[key];
	}
	return arg;
}

function Controller() {

	require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'components/recommendedShippingAddress';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};







	$.__views.recommendedShippingAddress = Ti.UI.createTableViewRow(
	{ selectionStyle: Ti.UI.iOS.TableViewCellSelectionStyle.NONE, layout: "vertical", id: "recommendedShippingAddress" });

	$.__views.recommendedShippingAddress && $.addTopLevelView($.__views.recommendedShippingAddress);
	$.__views.address_container = Ti.UI.createView(
	{ layout: "vertical", height: Ti.UI.SIZE, top: 0, id: "address_container" });

	$.__views.recommendedShippingAddress.add($.__views.address_container);
	$.__views.address_tile = Ti.UI.createView(
	{ layout: "horizontal", height: 60, width: "100%", left: 0, top: 10, id: "address_tile" });

	$.__views.address_container.add($.__views.address_tile);
	$.__views.radio_button_container = Ti.UI.createView(
	{ left: 0, width: 20, height: Ti.UI.SIZE, top: 2, id: "radio_button_container" });

	$.__views.address_tile.add($.__views.radio_button_container);
	$.__views.radio_button = Ti.UI.createImageView(
	{ image: Alloy.Styles.radioButtonOffImage, id: "radio_button", accessibilityValue: "radio_button" });

	$.__views.radio_button_container.add($.__views.radio_button);
	$.__views.address_column = Ti.UI.createView(
	{ left: 20, layout: "vertical", width: Ti.UI.SIZE, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, top: 0, id: "address_column" });

	$.__views.address_tile.add($.__views.address_column);
	$.__views.address1 = Ti.UI.createLabel(
	{ width: Ti.UI.SIZE, height: 20, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, ellipsize: true, wordWrap: false, font: Alloy.Styles.detailValueFont, text: $model.__transform.address1, id: "address1", accessibilityValue: "address1" });

	$.__views.address_column.add($.__views.address1);
	$.__views.address2 = Ti.UI.createLabel(
	{ width: Ti.UI.SIZE, height: 20, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, ellipsize: true, wordWrap: false, font: Alloy.Styles.detailValueFont, text: $model.__transform.address2, id: "address2", accessibilityValue: "address2" });

	$.__views.address_column.add($.__views.address2);
	$.__views.city_state_zip = Ti.UI.createLabel(
	{ width: Ti.UI.SIZE, height: 20, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, ellipsize: true, wordWrap: false, font: Alloy.Styles.detailValueFont, text: $model.__transform.city_state_zip, id: "city_state_zip", accessibilityValue: "city_state_zip" });

	$.__views.address_column.add($.__views.city_state_zip);
	exports.destroy = function () {};




	_.extend($, $.__views);










	$.recommendedShippingAddress.select = function (selected) {
		selected ? $.radio_button.setImage(Alloy.Styles.radioButtonOnImage) : $.radio_button.setImage(Alloy.Styles.radioButtonOffImage);
	};

	$.recommendedShippingAddress.getAddress = function () {
		return $model;
	};




	if ($model.getAddress2()) {
		$.address_tile.setHeight(60);
		$.address2.setHeight(20);
	} else {
		$.address_tile.setHeight(40);
		$.address2.setHeight(0);
	}









	_.extend($, exports);
}

module.exports = Controller;