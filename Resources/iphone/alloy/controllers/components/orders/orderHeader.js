var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'components/orders/orderHeader';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};




  $.customer_order = Alloy.createModel('baskets');


  $.__views.order_header_container = Ti.UI.createView(
  { layout: "horizontal", left: 28, height: Ti.UI.SIZE, top: 30, width: "100%", id: "order_header_container" });

  $.__views.order_header_container && $.addTopLevelView($.__views.order_header_container);
  $.__views.order_summary_container = Ti.UI.createView(
  { layout: "vertical", height: Ti.UI.SIZE, width: "60%", top: 0, id: "order_summary_container" });

  $.__views.order_header_container.add($.__views.order_summary_container);
  $.__views.order_date_container = Ti.UI.createView(
  { layout: "horizontal", height: Ti.UI.SIZE, id: "order_date_container" });

  $.__views.order_summary_container.add($.__views.order_date_container);
  $.__views.date_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, bottom: 0, font: Alloy.Styles.detailValueFont, textid: "_Order_Date__", id: "date_label", accessibilityValue: "date_label" });

  $.__views.order_date_container.add($.__views.date_label);
  $.__views.__alloyId125 = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, bottom: 0, font: Alloy.Styles.calloutCopyFont, accessibilityValue: "creation_date", id: "__alloyId125" });

  $.__views.order_date_container.add($.__views.__alloyId125);
  $.__views.order_status_container = Ti.UI.createView(
  { layout: "horizontal", height: Ti.UI.SIZE, id: "order_status_container" });

  $.__views.order_summary_container.add($.__views.order_status_container);
  $.__views.status_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, bottom: 0, font: Alloy.Styles.detailValueFont, textid: "_Status__", id: "status_label", accessibilityValue: "status_label" });

  $.__views.order_status_container.add($.__views.status_label);
  $.__views.order_status_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, bottom: 0, font: Alloy.Styles.detailLabelFont, id: "order_status_label", accessibilityValue: "order_status" });

  $.__views.order_status_container.add($.__views.order_status_label);
  $.__views.order_number_container = Ti.UI.createView(
  { layout: "horizontal", height: Ti.UI.SIZE, id: "order_number_container" });

  $.__views.order_summary_container.add($.__views.order_number_container);
  $.__views.number_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, bottom: 0, font: Alloy.Styles.detailValueFont, textid: "_Order_Number__", id: "number_label", accessibilityValue: "order_number_label" });

  $.__views.order_number_container.add($.__views.number_label);
  $.__views.__alloyId126 = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, bottom: 0, font: Alloy.Styles.detailLabelFont, accessibilityValue: "order_detail_number", id: "__alloyId126" });

  $.__views.order_number_container.add($.__views.__alloyId126);
  $.__views.order_button_container = Ti.UI.createView(
  { layout: "vertical", width: "40%", right: 0, top: 0, height: Ti.UI.SIZE, visible: false, id: "order_button_container" });

  $.__views.order_header_container.add($.__views.order_button_container);
  $.__views.email_button = Ti.UI.createButton(
  { backgroundImage: Alloy.Styles.secondaryButtonImage, color: Alloy.Styles.buttons.primary.color, width: 150, height: 35, font: Alloy.Styles.smallButtonFont, right: 50, id: "email_button", titleid: "_Email_Receipt", accessibilityValue: "email_button" });

  $.__views.order_button_container.add($.__views.email_button);
  $.__views.print_button = Ti.UI.createButton(
  { backgroundImage: Alloy.Styles.secondaryButtonImage, color: Alloy.Styles.buttons.primary.color, width: 150, height: 35, font: Alloy.Styles.smallButtonFont, right: 50, top: 8, id: "print_button", titleid: "_Print_Receipt", accessibilityValue: "print_button" });

  $.__views.order_button_container.add($.__views.print_button);
  var __alloyId127 = function () {$['customer_order'].__transform = _.isFunction($['customer_order'].transform) ? $['customer_order'].transform() : $['customer_order'].toJSON();$.__alloyId125.text = $['customer_order']['__transform']['creation_date'];$.order_status_label.text = $['customer_order']['__transform']['status'];$.__alloyId126.text = $['customer_order']['__transform']['order_no'];};$['customer_order'].on('fetch change destroy', __alloyId127);exports.destroy = function () {$['customer_order'] && $['customer_order'].off('fetch change destroy', __alloyId127);};




  _.extend($, $.__views);










  var storeInfo = Alloy.Models.storeInfo;
  var currentAssociate = Alloy.Models.associate;
  var currentBasket = Alloy.Models.basket;

  if (Alloy.CFG.payment_entry === 'pos') {
    $.pos_cancel_button.setVisible(true);
  }


  var buttonTextLength = 18;




  $.listenTo(Alloy.eventDispatcher, 'order_just_created', orderJustCreatedEventListener);




  $.email_button.addEventListener('click', emailButtonClickEventListener);

  $.print_button.addEventListener('click', printButtonClickEventListener);





  $.listenTo(Alloy.Models.customerOrder, 'sync', function (model) {
    $.customer_order.clear();
    $.customer_order.set(Alloy.Models.customerOrder.toJSON());
    $.order_button_container.hide();
  });




  exports.render = render;
  exports.deinit = deinit;












  function render(order, alwaysShowButtons) {
    $.customer_order.clear({
      silent: true });

    $.customer_order.set(order.toJSON());
    if (Alloy.CFG.payment_entry === 'pos') {
      $.order_status_label.setText(_L('Order created, Payment pending'));
    }
    if (alwaysShowButtons) {
      $.order_button_container.show();
      if (Alloy.CFG.printer_availability) {
        showPrintButtonHelper();
      } else {
        hidePrintButtonHelper();
      }
      if ($.email_button.getTitle().length > buttonTextLength || $.print_button.getTitle().length > buttonTextLength) {
        adjustButtonWidths();
      }
    } else if (Alloy.CFG.order_history_print_button || Alloy.CFG.order_history_email_button) {


      $.order_button_container.show();
      if (Alloy.CFG.order_history_print_button && Alloy.CFG.printer_availability) {
        showPrintButtonHelper();
      } else {
        hidePrintButtonHelper();
      }
      if (Alloy.CFG.order_history_email_button) {
        if ($.email_button.getTitle().length > buttonTextLength || $.print_button.getTitle().length > buttonTextLength) {
          adjustButtonWidths();
        }
      } else {
        $.email_button.setVisible(false);
        $.email_button.setHeight(0);
      }
    } else {
      $.order_button_container.hide();
    }
  }






  function deinit() {
    $.print_button.removeEventListener('click', printButtonClickEventListener);
    $.email_button.removeEventListener('click', emailButtonClickEventListener);
    $.stopListening();
    $.destroy();
  }










  function orderJustCreatedEventListener(order) {
    render(order, true);
  }









  function emailButtonClickEventListener() {
    var promise = Alloy.Models.basket.emailOrder($.customer_order.getOrderNo());
    Alloy.Router.showActivityIndicator(promise);
    promise.done(function () {
      notify(_L('Emailing receipt'));
    }).fail(function () {
      notify(_L('Error sending email'), {
        preventAutoClose: true });

    });
  }






  function printButtonClickEventListener() {
    var receipt = _.extend({}, $.customer_order);
    receipt.set('employee_id', currentAssociate.getEmployeeId());
    receipt.set('employee_name', currentAssociate.getFirstName() + ' ' + currentAssociate.getLastName());
    receipt.set('store_name', storeInfo.getId());
    receipt.set('store_address', storeInfo.constructStoreAddress());

    Alloy.Dialog.showCustomDialog({
      controllerPath: 'checkout/confirmation/printerChooserDialog',
      initOptions: receipt.toJSON(),
      continueEvent: 'printer_chooser:dismiss' });

  }









  function showPrintButtonHelper() {
    $.print_button.setVisible(true);
    $.print_button.setHeight(35);
    $.email_button.setTop(0);
  }






  function hidePrintButtonHelper() {
    $.print_button.setVisible(false);
    $.print_button.setHeight(0);
    $.email_button.setTop(15);
  }






  function adjustButtonWidths() {
    $.email_button.setWidth(170);
    $.print_button.setWidth(170);
  }









  _.extend($, exports);
}

module.exports = Controller;