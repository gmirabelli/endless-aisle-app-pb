var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'components/addressAutocomplete';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};




  $.addressSugesstions = Alloy.createCollection('autocompleteAddressList');


  $.__views.address_autocomplete_view = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.color.background.white, height: 0, width: 0, left: 620, top: 85, layout: "vertical", borderRadius: 10, viewShadowRadius: "10", viewShadowColor: Alloy.Styles.color.background.black, zIndex: 0, id: "address_autocomplete_view" });

  $.__views.address_autocomplete_view && $.addTopLevelView($.__views.address_autocomplete_view);
  $.__views.dismiss_suggestion_button = Ti.UI.createButton(
  { backgroundImage: Alloy.Styles.clearTextImage, top: 5, right: 5, visible: false, id: "dismiss_suggestion_button", accessibilityLabel: "close_icon" });

  $.__views.address_autocomplete_view.add($.__views.dismiss_suggestion_button);
  dismissSuggestions ? $.addListener($.__views.dismiss_suggestion_button, 'click', dismissSuggestions) : __defers['$.__views.dismiss_suggestion_button!click!dismissSuggestions'] = true;$.__views.address_sugestions_table = Ti.UI.createTableView(
  { height: 220, width: Ti.UI.FILL, backgroundColor: Alloy.Styles.color.background.white, visible: false, id: "address_sugestions_table" });

  $.__views.address_autocomplete_view.add($.__views.address_sugestions_table);
  var __alloyId75 = Alloy.Collections['$.addressSugesstions'] || $.addressSugesstions;function __alloyId76(e) {if (e && e.fromAdapter) {return;}var opts = __alloyId76.opts || {};var models = __alloyId75.models;var len = models.length;var rows = [];for (var i = 0; i < len; i++) {var __alloyId72 = models[i];__alloyId72.__transform = _.isFunction(__alloyId72.transform) ? __alloyId72.transform() : __alloyId72.toJSON();var __alloyId73 = Ti.UI.createTableViewRow(
      { height: 50, backgroundColor: Alloy.Styles.color.background.white });

      rows.push(__alloyId73);
      getAddressDetails ? $.addListener(__alloyId73, 'click', getAddressDetails) : __defers['__alloyId73!click!getAddressDetails'] = true;var __alloyId74 = Ti.UI.createLabel(
      { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, lines: 1, left: 10, right: 10, text: __alloyId72.__transform.description });

      __alloyId73.add(__alloyId74);
    }$.__views.address_sugestions_table.setData(rows);};__alloyId75.on('fetch destroy change add remove reset', __alloyId76);exports.destroy = function () {__alloyId75 && __alloyId75.off('fetch destroy change add remove reset', __alloyId76);};




  _.extend($, $.__views);









  var googleAddressDetails = $.args;
  var logger = require('logging')('components:addressAutoComplete', getFullControllerPath($.__controllerPath));
  var googlePlaceDetails = require('googlePlaceDetails')(googleAddressDetails);





  $.listenTo(Alloy.eventDispatcher, 'hideAuxillaryViews', function () {
    hideView();
  });



  exports.init = init;
  exports.deinit = deinit;
  exports.hideView = hideView;
  exports.showView = showView;
  exports.returnAddressCollection = returnAddressCollection;
  exports.isVisible = isVisible;









  function init() {
    logger.info('init called');
    Alloy.Dialog.getWindow().add($.address_autocomplete_view);
  }






  function deinit() {
    logger.info('DEINIT called');
    $.dismiss_suggestion_button.removeEventListener('click', dismissSuggestions);
    if ($.address_sugestions_table.getSections().length > 0) {
      _.each($.address_sugestions_table.getSections()[0].getRows(), function (row) {
        row.removeEventListener('click', getAddressDetails);
      });
    }
    $.stopListening();
    $.destroy();
  }










  function showView(customId) {
    if ($.addressSugesstions.length == 0) {
      hideView();
      return;
    }
    if ($.address_autocomplete_view.getVisible()) {
      return;
    }
    $.address_autocomplete_view.setHeight(1);
    if (customId == 'customer') {
      $.address_autocomplete_view.setLeft(10);
    }
    $.address_autocomplete_view.setWidth(390);

    var animation = Ti.UI.createAnimation({
      height: 270,
      curve: Ti.UI.ANIMATION_CURVE_EASE_IN_OUT,
      duration: 500,
      zIndex: 100 });


    var animationHandler = function () {
      animation.removeEventListener('complete', animationHandler);
      $.dismiss_suggestion_button.setVisible(true);
      $.address_sugestions_table.setVisible(true);
    };

    animation.addEventListener('complete', animationHandler);

    $.address_autocomplete_view.setVisible(true);
    $.address_autocomplete_view.animate(animation);
  }






  function hideView() {
    $.dismiss_suggestion_button.setVisible(false);
    $.address_sugestions_table.setVisible(false);
    $.address_autocomplete_view.setHeight(0);
    $.address_autocomplete_view.setWidth(0);
    $.address_autocomplete_view.setVisible(false);
  }







  function returnAddressCollection() {
    return $.addressSugesstions;
  }










  function getAddressDetails(event) {
    googlePlaceDetails.getPlaceDetails({
      reference: $.addressSugesstions.at(event.index).getReference() });

    hideView();
    Alloy.eventDispatcher.trigger('hideAuxillaryViews');
  }






  function dismissSuggestions() {
    hideView();
    $.trigger('addressSuggestionsCancelBtnClick');
  }







  function isVisible() {
    return $.address_autocomplete_view.getVisible();
  }





  __defers['$.__views.dismiss_suggestion_button!click!dismissSuggestions'] && $.addListener($.__views.dismiss_suggestion_button, 'click', dismissSuggestions);__defers['__alloyId73!click!getAddressDetails'] && $.addListener(__alloyId73, 'click', getAddressDetails);



  _.extend($, exports);
}

module.exports = Controller;