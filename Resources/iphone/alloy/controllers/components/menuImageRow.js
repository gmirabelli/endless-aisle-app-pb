var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
		delete obj[key];
	}
	return arg;
}

function Controller() {

	require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'components/menuImageRow';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};







	$.__views.menu_image_row = Ti.UI.createTableViewRow(
	{ layout: "horizontal", id: "menu_image_row" });

	$.__views.menu_image_row && $.addTopLevelView($.__views.menu_image_row);
	$.__views.menu_left = Ti.UI.createView(
	{ layout: "vertical", width: 255, id: "menu_left" });

	$.__views.menu_image_row.add($.__views.menu_left);
	$.__views.menu_label = Ti.UI.createLabel(
	{ width: "100%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 15, id: "menu_label" });

	$.__views.menu_left.add($.__views.menu_label);
	$.__views.menu_sublabel = Ti.UI.createLabel(
	{ width: "100%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 15, font: Alloy.Styles.smallButtonFont, id: "menu_sublabel" });

	$.__views.menu_left.add($.__views.menu_sublabel);
	$.__views.menu_image = Ti.UI.createImageView(
	{ height: 40, width: 40, id: "menu_image" });

	$.__views.menu_image_row.add($.__views.menu_image);
	exports.destroy = function () {};




	_.extend($, $.__views);










	var args = arguments[0] || {};
	var submenuLabel = args.submenuLabel;
	var label = args.label;
	var image = args.image;
	var properties = args.properties;



	if (image) {
		$.menu_image.setImage(image);
	}
	if (submenuLabel) {
		$.menu_sublabel.setText(submenuLabel);
		$.menu_label.setTop(10);
	} else {
		$.menu_sublabel.hide();
		$.menu_sublabel.setHeight(0);
		$.menu_label.setHeight('100%');
	}
	if (label) {
		$.menu_label.setText(label);
	}
	if (properties) {
		$.menu_image_row.applyProperties(properties);
	}









	_.extend($, exports);
}

module.exports = Controller;