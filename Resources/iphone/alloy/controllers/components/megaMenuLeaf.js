var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
		delete obj[key];
	}
	return arg;
}

function Controller() {

	require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'components/megaMenuLeaf';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};




	$.leafCategories = Alloy.createCollection('category');


	$.__views.leaf_container = Ti.UI.createView(
	{ layout: "vertical", width: 234, id: "leaf_container" });

	var __alloyId113 = Alloy.Collections['$.leafCategories'] || $.leafCategories;function __alloyId114(e) {if (e && e.fromAdapter) {return;}var opts = __alloyId114.opts || {};var models = __alloyId113.models;var len = models.length;var children = $.__views.leaf_container.children;for (var d = children.length - 1; d >= 0; d--) {$.__views.leaf_container.remove(children[d]);}for (var i = 0; i < len; i++) {var __alloyId109 = models[i];__alloyId109.__transform = _.isFunction(__alloyId109.transform) ? __alloyId109.transform() : __alloyId109.toJSON();var __alloyId111 = Ti.UI.createView(
			{ bottom: 5, height: 35, left: 35, width: Ti.UI.FILL, layout: "horizontal" });

			$.__views.leaf_container.add(__alloyId111);
			var __alloyId112 = Ti.UI.createLabel(
			{ width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.megaMenu.menuLeaf.color, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.megaMenu.menuLeaf.font, categoryid: __alloyId109.__transform.id, text: __alloyId109.__transform.name, accessibilityValue: "menu_leaf" });

			__alloyId111.add(__alloyId112);
		}};__alloyId113.on('fetch destroy change add remove reset', __alloyId114);$.__views.leaf_container && $.addTopLevelView($.__views.leaf_container);
	exports.destroy = function () {__alloyId113 && __alloyId113.off('fetch destroy change add remove reset', __alloyId114);};




	_.extend($, $.__views);










	var args = arguments[0] || {};

	$.leafCategories.reset($model.getVisibleCategories());




	$.listenTo(Alloy.Models.productSearch, 'change:selected_refinements', function () {
		var selRefs = Alloy.Models.productSearch.getSelectedRefinements();
		var category_id = selRefs.cgid;
		_.each($.leaf_container.children, function (el) {
			var child = el.children[0];
			if (child.categoryid == category_id) {
				child.setBackgroundImage(Alloy.Styles.megaMenuCategoryUnderline);
			} else {
				child.setBackgroundImage('');
			}
		});
	});









	_.extend($, exports);
}

module.exports = Controller;