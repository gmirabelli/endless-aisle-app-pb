var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'search/components/mediumSwatchRefinement';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.mediumSwatchRefinementHeader = Ti.UI.createView(
  { height: 50, layout: "absolute", backgroundColor: Alloy.Styles.color.background.light, id: "mediumSwatchRefinementHeader" });

  $.__views.mediumSwatchRefinementHeaderLabel = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.calloutCopyFont, left: 22, id: "mediumSwatchRefinementHeaderLabel", accessibilityValue: "medium_swatch_refinement_header_label" });

  $.__views.mediumSwatchRefinementHeader.add($.__views.mediumSwatchRefinementHeaderLabel);
  $.__views.mediumSwatchRefinement = Ti.UI.createTableViewSection(
  { headerView: $.__views.mediumSwatchRefinementHeader, id: "mediumSwatchRefinement" });

  $.__views.mediumSwatchRefinement && $.addTopLevelView($.__views.mediumSwatchRefinement);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var listenerViews = [];
  var logger = require('logging')('search:components:mediumSwatchRefinement', getFullControllerPath($.__controllerPath));

  var psr = Alloy.Models.productSearch;
  var refinement = $model;

  var visibleValues = 0;
  var onViewClick;




  exports.deinit = deinit;
  exports.init = init;
  exports.shouldBeVisible = shouldBeVisible;









  function init() {

    var values,
    rows = [],
    row,
    view,
    label;

    values = refinement.getValues();
    $.mediumSwatchRefinementHeaderLabel.setText(refinement.getLabel());


    while ($.mediumSwatchRefinement.rowCount > 0) {
      $.mediumSwatchRefinement.remove($.mediumSwatchRefinement.rows[0]);
    }

    row = Ti.UI.createTableViewRow({
      layout: 'horizontal',
      touchEnabled: false,
      allowsSelection: false,
      selectionStyle: Ti.UI.iOS.TableViewCellSelectionStyle.NONE });

    visibleValues = 0;

    if (values) {
      _.each(values, function (value) {
        var attribute_id = refinement.getAttributeId();
        var value_id = value.getValue();
        var hit_count = value.getHitCount();
        var swatchProperty = Alloy.CFG.product_search.refinements.colorForPresentationID[value.getPresentationId() || 'default'];
        var isRefinedBy = psr.isRefinedBy(attribute_id, value_id);

        if (hit_count == 0) {
          return;
        }
        visibleValues++;
        view = Ti.UI.createView({
          width: 90,
          height: 45,
          backgroundColor: isRefinedBy ? Alloy.Styles.accentColor : Alloy.Styles.color.background.medium,
          isRefinedBy: isRefinedBy,
          accessibilityLabel: 'select_' + value_id,
          left: 10,
          top: 20,
          bottom: 10,
          layout: 'absolute' });

        label = Ti.UI.createLabel({
          text: value.getLabel(),
          font: Alloy.Styles.lineItemLabelFont,
          color: isRefinedBy ? Alloy.Styles.color.text.white : Alloy.Styles.color.text.mediumdark });

        view.add(label);
        row.add(view);

        onViewClick = function (event) {
          event.cancelBubble = true;
          handleSwatchClick({
            attribute_id: attribute_id,
            value_id: value_id });

        };

        view.addEventListener('click', onViewClick);
        listenerViews.push(view);
      });
      $.mediumSwatchRefinement.add(row);
    }
  }






  function deinit() {
    logger.info('DEINIT called');
    deinitExistingRows();
    $.stopListening();
    $.destroy();
  }









  function shouldBeVisible() {
    return visibleValues > 1;
  }






  function deinitExistingRows() {
    var view = null;
    removeAllChildren($.mediumSwatchRefinement);
    _.each(listenerViews, function (view) {
      view.removeEventListener('click', onViewClick);
    });
  }









  function handleSwatchClick(event) {
    event.cancelBubble = true;
    logger.info('triggering refinement:select: ' + JSON.stringify(event));
    $.mediumSwatchRefinement.fireEvent('refinement:toggle', event);
  }




  init();









  _.extend($, exports);
}

module.exports = Controller;