var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'search/components/categoryGrid';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.categoryGrid = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.color.background.white, top: 0, layout: "absolute", id: "categoryGrid" });

  $.__views.categoryGrid && $.addTopLevelView($.__views.categoryGrid);
  exports.destroy = function () {};




  _.extend($, $.__views);











  var args = arguments[0] || {};
  var $model = $model || args.category;
  var logger = require('logging')('search:components:categoryGrid', getFullControllerPath($.__controllerPath));
  var allTiles = [];




  exports.deinit = deinit;










  function render(options) {
    logger.info('RENDER called');
    var subcategories = $model.getVisibleCategories();

    if (!subcategories) {
      return;
    }
    var subcatLength = subcategories.length;



    var grid_config = Alloy.CFG.category_grid;
    var aspect = grid_config.aspect;

    if (Alloy.isTablet) {
      grid_config = grid_config.tablet;
    } else {
      grid_config = grid_config.phone;
    }
    if (options.portrait) {
      grid_config = grid_config.portrait;
    } else {
      grid_config = grid_config.landscape;
    }

    var spacing = grid_config.spacing,
    min_padding = grid_config.min_padding,
    max_cols = grid_config.max_cols;


    var num_cols = Math.ceil(Math.sqrt(subcatLength));
    var num_rows = Math.ceil(subcatLength / num_cols),
    row,
    col;
    var remainder = subcatLength % num_cols;


    if (remainder == 1) {
      num_cols += 1;
    }


    if (num_cols > max_cols) {
      num_cols = max_cols;
    } else if (num_cols < 1) {
      num_cols = 1;
    }

    if (num_cols > subcatLength) {
      num_cols = subcatLength;
    }


    num_rows = Math.ceil(subcatLength / num_cols);


    var parent_width = grid_config.max_bounds.width;
    var parent_height = grid_config.max_bounds.height;


    var total_width = num_cols * (aspect.width + spacing.width) - spacing.width;
    var total_height = num_rows * (aspect.height + spacing.height) - spacing.height;
    var bound_width = parent_width - 2 * min_padding.width;



    var scalar_width = 1,
    scalar_height = 1,
    scalar = 1,
    fit_height = grid_config.map_to_height;

    if (total_width > bound_width) {
      scalar_width = bound_width / total_width;
    }
    var mapped_height = total_height * scalar_width;
    var bound_height = parent_height - 2 * min_padding.height;
    if (fit_height && mapped_height > bound_height) {
      scalar_height = bound_height / mapped_height;
      bound_width = bound_width * scalar_height;
    } else {

      bound_height = mapped_height;
    }
    scalar = scalar_height * scalar_width;
    var scaled_aspect = {
      width: aspect.width * scalar,
      height: aspect.height * scalar };


    var offsetX = (parent_width - bound_width) / 2;
    var offsetY = (parent_height - bound_height) / 2;

    var scroller_height = parent_height;
    if (bound_height > parent_height) {
      scroller_height = bound_height + 2 * min_padding.height;
      offsetY = min_padding.height;
    }

    var tileView, regionDef, subcategory;
    for (var i = 0, ii = subcatLength; i < ii; i++) {
      row = Math.floor(i / num_cols);
      col = i % num_cols;

      subcategory = subcategories[i];

      if (subcategory) {

        if ($.categoryGrid.children.length > i) {
          tileView = $.categoryGrid.children[i];
        } else {
          tileController = Alloy.createController('search/components/categoryTile', {
            category: subcategory,
            index: i });

          tileView = tileController.getView();
          allTiles.push(tileController);
        }


        tileView.left = offsetX + col * scalar * (aspect.width + spacing.width);
        tileView.top = offsetY + row * scalar * (aspect.height + spacing.height);
        tileView.width = scaled_aspect.width;
        tileView.height = scaled_aspect.height;


        if ($.categoryGrid.children.length <= i) {
          $.categoryGrid.add(tileView);
        }
      }
    }


    $.categoryGrid.setHeight(bound_height < parent_height ? parent_height : bound_height + 2 * scalar * min_padding.height);
  }






  function deinit() {
    logger.info('DEINIT called');
    _.each(allTiles, function (tileController) {
      tileController.deinit();
    });
    removeAllChildren($.categoryGrid);
    allTiles = [];
    $.destroy();
  }





  if ($model && !$model.isNew()) {
    render({
      type: 'orient',
      portrait: false });

  }









  _.extend($, exports);
}

module.exports = Controller;