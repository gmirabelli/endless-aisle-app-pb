var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'search/components/listItemRefinement';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.listItemRefinementHeader = Ti.UI.createView(
  { height: 50, layout: "absolute", backgroundColor: Alloy.Styles.color.background.light, id: "listItemRefinementHeader" });

  $.__views.listItemRefinementHeaderLabel = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.calloutCopyFont, left: 22, id: "listItemRefinementHeaderLabel", accessibilityValue: "list_item_refinement_header_label" });

  $.__views.listItemRefinementHeader.add($.__views.listItemRefinementHeaderLabel);
  $.__views.listItemRefinement = Ti.UI.createTableViewSection(
  { headerView: $.__views.listItemRefinementHeader, id: "listItemRefinement" });

  $.__views.listItemRefinement && $.addTopLevelView($.__views.listItemRefinement);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var listenerViews = [];
  var logger = require('logging')('search:components:listItemRefinement', getFullControllerPath($.__controllerPath));

  var psr = Alloy.Models.productSearch;
  var refinement = $model;

  var visibleValues = 0;
  var onRowClick;




  exports.deinit = deinit;
  exports.init = init;
  exports.shouldBeVisible = shouldBeVisible;









  function init() {
    logger.info('INIT called');
    var values,
    rows = [],
    row,
    hit_count,
    view,
    label;

    values = refinement.getValues();

    $.listItemRefinementHeaderLabel.setText(refinement.getLabel());


    deinitExistingRows();

    visibleValues = 0;

    if (values) {
      _.each(values, function (value) {
        var attribute_id = refinement.getAttributeId();
        var value_id = value.getValue();
        var hit_count = value.getHitCount();
        var swatchProperty = Alloy.CFG.product_search.refinements.colorForPresentationID[value.getPresentationId() || 'default'];
        var isRefinedBy = psr.isRefinedBy(attribute_id, value_id);

        if (hit_count == 0) {
          return;
        }
        visibleValues++;

        var isSelected = psr.isRefinedBy(attribute_id, value.getValue());
        row = Ti.UI.createTableViewRow({
          hasCheck: isSelected,
          height: 50,
          layout: 'absolute',
          selectedBackgroundColor: Alloy.Styles.accentColor,
          rightImage: isSelected ? 'images/icons/green_checkmark.png' : null,
          accessibilityLabel: 'select_' + value_id,
          isRefinedBy: isRefinedBy });

        label = Ti.UI.createLabel({
          text: value.getLabel(),
          font: Alloy.Styles.detailLabelFont,
          left: 42,
          highlightedColor: Alloy.Styles.color.text.white,
          color: Alloy.Styles.color.text.dark });

        row.add(label);
        $.listItemRefinement.add(row);

        onRowClick = function (event) {
          event.cancelBubble = true;
          handleRefinementClick({
            attribute_id: attribute_id,
            value_id: value_id });

        };

        row.addEventListener('click', onRowClick);
        listenerViews.push(row);
      });
    }
  }






  function deinit() {
    logger.info('DEINIT called');
    deinitExistingRows();
    $.stopListening();
    $.destroy();
  }









  function shouldBeVisible() {
    return visibleValues > 1;
  }






  function deinitExistingRows() {
    var view = null;
    removeAllChildren($.listItemRefinement);
    _.each(listenerViews, function (view) {
      view.removeEventListener('click', onRowClick);
    });
  }









  function handleRefinementClick(event) {
    event.cancelBubble = true;
    logger.info('triggering refinement:select: ' + JSON.stringify(event));
    $.listItemRefinement.fireEvent('refinement:toggle', event);
  }




  init();









  _.extend($, exports);
}

module.exports = Controller;