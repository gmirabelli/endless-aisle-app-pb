var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'search/components/sortingOptionsPanel';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.sortingOptionsPanel = Ti.UI.createView(
  { layout: "absolute", visible: false, top: 0, left: 752, bottom: 0, right: 0, zIndex: 2, id: "sortingOptionsPanel" });

  $.__views.sortingOptionsPanel && $.addTopLevelView($.__views.sortingOptionsPanel);
  $.__views.__alloyId244 = Ti.UI.createView(
  { left: 0, top: 0, width: 8, backgroundImage: Alloy.Styles.rightShadowImage, zIndex: 4, height: "100%", id: "__alloyId244" });

  $.__views.sortingOptionsPanel.add($.__views.__alloyId244);
  $.__views.sortingOptionsTableHeader = Ti.UI.createView(
  { height: 61, width: "100%", layout: "absolute", backgroundColor: Alloy.Styles.color.background.light, id: "sortingOptionsTableHeader" });

  $.__views.sortingOptionsTableHeaderLabel = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: 61, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.smallAccentFont, left: 13, backgroundColor: Alloy.Styles.color.background.transparent, id: "sortingOptionsTableHeaderLabel", accessibilityValue: "sorting_options_header_label", textid: "_Sorting_Options" });

  $.__views.sortingOptionsTableHeader.add($.__views.sortingOptionsTableHeaderLabel);
  $.__views.sortingOptionsTable = Ti.UI.createTableView(
  { top: 0, left: 8, width: 264, backgroundColor: Alloy.Styles.color.background.light, separatorStyle: Ti.UI.TABLE_VIEW_SEPARATOR_STYLE_NONE, color: Alloy.Styles.color.text.dark, zIndex: 4, borderWidth: 1, borderColor: Alloy.Styles.color.border.lightest, headerView: $.__views.sortingOptionsTableHeader, id: "sortingOptionsTable" });

  $.__views.sortingOptionsPanel.add($.__views.sortingOptionsTable);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var eaUtils = require('EAUtils');
  var currentSearch = Alloy.Models.productSearch;
  var logger = require('logging')('search:components:sortingOptionsPanel', getFullControllerPath($.__controllerPath));




  $.sortingOptionsTable.addEventListener('click', onSortClick);




  $.listenTo(currentSearch, 'change:sorting_options', render);




  exports.deinit = deinit;









  function deinit() {
    logger.info('DEINIT called');
    $.sortingOptionsTable.removeEventListener('click', onSortClick);
    $.stopListening();
    $.destroy();
  }






  function render() {
    logger.info('render called');
    var sortingOptions = currentSearch.getSortingOptionsCollection(),
    sortingOption,
    sortingOptionID,
    rows = [],
    row,
    view,
    label;
    var selectedSortOption = currentSearch.getSelectedSortingOption();
    var selectedSortOptionID = selectedSortOption ? selectedSortOption.getId() : null;
    for (var i = 0, ii = sortingOptions.length; i < ii; i++) {
      sortingOption = sortingOptions.at(i);
      sortingOptionID = sortingOption.getId();

      row = Ti.UI.createTableViewRow({
        option_id: sortingOptionID,
        height: 50,
        layout: 'absolute',
        selectedBackgroundColor: Alloy.Styles.accentColor });

      view = Ti.UI.createView({
        backgroundImage: Alloy.Styles.categoryOptionImage,
        opacity: '0.9',
        top: 0,
        left: 0,
        height: '100%',
        width: '100%',
        accessibilityLabel: 'sort_' + sortingOptionID });


      label = Ti.UI.createLabel({
        text: sortingOption.getLabel(),
        left: 26,
        font: Alloy.Styles.detailLabelFont,
        color: selectedSortOptionID == sortingOptionID ? Alloy.Styles.accentColor : Alloy.Styles.color.text.dark,
        highlightedColor: Alloy.Styles.color.text.dark });

      view.add(label);
      row.add(view);

      rows.push(row);
    }

    $.sortingOptionsTable.setData(rows);
  }











  function onSortClick(event) {
    event.cancelBubble = true;
    var option_id = event.rowData.option_id;

    currentSearch.setSelectedSortingOption(option_id, {
      silent: true });

    eaUtils.doProductSearch();
  }




  render();









  _.extend($, exports);
}

module.exports = Controller;