var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'product/images/imageZoom';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.imageZoom = Ti.UI.createView(
  { layout: "absolute", backgroundColor: Alloy.Styles.color.background.transparent, zIndex: 101, id: "imageZoom" });

  $.__views.imageZoom && $.addTopLevelView($.__views.imageZoom);
  $.__views.backdrop = Ti.UI.createView(
  { top: 0, left: 0, width: 1024, height: 768, opacity: 0.55, backgroundColor: Alloy.Styles.color.background.black, zIndex: 102, id: "backdrop" });

  $.__views.imageZoom.add($.__views.backdrop);
  $.__views.zoom_container = Ti.UI.createView(
  { layout: "absolute", backgroundColor: Alloy.Styles.color.background.white, width: "93%", height: "91%", zIndex: 103, id: "zoom_container" });

  $.__views.imageZoom.add($.__views.zoom_container);
  $.__views.close_button = Ti.UI.createButton(
  { width: 30, height: 31, right: 15, top: 15, zIndex: 1, backgroundImage: Alloy.Styles.clearTextImage, id: "close_button", accessibilityLabel: "zoom_close_button" });

  $.__views.zoom_container.add($.__views.close_button);
  $.__views.detail_image_container = Ti.UI.createScrollView(
  { contentWidth: 1, contentHeight: 1, height: 700, width: 700, oldZoom: 1, minZoomScale: "1.0", maxZoomScale: "2.0", showVerticalScrollIndicator: true, showHorizontalScrollIndicator: true, layout: "absolute", top: 0, bottom: 0, left: 0, right: 0, id: "detail_image_container" });

  $.__views.zoom_container.add($.__views.detail_image_container);
  $.__views.detail_image = Ti.UI.createImageView(
  { height: 700, width: Ti.UI.SIZE, id: "detail_image", accessibilityValue: "detail_image" });

  $.__views.detail_image_container.add($.__views.detail_image);
  $.__views.vertical_separator = Ti.UI.createView(
  { width: 2, backgroundColor: Alloy.Styles.color.background.dark, height: "100%", top: 0, left: 700, id: "vertical_separator" });

  $.__views.zoom_container.add($.__views.vertical_separator);
  $.__views.control_column = Ti.UI.createView(
  { width: 230, left: 720, height: Ti.UI.SIZE, top: 46, right: 0, layout: "vertical", scrollingEnabled: true, showVerticalScrollIndicator: true, id: "control_column" });

  $.__views.zoom_container.add($.__views.control_column);
  $.__views.__alloyId211 = Ti.UI.createView(
  { width: Ti.UI.FILL, height: Ti.UI.SIZE, layout: "horizontal", id: "__alloyId211" });

  $.__views.control_column.add($.__views.__alloyId211);
  $.__views.color_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, textid: "_Color_", font: Alloy.Styles.detailLabelFont, visible: false, id: "color_label", accessibilityValue: "zoom_color_label" });

  $.__views.__alloyId211.add($.__views.color_label);
  $.__views.color_name = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailValueFont, left: 5, id: "color_name", accessibilityValue: "zoom_color_name" });

  $.__views.__alloyId211.add($.__views.color_name);
  $.__views.swatches_container = Ti.UI.createScrollView(
  { layout: "absolute", height: 240, top: 0, right: 0, id: "swatches_container" });

  $.__views.control_column.add($.__views.swatches_container);
  $.__views.alt_swatches_box = Ti.UI.createView(
  { layout: "horizontal", width: Ti.UI.FILL, height: Ti.UI.SIZE, top: 0, id: "alt_swatches_box" });

  $.__views.swatches_container.add($.__views.alt_swatches_box);
  $.__views.__alloyId212 = Ti.UI.createView(
  { width: Ti.UI.FILL, height: Ti.UI.SIZE, layout: "horizontal", id: "__alloyId212" });

  $.__views.control_column.add($.__views.__alloyId212);
  $.__views.other_views_label = Ti.UI.createLabel(
  { width: 210, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, textid: "_Other_Views_", font: Alloy.Styles.detailLabelFont, id: "other_views_label", accessibilityValue: "other_views_label" });

  $.__views.__alloyId212.add($.__views.other_views_label);
  $.__views.alt_images_container = Ti.UI.createScrollView(
  { height: Ti.UI.FILL, bottom: 0, layout: "absolute", id: "alt_images_container" });

  $.__views.control_column.add($.__views.alt_images_container);
  $.__views.alt_center_box = Ti.UI.createView(
  { layout: "horizontal", width: Ti.UI.FILL, height: Ti.UI.SIZE, top: 0, id: "alt_center_box" });

  $.__views.alt_images_container.add($.__views.alt_center_box);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var selectedVariationValue = null;
  var selectedAlternateImage = 0;
  var swatchListeners = [];
  var altImageListeners = [];

  var logger = require('logging')('product:images:imageZoom', getFullControllerPath($.__controllerPath));




  $.close_button.addEventListener('click', dismiss);
  $.backdrop.addEventListener('click', dismiss);
  $.detail_image_container.addEventListener('pinch', onImagePinch);
  $.detail_image_container.addEventListener('touchend', onImageTouchEnd);




  exports.init = init;
  exports.deinit = deinit;










  function init(options) {
    logger.info('init called');
    var deferred = new _.Deferred();
    var product_id = options.product_id;
    selectedVariationValue = options.selected_variation_value;
    selectedAlternateImage = options.selected_alternate_image ? options.selected_alternate_image : 0;
    $model = options.model;
    if (product_id) {

      $model = Alloy.createModel('product');
      $model.id = product_id;
      $.detail_image_container.zoomScale = 1.0;
      $model.fetchModel().always(function (model) {
        render();
        deferred.resolve();
      });
    } else {

      render();
      deferred.resolve();
    }
    return deferred.promise();
  }






  function render() {
    logger.info('render called');

    cleanupView();

    $model.ensureImagesLoaded('altImages').done(function () {
      $model.ensureImagesLoaded('altZoomImages').done(function () {
        $model.ensureImagesLoaded('swatchImages').done(function () {
          var swatchGroups = $model.getSwatchImageGroups();


          if (swatchGroups && swatchGroups.length > 0) {

            var vas = $model.getVariationAttribute(Alloy.CFG.product.color_attribute);
            var vasModels = vas.getValues();
            var vasIndex = 0;
            if (selectedVariationValue) {
              for (var i = 0; i < vasModels.length; i++) {
                if (selectedVariationValue == vasModels[i].getValue()) {
                  vasIndex = i;
                  break;
                }
              }
            } else {
              selectedVariationValue = vasModels[0].getValue();
            }

            $.color_label.setVisible(true);
            $.color_name.setText(vasModels[vasIndex].getName());

            _.each(swatchGroups, function (group, i) {

              var var_attributes = group.getVariationAttributes()[0];
              var vv = var_attributes && var_attributes.getValues()[0].getValue();
              if (swatchGroups.length == 1 || vv) {
                var images = group.getImages();

                var validValue = false;
                for (var j = 0; j < vasModels.length; j++) {
                  if (vv == vasModels[j].getValue()) {
                    validValue = true;
                    break;
                  }
                }

                if (validValue) {
                  var altImages = $model.getAltImages(vv);
                  var swatchURL = images[0].getLink();
                  var alt_images = _.map(altImages, function (n) {
                    return n.getLink();
                  });

                  var swatchContainer = Ti.UI.createView($.createStyle({
                    classes: ['swatch_container_view'],
                    apiName: 'View',
                    borderColor: vv == selectedVariationValue ? Alloy.Styles.color.border.darkest : Alloy.Styles.color.border.lighter,
                    swatchValueId: vv }));

                  var iv = Ti.UI.createImageView($.createStyle({
                    classes: ['swatch_image_view'],
                    variation_value: vv,
                    swatch_image: i,
                    alt_images: alt_images,
                    accessibilityLabel: 'alt_color_' + i }));

                  Alloy.Globals.getImageViewImage(iv, swatchURL);
                  iv.addEventListener('click', onSwatchClick);
                  swatchListeners.push(iv);
                  swatchContainer.add(iv);

                  $.alt_swatches_box.add(swatchContainer);
                  if (!selectedVariationValue) {
                    selectedVariationValue = vv;
                  }
                }
              }
            });
          }


          var smallImages = $model.getAltImages(selectedVariationValue);
          var largeImages = $model.getAltZoomImages(selectedVariationValue);
          var hiresAvailable = true;
          if (!largeImages || largeImages.length == 0) {
            hiresAvailable = false;
          }

          var commonNumber = smallImages.length < largeImages.length ? smallImages.length : largeImages.length;
          var alt_images = _.map(smallImages, function (n) {
            return n.getLink();
          });

          for (var i = 0; i < commonNumber; i++) {
            var altviewContainer = Ti.UI.createView($.createStyle({
              classes: ['alt_view_container'],
              borderColor: i == selectedAlternateImage ? Alloy.Styles.color.border.darkest : Alloy.Styles.color.border.lighter,
              apiName: 'View' }));

            var iv = Ti.UI.createImageView($.createStyle({
              classes: ['alt_image_view'],
              apiName: 'ImageView',
              largeImage: largeImages[i].getLink(),
              alt_image: i,
              accessibilityLabel: 'alt_view_' + i }));

            Alloy.Globals.getImageViewImage(iv, smallImages[i].getLink());
            altviewContainer.add(iv);
            iv.addEventListener('click', onAltImageClick);
            altImageListeners.push(iv);
            $.alt_center_box.add(altviewContainer);
          }
          if (!selectedAlternateImage) {
            selectedAlternateImage = 0;
          }

          if (hiresAvailable) {
            $.detail_image_container.setMaxZoomScale(6.0);
          }

          if (largeImages && largeImages.length > selectedAlternateImage) {
            Alloy.Globals.getImageViewImage($.detail_image, largeImages[selectedAlternateImage].getLink());
          }
        });
      });
    });
  }






  function deinit() {
    logger.info('deinit called');
    $.close_button.removeEventListener('click', dismiss);
    $.backdrop.removeEventListener('click', dismiss);
    $.detail_image_container.removeEventListener('pinch', onImagePinch);
    $.detail_image_container.removeEventListener('touchend', onImageTouchEnd);
    $.stopListening();
    $.destroy();
    cleanupView();
  }









  function cleanupView() {
    _.each(altImageListeners, function (listener) {
      listener.removeEventListener('click', onAltImageClick);
    });
    altImageListeners = [];
    _.each(swatchListeners, function (listener) {
      listener.removeEventListener('click', onSwatchClick);
    });
    swatchListeners = [];
    removeAllChildren($.alt_swatches_box);
    removeAllChildren($.alt_center_box);
  }






  function dismiss() {
    $.trigger('image_zoom:dismiss');
  }









  function onAltImageClick(event) {
    event.cancelBubble = true;
    logger.info('altImageClick: ' + event.source.alt_image);
    $.detail_image_container.setZoomScale(1.0);
    selectedAlternateImage = event.source.alt_image;
    render();
  }






  function onSwatchClick(event) {
    event.cancelBubble = true;
    logger.info('swatchClick: ' + event.source.swatch_image);
    $.detail_image_container.setZoomScale(1.0);
    selectedVariationValue = event.source.variation_value;
    selectedAlternateImage = 0;
    render();
  }







  function onImagePinch(event) {
    $.detail_image_container.zoomScale -= (1.0 - event.scale) / $.detail_image_container.maxZoomScale;
  }






  function onImageTouchEnd() {
    $.detail_image_container.oldZoom = $.detail_image_container.zoomScale;
  }









  _.extend($, exports);
}

module.exports = Controller;