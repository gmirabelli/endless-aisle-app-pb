var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'product/images/imageContainer';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.color_container = Ti.UI.createView(
  { layout: "horizontal", id: "color_container" });

  $.__views.color_container && $.addTopLevelView($.__views.color_container);
  $.__views.image_view = Ti.UI.createView(
  { width: 350, height: 350, top: 25, left: 25, id: "image_view" });

  $.__views.color_container.add($.__views.image_view);
  $.__views.large_image_container = Ti.UI.createView(
  { borderColor: Alloy.Styles.color.border.medium, id: "large_image_container" });

  $.__views.image_view.add($.__views.large_image_container);
  if (Alloy.CFG.product.enable_video_player) {
    $.__views.pdp_video_player = Ti.Media.createVideoPlayer(
    { top: 2, left: 2, right: 2, bottom: 2, mediaControlStyle: Ti.Media.VIDEO_CONTROL_EMBEDDED, autoplay: false, visible: false, id: "pdp_video_player", accaccessibilityLabel: "pdp_video_player" });

    $.__views.large_image_container.add($.__views.pdp_video_player);
    handlePlaybackStateChange ? $.addListener($.__views.pdp_video_player, 'playbackstate', handlePlaybackStateChange) : __defers['$.__views.pdp_video_player!playbackstate!handlePlaybackStateChange'] = true;}
  if (Alloy.CFG.product.enable_video_player) {
    $.__views.video_play_button = Ti.UI.createButton(
    { backgroundImage: Alloy.Styles.videoPlayButtonImage, viewShadowColor: Alloy.Styles.color.background.black, viewShadowOffset: { x: 0, y: 0 }, viewShadowRadius: 0, width: 60, height: 60, borderRadius: 30, borderWidth: 3, borderColor: Alloy.Styles.color.background.black, visible: false, id: "video_play_button", accessibilityValue: "video_play_button" });

    $.__views.large_image_container.add($.__views.video_play_button);
    playProductVideo ? $.addListener($.__views.video_play_button, 'click', playProductVideo) : __defers['$.__views.video_play_button!click!playProductVideo'] = true;}
  $.__views.large_image_view = Ti.UI.createImageView(
  { top: 2, left: 2, right: 2, bottom: 2, regular_size: true, id: "large_image_view", accessibilityValue: "large_image_view" });

  $.__views.large_image_container.add($.__views.large_image_view);
  $.__views.alt_scroll_container = Ti.UI.createScrollView(
  { layout: "vertical", top: 5, left: 0, height: 300, id: "alt_scroll_container" });

  $.__views.color_container.add($.__views.alt_scroll_container);
  $.__views.alt_container = Ti.UI.createView(
  { layout: "horizontal", width: 350, top: 0, left: 25, height: 370, id: "alt_container" });

  $.__views.alt_scroll_container.add($.__views.alt_container);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var keepSessionAlive;
  var logger = require('logging')('product:images:imageContainer', getFullControllerPath($.__controllerPath));



  if (Alloy.CFG.product.enable_video_player) {
    $.listenTo(Alloy.eventDispatcher, 'app:header_bar_clicked', pauseVideo);
    $.listenTo(Alloy.eventDispatcher, 'app:navigation', stopVideo);
    $.listenTo(Alloy.eventDispatcher, 'app:dialog_displayed', pauseVideo);
  }




  exports.init = init;
  exports.deinit = deinit;
  exports.getAltContainer = getAltContainer;
  exports.getColorContainer = getColorContainer;
  exports.getLargeImageView = getLargeImageView;
  exports.getVideoPlayer = getVideoPlayer;
  exports.stopVideoAndShowImage = stopVideoAndShowImage;
  exports.showVideo = showVideo;
  exports.stopVideo = stopVideo;









  function init(valuePrefix, image) {
    logger.info('init called');
    $.large_image_view.id = 'large_image_' + valuePrefix;
    $.color_container.id = 'color_container_' + valuePrefix;
    $.image_view.id = 'image_' + valuePrefix;
    $.alt_container.id = 'alt_container_' + valuePrefix;
    Alloy.Globals.getImageViewImage($.large_image_view, image);
  }






  function deinit() {
    logger.info('deinit called');
    if (Alloy.CFG.product.enable_video_player) {
      $.pdp_video_player.stop();
      $.pdp_video_player.release();

      $.large_image_container.remove($.pdp_video_player);
      $.video_play_button.removeEventListener('click', playProductVideo);
      $.pdp_video_player.removeEventListener('playbackstate', handlePlaybackStateChange);
    }
    $.large_image_container.removeAllChildren();
    $.stopListening();
    $.destroy();
  }










  function getAltContainer() {
    return $.alt_container;
  }







  function getColorContainer() {
    return $.color_container;
  }







  function getLargeImageView() {
    return $.large_image_view;
  }







  function getVideoPlayer() {
    return $.pdp_video_player;
  }






  function stopVideoAndShowImage() {
    logger.info('stopVideoAndShowImage called');

    if (Alloy.CFG.product.enable_video_player) {
      $.pdp_video_player.stop();
      $.pdp_video_player.hide();
      $.video_play_button.hide();
      showLargeImage();
    }
  }






  function showVideo() {
    logger.info('showVideo called');

    if (Alloy.CFG.product.enable_video_player && !$.pdp_video_player.getVisible()) {
      $.pdp_video_player.show();
      $.video_play_button.show();
      $.pdp_video_player.play();
      hideLargeImage();
    }
  }






  function playProductVideo() {
    logger.info('playProductVideo called');

    $.pdp_video_player.play();
  }






  function hideLargeImage() {
    logger.info('hideLargeImage called');

    $.large_image_view.hide();
    _.extend($.large_image_view, {
      width: 0,
      height: 0 });

  }






  function showLargeImage() {
    logger.info('showLargeImage called');

    _.extend($.large_image_view, {
      width: null,
      height: null });


    $.large_image_view.show();
  }







  function handlePlaybackStateChange(event) {
    logger.info('handlePlaybackStateChange called');

    if (event.playbackState == Titanium.Media.VIDEO_PLAYBACK_STATE_STOPPED || event.playbackState == Titanium.Media.VIDEO_PLAYBACK_STATE_PAUSED) {

      clearInterval(keepSessionAlive);

      $.video_play_button.show();

    } else {
      if (event.playbackState == Titanium.Media.VIDEO_PLAYBACK_STATE_PLAYING) {

        Alloy.eventDispatcher.trigger('session:renew');

        keepSessionAlive = setInterval(function () {

          Alloy.eventDispatcher.trigger('session:renew');
        }, 10000);
        $.video_play_button.hide();

      }
    }
  }









  function stopVideo() {
    logger.info('stopVideo called');
    if (Alloy.CFG.product.enable_video_player && $.pdp_video_player.getVisible()) {
      if ($.pdp_video_player.getFullscreen()) {

        $.pdp_video_player.setFullscreen(false);

      }
      $.pdp_video_player.stop();

    }
  }






  function pauseVideo() {
    logger.info('pauseVideo called');
    if ($.pdp_video_player.getPlaybackState() === Ti.Media.VIDEO_PLAYBACK_STATE_PAUSED || $.pdp_video_player.getPlaybackState() === Ti.Media.VIDEO_PLAYBACK_STATE_PLAYING) {
      if ($.pdp_video_player.getFullscreen()) {

        $.pdp_video_player.setFullscreen(false);

      }
      $.pdp_video_player.pause();

    }
  }





  if (Alloy.CFG.product.enable_video_player) {
    __defers['$.__views.pdp_video_player!playbackstate!handlePlaybackStateChange'] && $.addListener($.__views.pdp_video_player, 'playbackstate', handlePlaybackStateChange);}
  if (Alloy.CFG.product.enable_video_player) {
    __defers['$.__views.video_play_button!click!playProductVideo'] && $.addListener($.__views.video_play_button, 'click', playProductVideo);}




  _.extend($, exports);
}

module.exports = Controller;