var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
		delete obj[key];
	}
	return arg;
}

function Controller() {

	require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'product/images/alternateImage';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};







	$.__views.alt_image_container = Ti.UI.createView(
	{ borderColor: Alloy.Styles.color.border.medium, right: 10, width: 71, height: 71, top: 10, id: "alt_image_container" });

	$.__views.alt_image_container && $.addTopLevelView($.__views.alt_image_container);
	$.__views.small_image_view = Ti.UI.createImageView(
	{ width: 67, height: Ti.UI.SIZE, top: 2, left: 2, id: "small_image_view", accessibilityValue: "small_image_view" });

	$.__views.alt_image_container.add($.__views.small_image_view);
	exports.destroy = function () {};




	_.extend($, $.__views);










	$.small_image_view.addEventListener('click', handleClick);




	exports.init = init;
	exports.deinit = deinit;










	function init(config) {
		$.small_image_view.large_view = config.largeImageView;
		$.small_image_view.large_image = config.largeImage;
		Alloy.Globals.getImageViewImage($.small_image_view, config.image);
		$.small_image_view.alt_image_number = config.altImageNumber;
		$.small_image_view.image_container_number = config.imageContainerNumber;
		$.small_image_view.product_number = config.productNumber;
	}






	function deinit() {
		$.small_image_view.large_view = null;
		$.small_image_view.large_image = null;
		$.small_image_view.alt_image_number = null;
		$.small_image_view.image_container_number = null;
		$.small_image_view.product_number = null;
		$.small_image_view.removeEventListener('click', handleClick);
	}










	function handleClick(event) {

		Alloy.Globals.getImageViewImage(event.source.large_view, event.source.large_image);
		$.alt_image_container.fireEvent('alt_image_selected', {
			image: event.source.alt_image_number,
			imageContainerNumber: event.source.image_container_number,
			productNumber: event.source.product_number });

	}









	_.extend($, exports);
}

module.exports = Controller;