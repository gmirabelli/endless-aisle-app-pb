var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
		delete obj[key];
	}
	return arg;
}

function Controller() {

	require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'product/images/alternateVideo';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};







	$.__views.alt_video_container = Ti.UI.createView(
	{ borderColor: Alloy.Styles.color.border.medium, right: 10, width: 71, height: 71, top: 10, id: "alt_video_container" });

	$.__views.alt_video_container && $.addTopLevelView($.__views.alt_video_container);
	$.__views.small_video_view = Ti.UI.createImageView(
	{ width: 67, height: Ti.UI.SIZE, top: 2, left: 2, image: Alloy.Styles.videoIconImage, id: "small_video_view", accessibilityValue: "small_video_view" });

	$.__views.alt_video_container.add($.__views.small_video_view);
	exports.destroy = function () {};




	_.extend($, $.__views);










	var logger = require('logging')('product:images:alternateVideo', getFullControllerPath($.__controllerPath));




	$.small_video_view.addEventListener('click', handleClick);




	exports.init = init;
	exports.deinit = deinit;










	function init(config) {
		logger.info('init called');
		$.small_video_view.videoPlayer = config.videoPlayer;
		$.small_video_view.video = config.videoURL;
		$.small_video_view.alt_image_number = config.altImageNumber;
		$.small_video_view.image_container_number = config.imageContainerNumber;
	}






	function deinit() {
		logger.info('deinit called');
		$.small_video_view.videoPlayer = null;
		$.small_video_view.video = null;
		$.small_video_view.alt_image_number = null;
		$.small_video_view.image_container_number = null;
		$.small_video_view.removeEventListener('click', handleClick);
	}










	function handleClick(event) {
		logger.info('handleClick called');

		event.source.videoPlayer.setUrl(event.source.video);
		$.alt_video_container.fireEvent('alt_image_selected', {
			video: event.source.video,
			imageContainerNumber: event.source.image_container_number });

	}









	_.extend($, exports);
}

module.exports = Controller;