var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'product/components/options';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.options = Ti.UI.createView(
  { layout: "vertical", height: Ti.UI.SIZE, top: 10, id: "options" });

  $.__views.options && $.addTopLevelView($.__views.options);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var optionLists = [];
  var eaUtils = require('EAUtils');
  var logger = require('logging')('product:components:options', getFullControllerPath($.__controllerPath));




  exports.init = init;
  exports.deinit = deinit;










  function init(args) {
    logger.info('init called');
    var model = args && args.model;
    var replaceItem = args && args.replaceItem;

    $model = $model || model || Alloy.Models.product;

    render(replaceItem);
  }






  function render(replaceItem) {
    logger.info('render called');

    var options = $model.getOptions();
    if (!options) {
      $.options.setHeight(0);
      $.options.setTop(0);
      return;
    }
    logger.info('render - has options');


    _.each(options, function (option) {
      var optionId = option.getId();


      var selectedIndex = 0;
      var defaultAttribute = 'c_' + optionId;
      var defaultOptionValue = $model.get(defaultAttribute);
      var values = option.getValues();
      for (var j = 0; j < values.length; j++) {
        var value = values[j];
        if (value.getId() == defaultOptionValue) {
          selectedIndex = j;
          break;
        }
      }

      var optionComponentName = Alloy.CFG.product.options.option_component[optionId];

      if (optionComponentName) {
        var optionComponent = Alloy.createController(optionComponentName, {
          product: $model,
          option: option });

        optionLists.push(optionComponent);
      } else {
        var optionName = option.getName();
        var optionValues = [];

        var j = 0,
        val,
        selectedOption;
        if (replaceItem && replaceItem.option_items) {

          selectedOption = _.find(replaceItem.option_items, function (item) {
            return item.option_id === optionId;
          });
        }
        while (val = option.getValuesCollection().at(j++)) {
          var v = val.toJSON();
          v.displayText = String.format('%s %s', v['name'], eaUtils.toCurrency(v['price']));
          optionValues.push(v);
          if (selectedOption && selectedOption.option_value_id == v.id) {
            selectedIndex = j - 1;
          }
          if ($model.hasSelectedOption() && $model.getSelectedOption().id == v.id) {
            selectedIndex = j - 1;
          }
        }


        var itemContainer = Ti.UI.createView($.createStyle({
          apiName: 'View',
          classes: ['item_container'] }));


        var optionList = Alloy.createController('components/selectWidget', {
          valueField: 'id',
          textField: 'displayText',
          selectListTitleStyle: {
            accessibilityValue: 'option_list' },

          values: optionValues,
          messageWhenSelection: '',
          messageWhenNoSelection: String.format(_L('Select %s'), optionName) });


        itemContainer.add(optionList.getView());
        $.options.add(itemContainer);
        optionList.setEnabled(true);
        $.listenTo(optionList, 'itemSelected', function (event) {
          logger.info('setting selected_option to ' + JSON.stringify(event.item));
          event.item.selected = true;
          $model.setSelectedOption(event.item);
        });

        if (selectedIndex >= 0) {
          optionList.updateSelectedItem(optionValues[selectedIndex].id);
        }
        optionLists.push(optionList);
      }
    });
  }






  function deinit() {
    logger.info('deinit called');

    $.stopListening();
    _.each(optionLists, function (option) {
      _.isFunction(option.deinit) && option.deinit();
    });
    removeAllChildren($.options);
  }









  _.extend($, exports);
}

module.exports = Controller;