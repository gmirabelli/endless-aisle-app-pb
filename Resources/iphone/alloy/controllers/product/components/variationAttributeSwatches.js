var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'product/components/variationAttributeSwatches';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.attribute_container = Ti.UI.createView(
  { layout: "vertical", height: Ti.UI.SIZE, width: "100%", top: 3, id: "attribute_container" });

  $.__views.attribute_container && $.addTopLevelView($.__views.attribute_container);
  $.__views.titleContainer = Ti.UI.createView(
  { layout: "horizontal", width: "100%", height: Ti.UI.SIZE, id: "titleContainer" });

  $.__views.attribute_container.add($.__views.titleContainer);
  $.__views.attribute_name = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, font: Alloy.Styles.detailLabelFont, id: "attribute_name", accessibilityValue: "attribute_name" });

  $.__views.titleContainer.add($.__views.attribute_name);
  $.__views.attribute_value = Ti.UI.createLabel(
  { width: "75%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailValueFont, left: 5, id: "attribute_value", accessibilityValue: "attribute_value" });

  $.__views.titleContainer.add($.__views.attribute_value);
  $.__views.swatch_list = Ti.UI.createView(
  { layout: "horizontal", width: "100%", height: Ti.UI.SIZE, left: 0, top: 8, id: "swatch_list" });

  $.__views.attribute_container.add($.__views.swatch_list);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var args = arguments[0] || {},
  $model = $model || args.variationAttribute || {},
  currentProduct = args.product;

  var logger = require('logging')('product:components:variationAttributeSwatches', getFullControllerPath($.__controllerPath));

  var attributeId = $model.getId(),
  attributeValues = $model.getValuesCollection(),
  attributeCfg = Alloy.CFG.product.attributeSelectComponent[attributeId] || {},
  selectedValue = null,
  swatchList = {},


  swatchContainerClickEventListeners = {};





  exports.deinit = deinit;
  exports.updateSelectedItem = updateSelectedItem;
  exports.updateItems = updateItems;









  function init() {
    logger.info('init start');
    if (!$model) {
      logger.info('MODEL NOT AVAILABLE');
      return;
    }

    if (attributeCfg.showTitle) {
      var attributeName = $model.getName();
      attributeName = attributeName.charAt(0).toUpperCase() + attributeName.substr(1);
      $.attribute_name.setText(attributeName + ':');
    } else {
      $.attribute_container.remove($.attribute_name);
    }

    currentProduct.ensureImagesLoaded('swatchImages').done(function (model, params, options) {
      logger.info('initialized. calling render()!');
      render();
    });

    logger.info('init end');

    return this;
  }






  function render() {
    logger.info('render start');

    var i = 0,
    len = attributeValues.length,
    attributeValue;

    var vvs = currentProduct.getVariationValues();
    var svv = vvs ? vvs[attributeId] : null;

    attributeValues.each(function (attributeValue, i) {

      var vv = attributeValue.getValue(),
      swatchGroupImages = currentProduct.getImages('swatchImages', vv),
      swatchImage = swatchGroupImages ? swatchGroupImages[0] : null;

      var swatchContainer = Ti.UI.createView($.createStyle({
        swatchValueId: vv,
        accessibilityLabel: vv,
        classes: ['swatch_container'],
        apiName: 'View' }));


      var swatchButton = Ti.UI.createImageView($.createStyle({
        accessibilityLabel: 'swatch_' + vv,
        classes: ['swatch_button'],
        apiName: 'ImageView' }));


      if (swatchImage) {
        Alloy.Globals.getImageViewImage(swatchButton, swatchImage.getLink());
      } else {
        swatchButton.add(Ti.UI.createLabel($.createStyle({
          classes: ['swatch_label'],
          apiName: 'Label',
          text: attributeValue.getName() })));

      }

      swatchContainer.add(swatchButton);

      var info = {};
      info[attributeId] = vv;

      var orderable = currentProduct.isOrderableVariationValue(info, attributeId);
      setSwatchEnabled(swatchContainer, orderable, orderable);

      $.swatch_list.add(swatchContainer);


      swatchList[vv] = swatchContainer;


      swatchContainerClickEventListeners[vv] = function () {
        swatchContainer.fireEvent('swatchSelected', {
          source: swatchContainer });

      };
      swatchContainer.addEventListener('click', swatchContainerClickEventListeners[vv]);
    });

    $.swatch_list.addEventListener('swatchSelected', swatchSelected);

    logger.info('render end');

    return this;
  }






  function deinit() {
    logger.info('deinit called');
    $.swatch_list.removeEventListener('swatchSelected', swatchSelected);
    for (var key in swatchList) {
      swatchList[key].removeEventListener('click', swatchContainerClickEventListeners[key]);
    }
    removeAllChildren($.swatch_list);
    $.stopListening();
    $.destroy();
  }









  function updateSelectedItem(selectedItem) {}








  function updateItems(items) {
    logger.info('updateItems start');

    var i = 0,
    list = items.models ? items.toJSON() : items,
    swatch,
    item,
    selected_swatch;

    while (item = list[i++]) {
      swatch = swatchList[item.value];
      if (!swatch) {
        continue;
      }

      setSwatchEnabled(swatch, item.enabled, item.orderable);
    }
    logger.info('updateItems end');
  }










  function swatchSelected(event) {
    logger.info('swatchSelected called');
    logger.trace('swatchSelected start');

    updateAttributeValueSelection(event.source.swatchValueId);

    $.trigger('itemSelected', {
      item: selectedValue.toJSON(),
      data: {
        variationAttributeId: attributeId } });



    logger.trace('swatchSelected end');
  }







  function updateAttributeValueSelection(value) {
    logger.info('updateAttributeValueSelection start ' + value);
    selectedValue = attributeValues.where({ value: value })[0];

    $.attribute_value.setText(selectedValue.getName());
    logger.info('updateAttributeValueSelection end');
  }









  function setSwatchEnabled(swatch, enabled, orderable) {
    logger.info('setSwatchEnabled: ' + enabled + ' for ' + swatch.swatchValueId);
    var vvs = currentProduct.getVariationValues(),
    svv;
    if (vvs) {
      svv = vvs[Alloy.CFG.product.color_attribute];
    }
    var borderColor = Alloy.Styles.color.border.lighter;
    if (svv == swatch.swatchValueId) {
      logger.info('setSwatchSelected: adding dark border to swatch: ' + swatch.swatchValueId);
      borderColor = Alloy.Styles.color.border.darkest;
      updateAttributeValueSelection(swatch.swatchValueId);
    } else if (!enabled) {
      borderColor = Alloy.Styles.color.border.dark;
    }
    if (swatch.getEnabled() != enabled) {
      swatch.setEnabled(enabled);
    }
    if (swatch.getTouchEnabled() != enabled) {
      swatch.setTouchEnabled(true);
    }
    if (swatch.getBorderColor() != borderColor) {
      swatch.setBorderColor(borderColor);
    }
    logger.info('setSwatchEnabled: end');
  }




  init();









  _.extend($, exports);
}

module.exports = Controller;