var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'product/components/addToWishList';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.add_to_wish_list = Ti.UI.createButton(
  { style: Ti.UI.iOS.SystemButtonStyle.PLAIN, backgroundColor: Alloy.Styles.color.background.white, color: Alloy.Styles.accentColor, selectedColor: Alloy.Styles.color.background.lightGray, width: 163, height: 35, left: 0, font: Alloy.Styles.buttonFont, titleid: "_Add_to_Wish_List", id: "add_to_wish_list", accessibilityValue: "add_to_wish_list" });

  $.__views.add_to_wish_list && $.addTopLevelView($.__views.add_to_wish_list);
  onAddToWishListClick ? $.addListener($.__views.add_to_wish_list, 'click', onAddToWishListClick) : __defers['$.__views.add_to_wish_list!click!onAddToWishListClick'] = true;exports.destroy = function () {};




  _.extend($, $.__views);










  var replaceItem;
  var currentCustomer = Alloy.Models.customer;
  var cCProductLists = currentCustomer.productLists;
  var lastProductAddedId, lastUsedProductListId;
  var EAUtils = require('EAUtils');
  var currentProductModel;


  var buttonTextLength = 20;
  var symbolButtonTextLength = 9;




  exports.init = init;
  exports.deinit = deinit;
  exports.isEnabled = isEnabled;
  exports.setUpdateInWishList = setUpdateInWishList;
  exports.setEnabled = setEnabled;










  function init(args) {
    if (args.productModel) {
      currentProductModel = args.productModel;
    }
    if (args.buttonTitle) {
      $.add_to_wish_list.setTitle(args.buttonTitle);
    }
    if ($.add_to_wish_list.getTitle().length > buttonTextLength || EAUtils.isSymbolBasedLanguage() && $.add_to_wish_list.getTitle().length > symbolButtonTextLength) {
      $.add_to_wish_list.setFont(Alloy.Styles.smallButtonFont);
    } else {
      $.add_to_wish_list.setFont(Alloy.Styles.buttonFont);
    }
    replaceItem = args.replaceItem;
    setUpdateInWishList(replaceItem);
  }






  function deinit() {
    $.add_to_wish_list.removeEventListener('click', onAddToWishListClick);
    currentProductModel = null;
    $.stopListening();
    $.destroy();
  }









  function isEnabled() {
    return $.add_to_wish_list.getTouchEnabled() || $.add_to_wish_list.getEnabled();
  }







  function setUpdateInWishList(replace) {
    if (replace) {
      $.add_to_wish_list.setTitle(_L('Replace in Wish List'));
    } else {
      $.add_to_wish_list.setTitle(_L('Add to Wish List'));
    }
  }







  function setEnabled(state) {

    $.add_to_wish_list.setEnabled(state);
    $.add_to_wish_list.setTouchEnabled(state);
    if (state) {
      $.add_to_wish_list.setColor(Alloy.Styles.accentColor);
    } else {
      $.add_to_wish_list.setColor(Alloy.Styles.color.background.darkGray);
    }
  }







  function isCustomerLoggedIn() {
    return Alloy.Models.customer.isLoggedIn();
  }







  function addProductToWishList(wishListId) {
    if (!currentProductModel) {
      return;
    }
    var productInfo = currentProductModel.getSelectedProductInfo();

    if (replaceItem && replaceItem.replaceInWishList && replaceItem.replaceInWishList.listId == wishListId) {
      var replaceInWishList = replaceItem.replaceInWishList;
      var deferred = new _.Deferred();
      Alloy.Router.showActivityIndicator(deferred);
      cCProductLists.deleteItem(replaceInWishList.listId, replaceInWishList.listItemId).done(function (model, opt) {
        cCProductLists.addItem(wishListId, productInfo).done(function (model, opt) {
          replaceItem.id = replaceItem.replaceInWishList.listItemId = model.getItemId();
          lastProductAddedId = productInfo.product_id;
          lastUsedProductListId = wishListId;
          var cPList = cCProductLists.where({
            id: wishListId });

          Alloy.Router.navigateToCart({
            switchToWishList: true });

        }).always(function () {
          deferred.resolve();
        });
      }).fail(function () {
        deferred.resolve();
      });
    } else {
      var promise = cCProductLists.addItem(wishListId, productInfo);
      promise.done(function (model, opt) {
        var wishListName = cCProductLists.getListNameById(wishListId);
        if (cCProductLists.length > 0 && wishListName) {
          wishListName = wishListName == _L('Wish List Title') ? '' : wishListName;
        } else {
          wishListName = '';
        }
        var message = String.format(_L('%s added to Wish List.'), model.getProductName());
        if (opt.update) {
          message = String.format(_L('%s updated in Wish List.'), model.getProductName());
          if (wishListName != '') {
            message = String.format(_L('%s updated in Wish List %s.'), model.getProductName(), wishListName);
          }
        } else if (wishListName != '') {
          message = String.format(_L('%s added to Wish List %s.'), model.getProductName(), wishListName);
        }
        notify(message);
        lastProductAddedId = productInfo.product_id;
        lastUsedProductListId = wishListId;
      });
      Alloy.Router.showActivityIndicator(promise);
    }
  }






  function onAddToWishListClick() {
    if (isCustomerLoggedIn()) {

      if (cCProductLists.getWishListCount() > 1 && !replaceItem) {
        Alloy.Dialog.showCustomDialog({
          controllerPath: 'product/components/productListSelectorDialog',
          continueEvent: 'productListSelectorDialog:continue',
          cancelEvent: 'productListSelectorDialog:dismiss',
          continueFunction: function (event) {
            if (event && event.listId) {
              addProductToWishList(event.listId);
            }
          },
          options: {
            wishListCollection: cCProductLists.getAllWishLists() } });


      } else if (cCProductLists.getWishListCount() == 1 || cCProductLists.getWishListCount() > 1 && replaceItem) {

        addProductToWishList(replaceItem ? replaceItem.replaceInWishList.listId : cCProductLists.getFirstWishListId());
      } else {

        Alloy.Dialog.showAlertDialog({
          messageString: String.format(_L('%s does not have a wish list'), currentCustomer.getFullName()) });

      }
    } else {

      Alloy.Router.presentCustomerSearchDrawer({
        message: _L('Customer must be logged in to add to wish list. Please look up customer.') });

    }
  }





  __defers['$.__views.add_to_wish_list!click!onAddToWishListClick'] && $.addListener($.__views.add_to_wish_list, 'click', onAddToWishListClick);



  _.extend($, exports);
}

module.exports = Controller;