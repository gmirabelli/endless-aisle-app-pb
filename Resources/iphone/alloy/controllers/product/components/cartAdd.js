var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'product/components/cartAdd';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.cart_add = Ti.UI.createView(
  { height: Ti.UI.SIZE, width: "100%", layout: "vertical", left: 0, id: "cart_add" });

  $.__views.cart_add && $.addTopLevelView($.__views.cart_add);
  $.__views.quantity_row = Ti.UI.createView(
  { width: "100%", layout: "horizontal", height: Ti.UI.SIZE, top: 10, id: "quantity_row" });

  $.__views.cart_add.add($.__views.quantity_row);
  $.__views.quantity_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, textid: "_Quantity_", font: Alloy.Styles.sectionTitleFont, id: "quantity_label", accessibilityValue: "product_quantity_label" });

  $.__views.quantity_row.add($.__views.quantity_label);
  $.__views.quantity_container = Ti.UI.createView(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, layout: "horizontal", left: 10, id: "quantity_container" });

  $.__views.quantity_row.add($.__views.quantity_container);
  $.__views.web_availability_footer_row = Ti.UI.createView(
  { layout: "horizontal", height: Ti.UI.SIZE, width: "100%", horizontalWrap: false, top: 15, id: "web_availability_footer_row" });

  $.__views.cart_add.add($.__views.web_availability_footer_row);
  $.__views.add_to_cart_btn = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.buttons.primary.color, disabledColor: Alloy.Styles.buttons.primary.disabledColor, backgroundImage: Alloy.Styles.buttons.primary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.primary.backgroundDisabledImage, height: 35, width: 163, titleid: "_Add_to_Cart", left: 0, font: Alloy.Styles.buttonFont, id: "add_to_cart_btn", accessibilityValue: "add_to_cart_btn" });

  $.__views.web_availability_footer_row.add($.__views.add_to_cart_btn);
  $.__views.availability_message_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 25, font: Alloy.Styles.detailValueFont, textid: "_Availability_", id: "availability_message_label", accessibilityValue: "availability_message_label" });

  $.__views.web_availability_footer_row.add($.__views.availability_message_label);
  $.__views.availability_message = Ti.UI.createLabel(
  { width: Ti.UI.FILL, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 15, right: 15, font: Alloy.Styles.detailLabelFont, id: "availability_message", accessibilityValue: "availability_message" });

  $.__views.web_availability_footer_row.add($.__views.availability_message);
  if (Alloy.CFG.enable_wish_list) {
    $.__views.add_to_product_list_container = Ti.UI.createView(
    { layout: "horizontal", height: Ti.UI.SIZE, width: Ti.UI.SIZE, top: 19, left: 0, id: "add_to_product_list_container" });

    $.__views.cart_add.add($.__views.add_to_product_list_container);
    $.__views.add_to_wish_list_button = Alloy.createController('product/components/addToWishList', { id: "add_to_wish_list_button", __parentSymbol: $.__views.add_to_product_list_container });
    $.__views.add_to_wish_list_button.setParent($.__views.add_to_product_list_container);
  }
  exports.destroy = function () {};




  _.extend($, $.__views);











  var analytics = require('analyticsBase');
  var logger = require('logging')('product:components:cartAdd', getFullControllerPath($.__controllerPath));

  var processing = false;
  var replaceItem = null;
  var currentBasket = Alloy.Models.basket;
  var currentProduct = Alloy.Models.product;
  var EAUtils = require('EAUtils');


  var buttonTextLength = 15;
  var symbolButtonTextLength = 9;




  $.add_to_cart_btn.addEventListener('click', addToCartBtnEventHandler);
  $.web_availability_footer_row.addEventListener('click', disabledAddToCartClickEventHandler);

  if (Alloy.CFG.enable_wish_list) {
    $.add_to_product_list_container.addEventListener('click', disabledAddToWishListClickEventHandler);
  }




  exports.init = init;
  exports.deinit = deinit;
  exports.getEnabled = getEnabled;









  function init(args) {
    logger.info('init called');

    replaceItem = args && args.replaceItem;
    $model = $model || args && args.model || currentProduct;
    $.listenTo($model, 'change:selected_details', onSelectedDetailsChange);
    $.listenTo($model, 'change:selected_variant', onSelectedVariantChange);
    if (Alloy.CFG.enable_wish_list) {
      if (replaceItem && replaceItem.replaceInWishList) {
        $.add_to_wish_list_button.init({
          productModel: $model,
          replaceItem: replaceItem });

      } else {
        $.add_to_wish_list_button.init({
          productModel: $model });

      }
    }
    render();
  }






  function render() {
    logger.info('render called');

    if (!isKioskMode() || isKioskMode() && isKioskCartEnabled()) {
      var values = [1, 2, 3, 4, 5, 6, 7, 8, 9];
      if (replaceItem && replaceItem.quantity > 9) {
        for (var i = 10; i <= replaceItem.quantity; i++) {
          values.push(i);
        }
      }
      if (replaceItem && !isNaN(replaceItem.quantity)) {

        currentProduct.set({
          quantity: replaceItem.quantity },
        {
          silent: true });

      }
      $.quantitySelectList = Alloy.createController('components/selectWidget', {
        values: values,
        selectListTitleStyle: {
          accessibilityValue: 'quantity_chooser',
          width: 105,
          left: 15 },

        selectListStyle: {
          width: 105,
          top: 0 },

        selectedItem: replaceItem ? replaceItem.quantity : 1 });

      if (replaceItem && replaceItem.quantity) {
        $model.setQuantity(replaceItem.quantity, {
          silent: true });

      }
      $.quantity_container.add($.quantitySelectList.getView());
      $.listenTo($.quantitySelectList, 'itemSelected', quantitySelectListEventHandler);
    }

    if (replaceItem && !replaceItem.replaceInWishList) {
      $.add_to_cart_btn.setTitle(_L('Replace in Cart'));
    } else {
      $.add_to_cart_btn.setTitle(_L('Add to Cart'));
    }
    if ($.add_to_cart_btn.getTitle().length > buttonTextLength || EAUtils.isSymbolBasedLanguage() && $.add_to_cart_btn.getTitle().length > symbolButtonTextLength) {
      $.add_to_cart_btn.setFont(Alloy.Styles.smallButtonFont);
    }
    if (isKioskMode()) {
      $.add_to_cart_btn.setVisible(isKioskCartEnabled());
      if (!isKioskCartEnabled()) {
        $.quantity_label.setVisible(false);
        $.quantity_label.setWidth(0);
        $.quantity_container.setVisible(false);
        $.quantity_container.setWidth(0);
        $.availability_message_label.setLeft(0);
        $.web_availability_footer_row.remove($.add_to_cart_btn);
      }
    }
  }






  function deinit() {
    logger.info('deinit called');


    $.stopListening();

    $.add_to_cart_btn.removeEventListener('click', addToCartBtnEventHandler);
    $.web_availability_footer_row.removeEventListener('click', disabledAddToCartClickEventHandler);
    $.quantitySelectList && $.quantitySelectList.deinit();
    $.stopListening($.quantitySelectList, 'itemSelected', quantitySelectListEventHandler);
    if (Alloy.CFG.enable_wish_list && $.add_to_product_list_container && $.add_to_wish_list_button) {
      $.add_to_product_list_container.removeEventListener('click', disabledAddToWishListClickEventHandler);
      $.add_to_wish_list_button.deinit();
    }
    removeAllChildren($.quantity_container);
    $.destroy();
  }











  function addProduct() {
    var productInfo = $model.getSelectedProductInfo();
    logger.info('Adding product to cart: ' + JSON.stringify(productInfo));
    var haveOptionsChanged = false;
    if (replaceItem) {
      haveOptionsChanged = optionsChanged(productInfo, replaceItem);
    }
    var promise;
    if (replaceItem && !replaceItem.replaceInWishList && replaceItem.product_id == productInfo.product_id && !haveOptionsChanged) {


      promise = currentBasket.replaceProduct({
        product_id: replaceItem.product_id,
        quantity: productInfo.quantity },
      replaceItem.item_id, {
        c_employee_id: Alloy.Models.associate.getEmployeeId(),
        c_store_id: Alloy.CFG.store_id });

      promise.done(function () {
        notify(_L('Item' + (productInfo.quantity > 1 ? 's' : '') + ' updated in the cart'));
        Alloy.Router.navigateToCart();
      }).fail(function (response) {
        logger.error('failure adding product to cart: ' + JSON.stringify(response));
        var msg = _L('Item' + (productInfo.quantity > 1 ? 's' : '') + ' could not be updated in the cart');
        if (response && response.fault && response.fault.message) {
          msg = response.fault.message;
        }
        notify(msg, {
          preventAutoClose: true });

      }).always(function () {
        processing = false;
      });
    } else if (replaceItem && !replaceItem.replaceInWishList && (replaceItem.product_id != productInfo.product_id || haveOptionsChanged)) {


      promise = currentBasket.replaceProduct(productInfo, replaceItem.item_id, {
        c_employee_id: Alloy.Models.associate.getEmployeeId(),
        c_store_id: Alloy.CFG.store_id });

      promise.done(function () {
        notify(_L('Item' + (productInfo.quantity > 1 ? 's' : '') + ' replaced in the cart'));
        Alloy.Router.navigateToCart();
      }).fail(function (response) {
        logger.error('failure adding product to cart: ' + JSON.stringify(response));
        var msg = _L('Item' + (productInfo.quantity > 1 ? 's' : '') + ' could not be replaced in the cart');
        if (response && response.fault && response.fault.message) {
          msg = response.fault.message;
        }
        notify(msg, {
          preventAutoClose: true });

      }).always(function () {
        processing = false;
      });
    } else {

      promise = currentBasket.addProduct(productInfo, {
        c_employee_id: Alloy.Models.associate.getEmployeeId(),
        c_store_id: Alloy.CFG.store_id });

      promise.done(function () {
        analytics.fireAnalyticsEvent({
          category: _L('Basket'),
          action: _L('Add To Basket'),
          label: $model.getName() + ($model.getSelectedVariant() ? ' (' + $model.getSelectedVariant().getProductId() + ')' : '') });

        notify(_L('Item' + (productInfo.quantity > 1 ? 's' : '') + ' added to the cart'));
      }).fail(function (response) {
        logger.error('failure adding product to cart: ' + JSON.stringify(response));
        var msg = _L('Unable to add item' + (productInfo.quantity > 1 ? 's' : '') + ' to the cart');
        if (response) {
          var fault = response.get('fault');
          if (fault && fault.message && fault.message != '') {
            msg = fault.message;
          }
        }
        notify(msg, {
          preventAutoClose: true });

      }).always(function () {
        processing = false;
      });
    }
    Alloy.Router.showActivityIndicator(promise);
    return promise;
  }






  function optionsChanged(newProduct, oldProduct) {
    logger.info('optionsChanged called');

    if (!newProduct.option_items && !oldProduct.option_items) {
      return false;
    }
    if (newProduct.option_items && oldProduct.option_items && newProduct.option_items.length == oldProduct.option_items.length) {
      for (var i = 0, n = newProduct.option_items.length; i < n; ++i) {
        if (newProduct.option_items[i].option_value_id !== oldProduct.option_items[i].option_value_id) {
          return true;
        }
      }
      return false;
    }
    return true;
  }






  function onSelectedDetailsChange(model) {
    logger.info('onSelectedDetailsChange called');

    var webAvailability = $model.getInventoryAvailability(model);
    logger.info('web availability: ' + JSON.stringify(webAvailability));
    $.availability_message.setColor(determineAvailabilityTextColor(webAvailability.levels));
    $.availability_message.setText(webAvailability.message);
    var enabled = false;
    if ($model.isProductSelected() && webAvailability.levels.notAvailable <= 0) {
      enabled = true;
    }
    var isOrderable;
    if ((webAvailability.levels.notAvailable > 0 || webAvailability.levels.notAvailable == null) && webAvailability.levels.stockLevel == null) {
      isOrderable = false;
    } else {
      isOrderable = true;
    }
    enableAddToButtons(enabled, isOrderable);
    $.trigger('cartAdd:add_cart_changed');
  }






  function determineAvailabilityTextColor(levels) {
    logger.info('determineAvailabilityTextColor called');
    var color = Alloy.Styles.color.text.dark;
    if (!levels) {
      return color;
    }

    if (levels.preorder > 0) {
      color = Alloy.Styles.color.text.blue;
    }

    if (levels.backorder > 0) {
      color = Alloy.Styles.accentColor;
    }

    if (levels.notAvailable > 0) {
      color = Alloy.Styles.color.text.red;
    }

    return color;
  }






  function enableAddToCart(enabled) {
    logger.info('enableAddToCart called');
    if (isKioskMode() && !isKioskCartEnabled()) {
      enabled = false;
    }
    $.add_to_cart_btn.setEnabled(enabled);
    $.add_to_cart_btn.setTouchEnabled(enabled);
  }








  function enableAddToButtons(enabled, orderable) {
    enableAddToCart(enabled);
    if (Alloy.CFG.enable_wish_list) {
      if (typeof orderable == 'boolean') {

        $.add_to_wish_list_button.setEnabled(orderable);
      } else {
        $.add_to_wish_list_button.setEnabled(enabled);
      }
    }
  }







  function getEnabled() {
    logger.info('getEnabled called');
    return $.add_to_cart_btn.getEnabled();
  }






  function removeOverride() {
    Alloy.Dialog.showConfirmationDialog({
      messageString: _L('A price override has been applied to this item. Replacing the item will remove the override.'),
      okButtonString: _L('Confirm'),
      okFunction: function () {
        var override = {
          lineItem_id: replaceItem.item_id,
          product_id: replaceItem.product_id,
          price_override_type: 'none',
          index: replaceItem.index,
          employee_id: Alloy.Models.associate.getEmployeeId(),
          employee_passcode: Alloy.Models.associate.getPasscode(),
          store_id: Alloy.CFG.store_id };

        if (isKioskManagerLoggedIn()) {
          override.manager_employee_id = getKioskManager().getEmployeeId();
          override.manager_employee_passcode = getKioskManager().getPasscode();
          override.manager_allowLOBO = getKioskManager().getPermissions().allowLOBO;
          override.kiosk_mode = isKioskMode();
        }

        Alloy.Models.basket.setProductPriceOverride(override, {
          c_employee_id: Alloy.Models.associate.getEmployeeId() }).
        done(function () {
          addProduct();
        }).fail(function (response) {
          logger.error('failure removing product override: ' + JSON.stringify(response));
          processing = false;
        });
      },
      cancelFunction: function () {
        processing = false;
      } });

  }









  function disabledAddToCartClickEventHandler() {
    logger.info('disabledAddToCartClickEventHandler called');
    if (!$.add_to_cart_btn.getTouchEnabled() || !$.add_to_cart_btn.getEnabled()) {
      $.trigger('cartAdd:verify_variants');
    }
  }






  function disabledAddToWishListClickEventHandler() {
    logger.info('disabledAddToWishListClickEventHandler called');
    if (!$.add_to_wish_list_button.isEnabled()) {
      $.trigger('cartAdd:verify_variants');
    }
  }






  function addToCartBtnEventHandler() {
    logger.info('addToCartBtnEventHandler called');
    if (processing) {
      return;
    }

    $.add_to_cart_btn.animate(Alloy.Animations.bounce);

    processing = true;

    if (replaceItem && replaceItem.price_override == 'true') {
      removeOverride();
    } else {
      addProduct();
    }
  }







  function quantitySelectListEventHandler(event) {
    logger.info('quantitySelectListEventHandler selection: ' + JSON.stringify(event.item.value));
    $model.setQuantity(event.item.value, {
      silent: true });


    fetchSelectedDetails();
  }






  function onSelectedVariantChange() {
    logger.info('onSelectedVariantChange called');
    fetchSelectedDetails();
  }






  function fetchSelectedDetails() {
    logger.info('fetchSelectedDetails called');


    enableAddToButtons(false);
    $.availability_message.setText('');

    if ($model.getSelectedVariant()) {
      var deferred = new _.Deferred();
      Alloy.Router.showActivityIndicator(deferred);
      $model.fetchSelectedDetails().always(function () {
        deferred.resolve();
      });
    } else {

      $model.trigger('change:selected_details', $model);
    }
  }






  enableAddToButtons(false);









  _.extend($, exports);
}

module.exports = Controller;