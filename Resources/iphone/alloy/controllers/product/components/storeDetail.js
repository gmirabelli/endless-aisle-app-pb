var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
		delete obj[key];
	}
	return arg;
}

function Controller() {

	require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'product/components/storeDetail';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};







	$.__views.store_row_table = Ti.UI.createTableViewRow(
	{ selectionStyle: "none", id: "store_row_table" });

	$.__views.store_row_table && $.addTopLevelView($.__views.store_row_table);
	$.__views.store_detail_container = Ti.UI.createView(
	{ layout: "vertical", height: Ti.UI.SIZE, id: "store_detail_container" });

	$.__views.store_row_table.add($.__views.store_detail_container);
	$.__views.store_detail_content_row = Ti.UI.createView(
	{ layout: "horizontal", height: Ti.UI.SIZE, id: "store_detail_content_row" });

	$.__views.store_detail_container.add($.__views.store_detail_content_row);
	$.__views.store_container_left = Ti.UI.createView(
	{ layout: "vertical", top: 0, bottom: 10, width: "33%", height: Ti.UI.SIZE, id: "store_container_left" });

	$.__views.store_detail_content_row.add($.__views.store_container_left);
	$.__views.store_name = Ti.UI.createLabel(
	{ width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.black, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 5, right: 5, top: 5, font: Alloy.Styles.lineItemLabelFont, id: "store_name", text: $model.__transform.name, accessibilityValue: "store_name" });

	$.__views.store_container_left.add($.__views.store_name);
	$.__views.store_address1 = Ti.UI.createLabel(
	{ width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.black, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 5, right: 5, top: 2, font: Alloy.Styles.lineItemFont, id: "store_address1", text: $model.__transform.address1, accessibilityValue: "store_address1" });

	$.__views.store_container_left.add($.__views.store_address1);
	$.__views.store_city_state_postal_code = Ti.UI.createLabel(
	{ width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.black, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 5, right: 5, top: 2, font: Alloy.Styles.lineItemFont, id: "store_city_state_postal_code", text: $model.__transform.city_state_postal_code, accessibilityValue: "store_city_state_postal_code" });

	$.__views.store_container_left.add($.__views.store_city_state_postal_code);
	$.__views.store_phone = Ti.UI.createLabel(
	{ width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.black, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 5, right: 5, top: 2, font: Alloy.Styles.lineItemFont, id: "store_phone", text: $model.__transform.phone, accessibilityValue: "store_phone" });

	$.__views.store_container_left.add($.__views.store_phone);
	$.__views.store_distance_text = Ti.UI.createLabel(
	{ width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.black, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 5, right: 5, top: 2, font: Alloy.Styles.lineItemFont, id: "store_distance_text", text: $model.__transform.distance_text, accessibilityValue: "store_distance_text" });

	$.__views.store_container_left.add($.__views.store_distance_text);
	$.__views.store_container_middle = Ti.UI.createView(
	{ layout: "vertical", top: 0, bottom: 10, width: "34%", height: Ti.UI.SIZE, id: "store_container_middle" });

	$.__views.store_detail_content_row.add($.__views.store_container_middle);
	$.__views.store_hours = Ti.UI.createLabel(
	{ width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.black, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 5, right: 5, top: 2, font: Alloy.Styles.lineItemFont, id: "store_hours", text: $model.__transform.store_hours, accessibilityValue: "store_hours" });

	$.__views.store_container_middle.add($.__views.store_hours);
	$.__views.store_events = Ti.UI.createLabel(
	{ width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.black, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 5, right: 5, top: 5, font: Alloy.Styles.lineItemLabelFont, id: "store_events", text: $model.__transform.store_events, accessibilityValue: "store_events" });

	$.__views.store_container_middle.add($.__views.store_events);
	$.__views.store_container_right = Ti.UI.createView(
	{ layout: "vertical", top: 0, bottom: 10, width: "33%", height: Ti.UI.SIZE, id: "store_container_right" });

	$.__views.store_detail_content_row.add($.__views.store_container_right);
	$.__views.store_availability = Ti.UI.createLabel(
	{ width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: $model.__transform.availability_color, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 5, right: 5, top: 5, font: Alloy.Styles.lineItemLabelFont, id: "store_availability", text: $model.__transform.availability_message, accessibilityValue: "store_availability" });

	$.__views.store_container_right.add($.__views.store_availability);
	$.__views.store_availability_stock_level = Ti.UI.createLabel(
	{ width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: $model.__transform.availability_color, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 5, right: 5, top: 5, font: Alloy.Styles.lineItemLabelFont, id: "store_availability_stock_level", text: $model.__transform.availability_stock_level, accessibilityValue: "store_availability_stock_level" });

	$.__views.store_container_right.add($.__views.store_availability_stock_level);
	$.__views.line = Ti.UI.createView(
	{ width: "100%", backgroundColor: Alloy.Styles.color.background.dark, height: 1, left: 10, top: 10, bottom: 2, right: 10, id: "line" });

	$.__views.store_detail_container.add($.__views.line);
	exports.destroy = function () {};




	_.extend($, $.__views);












	_.extend($, exports);
}

module.exports = Controller;