var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
		delete obj[key];
	}
	return arg;
}

function Controller() {

	require('/alloy/controllers/' + 'product/productDetailBase').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'product/main/productDetail';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};







	exports.destroy = function () {};




	_.extend($, $.__views);







	exports.baseController = 'product/productDetailBase';




	var logger = require('logging')('product:main:productDetail', getFullControllerPath($.__controllerPath));




	exports.deinit = deinit;
	exports.getDetailsTabView = getDetailsTabView;









	var superDeinit = $.deinit;
	function deinit() {
		logger.info('deinit called');
		$.pdp_details.deinit();
		superDeinit();
	}










	function getDetailsTabView() {
		logger.info('getDetailsTabView called');
		var deferred = new _.Deferred();
		$.pdp_details = Alloy.createController('product/tabs/productDetailsTab');

		$.pdp_details.init({
			replaceItem: $.replaceItem }).
		always(function () {
			deferred.resolve($.pdp_details.getView());
		});

		return deferred.promise();
	}









	_.extend($, exports);
}

module.exports = Controller;