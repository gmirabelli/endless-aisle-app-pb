var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'product/productDetailBase').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'product/main/productSetDetail';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  exports.destroy = function () {};




  _.extend($, $.__views);








  exports.baseController = 'product/productDetailBase';




  var logger = require('logging')('product:main:productSetDetail', getFullControllerPath($.__controllerPath));




  exports.init = init;
  exports.deinit = deinit;
  exports.getDetailsTabView = getDetailsTabView;
  exports.getHeaderView = getHeaderView;










  var superInit = $.init;
  function init(info) {
    logger.info('init override called');

    var deferred = new _.Deferred();
    superInit(info).done(function () {
      deferred.resolve();
    }).fail(function () {
      deferred.reject();
    });
    $.pdp_header_controller.enableAddAllToCart($.pdp_details.getAllEnabled());
    return deferred.promise();
  }






  var superDeinit = $.deinit;
  function deinit() {
    logger.info('deinit override called');
    $.stopListening();
    $.pdp_details.deinit();
    superDeinit();

    $.destroy();
  }










  function getDetailsTabView() {
    logger.info('getDetailsTabView called');
    var deferred = new _.Deferred();

    $.pdp_details = Alloy.createController('product/tabs/productDetailsTabSet');

    $.pdp_details.init({
      replaceItem: $.replaceItem }).
    always(function () {
      $.listenTo($.pdp_details, 'productDetailsTabSet:add_cart_changed', onAddCartChanged);

      $.currentProduct.trigger('change:selected_variant');
      deferred.resolve($.pdp_details.getView());
    });

    return deferred.promise();
  }







  function getHeaderView() {
    logger.info('getHeaderView called');

    $.pdp_header_controller = Alloy.createController('product/components/detailHeaderSet');

    $.pdp_header_controller.init();
    $.listenTo($.pdp_header_controller, 'detailHeaderSet:add_all_to_cart_btn_tap', function () {
      logger.info('add all tapped');
      $.pdp_details.addAllToCart();
    });
    $.listenTo($.pdp_header_controller, 'detailHeaderSet:check_all_variations', function () {
      logger.info('header tapped');
      $.pdp_details.verifyVariants();
    });

    return $.pdp_header_controller.getView();
  }









  function onAddCartChanged() {
    logger.info('onAddCartChanged called');
    var allEnabled = $.pdp_details.getAllEnabled();
    $.pdp_header_controller.enableAddAllToCart(allEnabled);
    if (allEnabled) {
      calculateTotal();
    }
  }






  function calculateTotal() {
    logger.info('calculateTotal called');
    var sum = 0.0;
    var selectedVariants = [];
    var products = [];
    _.each($.currentProduct.getSetProducts(), function (setProduct) {
      selectedVariants.push(setProduct.getSelectedVariant());
      products.push(setProduct.getSelectedProductInfo());
    });

    _.each(products, function (selectedProduct, index) {
      var variant = _.find(selectedVariants, function (v) {
        return v.getProductId() == selectedProduct.product_id;
      });
      if (variant) {
        sum += variant.getPrice() * selectedProduct.quantity;
      }
    });
    $.pdp_header_controller.setPrice(sum);
  }






  function getSelectedProductItems() {
    logger.info('getSelectedProductItems called');
    var products = [];
    _.each($.currentProduct.getSetProducts(), function (setProduct) {
      products.push(setProduct.getSelectedProductInfo());
    });
    return products;
  }









  _.extend($, exports);
}

module.exports = Controller;