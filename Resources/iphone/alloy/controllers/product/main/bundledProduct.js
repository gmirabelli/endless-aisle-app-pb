var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'product/main/bundledProduct';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.bundled_product = Ti.UI.createView(
  { height: Ti.UI.SIZE, layout: "vertical", top: 0, id: "bundled_product" });

  $.__views.bundled_product && $.addTopLevelView($.__views.bundled_product);
  $.__views.__alloyId214 = Ti.UI.createView(
  { height: Ti.UI.SIZE, layout: "horizontal", id: "__alloyId214" });

  $.__views.bundled_product.add($.__views.__alloyId214);
  $.__views.product_image = Ti.UI.createImageView(
  { height: 75, width: 75, id: "product_image", accessibilityValue: "product_image" });

  $.__views.__alloyId214.add($.__views.product_image);
  $.__views.vertical_container = Ti.UI.createView(
  { height: Ti.UI.SIZE, layout: "vertical", id: "vertical_container" });

  $.__views.__alloyId214.add($.__views.vertical_container);
  $.__views.name = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, font: Alloy.Styles.productCalloutFont, id: "name", accessibilityValue: "product_name" });

  $.__views.vertical_container.add($.__views.name);
  $.__views.__alloyId215 = Ti.UI.createView(
  { height: Ti.UI.SIZE, layout: "horizontal", id: "__alloyId215" });

  $.__views.vertical_container.add($.__views.__alloyId215);
  $.__views.quantity_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailValueFont, textid: "_Quantity_", id: "quantity_label", accessibilityValue: "quantity_label" });

  $.__views.__alloyId215.add($.__views.quantity_label);
  $.__views.quantity = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailValueFont, id: "quantity", accessibilityValue: "quantity" });

  $.__views.__alloyId215.add($.__views.quantity);
  $.__views.promotions = Alloy.createController('product/components/promotions', { id: "promotions", __parentSymbol: $.__views.bundled_product });
  $.__views.promotions.setParent($.__views.bundled_product);
  $.__views.variations = Alloy.createController('product/components/variations', { id: "variations", __parentSymbol: $.__views.bundled_product });
  $.__views.variations.setParent($.__views.bundled_product);
  $.__views.options = Alloy.createController('product/components/options', { id: "options", __parentSymbol: $.__views.bundled_product });
  $.__views.options.setParent($.__views.bundled_product);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var logger = require('logging')('product:main:bundledProduct', getFullControllerPath($.__controllerPath));




  exports.init = init;
  exports.deinit = deinit;











  function init(args) {
    logger.info('init called');
    var deferred = new _.Deferred();

    $.name.setText($model.getName());
    $.quantity.setText($model.getQuantity() || 1);

    $model.ensureImagesLoaded('bundleProductImages').done(function () {
      var smallImages = $model.getBundleProductImages();
      if (smallImages && smallImages.length > 0) {
        Alloy.Globals.getImageViewImage($.product_image, smallImages[0].getLink());
      }
      $.promotions.init({
        model: $model });

      $.variations.init({
        model: $model });

      $.options.init({
        model: $model,
        replaceItem: args.replaceItem });

      deferred.resolve();
    });
    return deferred.promise();
  }






  function deinit() {
    logger.info('deinit called');
    $.stopListening();

    $.promotions.deinit();
    $.variations.deinit();
    $.options.deinit();
  }









  _.extend($, exports);
}

module.exports = Controller;