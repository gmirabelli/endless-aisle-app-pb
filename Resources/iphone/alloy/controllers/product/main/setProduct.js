var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'product/main/setProduct';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.set_product = Ti.UI.createView(
  { height: Ti.UI.SIZE, layout: "vertical", top: 10, id: "set_product" });

  $.__views.set_product && $.addTopLevelView($.__views.set_product);
  $.__views.horizontal_container = Ti.UI.createView(
  { height: Ti.UI.SIZE, layout: "horizontal", id: "horizontal_container" });

  $.__views.set_product.add($.__views.horizontal_container);
  $.__views.product_image = Ti.UI.createImageView(
  { top: 5, right: 10, height: 75, width: 75, id: "product_image", accessibilityValue: "product_image" });

  $.__views.horizontal_container.add($.__views.product_image);
  $.__views.vertical_container = Ti.UI.createView(
  { height: Ti.UI.SIZE, layout: "vertical", id: "vertical_container" });

  $.__views.horizontal_container.add($.__views.vertical_container);
  $.__views.name = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, font: Alloy.Styles.productCalloutFont, id: "name", accessibilityValue: "set_item_name" });

  $.__views.vertical_container.add($.__views.name);
  $.__views.product_id = Alloy.createController('product/components/productId', { id: "product_id", __parentSymbol: $.__views.vertical_container });
  $.__views.product_id.setParent($.__views.vertical_container);
  $.__views.pricing = Alloy.createController('product/components/productPricing', { id: "pricing", __parentSymbol: $.__views.vertical_container });
  $.__views.pricing.setParent($.__views.vertical_container);
  $.__views.promotions = Alloy.createController('product/components/promotions', { id: "promotions", __parentSymbol: $.__views.vertical_container });
  $.__views.promotions.setParent($.__views.vertical_container);
  $.__views.variations = Alloy.createController('product/components/variations', { id: "variations", __parentSymbol: $.__views.vertical_container });
  $.__views.variations.setParent($.__views.vertical_container);
  $.__views.options = Alloy.createController('product/components/options', { id: "options", __parentSymbol: $.__views.vertical_container });
  $.__views.options.setParent($.__views.vertical_container);
  $.__views.cart_add = Alloy.createController('product/components/cartAdd', { id: "cart_add", __parentSymbol: $.__views.vertical_container });
  $.__views.cart_add.setParent($.__views.vertical_container);
  $.__views.__alloyId216 = Ti.UI.createView(
  { width: "100%", backgroundColor: Alloy.Styles.color.background.dark, height: 1, left: 0, top: 10, id: "__alloyId216" });

  $.__views.set_product.add($.__views.__alloyId216);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var logger = require('logging')('product:main:setProduct', getFullControllerPath($.__controllerPath));
  var lastSelectedColor;
  var eaUtils = require('EAUtils');




  $.listenTo($.cart_add, 'cartAdd:verify_variants', verifyVariants);
  $.listenTo($.cart_add, 'cartAdd:add_cart_changed', onAddCartChanged);




  $.name.addEventListener('click', navigateToProduct);
  $.product_id.getView().addEventListener('click', navigateToProduct);
  $.product_image.addEventListener('click', navigateToProduct);




  exports.init = init;
  exports.deinit = deinit;
  exports.getIsEnabled = getIsEnabled;
  exports.verifyVariants = verifyVariants;
  exports.getModel = getModel;










  function init() {
    logger.info('init called');
    var deferred = new _.Deferred();

    $.listenTo($model, 'change:selected_variant', selectionsHandler);
    $.listenTo($model, 'change:variation_values', variationChanged);

    $.cart_add.init({
      model: $model });

    $.pricing.init({
      model: $model });

    $.product_id.init();

    if ($model.isMaster()) {
      $model.trigger('change:selected_details', $model);
    } else if ($model.isVariant()) {
      $model.setSelectedVariant($model, {
        silent: true });

    } else if ($model.isStandard()) {
      $model.setSelectedVariant($model);

    }

    $.product_id.setId($model.getSelectedProductId());

    $.name.setText($model.getName());

    $model.ensureImagesLoaded('setProductImages').done(function () {
      var smallImages = $model.getSetProductImages();
      if (smallImages && smallImages.length > 0) {
        Alloy.Globals.getImageViewImage($.product_image, smallImages[0].getLink());
      }
      $.promotions.init({
        model: $model });

      $.variations.init({
        model: $model });

      $.options.init({
        model: $model });

      deferred.resolve();
    });
    return deferred.promise();
  }






  function deinit() {
    logger.info('deinit called');

    $.stopListening();

    $.name.removeEventListener('click', navigateToProduct);
    $.product_id.getView().removeEventListener('click', navigateToProduct);
    $.product_image.removeEventListener('click', navigateToProduct);

    $.pricing.deinit();
    $.promotions.deinit();
    $.variations.deinit();
    $.options.deinit();
    $.cart_add.deinit();

    $.destroy();
  }










  function getIsEnabled() {
    logger.info('getIsEnabled called');
    return $.cart_add.getEnabled();
  }






  function getModel() {
    logger.info('getModel called');
    return $model;
  }









  function navigateToProduct() {
    logger.info('navigateToProduct called');
    Alloy.Router.navigateToProduct({
      product_id: $model.getId(),
      variant_id: $model.getSelectedProductId() });

  }






  function verifyVariants() {
    logger.info('verifyVariants called');
    $.variations.verifySelectedVariations();
  }






  function variationChanged() {
    logger.info('variationChanged called');


    var selectedAttributes = $model.getVariationValues(),
    aid;
    for (aid in selectedAttributes) {
      if (aid === Alloy.CFG.product.color_attribute && lastSelectedColor != selectedAttributes[aid]) {
        var smallImages = $model.getSetProductImages(selectedAttributes[aid]);
        if (smallImages && smallImages.length > 0) {
          Alloy.Globals.getImageViewImage($.product_image, smallImages[0].getLink());
        }
      }
      lastSelectedColor = selectedAttributes[aid];
      break;
    }
  }






  function selectionsHandler() {
    logger.info('selectionsHandler called');
    var new_id = $model.getSelectedProductId();
    $.product_id.setId(new_id || _L('n/a'));
  }






  function onAddCartChanged() {
    logger.info('onAddCartChanged called');
    $.trigger('setProduct:add_cart_changed');
  }









  _.extend($, exports);
}

module.exports = Controller;