var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'product/productDetailBase').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'product/main/productBundleDetail';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  exports.destroy = function () {};




  _.extend($, $.__views);








  exports.baseController = 'product/productDetailBase';




  var logger = require('logging')('product:main:productBundleDetail', getFullControllerPath($.__controllerPath));




  exports.init = init;
  exports.deinit = deinit;
  exports.postInit = postInit;
  exports.getDetailsTabView = getDetailsTabView;










  var superInit = $.init;
  function init(info) {
    logger.info('init override called');

    var deferred = new _.Deferred();
    superInit(info).done(function () {
      deferred.resolve();
    }).fail(function () {
      deferred.reject();
    });

    return deferred.promise();
  }






  var superDeinit = $.deinit;
  function deinit() {
    logger.info('deinit called');
    $.pdp_details.deinit();
    superDeinit();
    $.stopListening();
    $.destroy();
  }






  function postInit() {
    var products = $.currentProduct.getBundledProducts();
    $.trigger('alt_image_selected', {
      productId: products[0].getId(),
      image: 0 });

  }










  function getDetailsTabView() {
    logger.info('getDetailsTabView called');
    var deferred = new _.Deferred();

    $.pdp_details = Alloy.createController('product/tabs/productDetailsTabBundle');

    $.pdp_details.init({
      replaceItem: $.replaceItem }).
    always(function () {

      $.currentProduct.setSelectedVariant($.currentProduct);
      deferred.resolve($.pdp_details.getView());
    });

    return deferred.promise();
  }









  _.extend($, exports);
}

module.exports = Controller;