var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'product/index';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.product_detail_window = Ti.UI.createView(
  { top: 55, id: "product_detail_window" });

  $.__views.product_detail_window && $.addTopLevelView($.__views.product_detail_window);
  $.__views.product_detail_container = Ti.UI.createView(
  { layout: "horizontal", id: "product_detail_container" });

  $.__views.product_detail_window.add($.__views.product_detail_container);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var analytics = require('analyticsBase');
  var logger = require('logging')('product:index', getFullControllerPath($.__controllerPath));
  var currentProduct = Alloy.Models.product;

  var historyCursor, replaceItem, selectedProductID, altImage, altProduct;




  $.product_detail_container.addEventListener('product:zoom', handleProductZoomSelection);




  exports.init = init;
  exports.render = render;
  exports.deinit = deinit;











  function init(options) {
    logger.info('init start');
    deinitPDP();

    historyCursor = options.historyCursor;
    replaceItem = options.replaceItem;
    var variantID = options.variant_id;
    altImage = null;
    altProduct = null;

    var productLoad = new _.Deferred();

    if (!options.product_id) {
      logger.info('NO PRODUCT ID AVAILABLE!');
      productLoad.resolve();
      return productLoad.promise();
    }

    if (currentProduct.id != options.product_id || _.keys(currentProduct.attributes).length < 1) {
      currentProduct.id = options.product_id;

      logger.info('fetching product ' + options.product_id + '... should be silent');
      var promise = currentProduct.fetchModel({
        silent: true });


      promise.done(function () {
        logger.info('init fetch done start');
        analytics.fireAnalyticsEvent({
          category: _L('Products'),
          action: _L('Product View'),
          label: currentProduct.getName() + ' (' + currentProduct.id + ')' });


        if (currentProduct.isMaster()) {


          var variants = currentProduct.getVariants();
          var variant = variants.length == 1 ? variants : variants.filter(function (v) {
            return v.getProductId() == variantID;
          });
          if (variant && variant.length) {
            currentProduct.setVariationValues(variant[0].getVariationValues(), {
              silent: true });

            currentProduct.setSelectedVariant(variant[0], {
              silent: true });

            render().done(function () {
              logger.info('init - Setting selected_variant and variation_values');
              productLoad.resolve();
              currentProduct.trigger('change:selected_variant', {});
            });
          } else {

            var vvs = currentProduct.getVariationValues();
            if (vvs && _.keys(vvs).length) {
              var filteredVariants = currentProduct.getOrderableVariants(vvs);

              if (filteredVariants && filteredVariants.length == 1) {
                var vId = filteredVariants[0].product_id;

                var variants = currentProduct.getVariants();
                var variant = variants.filter(function (v) {
                  return v.getProductId() == vId;
                });
                if (variant && variant.length) {
                  currentProduct.setVariationValues(variant[0].getVariationValues(), {
                    silent: true });

                  currentProduct.setSelectedVariant(variant[0], {
                    silent: true });

                }
              }
            }
            render().done(function () {
              productLoad.resolve();
              logger.info('init - I should be looking for selected_variant if my values are pre-filled!');
              if (variant && variant.length) {
                currentProduct.trigger('change:selected_variant', {});
              }
            });
          }
        } else if (currentProduct.isStandard()) {
          render().done(function () {
            logger.info('init - loading product prices for standard');
            currentProduct.getProductPrices().fail(function (response, status, err) {
              logger.error('getProductPrices fetch failed! \nmessage: ' + response.statusText + '\nerror: ' + err);
              productLoad.reject();
            }).done(function () {
              productLoad.resolve();
              logger.info('init - loaded product prices for standard or variant ... telling currentProduct to use selected_variant');
              currentProduct.setSelectedVariant(currentProduct);
            });
          });
        } else if (currentProduct.isVariant()) {

          var masterId = currentProduct.getMasterID();
          if (!masterId) {
            logger.error('Unable to get master id for ' + currentProduct.id);
            productLoad.reject();
            return;
          }
          var variantProduct = currentProduct.clone();
          currentProduct.clear({
            silent: true });

          currentProduct.id = masterId;
          logger.info('fetching master product ' + masterId + '... should be silent');
          var promise = currentProduct.fetchModel({
            silent: true });

          promise.done(function () {
            currentProduct.setVariationValues(variantProduct.getVariationValues(), {
              silent: true });

            currentProduct.setSelectedVariant(variantProduct, {
              silent: true });

            render().done(function () {
              logger.info('init - loading product prices for variant');
              currentProduct.getProductPrices().fail(function (response, status, err) {
                logger.error('getProductPrices fetch failed! \nmessage: ' + response.statusText + '\nerror: ' + err);
                productLoad.reject();
              }).done(function () {
                productLoad.resolve();
                logger.info('init - loaded product prices for standard or variant ... telling currentProduct to use selected_variant');
                currentProduct.trigger('change:selected_variant', {});
              });
            });
          }).fail(function (response, status, err) {
            logger.error('fetch failed! \nmessage: ' + response.statusText + '\nerror: ' + err);
            productLoad.reject();
          });
        } else {
          render().done(function () {
            productLoad.resolve();
          });
        }
        logger.info('init fetch done end');
      }).fail(function (response, status, err) {
        logger.error('fetch failed! \nmessage: ' + response.statusText + '\nerror: ' + err);
        productLoad.reject();
      });
    } else {

      render().done(function () {
        productLoad.resolve();
      });
    }
    logger.info('init end');

    return productLoad.promise();
  }






  function render() {
    logger.info('render start');

    var component = null;

    if (currentProduct.isBundle()) {
      component = 'product/main/productBundleDetail';
    } else if (currentProduct.isSet()) {
      component = 'product/main/productSetDetail';
    } else {
      component = 'product/main/productDetail';
    }

    $.product_detail = Alloy.createController(component, {
      product: currentProduct });

    $.listenTo($.product_detail, 'alt_image_selected', function (event) {
      altImage = event.image;
      altProduct = event.productId;
    });

    var deferred = new _.Deferred();
    $.product_detail.init({
      historyCursor: historyCursor,
      replaceItem: replaceItem }).
    always(function () {
      $.product_detail_container.add($.product_detail.getView());
      if (_.isFunction($.product_detail.postInit)) {
        $.product_detail.postInit();
      }
      deferred.resolve();
    });

    logger.info('render end');

    return deferred;
  }






  function deinit() {
    logger.info('DEINIT called');

    $.stopListening();
    deinitPDP();
    $.destroy();
    $.product_detail_container.removeEventListener('product:zoom', handleProductZoomSelection);

    logger.info('deinit end');
  }









  function deinitPDP() {
    if ($.product_detail) {
      removeAllChildren($.product_detail_container);
      $.stopListening($.product_detail);
      $.product_detail.deinit();
    }
  }










  function handleProductZoomSelection(event) {
    logger.info('handleProductZoomSelection start');

    if (!Alloy.CFG.enable_zoom_image) {
      return;
    }

    var productId = event.product_id;
    if (altProduct) {
      productId = altProduct;
    } else {
      productId = currentProduct.getId();
    }
    var selectedVariationValue = null;
    if (currentProduct.hasVariationValues()) {
      selectedVariationValue = currentProduct.getVariationValues()[Alloy.CFG.product.color_attribute];
    }
    Alloy.Dialog.showCustomDialog({
      controllerPath: 'product/images/imageZoom',
      options: {
        zIndex: 999 },

      initOptions: {
        model: currentProduct,
        selected_variation_value: selectedVariationValue,
        selected_alternate_image: altImage },

      continueEvent: 'image_zoom:dismiss' });


    logger.info('handleProductZoomSelection end');
  }









  _.extend($, exports);
}

module.exports = Controller;