var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'product/productDetailBase';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.product_order_details_view = Ti.UI.createView(
  { layout: "absolute", top: 0, left: 0, id: "product_order_details_view" });

  $.__views.product_order_details_view && $.addTopLevelView($.__views.product_order_details_view);
  $.__views.left_column = Ti.UI.createView(
  { top: 0, left: 0, layout: "vertical", width: 400, zIndex: 2, height: "100%", id: "left_column" });

  $.__views.product_order_details_view.add($.__views.left_column);
  $.__views.breadcrumb_bar = Ti.UI.createView(
  { layout: "horizontal", height: 49, backgroundColor: Alloy.Styles.tabs.backgroundColorDisabled, width: "100%", id: "breadcrumb_bar" });

  $.__views.left_column.add($.__views.breadcrumb_bar);
  $.__views.breadcrumbs_back_button = Ti.UI.createButton(
  { image: Alloy.Styles.backButtonImage, width: 49, height: 49, style: Ti.UI.iOS.SystemButtonStyle.PLAIN, id: "breadcrumbs_back_button", accessibilityLabel: "breadcrumbs_back_button" });

  $.__views.breadcrumb_bar.add($.__views.breadcrumbs_back_button);
  $.__views.__alloyId217 = Ti.UI.createView(
  { width: 1, backgroundColor: Alloy.Styles.color.background.dark, height: "100%", top: 0, id: "__alloyId217" });

  $.__views.breadcrumb_bar.add($.__views.__alloyId217);
  $.__views.primary_images = Alloy.createController('product/images/images', { id: "primary_images", __parentSymbol: $.__views.left_column });
  $.__views.primary_images.setParent($.__views.left_column);
  $.__views.vertical_shadow_separator_left = Ti.UI.createView(
  { left: 392, top: 0, width: 8, backgroundImage: Alloy.Styles.rightShadowImage, zIndex: 3, height: "100%", id: "vertical_shadow_separator_left" });

  $.__views.product_order_details_view.add($.__views.vertical_shadow_separator_left);
  $.__views.right_column = Ti.UI.createView(
  { top: 0, left: 400, layout: "vertical", width: 623, height: "100%", zIndex: 4, id: "right_column" });

  $.__views.product_order_details_view.add($.__views.right_column);
  $.__views.pdp_tab_buttons = Ti.UI.createView(
  { layout: "horizontal", height: 49, id: "pdp_tab_buttons" });

  $.__views.right_column.add($.__views.pdp_tab_buttons);
  $.__views.details_button = Ti.UI.createButton(
  { width: 207, height: 49, color: Alloy.Styles.color.text.dark, font: Alloy.Styles.buttonFont, style: Ti.UI.iOS.SystemButtonStyle.PLAIN, backgroundColor: Alloy.Styles.tabs.backgroundColorEnabled, titleid: "_Details", id: "details_button", associatedView: "pdp_details_tab", includePdpHeader: "yes", accessibilityValue: "pdp_details_tab_button" });

  $.__views.pdp_tab_buttons.add($.__views.details_button);
  $.__views.__alloyId218 = Ti.UI.createView(
  { width: 1, backgroundColor: Alloy.Styles.color.background.dark, height: "100%", top: 0, id: "__alloyId218" });

  $.__views.pdp_tab_buttons.add($.__views.__alloyId218);
  $.__views.description_button = Ti.UI.createButton(
  { width: 207, height: 49, color: Alloy.Styles.color.text.dark, font: Alloy.Styles.buttonFont, style: Ti.UI.iOS.SystemButtonStyle.PLAIN, backgroundColor: Alloy.Styles.tabs.backgroundColorDisabled, titleid: "_Description", id: "description_button", associatedView: "pdp_description_tab", includePdpHeader: "yes", accessibilityValue: "pdp_description_tab_button" });

  $.__views.pdp_tab_buttons.add($.__views.description_button);
  $.__views.recommendations_separator = Ti.UI.createView(
  { width: 1, backgroundColor: Alloy.Styles.color.background.dark, height: "100%", top: 0, id: "recommendations_separator" });

  $.__views.pdp_tab_buttons.add($.__views.recommendations_separator);
  $.__views.recommendations_button = Ti.UI.createButton(
  { width: 207, height: 49, color: Alloy.Styles.color.text.dark, font: Alloy.Styles.buttonFont, style: Ti.UI.iOS.SystemButtonStyle.PLAIN, backgroundColor: Alloy.Styles.tabs.backgroundColorDisabled, titleid: "_Recommendations", id: "recommendations_button", associatedSeparator: "recommendations_separator", associatedView: "pdp_recommendations_tab", accessibilityValue: "pdp_recommendations_tab_button" });

  $.__views.pdp_tab_buttons.add($.__views.recommendations_button);
  $.__views.store_availability_separator = Ti.UI.createView(
  { width: 1, backgroundColor: Alloy.Styles.color.background.dark, height: "100%", top: 0, id: "store_availability_separator", visible: false });

  $.__views.pdp_tab_buttons.add($.__views.store_availability_separator);
  $.__views.store_availability_button = Ti.UI.createButton(
  { width: 207, height: 49, color: Alloy.Styles.color.text.dark, font: Alloy.Styles.buttonFont, style: Ti.UI.iOS.SystemButtonStyle.PLAIN, backgroundColor: Alloy.Styles.tabs.backgroundColorDisabled, titleid: "_Inventory", visible: false, id: "store_availability_button", associatedSeparator: "store_availability_separator", associatedView: "pdp_store_availability_tab", accessibilityValue: "pdp_store_availability_tab_button" });

  $.__views.pdp_tab_buttons.add($.__views.store_availability_button);
  $.__views.tab_page_container = Ti.UI.createView(
  { layout: "vertical", left: 27, width: 596, height: Ti.UI.FILL, id: "tab_page_container" });

  $.__views.right_column.add($.__views.tab_page_container);
  $.__views.pdp_header = Ti.UI.createView(
  { height: Ti.UI.SIZE, width: "100%", id: "pdp_header" });

  $.__views.tab_page_container.add($.__views.pdp_header);
  $.__views.pdp_details_tab = Ti.UI.createView(
  { layout: "vertical", width: Ti.UI.SIZE, height: Ti.UI.FILL, visible: true, left: 7, id: "pdp_details_tab" });

  $.__views.tab_page_container.add($.__views.pdp_details_tab);
  $.__views.pdp_description_tab = Ti.UI.createView(
  { height: 0, visible: false, layout: "vertical", width: 0, left: 7, id: "pdp_description_tab" });

  $.__views.tab_page_container.add($.__views.pdp_description_tab);
  $.__views.product_description = Alloy.createController('product/tabs/description', { id: "product_description", __parentSymbol: $.__views.pdp_description_tab });
  $.__views.product_description.setParent($.__views.pdp_description_tab);
  $.__views.pdp_recommendations_tab = Ti.UI.createView(
  { height: 0, visible: false, layout: "vertical", width: 0, left: 0, id: "pdp_recommendations_tab" });

  $.__views.tab_page_container.add($.__views.pdp_recommendations_tab);
  $.__views.recommendations_container = Alloy.createController('product/tabs/recommendations', { id: "recommendations_container", __parentSymbol: $.__views.pdp_recommendations_tab });
  $.__views.recommendations_container.setParent($.__views.pdp_recommendations_tab);
  $.__views.pdp_store_availability_tab = Ti.UI.createView(
  { height: 0, visible: false, layout: "vertical", width: 0, left: 0, id: "pdp_store_availability_tab" });

  $.__views.tab_page_container.add($.__views.pdp_store_availability_tab);
  $.__views.store_availability_container = Alloy.createController('product/tabs/storeAvailability', { id: "store_availability_container", __parentSymbol: $.__views.pdp_store_availability_tab });
  $.__views.store_availability_container.setParent($.__views.pdp_store_availability_tab);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var eaUtils = require('EAUtils');
  var logger = require('logging')('product:productDetailBase', getFullControllerPath($.__controllerPath));

  var args = arguments[0] || {},
  historyCursor;

  $.replaceItem = null;


  $.currentProduct = Alloy.Models.product;
  $.currentBasket = Alloy.Models.basket;


  $.pdpTabButtons = [$.details_button, $.description_button];




  $.listenTo($.primary_images, 'alt_image_selected', function (event) {
    $.trigger('alt_image_selected', event);
  });

  $.pdp_tab_buttons.addEventListener('click', pdpTabButtonsClickEventHandler);
  $.breadcrumbs_back_button.addEventListener('click', breadcrumbsBackButtonClickEventHandler);




  $.listenTo($.currentProduct, 'change:selected_variant', selectionsHandler);




  exports.init = init;
  exports.deinit = deinit;
  exports.getDetailsTabView = getDetailsTabView;
  exports.getHeaderView = getHeaderView;











  function init(info) {
    logger.info('base init called');
    var deferred = new _.Deferred();

    historyCursor = info.historyCursor;
    $.replaceItem = info.replaceItem;

    $.primary_images.init();
    $.product_description.init();

    render().done(function () {
      deferred.resolve();
    });

    logger.info('base init end');

    return deferred.promise();
  }







  function render() {
    logger.info('base render called');

    var pdp_header_view = $.getHeaderView();
    if (pdp_header_view) {
      $.pdp_header.add(pdp_header_view);
    }

    initializeElements();

    var promise = $.getDetailsTabView().always(function (pdp_details_view) {
      if (pdp_details_view) {
        $.pdp_details_tab.add(pdp_details_view);
      }
    });

    logger.info('base render end');
    return promise;
  }






  function deinit() {
    logger.info('base deinit called');


    $.stopListening();

    $.pdp_tab_buttons.removeEventListener('click', pdpTabButtonsClickEventHandler);
    $.breadcrumbs_back_button.removeEventListener('click', breadcrumbsBackButtonClickEventHandler);


    $.primary_images.deinit();
    $.pdp_header_controller && $.pdp_header_controller.deinit();


    $.product_description.deinit();
    $.recommendations_container.deinit();
    $.store_availability_container.deinit();

    removeAllChildren($.pdp_details_tab);
    removeAllChildren($.pdp_header);

    $.destroy();
    logger.info('base deinit end');
  }











  function getDetailsTabView() {
    logger.info('base getDetailsTabView called');

    return null;
  }






  function getHeaderView() {
    logger.info('base getHeaderView called');

    $.pdp_header_controller = Alloy.createController('product/components/detailHeader');

    $.pdp_header_controller.init();

    return $.pdp_header_controller.getView();
  }









  function initializeElements() {
    logger.info('base initializeElements');

    var associatedSeparator;

    if (!Alloy.CFG.product.recommendations.enabled) {
      $.recommendations_button.hide();
      $.recommendations_button.setWidth(0);
      associatedSeparator = $.recommendations_button.associatedSeparator;
      $[associatedSeparator].hide();
      $[associatedSeparator].setWidth(0);
    } else {
      $.pdpTabButtons.push($.recommendations_button);
    }
    if (Alloy.CFG.store_availability.enabled && !$.currentProduct.isSet()) {
      $.pdpTabButtons.push($.store_availability_button);
      $.store_availability_button.show();
      associatedSeparator = $.store_availability_button.associatedSeparator;
      $[associatedSeparator].show();
      $[associatedSeparator].setWidth(1);

      $.store_availability_button.setColor(Alloy.Styles.color.text.light);
    } else {
      associatedSeparator = $.store_availability_button.associatedSeparator;
      $[associatedSeparator].hide();
      $[associatedSeparator].setWidth(0);
    }

    var size = 620 / $.pdpTabButtons.length;
    _.each($.pdpTabButtons, function (button) {
      button.setWidth(size);
    });


    $.primary_images.resetVariationSelection($.currentProduct.getVariationValues());
  }









  function pdpTabButtonsClickEventHandler(event) {
    logger.info('pdpTabButtonsClickEventHandler called');

    if (!event.source.associatedView) {
      return;
    }
    var i = 0,
    button,
    associatedView = event.source.associatedView,
    includeHeader,
    headerView = $.pdp_header_controller.getView();
    var productSelected = $.currentProduct.isProductSelected();

    $.tab_page_container.hide();

    while (button = $.pdpTabButtons[i++]) {
      if (event.source === button) {



        if ($[associatedView] !== $.pdp_store_availability_tab || $[associatedView] === $.pdp_store_availability_tab && productSelected) {
          button.setBackgroundColor(Alloy.Styles.tabs.backgroundColorEnabled);
          var isVisible = headerView.getVisible();
          var showHeader = button.includePdpHeader === 'yes';
          if (showHeader != isVisible) {
            if (showHeader) {
              headerView.setHeight(Ti.UI.SIZE);
              headerView.setWidth(Ti.UI.SIZE);
              headerView.show();
            } else {
              hideView(headerView);
            }
          }
        }
        var view = $[button.associatedView];


        if (view === $.pdp_store_availability_tab) {

          if (productSelected) {
            $.store_availability_container.init().done(function () {
              showView(view);
            });
          } else {
            notify(_L('Select a variation first'));
          }
        } else if (view === $.pdp_recommendations_tab) {
          $.recommendations_container.init().done(function () {
            showView(view);
          });
        } else {
          showView(view);
        }
      } else {


        if ($[associatedView] === $.pdp_store_availability_tab && !productSelected) {
          continue;
        }
        hideView($[button.associatedView]);
        button.setBackgroundColor(Alloy.Styles.tabs.backgroundColorDisabled);
      }
    }
    $.tab_page_container.show();
  }







  function showView(view) {

    view.setHeight(Ti.UI.FILL);
    view.setWidth(Ti.UI.SIZE);
    view.show();
  }







  function hideView(view) {
    view.hide();
    view.setHeight(0);
    view.setWidth(0);
  }





  function breadcrumbsBackButtonClickEventHandler() {
    eaUtils.returnToLastSearchOrProduct(historyCursor, $.currentProduct.getId());
  }






  function selectionsHandler() {
    logger.info('selection handler');

    if ($.currentProduct.isProductSelected()) {
      $.store_availability_button.setColor(Alloy.Styles.color.text.mediumdark);
    } else {
      $.store_availability_button.setColor(Alloy.Styles.color.text.light);
    }

    $.pdp_header_controller.setProductID($.currentProduct.getSelectedProductId());
  }









  _.extend($, exports);
}

module.exports = Controller;