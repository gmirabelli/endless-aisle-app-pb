var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
		delete obj[key];
	}
	return arg;
}

function Controller() {

	require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'product/tabs/productDetailsTab';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};







	$.__views.base_product = Ti.UI.createView(
	{ layout: "vertical", id: "base_product" });

	$.__views.base_product && $.addTopLevelView($.__views.base_product);
	$.__views.promotions = Alloy.createController('product/components/promotions', { id: "promotions", __parentSymbol: $.__views.base_product });
	$.__views.promotions.setParent($.__views.base_product);
	$.__views.variations = Alloy.createController('product/components/variations', { id: "variations", __parentSymbol: $.__views.base_product });
	$.__views.variations.setParent($.__views.base_product);
	$.__views.options = Alloy.createController('product/components/options', { id: "options", __parentSymbol: $.__views.base_product });
	$.__views.options.setParent($.__views.base_product);
	$.__views.cart_add = Alloy.createController('product/components/cartAdd', { id: "cart_add", __parentSymbol: $.__views.base_product });
	$.__views.cart_add.setParent($.__views.base_product);
	exports.destroy = function () {};




	_.extend($, $.__views);










	var logger = require('logging')('product:tabs:productDetailsTab', getFullControllerPath($.__controllerPath));




	$.listenTo($.cart_add, 'cartAdd:verify_variants', verifyVariants);




	exports.init = init;
	exports.deinit = deinit;











	function init(args) {
		logger.info('init called');
		var deferred = new _.Deferred();
		$.promotions.init();
		$.variations.init();
		$.options.init({
			replaceItem: args.replaceItem });

		$.cart_add.init({
			replaceItem: args.replaceItem });

		deferred.resolve();
		return deferred.promise();
	}






	function deinit() {
		logger.info('deinit called');
		$.promotions.deinit();
		$.variations.deinit();
		$.options.deinit();
		$.cart_add.deinit();

		$.stopListening();
		$.destroy();
	}









	function verifyVariants() {
		logger.info('verifyVariants called');
		$.variations.verifySelectedVariations();
	}









	_.extend($, exports);
}

module.exports = Controller;