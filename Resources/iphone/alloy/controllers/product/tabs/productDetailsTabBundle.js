var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'product/tabs/productDetailsTabBundle';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.bundle_product = Ti.UI.createView(
  { layout: "vertical", width: "100%", height: "100%", id: "bundle_product" });

  $.__views.bundle_product && $.addTopLevelView($.__views.bundle_product);
  $.__views.promotions = Alloy.createController('product/components/promotions', { id: "promotions", __parentSymbol: $.__views.bundle_product });
  $.__views.promotions.setParent($.__views.bundle_product);
  $.__views.bundle_products = Ti.UI.createScrollView(
  { layout: "vertical", right: 20, height: "70%", showVerticalScrollIndicator: true, id: "bundle_products" });

  $.__views.bundle_product.add($.__views.bundle_products);
  $.__views.cart_add = Alloy.createController('product/components/cartAdd', { id: "cart_add", __parentSymbol: $.__views.bundle_product });
  $.__views.cart_add.setParent($.__views.bundle_product);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var bundledProductControllers = [];
  var allVariations = [];
  var logger = require('logging')('product:tabs:productDetailsTabBundle', getFullControllerPath($.__controllerPath));
  var currentProduct = Alloy.Models.product;




  $.listenTo($.cart_add, 'cartAdd:verify_variants', verifyVariants);




  exports.init = init;
  exports.deinit = deinit;










  function init(args) {
    logger.info('init called');

    $.promotions.init();
    $.cart_add.init({
      replaceItem: args.replaceItem });


    var bundledProductsModels = _.extend([], currentProduct.getBundledProducts());


    var deferred = createBundledProduct(bundledProductsModels.shift(), args.replaceItem);
    return bundledProductsModels.reduce(function (sequence, bundledProductModel) {
      return sequence.then(function () {
        return createBundledProduct(bundledProductModel, args.replaceItem);
      });
    }, deferred);
  }






  function deinit() {
    logger.info('deinit called');
    $.promotions.deinit();
    $.cart_add.deinit();
    _.each(bundledProductControllers, function (controller) {
      controller.deinit();
    });
    removeAllChildren($.bundle_products);

    $.stopListening();
    $.destroy();
  }










  function createBundledProduct(model, replaceItem) {
    logger.info('createBundledProduct called');
    var bundledProduct = Alloy.createController('product/main/bundledProduct', {
      $model: model });

    $.bundle_products.add(bundledProduct.getView());
    bundledProductControllers.push(bundledProduct);
    return bundledProduct.init({
      replaceItem: replaceItem }).
    always(function () {
      allVariations.push(bundledProduct.variations);
    });
  }









  function verifyVariants() {
    logger.info('verifyVariants called');
    _.each(allVariations, function (variationGroup) {
      if (variationGroup) {
        variationGroup.verifySelectedVariations();
      }
    });
  }









  _.extend($, exports);
}

module.exports = Controller;