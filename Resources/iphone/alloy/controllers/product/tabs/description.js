var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
		delete obj[key];
	}
	return arg;
}

function Controller() {

	require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'product/tabs/description';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};







	$.__views.product_description = Ti.UI.createWebView(
	{ width: "100%", height: 465, top: 22, willHandleTouches: false, horizontalWrap: true, disableBounce: true, id: "product_description", accessibilityValue: "product_description" });

	$.__views.product_description && $.addTopLevelView($.__views.product_description);
	exports.destroy = function () {};




	_.extend($, $.__views);










	var args = arguments[0] || {};

	var currentProduct = args.product || Alloy.Models.product;
	var logger = require('logging')('product:tabs:description', getFullControllerPath($.__controllerPath));




	exports.init = init;
	exports.deinit = deinit;









	function init() {
		logger.info('init called');
		render();
	}






	function render() {
		logger.info('render called');
		$.product_description.setHtml('');

		var shortDescription = currentProduct.getShortDescription();
		var longDescription = currentProduct.getLongDescription();
		if (!shortDescription && !longDescription) {
			return;
		}

		var descriptionText = shortDescription || longDescription;

		var html = '<meta name="viewport" content="user-scalable=0,initial-scale=0">' + '<style>' + Alloy.CFG.webViewCssReset + (Alloy.CFG.product.descriptionWebViewCss || '') + '</style>' + '<div>' + descriptionText + '</div>';

		$.product_description.setHtml(html);
	}






	function deinit() {
		logger.info('deinit called');
		$.stopListening();
		$.destroy();
	}









	_.extend($, exports);
}

module.exports = Controller;