var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'product/tabs/storeAvailability';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};




  $.storeInventoryData = Alloy.createCollection('store');


  $.__views.store_availability_container = Ti.UI.createView(
  { layout: "vertical", width: Ti.UI.SIZE, height: Ti.UI.SIZE, top: 20, id: "store_availability_container" });

  $.__views.store_availability_container && $.addTopLevelView($.__views.store_availability_container);
  $.__views.no_store_availability_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, visible: false, font: Alloy.Styles.buttonFont, textid: "_There_is_no_store_availability_information_for_this_product_", id: "no_store_availability_label", accessibilityValue: "no_store_availability_label" });

  $.__views.store_availability_container.add($.__views.no_store_availability_label);
  $.__views.variation_container = Ti.UI.createView(
  { visible: false, height: Ti.UI.SIZE, top: 0, left: 0, right: 50, bottom: 10, layout: "vertical", backgroundColor: Alloy.Styles.color.background.white, id: "variation_container" });

  $.__views.store_availability_container.add($.__views.variation_container);
  $.__views.store_availability_view = Ti.UI.createTableView(
  { visible: false, height: 475, left: 0, right: 50, rowHeight: Ti.UI.SIZE, borderColor: Alloy.Styles.color.border.black, backgroundColor: Alloy.Styles.color.background.white, scrollable: true, separatorStyle: Ti.UI.TABLE_VIEW_SEPARATOR_STYLE_NONE, id: "store_availability_view" });

  $.__views.store_availability_container.add($.__views.store_availability_view);
  var __alloyId221 = Alloy.Collections['$.storeInventoryData'] || $.storeInventoryData;function __alloyId222(e) {if (e && e.fromAdapter) {return;}var opts = __alloyId222.opts || {};var models = __alloyId221.models;var len = models.length;var rows = [];for (var i = 0; i < len; i++) {var __alloyId219 = models[i];__alloyId219.__transform = transformStoreDetail(__alloyId219);var __alloyId220 = Alloy.createController('product/components/storeDetail', { $model: __alloyId219, __parentSymbol: __parentSymbol });
      rows.push(__alloyId220.getViewEx({ recurse: true }));
    }$.__views.store_availability_view.setData(rows);};__alloyId221.on('fetch destroy change add remove reset', __alloyId222);$.__views.store_availability_controls = Ti.UI.createView(
  { visible: false, top: 0, left: 0, right: 50, backgroundColor: Alloy.Styles.color.background.white, layout: "absolute", id: "store_availability_controls" });

  $.__views.store_availability_container.add($.__views.store_availability_controls);
  $.__views.last_btn = Ti.UI.createButton(
  { style: Ti.UI.iOS.SystemButtonStyle.PLAIN, color: Alloy.Styles.color.text.green, disabledColor: Alloy.Styles.color.text.light, top: 0, font: Alloy.Styles.smallButtonFont, left: 0, id: "last_btn", titleid: "_Last_5", accessibilityValue: "last_btn" });

  $.__views.store_availability_controls.add($.__views.last_btn);
  $.__views.next_btn = Ti.UI.createButton(
  { style: Ti.UI.iOS.SystemButtonStyle.PLAIN, color: Alloy.Styles.color.text.green, disabledColor: Alloy.Styles.color.text.light, top: 0, font: Alloy.Styles.smallButtonFont, right: 0, id: "next_btn", titleid: "_Next_5", accessibilityValue: "next_btn" });

  $.__views.store_availability_controls.add($.__views.next_btn);
  exports.destroy = function () {__alloyId221 && __alloyId221.off('fetch destroy change add remove reset', __alloyId222);};




  _.extend($, $.__views);










  var args = arguments[0] || {},
  currentProduct = Alloy.Models.product;

  var product = null,
  storeInventoryCurrentPage = 0;

  var logger = require('logging')('product:tabs:storeAvailability', getFullControllerPath($.__controllerPath));




  $.last_btn.addEventListener('click', lastButtonClickEventHandler);
  $.next_btn.addEventListener('click', nextButtonClickEventHandler);




  exports.init = init;
  exports.deinit = deinit;










  function init() {
    logger.info('init called');
    return render();
  }







  function render() {
    logger.info('render called');
    var deferred = new _.Deferred();
    updateDisplay();

    product = currentProduct.getSelectedVariant();
    if (!product) {
      product = currentProduct;
    }

    if (product) {
      load5Stores().always(function () {
        deferred.resolve();
      });
    } else {
      deferred.resolve();
    }
    return deferred.promise();
  }






  function deinit() {
    logger.info('deinit called');
    $.stopListening();
    $.last_btn.removeEventListener('click', lastButtonClickEventHandler);
    $.next_btn.removeEventListener('click', nextButtonClickEventHandler);
    removeAllChildren($.variation_container);
    $.destroy();
  }










  function load5Stores() {
    logger.info('load5Stores called');
    var deferred = new _.Deferred();
    if (!Alloy.CFG.store_availability.enabled) {
      deferred.resolve();
      return deferred;
    }

    if (!product) {
      deferred.resolve();
      return deferred;
    }

    $.storeInventoryData.reset([]);

    var start = storeInventoryCurrentPage * 5;

    var fiveStores = Alloy.Collections.allStores.getNextNStores(start, 5);

    Alloy.Router.showActivityIndicator(deferred);
    Alloy.Collections.allStores.getInventory(product, start, 5).done(function () {
      _.each(fiveStores, function (store) {
        var storeInv = store.getInventoryId();
        if (storeInv) {
          var storeAvailability = currentProduct.getInventoryAvailability(product, storeInv);
          if (storeAvailability.levels && storeAvailability.levels.stockLevel) {
            var stockLevelMsg = storeAvailability.levels.stockLevel != 999999 ? storeAvailability.levels.stockLevel + ' ' + _L('In_Stock') : _L('Perpetual_Stock');
            if (storeAvailability.levels.stockLevel > 100000) {
              stockLevelMsg = String.formatDecimal(100000) + '+ ' + _L('In_Stock');
            }
            store.setAvailabilityDetails(storeAvailability.message, determineAvailabilityTextColor(storeAvailability.levels), stockLevelMsg);
          } else {
            store.setAvailabilityDetails(_L('Unknown_Availability'), Alloy.Styles.color.text.dark, _L('Unknown_Stock_Level'));
          }
        } else {
          store.setAvailabilityDetails(_L('Unknown_Availability'), Alloy.Styles.color.text.dark, _L('Unknown_Stock_Level'));
        }
      });
      $.storeInventoryData.reset(fiveStores);
      $.store_availability_view.scrollToTop(0);

      updateStoreAvailabilityButtons();
    }).always(function () {
      deferred.resolve();
    });
    return deferred.promise();
  }






  function updateDisplay() {
    logger.info('updateDisplay called');
    product = currentProduct.getSelectedVariant();
    if (!product) {
      product = currentProduct;
    }

    removeAllChildren($.variation_container);

    if (product) {
      $.no_store_availability_label.setVisible(false);
      $.no_store_availability_label.setHeight(0);
      $.store_availability_view.setVisible(true);
      $.store_availability_controls.setVisible(true);
      $.variation_container.setVisible(true);

      var vvs = product.getVariationValues();

      for (var name in vvs) {
        var displayName = currentProduct.getVariationAttributeDisplayName(name);
        var value = currentProduct.getVariationAttributeByName(name);
        var label = Ti.UI.createLabel($.createStyle({
          classes: ['heading_label'],
          apiName: 'Label',
          text: String.format(_L('%s: %s'), displayName, value) }));

        $.variation_container.add(label);
      }
    } else {
      $.no_store_availability_label.setVisible(true);
      $.no_store_availability_label.setHeight(30);
      $.store_availability_view.setVisible(false);
      $.store_availability_controls.setVisible(false);
      $.variation_container.setVisible(false);
    }
  }






  function updateStoreAvailabilityButtons() {
    logger.info('updateStoreAvailabilityButtons called');
    var maxPageNum = Math.floor(Alloy.Collections.allStores.length / 5);
    if (Alloy.Collections.allStores.length % 5 == 0) {
      maxPageNum--;
    }
    $.next_btn.setEnabled(storeInventoryCurrentPage < maxPageNum);
    $.last_btn.setEnabled(storeInventoryCurrentPage > 0);
  }







  function determineAvailabilityTextColor(levels) {
    logger.info('determineAvailabilityTextColor called');
    var color = Alloy.Styles.color.text.dark;
    if (!levels) {
      return color;
    }

    if (levels.preorder > 0) {
      color = Alloy.Styles.color.text.blue;
    }

    if (levels.backorder > 0) {
      color = Alloy.Styles.accentColor;
    }

    if (levels.notAvailable > 0) {
      color = Alloy.Styles.color.text.red;
    }

    return color;
  }







  function transformStoreDetail(model) {
    return model.constructStoreAvailability();
  }









  function lastButtonClickEventHandler() {
    if (storeInventoryCurrentPage > 0) {
      storeInventoryCurrentPage--;
      load5Stores();
    }
    updateStoreAvailabilityButtons();
  }






  function nextButtonClickEventHandler() {
    if (storeInventoryCurrentPage < Math.floor(Alloy.Collections.allStores.length / 5)) {
      storeInventoryCurrentPage++;
      load5Stores();
    }
    updateStoreAvailabilityButtons();
  }









  _.extend($, exports);
}

module.exports = Controller;