var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'product/tabs/productDetailsTabSet';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.set_product = Ti.UI.createView(
  { layout: "vertical", width: "100%", height: "100%", id: "set_product" });

  $.__views.set_product && $.addTopLevelView($.__views.set_product);
  $.__views.promotions = Alloy.createController('product/components/promotions', { id: "promotions", __parentSymbol: $.__views.set_product });
  $.__views.promotions.setParent($.__views.set_product);
  $.__views.set_products = Ti.UI.createScrollView(
  { layout: "vertical", right: 20, height: "100%", showVerticalScrollIndicator: true, id: "set_products" });

  $.__views.set_product.add($.__views.set_products);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var setProductControllers = [];
  var logger = require('logging')('product:tabs:productDetailsTabSet', getFullControllerPath($.__controllerPath));
  var currentProduct = Alloy.Models.product;
  var currentBasket = Alloy.Models.basket;
  var analytics = require('analyticsBase');




  exports.init = init;
  exports.deinit = deinit;
  exports.verifyVariants = verifyVariants;
  exports.getAllEnabled = getAllEnabled;
  exports.addAllToCart = addAllToCart;









  function init(args) {
    logger.info('init called');
    $.promotions.init();

    var setProductsModels = _.extend([], currentProduct.getSetProducts());


    var deferred = createSetProduct(setProductsModels.shift());
    return setProductsModels.reduce(function (sequence, setProductModel) {
      return sequence.then(function () {
        return createSetProduct(setProductModel);
      });
    }, deferred);
  }






  function deinit() {
    logger.info('deinit called');
    $.promotions.deinit();
    _.each(setProductControllers, function (setProduct) {
      setProduct.deinit();
    });
    removeAllChildren($.set_products);

    $.stopListening();
    $.destroy();
  }










  function createSetProduct(model) {
    var setProduct = Alloy.createController('product/main/setProduct', {
      $model: model });

    $.set_products.add(setProduct.getView());
    setProductControllers.push(setProduct);
    $.listenTo(setProduct, 'setProduct:add_cart_changed', onAddCartChanged);
    return setProduct.init();
  }









  function verifyVariants() {
    logger.info('verifyVariants called');
    var scrolledToFirstVariationError = false;
    _.each(setProductControllers, function (setProduct, index) {
      if (setProduct) {
        setProduct.verifyVariants();
        if (!scrolledToFirstVariationError && !setProduct.getIsEnabled()) {
          if ($.set_products.getChildren()[index]) {
            scrolledToFirstVariationError = true;
            $.set_products.scrollTo(0, $.set_products.getChildren()[index].getRect().y);
          }
        }
      }
    });
  }







  function getAllEnabled() {
    logger.info('getAllEnabled called');
    var productsEnabled = [];
    _.each(setProductControllers, function (setProduct, index) {
      productsEnabled[index] = setProduct.getIsEnabled();
    });
    return _.every(productsEnabled);
  }






  function addAllToCart() {
    logger.info('addAllToCart called');


    var productInfos = [];
    var productIds = [];
    _.each(setProductControllers, function (setProductController) {
      var model = setProductController.getModel();
      var product_info = model.getSelectedProductInfo();
      product_info.c_employee_id = Alloy.Models.associate.getEmployeeId();
      product_info.c_store_id = Alloy.CFG.store_id;
      productIds.push(model.getSelectedVariant().getProductId());
      productInfos.push(product_info);
    });
    var promise = currentBasket.addProducts(productInfos);
    promise.done(function () {
      analytics.fireAnalyticsEvent({
        category: _L('Basket'),
        action: _L('Add All To Basket'),
        label: currentProduct.getName() + ' (' + productIds.join(', ') + ')' });

      notify(_L('Items added to the cart'));
    }).fail(function (response) {
      logger.error('failure adding products to cart: ' + JSON.stringify(response));
      var msg = _L('Unable to add items to the cart');
      if (response) {
        var fault = response.get('fault');
        if (fault && fault.message && fault.message != '') {
          msg = fault.message;
        }
      }
      notify(msg, {
        preventAutoClose: true });

    }).always(function () {
      processing = false;
    });
    Alloy.Router.showActivityIndicator(promise);
  }





  function onAddCartChanged() {
    logger.info('onAddCartChanged called');
    $.trigger('productDetailsTabSet:add_cart_changed');
  }









  _.extend($, exports);
}

module.exports = Controller;