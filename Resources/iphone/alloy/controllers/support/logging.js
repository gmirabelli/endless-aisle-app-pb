var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'support/logging';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.logging = Ti.UI.createView(
  { tabBarHidden: false, navBarHidden: false, id: "logging" });

  $.__views.logging && $.addTopLevelView($.__views.logging);
  $.__views.logging_main = Ti.UI.createView(
  { layout: "horizontal", width: "100%", height: "100%", id: "logging_main" });

  $.__views.logging.add($.__views.logging_main);
  $.__views.data_column = Ti.UI.createView(
  { layout: "vertical", width: "70%", height: "100%", id: "data_column" });

  $.__views.logging_main.add($.__views.data_column);
  $.__views.console_text = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.accentColor, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailLabelFont, left: 20, textid: "_Last_100_lines_of_console_log", top: 20, id: "console_text", accessibilityValue: "console_text" });

  $.__views.data_column.add($.__views.console_text);
  $.__views.console_scrollView = Ti.UI.createScrollView(
  { width: Ti.UI.FILL, height: Ti.UI.FILL, borderColor: Alloy.Styles.selectLists.optionsListBorderColor, showVerticalScrollIndicator: true, borderWidth: 2, borderRadius: 2, left: 20, right: 20, bottom: 20, id: "console_scrollView" });

  $.__views.data_column.add($.__views.console_scrollView);
  $.__views.console_tail = Ti.UI.createLabel(
  { width: 800, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.lineItemFont, id: "console_tail", accessibilityValue: "console_tail" });

  $.__views.console_scrollView.add($.__views.console_tail);
  $.__views.vertical_border = Ti.UI.createView(
  { width: 2, backgroundColor: Alloy.Styles.color.border.darkest, id: "vertical_border" });

  $.__views.logging_main.add($.__views.vertical_border);
  $.__views.action_column = Ti.UI.createScrollView(
  { layout: "vertical", width: "28%", contentWidth: "28%", showVerticalScrollIndicator: true, height: "100%", id: "action_column" });

  $.__views.logging_main.add($.__views.action_column);
  $.__views.action_section = Ti.UI.createView(
  { top: 15, left: 20, height: Ti.UI.SIZE, layout: "vertical", textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, id: "action_section" });

  $.__views.action_column.add($.__views.action_section);
  $.__views.action_title = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.accentColor, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailLabelFont, left: 4, textid: "_Actions_", id: "action_title", accessibilityValue: "action_title" });

  $.__views.action_section.add($.__views.action_title);
  $.__views.categories_text = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailValueFont, left: 4, top: 10, bottom: 10, textid: "_Loggable_Categories", id: "categories_text", accessibilityValue: "categories_text" });

  $.__views.action_section.add($.__views.categories_text);
  $.__views.runtime_cats = Ti.UI.createTextField(
  { borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED, left: 0, width: "90%", autocapitalization: false, id: "runtime_cats", accessibilityLabel: "runtime_cats" });

  $.__views.action_section.add($.__views.runtime_cats);
  $.__views.apply_runtime_cats_btn = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.buttons.primary.color, disabledColor: Alloy.Styles.buttons.primary.disabledColor, backgroundImage: Alloy.Styles.buttons.primary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.primary.backgroundDisabledImage, height: 30, width: Ti.UI.FILL, font: Alloy.Styles.smallButtonFont, top: 10, bottom: 20, titleid: "_Apply", id: "apply_runtime_cats_btn", accessibilityValue: "apply_runtime_cats_btn" });

  $.__views.action_section.add($.__views.apply_runtime_cats_btn);
  $.__views.effective_categories_text = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailValueFont, left: 4, top: 10, bottom: 10, textid: "_Effective_Loggable_Categories", id: "effective_categories_text", accessibilityValue: "effective_categories_text" });

  $.__views.action_section.add($.__views.effective_categories_text);
  $.__views.effective_cats = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailValueFont, left: 20, top: 10, bottom: 10, id: "effective_cats", value: "--", accessibilityValue: "effective_cats" });

  $.__views.action_section.add($.__views.effective_cats);
  $.__views.reload_console_btn = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.buttons.primary.color, disabledColor: Alloy.Styles.buttons.primary.disabledColor, backgroundImage: Alloy.Styles.buttons.primary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.primary.backgroundDisabledImage, height: 30, width: Ti.UI.FILL, font: Alloy.Styles.smallButtonFont, top: 10, bottom: 20, titleid: "_Reload_Console", id: "reload_console_btn", accessibilityValue: "reload_console_btn" });

  $.__views.action_section.add($.__views.reload_console_btn);
  $.__views.email_text = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailValueFont, left: 0, top: 10, bottom: 10, textid: "_EmailLogText", id: "email_text", accessibilityValue: "email_text" });

  $.__views.action_section.add($.__views.email_text);
  $.__views.email_address = Ti.UI.createTextArea(
  { font: Alloy.Styles.detailValueFont, left: 20, height: Ti.UI.SIZE, horizontalWrap: true, id: "email_address", accessibilityValue: "email_address" });

  $.__views.action_section.add($.__views.email_address);
  $.__views.email_log_btn = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.buttons.primary.color, disabledColor: Alloy.Styles.buttons.primary.disabledColor, backgroundImage: Alloy.Styles.buttons.primary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.primary.backgroundDisabledImage, height: 30, width: Ti.UI.FILL, font: Alloy.Styles.smallButtonFont, top: 10, bottom: 20, titleid: "_Send_Log_Via_Email", id: "email_log_btn", accessibilityValue: "email_log_btn" });

  $.__views.action_section.add($.__views.email_log_btn);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var args = arguments[0] || {};
  var logger = require('logging')('support:logging', getFullControllerPath($.__controllerPath));
  var getConsoleFile = require('EAUtils').getConsoleFile;
  var emailLogs = require('EAUtils').emailLogs;




  $.apply_runtime_cats_btn.addEventListener('click', onApplyRuntimeClick);
  $.reload_console_btn.addEventListener('click', onReloadClick);
  $.email_log_btn.addEventListener('click', onEmailClick);




  exports.deinit = deinit;









  function deinit() {
    $.apply_runtime_cats_btn.removeEventListener('click', onApplyRuntimeClick);
    $.reload_console_btn.removeEventListener('click', onReloadClick);
    $.email_log_btn.removeEventListener('click', onEmailClick);

    $.stopListening();
    $.destroy();
  }









  function updateEffectiveLoggableCategories() {
    var eff_cats = getLoggableCategories();
    $.effective_cats.setText(eff_cats.loggableCategories.join(', '));
  }






  function readConsoleLog() {
    var tailLength = 100;
    var file = getConsoleFile();

    if (file && file.exists()) {
      blob = file.read();
      if (blob) {
        var readText = blob.text;
        var consoleText = '';
        var arrayOfLines = readText.match(/[^\r\n]+/g);
        if (arrayOfLines) {
          var start = arrayOfLines.length < tailLength ? 0 : arrayOfLines.length - tailLength;
          for (var i = start; i < arrayOfLines.length; i++) {
            consoleText += i + ': ' + arrayOfLines[i] + '\n';
          }
        }
        $.console_tail.setText(consoleText);
      }
    }
  }









  function onReloadClick() {
    readConsoleLog();
  }






  function onEmailClick() {
    notify(_L('Sending console logs to server'));
    var file = getConsoleFile();
    if (file && file.exists()) {
      var blob = file.read();
      if (blob) {
        var promise = emailLogs(blob.text);
        Alloy.Router.showActivityIndicator(promise);
      }
    } else {
      logger.error('emailLogs: Log file not found');
    }
  }






  function onApplyRuntimeClick() {
    var new_value = $.runtime_cats.value;

    var new_cats = new_value.split(',');

    setRuntimeLoggableCategories(new_cats);

    updateEffectiveLoggableCategories();

    readConsoleLog();
  }




  updateEffectiveLoggableCategories();

  $.email_address.setValue(Alloy.CFG.admin_email ? Alloy.CFG.admin_email : '');
  $.email_address.setHeight(Ti.UI.SIZE);


  $.runtime_cats.setValue(Alloy.CFG.loggableCategories.join(',') || Alloy.Globals.runtimeLoggableCategories.join(','));









  _.extend($, exports);
}

module.exports = Controller;