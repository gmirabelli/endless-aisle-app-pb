var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'support/components/buildInfoDataColumn';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.container = Ti.UI.createView(
  { layout: "vertical", width: "100%", height: Ti.UI.SIZE, id: "container" });

  $.__views.container && $.addTopLevelView($.__views.container);
  $.__views.app_uuid_section = Ti.UI.createView(
  { top: 15, left: 20, height: Ti.UI.SIZE, layout: "vertical", id: "app_uuid_section" });

  $.__views.container.add($.__views.app_uuid_section);
  $.__views.app_uuid_title = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.accentColor, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailLabelFont, left: 0, bottom: 10, textid: "_Universally_Unique_Identifier_", id: "app_uuid_title", accessibilityValue: "app_uuid_title" });

  $.__views.app_uuid_section.add($.__views.app_uuid_title);
  $.__views.app_uuid_text = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailValueFont, left: 0, id: "app_uuid_text", accessibilityValue: "app_uuid_text" });

  $.__views.app_uuid_section.add($.__views.app_uuid_text);
  $.__views.version_section = Ti.UI.createView(
  { top: 15, left: 20, height: Ti.UI.SIZE, layout: "vertical", id: "version_section" });

  $.__views.container.add($.__views.version_section);
  $.__views.version_title = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.accentColor, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailLabelFont, left: 0, bottom: 10, textid: "_Versions_", id: "version_title", accessibilityValue: "version_title" });

  $.__views.version_section.add($.__views.version_title);
  $.__views.build_version = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailValueFont, left: 0, textid: "_No_Value", id: "build_version", accessibilityValue: "build_version" });

  $.__views.version_section.add($.__views.build_version);
  $.__views.config_section = Ti.UI.createView(
  { top: 15, left: 20, height: Ti.UI.SIZE, layout: "vertical", id: "config_section" });

  $.__views.container.add($.__views.config_section);
  $.__views.config_title = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.accentColor, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailLabelFont, left: 0, bottom: 10, textid: "_Config_Settings_", id: "config_title", accessibilityValue: "config_title" });

  $.__views.config_section.add($.__views.config_title);
  $.__views.config_text = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailValueFont, left: 0, textid: "_No_Value", id: "config_text", accessibilityValue: "config_text" });

  $.__views.config_section.add($.__views.config_text);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var args = $.args;




  exports.getAppConfiguration = getAppConfiguration;










  function getAppConfiguration() {
    return $.app_uuid_title.getText() + '\n' + swissArmyUtils.getUUID() + '\n\n' + $.version_title.getText() + '\n' + getBuildVersion() + '\n\n' + $.config_title.getText() + '\n' + getConfigSettings();
  }







  function getBuildVersion() {
    var versionText = _L('Client Version: ') + Ti.App.getVersion() + '\n';
    versionText += _L('Server Version: ') + (Alloy.CFG.server_version ? Alloy.CFG.server_version : _L('No Value')) + '\n';
    versionText += !Alloy.CFG.supported_client_versions || Alloy.CFG.supported_client_versions.length == 1 ? _L('Server Supported Version: ') : _L('Server Supported Versions: ');
    versionText += Alloy.CFG.supported_client_versions ? Alloy.CFG.supported_client_versions.join(', ') : _L('No Value');

    return versionText;
  }







  function getConfigSettings() {
    var configText = _L('Host: ') + Alloy.CFG.storefront_host + '\n\n';
    configText += _L('Storefront Settings:') + '\n\n';
    configText += '    ' + _L('Site URL: ') + Alloy.CFG.storefront.site_url + '\n';
    configText += '    ' + _L('Locale URL: ') + Alloy.CFG.storefront.locale_url + '\n';
    configText += '    ' + _L('Timeout: ') + Alloy.CFG.storefront.timeout + '\n';

    configText += '\n' + _L('OCAPI Settings:') + '\n\n';
    configText += '    ' + _L('Site URL: ') + Alloy.CFG.ocapi.site_url + '\n';
    configText += '    ' + _L('Base URL: ') + Alloy.CFG.ocapi.base_url + '\n';
    configText += '    ' + _L('Base Version: ') + Alloy.CFG.ocapi.version + '\n';
    configText += '    ' + _L('Locale: ') + Alloy.CFG.ocapi.default_locale + '\n';
    configText += '    ' + _L('Timeout: ') + Alloy.CFG.ocapi.timeout + '\n';
    configText += '    ' + _L('Client ID: ') + Alloy.CFG.ocapi.client_id + '\n';

    configText += '\n' + _L('Country Settings:') + '\n\n';
    configText += '    ' + _L('Country: ') + require('config/countries').countryConfig[Alloy.CFG.countrySelected].displayName + '\n';
    configText += '    ' + _L('Currency: ') + Alloy.CFG.appCurrency + '\n';
    configText += '    ' + _L('List Price Book: ') + Alloy.CFG.country_configuration[Alloy.CFG.countrySelected].list_price_book + '\n';
    configText += '    ' + _L('Sale Price Book: ') + Alloy.CFG.country_configuration[Alloy.CFG.countrySelected].sale_price_book + '\n';

    return configText;
  }




  $.build_version.setText(getBuildVersion());
  $.config_text.setText(getConfigSettings());
  $.app_uuid_text.setText(swissArmyUtils.getUUID());









  _.extend($, exports);
}

module.exports = Controller;