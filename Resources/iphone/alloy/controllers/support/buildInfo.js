var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
		delete obj[key];
	}
	return arg;
}

function Controller() {

	require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'support/buildInfo';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};







	$.__views.buildInfo = Ti.UI.createView(
	{ title: "Support Dashboard - Info", tabBarHidden: false, navBarHidden: false, id: "buildInfo" });

	$.__views.buildInfo && $.addTopLevelView($.__views.buildInfo);
	$.__views.build_info_main = Ti.UI.createView(
	{ layout: "horizontal", width: "100%", height: "100%", id: "build_info_main" });

	$.__views.buildInfo.add($.__views.build_info_main);
	$.__views.data_column = Ti.UI.createScrollView(
	{ layout: "vertical", width: "70%", height: "100%", id: "data_column" });

	$.__views.build_info_main.add($.__views.data_column);
	$.__views.data_panel = Alloy.createController('support/components/buildInfoDataColumn', { id: "data_panel", __parentSymbol: $.__views.data_column });
	$.__views.data_panel.setParent($.__views.data_column);
	$.__views.vertical_border = Ti.UI.createView(
	{ width: 2, height: Ti.UI.FILL, backgroundColor: Alloy.Styles.color.border.darkest, id: "vertical_border" });

	$.__views.build_info_main.add($.__views.vertical_border);
	$.__views.action_column = Ti.UI.createView(
	{ layout: "vertical", width: "28%", height: "100%", id: "action_column" });

	$.__views.build_info_main.add($.__views.action_column);
	$.__views.action_section = Ti.UI.createView(
	{ top: 15, left: 20, height: Ti.UI.SIZE, layout: "vertical", id: "action_section" });

	$.__views.action_column.add($.__views.action_section);
	$.__views.action_title = Ti.UI.createLabel(
	{ width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.accentColor, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailLabelFont, left: 0, bottom: 10, textid: "_Actions_", id: "action_title", accessibilityValue: "action_title" });

	$.__views.action_section.add($.__views.action_title);
	$.__views.cache_text = Ti.UI.createLabel(
	{ width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailValueFont, left: 0, textid: "_CacheText", id: "cache_text", accessibilityValue: "cache_text" });

	$.__views.action_section.add($.__views.cache_text);
	$.__views.clear_cache = Ti.UI.createButton(
	{ textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.buttons.primary.color, disabledColor: Alloy.Styles.buttons.primary.disabledColor, backgroundImage: Alloy.Styles.buttons.primary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.primary.backgroundDisabledImage, height: 30, width: Ti.UI.FILL, font: Alloy.Styles.smallButtonFont, top: 10, bottom: 20, titleid: "_Clear_Cache", id: "clear_cache", accessibilityValue: "clear_cache" });

	$.__views.action_section.add($.__views.clear_cache);
	clearCache ? $.addListener($.__views.clear_cache, 'click', clearCache) : __defers['$.__views.clear_cache!click!clearCache'] = true;$.__views.password_text = Ti.UI.createLabel(
	{ width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailValueFont, left: 0, textid: "_ChangeStorePasswordText", top: 20, id: "password_text", accessibilityValue: "password_text" });

	$.__views.action_section.add($.__views.password_text);
	$.__views.change_store_password = Ti.UI.createButton(
	{ textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.buttons.primary.color, disabledColor: Alloy.Styles.buttons.primary.disabledColor, backgroundImage: Alloy.Styles.buttons.primary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.primary.backgroundDisabledImage, height: 30, width: Ti.UI.FILL, font: Alloy.Styles.smallButtonFont, top: 10, bottom: 20, titleid: "_Change_Store_Password_Menu", id: "change_store_password", accessibilityValue: "change_store_password" });

	$.__views.action_section.add($.__views.change_store_password);
	changeStorePassword ? $.addListener($.__views.change_store_password, 'click', changeStorePassword) : __defers['$.__views.change_store_password!click!changeStorePassword'] = true;exports.destroy = function () {};




	_.extend($, $.__views);










	var args = arguments[0] || {};




	exports.deinit = deinit;
	exports.hideActionColumn = hideActionColumn;
	exports.showActionColumn = showActionColumn;









	function deinit() {

		$.clear_cache.removeEventListener('click', clearCache);
		$.change_store_password.removeEventListener('click', changeStorePassword);
		$.stopListening();
		$.destroy();
	}









	function hideActionColumn() {
		$.action_column.setVisible(false);
	}






	function showActionColumn() {
		$.action_column.setHeight('100%');
		$.action_column.setWidth('28%');
		$.action_column.setLayout('vertical');
		$.action_column.setVisible(true);
	}









	function clearCache() {
		Alloy.eventDispatcher.trigger('cache:flush');
	}






	function changeStorePassword() {
		Alloy.Router.presentChangeStorePasswordDrawer({
			isManager: true });

	}





	__defers['$.__views.clear_cache!click!clearCache'] && $.addListener($.__views.clear_cache, 'click', clearCache);__defers['$.__views.change_store_password!click!changeStorePassword'] && $.addListener($.__views.change_store_password, 'click', changeStorePassword);



	_.extend($, exports);
}

module.exports = Controller;