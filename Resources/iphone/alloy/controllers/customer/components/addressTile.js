var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
		delete obj[key];
	}
	return arg;
}

function Controller() {

	require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'customer/components/addressTile';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};







	$.__views.addressTile = Ti.UI.createView(
	{ backgroundImage: Alloy.Styles.customerBorderImage, layout: "horizontal", height: 146, width: "100%", left: 0, top: 0, id: "addressTile" });

	$.__views.addressTile && $.addTopLevelView($.__views.addressTile);
	$.__views.default_address_column = Ti.UI.createView(
	{ left: 0, width: 84, height: Ti.UI.SIZE, id: "default_address_column" });

	$.__views.addressTile.add($.__views.default_address_column);
	$.__views.address_column = Ti.UI.createView(
	{ top: 10, layout: "vertical", width: 360, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, id: "address_column" });

	$.__views.addressTile.add($.__views.address_column);
	$.__views.address_name = Ti.UI.createLabel(
	{ width: 360, height: 25, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, ellipsize: true, wordWrap: false, font: Alloy.Styles.calloutCopyFont, id: "address_name", text: $model.__transform.address_name, accessibilityValue: "address_name" });

	$.__views.address_column.add($.__views.address_name);
	$.__views.address_display = Ti.UI.createLabel(
	{ width: 360, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, ellipsize: true, wordWrap: false, font: Alloy.Styles.detailValueFont, id: "address_display", accessibilityValue: "address_display" });

	$.__views.address_column.add($.__views.address_display);
	$.__views.button_column = Ti.UI.createView(
	{ top: 7, id: "button_column" });

	$.__views.addressTile.add($.__views.button_column);
	$.__views.address_edit_button = Ti.UI.createButton(
	{ top: 0, font: Alloy.Styles.buttonFont, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, backgroundImage: Alloy.Styles.secondaryButtonImage, color: Alloy.Styles.buttons.secondary.color, width: 118, height: 35, id: "address_edit_button", titleid: "_Edit", accessibilityValue: "address_edit_button" });

	$.__views.button_column.add($.__views.address_edit_button);
	$.__views.address_delete_button = Ti.UI.createButton(
	{ top: 45, font: Alloy.Styles.buttonFont, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, backgroundImage: Alloy.Styles.secondaryButtonImage, color: Alloy.Styles.buttons.secondary.color, width: 118, height: 35, id: "address_delete_button", titleid: "_Delete", accessibilityValue: "address_delete_button" });

	$.__views.button_column.add($.__views.address_delete_button);
	exports.destroy = function () {};




	_.extend($, $.__views);











	if ($model) {
		var currentCustomer = Alloy.Models.customer;
		var preferredId = currentCustomer.addresses.getPreferredID();
		var addressLabel = $model.getAddressDisplay('customer');
		$.address_display.setText(addressLabel);

		if (preferredId == $model.getAddressId()) {
			var imageView = Ti.UI.createImageView({
				width: 32,
				height: 32 });

			Alloy.Globals.getImageViewImage(imageView, Alloy.Styles.defaultAddressImage);
			$.default_address_column.add(imageView);
		}
		$.address_edit_button.address_id = $.address_delete_button.address_id = $model.getAddressId();
		$.address_delete_button.address_name = $model.getAddressId();
	}









	_.extend($, exports);
}

module.exports = Controller;