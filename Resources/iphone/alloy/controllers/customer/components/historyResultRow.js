var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
		delete obj[key];
	}
	return arg;
}

function Controller() {

	require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'customer/components/historyResultRow';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};







	$.__views.history_result_row = Ti.UI.createTableViewRow(
	{ selectedBackgroundColor: Alloy.Styles.accentColor, id: "history_result_row" });

	$.__views.history_result_row && $.addTopLevelView($.__views.history_result_row);
	$.__views.history_tile_container = Ti.UI.createView(
	{ backgroundImage: Alloy.Styles.customerBorderImage, layout: "horizontal", width: "100%", height: 166, id: "history_tile_container" });

	$.__views.history_result_row.add($.__views.history_tile_container);
	$.__views.__alloyId153 = Ti.UI.createView(
	{ left: 28, right: 28, top: 40, height: 90, width: 90, id: "__alloyId153" });

	$.__views.history_tile_container.add($.__views.__alloyId153);
	$.__views.product_image = Ti.UI.createImageView(
	{ height: "100%", id: "product_image", accessibilityValue: "product_image", image: $model.__transform.imageUrl });

	$.__views.__alloyId153.add($.__views.product_image);
	$.__views.__alloyId154 = Ti.UI.createView(
	{ layout: "vertical", height: "100%", left: 0, width: 430, id: "__alloyId154" });

	$.__views.history_tile_container.add($.__views.__alloyId154);
	$.__views.__alloyId155 = Ti.UI.createView(
	{ height: Ti.UI.SIZE, width: "100%", layout: "horizontal", top: 35, id: "__alloyId155" });

	$.__views.__alloyId154.add($.__views.__alloyId155);
	$.__views.date_label = Ti.UI.createLabel(
	{ width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 15, font: Alloy.Styles.detailValueFont, textid: "_Date_Ordered_", id: "date_label", accessibilityValue: "date_label" });

	$.__views.__alloyId155.add($.__views.date_label);
	$.__views.order_date = Ti.UI.createLabel(
	{ width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 12, font: Alloy.Styles.callcoutCopyFont, id: "order_date", text: $model.__transform.creationDate, accessibilityValue: "order_date" });

	$.__views.__alloyId155.add($.__views.order_date);
	$.__views.__alloyId156 = Ti.UI.createView(
	{ height: Ti.UI.SIZE, width: "100%", layout: "horizontal", id: "__alloyId156" });

	$.__views.__alloyId154.add($.__views.__alloyId156);
	$.__views.status_label = Ti.UI.createLabel(
	{ width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 15, font: Alloy.Styles.detailValueFont, textid: "_Order_Status_", id: "status_label", accessibilityValue: "status_label" });

	$.__views.__alloyId156.add($.__views.status_label);
	$.__views.order_status = Ti.UI.createLabel(
	{ width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 12, font: Alloy.Styles.detailLabelFont, id: "order_status", text: $model.__transform.order_status, accessibilityValue: "order_status" });

	$.__views.__alloyId156.add($.__views.order_status);
	$.__views.__alloyId157 = Ti.UI.createView(
	{ height: Ti.UI.SIZE, width: "100%", layout: "horizontal", id: "__alloyId157" });

	$.__views.__alloyId154.add($.__views.__alloyId157);
	$.__views.total_label = Ti.UI.createLabel(
	{ width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 15, font: Alloy.Styles.detailValueFont, textid: "_Order_Total_", id: "total_label", accessibilityValue: "total_label" });

	$.__views.__alloyId157.add($.__views.total_label);
	$.__views.order_total = Ti.UI.createLabel(
	{ width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 12, font: Alloy.Styles.detailLabelFont, id: "order_total", text: $model.__transform.totalNetPrice, accessibilityValue: "order_total" });

	$.__views.__alloyId157.add($.__views.order_total);
	$.__views.__alloyId158 = Ti.UI.createView(
	{ height: Ti.UI.SIZE, width: "100%", layout: "horizontal", id: "__alloyId158" });

	$.__views.__alloyId154.add($.__views.__alloyId158);
	$.__views.number_label = Ti.UI.createLabel(
	{ width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 15, font: Alloy.Styles.detailValueFont, textid: "_Order_Number_", id: "number_label", accessibilityValue: "number_label" });

	$.__views.__alloyId158.add($.__views.number_label);
	$.__views.order_number = Ti.UI.createLabel(
	{ width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 12, font: Alloy.Styles.detailLabelFont, id: "order_number", text: $model.__transform.orderNo, accessibilityValue: "order_number" });

	$.__views.__alloyId158.add($.__views.order_number);
	$.__views.__alloyId159 = Ti.UI.createView(
	{ width: 40, id: "__alloyId159" });

	$.__views.history_tile_container.add($.__views.__alloyId159);
	$.__views.order_detail_button = Ti.UI.createButton(
	{ style: "PLAIN", color: Alloy.Styles.color.text.light, font: Alloy.Styles.bigButtonFont, titleid: ">", id: "order_detail_button", accessibilityValue: "order_detail_button" });

	$.__views.__alloyId159.add($.__views.order_detail_button);
	exports.destroy = function () {};




	_.extend($, $.__views);












	_.extend($, exports);
}

module.exports = Controller;