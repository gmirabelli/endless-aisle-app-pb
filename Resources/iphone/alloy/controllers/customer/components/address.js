var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'customer/components/address';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};




  $.currentAddress = Alloy.createModel('customerAddress');


  $.__views.customer_address = Ti.UI.createView(
  { layout: "vertical", height: Ti.UI.SIZE, width: "95%", top: 0, left: 15, id: "customer_address" });

  $.__views.customer_address && $.addTopLevelView($.__views.customer_address);
  $.__views.address_title = Alloy.createController('components/block_header', { customLabel: L('_Modify_Address'), id: "address_title", __parentSymbol: $.__views.customer_address });
  $.__views.address_title.setParent($.__views.customer_address);
  $.__views.address_error = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: 0, color: Alloy.Styles.color.text.red, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.textFieldFont, visible: false, id: "address_error", accessibilityValue: "address_error" });

  $.__views.customer_address.add($.__views.address_error);
  $.__views.address_container = Ti.UI.createScrollView(
  { layout: "vertical", top: 0, left: 5, height: Ti.UI.SIZE, disableBounce: "true", contentWidth: Ti.UI.SIZE, id: "address_container" });

  $.__views.customer_address.add($.__views.address_container);
  $.__views.inner_container = Ti.UI.createView(
  { width: 580, height: 580, layout: "vertical", id: "inner_container" });

  $.__views.address_container.add($.__views.inner_container);
  $.__views.__alloyId133 = Ti.UI.createView(
  { layout: "horizontal", width: "100%", left: 20, top: 20, height: 55, id: "__alloyId133" });

  $.__views.inner_container.add($.__views.__alloyId133);
  $.__views.address_name = Ti.UI.createTextField(
  { backgroundColor: Alloy.Styles.color.background.medium, height: 50, padding: { left: 18 }, left: 0, width: 532, color: Alloy.Styles.color.text.darkest, font: Alloy.Styles.textFieldFont, maxLength: 20, hintText: L('_Nickname'), id: "address_name", accessibilityLabel: "address_name" });

  $.__views.__alloyId133.add($.__views.address_name);
  $.__views.enter_address_container = Ti.UI.createView(
  { top: 10, left: 20, height: Ti.UI.SIZE, id: "enter_address_container" });

  $.__views.inner_container.add($.__views.enter_address_container);
  $.__views.enter_address = Alloy.createController('components/enterAddressDynamic', { id: "enter_address", customId: "customer", __parentSymbol: $.__views.enter_address_container });
  $.__views.enter_address.setParent($.__views.enter_address_container);
  $.__views.make_deferred_container = Ti.UI.createView(
  { layout: "horizontal", width: 275, top: 15, left: 20, height: Ti.UI.SIZE, id: "make_deferred_container" });

  $.__views.inner_container.add($.__views.make_deferred_container);
  $.__views.preferred_switch = Ti.UI.createSwitch(
  { width: 85, value: false, id: "preferred_switch", accessibilityLabel: "preferred_switch" });

  $.__views.make_deferred_container.add($.__views.preferred_switch);
  $.__views.preferred_label = Ti.UI.createLabel(
  { width: 170, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.switchLabelFont, left: 0, textid: "_Make_Preferred_Address", id: "preferred_label", accessibilityValue: "preferred_label" });

  $.__views.make_deferred_container.add($.__views.preferred_label);
  $.__views.__alloyId134 = Ti.UI.createView(
  { layout: "horizontal", width: "100%", left: 20, top: 20, height: 55, id: "__alloyId134" });

  $.__views.inner_container.add($.__views.__alloyId134);
  $.__views.first_name_container = Ti.UI.createView(
  { layout: "vertical", width: "50%", id: "first_name_container" });

  $.__views.__alloyId134.add($.__views.first_name_container);
  $.__views.cancel_button = Ti.UI.createButton(
  { left: 0, width: 240, bottom: 33, top: 15, height: 42, backgroundImage: Alloy.Styles.secondaryButtonImage, font: Alloy.Styles.buttonFont, color: Alloy.Styles.buttons.secondary.color, id: "cancel_button", titleid: "_Cancel", accessibilityValue: "cancel_address_button" });

  $.__views.first_name_container.add($.__views.cancel_button);
  $.__views.last_name_container = Ti.UI.createView(
  { layout: "vertical", width: "50%", id: "last_name_container" });

  $.__views.__alloyId134.add($.__views.last_name_container);
  $.__views.save_button = Ti.UI.createButton(
  { right: 52, width: 240, bottom: 33, top: 15, height: 42, backgroundImage: Alloy.Styles.primaryButtonImage, font: Alloy.Styles.buttonFont, color: Alloy.Styles.buttons.primary.color, id: "save_button", titleid: "_Save", accessibilityValue: "save_address_button" });

  $.__views.last_name_container.add($.__views.save_button);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var currentCustomer = Alloy.Models.customer;
  var customerAddress = Alloy.Models.customerAddress;
  var EAUtils = require('EAUtils');
  var addressVerification = EAUtils.addressVerification;
  var getAddressNickname = EAUtils.getAddressNickname;
  var logger = require('logging')('customer:components:address', getFullControllerPath($.__controllerPath));

  var currentAddress;
  var originalAddressName = '';
  var modify = false;
  var address_id;
  var route;





  $.save_button.addEventListener('click', onSaveClick);


  $.cancel_button.addEventListener('click', onCancelClick);




  exports.init = init;
  exports.deinit = deinit;
  exports.isModifyingAddress = isModifyingAddress;











  function init(options) {
    logger.info('Calling INIT');
    var deferred = new _.Deferred();

    address_id = options.address_id;
    var customerAddresses = [];
    clearErrors();


    if (address_id) {
      $.address_title.setCustomLabel(_L('Modify Address'));
      customerAddress.setModifyingCurrentAddress(true);
    } else {
      $.address_title.setCustomLabel(_L('Add Address'));
      originalAddressName = '';
      customerAddress.setModifyingCurrentAddress(false);
    }

    $.address_name.setValue('');


    Alloy.Models.customer.trigger('fetch', {});
    currentCustomer.addresses.fetchAddresses(currentCustomer.getCustomerId()).done(function () {
      var customerAddresses = currentCustomer.addresses.getAddresses();

      currentAddress = null;
      for (var i = 0; i < customerAddresses.length; i++) {
        var addr = customerAddresses[i];
        if (addr && address_id == addr.getAddressId()) {
          originalAddressName = address_id ? addr.getAddressId() : '';
          currentAddress = addr;
          break;
        }
      }

      $.enter_address.init('profile_');
      if (currentAddress) {

        $.enter_address.setAddress(currentAddress);
        $.address_name.setValue(currentAddress.getAddressId());


        var preferredId = currentCustomer.addresses.getPreferredID();
        $.preferred_switch.setValue(preferredId == currentAddress.getAddressId());
        deferred.resolve();
      } else {
        var address = {
          first_name: currentCustomer.getFirstName(),
          last_name: currentCustomer.getLastName() };

        $.enter_address.setAddress(address);
        deferred.resolve();
      }
    }).fail(function () {
      logger.info('cannot retrieve addresses');
      deferred.reject();
    });
    return deferred.promise();
  }






  function deinit() {
    logger.info('DEINIT called');
    $.toolbar.deinit();
    $.enter_address.deinit();
    $.save_button.removeEventListener('click', onSaveClick);
    $.cancel_button.removeEventListener('click', onCancelClick);
    $.stopListening();
    $.destroy();
  }









  function isModifyingAddress() {
    return modify;
  }






  function clearErrors() {

    $.address_error.setVisible(false);
    $.address_error.setHeight(0);
    $.address_error.setText('');
  }






  function updateAddress(newAddress) {
    $.enter_address.setAddress(newAddress);
    setCustomerAddress(false);
  }






  function continueSave() {
    notify(_L('Address successfully saved.'));
    $.customer_address.fireEvent('route', {
      page: 'addresses' });

  }






  function showErrorMessage(errorMessage) {
    $.address_error.setText(errorMessage);
    $.address_error.setVisible(true);
    $.address_error.setHeight(Ti.UI.SIZE);
  }






  function setCustomerAddress(verify) {
    var deferred = new _.Deferred();
    Alloy.Router.showActivityIndicator(deferred);
    var address = $.enter_address.getAddress();


    var nickname_value = $.address_name.getValue().trim();
    var nickname = nickname_value != '' ? nickname_value : getAddressNickname(address.city, address.state_code);
    address.address_id = nickname;
    address.original_id = nickname != originalAddressName ? originalAddressName : nickname;


    var preferredValue = $.preferred_switch.getValue();
    if (preferredValue == 'off') {
      preferredValue = false;
    }
    address.preferred = preferredValue;

    var funcToApply;

    if (customerAddress.isModifyingCustomerAddress()) {
      funcToApply = currentCustomer.addresses.updateAddress;
    } else {

      if (currentCustomer.addresses.nicknameExists(nickname)) {
        showErrorMessage(_L('Error: ') + String.format(_L('Customer address name \'%s\' is already in use.'), nickname));
        deferred.resolve();
        return;
      }
      funcToApply = currentCustomer.addresses.createAddress;
    }

    verify = verify != null ? verify : true;

    funcToApply.apply(currentCustomer.addresses, [address, currentCustomer.getCustomerId(), {
      c_employee_id: Alloy.Models.associate.getEmployeeId() },
    modify, verify]).done(function (results) {
      continueSave();
    }).fail(function (model) {
      var fault = model.get('fault');
      var faultHandled = false;
      if (verify) {
        faultHandled = addressVerification(fault, address, updateAddress);
      }
      if (faultHandled === false) {
        var error = null;
        if (fault && fault.arguments && fault.arguments.statusMessage) {
          error = fault.arguments.statusMessage;
        } else if (fault && fault.message) {
          error = fault.message;
        } else {
          error = _L('Cannot save address.');
        }
        showErrorMessage(_L('Error: ') + error);
      }
    }).always(function () {
      deferred.resolve();
    });
  }








  function onSaveClick() {
    logger.info('save_button click event listener');
    Alloy.eventDispatcher.trigger('hideAuxillaryViews');
    $.save_button.animate(Alloy.Animations.bounce);


    clearErrors();
    $.enter_address.validate();


    if ($.enter_address.isValid()) {
      setCustomerAddress();
    }
  }





  function onCancelClick() {
    logger.info('cancel_button click event listener');
    Alloy.eventDispatcher.trigger('hideAuxillaryViews');
    $.cancel_button.animate(Alloy.Animations.bounce);
    clearErrors();
    customerAddress.setModifyingCurrentAddress(false);
    $.customer_address.fireEvent('route', {
      page: 'addresses',
      isCancel: true });

  }





  $.toolbar = Alloy.createController('components/nextPreviousToolbar');
  var textFields = $.enter_address.getAllFormFieldsInOrder().slice();
  textFields.unshift($.address_name);
  $.toolbar.setTextFields(textFields);









  _.extend($, exports);
}

module.exports = Controller;