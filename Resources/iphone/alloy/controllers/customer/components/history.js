var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'customer/components/history';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};




  Alloy.Collections.instance('customerOrderHistory');$.orders = Alloy.createCollection('customerOrderHistory');


  $.__views.history_contents = Ti.UI.createView(
  { layout: "vertical", height: 650, width: "100%", top: 0, id: "history_contents" });

  $.__views.history_contents && $.addTopLevelView($.__views.history_contents);
  $.__views.empty_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: 0, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.tabFont, text: "", top: 0, id: "empty_label", accessibilityValue: "empty_label" });

  $.__views.history_contents.add($.__views.empty_label);
  $.__views.history_container = Ti.UI.createTableView(
  { layout: "vertical", id: "history_container", separatorStyle: "transparent" });

  $.__views.history_contents.add($.__views.history_container);
  var __alloyId151 = Alloy.Collections['$.orders'] || $.orders;function __alloyId152(e) {if (e && e.fromAdapter) {return;}var opts = __alloyId152.opts || {};var models = filterOrders(__alloyId151);var len = models.length;var rows = [];for (var i = 0; i < len; i++) {var __alloyId148 = models[i];__alloyId148.__transform = transformOrder(__alloyId148);var __alloyId150 = Alloy.createController('customer/components/historyResultRow', { $model: __alloyId148, __parentSymbol: __parentSymbol });
      rows.push(__alloyId150.getViewEx({ recurse: true }));
    }$.__views.history_container.setData(rows);};__alloyId151.on('fetch destroy change add remove reset', __alloyId152);exports.destroy = function () {__alloyId151 && __alloyId151.off('fetch destroy change add remove reset', __alloyId152);};




  _.extend($, $.__views);










  var currentHistory = Alloy.Collections.customerOrderHistory;
  var toCurrency = require('EAUtils').toCurrency;
  var currentCustomer = Alloy.Models.customer;
  var logger = require('logging')('customer:components:history', getFullControllerPath($.__controllerPath));
  var filteredOrders = [];




  $.history_container.addEventListener('singletap', handleOrderSelected);




  $.listenTo(currentCustomer, 'customer:clear', onCustomerClear);




  exports.init = init;
  exports.render = render;
  exports.deinit = deinit;










  function init() {
    logger.info('Calling INIT');
    var deferred = new _.Deferred();
    $.empty_label.setText(_L('There are no orders.'));
    $.empty_label.setTop(0);
    $.empty_label.setHeight(0);


    currentHistory.search({
      customerId: currentCustomer.getCustomerNumber() }).
    done(function () {
      $.orders.once('reset', function () {
        disableSelectionOnRows();
      });

      filteredOrders = filterOrders(currentHistory);
      $.orders.reset(filteredOrders);
      if (filteredOrders && filteredOrders.length == 0) {
        $.empty_label.setTop(40);
        $.empty_label.setHeight(40);
      } else {
        $.empty_label.setTop(0);
        $.empty_label.setHeight(0);
      }
      deferred.resolve();
    }).fail(function () {
      logger.info('cannot retrieve history');
      deferred.reject();
    });
    return deferred.promise();
  }






  function render() {
    logger.info('Calling RENDER');
  }






  function deinit() {
    logger.info('Calling deinit');
    $.history_container.removeEventListener('singletap', handleOrderSelected);
    $.stopListening();
    $.destroy();
  }









  function disableSelectionOnRows() {
    logger.info('disableSelectionOnRows');
    if ($.history_container.getSections().length >= 1) {
      var children = $.history_container.getSections()[0].getRows();

      _.each(children, function (child) {
        child.setSelectionStyle(Ti.UI.iOS.TableViewCellSelectionStyle.NONE);
      });
    }
  }






  function filterOrders(collection) {
    logger.info('filterOrders' + JSON.stringify(collection.models[0]));
    var filter = collection.filter(function (model) {
      return model.isStatusCompleted();
    });
    return filter;
  }







  function transformOrder(model) {
    logger.info('transform Order');
    var imageUrl = model.getImageURL().replace('https', 'http');
    return {
      imageUrl: encodeURI(imageUrl),
      creationDate: model.getCreationDate(),
      order_status: model.getStatus(),
      totalNetPrice: toCurrency(model.getTotalNetPrice(), model.getCurrencyCode()),
      orderNo: model.getOrderNo() };

  }









  function handleOrderSelected(event) {
    if (filteredOrders && filteredOrders.length > 0) {
      var order = filteredOrders[event.index];
      if (order) {
        var promise = Alloy.Models.customerOrder.getOrder({
          order_no: order.get('orderNo') });

        Alloy.Router.showActivityIndicator(promise);
        promise.done(function () {
          $.history_contents.fireEvent('route', {
            page: 'order' });

        }).fail(function () {
          notify(String.format(_L('Could not retrieve order \'%s\'.'), order.get('orderNo')), {
            preventAutoClose: true });

        });
      }
    }
  }








  function onCustomerClear() {
    logger.info('currentCustomer - customer:clear event listener');
    $.orders.reset();
  }









  _.extend($, exports);
}

module.exports = Controller;