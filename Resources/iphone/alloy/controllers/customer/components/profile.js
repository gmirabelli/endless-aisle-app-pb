var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
		delete obj[key];
	}
	return arg;
}

function Controller() {

	require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'customer/components/profile';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};




	Alloy.Models.instance('customer');


	$.__views.customer_profile = Ti.UI.createView(
	{ top: 25, layout: "horizontal", height: Ti.UI.SIZE, width: "95%", left: 20, id: "customer_profile" });

	$.__views.customer_profile && $.addTopLevelView($.__views.customer_profile);
	$.__views.profile_details = Ti.UI.createView(
	{ layout: "vertical", top: 15, id: "profile_details" });

	$.__views.customer_profile.add($.__views.profile_details);
	$.__views.first_name_container = Ti.UI.createView(
	{ width: 500, height: 50, left: 10, top: 10, id: "first_name_container" });

	$.__views.profile_details.add($.__views.first_name_container);
	$.__views.first_name_label = Ti.UI.createLabel(
	{ width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.calloutCopyFont, left: 0, textid: "_First_Name__", id: "first_name_label", accessibilityValue: "first_name_label" });

	$.__views.first_name_container.add($.__views.first_name_label);
	$.__views.first_name_text = Ti.UI.createLabel(
	{ width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 140, id: "first_name_text", accessibilityValue: "first_name_text" });

	$.__views.first_name_container.add($.__views.first_name_text);
	$.__views.last_name_container = Ti.UI.createView(
	{ width: 500, height: 50, left: 10, top: 10, id: "last_name_container" });

	$.__views.profile_details.add($.__views.last_name_container);
	$.__views.last_name_label = Ti.UI.createLabel(
	{ width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.calloutCopyFont, left: 0, textid: "_Last_Name__", id: "last_name_label", accessibilityValue: "last_name_label" });

	$.__views.last_name_container.add($.__views.last_name_label);
	$.__views.last_name_text = Ti.UI.createLabel(
	{ width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 140, id: "last_name_text", accessibilityValue: "last_name_text" });

	$.__views.last_name_container.add($.__views.last_name_text);
	$.__views.edit_button = Ti.UI.createButton(
	{ top: 80, left: 400, width: 118, height: 35, backgroundImage: Alloy.Styles.secondaryButtonImage, font: Alloy.Styles.buttonFont, color: Alloy.Styles.buttons.secondary.color, id: "edit_button", titleid: "_Edit", accessibilityValue: "edit_profile_button" });

	$.__views.profile_details.add($.__views.edit_button);
	var __alloyId171 = function () {Alloy['Models']['customer'].__transform = _.isFunction(Alloy['Models']['customer'].transform) ? Alloy['Models']['customer'].transform() : Alloy['Models']['customer'].toJSON();$.first_name_text.text = Alloy['Models']['customer']['__transform']['first_name'];$.last_name_text.text = Alloy['Models']['customer']['__transform']['last_name'];};Alloy['Models']['customer'].on('fetch change destroy', __alloyId171);exports.destroy = function () {Alloy['Models']['customer'] && Alloy['Models']['customer'].off('fetch change destroy', __alloyId171);};




	_.extend($, $.__views);










	var currentCustomer = Alloy.Models.customer;
	var logger = require('logging')('customer:components:profile', getFullControllerPath($.__controllerPath));




	$.edit_button.addEventListener('click', onEditClick);




	exports.init = init;
	exports.deinit = deinit;









	function init() {
		logger.info('INIT called');
		Alloy.Models.customer.trigger('fetch', {});
	}






	function deinit() {
		logger.info('DEINIT called');
		$.edit_button.removeEventListener('click', onEditClick);
		$.destroy();
	}









	function onEditClick(event) {
		logger.info('edit_button click event listener');
		event.cancelBubble = true;
		$.customer_profile.fireEvent('route', {
			page: 'editProfile' });

	}









	_.extend($, exports);
}

module.exports = Controller;