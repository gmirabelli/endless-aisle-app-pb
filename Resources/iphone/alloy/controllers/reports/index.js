var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'reports/index';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};




  $.quickStatsCollection = Alloy.createCollection('quickStats');


  $.__views.sales_report_dashboard_container = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.color.background.transparent, id: "sales_report_dashboard_container" });

  $.__views.sales_report_dashboard_container && $.addTopLevelView($.__views.sales_report_dashboard_container);
  $.__views.backdrop = Ti.UI.createView(
  { width: Ti.UI.FILL, height: Ti.UI.FILL, opacity: 0.25, backgroundColor: Alloy.Styles.color.background.black, id: "backdrop" });

  $.__views.sales_report_dashboard_container.add($.__views.backdrop);
  $.__views.sales_dashboard = Ti.UI.createView(
  { top: 20, width: Ti.UI.FILL, height: Ti.UI.FILL, backgroundColor: Alloy.Styles.color.background.lightGray, id: "sales_dashboard" });

  $.__views.sales_report_dashboard_container.add($.__views.sales_dashboard);
  $.__views.title_bar = Ti.UI.createView(
  { top: 0, height: 40, backgroundColor: Alloy.Styles.accentColor, visible: true, id: "title_bar" });

  $.__views.sales_dashboard.add($.__views.title_bar);
  $.__views.dashboard_title = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.white, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, backgroundColor: Alloy.Styles.accentColor, font: Alloy.Styles.detailLabelFont, id: "dashboard_title", accessibilityValue: "dashboard_title" });

  $.__views.title_bar.add($.__views.dashboard_title);
  if (Alloy.Models.associate.hasStoreLevelSalesReportsPrivileges()) {
    $.__views.hamburger_menu = Ti.UI.createButton(
    { left: 5, width: "4%", height: "95%", backgroundImage: Alloy.Styles.viewListIconWhite, backgroundSelectedImage: Alloy.Styles.viewListIconBlack, backgroundColor: Alloy.Styles.accentColor, id: "hamburger_menu", accessibilityLabel: "sales_hamburger_menu" });

    $.__views.title_bar.add($.__views.hamburger_menu);
    revealAssociateList ? $.addListener($.__views.hamburger_menu, 'click', revealAssociateList) : __defers['$.__views.hamburger_menu!click!revealAssociateList'] = true;}
  $.__views.close_icon = Ti.UI.createButton(
  { backgroundImage: Alloy.Styles.clearTextImage, right: 5, id: "close_icon", accessibilityLabel: "close_icon" });

  $.__views.title_bar.add($.__views.close_icon);
  if (Alloy.Models.associate.hasStoreLevelSalesReportsPrivileges()) {
    $.__views.side_panel = Ti.UI.createView(
    { top: 40, left: 0, width: "30%", height: Ti.UI.FILL, backgroundColor: Alloy.Styles.color.background.lightGray, id: "side_panel" });

    $.__views.sales_dashboard.add($.__views.side_panel);
    if (true) {
      $.__views.assoc_table_search_bar = Ti.UI.createSearchBar(
      { accessibilityLabel: "assoc_table_search_bar", id: "assoc_table_search_bar" });

    }
    $.__views.assoc_table = Ti.UI.createTableView(
    { top: 0, separatorColor: Alloy.Styles.accentColor, backgroundColor: Alloy.Styles.color.background.lightGray, search: $.__views.assoc_table_search_bar, id: "assoc_table" });

    $.__views.side_panel.add($.__views.assoc_table);
    handleAssociateSelect ? $.addListener($.__views.assoc_table, 'click', handleAssociateSelect) : __defers['$.__views.assoc_table!click!handleAssociateSelect'] = true;}
  $.__views.chart_area = Ti.UI.createView(
  { top: 40, right: 0, width: "100%", height: Ti.UI.FILL, backgroundColor: Alloy.Styles.color.background.lightGray, id: "chart_area" });

  $.__views.sales_dashboard.add($.__views.chart_area);
  $.__views.chart_control_panel = Ti.UI.createView(
  { top: "3%", left: "2%", height: "14%", width: "70%", backgroundColor: Alloy.Styles.color.background.white, viewShadowColor: Alloy.Styles.shadowGrayColor, viewShadowOffset: { x: 0, y: 0 }, viewShadowRadius: 2, id: "chart_control_panel" });

  $.__views.chart_area.add($.__views.chart_control_panel);
  $.__views.date_filter_control = Ti.UI.createView(
  { viewShadowColor: Alloy.Styles.shadowGrayColor, viewShadowOffset: { x: 0, y: 0 }, viewShadowRadius: 2, top: 0, layout: "horizontal", height: "55%", id: "date_filter_control" });

  $.__views.chart_control_panel.add($.__views.date_filter_control);
  $.__views.today = Ti.UI.createButton(
  { height: Ti.UI.FILL, width: "20%", bubbleParent: true, font: Alloy.Styles.bigButtonFont, borderColor: Alloy.Styles.accentColor, color: Alloy.Styles.accentColor, backgroundColor: Alloy.Styles.color.background.white, titleid: "_Today", id: "today", accessibilityValue: "filter_today" });

  $.__views.date_filter_control.add($.__views.today);
  $.__views.week = Ti.UI.createButton(
  { height: Ti.UI.FILL, width: "20%", bubbleParent: true, font: Alloy.Styles.bigButtonFont, borderColor: Alloy.Styles.accentColor, color: Alloy.Styles.color.text.white, backgroundColor: Alloy.Styles.accentColor, titleid: "_Week", id: "week", accessibilityValue: "filter_week" });

  $.__views.date_filter_control.add($.__views.week);
  $.__views.month = Ti.UI.createButton(
  { height: Ti.UI.FILL, width: "20%", bubbleParent: true, font: Alloy.Styles.bigButtonFont, borderColor: Alloy.Styles.accentColor, color: Alloy.Styles.accentColor, backgroundColor: Alloy.Styles.color.background.white, titleid: "_Month", id: "month", accessibilityValue: "filter_month" });

  $.__views.date_filter_control.add($.__views.month);
  $.__views.quarter = Ti.UI.createButton(
  { height: Ti.UI.FILL, width: "20%", bubbleParent: true, font: Alloy.Styles.bigButtonFont, borderColor: Alloy.Styles.accentColor, color: Alloy.Styles.accentColor, backgroundColor: Alloy.Styles.color.background.white, titleid: "_Quarter", id: "quarter", accessibilityValue: "filter_quarter" });

  $.__views.date_filter_control.add($.__views.quarter);
  $.__views.custom_date = Ti.UI.createButton(
  { height: Ti.UI.FILL, width: "20%", bubbleParent: true, font: Alloy.Styles.bigButtonFont, borderColor: Alloy.Styles.accentColor, color: Alloy.Styles.accentColor, backgroundColor: Alloy.Styles.color.background.white, titleid: "_Custom", id: "custom_date", accessibilityValue: "filter_custom_date" });

  $.__views.date_filter_control.add($.__views.custom_date);
  $.__views.date_filter_info_view = Ti.UI.createView(
  { top: 52, width: Ti.UI.FILL, height: "45%", id: "date_filter_info_view" });

  $.__views.chart_control_panel.add($.__views.date_filter_info_view);
  $.__views.date_range_info = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.FILL, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.detailLabelFont, id: "date_range_info", accessibilityValue: "date_range_info" });

  $.__views.date_filter_info_view.add($.__views.date_range_info);
  $.__views.chart_container = Ti.UI.createView(
  { top: "20%", left: "2%", layout: "vertical", height: "78%", width: "70%", backgroundColor: Alloy.Styles.color.background.white, viewShadowColor: Alloy.Styles.shadowGrayColor, viewShadowOffset: { x: 0, y: 0 }, viewShadowRadius: 2, id: "chart_container" });

  $.__views.chart_area.add($.__views.chart_container);
  $.__views.chart_panel = Ti.UI.createView(
  { height: Ti.UI.FILL, width: Ti.UI.FILL, layout: "vertical", id: "chart_panel" });

  $.__views.chart_container.add($.__views.chart_panel);
  $.__views.scrollable_view_control = Ti.UI.createView(
  { viewShadowColor: Alloy.Styles.shadowGrayColor, viewShadowOffset: { x: 0, y: 0 }, viewShadowRadius: 2, layout: "horizontal", backgroundColor: Alloy.Styles.accentColor, top: 0, height: "8%", id: "scrollable_view_control" });

  $.__views.chart_panel.add($.__views.scrollable_view_control);
  $.__views.sales_web_view_control = Ti.UI.createButton(
  { left: 0, width: "33.33%", borderColor: Alloy.Styles.accentColor, color: Alloy.Styles.color.text.white, backgroundColor: Alloy.Styles.accentColor, font: Alloy.Styles.buttonFont, bubbleParent: true, height: Ti.UI.FILL, titleid: "_Sales", id: "sales_web_view_control", accessibilityValue: "sales_web_view_control" });

  $.__views.scrollable_view_control.add($.__views.sales_web_view_control);
  if (Alloy.CFG.sales_reports.enabled_charts.items_sold) {
    $.__views.items_sold_view_control = Ti.UI.createButton(
    { left: 0, width: "33.33%", borderColor: Alloy.Styles.accentColor, color: Alloy.Styles.accentColor, backgroundColor: Alloy.Styles.color.background.white, font: Alloy.Styles.buttonFont, bubbleParent: true, height: Ti.UI.FILL, titleid: "_Items_Sold", id: "items_sold_view_control", accessibilityValue: "items_sold_view_control" });

    $.__views.scrollable_view_control.add($.__views.items_sold_view_control);
  }
  if (Alloy.CFG.sales_reports.enabled_charts.ranks) {
    $.__views.ranks_web_view_control = Ti.UI.createButton(
    { left: 0, width: "33.33%", borderColor: Alloy.Styles.accentColor, color: Alloy.Styles.accentColor, backgroundColor: Alloy.Styles.color.background.white, font: Alloy.Styles.buttonFont, bubbleParent: true, height: Ti.UI.FILL, titleid: "_Ranks", id: "ranks_web_view_control", accessibilityValue: "ranks_web_view_control" });

    $.__views.scrollable_view_control.add($.__views.ranks_web_view_control);
  }
  var __alloyId223 = [];
  $.__views.sales_view = Ti.UI.createView(
  { id: "sales_view" });

  __alloyId223.push($.__views.sales_view);
  $.__views.sales_web_view = Ti.UI.createWebView(
  { top: 0, willHandleTouches: false, hideLoadIndicator: true, id: "sales_web_view", urlPageName: Alloy.CFG.sales_reports.url_page_names.sales, accessibilityLabel: "sales_web_view" });

  $.__views.sales_view.add($.__views.sales_web_view);
  onMainChartLoad ? $.addListener($.__views.sales_web_view, 'load', onMainChartLoad) : __defers['$.__views.sales_web_view!load!onMainChartLoad'] = true;showWebViewLoadError ? $.addListener($.__views.sales_web_view, 'error', showWebViewLoadError) : __defers['$.__views.sales_web_view!error!showWebViewLoadError'] = true;showActivityIndicatorForPrimaryAndSecondaryWebView ? $.addListener($.__views.sales_web_view, 'beforeload', showActivityIndicatorForPrimaryAndSecondaryWebView) : __defers['$.__views.sales_web_view!beforeload!showActivityIndicatorForPrimaryAndSecondaryWebView'] = true;$.__views.sales_web_view_activity_indicator_wrapper = Ti.UI.createView(
  { width: 0, height: 0, backgroundColor: Alloy.Styles.color.background.transparentBlack30Percent, opacity: 0, id: "sales_web_view_activity_indicator_wrapper" });

  $.__views.sales_view.add($.__views.sales_web_view_activity_indicator_wrapper);
  $.__views.sales_web_view_error_msg = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.errorMessageFont, opacity: 0, backgroundColor: Alloy.Styles.color.background.white, id: "sales_web_view_error_msg", accessibilityValue: "sales_web_view_error_msg" });

  $.__views.sales_web_view_activity_indicator_wrapper.add($.__views.sales_web_view_error_msg);
  $.__views.sales_web_view_activity_indicator = Ti.UI.createActivityIndicator(
  { style: Ti.UI.ActivityIndicatorStyle.BIG, id: "sales_web_view_activity_indicator", accessibilityLabel: "sales_web_view_activity_indicator" });

  $.__views.sales_web_view_activity_indicator_wrapper.add($.__views.sales_web_view_activity_indicator);
  $.__views.reload_sales_chart_button = Ti.UI.createButton(
  { viewShadowColor: Alloy.Styles.accentColor, viewShadowOffset: { x: 0, y: 0 }, viewShadowRadius: 2, right: 10, bottom: 10, width: 40, height: 40, borderRadius: 20, backgroundImage: Alloy.Styles.refreshImageWhite, backgroundSelectedImage: Alloy.Styles.refreshImageBlack, borderWidth: 3, borderColor: Alloy.Styles.accentColor, id: "reload_sales_chart_button", accessibilityValue: "reload_sales_chart_button" });

  $.__views.sales_view.add($.__views.reload_sales_chart_button);
  reloadSalesChart ? $.addListener($.__views.reload_sales_chart_button, 'click', reloadSalesChart) : __defers['$.__views.reload_sales_chart_button!click!reloadSalesChart'] = true;if (Alloy.CFG.sales_reports.enabled_charts.items_sold) {
    $.__views.items_sold_view = Ti.UI.createView(
    { id: "items_sold_view" });

    __alloyId223.push($.__views.items_sold_view);
    $.__views.items_sold_web_view = Ti.UI.createWebView(
    { top: 0, willHandleTouches: false, hideLoadIndicator: true, id: "items_sold_web_view", urlPageName: Alloy.CFG.sales_reports.url_page_names.items_sold, accessibilityLabel: "items_sold_web_view" });

    $.__views.items_sold_view.add($.__views.items_sold_web_view);
    hideActivityIndicator ? $.addListener($.__views.items_sold_web_view, 'load', hideActivityIndicator) : __defers['$.__views.items_sold_web_view!load!hideActivityIndicator'] = true;showWebViewLoadError ? $.addListener($.__views.items_sold_web_view, 'error', showWebViewLoadError) : __defers['$.__views.items_sold_web_view!error!showWebViewLoadError'] = true;showActivityIndicator ? $.addListener($.__views.items_sold_web_view, 'beforeload', showActivityIndicator) : __defers['$.__views.items_sold_web_view!beforeload!showActivityIndicator'] = true;$.__views.items_sold_web_view_activity_indicator_wrapper = Ti.UI.createView(
    { width: 0, height: 0, backgroundColor: Alloy.Styles.color.background.transparentBlack30Percent, opacity: 0, id: "items_sold_web_view_activity_indicator_wrapper" });

    $.__views.items_sold_view.add($.__views.items_sold_web_view_activity_indicator_wrapper);
    $.__views.items_sold_web_view_error_msg = Ti.UI.createLabel(
    { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.errorMessageFont, opacity: 0, backgroundColor: Alloy.Styles.color.background.white, id: "items_sold_web_view_error_msg", accessibilityValue: "items_sold_web_view_error_msg" });

    $.__views.items_sold_web_view_activity_indicator_wrapper.add($.__views.items_sold_web_view_error_msg);
    $.__views.items_sold_web_view_activity_indicator = Ti.UI.createActivityIndicator(
    { style: Ti.UI.ActivityIndicatorStyle.BIG, accessibilityLabel: "items_sold_web_view_activity_indicator", id: "items_sold_web_view_activity_indicator" });

    $.__views.items_sold_web_view_activity_indicator_wrapper.add($.__views.items_sold_web_view_activity_indicator);
    $.__views.reload_product_sold_chart_button = Ti.UI.createButton(
    { viewShadowColor: Alloy.Styles.accentColor, viewShadowOffset: { x: 0, y: 0 }, viewShadowRadius: 2, right: 10, bottom: 10, width: 40, height: 40, borderRadius: 20, backgroundImage: Alloy.Styles.refreshImageWhite, backgroundSelectedImage: Alloy.Styles.refreshImageBlack, borderWidth: 3, borderColor: Alloy.Styles.accentColor, id: "reload_product_sold_chart_button", accessibilityValue: "reload_product_sold_chart_button" });

    $.__views.items_sold_view.add($.__views.reload_product_sold_chart_button);
    reloadItemsSoldChart ? $.addListener($.__views.reload_product_sold_chart_button, 'click', reloadItemsSoldChart) : __defers['$.__views.reload_product_sold_chart_button!click!reloadItemsSoldChart'] = true;}
  if (Alloy.CFG.sales_reports.enabled_charts.ranks) {
    $.__views.rank_view = Ti.UI.createView(
    { id: "rank_view" });

    __alloyId223.push($.__views.rank_view);
    if (Alloy.Models.associate.hasStoreLevelSalesReportsPrivileges()) {
      $.__views.rank_view_control_container = Ti.UI.createView(
      { top: 0, height: "10%", id: "rank_view_control_container" });

      $.__views.rank_view.add($.__views.rank_view_control_container);
      $.__views.rank_view_control = Ti.UI.createView(
      { viewShadowColor: Alloy.Styles.shadowGrayColor, viewShadowOffset: { x: 0, y: 0 }, viewShadowRadius: 2, width: "50%", height: "80%", layout: "horizontal", id: "rank_view_control" });

      $.__views.rank_view_control_container.add($.__views.rank_view_control);
      rankViewControlClickHandler ? $.addListener($.__views.rank_view_control, 'click', rankViewControlClickHandler) : __defers['$.__views.rank_view_control!click!rankViewControlClickHandler'] = true;$.__views.associates_ranking = Ti.UI.createButton(
      { left: 0, width: "49.99%", borderColor: Alloy.Styles.accentColor, color: Alloy.Styles.color.text.white, backgroundColor: Alloy.Styles.accentColor, font: Alloy.Styles.buttonFont, bubbleParent: true, height: Ti.UI.FILL, titleid: "_Associates", id: "associates_ranking", accessibilityValue: "associates_ranking" });

      $.__views.rank_view_control.add($.__views.associates_ranking);
      onRankViewControlClick ? $.addListener($.__views.associates_ranking, 'click', onRankViewControlClick) : __defers['$.__views.associates_ranking!click!onRankViewControlClick'] = true;$.__views.stores_ranking = Ti.UI.createButton(
      { left: 0, width: "49.99%", borderColor: Alloy.Styles.accentColor, color: Alloy.Styles.accentColor, backgroundColor: Alloy.Styles.color.background.white, font: Alloy.Styles.buttonFont, bubbleParent: true, height: Ti.UI.FILL, titleid: "_Stores", id: "stores_ranking", accessibilityValue: "stores_ranking" });

      $.__views.rank_view_control.add($.__views.stores_ranking);
      onRankViewControlClick ? $.addListener($.__views.stores_ranking, 'click', onRankViewControlClick) : __defers['$.__views.stores_ranking!click!onRankViewControlClick'] = true;}
    $.__views.ranking_web_view = Ti.UI.createWebView(
    function () {
      var o = {};
      Alloy.deepExtend(true, o, { top: 0, willHandleTouches: false, hideLoadIndicator: true });
      if (Alloy.Models.associate.hasStoreLevelSalesReportsPrivileges()) Alloy.deepExtend(true, o, { top: "10.5%" });
      if (!Alloy.Models.associate.hasStoreLevelSalesReportsPrivileges()) Alloy.deepExtend(true, o, { top: 0 });
      Alloy.deepExtend(true, o, { id: "ranking_web_view", urlPageName: Alloy.CFG.sales_reports.url_page_names.ranks.associates, accessibilityLabel: "ranking_web_view" });
      return o;
    }());

    $.__views.rank_view.add($.__views.ranking_web_view);
    hideActivityIndicator ? $.addListener($.__views.ranking_web_view, 'load', hideActivityIndicator) : __defers['$.__views.ranking_web_view!load!hideActivityIndicator'] = true;showActivityIndicator ? $.addListener($.__views.ranking_web_view, 'beforeload', showActivityIndicator) : __defers['$.__views.ranking_web_view!beforeload!showActivityIndicator'] = true;showWebViewLoadError ? $.addListener($.__views.ranking_web_view, 'error', showWebViewLoadError) : __defers['$.__views.ranking_web_view!error!showWebViewLoadError'] = true;$.__views.ranking_web_view_activity_indicator_wrapper = Ti.UI.createView(
    { width: 0, height: 0, backgroundColor: Alloy.Styles.color.background.transparentBlack30Percent, opacity: 0, id: "ranking_web_view_activity_indicator_wrapper" });

    $.__views.rank_view.add($.__views.ranking_web_view_activity_indicator_wrapper);
    $.__views.ranking_web_view_error_msg = Ti.UI.createLabel(
    { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.errorMessageFont, opacity: 0, backgroundColor: Alloy.Styles.color.background.white, id: "ranking_web_view_error_msg", accessibilityLabel: "ranking_web_view_error_msg" });

    $.__views.ranking_web_view_activity_indicator_wrapper.add($.__views.ranking_web_view_error_msg);
    $.__views.ranking_web_view_activity_indicator = Ti.UI.createActivityIndicator(
    { style: Ti.UI.ActivityIndicatorStyle.BIG, id: "ranking_web_view_activity_indicator", accessibilityLabel: "ranking_web_view_activity_indicator" });

    $.__views.ranking_web_view_activity_indicator_wrapper.add($.__views.ranking_web_view_activity_indicator);
    $.__views.reload_rank_chart_button = Ti.UI.createButton(
    { viewShadowColor: Alloy.Styles.accentColor, viewShadowOffset: { x: 0, y: 0 }, viewShadowRadius: 2, right: 10, bottom: 10, width: 40, height: 40, borderRadius: 20, backgroundImage: Alloy.Styles.refreshImageWhite, backgroundSelectedImage: Alloy.Styles.refreshImageBlack, borderWidth: 3, borderColor: Alloy.Styles.accentColor, id: "reload_rank_chart_button", accessibilityLabel: "reload_rank_chart_button" });

    $.__views.rank_view.add($.__views.reload_rank_chart_button);
    reloadRankChart ? $.addListener($.__views.reload_rank_chart_button, 'click', reloadRankChart) : __defers['$.__views.reload_rank_chart_button!click!reloadRankChart'] = true;}
  $.__views.scrollable_web_view_container = Ti.UI.createScrollableView(
  { top: 0, scrollingEnabled: false, views: __alloyId223, id: "scrollable_web_view_container" });

  $.__views.chart_panel.add($.__views.scrollable_web_view_container);
  $.__views.quick_stat_panel = Ti.UI.createView(
  { top: "3%", right: "2%", height: "95%", width: "24%", backgroundColor: Alloy.Styles.color.background.white, viewShadowColor: Alloy.Styles.shadowGrayColor, viewShadowOffset: { x: 0, y: 0 }, viewShadowRadius: 2, id: "quick_stat_panel" });

  $.__views.chart_area.add($.__views.quick_stat_panel);
  $.__views.quick_stat_panel_title = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.accentColor, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, top: 15, font: Alloy.Styles.bigButtonFont, textid: "_Quick_Stats", id: "quick_stat_panel_title", accessibilityValue: "quick_stat_panel_title" });

  $.__views.quick_stat_panel.add($.__views.quick_stat_panel_title);
  $.__views.__alloyId224 = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.accentColor, width: Ti.UI.FILL, height: 1, top: 50, id: "__alloyId224" });

  $.__views.quick_stat_panel.add($.__views.__alloyId224);
  $.__views.quick_stats_container = Ti.UI.createTableView(
  { top: 51, allowsSelection: false, width: Ti.UI.FILL, separatorColor: Alloy.Styles.color.background.transparent, id: "quick_stats_container" });

  $.__views.quick_stat_panel.add($.__views.quick_stats_container);
  var __alloyId228 = Alloy.Collections['$.quickStatsCollection'] || $.quickStatsCollection;function __alloyId229(e) {if (e && e.fromAdapter) {return;}var opts = __alloyId229.opts || {};var models = __alloyId228.models;var len = models.length;var rows = [];for (var i = 0; i < len; i++) {var __alloyId225 = models[i];__alloyId225.__transform = _.isFunction(__alloyId225.transform) ? __alloyId225.transform() : __alloyId225.toJSON();var __alloyId227 = Alloy.createController('reports/quickStats', { $model: __alloyId225 });
      rows.push(__alloyId227.getViewEx({ recurse: true }));
    }$.__views.quick_stats_container.setData(rows);};__alloyId228.on('fetch destroy change add remove reset', __alloyId229);if (Alloy.Models.associate.hasStoreLevelSalesReportsPrivileges()) {
    $.__views.sales_dashboard_overlay = Ti.UI.createView(
    { width: "0%", height: "0%", backgroundColor: Alloy.Styles.color.background.transparentBlack30Percent, id: "sales_dashboard_overlay" });

    $.__views.chart_area.add($.__views.sales_dashboard_overlay);
    hideAssociateList ? $.addListener($.__views.sales_dashboard_overlay, 'click', hideAssociateList) : __defers['$.__views.sales_dashboard_overlay!click!hideAssociateList'] = true;}
  exports.destroy = function () {__alloyId228 && __alloyId228.off('fetch destroy change add remove reset', __alloyId229);};




  _.extend($, $.__views);










  var logger = require('logging')('reports:index', getFullControllerPath($.__controllerPath));
  var associatetableShown = false;
  var TIME_START_DAY = 'T00:00:00',
  TIME_END_DAY = 'T23:59:59';

  var buildStorefrontURL = require('EAUtils').buildStorefrontURL;
  var getUIObjectType = require('EAUtils').getUIObjectType;
  var formatDate = require('EAUtils').formatDate;
  var sendErrorToServer = require('EAUtils').sendErrorToServer;
  var EAUtils = require('EAUtils');
  var associateModel = Alloy.Models.associate;
  var params = {
    employeeId: associateModel.getEmployeeId(),
    storeId: Alloy.CFG.store_id,
    loadEmployeeList: associateModel.hasStoreLevelSalesReportsPrivileges() };

  params = _.extend(params, EAUtils.getCurrencyConfiguration());
  var filterModel = new Backbone.Model(params);

  var associateCollection = new Backbone.Collection();

  var pageLoadTries = Alloy.CFG.sales_reports.page_load_tries || 3;

  var buttonStyle = {
    selected: {
      color: Alloy.Styles.color.background.white,
      backgroundColor: Alloy.Styles.accentColor },

    unselected: {
      color: Alloy.Styles.accentColor,
      backgroundColor: Alloy.Styles.color.background.white } },



  chartViewStyle = {
    disableArea: {
      width: '100%',
      height: '100%' },

    enableArea: {
      width: '0%',
      height: '0%' } };






  $.date_filter_control.addEventListener('click', dateFilterControlClickHandler);
  $.scrollable_view_control.addEventListener('click', scrollableViewControlClickHandler);
  $.today.addEventListener('click', loadTodayReport);
  $.week.addEventListener('click', loadThisWeek);
  $.month.addEventListener('click', loadThisMonth);
  $.quarter.addEventListener('click', loadThisQuater);
  $.custom_date.addEventListener('click', showCustomDatePickerDialog);
  $.close_icon.addEventListener('click', dismiss);




  $.listenTo(filterModel, 'change', loadReports);
  if (associateModel.hasStoreLevelSalesReportsPrivileges()) {
    associateCollection.once('reset', renderAssociateTable);
  }




  exports.init = init;
  exports.deinit = deinit;








  function init() {
    loadThisWeek();
  }





  function deinit() {
    logger.info('deinit');
    if (associateModel.hasStoreLevelSalesReportsPrivileges()) {
      $.assoc_table.removeEventListener('click', handleAssociateSelect);
      $.hamburger_menu.removeEventListener('click', revealAssociateList);
      $.sales_dashboard_overlay.removeEventListener('click', hideAssociateList);
      associateCollection.stopListening();
      $.assoc_table.setData([]);
      if (Alloy.CFG.sales_reports.enabled_charts.ranks) {
        $.rank_view_control.removeEventListener('click', rankViewControlClickHandler);
        $.associates_ranking.removeEventListener('click', onRankViewControlClick);
        $.ranking_web_view.removeEventListener('load', hideActivityIndicator);
        $.ranking_web_view.removeEventListener('error', showWebViewLoadError);
        $.ranking_web_view.removeEventListener('beforeLoad', showActivityIndicator);
        $.stores_ranking.removeEventListener('click', onRankViewControlClick);
        $.reload_rank_chart_button.removeEventListener('click', reloadRankChart);
        if (false) {
          $.ranking_web_view.release();
        }
      }
    }

    $.sales_web_view.removeEventListener('load', onMainChartLoad);
    $.sales_web_view.removeEventListener('load', hideActivityIndicator);
    $.sales_web_view.removeEventListener('error', showWebViewLoadError);
    $.sales_web_view.removeEventListener('beforeLoad', showActivityIndicatorForPrimaryAndSecondaryWebView);
    $.reload_sales_chart_button.removeEventListener('click', reloadSalesChart);
    if (false) {
      $.sales_web_view.release();
    }

    if (Alloy.CFG.sales_reports.enabled_charts.items_sold) {
      $.items_sold_web_view.removeEventListener('load', hideActivityIndicator);
      $.items_sold_web_view.removeEventListener('error', showWebViewLoadError);
      $.items_sold_web_view.removeEventListener('beforeLoad', showActivityIndicator);
      $.reload_product_sold_chart_button.removeEventListener('click', reloadItemsSoldChart);
      if (false) {
        $.items_sold_web_view.release();
      }
    }

    $.date_filter_control.removeEventListener('click', dateFilterControlClickHandler);
    $.scrollable_view_control.removeEventListener('click', scrollableViewControlClickHandler);

    $.today.removeEventListener('click', loadTodayReport);
    $.week.removeEventListener('click', loadThisWeek);
    $.month.removeEventListener('click', loadThisMonth);
    $.quarter.removeEventListener('click', loadThisQuater);
    $.custom_date.removeEventListener('click', showCustomDatePickerDialog);
    $.close_icon.removeEventListener('click', dismiss);

    $.stopListening();
    $.destroy();
    $.scrollable_web_view_container.views = [];

  }













  function buildWebViewURL(urlPageName, queryString) {
    return buildStorefrontURL('https', urlPageName) + '?' + queryString;
  }






  function resetPageReloadTries() {
    pageLoadTries = Alloy.CFG.sales_reports.page_load_tries || 3;
  }







  function getToday() {
    var dateObj = {};
    var date = new Date();
    dateObj.dateFrom = formatDate(date, TIME_START_DAY);
    dateObj.dateTo = formatDate(date, TIME_END_DAY);
    dateObj.filterKey = 'today';
    return dateObj;
  }









  function getFirstDayOfWeek(d) {
    if (!Alloy.CFG.sales_reports.start_of_week || Alloy.CFG.sales_reports.start_of_week === '') {
      return getMonday(d);
    }
    if (Alloy.CFG.sales_reports.start_of_week.toLowerCase() === 'monday') {
      return getMonday(d);
    }
    if (Alloy.CFG.sales_reports.start_of_week.toLowerCase() === 'sunday') {
      return getSunday(d);
    }
    return d;
  }









  function getMonday(d) {
    var day = d.getDay();
    if (day == 1) {
      return d;
    }
    var diff = d.getDate() - day;
    diff += day == 0 ? -6 : 1;

    return new Date(d.setDate(diff));
  }









  function getSunday(d) {
    var day = d.getDay();
    if (day == 0) {
      return d;
    }
    var diff = d.getDate() - day;
    diff += day == 0 ? -6 : 0;

    return new Date(d.setDate(diff));
  }









  function objToUrlParams(model) {

    var qString = [];
    for (var key in model.attributes) if (model.attributes[key]) {
      qString.push(key + '=' + model.get(key));
    }
    return qString.join('&');
  }





  function loadMainWebView() {
    Alloy.eventDispatcher.trigger('session:renew');
    var qStr = objToUrlParams(filterModel);
    var urlPageName = Alloy.CFG.sales_reports.url_page_names.sales;
    var cURL = buildWebViewURL(urlPageName, qStr);
    $.sales_web_view.urlPageName = urlPageName;
    if (cURL !== $.sales_web_view.url) {
      $.sales_web_view.setUrl(cURL);
    }
  }








  function findWebView(arrayOfViews) {
    var webView = null;
    webView = _.find(arrayOfViews, function (view) {
      return getUIObjectType(view) === 'WebView';
    });
    return webView;
  }







  function loadFocusedWebViewContent(webView) {
    if ($.scrollable_web_view_container.views.length > 0) {

      webView = webView || findWebView($.scrollable_web_view_container.views[$.scrollable_web_view_container.currentPage].getChildren());
      if (webView) {

        var urlPageName = '';
        switch (webView) {
          case $.sales_web_view:






            return;
            break;
          case $.items_sold_web_view:
            Alloy.eventDispatcher.trigger('session:renew');
            if (Alloy.CFG.sales_reports.enabled_charts.items_sold) {
              urlPageName = Alloy.CFG.sales_reports.url_page_names.items_sold;
            }
            break;
          case $.ranking_web_view:
            Alloy.eventDispatcher.trigger('session:renew');
            if (Alloy.CFG.sales_reports.enabled_charts.ranks) {
              var changedAttributes = filterModel.changedAttributes();
              if (changedAttributes.dateFrom || changedAttributes.dateTo || !$.ranking_web_view.url || $.ranking_web_view.url == '') {
                urlPageName = $.ranking_web_view.urlPageName || Alloy.CFG.sales_reports.url_page_names.ranks.associates;
              } else {
                return;
              }
            }

            break;
          default:
            break;}

        var qStr = objToUrlParams(filterModel);
        var cURL = buildWebViewURL(urlPageName, qStr);
        webView.urlPageName = urlPageName;
        if (cURL !== webView.previousURL) {
          webView.setUrl(cURL);
          webView.previousURL = cURL;
        }
      }
    }
  }








  function getWebViewLoadErrorMessage(errorCode) {
    var errorMsg = '';
    switch (errorCode) {
      case Titanium.UI.URL_ERROR_AUTHENTICATION:
        errorMsg = _L('Authentication error');
        errorMsg += '\n' + _L('Please contact your manager or support.');
        break;
      case Titanium.UI.URL_ERROR_BAD_URL:
        errorMsg = _L('Bad URL');
        errorMsg += '\n' + _L('Please contact your manager or support.');
        break;
      case Titanium.UI.URL_ERROR_CONNECT:
        errorMsg = _L('Failure to connect to host');
        errorMsg += '\n' + _L('Please reload');

        break;
      case Titanium.UI.URL_ERROR_SSL_FAILED:
        errorMsg = _L('SSL failure');
        errorMsg += '\n' + _L('Please contact your manager or support.');
        break;
      case Titanium.UI.URL_ERROR_FILE:
        errorMsg = _L('Failure to access a file resource on the host');
        errorMsg += '\n' + _L('Please try again in a few minutes.');
        break;
      case Titanium.UI.URL_ERROR_FILE_NOT_FOUND:
        errorMsg = _L('File requested does not exist on the host');
        errorMsg += '\n' + _L('Please try again in a few minutes.');
        break;
      case Titanium.UI.URL_ERROR_HOST_LOOKUP:
        errorMsg = _L('DNS lookup error - Host name cannot be resolved.');
        errorMsg += '\n' + _L('Please contact your manager or support.');
        break;
      case Titanium.UI.URL_ERROR_REDIRECT_LOOP:
        errorMsg = _L('Redirect loop is detected.');
        errorMsg += '\n' + _L('Please contact your manager or support.');
        break;
      case Titanium.UI.URL_ERROR_TIMEOUT:
        errorMsg = _L('Page timeout occured');
        errorMsg += '\n' + _L('Please reload');
        break;
      case Titanium.UI.URL_ERROR_UNKNOWN:
        errorMsg = _L('Unknown error occured');
        errorMsg += '\n' + _L('Please reload');
        break;
      case Titanium.UI.URL_ERROR_UNSUPPORTED_SCHEME:
        errorMsg = _L('URL contains an unsupported scheme');
        errorMsg += '\n' + _L('Please try again in a few minutes.');
        break;
      default:
        errorMsg = _L('Unknown error occured');
        errorMsg += '\n' + _L('Please reload');
        break;}

    return errorMsg;
  }






  function setScrollableViewControlContentDimension() {
    var content = $.scrollable_view_control.getChildren();
    _.each(content, function (cContent) {
      cContent.width = 100 / content.length - 0.00001 + '%';
    });
  }







  function handleErrorOnReportLoadFail(webview) {

    setTimeout(function () {
      if (!isPageLoadSuccess(webview)) {
        var errorMsgToDisplay = undefined,
        errorMsgToEmail = undefined;
        var titleText = _L('The sales report failed to load. The following error was returned from the server.') + '\n\n';
        var errorText = webview.evalJS('document.body.innerHTML');

        if (errorText == '') {

          return;
        }


        webview.evalJS('document.body.innerHTML=" "');

        try {

          var errorObj = JSON.parse(errorText);
          errorMsgToDisplay = errorObj.fault.description || errorObj.fault.message;
          if (errorMsgToDisplay) {
            errorMsgToDisplay = titleText + errorMsgToDisplay;
          }
        } catch (ex) {}


        errorMsgToEmail = titleText + errorText;


        Alloy.Dialog.showCustomDialog({
          controllerPath: 'components/errorPopover',
          initOptions: errorMsgToDisplay || errorMsgToEmail,
          continueEvent: 'errorPopover:dismiss',
          continueFunction: function (event) {
            errorMsgToEmail += '\n\nPage Name : \n' + webview.urlPageName;
            if (event.text) {
              errorMsgToEmail += '\n\nUser Data: \n' + event.text;
            }
            sendErrorToServer(errorMsgToEmail);
          } });

      }
    }, 500);
  }








  function isPageLoadSuccess(cWebview) {
    var pageLoaded = cWebview.evalJS('isPageLoaded()');
    if (pageLoaded === 'true') {
      return true;
    } else {
      return false;
    }
  }









  function loadThisWeek() {
    var dateObj = {};
    var date = new Date();
    dateObj.dateTo = formatDate(date, TIME_END_DAY);

    var last = getFirstDayOfWeek(date);
    dateObj.dateFrom = formatDate(last, TIME_START_DAY);

    dateObj.filterKey = 'week';
    filterModel.set(dateObj);
  }






  function loadThisMonth() {
    var dateObj = {};
    var date = new Date();

    dateObj.dateTo = formatDate(date, TIME_END_DAY);
    var first = new Date(date.getFullYear(), date.getMonth(), 1);
    dateObj.dateFrom = formatDate(first, TIME_START_DAY);

    dateObj.filterKey = 'month';
    filterModel.set(dateObj);
  }






  function loadThisQuater() {
    var dateObj = {};
    var date = new Date();

    dateObj.dateTo = formatDate(date, TIME_END_DAY);
    var month = date.getMonth();
    if (month < 2) {
      month = 0;
    } else {
      month -= 2;
    }
    var first = new Date(date.getFullYear(), month, 1);
    dateObj.dateFrom = formatDate(first, TIME_START_DAY);

    dateObj.filterKey = 'quater';
    filterModel.set(dateObj);
  }






  function loadThisYear() {
    var dateObj = {};
    var date = new Date();

    dateObj.dateTo = formatDate(date, TIME_END_DAY);
    var first = new Date(date.getFullYear(), 0, 1);
    dateObj.dateFrom = formatDate(first, TIME_START_DAY);

    dateObj.filterKey = 'year';
    filterModel.set(dateObj);
  }






  function loadTodayReport() {
    filterModel.set(getToday());
  }






  function showCustomDatePickerDialog() {
    Alloy.Dialog.showCustomDialog({
      controllerPath: 'reports/datePickers',
      continueEvent: 'datePickers:dismiss',
      options: {
        model: filterModel } });


  }







  function onMainChartLoad(event) {

    var chartData = $.sales_web_view.evalJS('getJSONData()');

    if (chartData) {
      resetPageReloadTries();
      chartData = JSON.parse(chartData);
      $.quickStatsCollection.reset(chartData.quickStats);

      if (associateModel.hasStoreLevelSalesReportsPrivileges()) {
        associateCollection.reset(chartData.storeEmployees);
      }

      setTimeout(loadFocusedWebViewContent, 200);
    } else {
      logger.info('EVALJS failed reloading');
      if (pageLoadTries > 0) {
        $.sales_web_view.reload();
        pageLoadTries--;
      } else {
        handleErrorOnReportLoadFail($.sales_web_view);
      }
    }
    hideActivityIndicator(event);
  }







  function scrollableViewControlClickHandler(event) {

    _.each($.scrollable_view_control.getChildren(), function (child, index) {
      if (child !== event.source) {
        _.extend(child, buttonStyle.unselected);
      } else {
        $.scrollable_web_view_container.scrollToView(index);
        _.extend(event.source, buttonStyle.selected);
      }
    });

    switch (event.source.id) {
      case 'items_sold_view_control':
        loadFocusedWebViewContent($.items_sold_web_view);
        break;
      case 'ranks_web_view_control':
        loadFocusedWebViewContent($.ranking_web_view);
        break;
      default:
        break;}

  }







  function dateFilterControlClickHandler(event) {
    _.each($.date_filter_control.getChildren(), function (child, index) {
      if (child === event.source) {
        _.extend(child, buttonStyle.selected);
      } else {
        _.extend(child, buttonStyle.unselected);
      }
    });
  }







  function rankViewControlClickHandler(event) {
    _.each($.rank_view_control.getChildren(), function (child, index) {
      if (child === event.source) {
        _.extend(child, buttonStyle.selected);
      } else {
        _.extend(child, buttonStyle.unselected);
      }
    });
  }






  function revealAssociateList() {

    if (!associatetableShown) {


      var firstMove = Ti.UI.createAnimation({
        duration: 400,
        left: '30%' });

      var focusAssocTable = function () {
        _.extend($.sales_dashboard_overlay, chartViewStyle.disableArea);
        firstMove.removeEventListener('complete', focusAssocTable);
      };
      firstMove.addEventListener('complete', focusAssocTable);

      $.chart_area.animate(firstMove);
      associatetableShown = true;
    } else {
      hideAssociateList();
    }
  }






  function hideAssociateList() {
    if (associatetableShown) {
      var secondMove = Ti.UI.createAnimation({
        duration: 400,
        left: '0' });

      var blurAssocTAble = function () {
        _.extend($.sales_dashboard_overlay, chartViewStyle.enableArea);
        secondMove.removeEventListener('complete', blurAssocTAble);
      };
      secondMove.addEventListener('complete', blurAssocTAble);

      $.chart_area.animate(secondMove);
      associatetableShown = false;
    }
  }







  function handleAssociateSelect(event) {

    if (event.rowData.id === 'ALL_STORE') {
      filterModel.unset('employeeId');
    } else {
      filterModel.set('employeeId', event.rowData.id);
    }
    $.dashboard_title.animate({
      duration: 300,
      opacity: 0 },
    function () {
      $.dashboard_title.setText(String.format(_L('Sales Report Dashboard '), event.rowData.title));
      $.dashboard_title.animate({
        duration: 300,
        opacity: 1 });

    });

    hideAssociateList();
  }






  function reloadSalesChart() {
    Alloy.eventDispatcher.trigger('session:renew');
    $.sales_web_view.reload();
  }






  function reloadRankChart() {
    Alloy.eventDispatcher.trigger('session:renew');
    $.ranking_web_view.reload();
  }






  function reloadItemsSoldChart() {
    Alloy.eventDispatcher.trigger('session:renew');
    $.items_sold_web_view.reload();
  }






  function dismiss() {
    $.trigger('salesReport:dismiss');
  }







  function showWebViewLoadError(event) {
    var wrapperID = event.source.id + '_activity_indicator_wrapper';
    if ($[wrapperID]) {
      if ($[event.source.id + '_activity_indicator']) {
        $[event.source.id + '_activity_indicator'].hide();
      }
      if ($[event.source.id + '_error_msg']) {
        $[event.source.id + '_error_msg'].setText(getWebViewLoadErrorMessage(event.errorCode));
        $[event.source.id + '_error_msg'].animate({
          duration: 200,
          opacity: 1 });

      }
    }
  }







  function hideActivityIndicator(event) {
    if (event.source !== $.sales_web_view) {

      handleErrorOnReportLoadFail(event.source);
    }
    var wrapperID = event.source.id + '_activity_indicator_wrapper';
    if ($[event.source.id + '_error_msg']) {
      $[event.source.id + '_error_msg'].setText('');
      $[event.source.id + '_error_msg'].setOpacity(0);
    }
    if ($[wrapperID]) {
      $[wrapperID].animate({
        duration: 500,
        opacity: 0 },
      function () {
        $[wrapperID].width = 0;
        $[wrapperID].height = 0;
        if ($[event.source.id + '_activity_indicator']) {
          $[event.source.id + '_activity_indicator'].hide();
        }
      });
    }
  }







  function showActivityIndicatorForPrimaryAndSecondaryWebView(event) {
    showActivityIndicator(event);
    if ($.scrollable_web_view_container.views.length > 0) {

      var webView = findWebView($.scrollable_web_view_container.views[$.scrollable_web_view_container.currentPage].getChildren());

      if (webView && webView !== event.source) {
        showActivityIndicator({
          source: webView });

      }
    }
  }







  function showActivityIndicator(event) {
    if (!event.source) {
      return;
    }

    var wrapperID = event.source.id + '_activity_indicator_wrapper';
    if ($[event.source.id + '_error_msg']) {
      $[event.source.id + '_error_msg'].setText('');
      $[event.source.id + '_error_msg'].setOpacity(0);
    }
    if ($[wrapperID]) {
      if ($[event.source.id + '_activity_indicator']) {
        $[event.source.id + '_activity_indicator'].show();
      };
      $[wrapperID].width = Ti.UI.FILL;
      $[wrapperID].height = Ti.UI.FILL;
      $[wrapperID].animate({
        duration: 500,
        opacity: 1 });

    }
  }







  function onRankViewControlClick(event) {
    Alloy.eventDispatcher.trigger('session:renew');
    if (Alloy.CFG.sales_reports.enabled_charts.ranks) {
      var changedAttributes = filterModel.changedAttributes();
      var qStr = objToUrlParams(filterModel);
      var urlPageName = '';
      switch (event.source) {
        case $.associates_ranking:
          urlPageName = Alloy.CFG.sales_reports.url_page_names.ranks.associates;
          break;
        case $.stores_ranking:
          urlPageName = Alloy.CFG.sales_reports.url_page_names.ranks.stores;
          break;
        default:
          break;}



      var cURL = buildWebViewURL(urlPageName, qStr);
      if (changedAttributes.dateFrom || changedAttributes.dateTo || urlPageName != $.ranking_web_view.urlPageName) {

        $.ranking_web_view.urlPageName = urlPageName;
        if (cURL !== $.ranking_web_view.url) {
          $.ranking_web_view.setUrl(cURL);
        }
      }
    }
  }









  function loadReports() {
    if (!$.date_range_info.getText()) {
      $.date_range_info.setText(moment(filterModel.get('dateFrom')).format('LL') + ' - ' + moment(filterModel.get('dateTo')).format('LL'));
    }
    $.date_range_info.animate({
      duration: 300,
      opacity: 0 },
    function () {

      $.date_range_info.setText(moment(filterModel.get('dateFrom')).format('LL') + ' - ' + moment(filterModel.get('dateTo')).format('LL'));
      $.date_range_info.animate({
        duration: 300,
        opacity: 1 });

    });
    loadMainWebView();
  }






  function renderAssociateTable() {
    var data = [];
    data.push({
      title: _L('My Store'),
      id: 'ALL_STORE',
      selectedBackgroundColor: Alloy.Styles.accentColor,
      hasChild: true });

    associateCollection.each(function (assocMod) {
      data.push({
        title: assocMod.get('firstName') + ' ' + assocMod.get('lastName'),
        id: assocMod.get('id'),
        selectedBackgroundColor: Alloy.Styles.accentColor,
        hasChild: true });

    });
    $.assoc_table.setData(data);
    filterModel.set({
      loadEmployeeList: false },
    {
      silent: true });

  }




  moment.locale(Alloy.CFG.languageSelected);
  $.dashboard_title.setText(String.format(_L('Sales Report Dashboard '), associateModel.getFullName()));

  if ($.ranking_web_view) {
    $.ranking_web_view.urlPageName = Alloy.CFG.sales_reports.url_page_names.ranks.associates;
  }
  if ($.items_sold_web_view) {
    $.items_sold_web_view.urlPageName = Alloy.CFG.sales_reports.url_page_names.items_sold;
  }
  if ($.sales_web_view) {
    $.sales_web_view.urlPageName = Alloy.CFG.sales_reports.url_page_names.sales;
  }

  setScrollableViewControlContentDimension();





  if (Alloy.Models.associate.hasStoreLevelSalesReportsPrivileges()) {
    __defers['$.__views.hamburger_menu!click!revealAssociateList'] && $.addListener($.__views.hamburger_menu, 'click', revealAssociateList);}
  __defers['$.__views.assoc_table!click!handleAssociateSelect'] && $.addListener($.__views.assoc_table, 'click', handleAssociateSelect);__defers['$.__views.sales_web_view!load!onMainChartLoad'] && $.addListener($.__views.sales_web_view, 'load', onMainChartLoad);__defers['$.__views.sales_web_view!error!showWebViewLoadError'] && $.addListener($.__views.sales_web_view, 'error', showWebViewLoadError);__defers['$.__views.sales_web_view!beforeload!showActivityIndicatorForPrimaryAndSecondaryWebView'] && $.addListener($.__views.sales_web_view, 'beforeload', showActivityIndicatorForPrimaryAndSecondaryWebView);__defers['$.__views.reload_sales_chart_button!click!reloadSalesChart'] && $.addListener($.__views.reload_sales_chart_button, 'click', reloadSalesChart);if (Alloy.CFG.sales_reports.enabled_charts.items_sold) {
    __defers['$.__views.items_sold_web_view!load!hideActivityIndicator'] && $.addListener($.__views.items_sold_web_view, 'load', hideActivityIndicator);}
  if (Alloy.CFG.sales_reports.enabled_charts.items_sold) {
    __defers['$.__views.items_sold_web_view!error!showWebViewLoadError'] && $.addListener($.__views.items_sold_web_view, 'error', showWebViewLoadError);}
  if (Alloy.CFG.sales_reports.enabled_charts.items_sold) {
    __defers['$.__views.items_sold_web_view!beforeload!showActivityIndicator'] && $.addListener($.__views.items_sold_web_view, 'beforeload', showActivityIndicator);}
  if (Alloy.CFG.sales_reports.enabled_charts.items_sold) {
    __defers['$.__views.reload_product_sold_chart_button!click!reloadItemsSoldChart'] && $.addListener($.__views.reload_product_sold_chart_button, 'click', reloadItemsSoldChart);}
  if (Alloy.CFG.sales_reports.enabled_charts.ranks && Alloy.Models.associate.hasStoreLevelSalesReportsPrivileges()) {
    __defers['$.__views.rank_view_control!click!rankViewControlClickHandler'] && $.addListener($.__views.rank_view_control, 'click', rankViewControlClickHandler);}
  if (Alloy.CFG.sales_reports.enabled_charts.ranks && Alloy.Models.associate.hasStoreLevelSalesReportsPrivileges()) {
    __defers['$.__views.associates_ranking!click!onRankViewControlClick'] && $.addListener($.__views.associates_ranking, 'click', onRankViewControlClick);}
  if (Alloy.CFG.sales_reports.enabled_charts.ranks && Alloy.Models.associate.hasStoreLevelSalesReportsPrivileges()) {
    __defers['$.__views.stores_ranking!click!onRankViewControlClick'] && $.addListener($.__views.stores_ranking, 'click', onRankViewControlClick);}
  if (Alloy.CFG.sales_reports.enabled_charts.ranks) {
    __defers['$.__views.ranking_web_view!load!hideActivityIndicator'] && $.addListener($.__views.ranking_web_view, 'load', hideActivityIndicator);}
  if (Alloy.CFG.sales_reports.enabled_charts.ranks) {
    __defers['$.__views.ranking_web_view!beforeload!showActivityIndicator'] && $.addListener($.__views.ranking_web_view, 'beforeload', showActivityIndicator);}
  if (Alloy.CFG.sales_reports.enabled_charts.ranks) {
    __defers['$.__views.ranking_web_view!error!showWebViewLoadError'] && $.addListener($.__views.ranking_web_view, 'error', showWebViewLoadError);}
  if (Alloy.CFG.sales_reports.enabled_charts.ranks) {
    __defers['$.__views.reload_rank_chart_button!click!reloadRankChart'] && $.addListener($.__views.reload_rank_chart_button, 'click', reloadRankChart);}
  if (Alloy.Models.associate.hasStoreLevelSalesReportsPrivileges()) {
    __defers['$.__views.sales_dashboard_overlay!click!hideAssociateList'] && $.addListener($.__views.sales_dashboard_overlay, 'click', hideAssociateList);}




  _.extend($, exports);
}

module.exports = Controller;