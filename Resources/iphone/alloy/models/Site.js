var Alloy = require('/alloy'),
    _ = require("/alloy/underscore")._,
    model,
    collection;

exports.definition = {
    config: {
        model_name: 'site',
        secure: false,
        adapter: {
            type: 'ocapi'
        }
    },

    extendModel: function (Model) {
        _.extend(Model.prototype, {

            urlRoot: '/site'
        });
        return Model;
    },

    extendCollection: function (Collection) {
        _.extend(Collection.prototype, {});
        return Collection;
    }
};

model = Alloy.M('site', exports.definition, []);

collection = Alloy.C('site', exports.definition, model);

exports.Model = model;
exports.Collection = collection;