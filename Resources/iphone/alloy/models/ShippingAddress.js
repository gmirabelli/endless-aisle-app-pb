var Alloy = require('/alloy'),
    _ = require("/alloy/underscore")._,
    model,
    collection;

var Address = require('dw/instore/Address');

exports.definition = {
    config: {
        model_name: 'shippingAddress',
        adapter: {
            type: 'ocapi'
        },
        superClass: Address
    },

    extendModel: function (Model) {
        _.extend(Model.prototype, {
            getEmail: function () {
                return this.get('email');
            }
        });

        return Model;
    },
    extendCollection: function (Collection) {
        _.extend(Collection.prototype, {});

        return Collection;
    }
};

model = Alloy.M('shippingAddress', exports.definition, []);

collection = Alloy.C('shippingAddress', exports.definition, model);

exports.Model = model;
exports.Collection = collection;