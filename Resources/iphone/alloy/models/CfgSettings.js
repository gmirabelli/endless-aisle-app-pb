var Alloy = require('/alloy'),
    _ = require("/alloy/underscore")._,
    model,
    collection;

function queryParams() {
    return {
        store_id: this.get('store_id')
    };
}

var StorefrontHelperSecure = require('dw/instore/StorefrontHelperSecure');

var KioskCfgSettings = StorefrontHelperSecure.extend({
    urlRoot: '/EAConfigs-GetKioskCFGSettings',
    queryParams: queryParams
});

exports.definition = {
    config: {
        model_name: 'cfgSettings',
        secure: true,
        cache: false,
        adapter: {
            type: 'storefront'
        }
    },

    extendModel: function (Model) {
        _.extend(Model.prototype, {

            urlRoot: '/EAConfigs-GetCFGSettings',

            queryParams: queryParams,

            loadServerConfigs: function (storeId) {
                this.set('store_id', storeId);
                var deferred = new _.Deferred();
                this.fetch().done(function () {
                    deferred.resolve();
                }).fail(function () {
                    deferred.reject();
                });
                return deferred.promise();
            },

            loadKioskServerConfigs: function (storeId) {
                var deferred = new _.Deferred();
                var kioskCfgSettings = new KioskCfgSettings();
                var self = this;
                kioskCfgSettings.set('store_id', storeId);
                kioskCfgSettings.fetch().done(function (model) {
                    self.set(model.toJSON(), { silent: true });
                    deferred.resolve(model);
                }).fail(function () {
                    deferred.reject(model);
                });
                return deferred.promise();
            },

            getSettings: function () {
                return this.get('CFG');
            }
        });
        return Model;
    }
};

model = Alloy.M('cfgSettings', exports.definition, []);

collection = Alloy.C('cfgSettings', exports.definition, model);

exports.Model = model;
exports.Collection = collection;