var Alloy = require('/alloy'),
    _ = require("/alloy/underscore")._,
    model,
    collection;

exports.definition = {
    config: {
        model_name: 'history',
        columns: {
            type: 'TEXT',
            time: 'INTEGER',
            associate_id: 'TEXT',
            customer_id: 'TEXT',
            route: 'TEXT',
            details: 'TEXT'
        },
        adapter: {
            type: 'sql',
            collection_name: 'history'
        }
    },
    extendModel: function (Model) {
        _.extend(Model.prototype, {});

        return Model;
    },
    extendCollection: function (Collection) {
        _.extend(Collection.prototype, {});

        return Collection;
    }
};

model = Alloy.M('history', exports.definition, []);

collection = Alloy.C('history', exports.definition, model);

exports.Model = model;
exports.Collection = collection;