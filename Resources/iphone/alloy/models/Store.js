var Alloy = require('/alloy'),
    _ = require("/alloy/underscore")._,
    model,
    collection;

var ShopAPI = require('dw/shop/index');

var StoreModel = ShopAPI.Store;

var getAddressStringFromAddressDataOrderAndType = require('EAUtils').getAddressStringFromAddressDataOrderAndType;

exports.definition = {
    config: {
        adapter: {
            type: 'ocapi',
            collection_name: 'store'
        },
        superClass: StoreModel,
        model_name: 'store',
        cache: false
    },

    extendModel: function (Model) {

        _.extend(Model.prototype, {
            constructDetailedStoreAddress: function () {
                var storeAddress = {
                    id: this.getId(),
                    name: this.get('name'),
                    address1: this.get('address1'),
                    address2: this.get('address2'),
                    city: this.get('city'),
                    state_code: this.get('stateCode'),
                    country_code: this.get('countryCode'),
                    postal_code: this.get('postalCode'),
                    city_state_postal_code: this.get('city') + ', ' + this.get('state_code') + " " + this.get('postal_code'),
                    phone: this.get('phone'),
                    inventory_id: this.get('inventory_id'),
                    distance: this.get('distance'),
                    distance_unit: this.get('distance_unit'),
                    distance_text: this.getTextualDistance(),
                    email: this.get('email'),
                    fax: this.get('fax'),
                    image: this.get('image'),
                    latitude: this.get('latitude'),
                    longitude: this.get('longitude'),
                    store_hours: this.get('store_hours'),
                    store_events: this.get('store_events')
                };
                if (this.get('address2')) {
                    storeAddress.address2 = this.get('address2');
                }
                return storeAddress;
            },

            constructStoreAvailability: function () {
                var storeInfo = this.constructDetailedStoreAddress();
                storeInfo = _.extend({
                    availability_message: this.get('availability_message'),
                    availability_color: this.get('availability_color'),
                    availability_stock_level: this.get('availability_stock_level')
                }, storeInfo);

                return storeInfo;
            },

            constructStoreAddress: function (id) {
                var storeAddress = {
                    first_name: ' ',
                    last_name: this.get('name'),
                    address1: this.get('address1'),
                    city: this.get('city'),
                    state_code: this.get('stateCode'),
                    country_code: this.get('countryCode'),
                    postal_code: this.get('postalCode'),
                    phone: this.get('phone')
                };
                if (this.get('address2')) {
                    storeAddress.address2 = this.get('address2');
                }
                return storeAddress;
            },

            constructStoreAddressForDifferentStorePickup: function (firstName, lastName) {
                var storeAddress = {
                    first_name: firstName,
                    last_name: lastName,
                    address1: this.get('name'),
                    address2: this.get('address1') + (this.get('address2') ? '   ' + this.get('address2') : ''),
                    city: this.get('city'),
                    state_code: this.get('stateCode'),
                    country_code: this.get('countryCode'),
                    postal_code: this.get('postalCode'),
                    phone: this.get('phone')
                };
                return storeAddress;
            },

            getAddressDisplay: function () {
                return getAddressStringFromAddressDataOrderAndType(this.constructStoreAddress(), require(Alloy.CFG.addressform).getAddressOrder(), 'shipping');
            },

            getId: function () {
                return this._get('id');
            },

            getPhone: function () {
                return this._get('phone');
            },

            getLatitude: function () {
                return this._get('latitude');
            },

            getLongitude: function () {
                return this._get('longitude');
            },

            getStore: function (storeId) {
                this.set({
                    id: storeId
                });
                return this.fetch();
            },

            getInventoryId: function () {
                return this._get('inventory_id') || this._get('c_inventoryListId');
            },

            getTextualDistance: function () {
                var dist = this.get('distance');
                var text = dist + " ";

                switch (this.get('distance_unit')) {
                    case 'mi':
                        if (dist == 1) {
                            text += _L('Mile');
                        } else {
                            text += _L('Miles');
                        }
                        break;
                    case 'km':
                        if (dist == 1) {
                            text += _L('Kilometer');
                        } else {
                            text += _L('Kilometers');
                        }
                        break;
                    default:
                        text += '???';
                        break;
                }
                return text;
            },

            setAvailabilityDetails: function (message, color, stockLevelMsg) {
                this.set({
                    availability_message: message,
                    availability_color: color,
                    availability_stock_level: stockLevelMsg
                });
            },

            setBasketInventoryAvailabilty: function (availability, unavailableItems) {
                this.set({
                    basket_inventory_available: availability,
                    unavailable_basket_items: unavailableItems || []
                }, {
                    silent: true
                });
            },

            isBasketAvailable: function () {
                return this.get('basket_inventory_available');
            },

            getUnavailableBasketItemIdsOnly: function () {
                var data = [];
                _.each(this.get('unavailable_basket_items'), function (item) {
                    data.push(item.id);
                });
                return data;
            },

            getAllUnavailableBasketItems: function () {
                return this.get('unavailable_basket_items');
            },

            setSelected: function (value) {
                this.set({
                    selected: value
                }, {
                    silent: true
                });
            },

            isSelected: function () {
                return this.get('selected') === true;
            }
        });
    },
    extendCollection: function (Collection) {
        _.extend(Collection.prototype, {
            url: function () {
                var ids = this.ids || _.pluck(this.models, 'id') || [];
                if (ids.length == 0) {
                    return '/stores';
                }

                return '/stores/(' + ids.join(',') + ')';
            },

            queryParams: function () {
                var params = {
                    country_code: this.country_code || Alloy.Models.storeInfo.getCountryCode(),
                    postal_code: this.postal_code || Alloy.Models.storeInfo.getPostalCode(),
                    max_distance: this.max_distance || Alloy.CFG.store_availability.max_distance_search,
                    distance_unit: this.distance_unit || Alloy.CFG.store_availability.distance_unit };
                if (_.isNumber(this.count) && _.isNumber(this.start)) {
                    params.count = this.count;
                    params.start = this.start;
                }
                return params;
            },

            parse: function (in_json) {
                if (in_json && 'data' in in_json) {
                    return in_json.data;
                } else if (in_json) {
                    return in_json;
                } else {
                    return [];
                }
            },

            getAllStores: function () {
                return this.fetch();
            },

            getStoresWithPagination: function (params, add, removeCurrentStore) {
                var deferred = new _.Deferred();
                add = _.isBoolean(add) ? add : false;
                _.extend(this, params || {});

                this.fetch({
                    add: add,
                    update: add,
                    remove: !add,
                    silent: true
                }).done(function () {
                    if (removeCurrentStore) {
                        this.filterOutCurrentStore();
                        deferred.resolve();
                    }
                }.bind(this)).fail(function () {
                    deferred.reject();
                });
                return deferred.promise();
            },

            getNextNStores: function (start, n) {
                var end = start + n > this.length ? this.length : start + n;
                return this.models.slice(start, end);
            },

            getInventory: function (product, start, count) {
                var stores = this.getNextNStores(start, count);
                var storeInventoryIds = [];
                _.each(stores, function (store) {
                    var inv = store.getInventoryId();
                    if (inv) {
                        storeInventoryIds.push(inv);
                    }
                });

                var v = Alloy.createModel('product', {
                    id: product.getProductId(),
                    expand: 'availability',
                    inventory_ids: storeInventoryIds
                });

                var promise = v.fetch({
                    cache: false
                });

                promise.done(function (model) {
                    product.set({
                        inventory: model._get('inventory'),
                        inventories: model._get('inventories')
                    }, {
                        silent: true
                    });
                });
                return promise;
            },

            getAllInventoryIds: function () {
                var storeInventoryIds = [];
                this.each(function (store) {
                    var inv = store.getInventoryId();
                    if (inv) {
                        storeInventoryIds.push(inv);
                    }
                });
                return storeInventoryIds;
            },

            getInventoryIdsByStartEndIndex: function (start, end) {
                return this.getAllInventoryIds().splice(start, end);
            },

            getStoreByInventoryId: function (inventoryId) {
                return this.find(function (store) {
                    return store.getInventoryId() === inventoryId;
                });
            },

            setBasketInventoryAvailabilty: function (basketItemsCollection, start, end) {
                if (_.isNumber(start) && _.isNumber(end)) {
                    for (var idx = start; idx <= end; idx++) {
                        var store = this.at(idx);
                        if (store) {
                            store.setBasketInventoryAvailabilty(basketItemsCollection.checkAvailabilityInStoreInventory(store.getInventoryId()), basketItemsCollection.getUnavailableItems(store.getInventoryId()));
                        }
                    }
                } else {
                    this.each(function (store) {
                        store.setBasketInventoryAvailabilty(basketItemsCollection.checkAvailabilityInStoreInventory(store.getInventoryId()), basketItemsCollection.getUnavailableItems(store.getInventoryId()));
                    });
                }
                this.trigger('change');
            },

            setSelectedStore: function (index) {
                this.each(function (store, idx) {
                    if (idx === index) {
                        store.setSelected(true);
                    } else {
                        store.setSelected(false);
                    }
                });
            },

            getSelectedStore: function () {
                return this.find(function (store) {
                    return store.isSelected() === true;
                });
            },

            getAllIds: function () {
                return this.pluck('id');
            },

            filterOutCurrentStore: function () {
                this.remove(Alloy.Models.storeInfo.getId(), { silent: true });
            }
        });
        return Collection;
    }
};

model = Alloy.M('store', exports.definition, []);

collection = Alloy.C('store', exports.definition, model);

exports.Model = model;
exports.Collection = collection;