var Alloy = require('/alloy'),
    _ = require("/alloy/underscore")._,
    model,
    collection;

var RecommendedItem = Backbone.Model.extend({
    getId: function () {
        return this.get('recommended_item_id');
    },

    getType: function () {
        return this.get('_type');
    },

    getRecommendationType: function () {
        return this.get('recommendation_type')._type;
    },

    getDisplayValue: function () {
        return this.get('recommendation_type').display_value;
    },

    getValue: function () {
        return this.get('recommendation_type').value;
    },

    getLink: function () {
        return this.get('recommended_item_link');
    }
});

var RecommendedItems = Backbone.Collection.extend({
    model: RecommendedItem,

    getAllProductIds: function () {
        return this.pluck('recommended_item_id');
    }
});

exports.definition = {
    config: {
        model_name: 'recommendations',
        adapter: {
            type: 'ocapi',
            collection_name: 'recommendations'
        }
    },
    extendModel: function (Model) {
        _.extend(Model.prototype, {

            relations: [{
                type: Backbone.Many,
                key: 'recommendations',
                collectionType: RecommendedItems,
                relatedModel: RecommendedItem }],

            initialize: function () {
                this.products = Alloy.createCollection('product');
            },

            getRecommendations: function (productId) {
                var self = this;
                var deferred = new _.Deferred();
                self.clear();
                self.products.reset();
                self.id = productId;
                self.fetch().done(function () {
                    if (self.get('recommendations') && self.get('recommendations').length > 0) {
                        self.products.ids = self.get('recommendations').getAllProductIds();

                        self.products.fetch().done(function () {
                            self.products.each(function (cProduct) {
                                cProduct.set({
                                    recommended_item_id: cProduct.get('product_id') || cProduct.getId()
                                });
                            });
                            deferred.resolve();
                        }).fail(function () {
                            deferred.reject();
                        });
                    } else {
                        deferred.resolve();
                    }
                }).fail(function () {
                    deferred.reject();
                });
                return deferred.promise();
            },

            getRecommendedItems: function () {
                return this.products;
            },

            getCount: function () {
                return this.get('recommendations').length;
            },

            url: function () {
                return '/products/' + this.id + '/recommendations';
            },

            queryParams: function () {
                return {};
            }
        });

        return Model;
    }
};

model = Alloy.M('recommendations', exports.definition, []);

collection = Alloy.C('recommendations', exports.definition, model);

exports.Model = model;
exports.Collection = collection;