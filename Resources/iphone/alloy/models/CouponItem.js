var Alloy = require('/alloy'),
    _ = require("/alloy/underscore")._,
    model,
    collection;

exports.definition = {
    config: {
        model_name: 'coupon_item',
        adapter: {
            type: 'ocapi'
        }
    },

    extendModel: function (Model) {
        _.extend(Model.prototype, {
            getCode: function () {
                return this.get('code');
            }
        });

        return Model;
    },
    extendCollection: function (Collection) {
        _.extend(Collection.prototype, {});

        return Collection;
    }
};

model = Alloy.M('couponItem', exports.definition, []);

collection = Alloy.C('couponItem', exports.definition, model);

exports.Model = model;
exports.Collection = collection;