var Alloy = require('/alloy'),
    _ = require("/alloy/underscore")._,
    model,
    collection;

var AuthorizationFault = Backbone.Model.extend({
    config: {
        secure: true,
        model_name: 'AuthorizationFault',
        cache: false
    },

    isBadLogin: function () {
        return this.has('fault') && this.get('fault').error == 'unauthorized_client';
    },

    isEmptyClientCredentials: function () {
        return this.has('fault') && this.get('fault').error == 'EmptyClientCredentials';
    }
});

exports.definition = {
    config: {
        model_name: 'authorizationToken',
        secure: true,
        cache: false,
        adapter: {
            type: 'storefront'
        }
    },

    extendModel: function (Model) {

        _.extend(Model.prototype, {

            urlRoot: '/EAUtils-GetAuthenticationToken',

            fetchToken: function (hostname) {
                var self = this;
                var now = new Date().getTime();
                var deferred = new _.Deferred();

                if (self.timeout && now < self.timeout) {
                    deferred.resolve();
                } else {
                    var savePromise = this.save({
                        hostname: hostname
                    });
                    self.clear();
                    savePromise.done(function (model) {
                        var now = new Date().getTime();
                        self.timeout = now + model.get('expires_in') * 1000;
                        deferred.resolve();
                    }).fail(function (fault) {
                        if (fault && fault.has('fault')) {
                            if (fault.get('fault').type === 'SessionTimeoutException') {
                                Alloy.Models.associate.loginAssociate({
                                    employee_id: Alloy.Models.associate.getEmployeeId(),
                                    passcode: Alloy.Models.associate.getPasscode()
                                }).done(function () {
                                    self.fetchToken(hostname).done(function () {
                                        deferred.resolve();
                                    });
                                }).fail(function () {
                                    deferred.reject();
                                });
                            } else {
                                var faultModel = new AuthorizationFault();
                                faultModel.set(fault.toJSON());
                                deferred.rejectWith(faultModel, [faultModel]);
                            }
                        } else {
                            deferred.reject();
                        }
                    });
                }
                return deferred.promise();
            },

            getToken: function () {
                return this.get('access_token');
            },

            resetToken: function () {
                this.timeout = null;
            }
        });

        return Model;
    }
};

model = Alloy.M('authorizationToken', exports.definition, []);

collection = Alloy.C('authorizationToken', exports.definition, model);

exports.Model = model;
exports.Collection = collection;