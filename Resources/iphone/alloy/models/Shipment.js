var Alloy = require('/alloy'),
    _ = require("/alloy/underscore")._,
    model,
    collection;

var price_adjustment = Backbone.Model.extend({});

exports.definition = {
    config: {
        model_name: 'shipment',
        adapter: {
            type: 'ocapi'
        }
    },

    extendModel: function (Model) {
        _.extend(Model.prototype, {

            relations: [{
                type: Backbone.One,
                key: 'shipping_address',
                relatedModel: require('alloy/models/' + ucfirst('shippingAddress')).Model
            }, {
                type: Backbone.One,
                key: 'shipping_method',
                relatedModel: require('alloy/models/' + ucfirst('shippingMethod')).Model
            }]
        });

        return Model;
    },
    extendCollection: function (Collection) {
        _.extend(Collection.prototype, {});

        return Collection;
    }
};

model = Alloy.M('shipment', exports.definition, []);

collection = Alloy.C('shipment', exports.definition, model);

exports.Model = model;
exports.Collection = collection;