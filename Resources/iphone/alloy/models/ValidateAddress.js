var Alloy = require('/alloy'),
    _ = require("/alloy/underscore")._,
    model,
    collection;

exports.definition = {
    config: {
        model_name: 'validateAddress',
        secure: true,
        cache: false,
        adapter: {
            type: 'storefront'
        }
    },

    extendModel: function (Model) {
        _.extend(Model.prototype, {

            urlRoot: '/EACheckout-AddressValidation'
        });

        return Model;
    }
};

model = Alloy.M('validateAddress', exports.definition, []);

collection = Alloy.C('validateAddress', exports.definition, model);

exports.Model = model;
exports.Collection = collection;