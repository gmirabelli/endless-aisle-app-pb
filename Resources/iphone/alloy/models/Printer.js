var Alloy = require('/alloy'),
    _ = require("/alloy/underscore")._,
    model,
    collection;

exports.definition = {

    config: {
        model_name: 'printer',
        secure: true,
        cache: false,
        adapter: {
            type: 'storefront'
        }
    },
    extendModel: function (Model) {
        _.extend(Model.prototype, {
            getIP: function () {
                return this.get('ip');
            }
        });

        return Model;
    },
    extendCollection: function (Collection) {
        _.extend(Collection.prototype, {});

        return Collection;
    }
};

model = Alloy.M('printer', exports.definition, []);

collection = Alloy.C('printer', exports.definition, model);

exports.Model = model;
exports.Collection = collection;