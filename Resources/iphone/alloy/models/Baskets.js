var Alloy = require('/alloy'),
    _ = require("/alloy/underscore")._,
    model,
    collection;

var logger = require('logging')('models:baskets', 'app/models/baskets');

function addSuccessCallback(options, basket, toRestore) {
    var success = options.success;
    var checkoutStatus = basket.getCheckoutStatus();
    var lastCheckoutStatus = basket.getLastCheckoutStatus();
    var enableCheckout = basket.canEnableCheckout();
    var shipToStore = basket.getShipToStore();
    var differentStorePickup = basket.getDifferentStorePickup();
    var differentStorePickupMessage = basket.getDifferentStorePickupMessage();

    options.success = function (model, resp, options) {
        if (model.etag) {
            basket.etag = model.etag;
        }
        basket.clear({
            silent: true
        });
        if (!model.set(basket.parse(resp, options), options)) {
            return false;
        }
        if (!basket.set(basket.parse(resp, options), options)) {
            return false;
        }
        basket.set('checkout_status', checkoutStatus, {
            silent: true
        });
        basket.set('last_checkout_status', lastCheckoutStatus, {
            silent: true
        });
        if (enableCheckout != undefined) {
            basket.set('enable_checkout', enableCheckout, {
                silent: true
            });
        }
        basket.set('ship_to_store', shipToStore, {
            silent: true
        });
        basket.set('different_store_pickup', differentStorePickup, {
            silent: true
        });
        basket.set('different_store_pickup_message', differentStorePickupMessage, {
            silent: true
        });
        if (toRestore) {
            _.each(toRestore, function (item) {
                basket.set(item, toRestore[item], {
                    silent: true
                });
            });
        }
        if (success) {
            success(model, resp, options);
        }
    };
}

var BasketModel = Backbone.Model.extend({
    config: {
        secure: true,
        cache: false
    },

    setModelName: function (name) {
        this.config.model_name = name;
    }
});

var StorefrontHelperSecure = require('dw/instore/StorefrontHelperSecure');

var BasketStorefrontModel = StorefrontHelperSecure.extend({
    setUrlRoot: function (urlRoot) {
        this.urlRoot = urlRoot;
    }
});

var BasketGiftCardBalance = BasketStorefrontModel.extend({
    urlRoot: '/EACheckout-GiftCardBalance',

    getBalance: function () {
        return this.get('gift_card_balance');
    },

    getMaskedCode: function () {
        return this.get('masked_gift_card_code');
    }
});

exports.definition = {
    config: {
        model_name: 'baskets',
        secure: true,
        cache: false,
        adapter: {
            type: 'ocapi'
        }
    },

    extendModel: function (Model) {
        _.extend(Model.prototype, {

            urlRoot: '/baskets',

            relations: [{
                type: Backbone.Many,
                key: 'product_items',
                relatedModel: require('alloy/models/' + ucfirst('productItem')).Model
            }, {
                type: Backbone.One,
                key: 'billing_address',
                relatedModel: require('alloy/models/' + ucfirst('billingAddress')).Model
            }, {
                type: Backbone.Many,
                key: 'shipments',
                relatedModel: require('alloy/models/' + ucfirst('shipment')).Model
            }, {
                type: Backbone.Many,
                key: 'coupon_items',
                relatedModel: require('alloy/models/' + ucfirst('couponItem')).Model
            }, {
                type: Backbone.Many,
                key: 'order_price_adjustments',
                relatedModel: require('alloy/models/' + ucfirst('orderPriceAdjustment')).Model
            }],

            idAttribute: 'basket_id',
            id: 'this',
            checkoutStates: [],

            syncOnEtagFailure: function (method) {
                var deferred = new _.Deferred();
                var self = this;
                method().fail(function (failModel, params, options) {
                    var fault = failModel ? failModel.get('fault') : null;

                    if (fault && fault.type && (fault.type === 'PreconditionFailedException' || fault.type === 'InvalidIfMatchException')) {
                        logger.info('Calling sync basket to resolve etag error');
                        self.fetchBasket(self.get('basket_id')).done(function () {
                            method().done(function (rspModel, params, options) {
                                deferred.resolveWith(rspModel, [rspModel, params, options]);
                            }).fail(function (failModel, params, options) {
                                deferred.rejectWith(failModel, [failModel, params, options]);
                            });
                        }).fail(function (failModel, params, options) {
                            deferred.rejectWith(failModel, [failModel, params, options]);
                        });
                    } else {
                        deferred.rejectWith(failModel, [failModel, params, options]);
                    }
                }).done(function (rspModel, params, options) {
                    deferred.resolveWith(rspModel, [rspModel, params, options]);
                });
                return deferred.promise();
            },

            save: function (attrs, options) {
                logger.info('save override called');
                var self = this;
                var method = function () {
                    return Backbone.Model.prototype.save.call(self, attrs, options);
                };
                return this.syncOnEtagFailure(method);
            },

            apiCallInt: function (model, params, options) {
                logger.info('apiCallInt override called');
                var self = this;
                var method = function () {
                    if (params && self.etag) {
                        if (params.headers) {
                            params.headers = _.extend(params.headers, {
                                'If-Match': self.etag
                            });
                        } else {
                            params = _.extend(params, {
                                headers: {
                                    'If-Match': self.etag
                                }
                            });
                        }
                    }
                    return self.apiCall(model, params, options);
                };
                return this.syncOnEtagFailure(method);
            },

            url: function () {
                var base = _.result(this, 'urlRoot') || _.result(this.collection, 'url') || urlError();
                if (this.isNew()) {
                    return base;
                }
                return base + (base.charAt(base.length - 1) === '/' ? '' : '/') + 'this';
            },

            standardHeaders: function () {
                return {
                    'content-type': 'application/json',
                    Authorization: 'Bearer ' + Alloy.Models.authorizationToken.getToken(),
                    'If-Match': this.etag
                };
            },

            standardOptions: function (options) {
                return _.extend({}, {
                    wait: true,
                    cache: false,
                    dontincludeid: true,
                    error: function () {}
                }, options);
            },

            getBasket: function (basket, customData, options) {
                var deferred = new _.Deferred();
                var self = this;
                basket = basket || {};
                var url = this.url().replace('baskets/this', 'baskets');
                basket = _.extend(basket, customData);
                Alloy.Models.authorizationToken.fetchToken(Alloy.CFG.storefront_host).done(function () {
                    var params = _.extend({}, {
                        url: url,
                        type: 'POST',
                        data: JSON.stringify(basket),
                        headers: {
                            'content-type': 'application/json',
                            Authorization: 'Bearer ' + Alloy.Models.authorizationToken.getToken()
                        }
                    });
                    options = self.standardOptions(options);
                    addSuccessCallback(options, self);
                    var getBasket = new BasketModel();
                    getBasket.setModelName('getBasket');
                    self.apiCall(getBasket, params, options).done(function () {
                        if (Alloy.CFG.siteCurrency != Alloy.CFG.appCurrency) {
                            self.updateCurrency({
                                currency: Alloy.CFG.appCurrency
                            }, {
                                silent: true
                            }).done(function () {
                                deferred.resolve();
                            }).fail(function () {
                                deferred.reject();
                            });
                        } else {
                            deferred.resolve();
                        }
                    }).fail(function () {
                        deferred.reject();
                    });
                });
                return deferred.promise();
            },
            getCurrencyCode: function () {
                return this.get('currency');
            },

            fetchBasket: function (id) {
                var self = this;
                var deferred = new _.Deferred();
                var url = this.url().replace('baskets/this', 'baskets/' + id);
                Alloy.Models.authorizationToken.fetchToken(Alloy.CFG.storefront_host).done(function () {
                    var params = _.extend({}, {
                        url: url,
                        type: 'GET',
                        headers: {
                            'content-type': 'application/json',
                            Authorization: 'Bearer ' + Alloy.Models.authorizationToken.getToken()
                        }
                    });
                    var options = self.standardOptions();
                    addSuccessCallback(options, self);
                    var fetchBasket = new BasketModel();
                    fetchBasket.setModelName('fetchBasket');
                    self.apiCall(fetchBasket, params, options).done(function () {
                        deferred.resolve();
                    }).fail(function () {
                        deferred.reject();
                    });
                }).fail(function () {
                    deferred.reject();
                });
                return deferred.promise();
            },

            deleteBasket: function () {
                var self = this;
                var deferred = new _.Deferred();
                var url = this.url().replace('baskets/this', 'baskets/' + this.get('basket_id'));
                Alloy.Models.authorizationToken.fetchToken(Alloy.CFG.storefront_host).done(function () {
                    var params = _.extend({}, {
                        url: url,
                        type: 'DELETE',
                        headers: self.standardHeaders()
                    });
                    var options = self.standardOptions();
                    addSuccessCallback(options, self);
                    var deleteBasket = new BasketModel();
                    deleteBasket.setModelName('deleteBasket');
                    var deleteBasketPromise = self.apiCallInt(deleteBasket, params, options);
                    deleteBasketPromise.done(function () {
                        deferred.resolve();
                        self.trigger('sync');
                    }).fail(function () {
                        deferred.reject();
                    });
                }).fail(function () {
                    deferred.reject();
                });
                return deferred.promise();
            },

            setBasket: function (basket_info, options) {
                var self = this;
                var deferred = new _.Deferred();
                var url = this.url().replace('baskets/this', 'baskets/' + this.get('basket_id'));
                Alloy.Models.authorizationToken.fetchToken(Alloy.CFG.storefront_host).done(function () {
                    var params = _.extend({}, {
                        url: url,
                        type: 'PATCH',
                        data: JSON.stringify(basket_info),
                        headers: self.standardHeaders()
                    });
                    options = self.standardOptions(options);
                    addSuccessCallback(options, self, options.toRestore ? options.toRestore : {
                        shipments: self.get('shipments'),
                        billing_address: self.getBillingAddress()
                    });
                    var setBasket = new BasketModel();
                    setBasket.setModelName('setBasket');
                    self.apiCallInt(setBasket, params, options).done(function () {
                        deferred.resolve();
                    }).fail(function () {
                        deferred.reject();
                    });
                }).fail(function () {
                    deferred.reject();
                });
                return deferred.promise();
            },

            replaceBasket: function (basket_info, customData, options) {
                var self = this;
                var deferred = new _.Deferred();
                options = options || {};
                var checkoutStatus = this.getCheckoutStatus();
                var lastCheckoutStatus = this.getLastCheckoutStatus();
                var basketUpdate = {
                    type: 'basket_replace',
                    data: {
                        product_items: basket_info.product_items || [],
                        shipments: basket_info.shipments || []
                    }
                };
                _.extend(basketUpdate.data, customData);
                var basketPatch = {
                    product_items: new Backbone.Collection([]),
                    coupon_items: basket_info.coupon_items,
                    c_patchInfo: JSON.stringify(basketUpdate)
                };
                _.extend(basketPatch, customData);
                this.setBasket(basketPatch, options).done(function (model, params, resultOptions) {
                    self.set('checkout_status', checkoutStatus);
                    self.set('last_checkout_status', lastCheckoutStatus, {
                        silent: true
                    });
                    deferred.resolveWith(model, [model, params, resultOptions]);
                    if (options && !options.silent) {
                        self.trigger('basket_sync');
                    }
                }).fail(function (model, params, options) {
                    deferred.rejectWith(model, [model, params, options]);
                });
                return deferred.promise();
            },

            addProduct: function (product_info, customData, options) {
                _.extend(product_info, customData);
                var products = [];
                products.push(product_info);
                return this.addProducts(products, options);
            },

            addProducts: function (products, options) {
                var self = this;
                var deferred = new _.Deferred();
                var addProduct = new BasketModel(products);
                addProduct.setModelName('addProduct');

                var url = this.url().replace('baskets/this', 'baskets/' + this.get('basket_id') + '/items');
                Alloy.Models.authorizationToken.fetchToken(Alloy.CFG.storefront_host).done(function () {
                    var params = _.extend({}, {
                        url: url,
                        type: 'POST',
                        data: JSON.stringify(products),
                        headers: self.standardHeaders()
                    });
                    options = self.standardOptions(options);
                    addSuccessCallback(options, self, {
                        shipments: self.get('shipments'),
                        billing_address: self.getBillingAddress()
                    });
                    var addProductPromise = self.apiCallInt(addProduct, params, options);
                    addProductPromise.done(function (model) {
                        if (addProduct.etag) {
                            deferred.resolveWith(model, [model, params, options]);
                            self.trigger('change:product_items');
                            self.trigger('basket_sync');
                        }
                    }).fail(function (model, params, options) {
                        deferred.rejectWith(model, [model, params, options]);
                    });
                }).fail(function () {
                    deferred.reject();
                });

                return deferred.promise();
            },

            validateCartForCheckout: function (customData, options) {
                var deferred = new _.Deferred();
                var self = this;

                var basketUpdate = {
                    type: 'validate_cart_for_checkout'
                };
                var basketPatch = {
                    c_patchInfo: JSON.stringify(basketUpdate)
                };
                _.extend(basketPatch, customData);

                self.setBasket(basketPatch, options).done(function (model, params, resultOptions) {
                    self.trigger('basket_sync');
                    deferred.resolveWith(model, [model, params, resultOptions]);
                }).fail(function () {
                    deferred.reject();
                });

                return deferred.promise();
            },

            replaceProduct: function (product_info, item_id, customData, options) {
                var self = this;
                _.extend(product_info, customData);
                var deferred = new _.Deferred();

                var replaceProduct = new BasketModel(product_info);
                replaceProduct.setModelName('replaceProduct');

                var url = this.url().replace('baskets/this', 'baskets/' + this.get('basket_id') + '/items/' + item_id);
                Alloy.Models.authorizationToken.fetchToken(Alloy.CFG.storefront_host).done(function () {
                    var params = _.extend({}, {
                        url: url,
                        type: 'PATCH',
                        data: JSON.stringify(product_info),
                        headers: self.standardHeaders()
                    });
                    options = self.standardOptions(options);
                    addSuccessCallback(options, self, {
                        shipments: self.get('shipments'),
                        billing_address: self.getBillingAddress()
                    });

                    var checkoutStatus = self.getCheckoutStatus();
                    var lastCheckoutStatus = self.getLastCheckoutStatus();

                    var replaceProductPromise = self.apiCallInt(replaceProduct, params, options);

                    replaceProductPromise.done(function (model) {
                        self.set('checkout_status', checkoutStatus);
                        self.set('last_checkout_status', lastCheckoutStatus, {
                            silent: true
                        });
                        deferred.resolveWith(model, [model, params, options]);
                        self.trigger('basket_sync');
                        self.trigger('change:checkout_status');
                    }).fail(function (model, params, options) {
                        deferred.rejectWith(model, [model, params, options]);
                    });
                }).fail(function () {
                    deferred.reject();
                });
                return deferred.promise();
            },

            removeItem: function (item_id, options) {
                var self = this;
                var deferred = new _.Deferred();
                var removeItem = new BasketModel();
                removeItem.setModelName('removeItem');

                var url = this.url().replace('baskets/this', 'baskets/' + this.get('basket_id') + '/items/' + item_id);
                Alloy.Models.authorizationToken.fetchToken(Alloy.CFG.storefront_host).done(function () {
                    var params = _.extend({}, {
                        url: url,
                        type: 'DELETE',
                        headers: self.standardHeaders()
                    });
                    options = self.standardOptions(options);
                    addSuccessCallback(options, self, {
                        shipments: self.get('shipments'),
                        billing_address: self.getBillingAddress()
                    });

                    var checkoutStatus = self.getCheckoutStatus();
                    var lastCheckoutStatus = self.getLastCheckoutStatus();
                    var removeItemPromise = self.apiCallInt(removeItem, params, options);

                    removeItemPromise.done(function (model) {
                        self.set('checkout_status', checkoutStatus);
                        self.set('last_checkout_status', lastCheckoutStatus, {
                            silent: true
                        });
                        deferred.resolveWith(model, [model, params, options]);
                        self.trigger('basket_sync');
                        self.trigger('change:checkout_status');
                    }).fail(function (model, params, options) {
                        deferred.rejectWith(model, [model, params, options]);
                    });
                }).fail(function () {
                    deferred.reject();
                });
                return deferred.promise();
            },

            setProductPriceOverride: function (override_info, customData, origOptions) {
                var self = this;
                var options = _.extend({}, {
                    wait: true,
                    silent: true
                }, origOptions);
                origOptions = origOptions || {};
                var deferred = new _.Deferred();
                var basketUpdate = {
                    type: 'product_price_override',
                    data: override_info
                };
                var basketPatch = {
                    product_items: new Backbone.Collection(),
                    c_patchInfo: JSON.stringify(basketUpdate)
                };
                _.extend(basketPatch, customData);

                this.setBasket(basketPatch, options).done(function (model, params, updateOptions) {
                    deferred.resolveWith(model, [model, params, updateOptions]);
                    if (!origOptions.silent) {
                        self.trigger('basket_sync');
                    }
                }).fail(function (model, params, options) {
                    deferred.rejectWith(model, [model, params, options]);
                });

                return deferred.promise();
            },

            setShippingMethod: function (method_info, patch, customData, origOptions) {
                var self = this;
                if (patch) {
                    method_info.c_patchInfo = patch;
                }
                _.extend(method_info, customData);

                var deferred = new _.Deferred();

                var shippingMethod = new BasketModel(method_info);
                shippingMethod.setModelName('basketShippingMethod');

                var url = this.url().replace('baskets/this', 'baskets/' + this.get('basket_id') + '/shipments/me/shipping_method');
                Alloy.Models.authorizationToken.fetchToken(Alloy.CFG.storefront_host).done(function () {
                    var params = _.extend({}, {
                        url: url,
                        type: 'PUT',
                        data: JSON.stringify(shippingMethod.toJSON()),
                        headers: self.standardHeaders()
                    });
                    var options = self.standardOptions(origOptions);
                    options.silent = true;
                    addSuccessCallback(options, self);
                    var shippingMethodPromise = self.apiCallInt(shippingMethod, params, options);

                    shippingMethodPromise.done(function (model, params, options) {
                        if (shippingMethod.etag) {
                            deferred.resolveWith(model, [model, params, options]);
                            if (!origOptions || !origOptions.silent) {
                                self.trigger('basket_sync');
                                self.trigger('change:shipments');
                            }
                        }
                    }).fail(function (model, params, options) {
                        deferred.rejectWith(model, [model, params, options]);
                    });
                }).fail(function () {
                    deferred.reject();
                });

                return deferred.promise();
            },

            updateShipment: function (shipment_info, origOptions) {
                var self = this;

                var shippingMethod = new BasketModel(shipment_info);
                shippingMethod.setModelName('updateShipment');

                var deferred = new _.Deferred();

                var url = this.url().replace('baskets/this', 'baskets/' + this.get('basket_id') + '/shipments/me');

                Alloy.Models.authorizationToken.fetchToken(Alloy.CFG.storefront_host).done(function () {
                    var params = _.extend({}, {
                        url: url,
                        type: 'PATCH',
                        data: JSON.stringify(shippingMethod.toJSON()),
                        headers: self.standardHeaders()
                    });
                    var options = self.standardOptions(origOptions);
                    options.silent = true;
                    addSuccessCallback(options, self);
                    var shippingMethodPromise = self.apiCallInt(shippingMethod, params, options);

                    shippingMethodPromise.done(function (model, params, options) {
                        if (shippingMethod.etag) {
                            deferred.resolveWith(model, [model, params, options]);
                            if (!origOptions || !origOptions.silent) {
                                self.trigger('basket_sync');
                                self.trigger('change:shipments');
                            }
                        }
                    }).fail(function (model, params, options) {
                        deferred.rejectWith(model, [model, params, options]);
                    });
                }).fail(function () {
                    deferred.reject();
                });
                return deferred.promise();
            },

            setShippingPriceOverride: function (override_info, customData, origOptions) {
                var basketUpdate = {
                    type: 'shipping_price_override',
                    data: override_info
                };

                return this.setShippingMethod({
                    id: this.getShippingMethod().get('id')
                }, basketUpdate, customData, origOptions);
            },

            getShippingMethods: function (customData, options) {
                var self = this;
                var deferred = new _.Deferred();

                var basketUpdate = {
                    type: 'get_shipping_method_list'
                };

                options = options || {};
                options.silent = true;
                var basketPatch = {
                    c_patchInfo: JSON.stringify(basketUpdate)
                };

                _.extend(basketPatch, customData);
                self.setBasket(basketPatch, options).done(function () {
                    var shippingMethods = Alloy.createCollection('availableShippingMethod', self.get('shipping_methods'));
                    self.shippingMethods = shippingMethods.models;
                    self.trigger('basket_shipping_methods');
                    deferred.resolve();
                });

                return deferred.promise();
            },

            updateCurrency: function (basketInfo, options) {
                var self = this;
                var deferred = new _.Deferred();
                self.setBasket(basketInfo, options).done(function () {
                    self.trigger('basket_sync');
                    deferred.resolve();
                }).fail(function () {
                    deferred.reject();
                });

                return deferred.promise();
            },

            getShippingMethodPrice: function (shipping_method_id) {
                if (this.shippingMethods) {
                    var methods = this.shippingMethods || [];
                    for (var m = 0; m < methods.length; ++m) {
                        method = methods[m];
                        if (method.getID() === shipping_method_id) {
                            var price = parseFloat(method.getBasePrice());
                            if (method.getSurcharge()) {
                                price += parseFloat(method.getSurcharge());
                            }
                            return price;
                        }
                    }
                }
                return 0;
            },

            getShippingMethodBasePrice: function (shipping_method_id) {
                if (this.shippingMethods) {
                    var methods = this.shippingMethods || [];
                    for (var m = 0; m < methods.length; ++m) {
                        method = methods[m];
                        if (method.getID() === shipping_method_id) {
                            return parseFloat(method.getBasePrice());
                        }
                    }
                }
                return 0;
            },

            setShippingAddress: function (address_info, verify, customData, options) {
                var self = this;
                var deferred = new _.Deferred();

                if (!verify) {
                    address_info.c_skipVerify = true;
                }
                _.extend(address_info, customData);

                var shippingAddress = new BasketModel(address_info);
                shippingAddress.setModelName('basketShipping');

                var url = this.url().replace('baskets/this', 'baskets/' + this.get('basket_id') + '/shipments/me/shipping_address');
                Alloy.Models.authorizationToken.fetchToken(Alloy.CFG.storefront_host).done(function () {
                    var params = _.extend({}, {
                        url: url,
                        type: 'PUT',
                        data: JSON.stringify(shippingAddress.toJSON()),
                        headers: self.standardHeaders()
                    });
                    options = self.standardOptions(options);
                    options.silent = true;
                    addSuccessCallback(options, self);
                    var shippingAddressPromise = self.apiCallInt(shippingAddress, params, options);

                    shippingAddressPromise.done(function (model, params, options) {
                        if (shippingAddress.etag) {
                            deferred.resolveWith(model, [model, params, options]);
                            self.trigger('basket_sync');
                            self.trigger('change:shipments');
                        }
                    }).fail(function (model, params, options) {
                        deferred.rejectWith(model, [model, params, options]);
                    });
                }).fail(function () {
                    deferred.reject();
                });

                return deferred.promise();
            },

            setBillingAddress: function (billing_address, verify, customData, options) {
                var self = this;
                var deferred = new _.Deferred();
                billing_address = billing_address.toJSON ? billing_address.toJSON() : billing_address;
                var address = this.get('billing_address');
                if (address && address.first_name == billing_address.first_name && address.last_name == billing_address.last_name && address.address1 == billing_address.address1 && address.address2 == billing_address.address2 && address.city == billing_address.city && address.state_code == billing_address.state_code && address.country_code == billing_address.country_code && address.postal_code == billing_address.postal_code) {
                    deferred.resolve();
                } else {
                    if (!verify) {
                        billing_address.c_skipVerify = true;
                    }

                    _.extend(billing_address, customData);

                    var billingAddress = new BasketModel(billing_address);
                    billingAddress.setModelName('basketBilling');

                    var url = this.url().replace('baskets/this', 'baskets/' + this.get('basket_id') + '/billing_address');
                    Alloy.Models.authorizationToken.fetchToken(Alloy.CFG.storefront_host).done(function () {
                        var params = _.extend({}, {
                            url: url,
                            type: 'PUT',
                            data: JSON.stringify(billingAddress.toJSON()),
                            headers: self.standardHeaders()
                        });
                        options = self.standardOptions(options);
                        addSuccessCallback(options, self);

                        var billingAddressPromise = self.apiCallInt(billingAddress, params, options);

                        billingAddressPromise.done(function (model, params, options) {
                            if (billingAddress.etag) {
                                deferred.resolveWith(model, [model, params, options]);
                                self.trigger('basket_sync');
                            }
                        }).fail(function (model, params, options) {
                            deferred.rejectWith(model, [model, params, options]);
                        });
                    }).fail(function () {
                        deferred.reject();
                    });
                }

                return deferred.promise();
            },

            setCustomerInfo: function (customer_info, options) {
                var self = this;
                var deferred = new _.Deferred();

                var customerInfo = new BasketModel(customer_info);
                customerInfo.setModelName('basketCustomerInfo');

                customerInfo.etag = this.etag;

                var url = self.url().replace('baskets/this', 'baskets/' + this.get('basket_id') + '/customer');
                Alloy.Models.authorizationToken.fetchToken(Alloy.CFG.storefront_host).done(function () {
                    var params = _.extend({}, {
                        url: url,
                        type: 'PUT',
                        data: JSON.stringify(customerInfo.toJSON()),
                        headers: self.standardHeaders()
                    });
                    options = self.standardOptions(options);
                    addSuccessCallback(options, self);

                    var customerInfoPromise = self.apiCallInt(customerInfo, params, options);

                    customerInfoPromise.done(function (model, params, options) {
                        if (customerInfo.etag) {
                            deferred.resolveWith(model, [model, params, options]);
                        }
                    }).fail(function (model, params, options) {
                        deferred.rejectWith(model, [model, params, options]);
                    });
                }).fail(function () {
                    deferred.reject();
                });

                return deferred.promise();
            },

            setShippingAddressAndEmail: function (address_info, customer_info, verify, customData, options) {
                var self = this;
                var deferred = new _.Deferred();
                address_info = address_info.toJSON ? address_info.toJSON() : address_info;

                if (this.has('customer_info')) {
                    customer_info = _.extend(this.get('customer_info'), customer_info);
                }
                var customerInfo = new BasketModel(customer_info);
                customerInfo.setModelName('basketCustomerInfo');

                customerInfo.etag = self.etag;
                var customerInfoUrl = self.url().replace('baskets/this', 'baskets/' + this.get('basket_id') + '/customer');
                Alloy.Models.authorizationToken.fetchToken(Alloy.CFG.storefront_host).done(function () {
                    var customerInfoParams = _.extend({}, {
                        url: customerInfoUrl,
                        type: 'PUT',
                        data: JSON.stringify(customerInfo.toJSON()),
                        headers: self.standardHeaders()
                    });

                    options = self.standardOptions(options);
                    addSuccessCallback(options, self);
                    var customerInfoPromise = self.apiCallInt(customerInfo, customerInfoParams, options);

                    customerInfoPromise.done(function () {
                        if (customerInfo.etag) {
                            self.etag = customerInfo.etag;

                            self.set(customerInfo.toJSON(), {
                                silent: true
                            });
                            self.setShippingAddress(address_info, verify, customData, options).done(function (model, params, options) {
                                if (model.etag) {
                                    deferred.resolveWith(model, [model, params, options]);
                                    self.trigger('change:customer_info');
                                }
                            }).fail(function (model, params, options) {
                                deferred.rejectWith(model, [model, params, options]);
                            });
                        }
                    }).fail(function (model, params, options) {
                        deferred.rejectWith(model, [model, params, options]);
                    });
                }).fail(function () {
                    deferred.reject();
                });
                return deferred.promise();
            },

            createOrder: function (options) {
                var self = this;
                var deferred = new _.Deferred();
                var submitBasket = new BasketModel();
                submitBasket.setModelName('submitBasket');

                var url = this.url().replace('baskets/this', 'orders');

                this.customer_no = this.get('customer_info').customer_no;
                Alloy.Models.authorizationToken.fetchToken(Alloy.CFG.storefront_host).done(function () {
                    var params = _.extend({}, {
                        url: url,
                        type: 'POST',
                        data: JSON.stringify({
                            basket_id: self.get('basket_id')
                        }),
                        headers: self.standardHeaders()
                    });
                    options = self.standardOptions(options);
                    addSuccessCallback(options, self, {
                        shipments: self.get('shipments'),
                        billing_address: self.getBillingAddress()
                    });

                    var submitBasketPromise = self.apiCallInt(submitBasket, params, options);

                    submitBasketPromise.done(function (model) {
                        if (submitBasket.etag) {
                            deferred.resolveWith(model, [model, params, options]);
                            self.trigger('change:product_items');
                            self.trigger('basket_sync');
                        }
                    }).fail(function (model, params, options) {
                        var basket = Alloy.createModel('baskets', model.toJSON());
                        deferred.rejectWith(basket, [basket, params, options]);
                    });
                }).fail(function () {
                    deferred.reject();
                });

                return deferred.promise();
            },

            applyCreditCard: function (credit_card_data) {
                var self = this;

                var applyCreditCard = new BasketStorefrontModel();
                applyCreditCard.setUrlRoot('/EACheckout-ApplyCreditCard');
                applyCreditCard.setModelName('applyCreditCard');
                var savePromise = applyCreditCard.save(credit_card_data, {
                    silent: true
                });
                savePromise.done(function (model) {
                    self.mergePaymentInformation(model);
                });
                return savePromise;
            },

            removeCreditCard: function (credit_card_data) {
                var self = this;

                var removeCreditCard = new BasketStorefrontModel();
                removeCreditCard.setUrlRoot('/EACheckout-RemoveCreditCard');
                removeCreditCard.setModelName('removeCreditCard');
                var savePromise = removeCreditCard.save(credit_card_data, {
                    silent: true
                });
                savePromise.done(function (model) {
                    self.mergePaymentInformation(model);
                });
                return savePromise;
            },

            authorizeCreditCard: function (credit_card_data) {
                var self = this;

                var authorizeCreditCard = new BasketStorefrontModel();
                authorizeCreditCard.setUrlRoot('/EACheckout-AuthorizeCreditCard');
                authorizeCreditCard.setModelName('authorizeCreditCard');
                var savePromise = authorizeCreditCard.save(credit_card_data, {
                    silent: true
                });
                savePromise.done(function (model) {
                    self.mergePaymentInformation(model);
                });
                return savePromise;
            },

            giftCardBalance: function (gift_card_data) {
                var giftCardBalance = new BasketGiftCardBalance();
                giftCardBalance.setModelName('giftCardBalance');
                var savePromise = giftCardBalance.save(gift_card_data, {
                    silent: true
                });
                return savePromise;
            },

            applyGiftCard: function (gift_card_data) {
                var self = this;

                var applyGiftCard = new BasketStorefrontModel();
                applyGiftCard.setUrlRoot('/EACheckout-ApplyGiftCard');
                applyGiftCard.setModelName('applyGiftCard');
                var savePromise = applyGiftCard.save(gift_card_data, {
                    silent: true
                });
                savePromise.done(function (model) {
                    self.mergePaymentInformation(model);
                });
                return savePromise;
            },

            removeGiftCard: function (gift_card_data) {
                var self = this;

                var removeGiftCard = new BasketStorefrontModel();
                removeGiftCard.setUrlRoot('/EACheckout-RemoveGiftCard');
                removeGiftCard.setModelName('removeGiftCard');
                var savePromise = removeGiftCard.save(gift_card_data, {
                    silent: true
                });
                savePromise.done(function (model) {
                    self.mergePaymentInformation(model);
                });
                return savePromise;
            },

            authorizeGiftCard: function (gift_card_data) {
                var self = this;

                var authorizeGiftCard = new BasketStorefrontModel();
                authorizeGiftCard.setUrlRoot('/EACheckout-AuthorizeGiftCard');
                authorizeGiftCard.setModelName('authorizeGiftCard');
                var savePromise = authorizeGiftCard.save(gift_card_data, {
                    silent: true
                });
                savePromise.done(function (model) {
                    self.mergePaymentInformation(model);
                });
                return savePromise;
            },

            authorizePayment: function () {
                var self = this;

                var authorizePayment = new BasketStorefrontModel();
                authorizePayment.setUrlRoot('/EACheckout-AuthorizePayment');
                authorizePayment.setModelName('authorizePayment');
                var savePromise = authorizePayment.save({
                    order_no: this.get('order_no')
                }, {
                    silent: true
                });
                savePromise.done(function (model) {
                    self.mergePaymentInformation(model);
                });
                return savePromise;
            },

            addCoupon: function (coupon_code, options) {
                var self = this;
                var deferred = new _.Deferred();
                var addCoupon = new BasketModel({
                    code: coupon_code
                });
                addCoupon.setModelName('addCoupon');

                var url = this.url().replace('baskets/this', 'baskets/' + this.get('basket_id') + '/coupons');
                Alloy.Models.authorizationToken.fetchToken(Alloy.CFG.storefront_host).done(function () {
                    var params = _.extend({}, {
                        url: url,
                        type: 'POST',
                        data: JSON.stringify({
                            code: coupon_code
                        }),
                        headers: self.standardHeaders()
                    });
                    options = self.standardOptions(options);
                    addSuccessCallback(options, self, {
                        shipments: self.get('shipments'),
                        billing_address: self.getBillingAddress()
                    });

                    var addCouponPromise = self.apiCallInt(addCoupon, params, options);

                    addCouponPromise.done(function (model) {
                        if (addCoupon.etag) {
                            deferred.resolveWith(model, [model, params, options]);
                            self.trigger('basket_sync');
                        }
                    }).fail(function (model, params, options) {
                        var m = Alloy.createModel('baskets', model.toJSON());
                        deferred.rejectWith(m, [m, params, options]);
                    });
                }).fail(function () {
                    deferred.reject();
                });
                return deferred.promise();
            },

            removeCoupon: function (coupon_code, options) {
                var self = this;
                var deferred = new _.Deferred();
                var foundCoupon = _.find(self.get('coupon_items').models, function (coupon) {
                    return coupon.get('code') === coupon_code;
                });

                var removeCoupon = new BasketModel({
                    code: coupon_code
                });
                removeCoupon.setModelName('removeCoupon');

                var url = this.url().replace('baskets/this', 'baskets/' + this.get('basket_id') + '/coupons/' + foundCoupon.get('coupon_item_id'));
                Alloy.Models.authorizationToken.fetchToken(Alloy.CFG.storefront_host).done(function () {
                    var params = _.extend({}, {
                        url: url,
                        type: 'DELETE',
                        headers: self.standardHeaders()
                    });
                    options = self.standardOptions(options);
                    addSuccessCallback(options, self, {
                        shipments: self.get('shipments'),
                        billing_address: self.getBillingAddress()
                    });

                    var removeCouponPromise = self.apiCallInt(removeCoupon, params, options);

                    removeCouponPromise.done(function (model) {
                        if (removeCoupon.etag) {
                            deferred.resolveWith(model, [model, params, options]);
                            self.trigger('basket_sync');
                            self.trigger('change:coupon_items');
                        }
                    }).fail(function (model, params, options) {
                        deferred.rejectWith(model, [model, params, options]);
                    });
                }).fail(function () {
                    deferred.reject();
                });
                return deferred.promise();
            },

            simpleAbandonOrder: function () {
                var abandonOrder = new BasketStorefrontModel();
                abandonOrder.setUrlRoot('/EACheckout-AbandonOrder');
                abandonOrder.setModelName('abandonOrder');
                return abandonOrder.save({
                    order_no: this.get('order_no')
                }, {
                    silent: true
                });
            },

            abandonOrder: function (employeeId, employeePasscode, storeId) {
                var self = this;

                var deferred = new _.Deferred();
                var savePromise = this.simpleAbandonOrder();
                var basket = Alloy.createModel('baskets');
                basket.set('product_items', self.get('product_items'), {
                    silent: true
                });
                _.each(basket.get('product_items').models, function (product_item) {
                    product_item.unset('price_override');
                    product_item.unset('price_override_value');
                    product_item.unset('price_override_type');
                    product_item.unset('price_override_reason_code');
                    product_item.unset('manager_employee_id');
                    product_item.unset('base_price_override');
                    product_item.unset('thumbnailUrl');
                    product_item.unset('message');
                });
                basket.set('billing_address', self.get('billing_address'), {
                    silent: true
                });
                var customer_info = self.get('customer_info');
                if (!self.customer_no) {
                    delete customer_info.customer_no;
                }
                basket.set('customer_info', customer_info, {
                    silent: true
                });
                basket.set('coupon_items', self.get('coupon_items'), {
                    silent: true
                });
                var shipments = self.get('shipments');
                var giftMessage = null;
                if (shipments && shipments.at(0) && shipments.at(0).get('gift')) {
                    giftMessage = {
                        is_gift: 'true',
                        message: shipments.at(0).get('gift_message')
                    };
                }
                var shippingMethod = shipments && shipments.at(0) ? shipments.at(0).get('shipping_method') : '';
                var shippingOverride = null;
                if (self.hasShippingPriceOverride()) {
                    shippingOverride = {
                        price_override: shippingMethod.get('price_overrride'),
                        price_override_type: shippingMethod.get('price_override_type'),
                        price_override_value: shippingMethod.get('price_override_value'),
                        price_override_reason_code: shippingMethod.get('price_override_reason_code'),
                        base_price: shippingMethod.get('base_price'),
                        base_price_override: shippingMethod.get('base_price_override'),
                        employee_id: employeeId,
                        employee_passcode: employeePasscode,
                        store_id: storeId
                    };
                }
                shippingMethod.unset('price_override');
                shippingMethod.unset('price_override_type');
                shippingMethod.unset('price_override_value');
                shippingMethod.unset('price_override_reason_code');
                shippingMethod.unset('base_price');
                shippingMethod.unset('base_price_override');
                shippingMethod.unset('manager_employee_id');
                var customer_info = this.get('customer_info');
                savePromise.always(function () {
                    self.unset('channel_type');
                    self.getBasket(basket, {
                        c_eaEmployeeId: employeeId
                    }, {
                        silent: true
                    }).done(function () {
                        var basketUpdate = {
                            type: 'after_abandon_order',
                            data: {}
                        };
                        if (shippingOverride) {
                            basketUpdate.data.shipping_override = shippingOverride;
                        }

                        if (giftMessage) {
                            basketUpdate.data.gift_message = giftMessage;
                        }
                        if (Alloy.CFG.appCurrency) {
                            basketUpdate.data.currency = Alloy.CFG.appCurrency;
                        }
                        basketUpdate.data.shipments = shipments;
                        self.setBasket({
                            c_patchInfo: JSON.stringify(basketUpdate),
                            c_employee_id: employeeId
                        }, {
                            toRestore: {}
                        }).done(function (model, params, resultOptions) {
                            self.setCustomerInfo(customer_info).done(function () {
                                self.trigger('basket_sync');
                                deferred.resolveWith(model, [model, params, resultOptions]);
                            });
                        }).fail(function () {
                            deferred.reject();
                        });
                    }).fail(function () {
                        deferred.reject();
                    });
                });
                return deferred.promise();
            },

            emailOrder: function (order_no) {
                var emailOrder = new BasketStorefrontModel();
                emailOrder.setUrlRoot('/EAOrder-SendEmail');
                emailOrder.setModelName('emailOrder');
                var savePromise = emailOrder.save({
                    order_no: order_no
                }, {
                    silent: true
                });
                return savePromise;
            },

            mergePaymentInformation: function (model) {
                this.set('payment_details', model.get('payment_details'));
                this.set('confirmation_status', model.get('confirmation_status'));
                this.set('payment_balance', model.get('payment_balance'));
            },

            calculateBalanceDue: function () {
                var balance = 0;
                if (this.has('order_total')) {
                    var balance = this.get('order_total') - 0;
                    if (this.get('payment_details')) {
                        var details = this.get('payment_details');
                        var paymentsApplied = 0;
                        _.each(details, function (detail) {
                            paymentsApplied += detail.amt_auth - 0;
                        });
                        balance -= paymentsApplied;
                    }
                }
                return balance;
            },

            getOrder: function (data, params, options) {
                var self = this;
                var deferred = new _.Deferred();
                Alloy.Models.authorizationToken.fetchToken(Alloy.CFG.storefront_host).done(function () {
                    self.authorization = 'Bearer ' + Alloy.Models.authorizationToken.getToken();
                    self.url = function () {
                        return '/orders/' + data.order_no;
                    };
                    self.fetch({
                        dontincludeid: true
                    }).done(function (model) {
                        deferred.resolveWith(model, [model, params, options]);
                    }).fail(function () {
                        deferred.reject();
                    });
                }).fail(function () {
                    deferred.reject();
                });
                return deferred.promise();
            },

            transform: function () {
                var shippingMethod = this.get('shipments') ? this.get('shipments').at(0).get('shipping_method') : null;
                var shippingCost = shippingMethod ? shippingMethod.get('base_price') || shippingMethod.get('price') : null;

                var shippingDiscount = shippingMethod ? this.get('shipping_total') - shippingCost : null;

                if (shippingDiscount && shippingDiscount == 0) {
                    shippingDiscount = null;
                }

                var orderDiscount = this.get('product_total') ? this.get('product_sub_total') - this.get('product_total') : null;
                if (orderDiscount && orderDiscount == 0) {
                    orderDiscount = null;
                }
                var toCurrency = require('EAUtils').toCurrency;

                var moment = require('alloy/moment');
                var toReturn = {
                    product_sub_total: this.get('product_sub_total') == 0 ? '' : toCurrency(this.get('product_sub_total')),
                    order_discount: orderDiscount ? toCurrency(orderDiscount) : '----',
                    shipping_total: shippingCost ? toCurrency(shippingCost - 0) : '----',
                    shipping_discount: shippingDiscount ? toCurrency(shippingDiscount) : '----',
                    tax_total: this.get('tax_total') ? toCurrency(this.get('tax_total') - 0) : '----',
                    order_total: this.get('order_total') ? toCurrency(this.get('order_total')) : '----',
                    creation_date: moment(this.get('creation_date')).format('LL'),
                    status: this.get('c_eaStatus'),
                    order_no: this.get('order_no')
                };
                return toReturn;
            },

            getCouponDescription: function (coupon_code) {
                var coupons = this.get('coupons');
                if (coupons) {
                    for (var i = 0; i < coupons.length; i++) {
                        var coupon = coupons[i];
                        if (coupon_code == coupon.coupon_code) {
                            var description = null;
                            if (!coupon.coupon_applied) {
                                description = _L('Not Applied');
                            } else if (coupon.coupon_price_adjustments && coupon.coupon_price_adjustments.length > 0) {
                                description = coupon.coupon_price_adjustments[0].item_text;
                            }
                            return description;
                        }
                    }
                }
                return null;
            },

            getShipments: function () {
                return this.get('shipments') || [];
            },

            getAddress: function (addressType) {
                var address;
                if (addressType === 'billing') {
                    address = this.getBillingAddress();
                } else {
                    address = this.getShippingAddress();
                }
                return address;
            },

            getShippingAddress: function () {
                if (this.has('shipments') && this.get('shipments').length > 0 && this.get('shipments[0]').get('shipping_address')) {
                    return this.get('shipments[0]').get('shipping_address');
                }
                return null;
            },

            getPaymentBalance: function () {
                var balance = 0;
                if (this.has('payment_balance')) {
                    balance = this.get('payment_balance');
                }
                return balance;
            },

            hasBillingAddress: function () {
                return this.has('billing_address');
            },

            getBillingAddress: function () {
                return this.get('billing_address');
            },

            doesPaymentRequireSignature: function () {
                var payments = this.getPaymentInstruments();
                for (var p = 0; p < payments.length; ++p) {
                    var payment = payments[p];

                    if (payment.c_eaRequireSignature) {
                        return true;
                    }
                }
                return false;
            },

            isOrderConfirmed: function () {
                return this.has('confirmation_status') && this.get('confirmation_status') === 'CONFIRMED';
            },

            getStatus: function () {
                return this.get('status');
            },

            isStatusCreated: function () {
                var status = this.getStatus();
                return status && status.toLowerCase() === 'created';
            },

            getShippingMethod: function () {
                if (this.has('shipments') && this.get('shipments').length > 0 && this.get('shipments[0]').has('shipping_method')) {
                    return this.get('shipments[0]').get('shipping_method');
                }
                return null;
            },

            getCustomerInfo: function () {
                return this.get('customer_info');
            },

            getCustomerEmail: function () {
                var info = this.getCustomerInfo();
                if (info) {
                    return info.email;
                } else if (this.has('shipments') && this.get('shipments').length > 0 && this.get('shipments[0]').has('customer_info')) {
                    return this.get('shipments[0]').get('customer_info').email;
                } else {
                    return null;
                }
            },

            canEnableCheckout: function () {
                return this.get('enable_checkout');
            },

            getAvailableShippingMethods: function () {
                return this.shippingMethods;
            },

            getShippingTotal: function () {
                return this.get('shipping_total');
            },

            getShippingTotalBasePrice: function () {
                return this.get('shipping_total_base_price');
            },

            getProductTotal: function () {
                return this.get('product_total');
            },

            getProductSubtotal: function () {
                return this.get('product_sub_total');
            },

            getOrderTotal: function () {
                return this.get('order_total');
            },

            getTaxTotal: function () {
                return this.get('tax_total');
            },

            getCurrency: function () {
                return this.get('currency');
            },

            getCountry: function () {
                return this.get('c_eaCountry');
            },

            hasProductItems: function () {
                return this.has('product_items') && this.get('product_items').length > 0;
            },

            hasShippingPriceOverride: function () {
                return this.has('shipments') && this.get('shipments[0]').has('shipping_method') && this.get('shipments[0]').get('shipping_method').has('price_override') && this.get('shipments[0]').get('shipping_method').get('price_override') === 'true' && this.get('shipments[0]').get('shipping_method').get('price_override_type') !== 'none';
            },

            getShippingPriceOverride: function () {
                if (this.hasShippingPriceOverride()) {
                    return {
                        price_override_type: this.get('shipments[0]').get('shipping_method').get('price_override_type'),
                        price_override_value: this.get('shipments[0]').get('shipping_method').get('price_override_value')
                    };
                }
                return null;
            },

            getCouponItems: function () {
                return this.has('coupon_items') && this.get('coupon_items').models ? this.get('coupon_items').models : [];
            },

            getOrderNo: function () {
                return this.get('order_no');
            },

            getOrderNumber: function () {
                return this.get('order_no');
            },

            getPaymentInstruments: function () {
                return this.get('payment_instruments');
            },

            getPaymentDetails: function () {
                return this.get('payment_details');
            },

            getOrderPriceAdjustments: function () {
                return this.has('order_price_adjustments') ? this.get('order_price_adjustments').models : [];
            },

            calculateOrderPriceAdjustments: function () {
                var orderDiscount = 0;
                _.each(this.getOrderPriceAdjustments(), function (model) {
                    orderDiscount += parseFloat(model.getPrice());
                });
                return orderDiscount;
            },

            getOrderPriceDescriptions: function () {
                var orderPriceDescriptions = [];
                _.each(this.getOrderPriceAdjustments(), function (model) {
                    orderPriceDescriptions.push(model);
                });
                return orderPriceDescriptions;
            },

            hasCoupon: function (code) {
                var self = this;
                var found = _.find(self.getCouponItems(), function (coupon) {
                    return coupon.getCode() === code;
                });
                return found != null;
            },

            getProductItems: function () {
                return this.has('product_items') ? this.get('product_items').models : [];
            },

            getProductItemsCollection: function () {
                return this.get('product_items') || new Backbone.Collection();
            },

            getAllProductItemIds: function () {
                var ids = [];
                if (this.get('product_items')) {
                    this.get('product_items').each(function (productItem) {
                        ids.push(productItem.getProductId());
                    });
                }
                return ids;
            },

            getAllProductItemsIdsAndQuantities: function () {
                var data = [];
                if (this.get('product_items')) {
                    this.get('product_items').each(function (productItem) {
                        data.push(productItem.getProductIdAndQuantity());
                    });
                }
                return data;
            },

            filterProductItemsByIds: function (filter) {
                var data = [];

                if (this.get('product_items')) {
                    _.each(filter, function (currentFilter) {
                        var productItem = this.get('product_items').where({
                            product_id: currentFilter.id
                        });
                        if (productItem.length > 0) {
                            productItem = productItem[0].toJSON();
                            productItem.current_stock_level = currentFilter.stock_level;
                            data.push(productItem);
                        }
                    }.bind(this));
                }

                return data;
            },

            getApproachingOrderPromotions: function () {
                return this.get('approaching_order_promotions') || [];
            },

            getApproachingShippingPromotions: function () {
                return this.get('approaching_shipping_promotions') || [];
            },

            getCheckoutStatus: function () {
                return this.get('checkout_status');
            },

            setCheckoutStatus: function (status, options) {
                if (status == this.getCheckoutStatus()) {
                    return;
                }
                this.set({
                    last_checkout_status: this.get('checkout_status')
                }, options);
                this.set({
                    checkout_status: status
                }, options);
            },

            getLastCheckoutStatus: function () {
                return this.get('last_checkout_status');
            },

            setLastCheckoutStatusSilently: function (value) {
                this.set('last_checkout_status', value, {
                    silent: true
                });
            },

            getCheckoutStates: function () {
                return this.checkoutStates;
            },

            getNextCheckoutState: function () {
                return this.checkoutStates[this.checkoutStates.indexOf(this.getCheckoutStatus()) + 1];
            },

            setCheckoutStates: function (checkoutStates) {
                this.checkoutStates = checkoutStates;
            },

            getIsGift: function (shipment_number) {
                var ship_num = shipment_number || 0;
                if (this.has('shipments')) {
                    var shipment = this.get('shipments').at(ship_num);
                    return shipment.get('gift');
                }
                return 'false';
            },

            getGiftMessage: function (shipment_number) {
                var ship_num = shipment_number || 0;
                if (this.has('shipments')) {
                    var shipment = this.get('shipments').at(ship_num);
                    return shipment.get('gift_message');
                }
                return '';
            },

            getFaultMessage: function () {
                if (this.has('fault')) {
                    return this.get('fault').message;
                }
                return null;
            },

            getFaultType: function () {
                if (this.has('fault')) {
                    return this.get('fault').type;
                }
                return null;
            },

            setShipToStore: function (shipToStore) {
                this.set('ship_to_store', shipToStore);
            },

            setDifferentStorePickup: function (value) {
                this.set('different_store_pickup', value);
            },

            setDifferentStorePickupMessage: function (value) {
                this.set('different_store_pickup_message', value);
            },

            getShipToStore: function () {
                return this.get('ship_to_store');
            },

            getDifferentStorePickup: function () {
                return this.get('different_store_pickup');
            },

            getDifferentStorePickupMessage: function () {
                return this.get('different_store_pickup_message');
            },

            parse: function (in_json) {
                in_json.shipping_total_excluding_discount = 0;
                in_json.shipping_total_base_price = 0;
                in_json.shipping_total = 0;
                _.each(in_json.product_items, function (item) {
                    if (item.c_eaCustomAttributes) {
                        var attr = JSON.parse(item.c_eaCustomAttributes);
                        if (attr.eaPriceOverrideType) {
                            item.manager_employee_id = attr.eaManagerEmployeeId;
                            item.price_override_type = attr.eaPriceOverrideType;
                            item.price_override = 'true';
                            item.price_override_value = attr.eaPriceOverrideValue;
                            item.price_override_reason_code = attr.eaOverrideReasonCode;
                        } else {
                            item.price_override = 'false';
                        }
                        item.base_price_override = parseFloat(attr.base_price_override);
                        item.base_price = parseFloat(attr.base_price);
                        item.price = parseFloat(attr.price);
                        item.thumbnailUrl = attr.thumbnailUrl;
                        item.message = attr.message;
                    }
                });

                _.each(in_json.shipping_items, function (item) {
                    var attr = null;
                    if (item.c_eaCustomAttributes) {
                        var shipment = _.find(in_json.shipments, function (ship) {
                            return ship.shipment_id === item.shipment_id;
                        });
                        attr = JSON.parse(item.c_eaCustomAttributes);
                        if (shipment.shipping_method) {
                            if (attr.eaPriceOverrideType && attr.eaPriceOverrideType != 'none') {
                                shipment.shipping_method.manager_employee_id = attr.eaManagerEmployeeId;
                                shipment.shipping_method.price_override_type = attr.eaPriceOverrideType;
                                shipment.shipping_method.price_override = 'true';
                                shipment.shipping_method.price_override_value = attr.eaPriceOverrideValue;
                                shipment.shipping_method.price_override_reason_code = attr.eaOverrideReasonCode;
                                shipment.shipping_method.base_price = parseFloat(attr.base_price);
                                shipment.shipping_method.base_price_override = parseFloat(attr.base_price_override);
                                in_json.shipping_total_excluding_discount += shipment.shipping_method.base_price;
                                in_json.shipping_total_base_price += shipment.shipping_method.base_price;
                            } else {
                                shipment.shipping_method.price_override = 'false';
                                in_json.shipping_total_base_price = parseFloat(item.base_price);
                            }
                        }
                    }

                    if (in_json.order_no) {
                        if (!attr) {
                            in_json.shipping_total_base_price = parseFloat(item.base_price);
                        }
                        in_json.shipping_total += item.price_after_item_discount ? parseFloat(item.price_after_item_discount) : parseFloat(item.base_price);
                    } else {
                        in_json.shipping_total += parseFloat(item.base_price);
                    }
                });

                if (in_json.c_eaCustomAttributes) {
                    var attr = JSON.parse(in_json.c_eaCustomAttributes);
                    in_json.coupons = attr.coupons;
                    if (attr.enable_checkout !== undefined) {
                        in_json.enable_checkout = attr.enable_checkout == 1;
                    }

                    in_json.shipping_methods = attr.shipping_methods;
                    in_json.product_sub_total = parseFloat(attr.product_sub_total);
                    in_json.product_total = parseFloat(attr.product_total);
                    in_json.order_total = parseFloat(attr.order_total);
                    in_json.tax_total = parseFloat(attr.tax_total);
                    in_json.shipping_total_base_price = parseFloat(attr.shipping_total_base_price);
                    in_json.shipping_discount = parseFloat(attr.shipping_discount);
                    in_json.shipping_total = parseFloat(attr.shipping_total);
                    in_json.shipping_total_excluding_discount = parseFloat(attr.shipping_total_excluding_discount);
                    in_json.product_total = parseFloat(attr.product_total);
                    in_json.product_sub_total = parseFloat(attr.product_sub_total);
                    in_json.creation_date = attr.creation_date;
                    if (attr.approaching_shipping_promotions) {
                        in_json.approaching_shipping_promotions = attr.approaching_shipping_promotions;
                        in_json.approaching_shipping_promotions = _.map(in_json.approaching_shipping_promotions, function (promotion) {
                            promotion.amount_to_qualify = parseFloat(promotion.amount_to_qualify);
                            return promotion;
                        });
                    }
                    if (attr.approaching_order_promotions) {
                        in_json.approaching_order_promotions = attr.approaching_order_promotions;
                        in_json.approaching_order_promotions = _.map(in_json.approaching_order_promotions, function (promotion) {
                            promotion.amount_to_qualify = parseFloat(promotion.amount_to_qualify);
                            return promotion;
                        });
                    }
                }
                return in_json;
            }
        });

        return Model;
    },

    extendCollection: function (Collection) {
        _.extend(Collection.prototype, {});

        return Collection;
    }
};

model = Alloy.M('baskets', exports.definition, []);

collection = Alloy.C('baskets', exports.definition, model);

exports.Model = model;
exports.Collection = collection;