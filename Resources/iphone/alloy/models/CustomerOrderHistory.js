var Alloy = require('/alloy'),
    _ = require("/alloy/underscore")._,
    model,
    collection;

var order = Backbone.Model.extend({
    config: {
        secure: true,
        model_name: 'order',
        cache: false
    }
});

exports.definition = {
    config: {
        model_name: 'customerOrderHistory',
        secure: true,
        cache: false,
        adapter: {
            type: 'storefront',
            collection_name: 'customerOrderHistory'
        }
    },
    extendModel: function (Model) {
        _.extend(Model.prototype, {

            urlRoot: '/EAOrder-OrderDetails',

            getImageURL: function () {
                return this.get('imageURL');
            },

            getOrderNo: function () {
                return this.get('orderNo');
            },

            getCurrencyCode: function () {
                return this.get('currencyCode');
            },

            getCreationDate: function () {
                return this.get('creationDate');
            },

            getTotalNetPrice: function () {
                return this.get('totalNetPrice');
            },

            getConfirmationStatus: function () {
                return this.get('confirmation_status');
            },

            getStatus: function () {
                return this.get('status');
            },

            isStatusCompleted: function () {
                var status = this.getStatus();
                return status && status !== 'failed' && status !== 'created';
            }
        });
        return Model;
    },
    extendCollection: function (Collection) {
        _.extend(Collection.prototype, {

            url: '/EAOrder-OrderHistory',

            queryParams: function () {
                return this.requestParams;
            },

            getModels: function () {
                return this.models;
            },

            search: function (params) {
                var self = this;
                self.requestParams = params;
                var promise = self.fetch();
                promise.always(function () {
                    self.requestParams = {};
                });
                return promise;
            },

            parse: function (in_json) {
                if ('orders' in in_json) {
                    return in_json.orders;
                }
                return [];
            }
        });
        return Collection;
    }
};

model = Alloy.M('customerOrderHistory', exports.definition, []);

collection = Alloy.C('customerOrderHistory', exports.definition, model);

exports.Model = model;
exports.Collection = collection;