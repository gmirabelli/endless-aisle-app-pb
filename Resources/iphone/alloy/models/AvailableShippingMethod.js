var Alloy = require('/alloy'),
    _ = require("/alloy/underscore")._,
    model,
    collection;

exports.definition = {
    config: {
        model_name: 'availableShippingMethod',
        adapter: {
            type: 'ocapi'
        }
    },

    extendModel: function (Model) {
        _.extend(Model.prototype, {
            getBasePrice: function () {
                return this.get('shipping_method_base_price');
            },

            getSurcharge: function () {
                return this.get('shipping_method_surcharge');
            },

            getDescription: function () {
                return this.get('shipping_method_description');
            },

            getName: function () {
                return this.get('shipping_method_name');
            },

            getID: function () {
                return this.get('shipping_method_id');
            },

            getDefaultMethod: function () {
                return this.get('default_method');
            }
        });

        return Model;
    },
    extendCollection: function (Collection) {
        _.extend(Collection.prototype, {});

        return Collection;
    }
};

model = Alloy.M('availableShippingMethod', exports.definition, []);

collection = Alloy.C('availableShippingMethod', exports.definition, model);

exports.Model = model;
exports.Collection = collection;