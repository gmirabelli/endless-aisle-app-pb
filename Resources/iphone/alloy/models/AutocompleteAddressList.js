var Alloy = require('/alloy'),
    _ = require("/alloy/underscore")._,
    model,
    collection;

exports.definition = {
    config: {

        adapter: {
            type: 'properties',
            collection_name: 'autocompleteAddressList'
        }
    },

    extendModel: function (Model) {
        _.extend(Model.prototype, {
            getDescription: function () {
                return this.get('description');
            },

            getReference: function () {
                return this.get('reference');
            }
        });

        return Model;
    }
};

model = Alloy.M('autocompleteAddressList', exports.definition, []);

collection = Alloy.C('autocompleteAddressList', exports.definition, model);

exports.Model = model;
exports.Collection = collection;