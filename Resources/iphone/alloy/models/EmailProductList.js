var Alloy = require('/alloy'),
    _ = require("/alloy/underscore")._,
    model,
    collection;

exports.definition = {
    config: {
        model_name: 'emailProductList',
        secure: true,
        cache: false,
        adapter: {
            type: 'storefront'
        }
    },
    extendModel: function (Model) {
        _.extend(Model.prototype, {

            urlRoot: 'EAAccount-EmailProductList',

            send: function (args) {
                this.id = null;
                return this.save(args);
            }
        });

        return Model;
    }
};

model = Alloy.M('emailProductList', exports.definition, []);

collection = Alloy.C('emailProductList', exports.definition, model);

exports.Model = model;
exports.Collection = collection;