var Alloy = require('/alloy'),
    _ = require("/alloy/underscore")._,
    model,
    collection;

function queryParams() {
    var params = _.extend({
        email: this.customer.getEmail()
    }, this.requestParamObj);
    delete this.requestParamObj;
    return params;
}

var ItemCRUDModel = Backbone.Model.extend({
    config: {
        secure: true,
        model_name: 'CRUDProductListItem',
        cache: false
    },

    hasFlash: function () {
        return this.has('_flash');
    },

    hasFault: function () {
        return this.has('fault');
    }
});

exports.definition = {
    config: {
        model_name: 'productLists',
        adapter: {
            type: 'ocapi',
            collection_name: 'productLists'
        }

    },
    extendModel: function (Model) {
        _.extend(Model.prototype, {

            urlRoot: '/product_lists',

            relations: [{
                type: Backbone.Many,
                key: 'customer_product_list_items',
                relatedModel: require('alloy/models/' + ucfirst('productListItems')).Model
            }],

            getId: function () {
                return this.get('id');
            },

            getName: function () {
                return this.get('name');
            },

            getTitle: function () {
                return this.get('title');
            },

            getType: function () {
                return this.get('type');
            },

            isPublic: function () {
                this.get('public');
            },

            getLink: function () {
                return this.get('link');
            },

            getListItems: function () {
                return this.get('items');
            },

            deleteItem: function (customerId, listId, itemId, opt) {
                var deferred = new _.Deferred();
                var self = this;

                var url = '/customers/' + customerId + this.urlRoot + '/' + listId + '/items/' + itemId;
                var itemDelete = new ItemCRUDModel();
                var onSuccess = function (response) {
                    itemDelete.set(response);
                };
                var options = _.extend({}, {
                    success: onSuccess,
                    error: onSuccess
                }, opt);

                Alloy.Models.authorizationToken.fetchToken(Alloy.CFG.storefront_host).done(function () {
                    var params = _.extend({}, {
                        url: url,
                        type: 'DELETE',
                        headers: {
                            'content-type': 'application/json',
                            Authorization: 'Bearer ' + Alloy.Models.authorizationToken.getToken()
                        }
                    });
                    self.apiCall(itemDelete, params, options).done(function () {
                        if (itemDelete.hasFlash()) {
                            deferred.reject(itemDelete);
                        } else {
                            var responseOptions = {
                                delete: true,
                                update: false
                            };
                            var list = self.get('items');
                            var item = list.where({
                                id: itemId
                            });
                            list.remove(item);
                            self.trigger('item:deleted');
                            deferred.resolve(itemDelete, responseOptions);
                        }
                    }).fail(function () {
                        deferred.reject(itemDelete);
                    });
                }).fail(function () {
                    deferred.reject();
                });
                return deferred.promise();
            },

            addItem: function (customerId, listId, product, opt) {
                var deferred = new _.Deferred();
                var self = this;
                var itemAdd = new ItemCRUDModel();
                var onSuccess = function (response) {
                    itemAdd.set(response);
                };
                var options = _.extend({}, {
                    success: onSuccess,
                    error: onSuccess,
                    dontincludeid: true
                }, opt);

                var data = {
                    type: 'product',
                    quantity: product.quantity || 1,
                    priority: 1,
                    public: true,
                    product_id: product.product_id
                };
                data = JSON.stringify(data);

                var url = '/customers/' + customerId + this.urlRoot + '/' + listId + '/items';
                Alloy.Models.authorizationToken.fetchToken(Alloy.CFG.storefront_host).done(function () {
                    var params = _.extend({}, {
                        url: url,
                        type: 'POST',
                        data: data,
                        headers: {
                            'content-type': 'application/json',
                            Authorization: 'Bearer ' + Alloy.Models.authorizationToken.getToken()
                        }
                    });
                    self.apiCall(itemAdd, params, options).done(function () {
                        if (itemAdd.hasFlash()) {
                            self.updateItem(customerId, product, listId, itemAdd.get('id')).done(function (model, options) {
                                deferred.resolve(model, options);
                            }).fail(function (model) {
                                deferred.reject(model);
                            });
                        } else {
                            var responseOptions = {
                                update: false
                            };
                            itemAdd.set('product_name', itemAdd.get('product_details_link').title);
                            var item = Alloy.createModel('productListItems', itemAdd.toJSON());
                            var list = self.get('items');
                            list.add(item);
                            deferred.resolve(item, responseOptions);
                            self.trigger('item:added');
                        }
                    }).fail(function () {
                        deferred.reject(itemAdd);
                    });
                }).fail(function () {
                    deferred.reject();
                });
                return deferred.promise();
            },

            updateItem: function (customerId, product, listId, itemId, opt) {
                var deferred = new _.Deferred();
                var self = this;
                var itemUpdate = new ItemCRUDModel();
                var onSuccess = function (response) {
                    itemUpdate.set(response);
                };
                var options = _.extend({}, {
                    success: onSuccess,
                    error: onSuccess
                }, opt);
                data = {
                    type: 'product',
                    quantity: product.quantity || 1,
                    priority: 1,
                    public: true,
                    product_id: product.product_id
                };
                data = JSON.stringify(data);
                var url = '/customers/' + customerId + this.urlRoot + '/' + listId + '/items/' + itemId;
                Alloy.Models.authorizationToken.fetchToken(Alloy.CFG.storefront_host).done(function () {
                    var params = _.extend({}, {
                        url: url,
                        type: 'PATCH',
                        data: data,
                        headers: {
                            'content-type': 'application/json',
                            Authorization: 'Bearer ' + Alloy.Models.authorizationToken.getToken()
                        }
                    });
                    self.apiCall(itemUpdate, params, options).done(function () {
                        if (itemUpdate.hasFlash()) {
                            deferred.reject(itemUpdate);
                        } else {
                            var responseOptions = {
                                update: true
                            };
                            var list = self.get('items');
                            var item = list.where({ id: itemId })[0];
                            itemUpdate.set('product_name', itemUpdate.get('product_details_link').title);
                            item.set(itemUpdate.toJSON());
                            self.trigger('item:updated');
                            deferred.resolve(item, responseOptions);
                        }
                    }).fail(function () {
                        deferred.reject(itemUpdate);
                    });
                }).fail(function () {
                    deferred.reject();
                });
                return deferred.promise();
            }
        });

        return Model;
    },

    extendCollection: function (Collection) {
        _.extend(Collection.prototype, {

            urlRoot: '/product_lists',
            queryParams: queryParams,

            initialize: function () {
                this.mail = Alloy.createModel('emailProductList');
            },

            setCustomer: function (customer) {
                this.customer = customer;
            },

            customerLoggedIn: function () {
                return this.customer ? this.customer.isLoggedIn() : false;
            },

            getCount: function () {
                return this.length;
            },

            getTotal: function () {
                return this.length;
            },

            getAllTitles: function () {
                return this.pluck('title');
            },

            getAllNames: function () {
                return this.pluck('name');
            },

            getAllLinks: function () {
                return this.pluck('link');
            },

            getAllIds: function () {
                var ids = [];
                this.each(function (model) {
                    ids.push(model.getId());
                });
                return ids;
            },

            getAllWishLists: function () {
                return this.where({
                    type: 'wish_list'
                });
            },

            getWishListCount: function () {
                return this.where({
                    type: 'wish_list'
                }).length;
            },

            getAllShoppingLists: function () {
                return this.where({
                    type: 'shopping_list'
                });
            },

            getAllGiftRegistries: function () {
                return this.where({
                    type: 'gift_registry'
                });
            },

            getListsByType: function (type) {
                if (type) {
                    return this.where({
                        type: type
                    });
                }
                return [];
            },

            getSelectorObjects: function () {
                var obj = [];
                this.each(function (model) {
                    obj.push({
                        listId: model.getId(),
                        listName: model.getName()
                    });
                });
                return obj;
            },

            getWishListSelectorObjects: function () {
                var obj = [];
                this.each(function (model) {
                    if (model.getType() == 'wish_list') {
                        obj.push({
                            wishListId: model.getId(),
                            wishListName: model.getName() ? model.getName() : _L('Wish List Title')
                        });
                    }
                });
                return obj;
            },

            getFirstWishListId: function () {
                var firstWishList = this.where({
                    type: 'wish_list'
                });
                if (firstWishList.length > 0) {
                    return firstWishList[0].getId();
                }
                return null;
            },

            getListNameById: function (listId) {
                var data = this.where({
                    id: listId
                });
                if (data.length > 0) {
                    return data[0].getName();
                }
                return null;
            },

            getListById: function (id) {
                var lists = this.where({
                    id: id
                });
                if (lists.length > 0) {
                    return lists[0];
                }
                return null;
            },

            getListItems: function (id) {
                var list = this.getListById(id);
                if (list) {
                    return list.getListItems();
                } else {
                    return null;
                }
            },

            getTotalQuantity: function () {
                var total = 0;
                this.each(function (model) {
                    _.each(model.get('items').models, function (list) {
                        total += list.getQuantity();
                    });
                });
                return total;
            },

            addItem: function (wishListId, product) {
                var list = this.getListById(wishListId);
                var self = this;
                var deferred = new _.Deferred();
                if (this.customerLoggedIn()) {
                    var promise = list.addItem(this.customer.getCustomerId(), wishListId, product);
                    promise.done(function (newItem, options) {
                        self.trigger('reset');
                        deferred.resolve(newItem, options);
                    }).fail(function () {
                        deferred.reject();
                    });
                } else {
                    deferred.reject();
                }
                return deferred.promise();
            },

            deleteItem: function (listId, productId) {
                var list = this.getListById(listId);
                var self = this;
                var promise = list.deleteItem(this.customer.getCustomerId(), listId, productId);
                promise.done(function (newList) {
                    self.trigger('reset');
                });
                return promise;
            },

            parse: function (collection) {
                var parsedData = [];
                var self = this;
                if (collection && collection.length > 0) {
                    _.each(collection, function (list) {
                        var link = list.items_link.link.split('/');
                        link = link[link.length - 1].split('?');
                        link = link[0];
                        if (!list.name) {
                            list.name = _L('Wish List Title');
                        }

                        var items = Alloy.createCollection('productListItems');
                        items.reset(self.parseProductListItems(list.customer_product_list_items));
                        list.items = items;
                        if (list.type == 'wish_list') {
                            if (Alloy.CFG.show_private_wish_list) {
                                parsedData.push(list);
                            } else if (!Alloy.CFG.show_private_wish_list && list.public == true) {
                                parsedData.push(list);
                            }
                        } else {
                            parsedData.push(list);
                        }
                    });
                }
                return parsedData;
            },

            parseProductListItems: function (collection) {
                var parsedData = [];
                if (collection && collection.length > 0) {
                    _.each(collection, function (item) {
                        if (item.product_details_link) {
                            item.product_name = item.product_details_link.title;
                            if (Alloy.CFG.show_private_product_list_item) {
                                parsedData.push(item);
                            } else if (!Alloy.CFG.show_private_product_list_item && item.public == true) {
                                parsedData.push(item);
                            }
                        }
                    });
                }
                parsedData.reverse();
                return parsedData;
            },

            getCollection: function (currentCustomer) {
                var deferred = new _.Deferred();
                var self = this;
                if (currentCustomer && currentCustomer.isLoggedIn() || this.customerLoggedIn()) {
                    this.customer = currentCustomer;
                    this.url = '/customers/' + currentCustomer.getCustomerId() + this.urlRoot;
                    Alloy.Models.authorizationToken.fetchToken(Alloy.CFG.storefront_host).done(function () {
                        self.authorization = 'Bearer ' + Alloy.Models.authorizationToken.getToken();
                        self.fetch().done(function () {
                            deferred.resolve();
                        }).fail(function (model) {
                            deferred.reject();
                        });
                    }).fail(function () {
                        deferred.reject();
                    });
                } else {
                    deferred.reject();
                }
                return deferred.promise();
            },

            clearData: function () {
                this.reset();
                this.id = null;
            }
        });

        return Collection;
    }
};

model = Alloy.M('productLists', exports.definition, []);

collection = Alloy.C('productLists', exports.definition, model);

exports.Model = model;
exports.Collection = collection;