var Alloy = require('/alloy'),
    _ = require("/alloy/underscore")._,
    model,
    collection;

exports.definition = {
    config: {
        model_name: 'storeWebOrder',
        secure: true,
        cache: false,
        adapter: {
            type: 'storefront'
        }
    },

    extendModel: function (Model) {

        _.extend(Model.prototype, {

            urlRoot: '/EACheckout-StoreWebOrder'
        });

        return Model;
    }
};

model = Alloy.M('storeWebOrder', exports.definition, []);

collection = Alloy.C('storeWebOrder', exports.definition, model);

exports.Model = model;
exports.Collection = collection;