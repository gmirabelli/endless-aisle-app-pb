var Alloy = require('/alloy'),
    _ = require("/alloy/underscore")._,
    model,
    collection;

var Address = require('dw/instore/Address');

exports.definition = {
    config: {
        model_name: 'billingAddress',
        adapter: {
            type: 'ocapi'
        },
        superClass: Address
    },

    extendModel: function (Model) {
        _.extend(Model.prototype, {});

        return Model;
    },
    extendCollection: function (Collection) {
        _.extend(Collection.prototype, {});

        return Collection;
    }
};

model = Alloy.M('billingAddress', exports.definition, []);

collection = Alloy.C('billingAddress', exports.definition, model);

exports.Model = model;
exports.Collection = collection;