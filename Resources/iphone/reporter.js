

var reporter = exports.reporter = Alloy.CFG.use_crash_reporter ? require('yy.logcatcher') : null;
var eaUtils = require('EAUtils');
var reporting = false;

if (reporter) {
    reporter.addEventListener('error', function (error) {
        if (!reporting) {
            reporting = true;
            sendReport(error);
        }
    });
}

function sendReport(error, screenshot) {
    Alloy.eventDispatcher.trigger('hideAuxillaryViews');
    if (Alloy.Dialog) {
        Alloy.Dialog.showCustomDialog({
            controllerPath: 'components/errorPopover',
            initOptions: formatMessage(error),
            continueEvent: 'errorPopover:dismiss',
            continueFunction: function (event) {
                sendMessage(event ? event.text : null, error);
            }
        });
    } else {
        sendMessage('startup error', error);
    }
}

function appendMessage(key, sourceObject) {
    var newMessage = '';
    if (sourceObject[key]) {
        newMessage = '\n    ' + key + ': ' + sourceObject[key];
    }
    return newMessage;
}

function formatMessage(error) {
    var messageBody = 'Script Errors: ';
    ['type', 'name', 'message', 'backtrace', 'stack', 'line', 'sourceURL'].forEach(function (key) {
        messageBody += appendMessage(key, error);
    });

    messageBody += '\n\nApplication Information: ';
    ['deployType', 'guid', 'id', 'installId', 'keyboardVisible', 'sessionId', 'version'].forEach(function (key) {
        messageBody += appendMessage(key, Ti.App);
    });
    var associate = Alloy.Models.associate;
    if (associate && associate.getEmployeeId()) {
        messageBody += '\n    Associate ID: ' + associate.getEmployeeId();
        messageBody += '\n    Associate Name: ' + associate.getFirstName() + ' ' + associate.getLastName();
    }
    messageBody += '\n    Store ID: ' + Alloy.CFG.store_id;

    messageBody += '\n\nPlatform Information: ';
    ['architecture', 'availableMemory', 'id', 'locale', 'macaddress', 'ip', 'manufacturer', 'model', 'name', 'netmask', 'osname', 'ostype', 'processorCount', 'runtime', 'username', 'version'].forEach(function (key) {
        messageBody += appendMessage(key, Ti.Platform);
    });

    messageBody += '\n\nDisplay Information: ';
    ['density', 'dpi', 'logicalDensityFactor', 'platformHeight', 'platformWidth', 'xdpi', 'ydpi'].forEach(function (key) {
        messageBody += appendMessage(key, Ti.Platform.displayCaps);
    });

    messageBody += '\n';
    return messageBody;
}

function sendMessage(text, error) {
    var messageBody = 'An error in the EA app has occurred.';

    if (text) {
        messageBody += '\n\nUser Data: \n' + text;
    }

    messageBody += '\n\n' + formatMessage(error);

    if (Alloy.CFG.error_reporting.js_crash_reporting) {
        eaUtils.sendErrorToServer(messageBody);
    }
    reporting = false;
}