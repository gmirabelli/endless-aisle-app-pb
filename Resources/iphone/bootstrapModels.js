

var deferred = _.Deferred();

var rootCategory = Alloy.createModel('category', {
    id: 'root'
});

var hash = Alloy.Globals.categoryHash = {};

rootCategory.fetchCategory().done(function () {
    rootCategory.getAllLevels().done(function () {
        hash['root'] = rootCategory;
        var ids = [];
        rootCategory.getCategoriesCollection().each(function (cat) {
            hash[cat.id] = cat;
            ids = ids.concat(_.pluck(cat.getCategories(), 'id'));
        });

        var collection = Alloy.createCollection('category');
        collection.ids = ids;
        collection.fetchCategories().done(function () {
            var cats = {};
            _.each(collection.models, function (subcat) {
                var parentId = subcat.getParentCategoryId();
                var categories = cats[parentId] || Alloy.createCollection('category');
                categories.add(subcat);
                cats[parentId] = categories;

                hash[subcat.id] = subcat;
                subcat.getCategoriesCollection().each(function (subsubcat) {
                    hash[subsubcat.id] = subsubcat;
                    subsubcat.getCategoriesCollection().each(function (leafcat) {
                        hash[leafcat.id] = leafcat;
                    });
                });
            });
            for (var c in cats) {
                hash[c].setCategories(cats[c].toJSON(), {
                    silent: true
                });
            }
        });
        deferred.resolve();
    }).fail(function (model) {
        deferred.reject(model);
    });
}).fail(function (model) {
    deferred.reject(model);
});

Alloy.Globals.categoryTree = rootCategory;

module.exports = deferred.promise();