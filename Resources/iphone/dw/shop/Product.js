

var Backbone = Backbone || Alloy ? Alloy.Backbone : require('backbone');
var logger = require('logging')('models:Product', 'app/lib/dw/shop/Product');

var AssociatedModel = AssociatedModel || Alloy ? Backbone.AssociatedModel : require('backbone-associations').AssociatedModel;

var ProductPrices = Backbone.Model.extend({
    config: {
        secure: true,
        cache: false,
        model_name: 'productPrices'
    },

    getPrices: function () {
        return this.get('prices');
    },

    getPrice: function () {
        return this.get('price');
    }
});

var ProductImages = Backbone.Model.extend({
    config: {
        cache: true,
        model_name: 'productImages'
    }
});

var Recommendation = require('alloy/models/Recommendations').Model;

var Product = AssociatedModel.extend({
    idAttribute: 'id',
    urlRoot: '/products',

    initialize: function () {
        this.recommendations = new Recommendation();
    },

    parse: function (response, options) {
        parseVariations(response);

        parseImageGroups(response, {});

        if (response.data && response.data.length) {
            if (response.data.length === 1) {
                return response.data[0];
            }

            _.each(response.data, function (data) {
                if (data.type.variant) {
                    return data;
                }
            });

            return response.data[0];
        } else if (response.bundled_products) {
            var bundled_products = response.bundled_products;
            for (var i = 0, ii = bundled_products.length; i < ii; i++) {
                bundled_products[i].product.quantity = bundled_products[i].quantity;
                bundled_products[i] = bundled_products[i].product;
            }
        }

        return response;
    },

    queryParams: function () {
        var expand = this.get('expand') || Alloy.CFG.product.default_expand || 'variations,availability,images,prices,options,promotions,set_products,bundled_products';
        var params = {
            expand: expand
        };

        if (expand.indexOf('images') > -1) {
            _.extend(params, {
                all_images: true
            });
        }
        if (Alloy.CFG.store_availability.enabled) {
            var inventory_ids = Alloy.Models.storeInfo.get('inventory_id');
            var ids = this.get('inventory_ids');
            if (ids) {
                inventory_ids = ids.join(',');
            }
            _.extend(params, {
                inventory_ids: inventory_ids
            });
        }
        return params;
    },

    getImageGroupsForViewType: function (view_type, variation_value) {
        var realViewType = this.getImageViewType(view_type),
            images = this._get('images');

        var viewTypeImagesHash = images[realViewType];

        var imageGroups;

        if (viewTypeImagesHash) {
            var imageGroupsJSON = _.values(viewTypeImagesHash);
            imageGroupsJSON = this.transformImageGroupImageURLs(imageGroupsJSON, view_type);

            if (imageGroupsJSON) {
                if (variation_value) {
                    imageGroupsJSON = _.filter(imageGroupsJSON, function (imageGroup) {
                        if (imageGroup.variation_attributes) {
                            var var_attributes = imageGroup.variation_attributes[0];
                            var vv = var_attributes.values[0].value;
                            if (vv == variation_value) {
                                return true;
                            } else {
                                return false;
                            }
                        }
                        return false;
                    });
                }
                var collection = new Backbone.Collection(imageGroupsJSON, {
                    model: Product.ImageGroup
                });
                imageGroups = collection.models;
            }
        }

        return imageGroups;
    },

    getImages: function (view_type, variation_value) {
        var realViewType = this.getImageViewType(view_type),
            images = this._get('images');

        var viewTypeImagesJSON = images[realViewType];
        if (viewTypeImagesJSON) {
            var imageGroupsJSON = viewTypeImagesJSON[variation_value];
            if (!imageGroupsJSON) {
                variation_value = 'default';
                imageGroupsJSON = viewTypeImagesJSON[variation_value];
            }
            if (imageGroupsJSON) {
                imageGroupsJSON = this.transformImageGroupImageURLs([imageGroupsJSON], view_type);
                var imageGroupJSON = imageGroupsJSON[0];

                var imagesJSON = imageGroupJSON.images;

                var collection = new Backbone.Collection(imagesJSON, {
                    model: Product.ImageGroup.Image
                });
                imageGroups = collection.models;
            } else {
                imageGroups = null;
            }
        } else {
            imageGroups = null;
        }

        if (!imageGroups) {
            logger.error('Image missing for ' + this.getId() + ' for view type: ' + realViewType);
            imageGroups = this.getPlaceholderImages(realViewType);
        }

        return imageGroups;
    },

    getPlaceholderImageGroup: function (view_type, vv_id) {
        var realViewType = this.getImageViewType(view_type);
        var imageGroup = new Product.ImageGroup();
        imageGroup.set({
            images: this.getPlaceholderImages(),
            variation_attributes: [{
                id: Alloy.CFG.product.color_attribute,
                values: [{
                    value: vv_id
                }]
            }],
            view_type: view_type || 'large'
        });

        return imageGroup;
    },

    getPlaceholderImages: function () {
        var images = new Backbone.Collection([{
            alt: _L('Image not available'),
            link: Alloy.Styles.imageNotAvailableImage,
            title: _L('Image not available')
        }], {
            model: Product.ImageGroup.Image
        });

        return images.models;
    },

    getOrderableColorImageGroups: function (view_type) {
        var orderableImageGroups,
            colorAttribute = this.getVariationAttribute(Alloy.CFG.product.color_attribute),
            self = this;

        if (colorAttribute) {
            var orderableValuesJSON = this._get('orderable_variation_values');
            var colorValuesJSON = orderableValuesJSON[Alloy.CFG.product.color_attribute];

            orderableImageGroups = colorValuesJSON ? _.map(colorValuesJSON, function (vv) {
                return self.getImageGroupsForViewType(view_type, vv.value)[0];
            }) : null;
        }
        return orderableImageGroups;
    },

    isOrderableVariationValue: function (attrs, attributeId) {
        var attributeId = attributeId ? attributeId : _.keys(attrs)[0],
            valueValue = attrs[attributeId];
        var orderableValuesJSON = this._get('orderable_variation_values');
        var valuesJSON = orderableValuesJSON[attributeId];

        var match = _.findWhere(valuesJSON, {
            value: valueValue
        });

        return !!match;
    },

    hasOrderableVariants: function (attrs) {
        var match_criteria = _.extend({}, attrs, {
            orderable: true
        });

        var orderable_variants = this._get('orderable_variants');

        var match = _.findWhere(orderable_variants, match_criteria);

        return !!match;
    },

    getOrderableVariants: function (attrs) {
        var match_criteria = _.extend({}, attrs, {
            orderable: true
        });

        var orderable_variants = this._get('orderable_variants');

        var match = _.where(orderable_variants, match_criteria);

        return match;
    },

    getMatchingVariants: function (attrs) {
        var match_criteria = _.extend({}, attrs);

        var matching_variants = this._get('variants');

        var matches = [];

        _.each(matching_variants.models, function (variant) {
            var vvs = variant._get('variation_values'),
                match = true;
            for (var va in match_criteria) {
                if (vvs[va] != match_criteria[va]) {
                    match = false;
                    break;
                }
            }
            if (match) {
                matches.push(variant);
            }
        });

        return matches;
    },

    getMasterID: function () {
        var id = null;
        var master = this._get('master');
        id = master && master.get('master_id');
        return id;
    },

    isMaster: function () {
        return this.isType('master');
    },

    isVariant: function () {
        return this.isType('variant');
    },

    isStandard: function () {
        return this.isType('item');
    },

    isBundle: function () {
        return this.isType('bundle');
    },

    isSet: function () {
        return this.isType('set');
    },

    isOption: function () {
        return this.isType('option');
    },

    isType: function (product_type) {
        var type = this._get('type');
        return type._get(product_type);
    },

    isProductSelected: function () {
        return this.hasSelectedVariant() || !this.hasAvailableVariants();
    },

    hasAvailableVariants: function () {
        var available = false;
        if (this._get('variation_values') && this._get('variation_attributes')) {
            available = true;
        };
        return available;
    },

    hasConfiguredVariant: function () {
        var variationValues = this._get('variation_values');
        if (!variationValues) {
            return false;
        }

        var selectedValues = _.keys(variationValues);
        var allAttributes = this._get('variation_attributes');

        return selectedValues.length == allAttributes.length;
    },

    hasSelectedVariant: function () {
        return !!this._get('selected_variant');
    },

    getSelectedVariant: function () {
        return this._get('selected_variant');
    },

    setSelectedVariant: function (value, options) {
        return this.set('selected_variant', value, options);
    },

    hasSelectedOption: function () {
        return !!this._get('selected_option');
    },

    getSelectedOption: function () {
        return this._get('selected_option');
    },

    setSelectedOption: function (value, options) {
        return this.set('selected_option', value, options);
    },

    setProductId: function (id) {
        this.set('id', id);
    },

    getProductId: function () {
        return this._get('id');
    },

    hasProductId: function () {
        return this.has('id');
    },

    hasRecommendedItemId: function () {
        return this.has('recommended_item_id');
    },

    getRecommendedItemId: function () {
        return this._get('recommended_item_id');
    },

    getQuantity: function () {
        return this._get('quantity');
    },

    setQuantity: function (value, options) {
        var selectedProduct = this.getSelectedVariant();
        if (selectedProduct) {
            selectedProduct.set('quantity', value, options);
        }
        return this.set('quantity', value, options);
    },

    getPromoPrice: function () {
        return this._get('promo_price');
    },

    getPrices: function () {
        return this._get('prices');
    },

    getSelectedProductId: function () {
        var productId = this.getId();
        if (this.hasConfiguredVariant()) {
            var selectedVariant = this.getSelectedVariant();
            if (selectedVariant) {
                productId = _.isFunction(selectedVariant.getProductId) ? selectedVariant.getProductId() : selectedVariant.getId();
            } else {
                productId = null;
            }
        }
        return productId;
    },

    setSelectedVariationValue: function (attr_id, value_id) {
        var pair = {},
            variationValues = this._get('variation_values');
        pair[attr_id] = value_id;

        logger.info('updating variation_values with new selection: ' + JSON.stringify(pair));
        var selectedVariationValues = _.extend({}, variationValues, pair);

        this.set({
            variation_values: selectedVariationValues
        });
    },

    getSelectedVariationValue: function (attr_id) {
        var svv,
            svvalue,
            svvs = this._get('variation_values');
        if (svvs) {
            svvalue = svvs[attr_id];
        }

        if (svvalue) {
            var vvs = this.getVariationAttributeValues(attr_id);
            if (vvs) {
                vvs = vvs.filter(function (vv) {
                    return vv._get('value') == svvalue;
                });
                if (vvs && vvs.length > 0) {
                    svv = vvs[0];
                }
            }
        }

        return svv;
    },

    getVariationAttribute: function (attr_id) {
        var va = null;
        var vas = this._get('variation_attributes');
        if (vas) {
            vas = vas.filter(function (va) {
                return va.id == attr_id;
            });

            if (vas && vas.length > 0) {
                va = vas[0];
            }
        }
        return va;
    },

    getVariationAttributeValues: function (attr_id) {
        var values = null;
        if (_.isString(attr_id)) {
            var attr = this.getVariationAttribute(attr_id);
            values = attr._get('values');
        } else {
            values = attr_id._get('values');
        }
        return values;
    },

    getVariationValues: function () {
        return this._get('variation_values') || {};
    },

    setVariationValues: function (value, options) {
        return this.set('variation_values', value, options);
    },

    getOrderableVariationValues: function (attr_id) {
        if (!_.isString(attr_id)) {
            attr_id = attr_id.id;
        }

        var orderableValuesHashJSON = this._get('orderable_variation_values'),
            orderableValuesHash;
        if (orderableValuesHashJSON) {
            orderableValuesHash = new Backbone.Collection(orderableValuesHashJSON[attr_id], {
                model: Product.VariationAttribute.VariationAttributeValue
            }).models;
        }

        return orderableValuesHash;
    },

    getBundledProducts: function () {
        var bundledProducts = this._get('bundled_products');
        if (bundledProducts) {
            var self = this;
            bundledProducts.each(function (product) {
                if (!('apiCall' in product)) {
                    product.apiCall = function () {
                        return self.apiCall.apply(product, arguments);
                    };
                }
            });
            return bundledProducts.models;
        } else {
            return [];
        };
    },

    getSetProducts: function () {
        var setProducts = this._get('set_products');
        if (setProducts) {
            var self = this;
            setProducts.each(function (product) {
                if (!('apiCall' in product)) {
                    product.apiCall = function () {
                        return self.apiCall.apply(product, arguments);
                    };
                }
                if (!('getInventoryAvailability' in product)) {
                    product.getInventoryAvailability = function () {
                        return self.getInventoryAvailability.apply(product, arguments);
                    };
                }
            });
            return setProducts.models;
        } else {
            return [];
        }
    },

    ensureImagesLoaded: function (view_type) {
        var realViewType = this.getImageViewType(view_type),
            images = this._get('images');

        if (images[realViewType]) {
            logger.info('Product Images already loaded for view_type: ' + view_type);
            var deferred = _.Deferred();
            deferred.resolve();

            return deferred.promise();
        }

        logger.info('Looking up images, by individual view_type: ' + view_type);

        var self = this;
        var productImages = new ProductImages();

        var url = this.url() + '/images';

        if (view_type) {
            url += '?view_type=' + realViewType;
        }

        var params = _.extend({}, {
            url: url,
            type: 'GET'
        });
        var options = _.extend({}, {
            cache: true,
            wait: true,
            success: function (response) {
                parseImageGroups(response, images);
                logger.info(JSON.stringify(self._get('images')));
            },
            error: function () {}
        }, options);

        return this.apiCall(productImages, params, options);
    },

    fetchModel: function (options) {
        return this.fetch(options);
    },

    fetchSelectedDetails: function (options) {
        var selectedVariant = this.getSelectedVariant();
        var self = this;
        var selectedId = this.getId();
        var selectedProduct = this;
        var expand = 'availability';
        if (selectedVariant) {
            selectedId = selectedVariant.getProductId() || selectedVariant.getId();
            selectedProduct = selectedVariant;
            if (!('hasPrices' in selectedVariant) || !selectedVariant.hasPrices()) {
                expand = 'availability,prices,promotions';
            }
        }

        var product = Alloy.createModel('product', {
            id: selectedId,
            expand: expand
        });
        var promise = product.fetchModel({
            cache: false
        });
        promise.done(function (model, params, options) {
            model.getLowestPromotionPrice();
            selectedProduct.set({
                inventory: model._get('inventory'),
                inventories: model._get('inventories'),
                type: model._get('type')
            }, {
                silent: true
            });
            self.trigger('change:selected_details', model);
        });
        return promise;
    },

    getAvailableAttributes: function () {
        var availableAttributes = {};

        var orderable_variation_matrix = this._get('orderable_variation_matrix');
        var orderable_variation_values = this._get('orderable_variation_values');
        var variation_values = this._get('variation_values') || {},
            vvkeys = _.keys(variation_values);
        var vas = this._get('variation_attributes');

        if (vas.length == 1) {
            var loneAttribute = vas.at(0),
                attributeId = loneAttribute.get('id');
            var ovvs = orderable_variation_values[attributeId];
            var hash = {};
            _.each(ovvs, function (vv) {
                hash[vv.value] = true;
            });
            availableAttributes[loneAttribute._get('id')] = hash;
        } else if (vvkeys.length == 1) {
            var lookupHash = orderable_variation_matrix[vvkeys[0]][variation_values[vvkeys[0]]];
            vas.each(function (va) {
                var attributeHash = {},
                    attributeId = va.get('id');

                if (attributeId == vvkeys[0]) {
                    var ovvs = orderable_variation_values[attributeId];
                    var hash = {};
                    _.each(ovvs, function (vv) {
                        hash[vv.value] = true;
                    });
                    availableAttributes[attributeId] = hash;
                } else if (lookupHash) {
                    availableAttributes[attributeId] = lookupHash[attributeId];
                }
            });
        } else if (vvkeys.length > 1) {
            vas.each(function (va) {
                var attributeId = va.get('id');

                var intersectionHash = {};
                _.each(vvkeys, function (attr_id) {
                    if (attr_id == attributeId) {
                        return;
                    }
                    if (!orderable_variation_matrix[attr_id]) {
                        return;
                    }

                    var attributeHashes = orderable_variation_matrix[attr_id][variation_values[attr_id]];
                    var valuesHash = attributeHashes ? orderable_variation_matrix[attr_id][variation_values[attr_id]][attributeId] : null;

                    if (valuesHash) {
                        var valuesArray = _.keys(valuesHash);

                        if (intersectionHash[attributeId]) {
                            intersectionHash[attributeId] = _.intersection(intersectionHash[attributeId], valuesArray);
                        } else {
                            intersectionHash[attributeId] = valuesArray;
                        }
                    }
                });
                var hash = {};
                _.each(intersectionHash[attributeId], function (value) {
                    hash[value] = true;
                });
                availableAttributes[attributeId] = hash;
            });
        } else {
            var attr_ids = _.keys(orderable_variation_matrix);
            var orderable_variation_values = this._get('orderable_variation_values');

            _.each(attr_ids, function (attr_id) {
                var allPossibleValues = _.keys(orderable_variation_values[attr_id]);
                var hash = {};
                _.each(allPossibleValues, function (value_id) {
                    hash[value_id] = true;
                });
                availableAttributes[attr_id] = hash;
            });
        }

        return availableAttributes;
    },

    getProductPrices: function () {
        var self = this;

        var getPrices = new ProductPrices();

        var url = this.url() + '?expand=prices,promotions';
        var params = _.extend({}, {
            url: url,
            type: 'GET'
        });
        var options = _.extend({}, {
            wait: true,
            success: function (response) {
                var promotions = response.product_promotions;
                if (promotions && promotions[0]) {
                    self.set({
                        promo_price: promotions[0].promotional_price
                    }, {
                        silent: true
                    });
                }
                if (response.price) {
                    self.set({
                        price: response.price
                    }, {
                        silent: true
                    });
                }
                if (response.prices) {
                    self.set({
                        prices: response.prices
                    }, {
                        silent: true
                    });
                }
            },
            error: function () {}
        }, options);
        return this.apiCall(getPrices, params, options);
    },

    getSalePrice: function () {
        if (this.getPrices() && this.getPrices()[Alloy.CFG.country_configuration[Alloy.CFG.countrySelected].list_price_book]) {
            var listPrice = this.getPrices()[Alloy.CFG.country_configuration[Alloy.CFG.countrySelected].list_price_book];
            var salePrice = this.getPrice();
            if (listPrice != salePrice) {
                return salePrice;
            }
        }
        return null;
    },

    getListPrice: function () {
        var listPriceBook = Alloy.CFG.country_configuration[Alloy.CFG.countrySelected].list_price_book;
        if (this.getPrices() && this.getPrices()[listPriceBook]) {
            return this.getPrices()[listPriceBook];
        }
        return null;
    },

    getVariationAttributeByName: function (attribute_name) {
        var vas = this.getVariationAttribute(attribute_name);
        if (this._get('variation_values')) {
            var attributeToMatch = this._get('variation_values')[attribute_name];
            if (vas) {
                var toReturn = _.filter(vas._get('values').models, function (va) {
                    return va._get('value') == attributeToMatch;
                });
                if (toReturn && toReturn.length > 0) {
                    return toReturn[0]._get('name');
                }
            }
        }
        return null;
    },

    getVariationAttributeDisplayName: function (attribute_name) {
        var vas = this.getVariationAttribute(attribute_name);
        return vas._get('name');
    },

    getLowestPromotionPrice: function () {
        var promotions = this.get('product_promotions');
        if (promotions && promotions.length > 0) {
            var lowest = _.min(promotions.models, function (promotion) {
                return promotion.get('promotional_price');
            });
            if (lowest && lowest != Infinity) {
                this.set('promo_price', lowest.get('promotional_price'), {
                    silent: true
                });
            }
        }
    },

    getSelectedProductInfo: function () {
        if (this.isBundle()) {
            return this.getBundleProductInfo();
        } else {
            return this.getProductInfo();
        }
    },

    getProductInfo: function () {
        var selectedVariant = this.getSelectedVariant();
        if (!selectedVariant && (this.isStandard() || this.isVariant())) {
            selectedVariant = this;
        }
        if (!selectedVariant) {
            return null;
        }

        var productInfo = {
            product_id: selectedVariant.getProductId(),
            quantity: this.getQuantity() || 1
        };

        var selectedOption = this.getSelectedOptionInfo();
        if (selectedOption) {
            productInfo.option_items = selectedOption;
        }

        return productInfo;
    },

    getBundleProductInfo: function () {
        var productInfo = {
            product_id: this.getProductId(),
            quantity: this.getQuantity() || 1
        };

        var selectedOption = this.getSelectedOptionInfo();
        if (selectedOption) {
            productInfo.option_items = selectedOption;
        }
        _.each(this.getBundledProducts(), function (product) {
            if (product.hasSelectedOption()) {
                var option = product.getSelectedOptionInfo();
                if (option) {
                    if (productInfo.option_items) {
                        productInfo.option_items.concat(option);
                    } else {
                        productInfo.option_items = option;
                    }
                }
            }
        });
        return productInfo;
    },

    getSelectedOptionInfo: function () {
        var items = null;
        var selectedOption = this.getSelectedOption();
        if (selectedOption && selectedOption.selected) {
            var option = this.getOptions()[0];
            items = [{
                option_id: option.getId(),
                option_value_id: selectedOption.id,
                item_text: selectedOption.displayText,
                price: selectedOption.price,
                quantity: selectedOption.quantity || 1
            }];
        }
        return items;
    }
}, {});

function parseImageGroups(response, imagesIn) {
    logger.info('start image parse');

    var images = imagesIn || {},
        image_groups = response.image_groups;

    _.each(image_groups, function (image_group) {
        var variation_value = 'default';
        if (image_group.variation_attributes) {
            var var_attributes = image_group.variation_attributes[0];
            variation_value = var_attributes.values[0].value;
        }

        var view_type = image_group.view_type;
        var view_hash = images[view_type] || (images[view_type] = {});
        view_hash[variation_value] = image_group;
    });

    response.images = images;

    logger.info('after images parse');
}

function parseVariations(response) {
    if (response.variation_attributes) {
        logger.info('start orderable variation values parse');

        response.orderable_variation_values = {};
        response.original_attributes = response.variation_attributes.slice(0);

        _.each(response.variation_attributes, function (va) {
            var orderableValues = _.filter(va.values, function (n) {
                return n.orderable;
            });
            response.orderable_variation_values[va.id] = orderableValues;

            if (Alloy.CFG.product.filterUnorderableVariationValues) {
                va.values = orderableValues;
            }

            if (Alloy.CFG.product.shouldSelectSingleValues) {
                if (orderableValues.length == 1) {
                    var variationValues = _.extend({}, response.variation_values);
                    variationValues[va.id] = orderableValues[0].value;
                    response.variation_values = variationValues;
                }
            }

            if (Alloy.CFG.product.shouldSelectFirstValues) {
                if (orderableValues.length > 0 && (!response.variation_values || !response.variation_values[va.id]) && _.indexOf(Alloy.CFG.product.shouldSelectFirstValues, va.id) > -1) {

                    var variationValues = _.extend({}, response.variation_values);
                    variationValues[va.id] = orderableValues[0].value;
                    response.variation_values = variationValues;
                }
            }
        });

        var variants = [];

        logger.info('start orderable variants parse');

        var op = Alloy.CFG.product.filterUnorderableVariants ? 'filter' : 'each';
        var keys = _.keys(response.variants[0].variation_values);
        var lookup_hash = {};
        _[op](response.variants, function (n) {
            var value = _.extend({
                product_id: n.product_id,
                orderable: n.orderable,
                variation_values: n.variation_values
            }, n.variation_values);
            if (n.orderable) {
                variants.push(value);

                _.each(keys, function (attribute_id) {
                    var valueValue = value[attribute_id],
                        attrHash,
                        valueHash;
                    attrHash = lookup_hash[attribute_id] = lookup_hash[attribute_id] || {};
                    valueHash = lookup_hash[attribute_id][valueValue] = lookup_hash[attribute_id][valueValue] || {};

                    _.each(keys, function (attr_id) {
                        if (attr_id == attribute_id && keys.length > 1) {
                            return;
                        }

                        var valueValue1 = value[attr_id],
                            attrHash1,
                            valueHash1;
                        attrHash1 = valueHash[attr_id] = valueHash[attr_id] || {};
                        valueHash[attr_id][valueValue1] = true;
                    });
                });
            }

            return n.orderable;
        });

        response.orderable_variation_matrix = lookup_hash;
        response.orderable_variants = variants;

        logger.info('end orderable variants parse');
    }
}

var mixinApiMethods = require('ocapi_methods');

mixinApiMethods(Product, require('dw/shop/metadata/product'));

module.exports = Product;