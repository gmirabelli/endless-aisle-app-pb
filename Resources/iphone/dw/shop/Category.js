

var Backbone = Backbone || Alloy ? Alloy.Backbone : require('backbone');

var AssociatedModel = AssociatedModel || Alloy ? Backbone.AssociatedModel : require('backbone-associations').AssociatedModel;

var Category = AssociatedModel.extend({
    initialize: function () {},

    url: function () {
        return '/categories/' + this.getId();
    },

    queryParams: function () {
        return {
            levels: 2
        };
    },

    parse: function (response, options) {
        if (response.data && response.data.length) {
            var pcatId = response.data[0].parent_category_id || 'root';
            var cat = {
                id: pcatId,
                name: pcatId,
                categories: response.data
            };

            return cat;
        }

        return response;
    },

    getAllLevels: function () {
        var deferred = new _.Deferred();
        var menuCategories = this.getCategories();
        var ocapiMax = 50,
            count = Math.floor(menuCategories.length / ocapiMax);
        for (var i = 0; i < menuCategories.length; i += ocapiMax) {
            var sizedCategories = menuCategories.slice(i, i + ocapiMax);
            this.getMenuCategories(sizedCategories).always(function () {
                if (count == 0) {
                    deferred.resolve();
                }
                count--;
            });
        }
        return deferred.promise();
    },

    getMenuCategories: function (menuCategories) {
        var deferred = new _.Deferred(),
            m = Alloy.createModel('category'),
            parent = this,
            ids = _.pluck(menuCategories, 'id');
        ids = _.map(ids, function (id) {
            return encodeURIComponent(id);
        });
        m.id = '(' + ids.join(',') + ')';
        m.set('id', m.id);
        return m.fetch().always(function () {
            var parentCategories = parent.getCategories();
            parentCategories.push(m.getCategories());
            parent.setCategories(parentCategories);
            deferred.resolve();
        });
    },

    getVisibleCategories: function () {
        var attribute = Alloy.CFG.category.attribute_show_in_endless_aisle;

        return this.getCategories().filter(function (category) {
            return category.get(attribute) === true;
        });
    }
}, {});

var mixinApiMethods = require('ocapi_methods');

mixinApiMethods(Category, require('dw/shop/metadata/category'));

module.exports = Category;