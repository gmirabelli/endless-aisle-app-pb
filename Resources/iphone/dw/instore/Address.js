

var Backbone = Backbone || Alloy ? Alloy.Backbone : require('backbone');

var AssociatedModel = AssociatedModel || Alloy ? Backbone.AssociatedModel : require('backbone-associations').AssociatedModel;
var getAddressStringFromAddressDataOrderAndType = require('EAUtils').getAddressStringFromAddressDataOrderAndType;

var Address = AssociatedModel.extend({
  initialize: function () {},

  getFirstName: function () {
    return this.get('first_name');
  },

  getLastName: function () {
    return this.get('last_name');
  },

  getFullName: function () {
    return this.get('full_name');
  },

  getPostalCode: function () {
    return this.get('postal_code');
  },

  getStateCode: function () {
    return this.get('state_code');
  },

  getCountryCode: function () {
    return this.get('country_code');
  },

  getAddress1: function () {
    return this.get('address1');
  },

  getAddress2: function () {
    return this.get('address2');
  },

  getCity: function () {
    return this.get('city');
  },

  getPhone: function () {
    return this.get('phone');
  },

  getAddressId: function () {
    return this.get('address_id');
  },

  getAddressDisplay: function (addressType) {
    return getAddressStringFromAddressDataOrderAndType(this.toJSON(), require(Alloy.CFG.addressform).getAddressOrder(), addressType);
  },

  isPreferredAddress: function () {
    return this.get('preferred');
  },

  setAddressId: function (name, options) {
    this.set('address_id', name, options);
  },

  setAddress2: function (address2, options) {
    this.set('address2', address2, options);
  },

  setCountryCode: function (code, options) {
    this.set('country_code', code, options);
  },

  setPhone: function (phone, options) {
    this.set('phone', phone, options);
  }
}, {});

module.exports = Address;