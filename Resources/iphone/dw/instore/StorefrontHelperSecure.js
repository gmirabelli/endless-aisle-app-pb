

var Backbone = Backbone || Alloy ? Alloy.Backbone : require('backbone');

var StorefrontHelperSecure = Backbone.Model.extend({
  sync: require('alloy/sync/storefront').sync,

  config: {
    secure: true,
    model_name: 'storefrontHelper'
  },

  setModelName: function (modelName) {
    this.config.model_name = modelName;
  }
});

module.exports = StorefrontHelperSecure;