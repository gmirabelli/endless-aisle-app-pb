#!/bin/sh
# ©2013-2018 salesforce.com, inc. All rights reserved.

PROG=$(basename "$0")
usage="${PROG} [guid] [appid] [device type] [client id] [storefront host] [build type] [storefront site]
Script to build tiapp.xml and user.js file depending on arguments.

where:
  Required to build tiapp.xml:
    [guid] Appcelerator guid 
        Example: aaeac944-1870-44fe-9aaa-67d56e0de42d
    [appid] application id 
        Example: com.testcorp.endlessaisle

  Optional to build tiapp.xml:
    [device type] payment terminal type ( Verifone | Adyen | PTW )
        Default: Verifone
        Example: Adyen

  Required input when building user.js as well as tiapp.xml:
    [client id] OCAPI client id
        Example: B398394B-31BE-4E95-9382-B7C60CDF1CEC
    [storefront host] storefront host
        Example: dev01-testcorp-dw.demandware.net

  Optional input to build user.js as well as tiapp.xml:
    [build type] build type ( Release | Test )
        Default: Release
        If Test then adds allow_simulate_payment and test_configuration_url to user.js, otherwise Release does nothing extra
        Example: Test
    [storefront site] storefront site to use instead of SiteGenesis
        Default: SiteGenesis
        Example: MySite

    Example to build tiapp.xml only for Verifone:

        ${PROG} aaeac944-1870-44fe-9aaa-67d56e0de42d com.testcorp.endlessaisle

    Example to build tiapp.xml only for Adyen:

        ${PROG} aaeac944-1870-44fe-9aaa-67d56e0de42d com.testcorp.endlessaisle Adyen

    Example to build tiapp.xml only for PTW:

        ${PROG} aaeac944-1870-44fe-9aaa-67d56e0de42d com.testcorp.endlessaisle PTW

    Example to build tiapp.xml and user.js for PTW against server dev01-testcorp-dw.demandware.net and SiteGenesis storefront (no automation):

        ${PROG} aaeac944-1870-44fe-9aaa-67d56e0de42d com.testcorp.endlessaisle PTW B398394B-31BE-4E95-9382-B7C60CDF1CEC dev01-testcorp-dw.demandware.net

    Example to build tiapp.xml and user.js for Verifone against server dev01-testcorp-dw.demandware.net with MySite storefront site for Automated tests:

        ${PROG} aaeac944-1870-44fe-9aaa-67d56e0de42d com.testcorp.endlessaisle Verifone B398394B-31BE-4E95-9382-B7C60CDF1CEC dev01-testcorp-dw.demandware.net Test MySite"

if [ -z $1 ]
then
    echo "$usage"
    exit
fi

### required for tiapp.xml
GUI_ID=$1
APP_ID=$2
### optional for tiapp.xml
DEVICE_TYPE=$3
### required for user.js
CLIENT_ID=$4
STOREFRONT_HOST=$5
### optional for user.js
BUILD_TYPE=$6
SITE_GEN_IN=$7


### Setup device specific config
TIAPP=tiapp.xml.sample.verifone
PAYMENT_TYPE="verifoneDevice"
if [ "${DEVICE_TYPE}" == "Adyen" ]; then
    TIAPP=tiapp.xml.sample.adyen
    PAYMENT_TYPE="adyenDevice"
elif [ "${DEVICE_TYPE}" == "PTW" ]; then
    TIAPP=tiapp.xml.sample.ptw
    PAYMENT_TYPE="webDevice"
fi

echo "Updating tiapp.xml"
echo "    Using sample ${TIAPP}"
echo "    Using gui id ${GUI_ID}"
echo "    Using app id ${APP_ID}"

### Make changes to the tiapp.xml files
sed "s/XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX/${GUI_ID}/g" ${TIAPP} | 
sed "s/<!-- insert new id here -->/${APP_ID}/g" > tiapp.xml

if [ -z ${CLIENT_ID} ]
then
    echo "Skipping user.js as there are not enough arguments"
    exit
fi

### Make changes to the config.json files
echo "Copying config.json.sample to config.json"
cp app/config.json.sample app/config.json

SITE_GEN="SiteGenesis"
if [ ! -z "${SITE_GEN_IN}" ]; then
    SITE_GEN="${SITE_GEN_IN}"
fi


EXTRA=""
if [ "${BUILD_TYPE}" == "Test" ]; then
    EXTRA="allow_simulate_payment : true, test_configuration_url : 'http://localhost/~jenkins/dss-test.json',"
fi

# Write out the user.js file
echo "Updating user.js"
echo "    Using storefront host ${STOREFRONT_HOST}"
echo "    Using device ${PAYMENT_TYPE}"
echo "    Using client id ${CLIENT_ID}"
echo "    Using site gen ${SITE_GEN}"
echo "    Using extra config ${EXTRA}"

sed "s/SiteGenesis/${SITE_GEN}/" app/assets/config/user.js.sample | 
sed "s%site_url : '/on/demandware.store/Sites-${SITE_GEN}-Site'%site_url : '/on/demandware.store/Sites-${SITE_GEN}-Site'%g" |
sed "s%storefront_home : '/on/demandware.store/Sites-${SITE_GEN}-Site',%\storefront_home : '/on/demandware.store/Sites-${SITE_GEN}-Site', ${EXTRA}%g" |
sed "s/<insert-hostname-here.demandware.net>/${STOREFRONT_HOST}/g" |
sed "s/verifoneDevice/${PAYMENT_TYPE}/g" |
sed "s/'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'/'${CLIENT_ID}'/g" > app/assets/config/user.js
