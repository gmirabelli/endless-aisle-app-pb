//
//  VFITableBarcodeResponse.h
//  VMF
//
//  Created by James on 10/10/16.
//  Copyright © 2016 VeriFone, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * Encapsulating data class utilized by VFIPinpad
 * Note: Please see VFI_Barcode_host.h to find what the various symbology types map out to
 */
@interface VFITableBarcodeResponse : NSObject {
    NSData *barcodeScanData;    //!< Barcode data returned from the T11 barcode scan response
    int code;                  //!< The Code symbology identifier returned from the T11 barcode scan response
    int symbol;                //!< The Symbol symbology identifier returned from the T11 barcode scan response
    int aim;                   //!< The AIM symbology identifier returned from the T11 barcode scan response
}
/**
 * clears all VFITableBarcodeResponse properties
 */
-(void)clear;

/**
 * Singleton instance of VFITableBarcodeResponse utilized by VFIPinpad
 */
+ (VFITableBarcodeResponse *)sharedController;

@property int code;
@property int symbol;
@property int aim;
#if !__has_feature(objc_arc)
@property (nonatomic, retain) NSData *barcodeScanData;
#else
@property (nonatomic, strong) NSData *barcodeScanData;
#endif


@end
