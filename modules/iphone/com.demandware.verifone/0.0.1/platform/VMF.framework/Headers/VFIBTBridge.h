//
//  VFIBTBridge.h
//  VMF
//

#import <Foundation/Foundation.h>
#import	<ExternalAccessory/ExternalAccessory.h>

@class VFIBTBridge;

@interface VFIBTBridge : NSObject {
}

/**
 * Framework Version.
 *
 * Returns VeriFone Mobile Framework Version.
 */
+(NSString* )frameworkVersion;

/**
 * Disable Bluetooth Bridge.
 *
 * This method removes bluetooth support from VMF.
 */
+(void) disableBTBridge;

/**
 * Disable Bluetooth Bridge Status.
 *
 * This method returns bluetooth disabled flag.
 */
+(BOOL) disabledBTBridge;

+(BOOL) notConnected;

/**
 * Control how framework handles connection when app goes to background
 *
 * When val is set to TRUE, all devices will stay connected in the background.
 * Note: This method works for sled devices as well as Bluetooth.
 */
+(void) setNoDisconnect:(BOOL)val;
+(BOOL) noDisconnect;

/**
 * Control how framework handles background thread usage
 *
 * When val is TRUE, VMF will run execute on a non-main thread
 * Note: This command needs to be performed during init and before initDevice
 */
+(void) setBackgroundMode:(BOOL)val;
+(BOOL) backgroundMode;

/**
 * Control how framework handles background thread usage
 *
 * When val is TRUE, VMF will run execute on a non-main thread
 * Note: This command needs to be performed during init and before initDevice
 */
+(void) setThreadMode:(BOOL)val thread:(NSThread*)thread;
+(BOOL) threadMode;

/**
 * Controls whether or not VMF expects payload length back in the response
 *
 * When val is TRUE, VMF will expect two bytes of length after STX in all responses from the pinpad. Default is off.
 
 \note
 This command needs to be performed during init and before initDevice
 *
 */
+(void) setSDIMode:(BOOL)val;
+(BOOL) sdiMode;

+(void) setInitAttempts:(float)val;

@end
