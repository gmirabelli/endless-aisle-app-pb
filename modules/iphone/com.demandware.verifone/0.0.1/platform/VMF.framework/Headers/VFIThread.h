//
//  VFIThread.h
//  VMF
//
//  Created by James on 6/15/16.
//  Copyright © 2016 VeriFone, Inc. All rights reserved.
//

//
//  VFIStream.h
//  VMF
//
//  Created by Randy Palermo on 9/11/13.
//  Copyright (c) 2013 VeriFone, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VFIBTBridge.h"

@interface VFIThread : NSObject {
    
    
    EAAccessory *_accessory;
    EASession *_session;
    NSString *_protocolString;

    bool _terminated;
}

@property (nonatomic) bool _terminated;

-(void) deallocAccessory;

+ (VFIThread *)sharedController;

#if !__has_feature(objc_arc)
@property (nonatomic, retain) NSThread *_thread;
@property (nonatomic, retain) NSRunLoop *_runLoop;
#else

@property (nonatomic, strong) NSThread *_thread;
@property (nonatomic, strong) NSRunLoop *_runLoop;
#endif

@end
