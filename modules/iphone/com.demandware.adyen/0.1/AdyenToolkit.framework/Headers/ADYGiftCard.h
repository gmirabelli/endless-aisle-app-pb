//
//  ADYGiftCard.h
//  Pods
//
//  Created by Oleg Lutsenko on 3/1/17.
//
//

#import <Foundation/Foundation.h>

#import "ADYGiftCardMask.h"

/**
 
 An `ADYGiftCard`, which is an NSObject that represents the shopper's gift card.

 */

@interface ADYGiftCard : NSObject

/**  holds the type of gift card.
*/
@property (strong, nonatomic) NSString *type;

/**  holds the gift card number.
*/
@property (strong, nonatomic) NSString *number;

/**  holds the expiry month of the card.
*/
@property (strong, nonatomic) NSString *expiryMonth;

/**  holds the expiry year of the card.
*/
@property (strong, nonatomic) NSString *expiryYear;

/**  holds a mask to validate the correct number of digits to enter.
*/
@property (strong, nonatomic) ADYGiftCardMask *mask;

@end
