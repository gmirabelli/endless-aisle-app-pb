//
// Created by Sam on 8/2/12.
//
//


#import <Foundation/Foundation.h>

@interface ADYAmount : NSObject

@property(nonatomic, copy) NSString *currency;
@property(nonatomic, copy) NSString *value;

- (instancetype)initWithCurrency:(NSString *)currency value:(NSString *)value;

@end
