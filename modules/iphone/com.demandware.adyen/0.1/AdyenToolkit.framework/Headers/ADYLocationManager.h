#import <CoreLocation/CoreLocation.h>

@interface ADYLocationManager : NSObject <CLLocationManagerDelegate>
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CLLocation *currentLocation;

+ (ADYLocationManager *)sharedInstance;
+ (BOOL)isEnabled;
+ (BOOL)isAuthorized;

- (void)determineLocation;
- (BOOL)locationKnown;

@end
