//
//  ADYGiftCardLoadRequestDelegate.h
//  Pods
//
//  Created by Oleg Lutsenko on 3/3/17.
//
//


@class ADYBalance;
@class ADYGiftCardLoadRequest;

/*
@protocol ADYGiftCardLoadRequestDelegate <NSObject>

// - provides additional data
// - balance aquired
// - loadRequestDidSucceed
// - loadRequestDidFailWithError:additional data

- (void)loadRequest:(ADYGiftCardLoadRequest *)request didAquireBalance:(ADYBalance *)balance additionalData:(NSDictionary *)additionalData;


@end
*/
