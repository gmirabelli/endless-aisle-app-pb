//
//  ADYReceipt.h
//  DeepThoughts
//
//  Created by Willem Lobbezoo on 29-04-12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ADYReceiptLine.h"

@interface ADYReceipt : NSObject

@property (strong, nonatomic) NSMutableArray *header;
@property (strong, nonatomic) NSMutableArray *content;
@property (strong, nonatomic) NSMutableArray *footer;

@property (strong, nonatomic) NSString *receiptType;

- (ADYReceipt *)init;

+ (ADYReceipt *)createTestReceipt;
+ (NSString *)getReceiptFormatted:(NSArray *)receipt;
+ (NSString *)getReceiptLineFormatted:(ADYReceiptLine *)receiptLine;

- (NSArray *)getReceiptCustomized;

- (NSString *)getFirstValueForReceiptLineKey:(NSString *)key;


// Private
- (NSString *)toXML;

@end
