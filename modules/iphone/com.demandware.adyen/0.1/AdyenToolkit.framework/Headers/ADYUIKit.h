//
//  ADYUIKit.h
//  Pods
//
//  Created by Taras Kalapun on 1/7/16.
//
//

#import <Foundation/Foundation.h>
#import "ADYConstants.h"

#if PLATFORM_IOS
#import <UIKit/UIKit.h>
#define ADYImageClassName UIImage
#define ADYColorClassName UIColor
#elif PLATFORM_OSX
#define ADYImageClassName NSImage
#define ADYColorClassName NSColor
#endif

@interface ADYUIKit : NSObject

+ (NSData *)pngRepresentationFromImage:(ADYImageClassName *)image;

/**
 * @param maxSize maximum resulting data-size, in kilobytes.
 */
+ (NSData *)signatureDataFromImage:(ADYImageClassName *)image withMaxSize:(NSUInteger)maxSize;

@end
