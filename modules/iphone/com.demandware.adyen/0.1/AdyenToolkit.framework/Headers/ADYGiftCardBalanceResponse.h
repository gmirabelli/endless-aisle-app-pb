//
//  ADYGiftCardBalanceResponse.h
//  Pods
//
//  Created by Oleg Lutsenko on 3/1/17.
//
//

#import <Foundation/Foundation.h>

@class ADYGiftCard;
@class ADYBalance;

/**
 
 An `ADYGiftCardBalanceResponse`, which is the response to a balance request, and includes the balance of the card.
 
  */

@interface ADYGiftCardBalanceResponse : NSObject

/**  a reference for the shopper's gift card
 */
@property (strong, nonatomic) ADYGiftCard *card;

/**   holds the current balance of the gift card.
 */
@property (strong, nonatomic) ADYBalance *balance;

/**   holds Additional data that the POS sends with the transaction.
 */
@property (strong, nonatomic) NSDictionary *additionalData;

/**   will be set if the transaction cannot be created for some reason.
 */
@property (strong, nonatomic) NSError *error;

@end
