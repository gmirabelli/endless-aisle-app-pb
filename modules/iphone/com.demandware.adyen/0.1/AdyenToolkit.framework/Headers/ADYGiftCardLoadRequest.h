//
//  ADYGiftCardLoadRequest.h
//  Pods
//
//  Created by Oleg Lutsenko on 2/27/17.
//
//

#import "ADYGiftCardRequest.h"


@class ADYAmount;
@class ADYBalance;
@class ADYGiftCardBalanceResponse;
@class ADYGiftCard;
@class ADYGiftCardLoadRequest;
@class ADYGiftCardLoadResponse;


typedef void (^ADYGiftCardConfirmLoadDetails)(ADYAmount * _Nullable updatedAmount, NSDictionary * _Nullable updatedAdditionalData);


NS_ASSUME_NONNULL_BEGIN

@protocol ADYGiftCardLoadRequestDelegate <NSObject>

- (void)loadRequest:(ADYGiftCardLoadRequest *)request didAquireCurrentBalance:(ADYBalance *)balance andAdditionalData:(nullable NSDictionary *)additionalData confirmLoadWithDetails:(ADYGiftCardConfirmLoadDetails)confirmLoad;

- (void)loadRequest:(ADYGiftCardLoadRequest *)request didFinishWithResponse:(ADYGiftCardLoadResponse *)response;

@end

NS_ASSUME_NONNULL_END


/**
 
 An `ADYGiftCardLoadRequest`, which is the start of loading a cash balance to a card.
 
  */

NS_ASSUME_NONNULL_BEGIN

@interface ADYGiftCardLoadRequest : ADYGiftCardRequest

/**  holds the amount currently loaded to the card
 */
@property (strong, nonatomic) ADYAmount *amount;

/**  a merchant-supplied reference for the transaction, as an `NSString`.
 */
@property (strong, nonatomic) NSString *reference;

/**  specifies the type of load
    i.e. `load`, `merchandise_return`, `activate`.
 */
@property (strong, nonatomic) NSString *loadType;

/**  a reference for the shopper's gift card
 */
@property (strong, nonatomic, nullable) ADYGiftCard *card;

/**  holds Additional data that the POS sends with the transaction.
 */
@property (strong, nonatomic, nullable) NSDictionary *additionalData;

/**  holds selected tender options that apply to the transaction
 */
@property (strong, nonatomic, nullable) NSArray *tenderOptions;


- (void)startWithDelegate:(id<ADYGiftCardLoadRequestDelegate>)delegate;

- (void)cancel;

@end

NS_ASSUME_NONNULL_END
