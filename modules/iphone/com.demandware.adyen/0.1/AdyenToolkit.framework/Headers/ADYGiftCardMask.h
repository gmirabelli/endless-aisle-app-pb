//
//  ADYGiftCardMask.h
//  Pods
//
//  Created by Oleg Lutsenko on 3/1/17.
//
//

#import <Foundation/Foundation.h>

/**
 
 An `ADYGiftCardMask`, which is an NSObject that represents the card mask, used to validate the correct number of digits to enter .
 
  */

@interface ADYGiftCardMask : NSObject

/**  holds the type of gift card.
*/
@property (strong, nonatomic, nullable) NSString *mask;

/**   holds the minimum number of characters in the card mask.
*/
@property (strong, nonatomic, nullable) NSString *maskMin;

/**   holds the maximum number of characters in the card mask.
*/
@property (strong, nonatomic, nullable) NSString *maskMax;

@end
