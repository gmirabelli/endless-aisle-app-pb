//
//  ADYGiftCardLoadResponse.h
//  Pods
//
//  Created by Oleg Lutsenko on 3/3/17.
//
//

#import <Foundation/Foundation.h>


@class ADYGiftCard;
@class ADYBalance;
@class ADYError;


NS_ASSUME_NONNULL_BEGIN

@interface ADYGiftCardLoadResponse : NSObject

@property (strong, nonatomic, nullable) ADYGiftCard *card;

@property (strong, nonatomic, nullable) ADYBalance *balance;

@property (strong, nonatomic, nullable) NSDictionary *additionalData;

@property (strong, nonatomic, nullable) ADYError *error;

@end

NS_ASSUME_NONNULL_END
