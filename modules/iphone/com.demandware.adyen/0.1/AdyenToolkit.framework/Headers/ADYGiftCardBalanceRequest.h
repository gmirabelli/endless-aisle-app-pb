//
//  ADYGiftCardBalanceInquiryOperation.h
//  Pods
//
//  Created by Oleg Lutsenko on 2/27/17.
//
//

#import "ADYGiftCardRequest.h"


@class ADYBalance;
@class ADYGiftCardMask;
@class ADYGiftCard;
@class ADYGiftCardBalanceResponse;


typedef void (^ADYGiftCardBalanceCompletion)(ADYGiftCardBalanceResponse * _Nonnull response);


/**
 
 An `ADYGiftCardBalanceRequest`, which is the start of requesting a balance and relevant additional data from a card.
 
  */

NS_ASSUME_NONNULL_BEGIN

@interface ADYGiftCardBalanceRequest : ADYGiftCardRequest

/**  a reference for the shopper's gift card
 */
@property (strong, nonatomic, nullable) ADYGiftCard *card;

/**  holds Additional data that the POS sends with the transaction.
 */
@property (strong, nonatomic, nullable) NSDictionary *additionalData;

/**  holds selected tender options that apply to the transaction
 */
@property (strong, nonatomic, nullable) NSArray *tenderOptions;


- (void)startWithCompletion:(ADYGiftCardBalanceCompletion)completion;

- (void)cancel;

@end

NS_ASSUME_NONNULL_END
