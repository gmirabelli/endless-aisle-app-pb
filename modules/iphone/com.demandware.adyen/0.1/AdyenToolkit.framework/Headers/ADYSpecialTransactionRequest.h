//
//  ADYSpecialTransactionRequest.h
//  AdyenToolkit
//
//  Created by Diego Marcon on 03/10/2017.
//

#import "ADYTransactionRequest.h"

/**
 Request for a special payment transaction.
 
 Obtain a request by calling `createSpecialTransactionRequestWithReference:` on the `ADYDevice` instance of choice. Then,
 fill in all required properties. Finally, start the transaction by calling startWithDelegate:. The
 delegate will be informed of the transaction's progress, and when the transaction is complete.
 
 You can call `requestCancel` to request cancellation of the running transaction.
 */
@interface ADYSpecialTransactionRequest: NSObject

/** A merchant-supplied reference for the transaction */
@property (nonatomic, strong) NSString *reference;

/** Payment method type */
@property (nonatomic, strong) NSString *paymentMethodType;

/** Transaction type */
@property (nonatomic, strong) NSString *transactionType;

/** Optional dictionary with transaction options. Only NSString typed values are allowed */
@property (nonatomic, strong) NSDictionary *options;

/** The currency to use on the transaction. */
@property (nonatomic, strong) NSString *currency;

/** The amount to process on the transaction in minor units (cents). */
@property (nonatomic, strong) NSNumber *amount;

/** The amount to process on the transaction in minor units (cents). */
@property (nonatomic) BOOL handleReceipt;

/** Optional dictionary to specify RAW additionalData keys and values to be passed to tender */
@property (nonatomic, strong) NSMutableDictionary *additionalData;

/**
 * Starts the transaction.
 *
 * @param delegate this delegate will be informed of the transaction's progress and completion.
 * @param error if the transaction cant be started, the error will be set.
 */
- (BOOL)startWithDelegate:(id<ADYTransactionProcessorDelegate>)delegate error:(NSError **)error;

/**
 * Requests for the transaction to be cancelled.
 * This is only a _request_ for cancellation. It is up to the payment device to honour this request
 * or not. This method returns immediately, the delegate given in the call to
 * startWithDelegate: will continue to receive updates on the transaction's status, and completion. If
 * cancellation is successful, the status of the completed transaction will be `FinalStateCANCELLED`,
 * but if the transaction could not be cancelled, it will be one of the other final states.
 */
- (void)requestCancel;

@end
