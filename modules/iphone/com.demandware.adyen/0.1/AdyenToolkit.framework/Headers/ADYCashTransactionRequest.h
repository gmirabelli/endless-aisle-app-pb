//
//  ADYCashTransactionRequest.h
//  Pods
//
//  Created by Oleg Lutsenko on 3/15/16.
//
//

#import <Foundation/Foundation.h>

@class ADYTransactionData;

@interface ADYCashTransactionRequest : NSObject

/** The currency to use on the transaction. */
@property (nonatomic, strong) NSString *currency;

/** The amount to process on the transaction in minor units (cents). */
@property (nonatomic, strong) NSNumber *amount;

/** The (merchant) reference for the transacion */
@property (nonatomic, strong) NSString *reference;

/**
 * Starts the Cash transaction asynchronous request and invokes the `completion` block when finished.
 *
 * If something went wrong during the request, the error object will contain the details.
 *
 * If the `result` object is not nil, the request was successfull. The `finalState` will indicate
 * whether the transaction was APPROVED, DECLINED or ERROR. Check the `declineInfo` and `errorInfo` properties
 * for more info.
 */
- (void)startWithCompletion:(void (^)(ADYTransactionData *result, NSError *error))completion;

@end
