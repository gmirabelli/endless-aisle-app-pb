//
//  ADYTransactionDetails.h
//  Pods
//
//  Created by Oleg Lutsenko on 5/23/17.
//
//

#import <Foundation/Foundation.h>

@class ADYAmount;
@class ADYReceipt;

@interface ADYTransactionDetails : NSObject

/**
 * Code indicating state of the transaction.
 *
 * Possible values are:
 *
 * - "APPROVED"
 * - "DECLINED"
 * - "CANCELLED"
 * - "ERROR"
 * - "PROCESSING_TENDER"
 */
@property (strong, nonatomic) NSString *state;

@property (strong, nonatomic) ADYAmount *amount;
@property (strong, nonatomic) NSString *tenderReference;
@property (strong, nonatomic) NSString *pspReference;
@property (strong, nonatomic) NSString *merchantReference;
@property (strong, nonatomic) NSString *timestamp;
@property (strong, nonatomic) NSString *capturePending;
@property (strong, nonatomic) NSString *transactionType;
@property (strong, nonatomic) NSString *maskedPan;
@property (strong, nonatomic) NSString *posEntryMode;

@property (strong, nonatomic) NSString *authCode DEPRECATED_MSG_ATTRIBUTE("This property will be removed on next library version.");
@property (nonatomic, strong) NSMutableDictionary *additionalData DEPRECATED_MSG_ATTRIBUTE("This property will be removed on next library version.");

@property (strong, nonatomic) NSArray<ADYReceipt *> *merchantReceipts;
@property (strong, nonatomic) NSArray<ADYReceipt *> *cardHolderReceipts;

@end
