// This is a test harness for your module
// You should do something interesting in this harness
// to test out the module and to provide instructions
// to users on how to use it by example.


// open a single window
var win = Ti.UI.createWindow({
	backgroundColor:'white'
});
var label = Ti.UI.createLabel();
win.add(label);
win.open();

// TODO: write your module tests here
var bluetooth_device_picker = require('com.demandware.ios.bluetoothDevicePicker');
Ti.API.info("module is => " + bluetooth_device_picker);

if (OS_IOS) {
	var button = Ti.UI.createButton({
		title: "Creating an example Proxy",
		backgroundColor: "red",
		width: 100,
		height: 100,
		top: 100,
		left: 150,
		color:"white"
	});
	button.addEventListener('click',function(e){
		bluetooth_device_picker.showBluetoothDevicePicker();
	});
	bluetooth_device_picker.addEventListener('pickerCancelled',function(e){
		alert('pickerCancelled');
	});
	bluetooth_device_picker.addEventListener('pickerDismissed',function(e){
		alert('pickerDismissed');
	});
	bluetooth_device_picker.addEventListener('deviceConnected',function(e){
		alert('deviceConnected');
	});
	win.add(button);
}

