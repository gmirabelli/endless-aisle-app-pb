// ©2013-2018 salesforce.com, inc. All rights reserved.
/**
 * assets/config/devices.js  - configuration for devices
 */

module.exports = {
    devices : {
        // The payment terminal, either 'verifoneDevice', 'adyenDevice', or 'webDevice'.
        // If using ‘webDevice’ be sure to set payment_entry from main.js to 'web' in your user.js file.
        payment_terminal_module : 'verifoneDevice',
        // The barcode scanner module to load at startup
        barcode_scanner_module : 'barcodeScanner',
        // The printer module to load at startup
        printer_module : 'epsonPrinter',

        // The url to use for validation
        validate_url : 'EAStore-ValidateDevice',

        // Enables/disables the bluetooth device picker functionality on the 'no connection to payment device' dialog.
        // Requires including com.demandware.ios.bluetoothdevicepicker module in tiapp.xml and using 4.1.0SDK.
        // Default is false
        bluetoothDevicePicker : false,

        // verifone specific settings
        verifone : {
            // The amount of time to wait for a card to actually be swiped once a transaction begins (specify seconds)
            card_swipe_timeout : 120,
            registart_url : 'Verifone-ActivateDevice'
        },

        // adyen specific settings
        adyen : {
            paymentStrings : {
                // The cardType should be lowercase and is what we get back from the Adyen module for cardString
                mc : {
                    // You can specify a default string and any localized strings
                    default : "Mastercard",
                    de : "MasterCard",
                    fr : "MasterCard",
                    ja : "マスターカード",
                    "zh-CN" : "万事达"
                },
                visa : {
                    default : "Visa",
                    ja : "ビザ",
                    "zh-CN" : "签证"
                },
                // You can also specify a single string if you do not need localization
                alipay : "AliPay",
                wechatpay_pos : "WeChat",
                amex : "Amex",
                discover : "Discover",
                diners : "Discover",
                maestro : "Mastercard",
                jcb : "JCB",
                cup : "China UnionPay",
                vpay : "Visa"
            }
        }
    }
};
