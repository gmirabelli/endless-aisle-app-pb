// ©2013-2018 salesforce.com, inc. All rights reserved.
/**
 * controllers/customer/components/profile.js - Functions for viewing customer profile
 */

//---------------------------------------------------
// ## VARIABLES

var logger = require('logging')('customer:components:profile', getFullControllerPath($.__controllerPath));


exports.init = init;
exports.deinit = deinit;

//---------------------------------------------------
// ## FUNCTIONS FOR VIEW/CONTROLLER LIFECYCLE

/**
 * INIT
 *
 * @api public
 */
function init() {
    logger.info('Calling INIT');
    var deferred = new _.Deferred();
    
    setTimeout(function(){
    	deferred.resolve();
	}, 500);

    return deferred.promise();
}


/**
 * DEINIT
 *
 * @api public
 */
function deinit() {
    logger.info('Calling deinit');
    $.stopListening();
    $.destroy();
}

