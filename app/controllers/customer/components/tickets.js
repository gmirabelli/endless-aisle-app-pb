// ©2013-2018 salesforce.com, inc. All rights reserved.
/**
 * controllers/customer/components/history.js - Functions for handling Customer History
 */

//---------------------------------------------------
// ## VARIABLES

var logger = require('logging')('customer:components:tickets', getFullControllerPath($.__controllerPath));


//---------------------------------------------------
// ## PUBLIC API

exports.init = init;
exports.render = render;
exports.deinit = deinit;

//---------------------------------------------------
// ## FUNCTIONS FOR VIEW/CONTROLLER LIFECYCLE

/**
 * INIT
 *
 * @return {Deferred} promise
 * @api public
 */
function init() {
    logger.info('Calling INIT');
    var deferred = new _.Deferred();
    
    setTimeout(function(){
    	deferred.resolve();
	}, 500);

    return deferred.promise();

}

/**
 * RENDER
 *
 * @api public
 */
function render() {
    logger.info('Calling RENDER');
}

/**
 * DEINIT
 *
 * @api public
 */
function deinit() {
    logger.info('Calling deinit');
    //$.tickets_image.deinit();
    $.stopListening();
    $.destroy();
}

