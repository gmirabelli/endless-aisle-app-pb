//
//  Appcelerator Titanium Mobile
//  WARNING: this is a generated file and should not be modified
//

#import <UIKit/UIKit.h>
#define _QUOTEME(x) #x
#define STRING(x) _QUOTEME(x)

NSString * const TI_APPLICATION_DEPLOYTYPE = @"development";
NSString * const TI_APPLICATION_ID = @"com.petitbateau.endlessaisle";
NSString * const TI_APPLICATION_PUBLISHER = @"salesforce.com";
NSString * const TI_APPLICATION_URL = @"http://www.commercecloud.com";
NSString * const TI_APPLICATION_NAME = @"EndlessAisle";
NSString * const TI_APPLICATION_VERSION = @"2.4.0";
NSString * const TI_APPLICATION_DESCRIPTION = @"In Store Commerce brought to you by Salesforce Commerce Cloud";
NSString * const TI_APPLICATION_COPYRIGHT = @"©2013-2018 salesforce.com, inc. All rights reserved.";
NSString * const TI_APPLICATION_GUID = @"1a082ba0-4862-41d0-9eee-0c288eb3dc9d";
BOOL const TI_APPLICATION_ANALYTICS = false;
BOOL const TI_APPLICATION_SHOW_ERROR_CONTROLLER = true;
NSString * const TI_APPLICATION_BUILD_TYPE = @"";

#ifdef TARGET_IPHONE_SIMULATOR
NSString * const TI_APPLICATION_RESOURCE_DIR = @"";
#endif

int main(int argc, char *argv[]) {
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];

	int retVal = UIApplicationMain(argc, argv, nil, @"TiApp");
    [pool release];
    return retVal;
}
