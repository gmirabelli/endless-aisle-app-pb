
(function () {
    "use strict";

    var root = this;

    var _, Backbone, BackboneModel, BackboneCollection, ModelProto, defaultEvents, AssociatedModel, pathChecker;

    if (typeof require !== 'undefined') {
        _ = require('alloy/underscore');
        Backbone = require('alloy/backbone');
        exports = module.exports = Backbone;
    } else {
        _ = root._;
        Backbone = root.Backbone;
    }

    BackboneModel = Backbone.Model;
    BackboneCollection = Backbone.Collection;
    ModelProto = BackboneModel.prototype;
    pathChecker = /[\.\[\]]+/g;

    defaultEvents = ["change", "add", "remove", "reset", "destroy", "sync", "error", "sort", "request"];

    Backbone.Many = "Many";
    Backbone.One = "One";

    AssociatedModel = Backbone.AssociatedModel = BackboneModel.extend({
        relations: undefined,

        _proxyCalls: undefined,

        get: function (attr) {
            var obj = ModelProto.get.call(this, attr);
            return obj ? obj : this.getAttr.apply(this, arguments);
        },

        set: function (key, value, options) {
            var attributes,
                attr,
                modelMap,
                modelId,
                obj,
                result = this;

            if (_.isObject(key) || key == null) {
                attributes = key;
                options = value;
            } else {
                attributes = {};
                attributes[key] = value;
            }
            if (!attributes) return this;
            for (attr in attributes) {
                modelMap || (modelMap = {});
                if (attr.match(pathChecker)) {
                    var pathTokens = getPathArray(attr),
                        initials = _.initial(pathTokens),
                        last = pathTokens[pathTokens.length - 1],
                        parentModel = this.get(initials);
                    if (parentModel instanceof AssociatedModel) {
                        obj = modelMap[parentModel.cid] || (modelMap[parentModel.cid] = {
                            model: parentModel,
                            data: {}
                        });
                        obj.data[last] = attributes[attr];
                    }
                } else {
                    obj = modelMap[this.cid] || (modelMap[this.cid] = {
                        model: this,
                        data: {}
                    });
                    obj.data[attr] = attributes[attr];
                }
            }
            if (modelMap) {
                for (modelId in modelMap) {
                    obj = modelMap[modelId];
                    this.setAttr.call(obj.model, obj.data, options) || (result = false);
                }
            } else {
                return this.setAttr.call(this, attributes, options);
            }
            return result;
        },

        setAttr: function (attributes, options) {
            var attr;

            options || (options = {});
            if (options.unset) for (attr in attributes) attributes[attr] = void 0;

            if (this.relations) {
                _.each(this.relations, function (relation) {
                    var relationKey = relation.key,
                        relatedModel = relation.relatedModel,
                        collectionType = relation.collectionType,
                        val,
                        relationOptions,
                        data,
                        relationValue;
                    if (attributes[relationKey]) {
                        val = _.result(attributes, relationKey);

                        relatedModel && _.isString(relatedModel) && (relatedModel = eval(relatedModel));
                        collectionType && _.isString(collectionType) && (collectionType = eval(collectionType));

                        relationOptions = relation.options ? _.extend({}, relation.options, options) : options;

                        if (relation.type === Backbone.Many) {
                            if (collectionType && !collectionType.prototype instanceof BackboneCollection) {
                                throw new Error('collectionType must inherit from Backbone.Collection');
                            }

                            if (val instanceof BackboneCollection) {
                                data = val;
                                attributes[relationKey] = data;
                            } else {
                                data = collectionType ? new collectionType() : this._createCollection(relatedModel);
                                data.add(val, relationOptions);
                                attributes[relationKey] = data;
                            }
                        } else if (relation.type === Backbone.One && relatedModel) {
                            data = val instanceof AssociatedModel ? val : new relatedModel(val);
                            attributes[relationKey] = data;
                        }

                        relationValue = data;

                        if (relationValue && !relationValue._proxyCallback) {
                            relationValue._proxyCallback = function () {
                                return this._bubbleEvent.call(this, relationKey, relationValue, arguments);
                            };
                            relationValue.on("all", relationValue._proxyCallback, this);
                        }
                    }
                }, this);
            }

            return ModelProto.set.call(this, attributes, options);
        },

        _bubbleEvent: function (relationKey, relationValue, eventArguments) {
            var args = eventArguments,
                opt = args[0].split(":"),
                eventType = opt[0],
                eventObject = args[1],
                indexEventObject = -1,
                _proxyCalls = relationValue._proxyCalls,
                eventPath,
                eventAvailable;

            _.size(opt) > 1 && (eventPath = opt[1]);

            if (relationValue instanceof BackboneCollection && "change" === eventType && eventObject) {
                var pathTokens = getPathArray(eventPath),
                    initialTokens = _.initial(pathTokens),
                    colModel;

                colModel = relationValue.find(function (model) {
                    if (eventObject === model) return true;
                    if (model) {
                        var changedModel = model.get(initialTokens);
                        if ((changedModel instanceof AssociatedModel || changedModel instanceof BackboneCollection) && eventObject === changedModel) return true;
                        changedModel = model.get(pathTokens);
                        return (changedModel instanceof AssociatedModel || changedModel instanceof BackboneCollection) && eventObject === changedModel;
                    }
                    return false;
                });
                colModel && (indexEventObject = relationValue.indexOf(colModel));
            }

            eventPath = relationKey + (indexEventObject !== -1 ? "[" + indexEventObject + "]" : "") + (eventPath ? "." + eventPath : "");
            args[0] = eventType + ":" + eventPath;

            if (_proxyCalls) {
                eventAvailable = _.find(_proxyCalls, function (value, eventKey) {
                    return eventPath.indexOf(eventKey, eventPath.length - eventKey.length) !== -1;
                });
                if (eventAvailable) return this;
            } else {
                _proxyCalls = relationValue._proxyCalls = {};
            }

            _proxyCalls[eventPath] = true;

            if ("change" === eventType) {
                this._previousAttributes[relationKey] = relationValue._previousAttributes;
                this.changed[relationKey] = relationValue;
            }

            this.trigger.apply(this, args);

            if (eventPath && _proxyCalls) {
                delete _proxyCalls[eventPath];
            }
            return this;
        },

        _createCollection: function (type) {
            var collection,
                relatedModel = type;
            _.isString(relatedModel) && (relatedModel = eval(relatedModel));

            if (relatedModel && relatedModel.prototype instanceof AssociatedModel) {
                collection = new BackboneCollection();
                collection.model = relatedModel;
            } else {
                throw new Error('type must inherit from Backbone.AssociatedModel');
            }
            return collection;
        },

        toJSON: function (options) {
            var json, aJson;
            if (!this.visited) {
                this.visited = true;

                json = ModelProto.toJSON.apply(this, arguments);

                if (this.relations) {
                    _.each(this.relations, function (relation) {
                        var attr = this.attributes[relation.key];
                        if (attr) {
                            aJson = attr.toJSON(options);
                            json[relation.key] = _.isArray(aJson) ? _.compact(aJson) : aJson;
                        }
                    }, this);
                }
                delete this.visited;
            }
            return json;
        },

        clone: function () {
            return new this.constructor(this.toJSON());
        },

        getAttr: function (path) {

            var result = this,
                attrs = getPathArray(path),
                key,
                i;
            if (_.size(attrs) < 1) {
                return;
            }
            for (i = 0; i < attrs.length; i++) {
                key = attrs[i];
                if (!result) {
                    break;
                }

                result = result instanceof BackboneCollection && !isNaN(key) ? result.at(key) : result.attributes[key];
            }
            return result;
        }
    });

    var delimiters = /[^\.\[\]]+/g;

    var getPathArray = function (path) {
        if (path === '') {
            return [''];
        }
        return _.isString(path) ? path.match(delimiters) : path || [];
    };
}).call(this);