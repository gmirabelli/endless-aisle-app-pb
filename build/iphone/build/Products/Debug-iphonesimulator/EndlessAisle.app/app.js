




var Alloy = require('/alloy'),
_ = Alloy._,
Backbone = Alloy.Backbone;














var logger = require('logging')('application:alloy', 'app/alloy');
var lastTrace = -1;
var lastMemory = -1;
var swissArmyUtils = require('com.demandware.SwissArmyUtils');






var ucfirst = function (text) {
    return text ? text[0].toUpperCase() + text.substr(1) : text;
};

var getFullControllerPath = function (controllerPath) {
    if (controllerPath.indexOf('app/controllers/') == -1) {
        return "app/controllers/" + controllerPath;
    }
    return controllerPath;
};





var supportLog = function (msg) {
    if (Alloy.Globals.consoleLog) {
        Alloy.Globals.consoleLog.unshift(msg);

        if (Alloy.Globals.consoleLog.length > 100) {
            Alloy.Globals.consoleLog.pop();
        }
    }
};





var setRuntimeLoggableCategories = function (categories) {
    Alloy.Globals.runtimeLoggableCategories = categories;
};




var getLoggableCategories = function () {
    var loggableCategories;
    var finalCategories = [],
    loggableSubcategories = {};

    if (Alloy.Globals.runtimeLoggableCategories && Alloy.Globals.runtimeLoggableCategories.length > 0) {
        loggableCategories = Alloy.Globals.runtimeLoggableCategories;
    } else {
        loggableCategories = Alloy.CFG.loggableCategories;
    }

    _.each(loggableCategories, function (category) {
        var index = category.indexOf(':');
        if (index != -1) {
            var catName = category.substring(0, index);
            var subCatNames = category.substring(index + 1);

            if (!loggableSubcategories[catName]) {
                loggableSubcategories[catName] = [];
            }

            loggableSubcategories[catName].push(subCatNames.split(':'));
        } else {
            finalCategories.push(category);
        }
    });

    return {
        loggableCategories: finalCategories,
        loggableSubcategories: loggableSubcategories };

};






var removeAllChildren = function (view) {

    if (!view) {
        return;
    }
    if (_.isFunction(view.getChildren)) {
        _.each(view.getChildren(), function (child) {
            _.isFunction(view.remove) && view.remove(child);

            setTimeout(function () {
                removeAllChildren(child);
            }, 50);
        });
    }
};




var allowAppSleep = function (truth) {
    logger.info('Setting allow app sleep to ' + truth);
    Ti.App.idleTimerDisabled = !truth;
};





Alloy.eventDispatcher = new Backbone.Model();


require('appConfiguration').loadDefaultConfigs();


require('alloyAdditions');

require('appResume');

require('imageUtils');

var moment = require('alloy/moment');





Alloy.Globals.resetCookies = function () {
    var http = Ti.Network.createHTTPClient({
        validatesSecureCertificate: Alloy.CFG.ocapi.validate_secure_cert });

    var host = Alloy.CFG.storefront_host;
    http.clearCookies('http://' + host);
    http.clearCookies('https://' + host);
};

Alloy.Styles = require('alloy/styles/' + Alloy.CFG.theme).styles;
Alloy.Animations = {
    bounce: Ti.UI.createAnimation({
        transform: Ti.UI.create2DMatrix().scale(0.9, 0.9),
        duration: 100,
        autoreverse: true,
        repeat: 1 }),

    bigBounce: Ti.UI.createAnimation({
        transform: Ti.UI.create2DMatrix().scale(0.6, 0.6),
        duration: 100,
        autoreverse: true,
        repeat: 1 }),

    fadeOut: Ti.UI.createAnimation({
        opacity: 0,
        duration: 400 }),

    fadeIn: Ti.UI.createAnimation({
        opacity: 1.0,
        duration: 400 }) };




require('timers');


var DialogMgr = require('DialogMgr');
Alloy.Dialog = new DialogMgr();






var notify = function (text, config) {
    var timeout = config && config.timeout || Alloy.CFG.notification_display_timeout;
    var preventAutoClose = config && config.preventAutoClose;
    Alloy.Dialog.showNotifyGrowl({
        label: text,
        timeout: timeout,
        preventAutoClose: preventAutoClose });

};





var removeNotify = function (message) {
    Alloy.Dialog.removeNotifyGrowl(message);
};

Alloy.eventDispatcher.listenTo(Alloy.eventDispatcher, 'configurations:unload', function () {
    Alloy.Kiosk.manager = null;
});




Alloy.CFG.kiosk_mode = {};
var isKioskMode = function () {
    return Alloy.CFG.kiosk_mode.enabled;
};

Alloy.Kiosk = {
    manager: null };


var isKioskManagerLoggedIn = function () {
    return isKioskMode() && Alloy.Kiosk.manager;
};

var isKioskCartEnabled = function () {
    return Alloy.CFG.kiosk_mode.enable_cart;
};

var getKioskManager = function () {
    if (isKioskManagerLoggedIn()) {
        return Alloy.Kiosk.manager;
    }

    return null;
};









var recordHistoryEvent = function () {
    var _lastNavEvent = {
        route: 'begin' };

    function recordHistoryEvent(event) {
        event = event || {};
        var history_event = _.extend({}, {
            associate_id: Alloy.Models.associate.getEmployeeId() || 'public',
            customer_id: Alloy.Models.customer.getCustomerNumber() || 'guest customer',
            time: new Date().getTime(),
            type: event.type || 'undefined',
            details: JSON.stringify(event) });


        if (event.route) {
            history_event.route = event.route;
        }

        if (_lastNavEvent && _lastNavEvent.route && event.route) {
            if (Alloy.CFG.use_appcelerator_analytics) {
                Ti.Analytics.navEvent(event.route, _lastNavEvent.route, event.type, event);
            }
            _lastNavEvent = event;
        } else {
            if (Alloy.CFG.use_appcelerator_analytics) {
                Ti.Analytics.featureEvent(event.type, event);
            }
        }

        var newEvent = Alloy.createModel('history');
        newEvent.set(history_event);

        Alloy.Collections.history.add(newEvent);
    }

    return recordHistoryEvent;
}();



Alloy.Globals.consoleLog = [];


Alloy.Globals.runtimeLoggableCategories = [];

swissArmyUtils.redirectConsoleLogToFile();
Alloy.createController('index');