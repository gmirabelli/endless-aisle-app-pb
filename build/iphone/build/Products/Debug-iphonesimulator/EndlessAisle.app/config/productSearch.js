

module.exports = {
    product_search: {
        default_expand: 'images,prices,variations',

        search_products_returned: 8,

        products_per_page: 8,

        cgid_select_params: '(total,query,count,start,hits,refinements[0],selected_refinements,selected_sorting_option,-sorting_options)',

        hits_select_params: '(count,hits.(**),total,start,-selected_refinements,-selected_sorting_option,-sorting_options,-refinements)',
        refinements: {
            refinement_component: {
                c_size: 'search/components/smallSwatchRefinement',
                c_width: 'search/components/smallSwatchRefinement',
                price: 'search/components/mediumSwatchRefinement',
                c_refinementColor: 'search/components/colorSwatchRefinement',
                default: 'search/components/listItemRefinement'
            },
            colorForPresentationID: {
                beige: {
                    key: 'backgroundColor',
                    value: '#f5f5dc'
                },
                black: {
                    key: 'backgroundColor',
                    value: '#000'
                },
                blue: {
                    key: 'backgroundColor',
                    value: 'blue'
                },
                navy: {
                    key: 'backgroundColor',
                    value: 'navy'
                },
                brown: {
                    key: 'backgroundColor',
                    value: '#783201'
                },
                green: {
                    key: 'backgroundColor',
                    value: 'green'
                },
                grey: {
                    key: 'backgroundColor',
                    value: '#8f979d'
                },
                orange: {
                    key: 'backgroundColor',
                    value: 'orange'
                },
                pink: {
                    key: 'backgroundColor',
                    value: '#fe249a'
                },
                purple: {
                    key: 'backgroundColor',
                    value: 'purple'
                },
                red: {
                    key: 'backgroundColor',
                    value: 'red'
                },
                white: {
                    key: 'backgroundColor',
                    value: '#fff'
                },
                yellow: {
                    key: 'backgroundColor',
                    value: '#ffff00'
                },
                miscellaneous: {
                    key: 'backgroundImage',
                    value: 'images/search/miscellaneous_swatch.png'
                },
                default: {
                    key: 'backgroundColor',
                    value: '#ebecec'
                }
            }
        }
    }
};