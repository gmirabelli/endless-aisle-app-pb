

module.exports = {
    local_address: {
        US: 'config/address/storePickup_NA',
        IT: 'config/address/storePickup_EU',
        CA: 'config/address/storePickup_NA',
        FR: 'config/address/storePickup_EU',
        NL: 'config/address/storePickup_EU',
        GB: 'config/address/storePickup_EU',
        ES: 'config/address/storePickup_EU',
        DE: 'config/address/storePickup_EU',
        JP: 'config/address/storePickup_Asia',
        CN: 'config/address/storePickup_Asia',
        default: 'config/address/storePickup_NA'
    }
};