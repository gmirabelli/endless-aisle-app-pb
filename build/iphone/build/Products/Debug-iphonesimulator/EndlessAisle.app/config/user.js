

module.exports = {
    theme: 'pb',

    storefront_host: 'dsanterre-inside-eu02-dw.demandware.net',

    storefront_home: '/on/demandware.store/Sites-PetitBateau-Site',
    storefront: {
        site_url: '/on/demandware.store/Sites-PetitBateau-Site'
    },
    ocapi: {
        site_url: '/s/Sites-PetitBateau-Site',

        client_id: 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'
    },
    devices: {
        payment_terminal_module: 'verifoneDevice'
    },

    payment_entry: 'default',

    countryConfig: {
        US: {
            ocapi: {
                site_url: '/s/Sites-PetitBateau-Site'
            },
            storefront: {
                site_url: '/on/demandware.store/Sites-PetitBateau-Site'
            },
            languagesSupported: ['en', 'fr', 'zh-CN', 'ja', 'de'],
            displayName: 'United States',
            value: 'US',
            appCurrency: 'USD' },
        IT: {
            ocapi: {
                site_url: '/s/Sites-PetitBateau-Site'
            },
            storefront: {
                site_url: '/on/demandware.store/Sites-PetitBateau-Site'
            },
            languagesSupported: ['en', 'fr', 'zh-CN', 'ja', 'de'],
            displayName: 'Italy',
            value: 'IT',
            appCurrency: 'EUR' },
        DE: {
            ocapi: {
                site_url: '/s/Sites-PetitBateau-Site'
            },
            storefront: {
                site_url: '/on/demandware.store/Sites-PetitBateau-Site'
            },
            languagesSupported: ['en', 'fr', 'zh-CN', 'ja', 'de'],
            displayName: 'Germany',
            value: 'DE',
            appCurrency: 'EUR' },
        FR: {
            ocapi: {
                site_url: '/s/Sites-PetitBateau-Site'
            },
            storefront: {
                site_url: '/on/demandware.store/Sites-PetitBateau-Site'
            },
            languagesSupported: ['en', 'fr', 'zh-CN', 'ja', 'de'],
            displayName: 'France',
            value: 'FR',
            appCurrency: 'EUR' },
        JP: {
            ocapi: {
                site_url: '/s/Sites-PetitBateau-Site'
            },
            storefront: {
                site_url: '/on/demandware.store/Sites-PetitBateau-Site'
            },
            languagesSupported: ['en', 'fr', 'zh-CN', 'ja', 'de'],
            displayName: 'Japan',
            value: 'JP',
            appCurrency: 'JPY' }
    }
};