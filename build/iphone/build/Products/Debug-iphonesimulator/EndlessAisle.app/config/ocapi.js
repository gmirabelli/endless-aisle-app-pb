

module.exports = {
    ocapi: {
        enable_proxy_cache: true,

        enable_http_cache: true,

        validate_secure_cert: false,
        data_site_url: '/s/-',
        data_base_url: '/dw/data/',

        site_url: '/s/SiteGenesis',

        base_url: '/dw/shop/',

        client_id: 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',

        default_locale: 'default',

        version: 'v18_2',

        cache_version: 2
    }
};