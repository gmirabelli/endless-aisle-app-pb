

module.exports = {
    storefront_host: 'override-in-user.js.demandware.net',

    storefront_home: '/on/demandware.store/Sites-SiteGenesis-Site',

    root_category_id: 'root',

    organization_custom_object_types: true,

    notification_display_timeout: 1000,

    allow_simulate_payment: false,

    enable_barcode_scanner: true,

    allBarcodeScannerTypes: ['Aztec', 'Code128', 'Code39', 'Code39Mod43', 'Code93', 'UPCE', 'EAN13', 'EAN8', 'PDF417', 'QR', 'Interleaved2of5', 'ITF14', 'DataMatrix'],

    acceptedBarcodeScannerTypes: ['UPCE', 'EAN13', 'EAN8'],

    loggableCategories: [],

    theme: 'wireframe',
    is_live: false,
    use_appcelerator_analytics: false,

    test_configuration_url: '',
    use_log_to_file: true,

    use_crash_reporter: true,

    app_url_scheme: 'dwea',

    defaultCustomerPage: 'addresses',

    payment_entry: 'default',

    regexes: {
        email: '^[A-Z0-9._%-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$',
        phone: {
            US: '^([0-9]( |-)?)?(\\(?[0-9]{3}\\)?|[0-9]{3})( |-|\\.)?([0-9]{3}( |-|\\.)?[0-9]{4}|[a-zA-Z0-9]{7})$',
            CA: '^([0-9]( |-)?)?(\\(?[0-9]{3}\\)?|[0-9]{3})( |-|\\.)?([0-9]{3}( |-|\\.)?[0-9]{4}|[a-zA-Z0-9]{7})$',
            DE: '^([+][0-9]{1,3}[ .-])?([(]{1}[0-9]{1,6}[)])?([0-9 .-/]{3,20})((x|ext|extension)[ ]?[0-9]{1,4})?$'
        },

        postal_code: {
            US: '^\\d{5}(-\\d{4})?$',
            CA: '^[ABCEGHJKLMNPRSTVXY]{1}\\d{1}[A-Z]{1} *\\d{1}[A-Z]{1}\\d{1}$'
        }
    },

    webViewCssReset: 'html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td, article, aside, canvas, details, embed, figure, figcaption, footer, header, hgroup, menu, nav, output, ruby, section, summary, time, mark, audio, video { margin: 0; padding: 0; border: 0; font-size: 100%; vertical-align: baseline; }',

    receipt_promotional_message_string_resource: 'receipt_promotional_message',

    perform_tests: false
};