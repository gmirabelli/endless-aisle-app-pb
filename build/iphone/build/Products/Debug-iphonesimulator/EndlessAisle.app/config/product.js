

module.exports = {
    product: {
        default_expand: 'variations,availability,images,prices,options,promotions,set_products,bundled_products',

        shouldSelectSingleValues: true,

        shouldSelectFirstValues: ['color'],
        ratings: {
            starFull: 'images/icons/fullstar.png',
            starHalf: 'images/icons/halfstar.png',
            starNone: 'images/icons/nostar.png'
        },
        options: {
            option_component: {
                specialHandlingCode: 'product/components/specialHandlingOption'
            }
        },
        attributeSelectComponent: {
            color: {
                view: 'product/components/variationAttributeSwatches',
                showTitle: true
            },
            default: {
                view: 'components/selectWidget',
                showTitle: true
            }
        },

        recommendations: {
            gridRows: 2,
            gridColumns: 3,
            maxPages: 10,
            tileImageWidth: 176,
            tileImageHeight: 221,
            tilePadding: 20,
            rowPadding: 24
        },
        descriptionWebViewCss: 'body { font-family:\'Helvetica Neue\'; font-size: 10pt; color: #3d3d3d; padding-right: 3em }',
        promotionWebViewCss: 'body {font-size: 16pt;font-family: \'Crete Round\';text-align:center }',

        enable_video_player: false
    }
};