

var logger = require('logging')('application:appConfiguration', 'app/lib/appConfiguration');

exports.loadDefaultConfigs = loadDefaultConfigs;
exports.loadConfigurations = loadConfigurations;

function loadDefaultConfigs() {
    var configFiles = ['config/admin', 'config/main', 'config/product', 'config/productSearch', 'config/devices', 'config/modelTests', 'config/ocapi', 'config/storefront', 'config/category', 'config/analytics', 'config/countries'];
    _.each(configFiles, function (configFile) {
        var configObj = require(configFile);
        _.extend(Alloy.CFG, configObj);
    });

    if (Ti.App.deployType === 'production') {
        Alloy.CFG.ocapi.validate_secure_cert = true;
        Alloy.CFG.storefront.validate_secure_cert = true;
    }

    applyUserProperties(require('config/user'), Alloy.CFG);
}

function loadConfigurations(startupOnly) {
    var deferred = new _.Deferred();
    logger.info('loadConfigurations called');

    loadDefaultConfigs();

    var appSettings = require('appSettings');

    appSettings.applyConfig();

    checkForTestConfiguration().always(function () {

        loadServerConfiguration(startupOnly).done(function () {
            checkForTestConfiguration().always(function () {
                appSettings.applyConfig();
                verifyAppSettingsDependencies();
                try {
                    if (Alloy.CFG.country_configuration) {
                        Alloy.CFG.country_configuration = JSON.parse(Alloy.CFG.country_configuration);
                    }
                } catch (e) {
                    logger.error('Unable to parse configurationJSON');
                    logger.secureLog('failed to parse proxied object: ' + Alloy.CFG.country_configuration);
                }
                deferred.resolve();
            });
        }).fail(function () {
            deferred.reject();
        });
    });
    return deferred.promise();
}

function loadServerConfiguration(startupOnly) {
    var deferred = new _.Deferred();

    if (startupOnly || !Alloy.CFG.store_id) {
        deferred.resolve();
    } else {
        logger.info('loading server configurations');
        var cfgSettings = Alloy.createModel('cfgSettings');
        cfgSettings.loadServerConfigs(Alloy.CFG.store_id).done(function () {
            var serverSettings = cfgSettings.getSettings();
            if (serverSettings) {
                applyUserProperties(serverSettings, Alloy.CFG);
                if (serverSettings.configuration_json) {
                    logger.info('configuration_json being applied');
                    try {
                        var json = JSON.parse(serverSettings.configuration_json);
                        applyUserProperties(json, Alloy.CFG);
                        deferred.resolve();
                    } catch (ex) {
                        logger.error('Unable to parse configurationJSON');
                        deferred.reject();
                    }
                } else {
                    deferred.resolve();
                }
            } else {
                deferred.resolve();
            }
        }).fail(function () {
            deferred.reject();
        });
    }
    return deferred.promise();
}

function checkForTestConfiguration() {
    var deferred = new _.Deferred();
    if (Alloy.CFG.test_configuration_url && (Ti.App.deployType == 'development' || Ti.App.deployType == 'test')) {
        var http = Ti.Network.createHTTPClient({
            timeout: Alloy.CFG.storefront.timeout
        });
        logger.log('request', 'checkForTestConfiguration GET ' + Alloy.CFG.test_configuration_url);
        http.open('GET', Alloy.CFG.test_configuration_url, true);
        http.onerror = function (eResp) {
            logger.error('checkForTestConfiguration error!\n url: ' + Alloy.CFG.test_configuration_url + '\n status: [' + http.status + ']\n response: [' + http.responseText + ']\n exception: [' + JSON.stringify(eResp, null, 2) + ']');
            deferred.reject();
            notify(String.format(_L('Unable to load \'%s\'.'), Alloy.CFG.test_configuration_url), {
                preventAutoClose: true
            });
        };
        http.onload = function () {
            if (http.responseText) {
                try {
                    logger.info('test_configuration_url being applied');
                    applyUserProperties(JSON.parse(http.responseText), Alloy.CFG);
                    logger.secureLog('checkForTestConfiguration response ' + JSON.stringify(http.responseText, null, 2), 'request-response');
                } catch (ex) {
                    logger.error('checkForTestConfiguration error ' + http.responseText);
                }
            }
            deferred.resolve();
        };
        http.send();
    } else {
        deferred.resolve();
    }
    return deferred.promise();
}

function applyUserProperties(userConfigObj, configObj) {
    var userConfigKeys = _.keys(userConfigObj);
    _.each(userConfigKeys, function (key) {
        if (key == 'id' || key == 'uri') {
            return;
        }
        if (configObj[key] == undefined) {
            configObj[key] = {};
        }
        var userValue = userConfigObj[key];
        if (_.isObject(userValue) && !_.isArray(userValue)) {
            if (!_.isObject(configObj[key])) {
                configObj[key] = {};
            }
            applyUserProperties(userValue, configObj[key]);
        } else {
            configObj[key] = userValue;
        }
    });
}

function verifyAppSettingsDependencies() {
    if (Alloy.CFG.kiosk_mode.enabled) {
        Alloy.CFG.enable_wish_list = false;
    }
    if (Alloy.CFG.devices.payment_terminal_module === 'webDevice') {
        Alloy.CFG.payment_entry = 'web';
        Alloy.CFG.collect_billing_address = true;
    }
    if (Alloy.CFG.devices.payment_terminal_module === 'adyenDevice') {
        Alloy.CFG.payment_entry = 'default';
        Alloy.CFG.enable_multi_tender_payments = false;
        Alloy.CFG.gift_cards_available = false;
    }
    if (Alloy.CFG.devices.payment_terminal_module === 'verifoneDevice') {
        Alloy.CFG.payment_entry = 'default';
    }
    if (Alloy.CFG.payment_entry === 'web') {
        Alloy.CFG.devices.payment_terminal_module = 'webDevice';
        Alloy.CFG.collect_billing_address = true;
    }
}