

var loggerFunction = null;

exports.setLoggerFunction = setLoggerFunction;
exports.getLoggerFunction = getLoggerFunction;

function setLoggerFunction(logFunc) {
  loggerFunction = logFunc;
}

function getLoggerFunction() {
  return loggerFunction;
}