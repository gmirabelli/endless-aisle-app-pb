
var eaUtils = require('EAUtils');

module.exports = function (args) {
    return new api(args);
};

function api(googleAddressDetails) {
    var xhr = null;
    var self = this;
    var logger = require('logging')('googlePlaceDetails', 'app/lib/googlePlaceDetails');

    function serviceCall(url, callback) {
        if (xhr == null) {
            xhr = Ti.Network.createHTTPClient();
        }
        logger.log('request', 'google place details GET ' + url);
        xhr.open('GET', url);
        xhr.setRequestHeader('Content-Type', 'application/json;charset=utf-8');
        xhr.onerror = function (eResp) {
            logger.error('API ERROR ' + eResp.error);
            if (callback.fail) {
                callback.fail(eResp);
            }
        };
        xhr.onload = function () {
            logger.log('request-response', 'API response: ' + this.responseText);
            if (callback.success) {
                var jsonResponse = JSON.parse(this.responseText);
                googleAddressDetails.set(jsonResponse.result);
                callback.success(jsonResponse);
            }
        };
        xhr.send();
    };

    this.getPlaceDetails = function (opts) {
        var options = {
            reference: opts.reference || '',
            sensor: opts.sensor || false,
            key: Alloy.CFG.google_places_key
        };
        var success = opts.success || function () {};
        var fail = opts.fail || function () {};
        var url = eaUtils.buildRequestUrl('https://maps.googleapis.com/maps/api/place/details/json', options);
        serviceCall(url, {
            success: success,
            fail: fail
        });
    };
};