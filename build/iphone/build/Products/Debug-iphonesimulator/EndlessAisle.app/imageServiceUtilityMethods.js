

exports.mixinImageServiceUtilityMethods = mixinImageServiceUtilityMethods;

function mixinImageServiceUtilityMethods(model_class) {
  model_class.prototype.getCartImage = function () {
    return this.getImageForContext('cart');
  };

  model_class.prototype.getProductTileImage = function () {
    return this.getImageForContext('productTile');
  };

  model_class.prototype.getHeroImage = function (variationValue) {
    return this.getImageForContext('heroImage', variationValue ? variationValue : null);
  };

  model_class.prototype.getHeroImageGroups = function (variationValue) {
    return this.getImageGroupsForViewType('heroImage', variationValue ? variationValue : null);
  };

  model_class.prototype.getCategoryTileImage = function () {
    return this.getImageForContext('categoryTile');
  };

  model_class.prototype.getImageSwatchLink = function () {
    return this.getImageForContext('imageSwatchLink');
  };

  model_class.prototype.getAltImages = function (variationValue) {
    return this.getImagesForContext('altImages', variationValue ? variationValue : null);
  };

  model_class.prototype.getAltZoomImages = function (variationValue) {
    return this.getImagesForContext('altZoomImages', variationValue ? variationValue : null);
  };

  model_class.prototype.getLargeAltZoomImages = function (variationValue) {
    return this.getImagesForContext('largeAltZoomImages', variationValue ? variationValue : null);
  };

  model_class.prototype.getBundleProductImages = function (variationValue) {
    return this.getImagesForContext('bundleProductImages', variationValue ? variationValue : null);
  };

  model_class.prototype.getSetProductImages = function (attribute) {
    return this.getImagesForContext('setProductImages', attribute ? attribute : null);
  };

  model_class.prototype.getSwatchImages = function (variationValue) {
    return this.getImagesForContext('swatchImages', variationValue ? variationValue : null);
  };

  model_class.prototype.getSwatchImageGroups = function (variationValue) {
    return this.getImageGroupsForViewType('swatchImages', variationValue ? variationValue : null);
  };

  model_class.prototype.getFirstImageFromImageGroup = function () {
    if (this.get('image_groups').length > 0) {
      return this.getHeroImage(this.get('image_groups').at(0));
    }
    return null;
  };
}