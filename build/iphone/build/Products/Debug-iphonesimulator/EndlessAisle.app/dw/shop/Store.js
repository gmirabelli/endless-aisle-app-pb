

var Backbone = Backbone || Alloy ? Alloy.Backbone : require('backbone');

var AssociatedModel = AssociatedModel || Alloy ? Backbone.AssociatedModel : require('backbone-associations').AssociatedModel;

var Store = AssociatedModel.extend({
  initialize: function () {},

  idAttribute: 'id',
  urlRoot: '/stores'
}, {});

var mixinApiMethods = require('ocapi_methods');

mixinApiMethods(Store, require('dw/shop/metadata/store'));

module.exports = Store;