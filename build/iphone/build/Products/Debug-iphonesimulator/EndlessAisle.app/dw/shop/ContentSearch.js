

var Backbone = Backbone || Alloy ? Alloy.Backbone : require('backbone');

var AssociatedModel = AssociatedModel || Alloy ? Backbone.AssociatedModel : require('backbone-associations').AssociatedModel;

var ContentSearch = AssociatedModel.extend({
  url: '/content_search'
}, {});

module.exports = ContentSearch;