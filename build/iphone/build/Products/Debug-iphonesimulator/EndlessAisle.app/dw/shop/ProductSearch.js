

var Backbone = Backbone || Alloy ? Alloy.Backbone : require('backbone');

var AssociatedModel = AssociatedModel || Alloy ? Backbone.AssociatedModel : require('backbone-associations').AssociatedModel;

var ProductSearch = AssociatedModel.extend({
    initialize: function (options) {
        options = options || {};
        this.batches = [];
        this.force = !!options.force;
    },

    fetch: function (options) {
        this.initialize(options);
        return AssociatedModel.prototype.fetch.apply(this, arguments);
    },

    queryParams: function () {
        var params = {
            q: this.getQuery() || '',
            start: this.getStart() || '0',
            count: Alloy.CFG.product_search.search_products_returned
        };

        if (params.start != 0 && !_.isEmpty(Alloy.CFG.product_search.hits_select_params)) {
            params['select'] = Alloy.CFG.product_search.hits_select_params;
        }

        params.expand = this.expand || Alloy.CFG.product_search.default_expand || 'images,prices,variations';

        var selectedRefinements = this.getSelectedRefinements() || {
            cgid: Alloy.CFG.root_category_id
        };
        var count = 0,
            x;
        for (x in selectedRefinements) {
            count++;
            params['refine_' + count] = '' + x + '=' + selectedRefinements[x];
        }

        var selectedSortingOption = this.getSelectedSortingOption();
        if (selectedSortingOption) {
            params['sort'] = selectedSortingOption.get('id');
        }

        if (this.isEmptySearch()) {
            if (!_.isEmpty(Alloy.CFG.product_search.hits_select_params)) {
                params['select'] = Alloy.CFG.product_search.cgid_select_params;
            }

            params.expand = '';

            params.count = 1;
        }

        return params;
    },
    urlRoot: '/product_search',

    getCategoryID: function () {
        return this.getSelectedRefinements().cgid;
    },

    getCategoryPath: function (returnLeaves) {
        var leaves = false;
        if (returnLeaves) {
            leaves = true;
        }
        var path = [];
        var currentCatID = this.getSelectedRefinements().cgid;
        var categoryRefinement = this.getRefinement('cgid');
        if (!categoryRefinement) {
            return [];
        }
        var values = categoryRefinement.getValues();
        var cats = _.filter(values, function (n) {
            var value = n.getValue();
            var hasChildren = n.hasValues();
            return value == currentCatID || hasChildren;
        });
        var cat = _.first(cats);
        var subcats = null;
        while (!_.isEmpty(cat)) {
            path.push(cat);

            if (cat.get('value') == currentCatID) {
                break;
            }

            subcats = cat.get('values');
            cat = _.first(subcats.filter(function (n) {
                return n.hasValues() || leaves && n.get('value') == currentCatID;
            }));
        }

        return path;
    },

    batchSearch: function (batch) {
        if (batch == 0) {
            return this;
        }

        var total = this.getTotal();
        var batchStart = batch * Alloy.CFG.product_search.search_products_returned;

        if (batchStart >= total) {
            return null;
        }

        var _batchSearch = this.batches[batch] || this.simpleClone();
        this.batches[batch] = _batchSearch;

        _batchSearch.setStart(batchStart, {
            silent: true
        });

        return _batchSearch;
    },

    simpleClone: function () {
        var clone = new ProductSearch();
        clone.set({
            selected_refinements: this.getSelectedRefinements(),
            selected_sorting_option: this.getSelectedSortingOption(),
            query: this.getQuery()
        }, {
            silent: true
        });

        return clone;
    },

    emptySearch: function () {
        this.set({
            query: '',
            selected_refinements: {
                cgid: Alloy.CFG.root_category_id
            }
        }, {
            silent: true
        });
        return this.fetch();
    },

    isEmptySearch: function () {
        var isEmpty = false;
        var query = this.getQuery();
        var refinements = this.getSelectedRefinements();
        if (query == '' && refinements && _.keys(refinements).length == 1 && refinements['cgid'] == Alloy.CFG.root_category_id) {
            isEmpty = true;
        }
        return isEmpty;
    },

    getSubcategoriesContext: function () {
        var lowestSubcategories = null,
            lowestParent = null;
        var currentCatID = this.getSelectedRefinements().cgid;
        var refinements = this.getRefinementsCollection(),
            categoryRefinement;
        if (refinements && this.getRefinement('cgid')) {
            categoryRefinement = this.getRefinement('cgid');

            var cat = _.first(_.filter(categoryRefinement.getValues(), function (n) {
                return n.getValue() == currentCatID || n.hasValues();
            }));

            var subcats = null,
                last_cat;
            while (!_.isEmpty(cat)) {
                subcats = cat.getValues();

                if (cat.getValue() == currentCatID) {
                    if (subcats.length < 1 && last_cat) {
                        lowestParent = last_cat;
                        lowestSubcategories = last_cat.getValues();
                    } else {
                        lowestParent = cat;
                        lowestSubcategories = subcats;
                    }
                    break;
                }
                last_cat = cat;

                cat = _.first(_.filter(subcats, function (n) {
                    return n.getValue() == currentCatID || n.hasValues();
                }));
            }
        }

        return {
            subcategories: lowestSubcategories || categoryRefinement && categoryRefinement.getValues(),
            parent: lowestParent
        };
    },

    isRefinedBy: function (attribute_id, value_id) {
        var isRefinedBy = false;

        var selectedRefinements = this.getSelectedRefinements();
        var existingRefinements = selectedRefinements[attribute_id];
        if (existingRefinements) {
            var values = existingRefinements.split('|');
            if (values.indexOf(value_id) > -1) {
                isRefinedBy = true;
            }
        }
        return isRefinedBy;
    },

    getRefinement: function (attribute_id) {
        var refinements = this.getRefinementsCollection();
        var refinement = refinements.filter(function (refinement) {
            return refinement.get('attribute_id') == attribute_id;
        });
        return refinement && refinement[0] ? refinement[0] : null;
    },

    getRefinementValue: function (attribute_id, value_id) {
        var refinement = this.getRefinement(attribute_id);
        if (!refinement) {
            return null;
        }
        var values = refinement.get('values');
        if (!values) {
            return null;
        }
        var value = values.filter(function (value) {
            return value.get('value') == value_id;
        });
        return value && value[0] ? value[0] : null;
    },

    toggleRefinementValue: function (attribute_id, value_id, options) {
        var selectedRefinements = this.getSelectedRefinements();
        var existingRefinements = selectedRefinements[attribute_id];
        if (existingRefinements) {
            if (attribute_id == 'price') {
                if (existingRefinements == value_id) {
                    delete selectedRefinements[attribute_id];
                } else {
                    selectedRefinements[attribute_id] = value_id;
                }
            } else {
                existingRefinements = existingRefinements.split('|');
                var existingIndex = existingRefinements.indexOf(value_id);
                if (existingIndex > -1) {
                    existingRefinements.splice(existingIndex, 1);
                } else {
                    existingRefinements.push(value_id);
                }
                existingRefinements = existingRefinements.join('|');
                selectedRefinements[attribute_id] = existingRefinements;
            }
        } else {
            selectedRefinements[attribute_id] = value_id;
        }

        this.setSelectedRefinements(selectedRefinements, options);
    },

    clearRefinementValue: function (attribute_id, options) {
        var selectedRefinements = this.getSelectedRefinements();
        if (attribute_id == 'cgid') {
            selectedRefinements = _.extend(selectedRefinements, this.defaultRefinements());
        } else if (selectedRefinements[attribute_id]) {
            delete selectedRefinements[attribute_id];
        }
        this.setSelectedRefinements(selectedRefinements, options);
    },

    clearAllRefinementValues: function (options) {
        var cgid = this._get('selected_refinements').cgid;
        var clearedRefinements = _.extend(this.defaultRefinements(), {
            cgid: cgid
        });
        this.set({
            selected_refinements: clearedRefinements
        }, options);
    },

    defaultRefinements: function () {
        return {
            cgid: Alloy.CFG.root_category_id
        };
    },

    getSelectedSortingOption: function () {
        var selectedOptionID = this._get('selected_sorting_option');
        var sortingOptions = this.getSortingOptionsCollection();
        var selectedOptions = sortingOptions.filter(function (opt) {
            return opt.get('id') == selectedOptionID;
        });
        return _.first(selectedOptions);
    },

    isSelectedSortingOption: function (option_id) {
        var selectedOptionID = this.getSelectedSortingOption();
        return option_id && option_id == selectedOptionID;
    }
}, {});

var mixinApiMethods = require('ocapi_methods');

mixinApiMethods(ProductSearch, require('dw/shop/metadata/product_search_result'));

module.exports = ProductSearch;