

var Backbone = Backbone || Alloy ? Alloy.Backbone : require('backbone');

var AssociatedModel = AssociatedModel || Alloy ? Backbone.AssociatedModel : require('backbone-associations').AssociatedModel;

var Folder = AssociatedModel.extend({
  url: '/folders'
}, {});

module.exports = Folder;