
var Backbone = Backbone || Alloy ? Alloy.Backbone : require('backbone');

var AssociatedModel = AssociatedModel || Alloy ? Backbone.AssociatedModel : require('backbone-associations').AssociatedModel;

var Promotion = AssociatedModel.extend({
  url: '/promotions'
}, {});

module.exports = Promotion;