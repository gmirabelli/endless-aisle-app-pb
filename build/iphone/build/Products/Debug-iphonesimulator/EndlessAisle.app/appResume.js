

(function () {

    var logger = require('logging')('application:appResume', 'app/lib/appResume');

    Ti.App.launchURL = '';
    Ti.App.pauseURL = '';

    var prefix = Alloy.CFG.app_url_scheme + '://';
    var lastPause = -1;

    if ('ipad' == Ti.Platform.osname || 'iphone' == Ti.Platform.osname) {
        var cmd = Ti.App.getArguments();
        logger.info('Launched app on ios: \nArguments:\n' + JSON.stringify(Ti.App.getArguments()));
        if (cmd && cmd.hasOwnProperty('url')) {
            Ti.App.launchURL = cmd.url;
            logger.info('Launched with url = ' + Ti.App.launchURL);
        }

        Ti.App.addEventListener('open', function (event) {
            logger.info('Open: ' + JSON.stringify(event) + '\nArguments:\n' + JSON.stringify(Ti.App.getArguments()));
        });

        Ti.App.addEventListener('close', function (event) {
            logger.info('Close: ' + JSON.stringify(event) + '\nArguments:\n' + JSON.stringify(Ti.App.getArguments()));

            Alloy.Router && Alloy.Router.associateLogout();
        });

        Ti.App.addEventListener('pause', function (event) {
            logger.info('Pause: ' + JSON.stringify(event) + '\nArguments:\n' + JSON.stringify(Ti.App.getArguments()));
            Ti.App.pauseURL = Ti.App.launchURL;
            Alloy.lastPause = new Date().getTime();
        });

        Ti.App.addEventListener('resumed', function (event) {
            logger.info('Resumed: ' + JSON.stringify(event) + '\nArguments:\n' + JSON.stringify(Ti.App.getArguments()));
            var now = new Date().getTime(),
                nextTimeout = Alloy.lastPause + Alloy.CFG.session_timeout;

            if (now > nextTimeout) {
                logger.info('Session Timeout exceeded!  Triggering a logout & restart.');

                Alloy.Router && Alloy.Router.associateLogout();
            } else {
                logger.info('Last Pause: ' + Alloy.lastPause + ' Now: ' + now + ' Next Timeout in ' + (nextTimeout - now) + 'ms');
            }
        });

        Ti.App.iOS.addEventListener('handleurl', function (event) {
            logger.info('handleurl: ' + JSON.stringify(event));

            if (event.launchOptions.source == 'com.apple.SafariViewService') {
                Alloy.eventDispatcher.trigger('appLaunch:browserUrl', event.launchOptions);
                logger.info('Safari resumed app.');
            }
        });
    }
})();