

var logger = require('logging')('images:disImageServiceMethods', 'app/lib/disImageServiceMethods');

var buildURLParams = require('EAUtils').buildURLParams;

exports.mixinImageServiceMethods = mixinImageServiceMethods;

function mixinImageServiceMethods(model_class) {
    model_class.prototype.getImageForContext = function (context, data) {
        if (!context) {
            return null;
        }

        var imageRef = null;
        switch (context) {
            case 'cart':
                imageRef = getCartImage(this, data);
                break;

            case 'productTile':
                imageRef = getSearchHitImage(this, data);
                break;

            case 'heroImage':
                imageRef = getImageFromImageGroup(this, data);
                break;

            case 'categoryTile':
                imageRef = getImageForCategoryTile(this, context);
                break;

            case 'imageSwatchLink':
                imageRef = getColorSwatchImage(this, context);
                break;
        }

        if (imageRef) {
            return getDynamicImageServiceImage(this, imageRef, context);
        }
        return Alloy.CFG.image_service.placeholderImageURL;

        function getSearchHitImage(hit, data) {
            var imageObj = hit.get('image');
            var variationAttributes;
            var variationValues;

            if (!imageObj && hit.getVariationAttributes()) {
                variationAttributes = hit.getVariationAttributes();
                outer: for (var i = 0; i < variationAttributes.length; i++) {
                    variationValues = variationAttributes[i].getValues();
                    for (var j = 0; j < variationValues.length; j++) {
                        if (variationValues[j].getImage()) {
                            imageObj = variationValues[j].getImage();
                            break outer;
                        }
                    }
                }
            }

            var link = imageObj ? imageObj.get('link') : null;
            return link;
        }

        function getCartImage(product, data) {
            var images,
                vvs = product.getVariationValues();
            if (vvs && vvs.color) {
                images = product.getImages('cart', vvs.color);
                if (!images) {
                    images = product.getImages('cart');
                }
            } else {
                images = product.getImages('cart');
            }
            if (images && images.length > 0) {
                return images[0].get('link');
            }
            return null;
        }

        function getImageFromImageGroup(product, imageGroup) {
            return imageGroup.get('images').at(0).get('link');
        }

        function getImageForCategoryTile(category, context) {
            return category.get('thumbnail');
        }

        function getColorSwatchImage(color, context) {
            return color.get('image_swatch').get('link');
        }

        function getDynamicImageServiceImage(model, imageRef, context) {
            logger.info('getImageForContext getDynamicImageServiceImage for ' + context);
            if (context === 'categoryTile') {
                return imageRef;
            }
            var urlParams = buildURLParams(Alloy.CFG.image_service.dynamic_size[context]);

            if (_.isArray(imageRef)) {
                _.each(imageRef, function (imageObj) {
                    var link = imageObj.get('link');
                    imageObj.set('link', model.transformImageLink(link, urlParams), {
                        silent: true
                    });
                });
            } else {
                imageRef = model.transformImageLink(imageRef, urlParams);
            }

            return imageRef;
        }
    };

    model_class.prototype.getImagesForContext = function (context, data) {
        if (!context) {
            return null;
        }

        var imageRef = null;
        switch (context) {
            case 'altImages':
            case 'altZoomImages':
            case 'largeAltZoomImages':
            case 'bundleProductImages':
            case 'setProductImages':
            case 'swatchImages':
                imageRef = getProductImages(this, context, data);
                break;
        }

        if (imageRef) {
            return getDynamicImageServiceImage(this, imageRef, context);
        }
        return Alloy.CFG.image_service.placeholderImageURL;

        function getProductImages(product, context, variationValue) {
            return product.getImages(context, variationValue) || [];
        }

        function getDynamicImageServiceImage(model, imageRef, context) {
            logger.info('getImagesForContext getDynamicImageServiceImage for ' + context);
            var urlParams = buildURLParams(Alloy.CFG.image_service.dynamic_size[context]);

            if (_.isArray(imageRef)) {
                _.each(imageRef, function (imageObj) {
                    var link = imageObj.get('link');
                    imageObj.set('link', model.transformImageLink(link, urlParams), {
                        silent: true
                    });
                });
            } else {
                imageRef = model.transformImageLink(imageRef, urlParams);
            }

            return imageRef;
        }
    };

    model_class.prototype.getImageGroupsForContext = function (context, data) {
        var imageGroups;

        if (this.isMaster() || this.isVariant()) {
            imageGroups = product.getOrderableColorImageGroups(context);
        }

        if (!imageGroups) {
            imageGroups = product.getImageGroupsForViewType(context);
        }

        return imageGroups;
    };

    model_class.prototype.getImageViewType = function (context) {
        return Alloy.CFG.image_service.view_type[context];
    };

    model_class.prototype.transformImageGroupImageURLs = function (imageGroupJSON, view_type) {
        logger.info('transformImageGroupImageURLs for ' + view_type);
        if (!imageGroupJSON) {
            return null;
        }

        if (view_type === 'categoryTile') {
            return imageGroupJSON;
        }
        var urlParams = buildURLParams(Alloy.CFG.image_service.dynamic_size[view_type]);

        if (_.isArray(imageGroupJSON)) {
            _.each(imageGroupJSON, function (imageGroupObj) {
                _.each(imageGroupObj.images, function (imageObj) {
                    imageObj.link = model_class.prototype.transformImageLink(imageObj.link, urlParams);
                });
            });
        } else {
            imageGroupJSON = model_class.prototype.transformImageLink(imageGroupJSON.link, urlParams);
        }

        return imageGroupJSON;
    };

    model_class.prototype.transformImageLink = function (link, urlParams) {
        var newLink = link,
            baseUrl = link.split('?')[0];

        if (link.indexOf(Alloy.Styles.imageNotAvailableImage) != -1) {
            return newLink;
        }
        var productionBaseUrl = Alloy.CFG.image_service.productionBaseUrl;
        if (link.search(productionBaseUrl) == -1) {
            var siteName = Alloy.CFG.storefront_home.substr(Alloy.CFG.storefront_home.lastIndexOf('/') + 1);
            if (baseUrl.indexOf('/' + siteName + '/') == -1) {
                siteName = '-';
            }
            var start = baseUrl.indexOf('/' + siteName + '/') + siteName.length + 2,
                filepath = baseUrl.substr(start);
            if (productionBaseUrl.lastIndexOf('/') < productionBaseUrl.length - 1) {
                productionBaseUrl += '/';
            }

            newLink = productionBaseUrl + filepath + urlParams;
        } else {
            newLink = baseUrl + urlParams;
        }
        logger.info('image link: ' + newLink);
        return newLink;
    };

    var imageServiceUtilityMethods = require('imageServiceUtilityMethods');
    imageServiceUtilityMethods.mixinImageServiceUtilityMethods(model_class);
}