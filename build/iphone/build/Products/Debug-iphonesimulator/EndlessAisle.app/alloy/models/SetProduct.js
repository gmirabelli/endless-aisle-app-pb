var Alloy = require('/alloy'),
    _ = require("/alloy/underscore")._,
    model,
    collection;

exports.definition = {
    config: {
        model_name: 'setProduct',
        secure: false,
        adapter: {
            type: 'ocapi'
        }
    },

    extendModel: function (Model) {
        _.extend(Model.prototype, {

            urlRoot: '/product',

            relations: [{
                type: Backbone.One,
                key: 'product',
                relatedModel: require('alloy/models/Product').Model
            }]

        });
        return Model;
    },

    extendCollection: function (Collection) {
        _.extend(Collection.prototype, {});
        return Collection;
    }
};

model = Alloy.M('setProduct', exports.definition, []);

collection = Alloy.C('setProduct', exports.definition, model);

exports.Model = model;
exports.Collection = collection;