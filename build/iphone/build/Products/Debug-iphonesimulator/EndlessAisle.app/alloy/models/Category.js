var Alloy = require('/alloy'),
    _ = require("/alloy/underscore")._,
    model,
    collection;

var ShopAPI = require('dw/shop/index');

var CategoryModel = ShopAPI.Category;

exports.definition = {
    config: {
        model_name: 'category',
        adapter: {
            type: 'ocapi',
            collection_name: 'category'
        },
        superClass: CategoryModel,
        cache: true
    },

    extendModel: function (Model) {
        _.extend(Model.prototype, {
            getParentCategoryId: function () {
                return this._get('parent_category_id');
            },

            fetchCategory: function () {
                return this.fetch();
            }
        });
        return Model;
    },

    extendCollection: function (Collection) {
        _.extend(Collection.prototype, {
            url: function () {
                var ids = this.ids || this.get('ids') || _.pluck(this.models, 'id') || [];

                return '/categories/(' + ids.join(',') + ')';
            },

            fetchCategories: function () {
                return this.fetch();
            },

            queryParams: function () {
                return {
                    levels: 2
                };
            }
        });
        return Collection;
    }
};

var mixinImageServiceMethods = require('imageServiceMethods');
mixinImageServiceMethods(CategoryModel);

model = Alloy.M('category', exports.definition, []);

collection = Alloy.C('category', exports.definition, model);

exports.Model = model;
exports.Collection = collection;