var Alloy = require('/alloy'),
    _ = require("/alloy/underscore")._,
    model,
    collection;

exports.definition = {
    config: {
        model_name: 'storeCountriesStates',
        secure: false,
        cache: false,
        adapter: {
            type: 'storefront'
        }
    },

    extendModel: function (Model) {

        _.extend(Model.prototype, {

            urlRoot: '/EAStore-GetCountriesStates',

            getStoreCountriesStates: function (options) {
                return this.fetch(options);
            }
        });

        return Model;
    }
};

model = Alloy.M('storeCountriesStates', exports.definition, []);

collection = Alloy.C('storeCountriesStates', exports.definition, model);

exports.Model = model;
exports.Collection = collection;