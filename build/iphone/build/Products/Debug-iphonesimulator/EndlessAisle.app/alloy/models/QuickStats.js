var Alloy = require('/alloy'),
    _ = require("/alloy/underscore")._,
    model,
    collection;

exports.definition = {
    config: {
        model_name: 'quickStats',
        adapter: {
            type: 'properties',
            collection_name: 'quickStats'
        }
    },
    extendModel: function (Model) {
        _.extend(Model.prototype, {});

        return Model;
    },
    extendCollection: function (Collection) {
        _.extend(Collection.prototype, {});

        return Collection;
    }
};

model = Alloy.M('quickStats', exports.definition, []);

collection = Alloy.C('quickStats', exports.definition, model);

exports.Model = model;
exports.Collection = collection;