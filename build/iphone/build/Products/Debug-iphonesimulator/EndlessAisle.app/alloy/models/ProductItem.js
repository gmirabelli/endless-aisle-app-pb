var Alloy = require('/alloy'),
    _ = require("/alloy/underscore")._,
    model,
    collection;

var option_item = Backbone.Model.extend({
    getItemText: function () {
        return this.get('item_text');
    },

    getPrice: function () {
        return this.get('price');
    }
});
var price_adjustment = Backbone.Model.extend({
    getItemText: function () {
        return this.get('item_text');
    }
});

var bundled_product_item = Backbone.Model.extend({});

exports.definition = {
    config: {
        model_name: 'product_item',
        adapter: {
            type: 'ocapi'
        }
    },

    extendModel: function (Model) {
        _.extend(Model.prototype, {

            relations: [{
                type: Backbone.Many,
                key: 'bundled_product_items',
                relatedModel: bundled_product_item
            }, {
                type: Backbone.Many,
                key: 'option_items',
                relatedModel: option_item
            }, {
                type: Backbone.Many,
                key: 'price_adjustments',
                relatedModel: price_adjustment
            }],

            getItemId: function () {
                return this.get('item_id');
            },

            getCurrencyCode: function () {
                return this.get('currency');
            },

            getProductId: function () {
                return this.get('product_id');
            },

            getItemText: function () {
                return this.get('item_text');
            },

            getProductName: function () {
                return this.get('product_name');
            },

            getQuantity: function () {
                return this.get('quantity');
            },

            getOptionItems: function () {
                return this.has('option_items') ? this.get('option_items').models : [];
            },

            getOptionItemsCollection: function () {
                return this.get('option_items') || new Backbone.Collection();
            },

            setPriceOverride: function (override, options) {
                this.set('price_override', override, options);
            },

            setPriceOverrideType: function (overrideType, options) {
                this.set('price_override_type', overrideType, options);
            },

            setMessage: function (message, options) {
                this.set('message', message, options);
            },

            getBasePrice: function () {
                return this.get('base_price');
            },

            getPrice: function () {
                return this.get('price');
            },

            getBasePriceOverride: function () {
                return this.get('base_price_override');
            },

            getPriceOverride: function () {
                return this.get('price_override');
            },

            getManagerEmployeeId: function () {
                return this.get('manager_employee_id');
            },

            getPriceOverrideType: function () {
                return this.get('price_override_type');
            },

            getPriceOverrideReasonCode: function () {
                return this.get('price_override_reason_code');
            },

            getPriceOverrideValue: function () {
                return this.get('price_override_value');
            },

            getMessage: function () {
                return this.get('message');
            },

            hasMessage: function () {
                return this.has('message');
            },

            getPriceAdjustments: function () {
                return this.has('price_adjustments') ? this.get('price_adjustments').models : [];
            },

            getThumbnailUrl: function () {
                return this.get('thumbnailUrl');
            },

            getProductIdAndQuantity: function () {
                return {
                    id: encodeURIComponent(this.getProductId()),
                    quantity: this.getQuantity()
                };
            },

            getCurrentStockLevel: function () {
                return this.get('current_stock_level');
            }
        });

        return Model;
    },
    extendCollection: function (Collection) {
        _.extend(Collection.prototype, {});

        return Collection;
    }
};

var mixinImageServiceMethods = require('imageServiceMethods');
mixinImageServiceMethods(bundled_product_item);

model = Alloy.M('productItem', exports.definition, []);

collection = Alloy.C('productItem', exports.definition, model);

exports.Model = model;
exports.Collection = collection;