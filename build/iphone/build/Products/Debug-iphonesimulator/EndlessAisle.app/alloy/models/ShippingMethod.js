var Alloy = require('/alloy'),
    _ = require("/alloy/underscore")._,
    model,
    collection;

exports.definition = {
    config: {
        model_name: 'shippingMethod',
        adapter: {
            type: 'ocapi'
        }
    },

    extendModel: function (Model) {
        _.extend(Model.prototype, {
            getName: function () {
                return this.get('name');
            },

            hasPriceOverride: function () {
                return this.has('price_override') && this.get('price_override') == 'true';
            },

            getPriceOverrideType: function () {
                return this.get('price_override_type');
            },

            getPriceOverrideValue: function () {
                return this.get('price_override_value');
            },

            getPriceOverrideReasonCode: function () {
                return this.get('price_override_reason_code');
            },

            getPriceOverrideManagerId: function () {
                return this.get('manager_employee_id');
            },

            getID: function () {
                return this.get('id');
            }
        });

        return Model;
    },
    extendCollection: function (Collection) {
        _.extend(Collection.prototype, {});

        return Collection;
    }
};

model = Alloy.M('shippingMethod', exports.definition, []);

collection = Alloy.C('shippingMethod', exports.definition, model);

exports.Model = model;
exports.Collection = collection;