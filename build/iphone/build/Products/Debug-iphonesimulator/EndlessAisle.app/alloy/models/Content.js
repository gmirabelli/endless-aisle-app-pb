var Alloy = require('/alloy'),
    _ = require("/alloy/underscore")._,
    model,
    collection;

exports.definition = {
    config: {
        model_name: 'content',
        secure: true,
        cache: false,
        adapter: {
            type: 'ocapi'
        }
    },
    extendModel: function (Model) {
        _.extend(Model.prototype, {

            idAttribute: 'id',
            urlRoot: '/content/',

            queryParams: function () {
                return {};
            },

            fetchContent: function () {
                return this.fetch();
            },

            getBody: function () {
                return this.get('c_body');
            }
        });

        return Model;
    },
    extendCollection: function (Collection) {
        _.extend(Collection.prototype, {});
        return Collection;
    }
};

model = Alloy.M('content', exports.definition, []);

collection = Alloy.C('content', exports.definition, model);

exports.Model = model;
exports.Collection = collection;