var Alloy = require('/alloy'),
    _ = require("/alloy/underscore")._,
    model,
    collection;

exports.definition = {
    config: {

        adapter: {
            type: 'properties',
            collection_name: 'addressDetails'
        }
    },

    extendModel: function (Model) {
        _.extend(Model.prototype, {
            getAddressComponents: function () {
                return this.get('address_components');
            },

            getName: function () {
                return this.get('name');
            },

            getPostalCode: function () {
                var postalCode = null;
                _.each(this.get('address_components'), function (child) {
                    if (child.types[0] === 'postal_code') {
                        postalCode = child.long_name;
                    }
                });
                return postalCode;
            },

            getStateCode: function () {
                var stateCode = null;
                _.each(this.get('address_components'), function (child) {
                    if (child.types[0] === 'administrative_area_level_1') {
                        stateCode = child.short_name;
                    }
                });
                return stateCode;
            },

            getCountryCode: function () {
                var countryCode = null;
                _.each(this.get('address_components'), function (child) {
                    if (child.types[0] === 'country') {
                        countryCode = child.short_name;
                    }
                });
                return countryCode;
            },

            getVicinity: function () {
                return this.get('vicinity');
            }
        });

        return Model;
    }
};

model = Alloy.M('googleAddressDetails', exports.definition, []);

collection = Alloy.C('googleAddressDetails', exports.definition, model);

exports.Model = model;
exports.Collection = collection;