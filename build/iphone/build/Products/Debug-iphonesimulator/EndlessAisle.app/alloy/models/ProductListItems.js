var Alloy = require('/alloy'),
    _ = require("/alloy/underscore")._,
    model,
    collection;

exports.definition = {
    config: {
        model_name: 'productListItem',
        adapter: {
            type: 'ocapi'
        }
    },
    extendModel: function (Model) {
        _.extend(Model.prototype, {
            getProductId: function () {
                return this.get('product_id');
            },

            getProductName: function () {
                return this.get('product_name');
            },

            getProductTitle: function () {
                return this.get('product_details_link').title;
            },

            getItemText: function () {
                return this.get('product_details_link').title;
            },

            getItemId: function () {
                return this.get('id');
            },

            getPurchasedQuantity: function () {
                return this.get('purchased_quantity');
            },

            getQuantity: function () {
                return this.get('quantity');
            },

            getPriority: function () {
                return this.get('priority');
            },

            geType: function () {
                return this.get('type');
            },

            getLink: function () {
                return this.get('link');
            },

            isPublic: function () {
                return this.get('public');
            }
        });

        return Model;
    },
    extendCollection: function (Collection) {
        _.extend(Collection.prototype, {
            getCount: function () {
                return this.length;
            },

            getTotal: function () {
                return this.length;
            },

            getTotalQuantity: function () {
                var total = 0;
                this.each(function (model) {
                    total += model.getQuantity();
                });
                return total;
            },

            getModelObjects: function () {
                var obj = [];
                this.each(function (model) {
                    obj.push(model);
                });
                return obj;
            },

            findItemByProductId: function (productId) {
                var models = this.filter(function (model) {
                    return productId == model.getProductId();
                });
                if (models.length > 0) {

                    return models[0];
                } else {
                    return null;
                }
            }
        });

        return Collection;
    }
};

model = Alloy.M('productListItems', exports.definition, []);

collection = Alloy.C('productListItems', exports.definition, model);

exports.Model = model;
exports.Collection = collection;