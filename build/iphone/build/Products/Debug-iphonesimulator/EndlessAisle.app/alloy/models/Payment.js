var Alloy = require('/alloy'),
    _ = require("/alloy/underscore")._,
    model,
    collection;

exports.definition = {
    config: {
        model_name: 'payment',
        adapter: {
            type: 'ocapi'
        }
    },

    extendModel: function (Model) {
        _.extend(Model.prototype, {
            getLastFourDigits: function () {
                if (this.has('masked_gift_certificate_code')) {
                    var card_number = this.get('masked_gift_certificate_code');
                    if (card_number) {
                        return card_number.substring(card_number.length - 4, card_number.length);
                    }
                } else {
                    return this.get('last_four_digits') || (this.get('payment_card') ? this.get('payment_card').number_last_digits : '');
                }
            },

            getPaymentMethod: function () {
                return this.get('payment_method');
            },

            getCanDelete: function () {
                return this.get('can_delete');
            },

            getCreditCardType: function () {
                return this.get('credit_card_type') || (this.get('payment_card') ? this.get('payment_card').card_type : '');
            },

            getAmountAuth: function () {
                return this.get('amt_auth') || this.get('amount');
            },

            isGiftCard: function () {
                var method_id = this.getPaymentMethod() ? this.getPaymentMethod() : this.get('payment_method_id');
                return method_id.toLowerCase().indexOf('gift_certificate') >= 0;
            },

            isCreditCard: function () {
                var method_id = this.getPaymentMethod() ? this.getPaymentMethod() : this.get('payment_method_id');
                return method_id.toLowerCase().indexOf('credit_card') >= 0;
            }
        });

        return Model;
    },
    extendCollection: function (Collection) {
        _.extend(Collection.prototype, {});

        return Collection;
    }
};

model = Alloy.M('payment', exports.definition, []);

collection = Alloy.C('payment', exports.definition, model);

exports.Model = model;
exports.Collection = collection;