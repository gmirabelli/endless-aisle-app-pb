var Alloy = require('/alloy'),
    _ = require("/alloy/underscore")._,
    model,
    collection;

exports.definition = {
    config: {
        model_name: 'order_price_adjustment',
        adapter: {
            type: 'ocapi'
        }
    },

    extendModel: function (Model) {
        _.extend(Model.prototype, {
            getPrice: function () {
                return this.get('price');
            }
        });

        return Model;
    },
    extendCollection: function (Collection) {
        _.extend(Collection.prototype, {});

        return Collection;
    }
};

model = Alloy.M('orderPriceAdjustment', exports.definition, []);

collection = Alloy.C('orderPriceAdjustment', exports.definition, model);

exports.Model = model;
exports.Collection = collection;