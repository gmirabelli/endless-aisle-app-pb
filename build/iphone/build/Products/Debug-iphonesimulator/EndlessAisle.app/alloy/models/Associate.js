var Alloy = require('/alloy'),
    _ = require("/alloy/underscore")._,
    model,
    collection;

var validateDevices = require('Validations').validateDevices;

var StorefrontHelperSecure = require('dw/instore/StorefrontHelperSecure');

var AssociateGetPermissions = StorefrontHelperSecure.extend({
    urlRoot: '/EAAccount-GetPermissions'
});

var AssociateSetDataOnNewSession = StorefrontHelperSecure.extend({
    urlRoot: '/EAAccount-SetDataOnNewSession'
});

var AssociateChangePassword = StorefrontHelperSecure.extend({
    urlRoot: '/EAAccount-ChangePassword'
});

var ValidateAssociateExists = StorefrontHelperSecure.extend({
    urlRoot: '/EAAccount-ValidateAssociateExists'
});

exports.definition = {
    config: {
        model_name: 'associate',
        secure: true,
        adapter: {
            type: 'storefront'
        }
    },

    extendModel: function (Model) {
        _.extend(Model.prototype, {

            urlRoot: '/EAAccount-AgentLogin',

            initialize: function () {
                this._isLoggedIn = false;
            },

            clear: function (options) {
                this._isLoggedIn = false;
                Backbone.Model.prototype.clear.call(this, options);
            },

            isLoggedIn: function (new_value) {
                if (this._isLoggedIn != new_value && (new_value === true || new_value === false)) {
                    this._isLoggedIn = new_value;
                }

                return this._isLoggedIn;
            },

            loginAssociate: function (login_info, options) {
                var deferred = new _.Deferred();
                var self = this;
                validateDevices().done(function () {
                    login_info = _.extend({}, login_info);
                    options = _.extend({}, {
                        silent: true,
                        wait: true
                    }, options);

                    self.clear({
                        silent: true
                    });

                    self.save(login_info, options).always(function () {
                        self._isLoggedIn = 'get' in this ? this.get('httpStatus') === 200 : false;
                    }).done(function () {
                        var sessionData = {
                            employeeId: this.get('employee_id'),
                            passcode: login_info.passcode,
                            storeId: Alloy.CFG.store_id.toString(),
                            appCurrency: Alloy.CFG.appCurrency,
                            country: Alloy.CFG.country
                        };
                        this.setDataOnNewSession(sessionData).done(function () {
                            deferred.resolve();
                        }).fail(function (model) {
                            deferred.reject(model);
                        });
                    }).fail(function (model) {
                        deferred.reject(model);
                    });
                }).fail(function (data) {
                    deferred.reject(data);
                });
                return deferred.promise();
            },

            logout: function (options) {
                var self = this;

                AgentLogout = Backbone.Model.extend({
                    config: {
                        secure: true,
                        model_name: 'agentLogout'
                    }
                }, {});
                var agentLogout = new AgentLogout();

                var url = this.url().replace('Login', 'Logout');

                var params = _.extend({}, {
                    url: url,
                    type: 'POST'
                });
                options = _.extend({}, {
                    wait: true,
                    cache: false,
                    success: function () {},
                    error: function () {}
                }, options);

                var logoutPromise = this.apiCall(agentLogout, params, options);

                logoutPromise.done(function () {
                    self.isLoggedIn(false);
                    self.clear();

                    Alloy.Models.authorizationToken.resetToken();
                });
                return logoutPromise;
            },

            setDataOnNewSession: function (login_info, options) {
                var setData = new AssociateSetDataOnNewSession();
                return setData.save(login_info, options);
            },

            fetchPermissions: function (login_info, options) {
                var permissions = new AssociateGetPermissions();
                var self = this;
                return permissions.save(login_info, options).done(function (model) {
                    self.set(model.toJSON(), {
                        silent: true
                    });
                    self.set(login_info, {
                        silent: true
                    });
                });
            },

            changePassword: function (new_password_info, options) {
                var deferred = new _.Deferred();
                var changePassword = new AssociateChangePassword();
                var self = this;
                changePassword.save(new_password_info, options).done(function (model) {
                    self.set(model.toJSON(), {
                        silent: true
                    });
                    deferred.resolve(model);
                }).fail(function (model) {
                    deferred.reject(model);
                });
                return deferred.promise();
            },

            validateAssociateExists: function (info, options) {
                var deferred = new _.Deferred();
                var validateAssociateExists = new ValidateAssociateExists();
                var self = this;
                validateAssociateExists.save(info, options).done(function (model) {
                    self.set(model.toJSON(), {
                        silent: true
                    });
                    deferred.resolve(model);
                }).fail(function (model) {
                    deferred.reject(model);
                });
                return deferred.promise();
            },

            getEmployeeId: function () {
                return this.get('employee_id');
            },

            getPasscode: function () {
                return this.get('passcode');
            },

            getAssociateInfo: function () {
                return this.get('associateInfo');
            },

            getFirstName: function () {
                var info = this.getAssociateInfo() || {};
                return info.firstName;
            },

            getLastName: function () {
                var info = this.getAssociateInfo() || {};
                return info.lastName;
            },

            getFullName: function () {
                var info = this.getAssociateInfo() || {};
                return info.firstName + ' ' + info.lastName;
            },

            hasPermissions: function () {
                return !!this.get('permissions');
            },

            getPermissions: function () {
                return this.get('permissions');
            },

            canDoManagerOverrides: function () {
                return this.has('permissions') && this.getPermissions().allowManagerOverrides;
            },

            hasAdminPrivileges: function () {
                return this.has('permissions') && this.getPermissions().allowAdmin;
            },

            hasStoreLevelSalesReportsPrivileges: function () {
                return this.has('permissions') && this.getPermissions().accessStoreLevelSalesReports;
            },

            hasSalesReportsPrivileges: function () {
                return this.has('permissions') && this.getPermissions().accessSalesReports;
            }
        });

        return Model;
    }
};

model = Alloy.M('associate', exports.definition, []);

collection = Alloy.C('associate', exports.definition, model);

exports.Model = model;
exports.Collection = collection;