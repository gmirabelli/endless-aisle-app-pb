var Alloy = require('/alloy'),
    _ = require("/alloy/underscore")._,
    model,
    collection;

exports.definition = {

    config: {
        model_name: 'recommendedAddress',
        secure: true,
        cache: false,
        adapter: {
            type: 'storefront'
        }
    },
    extendModel: function (Model) {
        _.extend(Model.prototype, {
            getAddress1: function () {
                return this.get('Address1');
            },

            getAddress2: function () {
                return this.get('Address2');
            },

            getCityStateZip: function () {
                return this.get('city') + ', ' + this.get('stateCode') + ' ' + this.get('postalCode');
            },

            transform: function () {
                var toReturn = {
                    address1: this.getAddress1(),
                    address2: this.getAddress2() ? this.getAddress2() : '',
                    city_state_zip: this.getCityStateZip()
                };
                return toReturn;
            }
        });

        return Model;
    },
    extendCollection: function (Collection) {
        _.extend(Collection.prototype, {});

        return Collection;
    }
};

model = Alloy.M('recommendedAddress', exports.definition, []);

collection = Alloy.C('recommendedAddress', exports.definition, model);

exports.Model = model;
exports.Collection = collection;