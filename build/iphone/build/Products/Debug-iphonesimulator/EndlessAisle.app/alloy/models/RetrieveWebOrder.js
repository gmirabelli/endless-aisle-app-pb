var Alloy = require('/alloy'),
    _ = require("/alloy/underscore")._,
    model,
    collection;

exports.definition = {
    config: {
        model_name: 'retrieveWebOrder',
        secure: true,
        cache: false,
        adapter: {
            type: 'storefront'
        }
    },

    extendModel: function (Model) {
        _.extend(Model.prototype, {

            urlRoot: '/EAOrder-RetrieveWebOrder'
        });
        return Model;
    }
};

model = Alloy.M('retrieveWebOrder', exports.definition, []);

collection = Alloy.C('retrieveWebOrder', exports.definition, model);

exports.Model = model;
exports.Collection = collection;