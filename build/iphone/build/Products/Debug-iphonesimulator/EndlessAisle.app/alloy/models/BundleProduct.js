var Alloy = require('/alloy'),
    _ = require("/alloy/underscore")._,
    model,
    collection;

exports.definition = {
    config: {
        model_name: 'bundleProduct',
        secure: false,
        adapter: {
            type: 'ocapi'
        }
    },

    extendModel: function (Model) {
        _.extend(Model.prototype, {

            relations: [{
                type: Backbone.One,
                key: 'product',
                relatedModel: require('alloy/models/Product').Model
            }],

            urlRoot: '/products'

        });
        return Model;
    },

    extendCollection: function (Collection) {
        _.extend(Collection.prototype, {});
        return Collection;
    }
};

model = Alloy.M('bundleProduct', exports.definition, []);

collection = Alloy.C('bundleProduct', exports.definition, model);

exports.Model = model;
exports.Collection = collection;