var Alloy = require('/alloy'),
    _ = require("/alloy/underscore")._,
    model,
    collection;

var logger = require('logging')('models:users', 'app/models/users');

function successFunction(model, resp, options) {
    if (!model.set(model.parse(resp, options), options)) {
        return false;
    }
    model.trigger('change:expiration_days');
}

exports.definition = {
    config: {
        model_name: 'users',
        useDataAPI: true,
        adapter: {
            type: 'ocapi'
        }
    },

    extendModel: function (Model) {
        _.extend(Model.prototype, {

            urlRoot: '/users/this',

            fetchExpiration: function () {
                var deferred = new _.Deferred();
                var self = this;
                var url = this.urlRoot;

                Alloy.Models.authorizationToken.fetchToken(Alloy.CFG.storefront_host).done(function () {
                    var params = _.extend({}, {
                        url: url,
                        type: 'GET',
                        headers: {
                            'content-type': 'application/json',
                            Authorization: 'Bearer ' + Alloy.Models.authorizationToken.getToken()
                        }
                    });
                    var options = _.extend({}, {
                        wait: true,
                        cache: false,
                        dontincludeid: true,
                        error: function () {}
                    }, options);
                    options.success = successFunction;

                    self.apiCall(self, params, options).done(function () {
                        deferred.resolve();
                    }).fail(function (fault) {
                        var message = _L('Unable to obtain store password expiration.');
                        var type = 'UnknownException';
                        if (fault && fault.has('fault')) {
                            type = fault.get('fault').type;
                            switch (type) {
                                case 'UserNotAvailableException':
                                    message = _L('The user cannot be found.');
                                    break;
                                case 'UserIsLockedException':
                                    message = _L('The user is locked.');
                                    break;
                            }
                        }
                        deferred.reject({
                            type: type,
                            message: message
                        });
                    });
                }).fail(function () {
                    deferred.reject();
                });
                return deferred.promise();
            },

            updateStorePassword: function (oldPassword, password) {
                var deferred = new _.Deferred();
                var self = this;
                var url = this.urlRoot + '/password';

                var data = {
                    current_password: oldPassword,
                    password: password
                };
                data = JSON.stringify(data);
                Alloy.Models.authorizationToken.fetchToken(Alloy.CFG.storefront_host).done(function () {
                    var params = _.extend({}, {
                        url: url,
                        type: 'PATCH',
                        data: data,
                        headers: {
                            'content-type': 'application/json',
                            Authorization: 'Bearer ' + Alloy.Models.authorizationToken.getToken()
                        }
                    });
                    var options = _.extend({}, {
                        wait: true,
                        cache: false,
                        dontincludeid: true,
                        error: function () {}
                    }, options);
                    options.success = successFunction;

                    self.apiCall(self, params, options).done(function () {
                        deferred.resolve();
                    }).fail(function (fault) {
                        var message = _L('Unable to update the store password.');
                        var type = 'UnknownException';
                        if (fault && fault.has('fault')) {
                            type = fault.get('fault').type;
                            switch (type) {
                                case 'PasswordPolicyViolationException':
                                    message = _L('The provided new password does not meet the acceptance criteria.');
                                    break;
                                case 'PasswordNotValidForReuseException':
                                    message = _L('The password was recently used and is not valid for reuse.');
                                    break;
                                case 'InvalidPasswordException':
                                    message = _L('The current user password is invalid.');
                                    break;
                                case 'UserNotAvailableException':
                                    message = _L('The user cannot be found.');
                                    break;
                                case 'UserIsLockedException':
                                    message = _L('The user is locked.');
                                    break;
                            }
                        }
                        deferred.reject({
                            type: type,
                            message: message
                        });
                    });
                }).fail(function () {
                    deferred.reject();
                });
                return deferred.promise();
            },

            getStoreUsername: function () {
                return this.get('login');
            },

            getExpirationDays: function () {
                var days = -1;
                var expString = this.get('password_expiration_date');
                if (expString) {
                    var today = new Date();
                    var msec = Date.parse(expString);
                    var expDate = new Date(msec);
                    var timeDiff = Math.abs(expDate.getTime() - today.getTime());
                    days = Math.ceil(timeDiff / (1000 * 3600 * 24));
                }
                return days;
            }
        });

        return Model;
    }
};

model = Alloy.M('users', exports.definition, []);

collection = Alloy.C('users', exports.definition, model);

exports.Model = model;
exports.Collection = collection;