
exports.HORIZONTAL = 'horizontal';

exports.VERTICAL = 'vertical';

exports.flip = true ? function (from, to, direction, duration, finishCallback) {
	var vertical = direction === exports.VERTICAL;
	var flipped_matrix = Ti.UI.create3DMatrix().rotate(-90, vertical ? 1 : 0, vertical ? 0 : 1, 0);
	var from_animation = Ti.UI.createAnimation({
		transform: flipped_matrix,
		duration: duration
	});
	to.transform = flipped_matrix;
	from.animate(from_animation, function () {
		var unflipped_matrix = Ti.UI.create3DMatrix().rotate(0, vertical ? 1 : 0, vertical ? 0 : 1, 0);
		var to_animation = Ti.UI.createAnimation({
			transform: unflipped_matrix,
			duration: duration
		});
		finishCallback ? to.animate(to_animation, finishCallback) : to.animate(to_animation);
	});
} : function () {
	Ti.API.error('The builtin flip-animation is iOS-only.');
};

exports.flipHorizontal = function (from, to, duration, finishCallback) {
	exports.flip(from, to, exports.HORIZONTAL, duration, finishCallback);
};

exports.flipVertical = function (from, to, duration, finishCallback) {
	exports.flip(from, to, exports.VERTICAL, duration, finishCallback);
};

exports.crossFade = function (from, to, duration, finishCallback) {
	if (from) from.animate({
		opacity: 0,
		duration: duration
	});
	if (to) to.animate({
		opacity: 1,
		duration: duration
	});
	if (finishCallback) setTimeout(finishCallback, duration + 300);
};

exports.fadeAndRemove = function (from, duration, container, finishCallback) {
	if (from && container) {
		from.animate({
			opacity: 0,
			duration: duration
		}, function () {
			container.remove(from);
			container = from = duration = null;
			if (finishCallback) finishCallback();
		});
	}
};

exports.fadeIn = function (to, duration, finishCallback) {
	if (finishCallback) {
		if (to) {
			to.animate({
				opacity: 1,
				duration: duration
			}, finishCallback);
		}
	} else {
		if (to) {
			to.animate({
				opacity: 1,
				duration: duration
			});
		}
	}
};

exports.fadeOut = function (to, duration, finishCallback) {
	if (finishCallback) {
		if (to) {
			to.animate({
				opacity: 0,
				duration: duration
			}, finishCallback);
		}
	} else {
		if (to) {
			to.animate({
				opacity: 0,
				duration: duration
			});
		}
	}
};

exports.popIn = function (view, finishCallback) {
	if (!true) {
		view.transform = Ti.UI.create2DMatrix();
		view.opacity = 1;
		return;
	}

	var animate1 = Ti.UI.createAnimation({
		opacity: 1,
		transform: Ti.UI.create2DMatrix().scale(1.05, 1.05),
		duration: 200
	});
	var animate2 = Ti.UI.createAnimation({
		transform: Ti.UI.create2DMatrix(),
		duration: 300
	});

	exports.chainAnimate(view, [animate1, animate2], finishCallback);
	view = null;
};

exports.shake = function (view, delay, finishCallback) {
	var shake1 = Ti.UI.createAnimation({
		transform: Ti.UI.create2DMatrix().translate(5, 0),
		duration: 100
	});
	var shake2 = Ti.UI.createAnimation({
		transform: Ti.UI.create2DMatrix().translate(-5, 0),
		duration: 100
	});
	var shake3 = Ti.UI.createAnimation({
		transform: Ti.UI.create2DMatrix().translate(5, 0),
		duration: 100
	});
	var shake4 = Ti.UI.createAnimation({
		transform: Ti.UI.create2DMatrix().translate(-5, 0),
		duration: 100
	});
	var shake5 = Ti.UI.createAnimation({
		transform: Ti.UI.create2DMatrix(),
		duration: 100
	});
	if (delay) {
		setTimeout(function () {
			exports.chainAnimate(view, [shake1, shake2, shake3, shake4, shake5], finishCallback);
			view = shake1 = shake2 = shake3 = shake4 = shake5 = null;
		}, delay);
	} else {
		exports.chainAnimate(view, [shake1, shake2, shake3, shake4, shake5], finishCallback);
	}
};

exports.flash = function (view, delay, finishCallback) {
	var flash1 = Ti.UI.createAnimation({
		opacity: 0.7,
		duration: 100
	});
	var flash2 = Ti.UI.createAnimation({
		opacity: 1,
		duration: 100
	});
	var flash3 = Ti.UI.createAnimation({
		opacity: 0.7,
		duration: 100
	});
	var flash4 = Ti.UI.createAnimation({
		opacity: 1,
		duration: 100
	});
	if (delay) {
		setTimeout(function () {
			exports.chainAnimate(view, [flash1, flash2, flash3, flash4], finishCallback);
			view = flash1 = flash2 = flash3 = flash4 = null;
		}, delay);
	} else {
		exports.chainAnimate(view, [flash1, flash2, flash3, flash4], finishCallback);
	}
};

exports.chainAnimate = function (view, animations, finishCallback) {
	function step() {
		if (animations.length === 0) {
			view = animations = null;
			if (finishCallback) finishCallback();
			return;
		}
		var animation = animations.shift();
		animation.addEventListener('complete', step);
		view.animate(animation);
	}

	step();
};