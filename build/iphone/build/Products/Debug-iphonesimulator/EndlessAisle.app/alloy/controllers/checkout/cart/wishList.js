var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'checkout/cart/wishList';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};




  $.customerWishListItems = Alloy.createCollection('productItem');


  if (Alloy.CFG.enable_wish_list) {
    $.__views.wish_list_products_container = Ti.UI.createView(
    { top: 0, layout: "vertical", width: "100%", height: "100%", id: "wish_list_products_container", visible: true });

    $.__views.wish_list_products_container && $.addTopLevelView($.__views.wish_list_products_container);
    $.__views.wish_list_selector_container = Ti.UI.createView(
    { top: 5, width: Ti.UI.FILL, height: 50, backgroundColor: Alloy.Styles.color.background.white, id: "wish_list_selector_container" });

    $.__views.wish_list_products_container.add($.__views.wish_list_selector_container);
    $.__views.email_wish_list_button = Ti.UI.createButton(
    { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.buttons.primary.color, disabledColor: Alloy.Styles.buttons.primary.disabledColor, backgroundImage: Alloy.Styles.buttons.primary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.primary.backgroundDisabledImage, height: 40, width: 200, titleid: "_Email_Wish_List", right: 20, id: "email_wish_list_button", accessibilityValue: "email_wish_list_button" });

    $.__views.wish_list_selector_container.add($.__views.email_wish_list_button);
    showEmailWishListDialog ? $.addListener($.__views.email_wish_list_button, 'click', showEmailWishListDialog) : __defers['$.__views.email_wish_list_button!click!showEmailWishListDialog'] = true;$.__views.wish_list_products_table = Ti.UI.createTableView(
    { top: 5, height: 600, separatorStyle: "transparent", id: "wish_list_products_table" });

    $.__views.wish_list_products_container.add($.__views.wish_list_products_table);
    var __alloyId46 = Alloy.Collections['$.customerWishListItems'] || $.customerWishListItems;function __alloyId47(e) {if (e && e.fromAdapter) {return;}var opts = __alloyId47.opts || {};var models = __alloyId46.models;var len = models.length;var rows = [];for (var i = 0; i < len; i++) {var __alloyId43 = models[i];__alloyId43.__transform = transformWishListPLI(__alloyId43);var __alloyId45 = Alloy.createController('checkout/cart/wishListItemRow', { $model: __alloyId43, __parentSymbol: __parentSymbol });
        rows.push(__alloyId45.getViewEx({ recurse: true }));
      }$.__views.wish_list_products_table.setData(rows);};__alloyId46.on('fetch destroy change add remove reset', __alloyId47);handleWishListItemClick ? $.addListener($.__views.wish_list_products_table, 'click', handleWishListItemClick) : __defers['$.__views.wish_list_products_table!click!handleWishListItemClick'] = true;$.__views.no_wish_list_products_container = Ti.UI.createView(
    { height: 0, width: Ti.UI.FILL, top: 0, backgroundColor: Alloy.Styles.color.background.white, id: "no_wish_list_products_container" });

    $.__views.wish_list_products_container.add($.__views.no_wish_list_products_container);
    $.__views.no_wish_list_products_label = Ti.UI.createLabel(
    { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.tabFont, top: 50, id: "no_wish_list_products_label", accessibilityValue: "no_wish_list_products_label" });

    $.__views.no_wish_list_products_container.add($.__views.no_wish_list_products_label);
  }
  exports.destroy = function () {__alloyId46 && __alloyId46.off('fetch destroy change add remove reset', __alloyId47);};




  _.extend($, $.__views);










  var args = arguments[0] || {};
  var confirmDeleteItem = args.confirmDeleteItem;
  var addToCartFromAnotherList = args.addToCartFromAnotherList;
  var navigateToProduct = args.navigateToProduct;
  var updateWishListButtonTitle = args.updateWishListButtonTitle;
  var fetchImageForProducts = args.fetchImageForProducts;
  var getWishListNameFromPListArray = args.getWishListNameFromPListArray;
  var currentCustomer = Alloy.Models.customer;
  var cCProductLists = currentCustomer.productLists;
  var selectedWishListId;
  var wishListSelectorController;
  var wishListRenderedOnce = false;
  var EAUtils = require('EAUtils');
  var logger = require('logging')('checkout:cart:wishList', getFullControllerPath($.__controllerPath));


  var buttonTextLength = 20;




  if ($.email_wish_list_button.getTitle().length > buttonTextLength || EAUtils.isSymbolBasedLanguage()) {
    $.email_wish_list_button.setFont(Alloy.Styles.smallButtonFont);
  }

  $.listenTo(cCProductLists, 'reset', firstTimeWishListRender);




  exports.deinit = deinit;









  function deinit() {
    logger.info('Calling deinit, removing listeners');
    $.email_wish_list_button.removeEventListener('click', showEmailWishListDialog);
    deinitRows();
    if (wishListSelectorController) {
      $.wish_list_selector_container.removeAllChildren();
      $.stopListening(wishListSelectorController);
      wishListSelectorController.deinit();
      wishListSelectorController = null;
    }
    $.stopListening();
    $.destroy();
  }









  function deinitRows() {
    if ($.wish_list_products_table.getSections().length > 0) {
      _.each($.wish_list_products_table.getSections()[0].getRows(), function (row) {
        row.deinit();
      });
    }
  }








  function deleteWishListItem(wishListId, wishListItemId) {
    logger.info('delete wish List item ');
    var promise = cCProductLists.deleteItem(wishListId, wishListItemId);
    Alloy.Router.showActivityIndicator(promise);
  }







  function handleWishListItemClick(event) {
    var item = cCProductLists.getListItems(selectedWishListId).at(event.index);
    var productId = item.getProductId();
    var productName = item.getProductName();
    var itemId = item.getItemId();
    switch (event.source.id) {
      case 'add_to_cart_button':

        if (_.isFunction(addToCartFromAnotherList)) {
          addToCartFromAnotherList(item);
        }
        break;
      case 'edit_button':

        if (_.isFunction(navigateToProduct)) {
          var itemJSON = item.toJSON();
          itemJSON.index = event.index;
          itemJSON.replaceInWishList = {
            listId: selectedWishListId,
            listItemId: item.getItemId() };

          navigateToProduct(item.getProductId(), itemJSON);
        }

        break;
      case 'delete_button':

        if (_.isFunction(confirmDeleteItem)) {
          confirmDeleteItem({
            index: event.index,
            product_name: productName,
            wishListId: selectedWishListId,
            wishListItemId: item.getItemId() },
          deleteWishListItem);
        }
        break;

      default:
        break;}

  }







  function initWishListItemsCollection(collection) {

    updateWishListViewAndButtonTitle(collection.getTotalQuantity());


    deinitRows();
    if (collection && collection.getCount() > 0) {
      logger.info('Calling initWishListItemsCollection with data');

      $.customerWishListItems.once('reset', function () {
        logger.info('Fetching images for saved plis');
        if ($.wish_list_products_table.getSections().length > 0) {
          fetchImageForProducts(collection.getModelObjects(), $.wish_list_products_table);
        }
      });

      $.customerWishListItems.reset(collection.toJSON());
    } else {
      logger.info('Calling initWishListItemsCollection with no data');
      $.customerWishListItems.reset([]);
    }
  }







  function updateWishListViewAndButtonTitle(count) {
    if (_.isFunction(updateWishListButtonTitle)) {
      updateWishListButtonTitle(count);
    }
    if (count <= 0) {
      if (count < 0) {
        $.no_wish_list_products_label.setText(String.format(_L('%s does not have a wish list'), currentCustomer.getFullName()));
      } else {
        $.no_wish_list_products_label.setText(_L('There are no items in this wish list'));
      }
      $.wish_list_products_table.hide();
      $.wish_list_products_table.setHeight(0);
      $.no_wish_list_products_container.setHeight(Ti.UI.FILL);
      $.no_wish_list_products_container.show();
      $.email_wish_list_button.setEnabled(false);
      $.email_wish_list_button.setTouchEnabled(false);
    } else {
      $.no_wish_list_products_container.hide();
      $.no_wish_list_products_container.setHeight(0);
      $.wish_list_products_table.setHeight(Ti.UI.FILL);
      $.wish_list_products_table.show();
      $.email_wish_list_button.setEnabled(true);
      $.email_wish_list_button.setTouchEnabled(true);
    }
  }







  function renderWishList(wishListId) {
    var currentWishListId = wishListId || cCProductLists.getFirstWishListId();
    selectedWishListId = currentWishListId;
    if (currentCustomer.isLoggedIn()) {
      if (cCProductLists.getWishListCount() > 1) {
        if (wishListSelectorController) {
          wishListSelectorController.updateSelectedItem(currentWishListId);
        }
      } else {
        if (cCProductLists.getWishListCount() == 1) {
          loadInitSelectedWishList(currentWishListId);
        } else if (cCProductLists.getWishListCount() == 0) {
          updateWishListViewAndButtonTitle(-1);
        }
      }
    }
  }







  function loadInitSelectedWishList(wishListId) {
    if (wishListId) {
      initWishListItemsCollection(cCProductLists.getListItems(wishListId));
    }
  }






  function showEmailWishListDialog() {
    var successMsg = _L('Your Wish List has been emailed.');
    var wishListName = _.isFunction(getWishListNameFromPListArray) ? getWishListNameFromPListArray(cCProductLists.where({
      id: selectedWishListId })) :
    '';
    if (wishListName != '') {
      successMsg = String.format(_L('Your Wish List %s has been emailed.'), wishListName);
    }
    Alloy.Dialog.showCustomDialog({
      controllerPath: 'checkout/cart/emailCollectorDialog',
      cancelEvent: 'email_collector_dialog:dismiss',
      continueEvent: 'email_collector_dialog:continue',
      options: {
        emailData: {
          productListId: selectedWishListId,
          senderEmail: currentCustomer.getEmail(),
          senderName: currentCustomer.getFullName() },

        successNotifyMessage: successMsg,
        failNotifyMessage: _L('Your email failed to be sent. Please try again later.') } }).

    focus();
  }






  function renderWishListSelector() {

    var selectorObj = cCProductLists.getWishListSelectorObjects();

    removeWishListSelector();

    wishListSelectorController = Alloy.createController('components/selectWidget', {
      valueField: 'wishListId',
      textField: 'wishListName',
      values: selectorObj,
      messageWhenSelection: '',
      messageWhenNoSelection: _L('Select Wish List'),
      selectListTitleStyle: {
        backgroundColor: Alloy.Styles.color.background.white,
        font: Alloy.Styles.tabFont,
        color: Alloy.Styles.accentColor,
        disabledColor: Alloy.Styles.color.text.light,
        width: Ti.UI.FILL,
        height: Ti.UI.FILL,
        disabledBackgroundColor: Alloy.Styles.color.background.light,
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        accessibilityValue: 'wish_list_selector' },

      selectListStyle: {
        width: 320,
        left: 20,
        height: 40,
        top: 5,
        bottom: 5,
        font: Alloy.Styles.textFieldFont,
        selectedFont: Alloy.Styles.textFieldFont,
        unselectedFont: Alloy.Styles.textFieldFont,
        color: Alloy.Styles.color.text.darkest,
        selectedOptionColor: Alloy.Styles.color.text.darkest,
        disabledColor: Alloy.Styles.color.text.light,
        backgroundColor: Alloy.Styles.color.background.white,
        selectedOptionBackgroundColor: Alloy.Styles.color.background.light,
        disabledBackgroundColor: Alloy.Styles.color.background.light,
        borderColor: Alloy.Styles.accentColor },

      needsCallbackAfterClick: true });


    $.listenTo(wishListSelectorController, 'itemSelected', function (event) {
      if (event.item) {
        selectedWishListId = event.item.wishListId;
        loadInitSelectedWishList(selectedWishListId);
      }
    });
    $.listenTo(wishListSelectorController, 'dropdownSelected', function () {
      wishListSelectorController.continueAfterClick();
    });

    wishListSelectorController.updateItems(selectorObj);
    wishListSelectorController.setEnabled(true);
    $.wish_list_selector_container.add(wishListSelectorController.getView());
    $.wish_list_selector_container.show();

    $.email_wish_list_button.setRight(20);
  }






  function removeWishListSelector() {
    _.each($.wish_list_selector_container.getChildren(), function (childView) {
      if (childView.id !== 'email_wish_list_button') {
        $.wish_list_selector_container.remove(childView);
      } else {

        childView.setRight(null);
        childView.setLeft(null);
      }
    });
    if (wishListSelectorController) {
      $.stopListening(wishListSelectorController);
      wishListSelectorController.deinit();
      wishListSelectorController = null;
    }
  }





  function transformWishListPLI(model) {
    logger.info('transform wish list PLI');
    return {
      row_id: 'id_' + model.getProductId() };

  }









  function firstTimeWishListRender() {
    logger.info('Responding to product lists reset event');
    if (wishListRenderedOnce) {
      return;
    }
    if (currentCustomer.isLoggedIn()) {
      wishListRenderedOnce = true;
    }
    if (cCProductLists.getWishListCount() > 1) {

      renderWishListSelector();
    } else {
      removeWishListSelector();
    }
    renderWishList();

    _.each(cCProductLists.getAllWishLists(), function (list) {
      $.listenTo(list, 'item:deleted item:added item:updated', function () {
        renderWishList(list.getId());
      });
    });
  }











  firstTimeWishListRender();





  if (Alloy.CFG.enable_wish_list) {
    __defers['$.__views.email_wish_list_button!click!showEmailWishListDialog'] && $.addListener($.__views.email_wish_list_button, 'click', showEmailWishListDialog);}
  __defers['$.__views.wish_list_products_table!click!handleWishListItemClick'] && $.addListener($.__views.wish_list_products_table, 'click', handleWishListItemClick);



  _.extend($, exports);
}

module.exports = Controller;