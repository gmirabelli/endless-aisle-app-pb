var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'checkout/shippingMethod/shippingOverrides';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.shipping_overrides_window = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.color.background.transparent, zIndex: 101, id: "shipping_overrides_window" });

  $.__views.shipping_overrides_window && $.addTopLevelView($.__views.shipping_overrides_window);
  $.__views.backdrop = Ti.UI.createView(
  { top: 0, left: 0, width: 1024, height: 768, opacity: 0.55, backgroundColor: Alloy.Styles.color.background.black, id: "backdrop" });

  $.__views.shipping_overrides_window.add($.__views.backdrop);
  $.__views.shipping_overrides_container = Ti.UI.createView(
  { layout: "vertical", width: 785, left: 120, height: 507, top: 47, backgroundColor: Alloy.Styles.color.background.white, id: "shipping_overrides_container" });

  $.__views.shipping_overrides_window.add($.__views.shipping_overrides_container);
  $.__views.shipping_overrides_header = Ti.UI.createView(
  { layout: "horizontal", height: Ti.UI.SIZE, width: "100%", top: 13, id: "shipping_overrides_header" });

  $.__views.shipping_overrides_container.add($.__views.shipping_overrides_header);
  $.__views.cancel_button = Ti.UI.createButton(
  { backgroundImage: Alloy.Styles.secondaryButtonImage, color: Alloy.Styles.buttons.secondary.color, width: 146, left: 15, height: 35, font: Alloy.Styles.buttonFont, id: "cancel_button", titleid: "_Cancel", accessibilityValue: "cancel_button_shipping_override" });

  $.__views.shipping_overrides_header.add($.__views.cancel_button);
  $.__views.shipping_overrides_label = Ti.UI.createLabel(
  { width: 290, height: Ti.UI.SIZE, color: Alloy.Styles.accentColor, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.headlineFont, left: 125, top: 0, textid: "_Shipping_Overrides", id: "shipping_overrides_label", accessibilityValue: "shipping_overrides_label" });

  $.__views.shipping_overrides_header.add($.__views.shipping_overrides_label);
  $.__views.apply_button = Ti.UI.createButton(
  { backgroundImage: Alloy.Styles.primaryButtonImage, color: Alloy.Styles.buttons.primary.color, width: 146, height: 35, font: Alloy.Styles.buttonFont, left: 50, id: "apply_button", titleid: "_Apply", accessibilityValue: "apply_button_shipping_override" });

  $.__views.shipping_overrides_header.add($.__views.apply_button);
  $.__views.__alloyId71 = Ti.UI.createView(
  { id: "__alloyId71" });

  $.__views.shipping_overrides_container.add($.__views.__alloyId71);
  $.__views.show_overrides = Ti.UI.createView(
  { left: 43, id: "show_overrides" });

  $.__views.__alloyId71.add($.__views.show_overrides);
  $.__views.overrides = Alloy.createController('checkout/cart/productPriceOverrides', { id: "overrides", __parentSymbol: $.__views.show_overrides });
  $.__views.overrides.setParent($.__views.show_overrides);
  $.__views.manager_override_already_exists = Ti.UI.createView(
  { top: 100, id: "manager_override_already_exists", visible: false });

  $.__views.__alloyId71.add($.__views.manager_override_already_exists);
  $.__views.manager_override_label = Ti.UI.createLabel(
  { width: 450, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.subtitleFont, top: 0, textid: "_A_manager_override_has_already_been_applied__Click_Remove_to_remove_the_override_and_continue_to_the_override_screen_", id: "manager_override_label", accessibilityValue: "manager_override_label" });

  $.__views.manager_override_already_exists.add($.__views.manager_override_label);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var currentBasket = Alloy.Models.basket;
  var currentAssociate = Alloy.Models.associate;
  var showActivityIndicator = require('dialogUtils').showActivityIndicator;
  var price_override_type = 'none';
  var price_override_value = null;
  var selectedShippingMethod = null;
  var selectedShippingOverride = null;
  var overrideReason = null;

  var doingManagerAuthorization = false;
  var associatePermissions = null;
  var isRemovingOverride = false;

  var isValid = false;




  $.cancel_button.addEventListener('click', onCancelClick);

  $.apply_button.addEventListener('click', onApplyClick);

  $.listenTo($.overrides, 'enableApply', onEnableApply);



  $.listenTo($.overrides, 'managerLoggedIn', onManagerLoggedIn);


  $.listenTo($.overrides, 'needManagerAuthorization', onNeedsManagerAuthorization);




  exports.init = init;
  exports.deinit = deinit;











  function init(args) {
    var deferred = new _.Deferred();
    selectedShippingOverride = args.selectedOverride;
    selectedShippingMethod = args.selectedMethod;

    if (selectedShippingOverride && selectedShippingOverride.manager_employee_id) {
      isRemovingOverride = true;
      $.manager_override_already_exists.show();
      $.show_overrides.hide();
      $.apply_button.setTitle(_L('Remove'));
      isRemovingOverride = true;
      $.apply_button.setEnabled(true);
      deferred.resolve();
    } else {

      $.manager_override_already_exists.hide();
      $.show_overrides.show();
      isRemovingOverride = false;
      initOverrides(deferred);
    }
    return deferred.promise();
  }






  function deinit() {
    $.overrides.deinit();
    $.cancel_button.removeEventListener('click', onCancelClick);
    $.apply_button.removeEventListener('click', onApplyClick);
    $.stopListening($.overrides, 'enableApply', onEnableApply);
    $.stopListening($.overrides, 'managerLoggedIn', onManagerLoggedIn);
    $.stopListening($.overrides, 'needManagerAuthorization', onNeedsManagerAuthorization);
    $.stopListening();
    $.destroy();
  }










  function initOverrides(deferred) {
    var assoc = currentAssociate;
    if (isKioskManagerLoggedIn()) {
      assoc = Alloy.Kiosk.manager;
    }

    var permissions = {
      allowByAmount: assoc.getPermissions().allowShippingOverrideByAmount,
      allowByPercent: assoc.getPermissions().allowShippingOverrideByPercent,
      allowFixed: assoc.getPermissions().allowShippingOverrideFixed };


    $.overrides.init({
      basePrice: selectedShippingMethod.getBasePrice(),
      surcharge: selectedShippingMethod.getSurcharge(),
      permissions: permissions,
      maxPercentOff: assoc.getPermissions().shippingPriceOverrideMaxPercent,
      selectedOverride: selectedShippingOverride,
      adjustmentReasons: Alloy.CFG.shipping_override_reasons });

    if (deferred) {
      deferred.resolve();
    }
  }






  function dismiss() {
    $.trigger('shippingOverride:dismiss');
  }









  function onCancelClick() {
    $.overrides.hideDropdown();

    if (doingManagerAuthorization) {

      if (associatePermissions && associatePermissions.permissions === 'none') {
        dismiss();
      }

      $.shipping_overrides_label.setText(_L('Shipping Overrides'));
      $.overrides.cancelOverride();
      doingManagerAuthorization = false;
    } else {

      dismiss();
    }
  }






  function onApplyClick() {
    var deferred = new _.Deferred();
    showActivityIndicator(deferred);
    $.overrides.hideDropdown();

    if (isRemovingOverride) {
      currentBasket.setShippingPriceOverride({
        shipping_method_id: selectedShippingMethod.id,
        price_override_type: 'none',
        employee_id: Alloy.Models.associate.getEmployeeId(),
        employee_passcode: Alloy.Models.associate.getPasscode(),
        store_id: Alloy.CFG.store_id },
      {
        c_employee_id: Alloy.Models.associate.getEmployeeId() }).
      done(function () {
        selectedShippingOverride = {
          price_override_type: 'none' };

        initOverrides();
        $.manager_override_already_exists.hide();
        $.show_overrides.show();
        isRemovingOverride = false;
        $.apply_button.setTitle(_L('Apply'));
      }).always(function () {
        deferred.resolve();
      });
    } else if (doingManagerAuthorization) {

      $.overrides.doManagerAuthorization().always(function () {
        deferred.resolve();
      });
    } else {

      var override = $.overrides.getOverride();
      if (override) {
        override.shipping_method_id = selectedShippingMethod.id;
        override.employee_id = Alloy.Models.associate.getEmployeeId();
        override.employee_passcode = Alloy.Models.associate.getPasscode();
        override.store_id = Alloy.CFG.store_id;
        if (!override.manager_employee_id && isKioskManagerLoggedIn()) {
          override.manager_employee_id = getKioskManager().getEmployeeId();
          override.manager_employee_passcode = getKioskManager().getPasscode();
          override.manager_allowLOBO = getKioskManager().getPermissions().allowLOBO;
        }
        override.kiosk_mode = isKioskMode();
        currentBasket.setShippingPriceOverride(override, {
          c_employee_id: Alloy.Models.associate.getEmployeeId() }).
        done(function () {
          dismiss();
        }).fail(function () {
          notify(_L('Unable to apply a shipping price override'), {
            preventAutoClose: true });

        }).always(function () {
          deferred.resolve();
        });
      } else {
        dismiss();
        deferred.resolve();
      }
    }
  }






  function onEnableApply(enable) {
    if (!isRemovingOverride) {
      $.apply_button.setEnabled(enable.valid);
    }
  }








  function onManagerLoggedIn(manager) {
    var permissions = {
      allowByAmount: manager.getPermissions().allowShippingOverrideByAmount,
      allowByPercent: manager.getPermissions().allowShippingOverrideByPercent,
      allowFixed: manager.getPermissions().allowShippingOverrideFixed,
      maxPercentOff: manager.getPermissions().shippingPriceOverrideMaxPercent };

    doingManagerAuthorization = false;
    $.overrides.resetPermissions(permissions);
    $.shipping_overrides_label.setText(_L('Shipping Overrides'));
  }







  function onNeedsManagerAuthorization(permissions) {
    $.shipping_overrides_label.setText(_L('Manager Authorization'));
    doingManagerAuthorization = true;
    associatePermissions = permissions;
    $.apply_button.setEnabled(true);
  }









  _.extend($, exports);
}

module.exports = Controller;