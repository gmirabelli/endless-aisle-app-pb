var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'product/tabs/recommendations';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.no_recommendations_container = Ti.UI.createView(
  { id: "no_recommendations_container" });

  $.__views.no_recommendations_container && $.addTopLevelView($.__views.no_recommendations_container);
  $.__views.no_recommendations_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.buttonFont, top: 50, textid: "_There_are_no_recommendations_for_this_product_", id: "no_recommendations_label", accessibilityValue: "no_recommendations_label" });

  $.__views.no_recommendations_container.add($.__views.no_recommendations_label);
  $.__views.container = Ti.UI.createView(
  { layout: "vertical", left: 7, id: "container" });

  $.__views.container && $.addTopLevelView($.__views.container);
  $.__views.recommendations_tab_header = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, textid: "_Recommendations_For_You", left: 0, font: Alloy.Styles.buttonFont, top: 32, id: "recommendations_tab_header", accessibilityValue: "recommendations_tab_header_label" });

  $.__views.container.add($.__views.recommendations_tab_header);
  $.__views.recommendations_container = Ti.UI.createView(
  { layout: "vertical", width: Ti.UI.SIZE, height: Ti.UI.SIZE, top: 20, id: "recommendations_container" });

  $.__views.container.add($.__views.recommendations_container);
  $.__views.recommendations_scroller_container = Ti.UI.createView(
  { layout: "horizontal", height: Ti.UI.SIZE, id: "recommendations_scroller_container" });

  $.__views.recommendations_container.add($.__views.recommendations_scroller_container);
  $.__views.paging = Ti.UI.createView(
  { layout: "composite", width: "100%", height: 60, left: 0, bottom: 30, id: "paging" });

  $.__views.container.add($.__views.paging);
  $.__views.paging_container = Ti.UI.createView(
  { id: "paging_container" });

  $.__views.paging.add($.__views.paging_container);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var eaUtils = require('EAUtils');
  var PagingControl = require('PagingControl');
  var previouslyFetchedId = null;
  var args = arguments[0] || {},
  currentProduct = args.product || Alloy.Models.product;
  var logger = require('logging')('product:tabs:recommendations', getFullControllerPath($.__controllerPath));




  exports.init = init;
  exports.deinit = deinit;










  function init() {
    logger.info('init called');
    if (!currentProduct.recommendations) {
      return;
    }

    var def = new _.Deferred();
    var id = currentProduct.getId();

    if (previouslyFetchedId == id) {
      def.resolve();
      return def.promise();
    }

    Alloy.Router.showActivityIndicator(def);
    currentProduct.recommendations.getRecommendations(id).done(function () {
      previouslyFetchedId = id;
      render();
      def.resolve();
    }).fail(function () {
      def.reject();
    });
    return def.promise();
  }






  function render() {
    logger.info('render called');
    var recommendedItems = currentProduct.recommendations ? currentProduct.recommendations.getRecommendedItems() : null;

    if (!recommendedItems || !recommendedItems.length) {
      logger.info('NO RECOMMENDED ITEMS!');
      $.no_recommendations_container.setVisible(true);
      $.container.setVisible(false);
      $.container.setHeight(0);
      return;
    }
    $.no_recommendations_container.setVisible(false);
    $.no_recommendations_container.setHeight(0);
    $.container.setVisible(true);
    var recConfig = eaUtils.getConfigValue('product.recommendations', {});
    var item,
    itemIdx = 0,
    page = 0,
    pageViews = [],
    itemCount = recommendedItems.length,
    rows = recConfig.gridRows || 2,
    columns = recConfig.gridColumns || 3,
    tileWidth = recConfig.tileImageWidth || 176,
    tileHeight = recConfig.tileImageHeight || 221,
    tilePadding = recConfig.tilePadding || 20,
    pages = Math.ceil(itemCount / (rows * columns));

    var fieldmap = {
      id: 'recommended_item_id',
      name: 'name' };


    var tileProperties = {
      imageWidth: tileWidth,
      title: {
        font: Alloy.Styles.smallerAccentFont,
        minimumFontSize: '12pt',
        ellipsize: true,
        wordWrap: false },


      price: {
        font: Alloy.Styles.lineItemFont,
        minimumFontSize: '12pt',
        ellipsize: true,
        wordWrap: false } };



    _.times(pages, function () {


      var pageView = Ti.UI.createView($.createStyle({
        classes: ['page_view'],
        apiName: 'View' }));



      _.times(rows, function () {
        var rowView = Ti.UI.createView($.createStyle({
          classes: ['row_view'],
          apiName: 'View',
          top: recConfig.rowPadding,
          height: tileHeight }));



        var c = 0;
        while (columns > c++ && (item = recommendedItems.at(itemIdx++))) {

          var tileView = Alloy.createController('search/components/productTile', {
            hit: item,
            index: itemIdx,
            fieldmap: fieldmap,
            tileProperties: tileProperties,
            includeMagnifyingGlass: false }).
          getView();

          tileView.setHeight(tileHeight);
          tileView.setWidth(tileWidth);
          tileView.setLeft(c > 1 ? tilePadding : 0);
          tileView.setTop(0);

          rowView.add(tileView);
        }

        pageView.add(rowView);
      });


      pageViews.push(pageView);
    });

    var recommendedScroller = Ti.UI.createScrollableView($.createStyle({
      views: pageViews,
      classes: ['recommended_scroller'],
      apiName: 'ScrollableView',
      showPagingControl: false,
      height: Ti.UI.SIZE,
      overlayEnabled: false }));


    recommendedScroller.addEventListener('product:select', recommendedScrollerEventHandler);

    $.recommendations_scroller_container.add(recommendedScroller);

    if (pages > 1) {

      var pagingControls = new PagingControl(recommendedScroller);
      var pagingView = pagingControls.getView();
      $.paging_container.setWidth(tileWidth * columns + tilePadding * (columns - 1));
      pagingView.applyProperties({
        top: 20 });

      $.paging_container.add(pagingView);
    }
  }






  function deinit() {
    logger.info('deinit called');
    $.stopListening();
    if ($.recommendations_scroller_container.children && $.recommendations_scroller_container.children[0]) {
      $.recommendations_scroller_container.children[0].removeEventListener('product:select', recommendedScrollerEventHandler);
    }
    removeAllChildren($.recommendations_scroller_container);
    removeAllChildren($.paging_container);
    $.destroy();
  }










  function recommendedScrollerEventHandler(event) {
    logger.info('Recommendation selected: ', event.product_id);
    Alloy.Router.navigateToProduct({
      product_id: event.product_id });

  }









  _.extend($, exports);
}

module.exports = Controller;