var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'customer/components/editProfile';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.edit_customer_profile = Ti.UI.createView(
  { left: 20, id: "edit_customer_profile" });

  $.__views.edit_customer_profile && $.addTopLevelView($.__views.edit_customer_profile);
  $.__views.__alloyId142 = Alloy.createController('components/block_header', { customLabel: L('_Edit_Profile'), id: "__alloyId142", __parentSymbol: $.__views.edit_customer_profile });
  $.__views.__alloyId142.setParent($.__views.edit_customer_profile);
  $.__views.__alloyId143 = Ti.UI.createView(
  { layout: "vertical", id: "__alloyId143" });

  $.__views.edit_customer_profile.add($.__views.__alloyId143);
  $.__views.__alloyId144 = Ti.UI.createView(
  { layout: "horizontal", width: "100%", left: 0, top: 68, height: 55, id: "__alloyId144" });

  $.__views.__alloyId143.add($.__views.__alloyId144);
  $.__views.first_name_container = Ti.UI.createView(
  { layout: "vertical", width: "50%", id: "first_name_container" });

  $.__views.__alloyId144.add($.__views.first_name_container);
  $.__views.customer_profile_first_name = Ti.UI.createTextField(
  { padding: { left: 18 }, height: 50, width: 240, borderStyle: Ti.UI.INPUT_BORDERSTYLE_LINE, borderColor: Alloy.Styles.color.border.dark, borderWidth: 1, color: Alloy.Styles.color.text.dark, font: Alloy.Styles.textFieldFont, hintText: L('_First_Name'), autocorrect: false, id: "customer_profile_first_name", accessibilityLabel: "customer_profile_first_name" });

  $.__views.first_name_container.add($.__views.customer_profile_first_name);
  $.__views.first_name_error = Ti.UI.createLabel(
  { width: "100%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.red, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 26, top: 3, font: Alloy.Styles.errorMessageFont, id: "first_name_error", accessibilityValue: "first_name_error" });

  $.__views.first_name_container.add($.__views.first_name_error);
  $.__views.last_name_container = Ti.UI.createView(
  { layout: "vertical", width: "49%", id: "last_name_container" });

  $.__views.__alloyId144.add($.__views.last_name_container);
  $.__views.customer_profile_last_name = Ti.UI.createTextField(
  { padding: { left: 18 }, height: 50, width: 240, borderStyle: Ti.UI.INPUT_BORDERSTYLE_LINE, borderColor: Alloy.Styles.color.border.dark, borderWidth: 1, color: Alloy.Styles.color.text.dark, font: Alloy.Styles.textFieldFont, hintText: L('_Last_Name'), autocorrect: false, id: "customer_profile_last_name", accessibilityLabel: "customer_profile_last_name" });

  $.__views.last_name_container.add($.__views.customer_profile_last_name);
  $.__views.last_name_error = Ti.UI.createLabel(
  { width: "100%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.red, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 26, top: 3, font: Alloy.Styles.errorMessageFont, id: "last_name_error", accessibilityValue: "last_name_error" });

  $.__views.last_name_container.add($.__views.last_name_error);
  $.__views.save_button = Ti.UI.createButton(
  { top: 20, right: 28, width: 240, height: 42, backgroundImage: Alloy.Styles.primaryButtonImage, font: Alloy.Styles.buttonFont, color: Alloy.Styles.buttons.primary.color, id: "save_button", titleid: "_Save", accessibilityValue: "save_profile_button" });

  $.__views.__alloyId143.add($.__views.save_button);
  $.__views.cancel_button = Ti.UI.createButton(
  { top: 20, right: 28, width: 240, height: 42, backgroundImage: Alloy.Styles.secondaryButtonImage, font: Alloy.Styles.buttonFont, color: Alloy.Styles.buttons.secondary.color, id: "cancel_button", titleid: "_Cancel", accessibilityValue: "cancel_profile_button" });

  $.__views.__alloyId143.add($.__views.cancel_button);
  var __alloyId145 = function () {Alloy['Models']['customer'].__transform = _.isFunction(Alloy['Models']['customer'].transform) ? Alloy['Models']['customer'].transform() : Alloy['Models']['customer'].toJSON();$.customer_profile_first_name.value = Alloy['Models']['customer']['__transform']['first_name'];$.customer_profile_last_name.value = Alloy['Models']['customer']['__transform']['last_name'];};Alloy['Models']['customer'].on('fetch change destroy', __alloyId145);exports.destroy = function () {Alloy['Models']['customer'] && Alloy['Models']['customer'].off('fetch change destroy', __alloyId145);};




  _.extend($, $.__views);










  var currentCustomer = Alloy.Models.customer;
  var showError = require('EAUtils').showError;
  var clearError = require('EAUtils').clearError;
  var email_regex = new RegExp(Alloy.CFG.regexes.email, 'i');
  var logger = require('logging')('customer:components:editProfile', getFullControllerPath($.__controllerPath));




  $.listenTo(Alloy.eventDispatcher, 'hideAuxillaryViews', closeKeyboards);




  $.save_button.addEventListener('click', onSaveClick);

  $.cancel_button.addEventListener('click', onCancelClick);




  exports.init = init;
  exports.deinit = deinit;









  function init() {
    logger.info('INIT called');
    clearAllErrors();
    Alloy.Models.customer.trigger('fetch', {});
  }






  function deinit() {
    logger.info('DEINIT called');
    $.save_button.removeEventListener('click', onSaveClick);
    $.cancel_button.removeEventListener('click', onCancelClick);
    $.stopListening();
    $.destroy();
  }








  function clearAllErrors() {
    logger.info('clearAllErrors');
    clearError($.customer_profile_first_name, $.first_name_error, null, null, true);
    clearError($.customer_profile_last_name, $.last_name_error, null, null, true);
  }





  function closeKeyboards() {
    logger.info('closeKeyboards');
    $.customer_profile_first_name.blur();
    $.customer_profile_last_name.blur();
  }








  function onSaveClick() {
    logger.info('save_button click event handler');
    closeKeyboards();
    clearAllErrors();
    var isValid = true;

    if (!$.customer_profile_first_name.value) {
      showError($.customer_profile_first_name, $.first_name_error, _L('Please provide a first name.'), true);
      isValid = false;
    }
    if (!$.customer_profile_last_name.value) {
      showError($.customer_profile_last_name, $.last_name_error, _L('Please provide a last name.'), true);
      isValid = false;
    }

    if (isValid) {
      var params = {
        first_name: $.customer_profile_first_name.value,
        last_name: $.customer_profile_last_name.value };


      var promise = currentCustomer.setProfile(params);
      Alloy.Router.showActivityIndicator(promise);
      promise.done(function (model, params, options) {
        notify(_L('Customer profile data successfully saved.'));
        $.edit_customer_profile.fireEvent('route', {
          page: 'profile',
          isCancel: true });

      });
    }

    if (!isValid) {
      notify(_L('Please fill in all the required fields.'));
    }
  }





  function onCancelClick() {
    logger.info('cancel_button click event handler');
    clearAllErrors();
    closeKeyboards();
    $.edit_customer_profile.fireEvent('route', {
      page: 'profile',
      isCancel: true });

  }









  _.extend($, exports);
}

module.exports = Controller;