var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
		delete obj[key];
	}
	return arg;
}

function Controller() {

	require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'checkout/cart/wishListItemRow';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};







	$.__views.product_list_item_row_table = Ti.UI.createTableViewRow(
	{ layout: "vertical", selectionStyle: Ti.UI.iOS.TableViewCellSelectionStyle.NONE, contains_product_image: true, product_id: $model.__transform.row_id, id: "product_list_item_row_table" });

	$.__views.product_list_item_row_table && $.addTopLevelView($.__views.product_list_item_row_table);
	$.__views.product_list_item_container = Ti.UI.createView(
	{ top: 20, height: 195, width: 650, layout: "horizontal", left: 32, backgroundImage: Alloy.Styles.customerBorderImage, id: "product_list_item_container", contains_product_image: true });

	$.__views.product_list_item_row_table.add($.__views.product_list_item_container);
	$.__views.product_list_item = Alloy.createController('checkout/cart/productItem', { id: "product_list_item", __parentSymbol: $.__views.product_list_item_container });
	$.__views.product_list_item.setParent($.__views.product_list_item_container);
	$.__views.button_container = Ti.UI.createView(
	{ layout: "vertical", left: 20, top: 20, id: "button_container" });

	$.__views.product_list_item_container.add($.__views.button_container);
	$.__views.edit_delete_button_container = Ti.UI.createView(
	{ top: 10, left: 0, height: Ti.UI.SIZE, width: "100%", id: "edit_delete_button_container" });

	$.__views.button_container.add($.__views.edit_delete_button_container);
	$.__views.edit_button = Ti.UI.createButton(
	{ height: 55, width: 55, backgroundImage: Alloy.Styles.editButtonIcon, left: 10, id: "edit_button", accessibilityValue: "edit_button" });

	$.__views.edit_delete_button_container.add($.__views.edit_button);
	$.__views.delete_button = Ti.UI.createButton(
	{ height: 55, width: 55, backgroundImage: Alloy.Styles.deleteButtonIcon, right: 10, id: "delete_button", accessibilityValue: "delete_button" });

	$.__views.edit_delete_button_container.add($.__views.delete_button);
	$.__views.add_to_cart_button = Ti.UI.createButton(
	{ backgroundImage: Alloy.Styles.secondaryButtonImage, width: 140, height: 28, color: Alloy.Styles.buttons.primary.color, font: Alloy.Styles.switchLabelFont, top: 9, titleid: "_Add_to_Cart", id: "add_to_cart_button", accessibilityValue: "add_button" });

	$.__views.button_container.add($.__views.add_to_cart_button);
	exports.destroy = function () {};




	_.extend($, $.__views);











	var buttonTextLength = 18;
	var logger = require('logging')('checkout:cart:wishListItemRow', getFullControllerPath($.__controllerPath));









	$.product_list_item_row_table.deinit = function () {
		logger.info('DEINIT called');
		$.product_list_item.deinit();
		$.destroy();
	};









	$.product_list_item_row_table.update = function (product, pli, showDivider) {
		logger.info('update called');
		$.product_list_item.init(product, pli);
		if (!showDivider) {
			$.product_list_item_container.setBackgroundImage(null);
		}
	};




	if ($.add_to_cart_button.getTitle().length > buttonTextLength) {
		$.add_to_cart_button.setFont(Alloy.Styles.detailInfoBoldFont);
	}









	_.extend($, exports);
}

module.exports = Controller;