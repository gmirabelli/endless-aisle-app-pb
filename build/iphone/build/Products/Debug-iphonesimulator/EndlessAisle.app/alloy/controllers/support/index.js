var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'support/index';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.support_window = Ti.UI.createView(
  { zIndex: 101, backgroundColor: Alloy.Styles.color.background.transparent, id: "support_window" });

  $.__views.support_window && $.addTopLevelView($.__views.support_window);
  $.__views.backdrop = Ti.UI.createView(
  { top: 0, left: 0, width: Ti.UI.FILL, height: Ti.UI.FILL, opacity: 0.25, backgroundColor: Alloy.Styles.color.background.black, id: "backdrop" });

  $.__views.support_window.add($.__views.backdrop);
  $.__views.dashboard = Ti.UI.createView(
  { layout: "vertical", backgroundColor: Alloy.Styles.color.background.white, id: "dashboard" });

  $.__views.support_window.add($.__views.dashboard);
  $.__views.titlebar = Ti.UI.createView(
  { height: 55, layout: "horizontal", backgroundColor: Alloy.Styles.accentColor, visible: false, id: "titlebar" });

  $.__views.dashboard.add($.__views.titlebar);
  $.__views.title = Ti.UI.createLabel(
  { width: "95%", height: 55, color: Alloy.Styles.color.text.white, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, top: 0, backgroundColor: Alloy.Styles.accentColor, font: Alloy.Styles.detailLabelFont, id: "title", textid: "_Admin_Dashboard", accessibilityValue: "dashboard_title" });

  $.__views.titlebar.add($.__views.title);
  $.__views.close_icon = Ti.UI.createImageView(
  { image: Alloy.Styles.clearTextImage, right: 0, top: 12, id: "close_icon", accessibilityLabel: "dashboard_close_icon" });

  $.__views.titlebar.add($.__views.close_icon);
  handleClose ? $.addListener($.__views.close_icon, 'click', handleClose) : __defers['$.__views.close_icon!click!handleClose'] = true;$.__views.dashboard_container = Ti.UI.createView(
  { layout: "horizontal", top: 0, height: Ti.UI.FILL, visible: false, id: "dashboard_container" });

  $.__views.dashboard.add($.__views.dashboard_container);
  $.__views.header = Alloy.createController('support/components/header', { height: 40, backgroundColor: Alloy.Styles.color.background.medium, layout: "horizontal", id: "header", __parentSymbol: $.__views.dashboard_container });
  $.__views.header.setParent($.__views.dashboard_container);
  $.__views.header_tabgroup = Ti.UI.createView(
  { top: 0, left: 0, width: Ti.UI.FILL, height: Ti.UI.FILL, layout: "absolute", id: "header_tabgroup" });

  $.__views.dashboard_container.add($.__views.header_tabgroup);
  $.__views.configuration_tab = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.tabs.backgroundColorEnabled, font: Alloy.Styles.tabFont, width: Ti.UI.FILL, top: 0, left: 0, height: Ti.UI.FILL, visible: true, enabled: true, id: "configuration_tab" });

  $.__views.header_tabgroup.add($.__views.configuration_tab);
  $.__views.info_window = Alloy.createController('support/buildInfo', { top: 0, left: 0, width: 200, height: 200, id: "info_window", __parentSymbol: $.__views.configuration_tab });
  $.__views.info_window.setParent($.__views.configuration_tab);
  $.__views.app_config_tab = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.tabs.backgroundColorEnabled, font: Alloy.Styles.tabFont, width: Ti.UI.FILL, top: 0, left: 0, height: Ti.UI.FILL, visible: false, enabled: false, id: "app_config_tab" });

  $.__views.header_tabgroup.add($.__views.app_config_tab);
  $.__views.app_config_window = Alloy.createController('support/appConfig', { id: "app_config_window", __parentSymbol: $.__views.app_config_tab });
  $.__views.app_config_window.setParent($.__views.app_config_tab);
  $.__views.payment_terminal_tab = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.tabs.backgroundColorEnabled, font: Alloy.Styles.tabFont, width: Ti.UI.FILL, top: 0, left: 0, height: Ti.UI.FILL, visible: false, enabled: false, id: "payment_terminal_tab" });

  $.__views.header_tabgroup.add($.__views.payment_terminal_tab);
  $.__views.payment_terminal_window = Alloy.createController('support/devices', { id: "payment_terminal_window", device: "payment_terminal", __parentSymbol: $.__views.payment_terminal_tab });
  $.__views.payment_terminal_window.setParent($.__views.payment_terminal_tab);
  $.__views.receipt_printer_tab = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.tabs.backgroundColorEnabled, font: Alloy.Styles.tabFont, width: Ti.UI.FILL, top: 0, left: 0, height: Ti.UI.FILL, visible: false, enabled: false, id: "receipt_printer_tab" });

  $.__views.header_tabgroup.add($.__views.receipt_printer_tab);
  if (Alloy.CFG.printer_availability) {
    $.__views.receipt_printer_window = Alloy.createController('support/devices', { id: "receipt_printer_window", device: "printer", __parentSymbol: $.__views.receipt_printer_tab });
    $.__views.receipt_printer_window.setParent($.__views.receipt_printer_tab);
  }
  $.__views.test_tab = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.tabs.backgroundColorEnabled, font: Alloy.Styles.tabFont, width: Ti.UI.FILL, top: 0, left: 0, height: Ti.UI.FILL, visible: false, enabled: false, id: "test_tab" });

  $.__views.header_tabgroup.add($.__views.test_tab);
  $.__views.testing_window = Alloy.createController('support/testing', { id: "testing_window", __parentSymbol: $.__views.test_tab });
  $.__views.testing_window.setParent($.__views.test_tab);
  $.__views.logs_tab = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.tabs.backgroundColorEnabled, font: Alloy.Styles.tabFont, width: Ti.UI.FILL, top: 0, left: 0, height: Ti.UI.FILL, visible: false, enabled: false, id: "logs_tab" });

  $.__views.header_tabgroup.add($.__views.logs_tab);
  $.__views.logging_window = Alloy.createController('support/logging', { id: "logging_window", __parentSymbol: $.__views.logs_tab });
  $.__views.logging_window.setParent($.__views.logs_tab);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var currentPage = -1;
  var showActivityIndicator = require('dialogUtils').showActivityIndicator;
  var logger = require('logging')('support:index', getFullControllerPath($.__controllerPath));




  $.listenTo($.header, 'changeDashboardPage', handlePageSelection);
  $.backdrop.addEventListener('click', handleClose);
  $.support_window.addEventListener('postlayout', handlePostLayout);




  exports.init = init;
  exports.deinit = deinit;









  function init() {
    logger.info('init called');
    showAdminDashboard();
    $.app_config_window.init();
  }






  function deinit() {
    logger.info('deinit called');
    $.backdrop.removeEventListener('click', handleClose);
    $.support_window.removeEventListener('postlayout', handlePostLayout);

    $.close_icon.removeEventListener('click', handleClose);

    $.header.deinit();
    $.info_window.deinit();
    $.app_config_window.deinit();
    $.payment_terminal_window.deinit();
    $.receipt_printer_window && $.receipt_printer_window.deinit();
    $.testing_window.deinit();
    $.logging_window.deinit();
    $.stopListening();
    $.destroy();
  }









  function showAdminDashboard() {
    $.dashboard.setWidth('95%');
    $.dashboard.setHeight('93%');
    $.dashboard.setTop('5%');
    $.dashboard.setBottom('5%');
    $.dashboard.setRight('3%');
    $.dashboard.setLeft(20);
    $.dashboard.setOpacity('1.0');
    $.dashboard_container.setHeight(Ti.UI.FILL);
    $.dashboard_container.setWidth(Ti.UI.FILL);
    handlePageSelection({
      page: 'configurations' });

  }







  function continuePageSelection(event) {
    if (currentPage >= 0) {
      $.header_tabgroup.children[currentPage].hide();
    }

    switch (event.page) {
      case 'configurations':
        currentPage = 0;
        break;
      case 'app_config':
        currentPage = 1;
        $.app_config_window.render();
        break;
      case 'payment_terminal':
        currentPage = 2;
        break;
      case 'receipt_printer':
        currentPage = 3;
        break;
      case 'test':
        currentPage = 4;
        break;
      case 'logs':
        currentPage = 5;
        break;}

    $.header_tabgroup.children[currentPage].show();
    $.header.selectTab(event.page);
    Alloy.eventDispatcher.trigger('session:renew');
  }






  function continueClose() {
    Alloy.eventDispatcher.trigger('session:renew');
    $.trigger('dashboard:dismiss', {});
  }









  function handlePostLayout() {
    $.titlebar.setVisible(true);
    $.dashboard_container.setVisible(true);
  }







  function handlePageSelection(event) {
    if (event.page == 'configurations' && currentPage == 0 || event.page == 'app_config' && currentPage == 1 || event.page == 'payment_terminal' && currentPage == 2 || event.page == 'receipt_printer' && currentPage == 3 || event.page == 'test' && currentPage == 4 || event.page == 'logs' && currentPage == 5) {
      return;
    }

    if (currentPage == 1) {
      $.app_config_window.showWarning().done(function () {
        continuePageSelection(event);
      });
    } else {
      continuePageSelection(event);
    }
  }






  function handleClose() {
    if (currentPage == 1) {
      $.app_config_window.showWarning().done(function () {
        continueClose();
      });
    } else {
      continueClose();
    }
  }





  __defers['$.__views.close_icon!click!handleClose'] && $.addListener($.__views.close_icon, 'click', handleClose);



  _.extend($, exports);
}

module.exports = Controller;