var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'support/appConfig';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.app_config_main = Ti.UI.createView(
  { layout: "horizontal", width: "100%", height: "100%", id: "app_config_main" });

  $.__views.app_config_main && $.addTopLevelView($.__views.app_config_main);
  $.__views.data_column = Ti.UI.createScrollView(
  { layout: "vertical", width: "70%", height: "100%", backgroundColor: Alloy.Styles.color.background.light, id: "data_column" });

  $.__views.app_config_main.add($.__views.data_column);
  $.__views.app_settings_view = Alloy.createController('components/appSettingsView', { id: "app_settings_view", __parentSymbol: $.__views.data_column });
  $.__views.app_settings_view.setParent($.__views.data_column);
  $.__views.vertical_border = Ti.UI.createView(
  { width: 2, height: "100%", backgroundColor: Alloy.Styles.color.border.darkest, id: "vertical_border" });

  $.__views.app_config_main.add($.__views.vertical_border);
  $.__views.action_column = Ti.UI.createScrollView(
  { layout: "vertical", width: "28%", contentWidth: "28%", height: "100%", id: "action_column" });

  $.__views.app_config_main.add($.__views.action_column);
  $.__views.action_section = Ti.UI.createView(
  { top: 15, left: 20, height: Ti.UI.SIZE, layout: "vertical", textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, id: "action_section" });

  $.__views.action_column.add($.__views.action_section);
  $.__views.action_title = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.accentColor, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailLabelFont, left: 0, bottom: 10, textid: "_Actions_", id: "action_title", accessibilityValue: "action_title" });

  $.__views.action_section.add($.__views.action_title);
  $.__views.apply_text = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailValueFont, left: 0, textid: "_ApplyConfigText", id: "apply_text", accessibilityValue: "apply_text" });

  $.__views.action_section.add($.__views.apply_text);
  $.__views.requires_restart = Ti.UI.createLabel(
  { width: Ti.UI.FILL, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.detailValueFont, left: 0, textid: "___Requires_Logout", id: "requires_restart", accessibilityValue: "requires_restart" });

  $.__views.action_section.add($.__views.requires_restart);
  $.__views.apply_changes = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.buttons.primary.color, disabledColor: Alloy.Styles.buttons.primary.disabledColor, backgroundImage: Alloy.Styles.buttons.primary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.primary.backgroundDisabledImage, height: 30, width: Ti.UI.FILL, font: Alloy.Styles.smallButtonFont, top: 10, bottom: 20, titleid: "_Apply", id: "apply_changes", accessibilityValue: "apply_changes", enabled: false });

  $.__views.action_section.add($.__views.apply_changes);
  $.__views.email_text = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailValueFont, left: 0, textid: "_EmailText", id: "email_text", accessibilityValue: "email_text" });

  $.__views.action_section.add($.__views.email_text);
  $.__views.email_address = Ti.UI.createTextArea(
  { top: 10, layout: "vertical", font: Alloy.Styles.detailValueFont, left: 20, height: Ti.UI.SIZE, horizontalWrap: true, id: "email_address", accessibilityValue: "email_address" });

  $.__views.action_section.add($.__views.email_address);
  $.__views.email_configs = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.buttons.primary.color, disabledColor: Alloy.Styles.buttons.primary.disabledColor, backgroundImage: Alloy.Styles.buttons.primary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.primary.backgroundDisabledImage, height: 30, width: Ti.UI.FILL, font: Alloy.Styles.smallButtonFont, top: 10, bottom: 20, titleid: "_Email_Configs", id: "email_configs", accessibilityValue: "email_configs" });

  $.__views.action_section.add($.__views.email_configs);
  $.__views.clear_changes_text = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailValueFont, left: 0, textid: "_ClearChangesText", id: "clear_changes_text", accessibilityValue: "clear_changes_text", visible: false });

  $.__views.action_section.add($.__views.clear_changes_text);
  $.__views.clear_changes = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.buttons.primary.color, disabledColor: Alloy.Styles.buttons.primary.disabledColor, backgroundImage: Alloy.Styles.buttons.primary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.primary.backgroundDisabledImage, height: 30, width: Ti.UI.FILL, font: Alloy.Styles.smallButtonFont, top: 10, bottom: 20, titleid: "_Clear_App_Settings", id: "clear_changes", accessibilityValue: "clear_changes", visible: false });

  $.__views.action_section.add($.__views.clear_changes);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var appSettings = require('appSettings');
  var eaUtils = require('EAUtils');
  var logger = require('logging')('support:appConfig', getFullControllerPath($.__controllerPath));
  var changesMade = [];
  var warningDisplayed = false;




  exports.init = init;
  exports.render = render;
  exports.deinit = deinit;
  exports.hideActionColumn = hideActionColumn;
  exports.showActionColumn = showActionColumn;
  exports.showWarning = showWarning;









  function init() {
    logger.info('INIT called');
    $.apply_changes.addEventListener('click', applyChanges);
    $.email_configs.addEventListener('click', emailConfigs);
    $.clear_changes.addEventListener('click', clearAppSettings);
    $.app_settings_view.init();
  }






  function render() {
    logger.info('render called');
    $.listenTo($.app_settings_view, 'setting:change', handleChange);
    var deferred = new _.Deferred();
    Alloy.Router.showActivityIndicator(deferred);
    changesMade = [];

    $.app_settings_view.render().always(function () {
      deferred.resolve();
    });
  }






  function deinit() {
    logger.info('deinit called');
    changesMade = [];

    $.apply_changes.removeEventListener('click', applyChanges);
    $.email_configs.removeEventListener('click', emailConfigs);
    $.clear_changes.removeEventListener('click', clearAppSettings);
    $.stopListening($.app_settings_view, 'setting:change', handleChange);
    $.app_settings_view.deinit();
    $.destroy();
  }









  function hideActionColumn() {
    $.action_column.hide();
  }






  function showActionColumn() {
    $.action_column.setHeight('100%');
    $.action_column.setWidth('28%');
    $.action_column.setLayout('vertical');
    $.action_column.show();
  }







  function showWarning() {

    if (warningDisplayed) {
      var deferred = new _.Deferred();
      deferred.resolve();
      return deferred.promise();
    }
    var closeDeferred = new _.Deferred();
    warningDisplayed = true;
    if (changesMade.length == 0) {
      closeDeferred.resolve();
      warningDisplayed = false;
    } else {
      Alloy.Dialog.showConfirmationDialog({
        messageString: _L('You have modified settings. Are you sure you want to discard the changes?'),
        titleString: _L('Discard Changes'),
        okFunction: function () {
          changesMade = [];
          $.apply_changes.setEnabled(false);
          closeDeferred.resolve();
          warningDisplayed = false;
        },
        cancelFunction: function () {
          closeDeferred.reject();
          warningDisplayed = false;
        } });

    }
    return closeDeferred.promise();
  }









  function handleChange(event) {
    var oldValue = eaUtils.getConfigValue(event.configName);
    logger.info('Change for configName: ' + event.configName + ' before value: ' + oldValue + ' new value: ' + event.value + ' restart: ' + event.restart);

    var found = _.find(changesMade, function (change) {
      return change.configName == event.configName;
    });
    if (found) {
      var index = changesMade.indexOf(found);
      changesMade.splice(index, 1);
    }
    if (oldValue != event.value) {
      changesMade.push({
        configName: event.configName,
        newValue: event.value,
        restart: event.restart });

    }
    $.apply_changes.setEnabled(changesMade.length != 0);
  }






  function emailConfigs() {
    var messageBody = 'Configuration Email:';

    messageBody += '\n\nApplication Configurations:\n\n' + JSON.stringify(Alloy.CFG, null, 4);
    messageBody += '\n\nApplication Information: ';
    ['deployType', 'guid', 'id', 'installId', 'keyboardVisible', 'sessionId', 'version'].forEach(function (key) {
      messageBody += '\n    ' + key + ': ' + Ti.App[key];
    });
    messageBody += '\n\nPlatform Information: ';
    ['architecture', 'availableMemory', 'id', 'locale', 'macaddress', 'ip', 'manufacturer', 'model', 'name', 'netmask', 'osname', 'ostype', 'processorCount', 'runtime', 'username', 'version'].forEach(function (key) {
      messageBody += '\n    ' + key + ': ' + Ti.Platform[key];
    });

    messageBody += '\n\nDisplay Information: ';
    ['density', 'dpi', 'logicalDensityFactor', 'platformHeight', 'platformWidth', 'xdpi', 'ydpi'].forEach(function (key) {
      messageBody += '\n    ' + key + ': ' + Ti.Platform.displayCaps[key];
    });

    eaUtils.emailLogs(messageBody);

    notify(_L('Email of configuration has been sent.'));
  }






  function applyChanges() {
    var restartMessage = false;
    _.each(changesMade, function (change) {
      appSettings.setSetting(change.configName, change.newValue);
      if (change.restart) {
        restartMessage = true;
      }
    });
    notify(_L('Application settings applied.'));

    Alloy.eventDispatcher.trigger('session:renew');
    if (restartMessage) {
      Alloy.Dialog.showConfirmationDialog({
        titleString: _L('Logout Required'),
        messageString: _L('Logout Required Text'),
        okButtonString: _L('Logout Now'),
        hideCancel: true,
        okFunction: function () {
          Alloy.Router.associateLogout();
        } });

    }
    changesMade = [];
    $.apply_changes.setEnabled(false);
  }






  function clearAppSettings() {
    appSettings.clearDB();
    notify(_L('Application settings cleared.'));
    Alloy.Dialog.showConfirmationDialog({
      messageString: _L('A restart is required.'),
      hideCancel: true });

  }




  if (Alloy.CFG.admin_email != null) {
    $.email_address.setValue(Alloy.CFG.admin_email);
    $.email_address.setHeight(Ti.UI.SIZE);
  } else {
    $.email_text.setHeight(0);
    $.email_text.setVisible(false);
    $.email_address.setHeight(0);
    $.email_address.setVisible(false);
    $.email_configs.setHeight(0);
    $.email_configs.setVisible(false);
  }

  if (Ti.App.deployType !== 'production') {
    $.clear_changes_text.setVisible(true);
    $.clear_changes.setVisible(true);
  }









  _.extend($, exports);
}

module.exports = Controller;