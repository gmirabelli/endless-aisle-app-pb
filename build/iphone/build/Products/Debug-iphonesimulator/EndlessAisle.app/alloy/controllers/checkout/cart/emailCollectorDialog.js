var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'checkout/cart/emailCollectorDialog';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.container_window = Ti.UI.createView(
  { layout: "absolute", backgroundColor: Alloy.Styles.color.background.transparent, id: "container_window" });

  $.__views.container_window && $.addTopLevelView($.__views.container_window);
  $.__views.backdrop = Ti.UI.createView(
  { top: 0, left: 0, width: 1024, height: 768, opacity: 0.55, backgroundColor: Alloy.Styles.color.background.black, id: "backdrop" });

  $.__views.container_window.add($.__views.backdrop);
  $.__views.main_container = Ti.UI.createView(
  { layout: "vertical", top: 50, width: "50%", height: Ti.UI.SIZE, backgroundColor: Alloy.Styles.color.background.white, id: "main_container" });

  $.__views.container_window.add($.__views.main_container);
  $.__views.title = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.accentColor, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.calloutFont, top: 20, textid: "_Email_Wish_List", id: "title" });

  $.__views.main_container.add($.__views.title);
  $.__views.email_address = Ti.UI.createTextField(
  { maxLength: 50, backgroundColor: Alloy.Styles.color.background.medium, disabledBackgroundColor: Alloy.Styles.color.background.light, height: 50, top: 30, width: "90%", color: Alloy.Styles.color.text.darkest, disabledColor: Alloy.Styles.color.text.light, font: Alloy.Styles.textFieldFont, padding: { left: 18 }, autocorrect: false, autocapitalization: Ti.UI.TEXT_AUTOCAPITALIZATION_NONE, keyboardType: Ti.UI.KEYBOARD_TYPE_EMAIL, id: "email_address", accessibilityLabel: "email_address" });

  $.__views.main_container.add($.__views.email_address);
  validateEmailAddress ? $.addListener($.__views.email_address, 'return', validateEmailAddress) : __defers['$.__views.email_address!return!validateEmailAddress'] = true;$.__views.error_container = Ti.UI.createView(
  { width: "90%", height: 20, top: 10, id: "error_container" });

  $.__views.main_container.add($.__views.error_container);
  $.__views.email_error = Ti.UI.createLabel(
  { width: "100%", height: 18, color: Alloy.Styles.color.text.red, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, font: Alloy.Styles.errorMessageFont, id: "email_error", accessibilityValue: "email_error" });

  $.__views.error_container.add($.__views.email_error);
  $.__views.button_container = Ti.UI.createView(
  { layout: "horizontal", height: Ti.UI.SIZE, width: Ti.UI.SIZE, top: 40, bottom: 20, id: "button_container" });

  $.__views.main_container.add($.__views.button_container);
  $.__views.cancel_button = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, backgroundImage: Alloy.Styles.buttons.secondary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.secondary.backgroundDisabledImage, color: Alloy.Styles.buttons.secondary.color, disabledColor: Alloy.Styles.buttons.secondary.disabledColor, height: 40, width: 200, titleid: "_Cancel", id: "cancel_button", accessibilityValue: "confirmation_cancel_button" });

  $.__views.button_container.add($.__views.cancel_button);
  dismiss ? $.addListener($.__views.cancel_button, 'click', dismiss) : __defers['$.__views.cancel_button!click!dismiss'] = true;$.__views.send_button = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.buttons.primary.color, disabledColor: Alloy.Styles.buttons.primary.disabledColor, backgroundImage: Alloy.Styles.buttons.primary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.primary.backgroundDisabledImage, height: 40, width: 200, titleid: "_Send", left: 20, id: "send_button", accessibilityValue: "confirmation_ok_button" });

  $.__views.button_container.add($.__views.send_button);
  validateEmailAddress ? $.addListener($.__views.send_button, 'click', validateEmailAddress) : __defers['$.__views.send_button!click!validateEmailAddress'] = true;exports.destroy = function () {};




  _.extend($, $.__views);










  var args = arguments[0] || {};
  var showError = require('EAUtils').showError;
  var clearError = require('EAUtils').clearError;
  var email_regex = new RegExp(Alloy.CFG.regexes.email, 'i');
  var logger = require('logging')('checkout:cart:emailCollectorDialog', getFullControllerPath($.__controllerPath));




  exports.init = init;
  exports.deinit = deinit;
  exports.focus = focus;









  function init() {
    $.email_address.setHintText(_L('Enter email here'));
    logger.info('INIT called');
  }






  function deinit() {
    logger.info('DEINIT called');
    $.send_button.removeEventListener('click', validateEmailAddress);
    $.email_address.removeEventListener('return', validateEmailAddress);
    $.cancel_button.removeEventListener('click', dismiss);
    $.stopListening();
    $.destroy();
  }









  function focus() {
    $.email_address.focus();
  }






  function dismiss(event) {
    logger.info('dimiss called');
    $.email_address.blur();
    $.trigger('email_collector_dialog:dismiss');
  }






  function validateEmailAddress() {
    if ($.email_address.getValue()) {
      if (email_regex.test($.email_address.getValue())) {
        clearError($.email_address, $.email_error, Alloy.Styles.color.text.lightest, Alloy.Styles.color.text.lightest, false);
        $.email_address.blur();
        args.emailData.receiverEmail = $.email_address.getValue().trim();
        var promise = Alloy.Models.customer.productLists.mail.send(args.emailData);
        Alloy.Router.showActivityIndicator(promise);
        promise.done(function () {
          $.trigger('email_collector_dialog:continue');
          notify(args.successNotifyMessage);
        }).fail(function () {
          notify(args.failNotifyMessage);
        });
        return;
      } else {
        showError($.email_address, $.email_error, _L('Invalid email address'), false);
        return;
      }
    }
    showError($.email_address, $.email_error, _L('Enter email address'), false);
    return;
  }





  __defers['$.__views.email_address!return!validateEmailAddress'] && $.addListener($.__views.email_address, 'return', validateEmailAddress);__defers['$.__views.cancel_button!click!dismiss'] && $.addListener($.__views.cancel_button, 'click', dismiss);__defers['$.__views.send_button!click!validateEmailAddress'] && $.addListener($.__views.send_button, 'click', validateEmailAddress);



  _.extend($, exports);
}

module.exports = Controller;