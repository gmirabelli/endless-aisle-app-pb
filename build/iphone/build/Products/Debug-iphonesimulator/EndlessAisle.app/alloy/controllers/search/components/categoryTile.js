var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'search/components/categoryTile';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.categoryTile = Ti.UI.createView(
  { layout: "absolute", backgroundColor: Alloy.Styles.color.background.light, borderColor: Alloy.Styles.color.border.lightest, borderWidth: 1, width: 282, height: 282, id: "categoryTile" });

  $.__views.categoryTile && $.addTopLevelView($.__views.categoryTile);
  onCategoryClick ? $.addListener($.__views.categoryTile, 'click', onCategoryClick) : __defers['$.__views.categoryTile!click!onCategoryClick'] = true;$.__views.tile_image = Ti.UI.createImageView(
  { top: 5, left: 5, right: 5, bottom: 5, bubbleParent: false, id: "tile_image", accessibilityValue: "tile_image" });

  $.__views.categoryTile.add($.__views.tile_image);
  onCategoryClick ? $.addListener($.__views.tile_image, 'click', onCategoryClick) : __defers['$.__views.tile_image!click!onCategoryClick'] = true;$.__views.name_container = Ti.UI.createView(
  { bottom: 15, left: 0, color: Alloy.Styles.color.text.black, backgroundColor: Alloy.Styles.color.background.light, opacity: 0.9, width: 160, height: 40, borderColor: Alloy.Styles.color.border.light, borderWidth: 1, id: "name_container" });

  $.__views.categoryTile.add($.__views.name_container);
  $.__views.tile_name = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 20, font: Alloy.Styles.productFont, id: "tile_name" });

  $.__views.name_container.add($.__views.tile_name);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var args = arguments[0] || {};
  var logger = require('logging')('search:components:categoryTile', getFullControllerPath($.__controllerPath));
  var EAUtils = require('EAUtils');
  var category = args.category;
  var index = args.index;




  exports.deinit = deinit;








  function init() {
    var name = category.getName();
    var image = category.getCategoryTileImage();

    $.tile_name.setText(name);

    var cat_tile_label_base = 'category_tile_' + category.getId();

    $.categoryTile.setAccessibilityLabel(cat_tile_label_base);
    $.tile_image.setAccessibilityLabel(cat_tile_label_base + '_image');
    $.name_container.setAccessibilityLabel(cat_tile_label_base + '_name_container');
    $.tile_name.setAccessibilityLabel(cat_tile_label_base + '_name');

    Alloy.Globals.getImageViewImage($.tile_image, image);
    if (EAUtils.isLatinBasedLanguage()) {
      $.name_container.setWidth(190);
    }
  }





  function deinit() {
    $.categoryTile.removeEventListener('click', onCategoryClick);

    $.tile_image.removeEventListener('click', onCategoryClick);
    $.stopListening();
    $.destroy();
  }









  function onCategoryClick(event) {

    logger.info('singletapped on tile!' + JSON.stringify(event));

    var matrix = Ti.UI.create2DMatrix();
    matrix = matrix.scale(0.9, 0.9);

    $.categoryTile.animate({
      transform: matrix,
      duration: 100,
      autoreverse: true,
      repeat: 1 },
    function () {
      Alloy.Router.navigateToCategory({
        category_id: category.id });

    });
  }




  init();





  __defers['$.__views.categoryTile!click!onCategoryClick'] && $.addListener($.__views.categoryTile, 'click', onCategoryClick);__defers['$.__views.tile_image!click!onCategoryClick'] && $.addListener($.__views.tile_image, 'click', onCategoryClick);



  _.extend($, exports);
}

module.exports = Controller;