var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'components/ConfirmationDialog';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.confirmation_window = Ti.UI.createView(
  { layout: "absolute", backgroundColor: Alloy.Styles.color.background.transparent, id: "confirmation_window" });

  $.__views.confirmation_window && $.addTopLevelView($.__views.confirmation_window);
  $.__views.backdrop = Ti.UI.createView(
  { top: 0, left: 0, width: 1024, height: 768, opacity: 0.55, backgroundColor: Alloy.Styles.color.background.black, id: "backdrop" });

  $.__views.confirmation_window.add($.__views.backdrop);
  $.__views.confirmation_container = Ti.UI.createView(
  { layout: "vertical", width: "50%", height: Ti.UI.SIZE, backgroundColor: Alloy.Styles.color.background.white, id: "confirmation_container" });

  $.__views.confirmation_window.add($.__views.confirmation_container);
  $.__views.title = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.accentColor, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.calloutFont, top: 20, id: "title", accessibilityValue: "confirmation_title" });

  $.__views.confirmation_container.add($.__views.title);
  $.__views.message_section = Ti.UI.createView(
  { layout: "horizontal", top: 36, left: 50, right: 50, width: Ti.UI.FILL, height: Ti.UI.SIZE, id: "message_section" });

  $.__views.confirmation_container.add($.__views.message_section);
  $.__views.icon = Ti.UI.createImageView(
  { width: 0, top: 0, height: 0, visible: false, id: "icon", accessibilityLabel: "confirmation_icon" });

  $.__views.message_section.add($.__views.icon);
  $.__views.message = Ti.UI.createLabel(
  { width: Ti.UI.FILL, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.textFieldFont, id: "message", accessibilityValue: "confirmation_message" });

  $.__views.message_section.add($.__views.message);
  $.__views.button_container = Ti.UI.createView(
  { layout: "horizontal", height: Ti.UI.SIZE, width: Ti.UI.SIZE, top: 40, bottom: 20, id: "button_container" });

  $.__views.confirmation_container.add($.__views.button_container);
  $.__views.cancel_button = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, backgroundImage: Alloy.Styles.buttons.secondary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.secondary.backgroundDisabledImage, color: Alloy.Styles.buttons.secondary.color, disabledColor: Alloy.Styles.buttons.secondary.disabledColor, height: 40, width: 220, id: "cancel_button", accessibilityValue: "confirmation_cancel_button" });

  $.__views.button_container.add($.__views.cancel_button);
  $.__views.ok_button = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.buttons.primary.color, disabledColor: Alloy.Styles.buttons.primary.disabledColor, backgroundImage: Alloy.Styles.buttons.primary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.primary.backgroundDisabledImage, height: 40, width: 220, id: "ok_button", accessibilityValue: "confirmation_ok_button" });

  $.__views.button_container.add($.__views.ok_button);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var logger = require('logging')('components:ConfirmationDialog', getFullControllerPath($.__controllerPath));

  var args = arguments[0] || {};
  var okButtonText = args.okButtonString || _L('OK');
  var cancelButtonText = args.cancelButtonString || _L('Cancel');
  var messageText = args.messageString;
  var titleText = args.titleString || _L('Confirm');
  var hideCancel = args.hideCancel || false;
  var icon = args.icon;




  $.ok_button.addEventListener('click', onOKButton);
  $.cancel_button.addEventListener('click', dismiss);




  exports.init = init;
  exports.deinit = deinit;









  function init() {
    logger.info('INIT called');
    titleText && $.title.setText(titleText);
    messageText && $.message.setText(messageText);

    okButtonText && $.ok_button.setTitle(okButtonText);
    if (hideCancel) {
      $.cancel_button.setWidth(0);
      $.cancel_button.hide();
    } else {
      cancelButtonText && $.cancel_button.setTitle(cancelButtonText);
      $.ok_button.setLeft(20);
    }
    if (icon) {
      $.message.setLeft(10);
      $.icon.setWidth(64);
      $.icon.setHeight(64);
      $.icon.setImage(icon);
      $.icon.setVisible(true);
    }
  }






  function deinit() {
    logger.info('DEINIT called');
    $.ok_button.removeEventListener('click', onOKButton);
    $.cancel_button.removeEventListener('click', dismiss);
    $.destroy();
  }









  function onOKButton() {
    $.trigger('confirmation_dialog:continue');
  }






  function dismiss() {
    $.trigger('confirmation_dialog:dismiss');
  }









  _.extend($, exports);
}

module.exports = Controller;