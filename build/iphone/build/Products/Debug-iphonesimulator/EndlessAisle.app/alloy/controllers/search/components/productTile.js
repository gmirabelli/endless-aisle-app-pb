var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'search/components/productTile';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.productTile = Ti.UI.createView(
  { layout: "absolute", width: 217, height: 271, backgroundColor: Alloy.Styles.color.background.light, borderColor: Alloy.Styles.color.border.lightest, top: 31, left: 31, borderWidth: 1, id: "productTile" });

  $.__views.productTile && $.addTopLevelView($.__views.productTile);
  handleProductTileClick ? $.addListener($.__views.productTile, 'click', handleProductTileClick) : __defers['$.__views.productTile!click!handleProductTileClick'] = true;$.__views.tile_image = Ti.UI.createImageView(
  { width: 209, top: 3, left: 3, borderColor: Alloy.Styles.color.border.lightest, backgroundColor: Alloy.Styles.color.background.white, borderWidth: 1, id: "tile_image" });

  $.__views.productTile.add($.__views.tile_image);
  handleProductTileClick ? $.addListener($.__views.tile_image, 'click', handleProductTileClick) : __defers['$.__views.tile_image!click!handleProductTileClick'] = true;$.__views.tile_caption = Ti.UI.createView(
  { bottom: 0, backgroundColor: Alloy.Styles.color.background.light, width: "100%", height: 55, layout: "vertical", id: "tile_caption" });

  $.__views.productTile.add($.__views.tile_caption);
  $.__views.tile_name = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: "50%", color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 10, font: Alloy.Styles.productFont, top: 3, id: "tile_name" });

  $.__views.tile_caption.add($.__views.tile_name);
  $.__views.__alloyId242 = Ti.UI.createView(
  { layout: "absolute", id: "__alloyId242" });

  $.__views.tile_caption.add($.__views.__alloyId242);
  $.__views.price_container = Ti.UI.createView(
  { layout: "horizontal", width: Ti.UI.SIZE, top: 0, left: 10, id: "price_container" });

  $.__views.__alloyId242.add($.__views.price_container);
  $.__views.list_price_container = Ti.UI.createView(
  { left: 0, height: "50%", width: Ti.UI.SIZE, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, id: "list_price_container" });

  $.__views.price_container.add($.__views.list_price_container);
  $.__views.list_price = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: "50%", color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailTextFont, id: "list_price", accessibilityValue: "list_price" });

  $.__views.list_price_container.add($.__views.list_price);
  $.__views.sale_price_container = Ti.UI.createView(
  { left: 0, height: "50%", width: Ti.UI.SIZE, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, id: "sale_price_container" });

  $.__views.price_container.add($.__views.sale_price_container);
  $.__views.sale_price = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: "50%", color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailTextFont, id: "sale_price", accessibilityValue: "sale_price" });

  $.__views.sale_price_container.add($.__views.sale_price);
  $.__views.swatch_container = Ti.UI.createView(
  { layout: "absolute", width: 150, top: 0, right: 10, id: "swatch_container" });

  $.__views.__alloyId242.add($.__views.swatch_container);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var eaUtils = require('EAUtils');
  var animation = require('alloy/animation');
  var logger = require('logging')('search:components:productTile', getFullControllerPath($.__controllerPath));

  var args = arguments[0] || {},
  loading = false,
  hit = args.hit || {},
  index = args.index || 0,
  fieldmap = args.fieldmap || {},
  productId = hit.get(fieldmap.id || 'product_id'),
  name = hit.get(fieldmap.name || 'product_name'),
  price = hit.get(fieldmap.price || 'price'),
  prices = hit.get(fieldmap.prices || 'prices'),
  price_max = hit.get(fieldmap.priceMax || 'price_max'),
  tileProperties = args.tileProperties || {},
  priceText = '',
  maxPriceText = '',
  onBadgeContainerClick,
  badgeContainer,
  includeMagnifyingGlass = args.hasOwnProperty('includeMagnifyingGlass') ? args.includeMagnifyingGlass : true;




  exports.init = init;
  exports.deinit = deinit;









  function init() {
    logger.info('INIT called');
    if (tileProperties.imageWidth) {
      $.tile_image.setWidth(tileProperties.imageWidth);
    }
    if (tileProperties.imageHeight) {
      $.tile_image.setHeight(tileProperties.imageHeight);
    }

    $.tile_name.setText(name);

    var prod_tile_label_base = 'product_tile_' + productId;

    $.tile_image.setAccessibilityLabel(prod_tile_label_base + '_image');
    $.tile_name.setAccessibilityValue(prod_tile_label_base + '_name');
    $.sale_price.setAccessibilityValue(prod_tile_label_base + '_price');

    if (tileProperties.title) {
      $.tile_name.applyProperties(tileProperties.title);
    }



    if (price !== null || !isNaN(price)) {
      priceText += eaUtils.toCurrency(price);
      $.sale_price.setText(priceText);
    }
    var listPriceBook = Alloy.CFG.country_configuration[Alloy.CFG.countrySelected].list_price_book;

    if (prices && prices[listPriceBook] && price != prices[listPriceBook]) {
      $.list_price.setText(eaUtils.toCurrency(prices[listPriceBook]));
      eaUtils.strikeThrough($.list_price, $.list_price.getText());
    }

    if (price_max) {
      maxPriceText += eaUtils.toCurrency(price_max);
      $.sale_price.setText(priceText + ' - ' + maxPriceText);
    }

    if (tileProperties.price) {
      $.sale_price.applyProperties(tileProperties.price);
    }

    var priceSize = $.sale_price.text.length;

    var colors = 0;
    var vas = hit.getVariationAttributes();
    var colorAttribute = null;
    if (vas) {
      colorAttribute = _.first(vas.filter(function (va) {
        return va.get('id') == Alloy.CFG.product.color_attribute;
      }));
      colors = colorAttribute ? colorAttribute.get('values').length : 0;
    }


    addColorSwatches(colorAttribute, colors, priceSize);


    if (includeMagnifyingGlass && Alloy.CFG.enable_zoom_image) {
      badgeContainer = Ti.UI.createView({
        layout: 'absolute',
        height: 60,
        width: 60,
        right: 0,
        top: 0,
        accessibilityLabel: 'product_tile_' + productId + '_zoom_container' });

      var magImage = Ti.UI.createImageView({
        width: 24,
        height: 24,
        top: 12,
        right: 15,
        zindex: 100,
        product_id: productId,
        accessibilityLabel: 'product_tile_' + productId + '_zoom_image' });

      Alloy.Globals.getImageViewImage(magImage, Alloy.Styles.zoomImage);
      badgeContainer.add(magImage);
      $.productTile.add(badgeContainer);
      onBadgeContainerClick = function (event) {
        event.cancelBubble = true;

        var matrix = Ti.UI.create2DMatrix();
        matrix = matrix.scale(0.9, 0.9);
        var a = Ti.UI.createAnimation({
          transform: matrix,
          duration: 100,
          autoreverse: true,
          repeat: 1 });


        badgeContainer.animate(a);

        logger.info('triggering zoom: ' + productId);
        $.productTile.fireEvent('product:zoom', {
          product_id: productId,
          selected_variation_value: colorAttribute ? colorAttribute.getValues()[0].getValue() : '' });

      };
      badgeContainer.addEventListener('click', onBadgeContainerClick);
    }

    var prodId = _.isFunction(hit.getProductId) && hit.getProductId() ? hit.getProductId() : hit.hasRecommendedItemId() ? hit.getRecommendedItemId() : '';
    var image;
    if (_.isFunction(hit.hasRecommendedItemId) && hit.hasRecommendedItemId()) {
      image = hit.getFirstImageFromImageGroup() || hit.getProductTileImage(prodId);
    } else {
      image = hit.getProductTileImage(prodId);
    }

    Alloy.Globals.getImageViewImage($.tile_image, image);
  }






  function deinit() {
    $.productTile.removeEventListener('click', handleProductTileClick);

    $.tile_image.removeEventListener('click', handleProductTileClick);
    $.stopListening();
    badgeContainer && badgeContainer.removeEventListener('click', onBadgeContainerClick);
    removeAllChildren($.productTile);
    $.destroy();
  }











  function addColorSwatches(colorAttribute, colors, length) {
    var maxSwatches = 5;
    if (length > 5 && length < 10) {
      maxSwatches = 4;
    } else if (length >= 10 && length < 15) {
      maxSwatches = 3;
    } else if (length >= 15 && length < 20) {
      maxSwatches = 2;
    } else if (length >= 20) {
      maxSwatches = 1;
    }

    if (colors > 0) {

      var models = colorAttribute.get('values').models;
      var swatchImages = [];
      _.each(models, function (color) {
        if (color.hasImageSwatch() && color.getImageSwatch().hasLink()) {
          swatchImages.push(color.getImageSwatchLink());
        }
      });
      var offset = 0;
      var images = [];

      if (swatchImages.length > maxSwatches) {
        view = Titanium.UI.createLabel({
          width: 15,
          height: 10,
          right: 0,
          top: 0,
          text: '+' + (swatchImages.length - maxSwatches),
          font: Alloy.Styles.swatchFont,
          color: Alloy.Styles.color.text.dark,
          accessibilityLabel: 'product_tile_' + productId + '_swatch_plus_label' });


        $.swatch_container.add(view);
        offset = view.width + 5;
        images = swatchImages.slice(0, maxSwatches).reverse();
      } else {
        images = swatchImages.reverse();
      }


      for (var i = 0, n = images.length; i < n; ++i) {
        var view = Titanium.UI.createImageView({
          width: 13,
          height: 13,
          right: 15 * i + offset,
          accessibilityLabel: 'product_tile_' + productId + '_swatch',
          top: 0 });


        Alloy.Globals.getImageViewImage(view, images[i]);
        $.swatch_container.add(view);
      }
    }
  }









  function handleProductTileClick(event) {
    event.cancelBubble = true;

    var matrix = Ti.UI.create2DMatrix();
    matrix = matrix.scale(0.9, 0.9);
    var a = Ti.UI.createAnimation({
      transform: matrix,
      duration: 100,
      autoreverse: true,
      repeat: 1 });


    $.productTile.animate(a);
    $.productTile.fireEvent('product:select', {
      product_id: productId });

  }




  init();





  __defers['$.__views.productTile!click!handleProductTileClick'] && $.addListener($.__views.productTile, 'click', handleProductTileClick);__defers['$.__views.tile_image!click!handleProductTileClick'] && $.addListener($.__views.tile_image, 'click', handleProductTileClick);



  _.extend($, exports);
}

module.exports = Controller;