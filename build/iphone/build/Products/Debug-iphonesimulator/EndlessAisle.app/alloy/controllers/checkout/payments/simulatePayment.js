var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'checkout/payments/simulatePayment';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.simulate_payment_window = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.color.background.transparent, id: "simulate_payment_window" });

  $.__views.simulate_payment_window && $.addTopLevelView($.__views.simulate_payment_window);
  $.__views.backdrop = Ti.UI.createView(
  { top: 0, left: 0, width: 1024, height: 768, opacity: 0.55, backgroundColor: Alloy.Styles.color.background.black, id: "backdrop" });

  $.__views.simulate_payment_window.add($.__views.backdrop);
  $.__views.card_container = Ti.UI.createView(
  { layout: "vertical", width: 450, left: 287, height: 380, top: 94, backgroundColor: Alloy.Styles.color.background.white, id: "card_container" });

  $.__views.simulate_payment_window.add($.__views.card_container);
  $.__views.title = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.accentColor, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.titleFont, top: 20, text: "Simulate Payments", id: "title", accessibilityLabel: "balance_title" });

  $.__views.card_container.add($.__views.title);
  $.__views.button_container = Ti.UI.createScrollView(
  { layout: "vertical", width: Ti.UI.FILL, height: 240, id: "button_container" });

  $.__views.card_container.add($.__views.button_container);
  $.__views.swipe_button = Ti.UI.createButton(
  { backgroundImage: Alloy.Styles.buttons.primary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.primary.backgroundDisabledImage, color: Alloy.Styles.buttons.primary.color, width: 390, height: 40, left: 35, top: 20, id: "swipe_button", titleid: "Swipe" });

  $.__views.button_container.add($.__views.swipe_button);
  $.__views.cancel_button = Ti.UI.createButton(
  { backgroundImage: Alloy.Styles.buttons.secondary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.secondary.backgroundDisabledImage, color: Alloy.Styles.buttons.primary.color, width: 390, height: 40, left: 35, top: 30, id: "cancel_button", titleid: "Cancel" });

  $.__views.card_container.add($.__views.cancel_button);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var logger = require('logging')('checkout:payments:simulatePayment', getFullControllerPath($.__controllerPath));




  $.backdrop.addEventListener('click', dismiss);

  $.cancel_button.addEventListener('click', dismiss);

  $.swipe_button.addEventListener('click', swipeEventHandler);

  var buttons = [];
  var eventType;
  var buttonData;




  exports.init = init;
  exports.deinit = deinit;










  function init(details) {
    logger.info('init');
    eventType = details.event_type;
    buttonData = details.button_data;
    _.each(details.button_data, function (button_data, index) {
      var button = Ti.UI.createButton({
        backgroundImage: Alloy.Styles.buttons.primary.backgroundImage,
        backgroundDisabledImage: Alloy.Styles.buttons.primary.backgroundDisabledImage,
        color: Alloy.Styles.buttons.primary.color,
        width: 390,
        height: 40,
        left: 35,
        top: 20,
        index: index,
        title: details.button_data[index].title });

      $.button_container.add(button);
      button.addEventListener('click', clickButton);
      buttons.push(button);
    });
  }






  function deinit() {
    _.each(buttons, function (button) {
      button.removeEventListener('click', clickButton);
    });
    $.backdrop.removeEventListener('click', dismiss);
    $.cancel_button.removeEventListener('click', dismiss);
    $.swipe_button.removeEventListener('click', swipeEventHandler);
    $.stopListening();
    $.destroy();
  }









  function dismiss() {
    $.trigger('simulate_payment:dismiss');
  }







  function clickButton(event) {
    var index = event.source.index;
    delete buttonData[index].title;
    Alloy.eventDispatcher.trigger(eventType, buttonData[index]);
    dismiss();
  }






  function swipeEventHandler() {
    $.trigger('simulate_payment:swipe');
    dismiss();
  }









  _.extend($, exports);
}

module.exports = Controller;