var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'components/header';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.header = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.color.background.black, top: 0, left: 0, height: 75, zIndex: 1, id: "header" });

  $.__views.header && $.addTopLevelView($.__views.header);
  $.__views.backdrop = Ti.UI.createView(
  { top: 0, left: 0, width: "100%", height: 75, opacity: 0.25, backgroundColor: Alloy.Styles.color.background.white, zIndex: 999, id: "backdrop", visible: false });

  $.__views.header.add($.__views.backdrop);
  $.__views.navigation_container = Ti.UI.createView(
  { layout: "absolute", backgroundColor: Alloy.Styles.accentColor, top: 20, left: 0, height: 55, width: "100%", id: "navigation_container" });

  $.__views.header.add($.__views.navigation_container);
  $.__views.header_tab_container = Ti.UI.createView(
  { layout: "horizontal", left: 0, width: "38%", id: "header_tab_container" });

  $.__views.navigation_container.add($.__views.header_tab_container);
  $.__views.hamburger_menu = Ti.UI.createButton(
  { left: 5, top: 10, width: 38, height: 35, backgroundImage: Alloy.Styles.hamburgerMenuImageWhite, backgroundSelectedImage: Alloy.Styles.hamburgerMenuImageBlack, backgroundColor: Alloy.Styles.accentColor, id: "hamburger_menu", accessibilityValue: "hamburger_menu" });

  $.__views.header_tab_container.add($.__views.hamburger_menu);
  showHideHamburgerMenu ? $.addListener($.__views.hamburger_menu, 'click', showHideHamburgerMenu) : __defers['$.__views.hamburger_menu!click!showHideHamburgerMenu'] = true;$.__views.home_icon = Ti.UI.createImageView(
  { width: 60, height: 45, top: 5, left: 5, image: Alloy.Styles.homeImage, accessibilityLabel: "home_icon", id: "home_icon", accessibilityValue: "home_icon" });

  $.__views.header_tab_container.add($.__views.home_icon);
  handleHomeClick ? $.addListener($.__views.home_icon, 'click', handleHomeClick) : __defers['$.__views.home_icon!click!handleHomeClick'] = true;$.__views.__alloyId89 = Ti.UI.createImageView(
  { accessibilityValue: "vertical_divider_1", id: "__alloyId89" });

  $.__views.header_tab_container.add($.__views.__alloyId89);
  $.__views.__alloyId90 = Ti.UI.createImageView(
  { accessibilityValue: "vertical_divider_2", width: 10, id: "__alloyId90" });

  $.__views.header_tab_container.add($.__views.__alloyId90);
  $.__views.payment_device_icon = Ti.UI.createImageView(
  { width: 0, height: 34, top: 11, left: 0, image: Alloy.Styles.paymentDeviceConnectedImage, accessibilityLabel: "payment_device_icon", id: "payment_device_icon", accessibilityValue: "payment_device_icon", visible: false });

  $.__views.header_tab_container.add($.__views.payment_device_icon);
  handlePaymentDeviceClick ? $.addListener($.__views.payment_device_icon, 'click', handlePaymentDeviceClick) : __defers['$.__views.payment_device_icon!click!handlePaymentDeviceClick'] = true;$.__views.__alloyId91 = Ti.UI.createImageView(
  { accessibilityValue: "vertical_divider_3", width: 2, id: "__alloyId91" });

  $.__views.header_tab_container.add($.__views.__alloyId91);
  $.__views.product_search_container = Ti.UI.createView(
  { top: 6, height: 45, width: 210, left: 20, layout: "horizontal", accessibilityLabel: "product_search_container", id: "product_search_container", accessibilityValue: "product_search_container" });

  $.__views.header_tab_container.add($.__views.product_search_container);
  handleProductSearchClick ? $.addListener($.__views.product_search_container, 'click', handleProductSearchClick) : __defers['$.__views.product_search_container!click!handleProductSearchClick'] = true;$.__views.product_search_icon = Ti.UI.createLabel(
  { width: 30, height: 30, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, top: 5, left: 0, backgroundImage: Alloy.Styles.productSearchImage, id: "product_search_icon", accessibilityValue: "product_search_icon" });

  $.__views.product_search_container.add($.__views.product_search_icon);
  $.__views.product_search_query = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: 45, color: Alloy.Styles.color.text.white, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.buttonFont, shadowColor: Alloy.Styles.color.shadow.medium, shadowOffset: { x: 1, y: 1 }, left: 10, ellipsize: true, textid: "_Search_Button", id: "product_search_query", accessibilityValue: "product_search_query" });

  $.__views.product_search_container.add($.__views.product_search_query);
  $.__views.__alloyId92 = Ti.UI.createImageView(
  { accessibilityValue: "vertical_divider_4", id: "__alloyId92" });

  $.__views.navigation_container.add($.__views.__alloyId92);
  $.__views.modal_control_container = Ti.UI.createView(
  { top: 5, left: "36%", layout: "horizontal", width: "37%", id: "modal_control_container" });

  $.__views.navigation_container.add($.__views.modal_control_container);
  $.__views.associate_logout_container = Ti.UI.createView(
  { top: 0, width: 160, height: 55, left: 0, layout: "vertical", id: "associate_logout_container" });

  $.__views.modal_control_container.add($.__views.associate_logout_container);
  $.__views.associate_logout_button = Ti.UI.createView(
  { layout: "horizontal", width: 160, height: 45, left: 0, backgroundSelectedImage: Alloy.Styles.headerButtonDownImage, backgroundFocusedImage: Alloy.Styles.headerButtonDownImage, top: 0, id: "associate_logout_button" });

  $.__views.associate_logout_container.add($.__views.associate_logout_button);
  handleAssociateLogoutClick ? $.addListener($.__views.associate_logout_button, 'click', handleAssociateLogoutClick) : __defers['$.__views.associate_logout_button!click!handleAssociateLogoutClick'] = true;$.__views.associate_icon = Ti.UI.createImageView(
  { image: Alloy.Styles.agentImage, height: 24, width: 24, left: 14, top: 10, id: "associate_icon", accessibilityValue: "associate_icon" });

  $.__views.associate_logout_button.add($.__views.associate_icon);
  $.__views.associate_label = Ti.UI.createLabel(
  { width: 106, height: 45, color: Alloy.Styles.color.text.white, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.buttonFont, shadowColor: Alloy.Styles.color.shadow.medium, shadowOffset: { x: 1, y: 1 }, left: 10, ellipsize: true, id: "associate_label", accessibilityValue: "associate_label" });

  $.__views.associate_logout_button.add($.__views.associate_label);
  $.__views.__alloyId93 = Ti.UI.createImageView(
  { accessibilityValue: "vertical_divider_5", id: "__alloyId93" });

  $.__views.modal_control_container.add($.__views.__alloyId93);
  $.__views.customer_shopfor_container = Ti.UI.createView(
  { layout: "horizontal", top: 0, width: 210, height: 55, left: 0, id: "customer_shopfor_container" });

  $.__views.modal_control_container.add($.__views.customer_shopfor_container);
  $.__views.customer_lookup = Ti.UI.createView(
  { layout: "horizontal", top: 0, width: 210, height: 45, left: 0, backgroundSelectedImage: Alloy.Styles.headerButtonDownImage, backgroundFocusedImage: Alloy.Styles.headerButtonDownImage, id: "customer_lookup" });

  $.__views.customer_shopfor_container.add($.__views.customer_lookup);
  handleCustomerSearchClick ? $.addListener($.__views.customer_lookup, 'click', handleCustomerSearchClick) : __defers['$.__views.customer_lookup!click!handleCustomerSearchClick'] = true;$.__views.customer_lookup_icon = Ti.UI.createImageView(
  { image: Alloy.Styles.customerImage, height: 28, width: 38, left: 4, top: 10, id: "customer_lookup_icon", accessibilityValue: "customer_lookup_icon" });

  $.__views.customer_lookup.add($.__views.customer_lookup_icon);
  $.__views.customer_lookup_label = Ti.UI.createLabel(
  { width: 160, height: 45, color: Alloy.Styles.color.text.white, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.buttonFont, shadowColor: Alloy.Styles.color.shadow.medium, shadowOffset: { x: 1, y: 1 }, left: 10, ellipsize: true, textid: "_Customer_Search_Button", id: "customer_lookup_label", accessibilityValue: "customer_lookup_label" });

  $.__views.customer_lookup.add($.__views.customer_lookup_label);
  $.__views.logged_in_customer_container = Ti.UI.createView(
  { layout: "horizontal", top: 0, width: 210, height: 45, left: 0, backgroundSelectedImage: Alloy.Styles.headerButtonDownImage, backgroundFocusedImage: Alloy.Styles.headerButtonDownImage, id: "logged_in_customer_container" });

  $.__views.customer_shopfor_container.add($.__views.logged_in_customer_container);
  handleCustomerLabelClick ? $.addListener($.__views.logged_in_customer_container, 'click', handleCustomerLabelClick) : __defers['$.__views.logged_in_customer_container!click!handleCustomerLabelClick'] = true;$.__views.customer_icon_loggedIn = Ti.UI.createImageView(
  { image: Alloy.Styles.customerImage, height: 28, width: 38, left: 4, top: 10, id: "customer_icon_loggedIn", accessibilityValue: "customer_icon_loggedIn" });

  $.__views.logged_in_customer_container.add($.__views.customer_icon_loggedIn);
  $.__views.customer_label = Ti.UI.createLabel(
  { width: 160, height: 45, color: Alloy.Styles.color.text.white, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.buttonFont, shadowColor: Alloy.Styles.color.shadow.medium, shadowOffset: { x: 1, y: 1 }, left: 10, ellipsize: true, id: "customer_label", accessibilityValue: "customer_label" });

  $.__views.logged_in_customer_container.add($.__views.customer_label);
  $.__views.wish_list_container = Ti.UI.createView(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT, width: 0, left: "73%", top: 5, borderRadius: 22.5, height: 0, visible: false, id: "wish_list_container" });

  $.__views.navigation_container.add($.__views.wish_list_container);
  $.__views.wish_list_button = Ti.UI.createButton(
  { width: "76%", backgroundImage: Alloy.Styles.starImageWhite, color: Alloy.Styles.accentColor, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.detailValueFont, left: 1, bottom: 4, top: 2, height: "96%", id: "wish_list_button", accessibilityValue: "wish_list_button" });

  $.__views.wish_list_container.add($.__views.wish_list_button);
  handleWishListClick ? $.addListener($.__views.wish_list_button, 'click', handleWishListClick) : __defers['$.__views.wish_list_button!click!handleWishListClick'] = true;$.__views.cart_container = Ti.UI.createView(
  { layout: "horizontal", textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT, width: "20%", left: "80%", top: 5, backgroundSelectedImage: Alloy.Styles.headerButtonDownImage, backgroundFocusedImage: Alloy.Styles.headerButtonDownImage, height: 45, id: "cart_container" });

  $.__views.navigation_container.add($.__views.cart_container);
  handleCartClick ? $.addListener($.__views.cart_container, 'click', handleCartClick) : __defers['$.__views.cart_container!click!handleCartClick'] = true;$.__views.product_items_count = Ti.UI.createLabel(
  { width: 40, height: 32, color: Alloy.Styles.accentColor, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, top: 6, left: 2, backgroundImage: Alloy.Styles.cartImage, font: Alloy.Styles.detailValueFont, id: "product_items_count", accessibilityValue: "product_items_count" });

  $.__views.cart_container.add($.__views.product_items_count);
  $.__views.cart_label = Ti.UI.createLabel(
  { width: 140, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.white, textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT, shadowColor: Alloy.Styles.color.shadow.medium, shadowOffset: { x: 1, y: 1 }, left: 2, top: 7, font: Alloy.Styles.calloutFont, id: "cart_label", accessibilityValue: "cart_label" });

  $.__views.cart_container.add($.__views.cart_label);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var paymentTerminal = require(Alloy.CFG.devices.payment_terminal_module);
  var logger = require('logging')('components:header', getFullControllerPath($.__controllerPath));

  var storeInfo = Alloy.Models.storeInfo;
  var currentCustomer = Alloy.Models.customer;
  var currentAssociate = Alloy.Models.associate;
  var currentBasket = Alloy.Models.basket;
  var customerAddress = Alloy.Models.customerAddress;
  var currentProductSearch = Alloy.Models.productSearch;
  var toCurrency = require('EAUtils').toCurrency;
  var storePasswordHelpers = require('storePasswordHelpers');
  var paymentDeviceInterval = null;
  var verifyAddressEditBeforeNavigation = require('EAUtils').verifyAddressEditBeforeNavigation;
  var exitButton = null;
  var associateLogoutButtonKiosk = null;


  var labelTextLength = 8;




  $.listenTo(Alloy.eventDispatcher, 'configurations:postlogin', renderCart);
  $.listenTo(Alloy.eventDispatcher, 'shipping_method:selected', renderCart);
  $.listenTo(Alloy.eventDispatcher, 'payments_active', function (event) {
    if (event.payments_active) {
      $.backdrop.show();
      $.backdrop.setBubbleParent(false);
    } else {
      $.backdrop.hide();
      $.backdrop.setBubbleParent(true);
    }
  });

  $.listenTo(Alloy.eventDispatcher, 'order_just_created', function () {
    $.backdrop.hide();
    $.backdrop.setBubbleParent(true);
  });

  $.listenTo(Alloy.eventDispatcher, 'customer_replace_basket', function () {
    $.listenTo(currentBasket, 'basket_sync', renderCart);
  });

  $.listenTo(Alloy.eventDispatcher, 'associate_logout', function () {
    $.navigation_container.setBorderWidth(1);
    $.navigation_container.setBorderColor('transparent');
    if (paymentDeviceInterval) {
      logger.info('clearing payment device interval');
      clearInterval(paymentDeviceInterval);
      paymentDeviceInterval = null;
    }

    $.payment_device_icon.setImage(Alloy.Styles.paymentDeviceConnectedImage);

    if (isKioskMode()) {
      $.stopListening(Alloy.eventDispatcher, 'kiosk:manager_login_change', handleKioskManagerLoginChange);
    }
    cleanupComponentsOnExit();
  });

  $.listenTo(Alloy.eventDispatcher, 'configurations:postlogin', function () {
    if (Alloy.CFG.devices.payment_terminal_module != 'webDevice' && Alloy.CFG.devices.check_device_connected_interval > 0) {
      $.payment_device_icon.setVisible(true);
      $.payment_device_icon.setWidth(34);
    } else {
      $.payment_device_icon.setVisible(false);
      $.payment_device_icon.setWidth(0);
    }
  });

  $.listenTo(Alloy.eventDispatcher, 'app:ready', function () {
    if (isKioskMode()) {
      $.listenTo(Alloy.eventDispatcher, 'kiosk:manager_login_change', handleKioskManagerLoginChange);
    }
  });




  $.header.addEventListener('click', onHeaderClick);






  $.listenTo(currentCustomer.productLists, 'reset', function () {
    var totalQuant = currentCustomer.productLists.getTotalQuantity();
    $.wish_list_button.setTitle(currentCustomer.productLists.getWishListCount() > 0 ? totalQuant || 0 : '');
  });



  $.listenTo(currentCustomer, 'change:login', function () {
    render();
  });


  $.listenTo(currentAssociate, 'change', render);

  $.listenTo(currentCustomer, 'change:first_name change:last_name', render);


  $.listenTo(currentBasket, 'basket_sync', renderCart);


  $.listenTo(Alloy.eventDispatcher, 'customer_logging_in', function () {
    $.stopListening(currentBasket);
  });




  exports.init = init;
  exports.deinit = deinit;
  exports.updateDeviceStatus = updateDeviceStatus;
  exports.initNotifications = initNotifications;









  function init() {
    logger.info('INIT called');
    render();
    adjustCartLabel();
  }






  function deinit() {
    logger.info('DEINIT called');
    removeAllChildren($.navigation_container);
    exitButton && exitButton.removeEventListener('click', handleExitForKiosk);
    associateLogoutButtonKiosk && associateLogoutButtonKiosk.removeEventListener('click', handleAssociateButtonForKiosk);

    $.removeListener();
    $.header.removeEventListener('click', onHeaderClick);
    $.stopListening();
    $.destroy();
  }






  function render() {
    isKioskMode() ? renderKioskMode() : renderStandard();
  }









  function initNotifications() {
    if (Alloy.CFG.devices.payment_terminal_module != 'webDevice' && Alloy.CFG.devices.check_device_connected_interval > 0) {

      setTimeout(function () {
        checkPaymentDeviceConnection();
        logger.info('creating payment device interval ' + Alloy.CFG.devices.check_device_connected_interval);
        paymentDeviceInterval = setInterval(checkPaymentDeviceConnection, Alloy.CFG.devices.check_device_connected_interval);
      }, 300);
    }
    setHamburgerWarning();
    if (storePasswordHelpers.isStorePasswordExpiring()) {

      $.listenTo(Alloy.Models.storeUser, 'change:expiration_days', setHamburgerWarning);
    }
  }






  function renderCart() {
    logger.info('HEADER RENDERING BASKET CHANGES');
    logger.secureLog(JSON.stringify(currentBasket.toJSON()));

    var productItems = currentBasket.getProductItems();
    var numItems = 0;
    _.each(productItems, function (productItem) {
      numItems += productItem.getQuantity();
    });
    var totalPrice = currentBasket.getOrderTotal() || currentBasket.getProductTotal();
    if (currentBasket.getOrderNo() && currentBasket.getOrderNo() !== 'null') {
      totalPrice = currentBasket.calculateBalanceDue();
    }
    var padding = '';
    if (numItems < 10) {
      padding = '  ';
    } else if (numItems < 100) {
      padding = ' ';
    }
    $.product_items_count.setText(padding + numItems);
    if (numItems == 0) {
      $.cart_label.setText(toCurrency(0));
    } else {
      $.cart_label.setText(toCurrency(totalPrice));
    }
    adjustCartLabel();
  }









  function handleProductSearchClick() {
    $.product_search_container.animate(Alloy.Animations.bounce);
    verifyAddressEditBeforeNavigation(Alloy.Router.presentProductSearchDrawer);
  }






  function onHeaderClick() {
    Alloy.eventDispatcher.trigger('app:header_bar_clicked');
  }






  function handleHomeClick() {
    $.home_icon.animate(Alloy.Animations.bounce);
    verifyAddressEditBeforeNavigation(Alloy.Router.navigateToHome);
  }







  function adjustCartLabel() {
    if ($.cart_label.getText().length > labelTextLength) {
      $.cart_label.setFont(Alloy.Styles.headlineFont);
      $.cart_label.setTop(10);
    } else {
      $.cart_label.setFont(Alloy.Styles.calloutFont);
      $.cart_label.setTop(7);
    }
  }






  function handlePaymentDeviceClick() {
    $.payment_device_icon.animate(Alloy.Animations.bounce);
    checkPaymentDeviceConnection(true);
  }






  function handleAssociateLogoutClick() {
    showAssociatePopover();
  }






  function handleCustomerSearchClick() {
    $.customer_lookup.animate(Alloy.Animations.bounce);
    Alloy.Router.presentCustomerSearchDrawer();
  }






  function handleCustomerLabelClick() {
    showCustomerProfilePopover();
  }






  function handleWishListClick() {
    verifyAddressEditBeforeNavigation(function () {
      if (isCustomerLoggedIn()) {
        Alloy.Router.navigateToCart({
          switchToWishList: true });

      }
    });
  }






  function handleCustomerLogoutClick() {
    showCustomerProfilePopover();
  }






  function handleCartClick() {
    $.cart_container.animate(Alloy.Animations.bounce);
    verifyAddressEditBeforeNavigation(function () {
      Alloy.Router.navigateToCart({
        switchToCart: true });

    });
  }






  function handleKioskManagerLoginChange() {
    if (!isKioskManagerLoggedIn()) {

      if (associateLogoutButtonKiosk) {
        hideKioskLogout();
      }
      if (Alloy.ViewManager.isOrderSearchViewVisible()) {
        Alloy.Router.navigateToHome();
      }
    }
    setHamburgerWarning();
  }







  function showSupportDashboard(requireAuth) {
    logger.info('Showing Admin Dashboard');
    allowAppSleep(false);
    Alloy.Dialog.showCustomDialog({
      fullScreen: true,
      viewName: 'admin_dashbord',
      controllerPath: 'support/index',
      options: {
        requireAuthorization: requireAuth },

      continueEvent: 'dashboard:dismiss',
      continueFunction: function () {
        Alloy.eventDispatcher.trigger('dashboard:dismiss');
      } });

  }






  function renderKioskMode() {
    logger.info('renderKioskMode called');

    $.navigation_container.remove($.modal_control_container);
    if (!isKioskCartEnabled()) {
      $.navigation_container.remove($.cart_container);
    } else {
      var width = $.cart_label.getWidth();
      if (!_.isNumber(width)) {
        width = 140;
      }
      $.navigation_container.add($.cart_container);
    }


    if (!exitButton) {
      exitButton = Ti.UI.createButton({
        id: 'exit_session_button',
        titleid: '_Exit',
        width: 80,
        height: 36,
        top: 10,
        left: isKioskCartEnabled() ? 710 : 925,
        borderRadius: 18,
        verticalAlign: Titanium.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
        font: Alloy.Styles.detailTextFont,
        color: Alloy.Styles.accentColor,
        backgroundColor: Alloy.Styles.color.background.light,
        accessibilityValue: 'exit_session_button' });

      exitButton.addEventListener('click', handleExitForKiosk);
      $.navigation_container.add(exitButton);
    } else {
      exitButton.setLeft(isKioskCartEnabled() ? 710 : 925);
    }

    $.wish_list_container.hide();
    $.wish_list_container.setHeight(0);
    $.wish_list_container.setWidth(0);
  }






  function renderStandard() {
    logger.info('HEADER RENDERING ASSOCIATE CHANGES');
    logger.secureLog(JSON.stringify(currentAssociate.toJSON()));
    if (exitButton) {
      $.navigation_container.remove(exitButton);
      exitButton.removeEventListener('click', handleExitForKiosk);
      exitButton = null;
      $.navigation_container.add($.modal_control_container);
      $.navigation_container.add($.cart_container);
    }

    var associateInfo = currentAssociate.getAssociateInfo();
    if (associateInfo) {
      var firstName = associateInfo.firstName;
      var lastName = associateInfo.lastName;

      var associateText = firstName ? firstName + ' ' : '';
      associateText += lastName ? lastName[0] + '.' : '';

      if (!firstName && !lastName) {
        associateText = _L('Associate');
      }
      $.associate_label.setText(associateText);
    }

    logger.info('HEADER RENDERING CUSTOMER CHANGES');
    logger.secureLog(JSON.stringify(currentCustomer.toJSON()));
    var agentLoggedIn = currentAssociate.isLoggedIn();
    var customer_no = currentCustomer.getCustomerNumber();
    var canLOBO = currentAssociate.hasPermissions() ? currentAssociate.getPermissions().allowLOBO : true;

    if (!agentLoggedIn || !canLOBO) {
      $.customer_label.setVisible(false);
      $.customer_icon_loggedIn.setVisible(false);
      $.customer_lookup.setVisible(false);
    } else if (customer_no) {
      $.customer_lookup.hide();
      $.customer_lookup.setWidth(0);
      var firstName = currentCustomer.getFirstName();
      var lastName = currentCustomer.getLastName();
      var customerText = firstName ? firstName + ' ' : '';
      customerText += lastName ? lastName : '';

      $.customer_label.setText(customerText);
      $.logged_in_customer_container.show();
      $.logged_in_customer_container.setWidth(220);
      $.customer_label.setVisible(true);
      $.customer_icon_loggedIn.setVisible(true);
      if (Alloy.CFG.enable_wish_list) {
        $.wish_list_container.setHeight(45);
        $.wish_list_container.setWidth('6%');
        $.wish_list_container.show();
      } else {
        $.wish_list_container.hide();
        $.wish_list_container.setHeight(0);
        $.wish_list_container.setWidth(0);
      }
    } else {
      $.logged_in_customer_container.hide();
      $.customer_lookup.show();
      $.customer_lookup.setWidth(220);
      $.customer_icon_loggedIn.setVisible(false);
      $.wish_list_container.hide();
      $.wish_list_container.setHeight(0);
      $.wish_list_container.setWidth(0);
    }
  }






  function handleExitForKiosk() {
    Alloy.Dialog.showConfirmationDialog({
      messageString: _L('Reset Session Message'),
      titleString: _L('Reset Session'),
      okFunction: function () {
        if (Alloy.Kiosk.hasOwnProperty('order_complete_timer')) {
          clearTimeout(Alloy.Kiosk.order_complete_timer);
        }


        if (Object.keys(Alloy.Kiosk).length == 0) {
          return;
        }
        Alloy.Router.associateLogout();
        Alloy.Kiosk = {};
      } });

  }






  function hideKioskLogout() {
    $.navigation_container.remove(associateLogoutButtonKiosk);
    associateLogoutButtonKiosk.removeEventListener('click', handleAssociateButtonForKiosk);
    associateLogoutButtonKiosk = null;
  }






  function handleAssociateButtonForKiosk() {

    if (Alloy.Kiosk.manager) {
      Alloy.Kiosk.manager = null;
      Alloy.eventDispatcher.trigger('kiosk:manager_login_change');
      notify(_L('Associate Logged Out'));
      return;
    }

    var associate = Alloy.createModel('associate');
    Alloy.Dialog.showCustomDialog({
      controllerPath: 'associate/authorization',
      initOptions: {
        titleText: _L('Associate Authorization'),
        subTitleText: _L('Enter Associate Credentials Kiosk'),
        managerIdHintText: _L('Associate ID'),
        managerPasswordHintText: _L('Password'),
        associate: associate,
        checkForAdminPrivileges: false },

      continueEvent: 'authorization:dismiss',
      continueFunction: function (result) {
        if (result && !result.result) {

          return;
        }

        if (result) {
          Alloy.Kiosk.manager = result.associate;
          Alloy.eventDispatcher.trigger('kiosk:manager_login_change');
          if (Alloy.Kiosk.manager) {

            associateLogoutButtonKiosk = Ti.UI.createButton({
              id: 'associate_logout_button_header',
              height: 24,
              width: 24,
              left: isKioskCartEnabled() ? 665 : 880,
              top: 15,
              backgroundImage: Alloy.Styles.agentImage,
              accessibilityLabel: 'associate_logout_button_header' });

            associateLogoutButtonKiosk.addEventListener('click', handleAssociateButtonForKiosk);
            $.navigation_container.add(associateLogoutButtonKiosk);
          }
          Alloy.Router.showHideHamburgerMenu();
        }
      } });

  }






  function showCustomerProfilePopover() {
    Alloy.Dialog.showCustomDialog({
      controllerPath: 'components/customerPopover',
      continueEvent: 'customerPopover:dismiss' });

  }






  function showAssociatePopover() {
    Alloy.Dialog.showCustomDialog({
      controllerPath: 'components/associatePopover',
      continueEvent: 'associatePopover:dismiss' });

  }






  function cleanupComponentsOnExit() {

    if (associateLogoutButtonKiosk) {
      hideKioskLogout();
    }
  }






  function isCustomerLoggedIn() {
    return currentCustomer.isLoggedIn();
  }








  function checkPaymentDeviceConnection(userTrigger) {
    var userTriggered = userTrigger || false;
    logger.info('checking payment device');
    updateDeviceStatus(userTriggered, paymentTerminal.verifyDeviceConnection());
  }









  function updateDeviceStatus(userTriggered, connected) {
    if ($.payment_device_icon.getVisible()) {
      logger.info('checking payment device connection');
      if (connected) {
        if ($.payment_device_icon.image == Alloy.Styles.paymentDeviceConnectedImage) {
          if (!userTriggered) {

            return;
          }
        }
        $.payment_device_icon.setImage(Alloy.Styles.paymentDeviceConnectedImage);
        notify(_L('Payment Device Connected'), 3000);
      } else {
        if ($.payment_device_icon.image == Alloy.Styles.paymentDeviceNotConnectedImage) {
          if (!userTriggered) {

            return;
          }
        }
        $.payment_device_icon.setImage(Alloy.Styles.paymentDeviceNotConnectedImage);
        Alloy.Router.checkPaymentDevice(true);
      }
    }
  }






  function showHideHamburgerMenu() {
    Alloy.eventDispatcher.trigger('hideAuxillaryViews');
    if (!isKioskManagerLoggedIn() && isKioskMode()) {
      handleAssociateButtonForKiosk();
    } else {

      var promise = new _.Deferred();
      if (storePasswordHelpers.isStorePasswordExpiring()) {
        promise = storePasswordHelpers.checkStorePassword();
      } else {
        promise.resolve();
      }
      Alloy.Router.showActivityIndicator(promise);
      promise.done(function () {
        setHamburgerWarning();
        Alloy.Router.showHideHamburgerMenu();
      });
    }
  }






  function setHamburgerWarning() {
    if (!isKioskMode() && storePasswordHelpers.isStorePasswordExpiring() || isKioskMode() && isKioskManagerLoggedIn() && storePasswordHelpers.isStorePasswordExpiring()) {
      $.hamburger_menu.setBackgroundImage(Alloy.Styles.hamburgerMenuImageNotification);
    } else {
      $.hamburger_menu.setBackgroundImage(Alloy.Styles.hamburgerMenuImageWhite);
    }
  }




  $.cart_label.setText(toCurrency(0));





  __defers['$.__views.hamburger_menu!click!showHideHamburgerMenu'] && $.addListener($.__views.hamburger_menu, 'click', showHideHamburgerMenu);__defers['$.__views.home_icon!click!handleHomeClick'] && $.addListener($.__views.home_icon, 'click', handleHomeClick);__defers['$.__views.payment_device_icon!click!handlePaymentDeviceClick'] && $.addListener($.__views.payment_device_icon, 'click', handlePaymentDeviceClick);__defers['$.__views.product_search_container!click!handleProductSearchClick'] && $.addListener($.__views.product_search_container, 'click', handleProductSearchClick);__defers['$.__views.associate_logout_button!click!handleAssociateLogoutClick'] && $.addListener($.__views.associate_logout_button, 'click', handleAssociateLogoutClick);__defers['$.__views.customer_lookup!click!handleCustomerSearchClick'] && $.addListener($.__views.customer_lookup, 'click', handleCustomerSearchClick);__defers['$.__views.logged_in_customer_container!click!handleCustomerLabelClick'] && $.addListener($.__views.logged_in_customer_container, 'click', handleCustomerLabelClick);__defers['$.__views.wish_list_button!click!handleWishListClick'] && $.addListener($.__views.wish_list_button, 'click', handleWishListClick);__defers['$.__views.cart_container!click!handleCartClick'] && $.addListener($.__views.cart_container, 'click', handleCartClick);



  _.extend($, exports);
}

module.exports = Controller;