var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'product/components/detailHeader').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'product/components/detailHeaderSet';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  exports.destroy = function () {};




  _.extend($, $.__views);







  exports.baseController = 'product/components/detailHeader';




  var logger = require('logging')('product:components:detailHeaderSet', getFullControllerPath($.__controllerPath));




  exports.init = init;
  exports.deinit = deinit;
  exports.enableAddAllToCart = enableAddAllToCart;









  function init(info) {
    logger.info('init override called');

    $.pricing.init();

    $.product_id.init();
    if (!isKioskMode() || isKioskMode() && isKioskCartEnabled()) {

      $.add_all_to_cart_btn = Ti.UI.createButton({
        left: 320,
        enabled: false,
        titleid: '_Add_All_to_Cart',
        accessibilityLabel: 'add_to_cart_btn',
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        color: Alloy.Styles.buttons.primary.color,
        disabledColor: Alloy.Styles.buttons.primary.disabledColor,
        backgroundImage: Alloy.Styles.buttons.primary.backgroundImage,
        backgroundDisabledImage: Alloy.Styles.buttons.primary.backgroundDisabledImage,
        height: 35,
        width: 200,
        top: 10 });

      $.pricing_container.add($.add_all_to_cart_btn);
      $.add_all_to_cart_btn.addEventListener('singletap', onAddAllToCart);
      $.pricing_container.addEventListener('singletap', onPricingTap);
    }

    var horizontal_separator = Ti.UI.createView({
      width: Ti.UI.FILL,
      backgroundColor: Alloy.Styles.color.background.dark,
      height: 1,
      left: 0,
      top: 10 });

    $.pdp_header.add(horizontal_separator);
    $.render();
  }






  var superDeinit = $.deinit;
  function deinit() {
    logger.info('deinit override called');
    $.stopListening();
    superDeinit();
    $.add_all_to_cart_btn && $.add_all_to_cart_btn.removeEventListener('singletap', onAddAllToCart);
    $.pricing_container.removeEventListener('singletap', onPricingTap);
    removeAllChildren($.pricing_container);
    removeAllChildren($.pdp_header);
    $.destroy();
  }










  function enableAddAllToCart(enabled) {
    logger.info('enableAddAllToCart called');
    if (!isKioskMode() || isKioskMode() && isKioskCartEnabled()) {
      $.add_all_to_cart_btn.setEnabled(enabled);
      $.add_all_to_cart_btn.setTouchEnabled(enabled);
    }
  }









  function onAddAllToCart() {
    logger.info('onAddAllToCart called');
    $.trigger('detailHeaderSet:add_all_to_cart_btn_tap');
  }







  function onPricingTap() {
    logger.info('onPricingTap called');
    if ($.add_all_to_cart_btn && (!$.add_all_to_cart_btn.getTouchEnabled() || !$.add_all_to_cart_btn.getEnabled())) {
      $.trigger('detailHeaderSet:check_all_variations');
    }
  }









  _.extend($, exports);
}

module.exports = Controller;