var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'checkout/payments/index';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.payments = Ti.UI.createView(
  { layout: "vertical", id: "payments" });

  $.__views.payments && $.addTopLevelView($.__views.payments);
  $.__views.selectorRow = Ti.UI.createView(
  { layout: "horizontal", width: "100%", height: 51, top: 10, id: "selectorRow" });

  $.__views.payments.add($.__views.selectorRow);
  $.__views.one_card_button = Ti.UI.createButton(
  { left: 170, top: 12, width: 180, height: 27, color: Alloy.Styles.buttons.primary.color, font: Alloy.Styles.smallButtonFont, backgroundImage: Alloy.Styles.buttonLeftOnImage, titleid: "_Pay_with_one_card", id: "one_card_button", accessibilityValue: "one_card_button" });

  $.__views.selectorRow.add($.__views.one_card_button);
  $.__views.multiple_cards_button = Ti.UI.createButton(
  { top: 12, width: 180, height: 27, color: Alloy.Styles.buttons.primary.color, font: Alloy.Styles.smallButtonFont, backgroundImage: Alloy.Styles.buttonRightOffImage, titleid: "_Pay_with_multiple_cards", id: "multiple_cards_button", accessibilityValue: "multiple_cards_button" });

  $.__views.selectorRow.add($.__views.multiple_cards_button);
  $.__views.balance_due_container = Ti.UI.createView(
  { layout: "horizontal", left: 210, top: 12, height: Ti.UI.SIZE, id: "balance_due_container" });

  $.__views.payments.add($.__views.balance_due_container);
  $.__views.balance_due_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.titleFont, textid: "_Balance_Due__", id: "balance_due_label", accessibilityValue: "balance_due_label" });

  $.__views.balance_due_container.add($.__views.balance_due_label);
  $.__views.balance_due = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.titleFont, left: 10, id: "balance_due", accessibilityValue: "balance_due" });

  $.__views.balance_due_container.add($.__views.balance_due);
  $.__views.payment_types_container = Ti.UI.createView(
  { layout: "horizontal", left: 140, top: 8, height: Ti.UI.SIZE, id: "payment_types_container" });

  $.__views.payments.add($.__views.payment_types_container);
  $.__views.credit_card_button = Ti.UI.createLabel(
  { width: 200, height: 46, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, backgroundImage: Alloy.Styles.creditCardButtonUpImage, backgroundFocusedImage: Alloy.Styles.creditCardButtonDownImage, backgroundSelectedImage: Alloy.Styles.creditCardButtonDownImage, font: Alloy.Styles.buttonMediumFont, textid: "_Credit_Card", id: "credit_card_button", accessibilityValue: "credit_card_button" });

  $.__views.payment_types_container.add($.__views.credit_card_button);
  $.__views.gift_card_button = Ti.UI.createLabel(
  { width: 200, height: 46, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, backgroundImage: Alloy.Styles.creditCardButtonUpImage, backgroundFocusedImage: Alloy.Styles.creditCardButtonDownImage, backgroundSelectedImage: Alloy.Styles.creditCardButtonDownImage, font: Alloy.Styles.buttonMediumFont, textid: "_Gift_Card", left: 24, id: "gift_card_button", accessibilityValue: "gift_card_button" });

  $.__views.payment_types_container.add($.__views.gift_card_button);
  $.__views.tender_amount_container = Ti.UI.createView(
  { top: 26, left: 100, width: 500, height: 60, borderWidth: 1, borderColor: Alloy.Styles.color.border.lighter, layout: "horizontal", id: "tender_amount_container", visible: false });

  $.__views.payments.add($.__views.tender_amount_container);
  $.__views.currency_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.registerReadoutFont, text: Alloy.CFG.currencySymbol, left: 7, id: "currency_label", accessibilityValue: "currency_label" });

  $.__views.tender_amount_container.add($.__views.currency_label);
  $.__views.number_entered = Ti.UI.createLabel(
  { width: "92%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT, font: Alloy.Styles.registerReadoutFont, right: 0, left: 10, text: "", id: "number_entered", accessibilityValue: "number_entered" });

  $.__views.tender_amount_container.add($.__views.number_entered);
  $.__views.number_entry_container = Ti.UI.createView(
  { layout: "horizontal", left: 101, height: Ti.UI.SIZE, top: 15, id: "number_entry_container", visible: false });

  $.__views.payments.add($.__views.number_entry_container);
  $.__views.numbers = Ti.UI.createView(
  { layout: "vertical", width: "62%", height: Ti.UI.SIZE, top: 0, id: "numbers" });

  $.__views.number_entry_container.add($.__views.numbers);
  $.__views.top_row = Ti.UI.createView(
  { layout: "horizontal", height: Ti.UI.SIZE, left: 0, id: "top_row" });

  $.__views.numbers.add($.__views.top_row);
  $.__views.seven_button = Ti.UI.createButton(
  { width: 109, height: 55, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.color.text.dark, backgroundImage: Alloy.Styles.calcNumberButtonUpImage, backgroundFocusedImage: Alloy.Styles.calcNumberButtonDownImage, backgroundSelectedImage: Alloy.Styles.calcNumberButtonDownImage, font: Alloy.Styles.registerButtonFont, title: 7, id: "seven_button", accessibilityValue: "seven_button" });

  $.__views.top_row.add($.__views.seven_button);
  $.__views.eight_button = Ti.UI.createButton(
  { width: 109, height: 55, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.color.text.dark, backgroundImage: Alloy.Styles.calcNumberButtonUpImage, backgroundFocusedImage: Alloy.Styles.calcNumberButtonDownImage, backgroundSelectedImage: Alloy.Styles.calcNumberButtonDownImage, font: Alloy.Styles.registerButtonFont, left: 15, title: 8, id: "eight_button", accessibilityValue: "eight_button" });

  $.__views.top_row.add($.__views.eight_button);
  $.__views.nine_button = Ti.UI.createButton(
  { width: 109, height: 55, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.color.text.dark, backgroundImage: Alloy.Styles.calcNumberButtonUpImage, backgroundFocusedImage: Alloy.Styles.calcNumberButtonDownImage, backgroundSelectedImage: Alloy.Styles.calcNumberButtonDownImage, font: Alloy.Styles.registerButtonFont, left: 15, title: 9, id: "nine_button", accessibilityValue: "nine_button" });

  $.__views.top_row.add($.__views.nine_button);
  $.__views.second_row = Ti.UI.createView(
  { layout: "horizontal", height: Ti.UI.SIZE, left: 0, top: 25, id: "second_row" });

  $.__views.numbers.add($.__views.second_row);
  $.__views.four_button = Ti.UI.createButton(
  { width: 109, height: 55, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.color.text.dark, backgroundImage: Alloy.Styles.calcNumberButtonUpImage, backgroundFocusedImage: Alloy.Styles.calcNumberButtonDownImage, backgroundSelectedImage: Alloy.Styles.calcNumberButtonDownImage, font: Alloy.Styles.registerButtonFont, title: 4, id: "four_button", accessibilityValue: "four_button" });

  $.__views.second_row.add($.__views.four_button);
  $.__views.five_button = Ti.UI.createButton(
  { width: 109, height: 55, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.color.text.dark, backgroundImage: Alloy.Styles.calcNumberButtonUpImage, backgroundFocusedImage: Alloy.Styles.calcNumberButtonDownImage, backgroundSelectedImage: Alloy.Styles.calcNumberButtonDownImage, font: Alloy.Styles.registerButtonFont, left: 15, title: 5, id: "five_button", accessibilityValue: "five_button" });

  $.__views.second_row.add($.__views.five_button);
  $.__views.six_button = Ti.UI.createButton(
  { width: 109, height: 55, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.color.text.dark, backgroundImage: Alloy.Styles.calcNumberButtonUpImage, backgroundFocusedImage: Alloy.Styles.calcNumberButtonDownImage, backgroundSelectedImage: Alloy.Styles.calcNumberButtonDownImage, font: Alloy.Styles.registerButtonFont, left: 15, title: 6, id: "six_button", accessibilityValue: "six_button" });

  $.__views.second_row.add($.__views.six_button);
  $.__views.third_row = Ti.UI.createView(
  { layout: "horizontal", height: Ti.UI.SIZE, left: 0, top: 25, id: "third_row" });

  $.__views.numbers.add($.__views.third_row);
  $.__views.one_button = Ti.UI.createButton(
  { width: 109, height: 55, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.color.text.dark, backgroundImage: Alloy.Styles.calcNumberButtonUpImage, backgroundFocusedImage: Alloy.Styles.calcNumberButtonDownImage, backgroundSelectedImage: Alloy.Styles.calcNumberButtonDownImage, font: Alloy.Styles.registerButtonFont, title: 1, id: "one_button", accessibilityValue: "one_button" });

  $.__views.third_row.add($.__views.one_button);
  $.__views.two_button = Ti.UI.createButton(
  { width: 109, height: 55, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.color.text.dark, backgroundImage: Alloy.Styles.calcNumberButtonUpImage, backgroundFocusedImage: Alloy.Styles.calcNumberButtonDownImage, backgroundSelectedImage: Alloy.Styles.calcNumberButtonDownImage, font: Alloy.Styles.registerButtonFont, left: 15, title: 2, id: "two_button", accessibilityValue: "two_button" });

  $.__views.third_row.add($.__views.two_button);
  $.__views.three_button = Ti.UI.createButton(
  { width: 109, height: 55, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.color.text.dark, backgroundImage: Alloy.Styles.calcNumberButtonUpImage, backgroundFocusedImage: Alloy.Styles.calcNumberButtonDownImage, backgroundSelectedImage: Alloy.Styles.calcNumberButtonDownImage, font: Alloy.Styles.registerButtonFont, left: 15, title: 3, id: "three_button", accessibilityValue: "three_button" });

  $.__views.third_row.add($.__views.three_button);
  $.__views.fourth_row = Ti.UI.createView(
  { layout: "horizontal", height: Ti.UI.SIZE, left: 0, top: 25, id: "fourth_row" });

  $.__views.numbers.add($.__views.fourth_row);
  $.__views.zero_button = Ti.UI.createButton(
  { width: 109, height: 55, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.color.text.dark, backgroundImage: Alloy.Styles.calcNumberButtonUpImage, backgroundFocusedImage: Alloy.Styles.calcNumberButtonDownImage, backgroundSelectedImage: Alloy.Styles.calcNumberButtonDownImage, font: Alloy.Styles.registerButtonFont, title: 0, id: "zero_button", accessibilityValue: "zero_button" });

  $.__views.fourth_row.add($.__views.zero_button);
  $.__views.delete_button = Ti.UI.createButton(
  { width: 233, height: 55, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.color.text.dark, backgroundImage: Alloy.Styles.backspaceButtonUpImage, backgroundFocusedImage: Alloy.Styles.backspaceButtonDownImage, backgroundSelectedImage: Alloy.Styles.backspaceButtonDownImage, font: Alloy.Styles.registerButtonFont, left: 15, id: "delete_button", accessibilityValue: "delete_button" });

  $.__views.fourth_row.add($.__views.delete_button);
  $.__views.keypadButtons = Ti.UI.createView(
  { layout: "vertical", width: Ti.UI.SIZE, height: Ti.UI.SIZE, top: 0, left: 7, id: "keypadButtons" });

  $.__views.number_entry_container.add($.__views.keypadButtons);
  $.__views.swipe_card_in_keypad = Ti.UI.createButton(
  { left: 0, height: 215, width: 129, backgroundImage: Alloy.Styles.enterButtonUpImage, backgroundFocusedImage: Alloy.Styles.enterButtonDownImage, backgroundSelectedImage: Alloy.Styles.enterButtonDownImage, color: Alloy.Styles.color.text.white, font: Alloy.Styles.buttonFont, top: 0, titleid: "_Pay_Now", id: "swipe_card_in_keypad", accessibilityValue: "swipe_card_in_keypad" });

  $.__views.keypadButtons.add($.__views.swipe_card_in_keypad);
  $.__views.cancel_order_button = Ti.UI.createButton(
  { width: 129, height: 55, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.accentColor, backgroundImage: Alloy.Styles.calcNumberButtonUpImage, backgroundFocusedImage: Alloy.Styles.calcNumberButtonUpImage, backgroundSelectedImage: Alloy.Styles.calcNumberButtonUpImage, font: Alloy.Styles.buttonFont, left: 0, top: 25, titleid: "_Cancel_Order", id: "cancel_order_button", accessibilityValue: "cancel_order_button" });

  $.__views.keypadButtons.add($.__views.cancel_order_button);
  $.__views.just_swipe_container = Ti.UI.createView(
  { layout: "vertical", top: 10, height: Ti.UI.SIZE, left: 55, id: "just_swipe_container" });

  $.__views.payments.add($.__views.just_swipe_container);
  $.__views.swipe_no_keypad = Ti.UI.createButton(
  { height: 90, width: 583, backgroundImage: Alloy.Styles.enterButtonUpImage, backgroundFocusedImage: Alloy.Styles.enterButtonDownImage, backgroundSelectedImage: Alloy.Styles.enterButtonDownImage, color: Alloy.Styles.color.text.white, font: Alloy.Styles.bigButtonFont, titleid: "_Pay_Now", left: 0, id: "swipe_no_keypad", accessibilityValue: "swipe_no_keypad" });

  $.__views.just_swipe_container.add($.__views.swipe_no_keypad);
  $.__views.cancel_no_keypad = Ti.UI.createButton(
  { height: 45, width: 583, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.accentColor, backgroundImage: Alloy.Styles.calcNumberButtonUpImage, backgroundFocusedImage: Alloy.Styles.calcNumberButtonUpImage, backgroundSelectedImage: Alloy.Styles.calcNumberButtonUpImage, font: Alloy.Styles.buttonFont, titleid: "_Cancel_Order", top: 15, left: 0, id: "cancel_no_keypad", accessibilityValue: "cancel_no_keypad" });

  $.__views.just_swipe_container.add($.__views.cancel_no_keypad);
  $.__views.payment_message_label = Ti.UI.createLabel(
  { width: "100%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.red, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.detailLabelFont, top: 50, left: 0, textid: "_Payment_Terminal_Info_Message", id: "payment_message_label", accessibilityValue: "payment_terminal_message" });

  $.__views.payments.add($.__views.payment_message_label);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var toCurrency = require('EAUtils').toCurrency;
  var analytics = require('analyticsBase');
  var logger = require('logging')('checkout:payments:index', getFullControllerPath($.__controllerPath));
  var currentBasket = Alloy.Models.basket;
  var currentCustomer = Alloy.Models.customer;
  var countryConfig = require('config/countries').countryConfig;
  var currencyConfig = require('config/countries').currencyConfig;
  var paymentTerminal = require(Alloy.CFG.devices.payment_terminal_module);
  var selectedButtonBackground = Alloy.Styles.creditCardButtonDownImage;
  var deselectedButtonBackground = Alloy.Styles.creditCardButtonUpImage;
  var selectedColor = Alloy.Styles.color.text.white;
  var deselectedColor = Alloy.Styles.color.text.dark;
  var listeningGC = false;
  var listeningCC = false;
  var singleTender = true;

  var creditCardPaymentType = 'creditCard';
  var giftCardPaymentType = 'giftCard';
  var dot = currencyConfig[countryConfig[Alloy.CFG.countrySelected].appCurrency].delimiters.decimal;

  var blankNumberEntered = '0' + dot + '00';
  var usingMultipleCards = false;


  var buttonTextLength = 13;





  $.listenTo(Alloy.eventDispatcher, 'payment_terminal:retry', startPaymentListening);






  $.multiple_cards_button.addEventListener('click', onMultipleCardsClick);

  $.one_card_button.addEventListener('click', onOneCardClick);

  $.credit_card_button.addEventListener('click', onCreditCardClick);

  $.gift_card_button.addEventListener('click', onGiftCardClick);

  $.swipe_card_in_keypad.addEventListener('click', onSwipeCardClick);

  $.swipe_no_keypad.addEventListener('click', onSwipeNoKeypadClick);

  $.cancel_order_button.addEventListener('click', cancelOrder);

  $.cancel_no_keypad.addEventListener('click', cancelOrder);

  $.one_button.addEventListener('click', onNumberButtonClick);

  $.two_button.addEventListener('click', onNumberButtonClick);

  $.three_button.addEventListener('click', onNumberButtonClick);

  $.four_button.addEventListener('click', onNumberButtonClick);

  $.five_button.addEventListener('click', onNumberButtonClick);

  $.six_button.addEventListener('click', onNumberButtonClick);

  $.seven_button.addEventListener('click', onNumberButtonClick);

  $.eight_button.addEventListener('click', onNumberButtonClick);

  $.nine_button.addEventListener('click', onNumberButtonClick);

  $.zero_button.addEventListener('click', onNumberButtonClick);

  $.delete_button.addEventListener('click', onDeleteButtonClick);





  $.listenTo(currentBasket, 'change', function () {
    calculateBalanceDue();
  });

  $.listenTo(currentBasket, 'change:order_abandonded', function () {
    if (currentBasket.get('order_abandonded')) {
      $.number_text.setText('');
    }
  });




  exports.init = init;
  exports.render = render;
  exports.deinit = deinit;
  exports.applyGCPayment = applyGCPayment;
  exports.stopPaymentListening = stopPaymentListening;










  function init() {
    logger.info('INIT called');
    var def = new _.Deferred();
    def.resolve();
    return def.promise();
  }






  function render() {
    logger.info('render called');
    var giftCardsAvailability = Alloy.CFG.gift_cards_available;
    if (!Alloy.CFG.enable_multi_tender_payments) {

      $.selectorRow.hide();
      $.selectorRow.setHeight(0);
      $.balance_due_container.setTop(40);
      $.payment_message_label.setTop(70);
      if (!paymentTerminal.supports('gift_cards') || !giftCardsAvailability) {
        $.just_swipe_container.setTop(0);
        $.payment_types_container.setTop(0);
      } else {
        $.payment_types_container.setTop(30);
      }
    } else {
      $.selectorRow.show();
      $.selectorRow.setHeight(51);
      $.balance_due_container.setTop(12);
      $.payment_message_label.setTop(50);

      $.multiple_cards_button.setBackgroundImage(Alloy.Styles.buttonRightOffImage);
      $.one_card_button.setBackgroundImage(Alloy.Styles.buttonLeftOnImage);
      if (!paymentTerminal.supports('gift_cards') || !giftCardsAvailability) {
        $.just_swipe_container.setTop(10);
        $.payment_types_container.setTop(0);
      } else {
        $.payment_types_container.setTop(30);
      }
    }


    if (!paymentTerminal.supports('gift_cards') || !giftCardsAvailability) {
      $.payment_types_container.hide();
      $.payment_types_container.setHeight(0);
    } else {
      $.payment_types_container.show();
      $.payment_types_container.setHeight(Titanium.UI.SIZE);


      selectButton($.credit_card_button, true);
      selectButton($.gift_card_button, false);
    }

    paymentMethodType = creditCardPaymentType;

    $.just_swipe_container.setHeight(Ti.UI.SIZE);
    $.just_swipe_container.show();

    $.number_entry_container.hide();
    $.number_entry_container.setHeight(0);
    $.tender_amount_container.setHeight(0);
    $.tender_amount_container.hide();
    $.number_entered.setText('0' + dot + '00');
    usingMultipleCards = false;
    if ($.swipe_card_in_keypad.getTitle().length > buttonTextLength || $.cancel_order_button.getTitle().length > buttonTextLength) {
      $.swipe_card_in_keypad.setFont(Alloy.Styles.smallButtonFont);
      $.cancel_order_button.setFont(Alloy.Styles.smallButtonFont);
    }
  }






  function deinit() {
    logger.info('DEINIT called');

    $.stopListening();
    $.multiple_cards_button.removeEventListener('click', onMultipleCardsClick);
    $.one_card_button.removeEventListener('click', onOneCardClick);
    $.credit_card_button.removeEventListener('click', onCreditCardClick);
    $.gift_card_button.removeEventListener('click', onGiftCardClick);
    $.swipe_card_in_keypad.removeEventListener('click', onSwipeCardClick);
    $.swipe_no_keypad.removeEventListener('click', onSwipeNoKeypadClick);
    $.cancel_order_button.removeEventListener('click', cancelOrder);
    $.cancel_no_keypad.removeEventListener('click', cancelOrder);
    $.one_button.removeEventListener('click', onNumberButtonClick);
    $.two_button.removeEventListener('click', onNumberButtonClick);
    $.three_button.removeEventListener('click', onNumberButtonClick);
    $.four_button.removeEventListener('click', onNumberButtonClick);
    $.five_button.removeEventListener('click', onNumberButtonClick);
    $.six_button.removeEventListener('click', onNumberButtonClick);
    $.seven_button.removeEventListener('click', onNumberButtonClick);
    $.eight_button.removeEventListener('click', onNumberButtonClick);
    $.nine_button.removeEventListener('click', onNumberButtonClick);
    $.zero_button.removeEventListener('click', onNumberButtonClick);
    $.delete_button.removeEventListener('click', onDeleteButtonClick);
    $.destroy();
  }











  function applyGCPayment(payment, toApply) {
    payment.redeem_amount = toApply;

    if (Alloy.CFG.payment_process_flow === 'A') {
      var deferred = new _.Deferred();
      Alloy.Router.showActivityIndicator(deferred);
      currentBasket.applyGiftCard(payment).done(function () {
        $.number_entered.setText(blankNumberEntered);

        setPaymentType(creditCardPaymentType);

        if (Math.abs(currentBasket.calculateBalanceDue()) < 0.001) {
          authorizePayments(deferred);
        } else {
          deferred.resolve();
        }
      }).fail(function (failedModel, failedOptions, failedParams) {
        logger.error('A flow applyGiftCard failure: ' + handleCardAuthorizationFailure(failedModel));


        Ti.App.fireEvent('payment_terminal:enable_retry', {
          message: _L('Unable to complete order. Retry?'),
          payment_data: payment });


        deferred.reject();
      });
    } else if (Alloy.CFG.payment_process_flow === 'B') {

      var deferred = new _.Deferred();
      Alloy.Router.showActivityIndicator(deferred);
      currentBasket.authorizeGiftCard(payment).done(function (model, options, params) {
        $.number_entered.setText(blankNumberEntered);

        setPaymentType(creditCardPaymentType);

        if (currentBasket.isOrderConfirmed()) {
          $.trigger('payment:success', {
            title: _L('Approved'),
            message: _L('Payment Approved'),
            completionFunction: function () {
              fetchOrder();
            } });

          sendAnalyticsEvents();
          deferred.resolve();
        } else if (currentBasket.getPaymentBalance() == 0) {


          deferred.reject();
          $.trigger('payment:error', {
            title: _L('Payment Status'),
            message: _L('Error applying gift card. \nThe order has not been confirmed on server.') });

        } else {
          $.trigger('payment:success', {
            title: _L('Approved'),
            message: _L('Payment Approved') });

          deferred.resolve();
        }
      }).fail(function (failedModel, failedOptions, failedParams) {
        logger.error('B flow authorizeGiftCard failure: ' + handleCardAuthorizationFailure(failedModel));


        Ti.App.fireEvent('payment_terminal:enable_retry', {
          message: _L('Unable to complete order. Retry?'),
          payment_data: payment });


        deferred.reject();
      });
    }
  }






  function stopPaymentListening() {
    listeningCC = false;
    listeningGC = false;
    $.stopListening(Alloy.eventDispatcher, 'payment:cc_approved', ccPaymentApproved);
    $.stopListening(Alloy.eventDispatcher, 'payment:credit_card_data', ccPaymentEntered);
    $.stopListening(Alloy.eventDispatcher, 'payment:cc_declined', ccPaymentDeclined);
    $.stopListening(Alloy.eventDispatcher, 'payment:cc_error', ccPaymentError);
    $.stopListening(Alloy.eventDispatcher, 'payment:gift_card_data', gcPaymentEntered);
    $.stopListening(Alloy.eventDispatcher, 'payment:stop_payment_listening', stopPaymentListening);
    Alloy.eventDispatcher.trigger('payment:payment_listeners_stopped');
  }






  function startPaymentListening() {

    if (!listeningCC) {

      $.listenTo(Alloy.eventDispatcher, 'payment:credit_card_data', ccPaymentEntered);

      $.listenTo(Alloy.eventDispatcher, 'payment:cc_approved', ccPaymentApproved);

      $.listenTo(Alloy.eventDispatcher, 'payment:cc_declined', ccPaymentDeclined);

      $.listenTo(Alloy.eventDispatcher, 'payment:cc_error', ccPaymentError);

      $.listenTo(Alloy.eventDispatcher, 'payment:stop_payment_listening', stopPaymentListening);
      listeningCC = true;
    }
  }










  function addNumber(number) {

    var numberEntered = $.number_entered.getText();

    var floatingPointPart = numberEntered.substring(numberEntered.indexOf(dot) + 1);
    var integerPart = numberEntered.substring(0, numberEntered.indexOf(dot));
    if (integerPart.substring(0, 1) == '0') {
      integerPart = '';
    }

    var newNumber = integerPart + floatingPointPart.substring(0, 1) + dot + floatingPointPart.substring(1) + number;
    $.number_entered.setText(newNumber);
  }






  function onMultipleCardsClick() {
    $.tender_amount_container.show();
    $.tender_amount_container.setHeight(Ti.UI.SIZE);
    $.number_entry_container.show();
    $.number_entry_container.setHeight(Ti.UI.SIZE);
    $.just_swipe_container.hide();
    $.just_swipe_container.setHeight(0);

    $.multiple_cards_button.setBackgroundImage(Alloy.Styles.buttonRightOnImage);
    $.one_card_button.setBackgroundImage(Alloy.Styles.buttonLeftOffImage);
    $.payment_message_label.setTop(20);
    usingMultipleCards = true;
  }






  function onOneCardClick() {
    $.tender_amount_container.hide();
    $.tender_amount_container.setHeight(0);
    $.number_entry_container.hide();
    $.number_entry_container.setHeight(0);
    $.just_swipe_container.show();
    $.just_swipe_container.setHeight(Ti.UI.SIZE);
    $.multiple_cards_button.setBackgroundImage(Alloy.Styles.buttonRightOffImage);
    $.one_card_button.setBackgroundImage(Alloy.Styles.buttonLeftOnImage);
    usingMultipleCards = false;
  }






  function onCreditCardClick() {
    logger.info('credit card button tapped');
    setPaymentType(creditCardPaymentType);
  }






  function onGiftCardClick() {
    logger.info('gift card button tapped');
    setPaymentType(giftCardPaymentType);
  }






  function onSwipeCardClick() {
    logger.info('enter button tapped');
    if ($.number_entered.getText().trim() == blankNumberEntered) {
      var value = currentBasket.calculateBalanceDue().toFixed(2);
      value = value.replace('.', dot);
      $.number_entered.setText(value);
    }

    var amount = parseFloat($.number_entered.text).toFixed(2);
    handleSwipeButton(amount);
  }






  function onSwipeNoKeypadClick() {
    handleSwipeButton(currentBasket.calculateBalanceDue().toFixed(2));
  }






  function onDeleteButtonClick() {
    var number = $.number_entered.getText();
    var floatPart = number.substring(number.indexOf(dot) + 1);
    var integerPart = number.substring(0, number.indexOf(dot));

    var newIntegerPart = integerPart.substring(0, integerPart.length - 1);
    if (newIntegerPart.length == 0) {
      newIntegerPart = '0';
    }
    var newFloatPart = integerPart.substring(integerPart.length - 1) + floatPart.substring(0, 1);
    $.number_entered.setText(newIntegerPart + dot + newFloatPart);
  }






  function onNumberButtonClick(event) {

    logger.info('onNumberButtonClick ' + JSON.stringify(event));
    addNumber(event.source.title);
  }







  function ccPaymentEntered(event) {
    logger.secureLog('Attempting to complete order with credit card info: ' + JSON.stringify(event, null, 4));
    var payment = {
      track_1: event.track_1,
      track_2: event.track_2,
      pan: event.pan,
      expire_date: event.month && event.year ? event.month + '/' + event.year : '',
      order_no: currentBasket.getOrderNumber(),
      transaction_id: event.transaction_id,
      payment_reference_id: event.payment_reference_id,
      owner: event.owner,
      card_type: event.card_type,
      is_contactless: event.is_contactless,
      terminal_id: event.terminal_id,
      merchant_reference: event.merchant_reference,
      payment_type: event.payment_type };


    if (Alloy.CFG.payment_process_flow === 'A') {
      var deferred = new _.Deferred();
      Alloy.Router.showActivityIndicator(deferred);
      currentBasket.applyCreditCard(payment).done(function (model, options, params) {
        $.number_entered.setText(blankNumberEntered);

        if (Math.abs(currentBasket.calculateBalanceDue()) < 0.001) {
          authorizePayments(deferred);
        } else {
          deferred.resolve();
        }
        stopPaymentListening();
        Alloy.eventDispatcher.trigger('payment_terminal:dismiss');
      }).fail(function (failedModel, failedOptions, failedParams) {
        logger.error('A flow applyCreditCard failure: ' + handleCardAuthorizationFailure(failedModel));


        Ti.App.fireEvent('payment_terminal:enable_retry', {
          message: _L('Unable to complete order. Retry?'),
          payment_data: event });


        deferred.reject();
      });
    } else if (Alloy.CFG.payment_process_flow === 'B') {

      var amountToApply = currentBasket.calculateBalanceDue();

      if (usingMultipleCards) {

        var amountEntered = $.number_entered.getText();

        if (amountEntered !== blankNumberEntered) {
          var enteredNumber = formatAmount(amountEntered);
          var entered = enteredNumber - 0;
          if (entered < amountToApply) {
            amountToApply = enteredNumber - 0;
          }
        }
      }
      payment.auth_amount = amountToApply;

      if (event.transaction_id) {
        logger.info('IS PAID!');
      }
      var deferred = new _.Deferred();
      Alloy.Router.showActivityIndicator(deferred);
      currentBasket.authorizeCreditCard(payment).done(function (model, options, params) {
        $.number_entered.setText(blankNumberEntered);

        if (currentBasket.isOrderConfirmed()) {
          Alloy.eventDispatcher.trigger('payment_terminal:dismiss');
          $.trigger('payment:success', {
            title: _L('Approved'),
            message: _L('Payment Approved'),
            completionFunction: function () {
              fetchOrder();
            } });

          stopPaymentListening();
          sendAnalyticsEvents();
          deferred.resolve();
        } else if (currentBasket.getPaymentBalance() == 0) {


          deferred.reject();
          Alloy.eventDispatcher.trigger('payment_terminal:dismiss');
          $.trigger('payment:error', {
            title: _L('Payment Status'),
            message: _L('Error applying credit card. \nThe order has not been confirmed on server.') });

          stopPaymentListening();
        } else {
          $.trigger('payment:success', {
            title: _L('Approved'),
            message: _L('Payment Approved') });

          deferred.resolve();
        }
      }).fail(function (failedModel, failedOptions, failedParams) {
        logger.error('B flow authorizeCreditCard failure: ' + handleCardAuthorizationFailure(failedModel));


        Ti.App.fireEvent('payment_terminal:enable_retry', {
          message: _L('Unable to complete order. Retry?'),
          payment_data: event });


        deferred.reject();
      });
    }
  }







  function formatAmount(amountEntered) {
    return amountEntered.substring(0, amountEntered.indexOf(dot)) + '.' + amountEntered.substring(amountEntered.indexOf(dot) + 1, amountEntered.length);
  }







  function handleCardAuthorizationFailure(failedModel) {
    var alertMessage = _L('Error applying credit card. \nPlease try again.');
    if (failedModel && failedModel.has('fault') && failedModel.get('fault')) {
      var fault = failedModel.get('fault');
      if (fault) {
        if (fault.message && fault.message != '') {
          alertMessage = fault.message;
        } else if (fault.decision && fault.decision == 'REJECT') {
          alertMessage = _L('Card was declined. Please try another card.');
        }

        if (fault.description && fault.description != '') {
          alertMessage += '\n' + fault.description;
        }
        if (fault.reasonCode && fault.reasonCode != '') {
          alertMessage += ' (' + _L('Reason Code: ') + fault.reasonCode + ')';
        }
      }
    }

    return alertMessage;
  }







  function ccPaymentApproved(event) {
    logger.secureLog('event data ' + JSON.stringify(event, null, 4));
    return ccPaymentEntered(event);
  }







  function ccPaymentDeclined(event) {
    var message = event.description;
    if (!message || _.isEmpty(message)) {

      message = _L('Card was declined. Please try another card.');
    }
    Alloy.Dialog.showAlertDialog({
      messageString: message,
      titleString: _L('Payment Declined') });

  }







  function ccPaymentError(event) {
    Alloy.Dialog.showAlertDialog({
      messageString: event.message,
      titleString: _L('Payment Error') });

  }







  function swipeCreditCard(options) {
    startPaymentListening();

    if (Alloy.CFG.allow_simulate_payment && Alloy.CFG.devices.payment_terminal_module != 'adyenDevice') {
      Alloy.Dialog.showCustomDialog({
        controllerPath: 'checkout/payments/simulatePayment',
        initOptions: {
          event_type: 'payment:credit_card_data',
          button_data: [{
            track_1: '%B4111111111111111^LEBOWSKI/JEFF ^1812101000000000011111111000000?',
            track_2: ';4111111111111111=181210111111111?',
            is_contactless: false,
            title: 'Simulate Visa' },
          {
            track_1: '%5555555555554444^SOZE/KEYSER ^1812101000000000011111111000000?',
            track_2: ';5555555555554444=181210111111111?',
            is_contactless: false,
            title: 'Simulate Mastercard' },
          {
            track_1: '%378282246310005^SOZE/KEYSER ^1812101000000000011111111000000?',
            track_2: ';378282246310005=181210111111111?',
            is_contactless: false,
            title: 'Simulate AMEX' },
          {
            track_1: '%6011822463100051^SOZE/KEYSER ^1812101000000000011111111000000?',
            track_2: ';6011822463100051=181210111111111?',
            is_contactless: false,
            title: 'Simulate Discover' },
          {
            track_1: '%30569309025904^SOZE/KEYSER ^1812101000000000011111111000000?',
            track_2: ';30569309025904=181210111111111?',
            is_contactless: false,
            title: 'Simulate Diners Club' },
          {
            track_1: '%3528000700000000^SOZE/KEYSER ^1812101000000000011111111000000?',
            track_2: ';3528000700000000=181210111111111?',
            is_contactless: false,
            title: 'Simulate JCB' },
          {
            track_1: '%6759649826438453^SOZE/KEYSER ^1812101000000000011111111000000?',
            track_2: ';6759649826438453=181210111111111?',
            is_contactless: false,
            title: 'Simulate Maestro' },
          {
            track_1: '%630495060000000000^SOZE/KEYSER ^1812101000000000011111111000000?',
            track_2: ';630495060000000000=181210111111111?',
            is_contactless: false,
            title: 'Simulate Laser' },
          {
            track_1: '%6334960300099354^SOZE/KEYSER ^1812101000000000011111111000000?',
            track_2: ';6334960300099354=181210111111111?',
            is_contactless: false,
            title: 'Simulate Solo' },
          {
            track_1: '%6395129637937969^SOZE/KEYSER ^1812101000000000011111111000000?',
            track_2: ';6395129637937969=181210111111111?',
            is_contactless: false,
            title: 'Simulate Instapayment' },
          {
            track_1: '%6240008631401148^SOZE/KEYSER ^1812101000000000011111111000000?',
            track_2: ';6240008631401148=181210111111111?',
            is_contactless: false,
            title: 'Simulate China Unionpay' }] },


        cancelEvent: 'simulate_payment:dismiss',
        continueEvent: 'simulate_payment:swipe',
        continueFunction: function () {
          $.trigger('payment:use_terminal', options);
        } });

    } else if (Alloy.CFG.allow_simulate_payment && Alloy.CFG.devices.payment_terminal_module == 'adyenDevice') {
      Alloy.Dialog.showCustomDialog({
        controllerPath: 'checkout/payments/simulatePayment',
        initOptions: {
          event_type: 'payment:cc_approved',
          button_data: [{
            owner: 'TC04_MC_Approved_DCC',
            card_type: 'Mastercard',
            pan: 'xxxx-xxxx-xxxx-9990',
            month: '02',
            year: '2028',
            transaction_id: '8814782013951911',
            payment_reference_id: '123456',
            is_contactless: false,
            terminal_id: 'E355-400163777',
            tender_reference: 'zDNA001162668548081',
            final_state: 47,
            final_state_string: 'APPROVED',
            title: 'Simulate Adyen (Mastercard)' }] },


        cancelEvent: 'simulate_payment:dismiss',
        continueEvent: 'simulate_payment:swipe',
        continueFunction: function () {
          $.trigger('payment:use_terminal', options);
        } });

    } else {
      logger.info('trigger use payment terminal event');
      $.trigger('payment:use_terminal', options);
    }
  }







  function gcPaymentEntered(event) {
    stopPaymentListening();
    var payment = {
      track_1: event.track_1,
      track_2: event.track_2,
      order_no: currentBasket.getOrderNumber() };

    var checkBalance = currentBasket.giftCardBalance(payment);
    Alloy.Router.showActivityIndicator(checkBalance);

    checkBalance.done(function (model, options, params) {
      var balanceOnCard = model.getBalance();
      var amountToApply = 0;
      var remainingBalance;
      if (balanceOnCard != 0) {





        var amountEntered = $.number_entered.getText();
        var amountToApply = currentBasket.calculateBalanceDue();
        if (usingMultipleCards) {

          if (amountEntered !== blankNumberEntered) {
            var enteredNumber = formatAmount(amountEntered);
            var entered = enteredNumber - 0;
            if (entered < amountToApply) {
              amountToApply = enteredNumber - 0;
            }
          }
          var toApply = 0;
          if (amountEntered !== blankNumberEntered && amountToApply > balanceOnCard) {
            amountToApply = balanceOnCard;
          } else if (amountToApply > balanceOnCard) {


            amountToApply = balanceOnCard;
          }
        }
      }
      var lastFour = model.getMaskedCode();
      lastFour = lastFour.substr(lastFour.length - 4);
      remainingBalance = balanceOnCard - amountToApply;

      $.trigger('payment:gc_balance', {
        message: _L('Details for gift card ending in:') + ' ' + lastFour,
        toApply: amountToApply,
        currentBalance: balanceOnCard,
        remainingBalance: remainingBalance,
        track_1: event.track_1,
        track_2: event.track_2,
        order_no: currentBasket.getOrderNumber() });

    }).fail(function (failModel, failOptions, failParams) {
      $.trigger('payment:error', {
        title: _L('Payment Status'),
        message: _L('The Gift Card is not recognized by the gift card processor. Please try again or choose a different payment method.') });

    });
  }







  function swipeGiftCard(options) {

    if (!listeningGC) {
      $.listenTo(Alloy.eventDispatcher, 'payment:gift_card_data', gcPaymentEntered);
      listeningGC = true;
    }

    if (Alloy.CFG.allow_simulate_payment) {
      Alloy.Dialog.showCustomDialog({
        controllerPath: 'checkout/payments/simulatePayment',
        initOptions: {
          event_type: 'payment:gift_card_data',
          button_data: [{
            track_1: '%B89897686443^YOU/A GIFT FOR            ^1905122100606873?',
            track_2: ';89897686443=190512210060687300000?',
            title: 'Simulate 200k' },
          {
            track_1: '%B45198732542^YOU/A GIFT FOR            ^1905122100606873?',
            track_2: ';45198732542=190512210060687300000?',
            title: 'Simulate 0' },
          {
            track_1: '%B77812450912^YOU/A GIFT FOR            ^1905122100606873?',
            track_2: ';7781240912=190512210060687300000?',
            title: 'Simulate bad card' }] },


        cancelEvent: 'simulate_payment:dismiss',
        continueEvent: 'simulate_payment:swipe',
        continueFunction: function () {
          $.trigger('payment:use_terminal', options);
        } });

    } else {

      $.trigger('payment:use_terminal', options);
    }
  }







  function authorizePayments(deferred) {
    currentBasket.authorizePayment().done(function () {

      deferred.resolve();
      $.trigger('payment:success', {
        title: _L('Approved'),
        message: _L('Payment Approved'),
        completionFunction: function () {
          fetchOrder();
        } });

    }).fail(function () {
      deferred.reject();
      notify(_L('Payments could not be authorized'), {
        preventAutoClose: true });

    });
  }







  function fetchOrder(deferred) {
    var order_no = currentBasket.getOrderNumber();
    var b = currentBasket.clone();

    if (!deferred) {
      deferred = new _.Deferred();
      Alloy.Router.showActivityIndicator(deferred);
    }
    currentBasket.clear({
      silent: true });


    currentBasket.getBasket({
      c_eaEmployeeId: Alloy.Models.associate.getEmployeeId() }).
    done(function () {
      if (currentCustomer.isLoggedIn()) {
        currentCustomer.claimBasket(currentBasket).done(function () {
          finishFetchOrder(order_no, deferred);
        }).fail(function () {
          deferred.reject();
        });
      } else {
        finishFetchOrder(order_no, deferred);
      }
    }).fail(function () {
      deferred.reject();
    });
  }








  function finishFetchOrder(order_no, deferred) {
    currentBasket.trigger('change');
    currentBasket.trigger('basket_sync');

    var customerOrder = Alloy.createModel('baskets');
    customerOrder.getOrder({
      order_no: order_no }).
    done(function () {

      Alloy.Models.order = this;
      Alloy.eventDispatcher.trigger('order_just_created', this);
      deferred.resolve();
    }).fail(function () {
      deferred.reject();
    });
  }








  function selectButton(button, selected) {
    if (selected) {
      button.setBackgroundImage(selectedButtonBackground);
      button.setColor(selectedColor);
    } else {
      button.setBackgroundImage(deselectedButtonBackground);
      button.setColor(deselectedColor);
    }
  }






  function calculateBalanceDue() {
    var balance = currentBasket.calculateBalanceDue();
    $.balance_due.setText(toCurrency(balance));
    $.credit_card_button.setEnabled(balance != 0);
    $.gift_card_button.setEnabled(balance != 0);

    Alloy.eventDispatcher.trigger('payment_terminal:dismiss');
    return balance;
  }






  function canAddNumber() {
    if (!Alloy.CFG.enable_multi_tender_payments) {
      return false;
    }
    var number = $.number_entered.getText();
    return number.indexOf('.') < 0 || number.indexOf('.') >= number.length - 2;
  }






  function cancelOrder() {
    logger.info('cancel order button tapped');

    stopPaymentListening();

    var orderNo = currentBasket.getOrderNo();



    setTimeout(function () {
      logger.info('payments/index(cancelOrder): cancelServerTransaction for order: ' + orderNo);
      paymentTerminal.cancelServerTransaction({
        order_no: orderNo });

    }, 5000);

    var promise = currentBasket.abandonOrder(Alloy.Models.associate.getEmployeeId(), Alloy.Models.associate.getPasscode(), Alloy.CFG.store_id);
    Alloy.Router.showActivityIndicator(promise);
    promise.done(function () {

      selectButton($.credit_card_button, false);
      selectButton($.gift_card_button, false);
      $.number_entered.setText('');
      $.trigger('orderAbandoned', {});
    }).fail(function () {
      Alloy.Dialog.showConfirmationDialog({
        messageString: _L('The order needs to be cancelled before you can continue. Please try again.'),
        titleString: _L('Unable to cancel order.'),
        okButtonString: _L('Retry'),
        cancelButtonString: _L('Cancel'),
        hideCancel: false,
        okFunction: function () {
          removeNotify();
          cancelOrder();
        },
        cancelFunction: function () {
          removeNotify();
        } });

    });
  }







  function handleSwipeButton(amount) {
    logger.info('enter button tapped');
    if (paymentMethodType === creditCardPaymentType) {
      swipeCreditCard({
        amount: amount });

    } else if (paymentMethodType === giftCardPaymentType) {
      swipeGiftCard({
        amount: amount });

    }
  }







  function setPaymentType(type) {
    if (type === creditCardPaymentType) {
      selectButton($.credit_card_button, true);
      selectButton($.gift_card_button, false);
    } else if (type == giftCardPaymentType) {
      selectButton($.credit_card_button, false);
      selectButton($.gift_card_button, true);
    }
    paymentMethodType = type;
  }






  function sendAnalyticsEvents() {
    analytics.fireTransactionEvent({
      orderId: currentBasket.getOrderNo(),
      storeId: Alloy.Models.storeInfo.getId(),
      total: currentBasket.getOrderTotal(),
      tax: currentBasket.getTaxTotal(),
      shipping: currentBasket.getShippingTotal(),
      currency: currentBasket.getCurrency() });


    var products = currentBasket.getProductItems();
    if (!products) {
      return;
    }

    _.each(products, function (product) {
      analytics.fireTransactionItemEvent({
        orderId: currentBasket.getOrderNo(),
        name: product.getProductName(),
        productId: product.getProductId(),
        price: product.getPrice(),
        quantity: product.getQuantity(),
        currency: currentBasket.getCurrency() });

    });
  }









  _.extend($, exports);
}

module.exports = Controller;