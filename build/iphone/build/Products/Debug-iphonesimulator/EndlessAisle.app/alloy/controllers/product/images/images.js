var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'product/images/images';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.images = Ti.UI.createView(
  { layout: "vertical", height: "100%", width: "100%", id: "images" });

  $.__views.images && $.addTopLevelView($.__views.images);
  var __alloyId213 = [];
  $.__views.pdp_image_scroller = Ti.UI.createScrollableView(
  { height: "100%", width: "100%", views: __alloyId213, id: "pdp_image_scroller" });

  $.__views.images.add($.__views.pdp_image_scroller);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var eaUtils = require('EAUtils');

  var args = arguments[0] || {},
  COLOR_CONTAINER_ID_PREFIX = 'color_container_',
  $model = $model || args.product,
  product_id = args.product_id,
  loading = false,
  imageContainers = [],
  smallImagesViews = [],
  smallImagesControllers = [];

  var logger = require('logging')('product:images:images', getFullControllerPath($.__controllerPath));


  var currentProduct = $model || Alloy.Models.product;




  $.images.addEventListener('doubletap', imagesDoubleTapEventHandler);
  $.pdp_image_scroller.addEventListener('scrollend', pdpImageScrollerScrollendEventHandler);




  $.listenTo(currentProduct, 'change:variation_values', function (model, change, options) {
    if (change && change[Alloy.CFG.product.color_attribute]) {
      resetVariationSelection(currentProduct.getVariationValues());
    }
  });

  $.listenTo(currentProduct, 'change:id', render);





  exports.init = init;
  exports.deinit = deinit;
  exports.resetVariationSelection = resetVariationSelection;









  function init() {
    imageContainers = [];
    if (product_id && !$model) {

      currentProduct = Alloy.createModel('product');
      currentProduct.id = product_id;
      currentProduct.ensureImagesLoaded('heroImage').done(function () {
        render();
      });
    } else {

      currentProduct.ensureImagesLoaded('heroImage').done(function () {
        render();
      }).fail(function () {
        logger.error('cannot get images!');
      });
    }
  }






  function render() {
    logger.info('render start');

    var imageViews = $.pdp_image_scroller.views || [];

    for (var i = 0, ii = imageViews.len; i < ii; i++) {
      $.pdp_image_scroller.removeView(imageViews[i]);
    }
    if (currentProduct.isBundle()) {


      renderBundleOrSet(currentProduct.getBundledProducts());
    } else if (currentProduct.isSet()) {
      renderBundleOrSet(currentProduct.getSetProducts());
    } else {
      renderProduct();
    }

    logger.info('render end');
  }






  function deinit() {
    logger.info('deinit called');
    $.images.removeEventListener('doubletap', imagesDoubleTapEventHandler);
    $.pdp_image_scroller.removeEventListener('scrollend', pdpImageScrollerScrollendEventHandler);
    _.each(smallImagesViews, function (view) {
      view.removeEventListener('alt_image_selected', altImageSelectedEventHandler);
    });
    _.each(smallImagesControllers, function (controller) {
      controller.deinit();
    });
    _.each(imageContainers, function (controller, key) {
      controller.deinit();
    });
    smallImagesViews = null;
    smallImagesControllers = null;
    imageContainers = null;
    removeAllChildren($.pdp_image_scroller);
    $.stopListening();
    $.destroy();
  }










  function resetVariationSelection(event) {
    logger.info('resetVariationSelection start');


    var selectedAttributes = currentProduct.getVariationValues(),
    aid;

    for (aid in selectedAttributes) {
      if (aid === Alloy.CFG.product.color_attribute) {
        var viewId = COLOR_CONTAINER_ID_PREFIX + event[aid];
        var imageView = $.pdp_image_scroller[viewId];
        if (imageView) {
          $.pdp_image_scroller.scrollToView(imageView);
        }
      }
    }

    logger.info('resetVariationSelection end');
  }






  function renderProduct() {
    logger.info('renderProduct start');
    var imageGroups = currentProduct.getHeroImageGroups();
    var selectionVariationValue = null;
    if (currentProduct.getSelectedVariant()) {
      selectionVariationValue = currentProduct.getSelectedVariant().getVariationValues();
    }
    var altImagesInfo = [],
    imageViews = [],
    selectedVariantPosition = 0,
    isSelected = false;

    currentProduct.ensureImagesLoaded('altImages').done(function () {
      var imageGroup,
      imageGroupImages,
      image,
      imageContainer,
      variationValue,
      valuePrefix,
      altContainer,
      altImages,
      commonNumber,
      smallImage,
      counter = -1,
      imageGroupsLength = imageGroups ? imageGroups.length : 0;

      if (imageGroupsLength == 0) {
        logger.error('product/components/images: heroImage group empty for product id ' + currentProduct.getId());
      }
      for (var i = 0, ii = imageGroupsLength; i < ii; i++) {

        imageGroup = imageGroups[i];
        variationValue = determineImageVariationValue(imageGroup);


        if (selectionVariationValue) {
          isSelected = isSelectedVariantImageGroup(imageGroup, selectionVariationValue);
          if (isSelected) {
            selectedVariantPosition = i - 1;
          }
        }

        valuePrefix = variationValue || 'default';


        if (ii > 1 && !variationValue) {
          continue;
        }
        counter++;
        imageGroupImages = imageGroup.getImages();
        image = imageGroupImages[0].getLink();

        imageContainer = Alloy.createController('product/images/imageContainer');

        imageContainer.init(valuePrefix, image);
        imageContainer.number = counter;

        altContainer = imageContainer.getAltContainer();

        altImages = currentProduct.getAltImages(valuePrefix);
        commonNumber = imageGroupImages.length > altImages.length ? altImages.length : imageGroupImages.length;

        for (var j = 0; j < commonNumber; j++) {
          smallImage = Alloy.createController('product/images/alternateImage');
          smallImagesControllers.push(smallImage);

          smallImage.init({
            largeImageView: imageContainer.getLargeImageView(),
            largeImage: imageGroupImages[j].getLink(),
            image: altImages[j].getLink(),
            altImageNumber: j,
            imageContainerNumber: imageContainer.number });

          altContainer.add(smallImage.getView());

          smallImagesViews.push(smallImage.getView());
        }

        addVideos({
          videoURL: 'http://assets.appcelerator.com.s3.amazonaws.com/video/media.m4v',
          imageContainerNumber: imageContainer.number,
          altImageNumber: commonNumber,
          videoPlayer: imageContainer.getVideoPlayer(),
          altContainer: altContainer });



        _.each(smallImagesViews, function (view) {
          view.addEventListener('alt_image_selected', altImageSelectedEventHandler);
        });

        imageViews.push(imageContainer.getView());
        $.pdp_image_scroller[COLOR_CONTAINER_ID_PREFIX + valuePrefix] = imageContainer.getColorContainer();
        imageContainers[counter] = imageContainer;
        imageContainer.selectedImage = 0;
      }
      swapArrayElements(imageViews, 0, selectedVariantPosition);
      $.pdp_image_scroller.setViews(imageViews);
    });

    logger.info('renderProduct end');
  }








  function swapArrayElements(arr, indexA, indexB) {
    var temp = arr[indexA];
    arr[indexA] = arr[indexB];
    arr[indexB] = temp;
  }








  function isSelectedVariantImageGroup(imageGroup, selectionVariationValue) {
    var isSelectedVariantImageGroup = false;
    var variationAttribute = null;
    var variationValue = null;
    var selValue = null;
    if (imageGroup && imageGroup.getVariationAttributes) {
      var variationAttributes = imageGroup.getVariationAttributes();
      for (var i = 0, ii = variationAttributes.length; i < ii; i++) {
        variationAttribute = variationAttributes[i];
        variationValue = variationAttribute.getValues()[0].getValue();
        selValue = selectionVariationValue[variationAttribute.id];
        if (variationValue === selValue) {
          isSelectedVariantImageGroup = true;
        }
      }
    }
    return isSelectedVariantImageGroup;
  }







  function renderBundleOrSet(products) {
    logger.info('renderBundleOrSet start');

    var largeImageGroups = [],
    smallImageGroups = [],
    altImages = [],
    imageProduct = currentProduct,
    imagePromises = [];


    _.each(products, function (product) {
      var deferred = new _.Deferred();
      product.ensureImagesLoaded('heroImage').always(function () {
        product.ensureImagesLoaded('altImages').always(function () {
          deferred.resolve();
        });
      });
      imagePromises.push(deferred);
    });

    _.when(imagePromises).done(function () {

      var imageGroups = products[0].getHeroImageGroups();
      var imageGroup = imageGroups[0];
      if (!imageGroup) {
        logger.error('product/components/images: heroImage group empty for products of bundle or set ' + currentProduct.getId());
        return;
      }
      var imageGroupImages = imageGroup.getImages();
      var valuePrefix = determineImageVariationValue(imageGroup) || 'default';


      var imageContainer = Alloy.createController('product/images/imageContainer');
      imageContainer.init(valuePrefix, imageGroupImages[0].getLink());

      var altContainer = imageContainer.getAltContainer();



      _.each(products, function (product, index) {
        var imageGroups = product.getHeroImageGroups();
        var imageGroup = imageGroups[0];
        imageGroupImages = imageGroup.getImages();

        var altImages = product.getAltImages(valuePrefix);

        var altImageLink = altImages && altImages.length > 0 ? altImages[0].getLink() : Alloy.Styles.imageNotAvailableImage;
        var smallImage = Alloy.createController('product/images/alternateImage');
        smallImagesControllers.push(smallImage);
        smallImage.init({
          largeImageView: imageContainer.getLargeImageView(),
          largeImage: imageGroupImages[0].getLink(),
          image: altImageLink,
          altImageNumber: 0,
          imageContainerNumber: index,
          productNumber: index });

        altContainer.add(smallImage.getView());

        smallImagesViews.push(smallImage.getView());
      });
      addVideos({
        videoURL: 'http://assets.appcelerator.com.s3.amazonaws.com/video/media.m4v',
        imageContainerNumber: 0,
        altImageNumber: largeImageGroups.length,
        videoPlayer: imageContainer.getVideoPlayer(),
        altContainer: altContainer });

      altImageSelectedEventHandler = function (event) {

        if (event.video) {

          imageContainer.showVideo();
        } else {
          event.productId = products[0].getId();
          $.trigger('alt_image_selected', event);
          imageContainer.stopVideoAndShowImage();

        }
      };
      _.each(smallImagesViews, function (view) {
        view.addEventListener('alt_image_selected', altImageSelectedEventHandler);
      });

      $.pdp_image_scroller.addView(imageContainer.getView());
      $.pdp_image_scroller[COLOR_CONTAINER_ID_PREFIX + valuePrefix] = imageContainer.getColorContainer();

      imageContainers[0] = imageContainer;
      imageContainer.selectedImage = 0;
    });

    logger.info('renderBundleOrSet end');
  }







  function determineImageVariationValue(imageGroup) {
    var variationValue = null;
    if (imageGroup && imageGroup.getVariationAttributes) {
      var varAttributes = imageGroup.getVariationAttributes()[0];
      if (varAttributes) {
        variationValue = varAttributes.getValues()[0].getValue();
      }
    }
    return variationValue;
  }










  function imagesDoubleTapEventHandler(event) {
    event.cancelBubble = true;
    $.images.fireEvent('product:zoom', {
      product_id: product_id });

  }







  function pdpImageScrollerScrollendEventHandler(event) {
    _.each(imageContainers, function (controller) {
      controller.stopVideo();
    });
    if (event.view && event.view.id && event.view.id.indexOf(COLOR_CONTAINER_ID_PREFIX) == 0) {
      var variationValue = event.view.id.substring(COLOR_CONTAINER_ID_PREFIX.length);

      var pair = {},
      variationValues = currentProduct.getVariationValues();
      pair[Alloy.CFG.product.color_attribute] = variationValue;

      logger.info('updating variation_values with new selection: ' + JSON.stringify(pair));
      var selectedVariationValues = _.extend({}, variationValues, pair);
      currentProduct.setVariationValues(selectedVariationValues);
      $.trigger('alt_image_selected', {
        image: imageContainers[event.currentPage].selectedImage });

    }
  }







  function altImageSelectedEventHandler(event) {

    if (event.video) {

      imageContainers[event.imageContainerNumber].showVideo();
    } else {
      $.trigger('alt_image_selected', event);
      imageContainers[event.imageContainerNumber].selectedImage = event.image;
      imageContainers[event.imageContainerNumber].stopVideoAndShowImage();

    }
  }











  function addVideos(args) {

    if (Alloy.CFG.product.enable_video_player) {


      var vidImage = Alloy.createController('product/images/alternateVideo');

      smallImagesControllers.push(vidImage);


      vidImage.init({
        videoURL: args.videoURL,
        videoPlayer: args.videoPlayer,
        altImageNumber: args.altImageNumber,
        imageContainerNumber: args.imageContainerNumber });

      args.altContainer.add(vidImage.getView());


      smallImagesViews.push(vidImage.getView());

    }
  }













  _.extend($, exports);
}

module.exports = Controller;