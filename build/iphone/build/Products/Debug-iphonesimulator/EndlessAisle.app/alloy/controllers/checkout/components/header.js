var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'checkout/components/header';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.checkout_header = Ti.UI.createView(
  { layout: "horizontal", height: 40, top: 0, left: 0, backgroundColor: Alloy.Styles.color.background.white, id: "checkout_header" });

  $.__views.checkout_header && $.addTopLevelView($.__views.checkout_header);
  $.__views.button_container = Ti.UI.createView(
  { layout: "horizontal", width: "100%", left: 0, id: "button_container" });

  $.__views.checkout_header.add($.__views.button_container);
  $.__views.back_button = Ti.UI.createButton(
  { top: 0, width: 40, height: 40, style: Ti.UI.iOS.SystemButtonStyle.PLAIN, image: Alloy.Styles.backButtonImage, borderWidth: 1, borderColor: Alloy.Styles.color.border.medium, id: "back_button", accessibilityValue: "back_button" });

  $.__views.button_container.add($.__views.back_button);
  $.__views.checkout_cart_button = Ti.UI.createButton(
  { top: 0, width: 130, height: 40, color: Alloy.Styles.color.text.dark, selectedColor: Alloy.Styles.color.text.dark, disabledColor: Alloy.Styles.color.text.light, font: Alloy.Styles.detailLabelFont, style: Ti.UI.iOS.SystemButtonStyle.PLAIN, backgroundColor: Alloy.Styles.color.background.medium, borderWidth: 1, borderColor: Alloy.Styles.color.border.medium, titleid: "_Cart_Tab", id: "checkout_cart_button", accessibilityValue: "checkout_cart_button" });

  $.__views.button_container.add($.__views.checkout_cart_button);
  $.__views.checkout_address_button = Ti.UI.createButton(
  { top: 0, width: 130, height: 40, color: Alloy.Styles.color.text.dark, selectedColor: Alloy.Styles.color.text.dark, disabledColor: Alloy.Styles.color.text.light, font: Alloy.Styles.detailLabelFont, style: Ti.UI.iOS.SystemButtonStyle.PLAIN, backgroundColor: Alloy.Styles.color.background.medium, borderWidth: 1, borderColor: Alloy.Styles.color.border.medium, titleid: "_Shipping", id: "checkout_address_button", accessibilityValue: "checkout_address_button" });

  $.__views.button_container.add($.__views.checkout_address_button);
  $.__views.checkout_billing_address_button = Ti.UI.createButton(
  { top: 0, width: 130, height: 40, color: Alloy.Styles.color.text.dark, selectedColor: Alloy.Styles.color.text.dark, disabledColor: Alloy.Styles.color.text.light, font: Alloy.Styles.detailLabelFont, style: Ti.UI.iOS.SystemButtonStyle.PLAIN, backgroundColor: Alloy.Styles.color.background.medium, borderWidth: 1, borderColor: Alloy.Styles.color.border.medium, titleid: "_Billing", id: "checkout_billing_address_button", accessibilityValue: "checkout_billing_address_button" });

  $.__views.button_container.add($.__views.checkout_billing_address_button);
  $.__views.checkout_shipping_methods_button = Ti.UI.createButton(
  { top: 0, width: 130, height: 40, color: Alloy.Styles.color.text.dark, selectedColor: Alloy.Styles.color.text.dark, disabledColor: Alloy.Styles.color.text.light, font: Alloy.Styles.detailLabelFont, style: Ti.UI.iOS.SystemButtonStyle.PLAIN, backgroundColor: Alloy.Styles.color.background.medium, borderWidth: 1, borderColor: Alloy.Styles.color.border.medium, titleid: "_Ship_Methods", id: "checkout_shipping_methods_button", accessibilityValue: "checkout_shipping_methods_button" });

  $.__views.button_container.add($.__views.checkout_shipping_methods_button);
  $.__views.checkout_payments_button = Ti.UI.createButton(
  { top: 0, width: 130, height: 40, color: Alloy.Styles.color.text.dark, selectedColor: Alloy.Styles.color.text.dark, disabledColor: Alloy.Styles.color.text.light, font: Alloy.Styles.detailLabelFont, style: Ti.UI.iOS.SystemButtonStyle.PLAIN, backgroundColor: Alloy.Styles.color.background.medium, borderWidth: 1, borderColor: Alloy.Styles.color.border.medium, titleid: "_Payments", id: "checkout_payments_button", accessibilityValue: "checkout_payments_button" });

  $.__views.button_container.add($.__views.checkout_payments_button);
  $.__views.checkout_confirmation_button = Ti.UI.createButton(
  { top: 0, width: 130, height: 40, color: Alloy.Styles.color.text.dark, selectedColor: Alloy.Styles.color.text.dark, disabledColor: Alloy.Styles.color.text.light, font: Alloy.Styles.detailLabelFont, style: Ti.UI.iOS.SystemButtonStyle.PLAIN, backgroundColor: Alloy.Styles.color.background.medium, borderWidth: 1, borderColor: Alloy.Styles.color.border.medium, titleid: "_Confirmation", id: "checkout_confirmation_button", accessibilityValue: "checkout_confirmation_button" });

  $.__views.button_container.add($.__views.checkout_confirmation_button);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var currentBasket = Alloy.Models.basket;
  var currentCustomer = Alloy.Models.customer;
  var returnToShopping = require('EAUtils').returnToShopping;

  var inactiveBackgroundColor = Alloy.Styles.tabs.backgroundColorDisabled;
  var activeBackgroundColor = Alloy.Styles.tabs.backgroundColorEnabled;
  var inactiveBorderColor = Alloy.Styles.color.border.darker;
  var EAUtils = require('EAUtils');

  var differentStoreSelectedForPickup = false;


  var tabTextLength = 13;






  $.listenTo(Alloy.eventDispatcher, 'payments_active', function (event) {
    $.back_button.setEnabled(!event.payments_active);
  });


  $.listenTo(Alloy.eventDispatcher, 'order_just_created', function () {
    $.back_button.setEnabled(true);
  });


  $.listenTo(Alloy.eventDispatcher, 'configurations:postlogin', function () {
    var buttonWidth;
    if (Alloy.CFG.payment_entry === 'web') {
      buttonWidth = 130;
      $.checkout_billing_address_button.setWidth(buttonWidth);
      $.checkout_billing_address_button.setVisible(true);
      $.checkout_address_button.setTitle(_L('Shipping'));
      $.checkout_shipping_methods_button.setTitle(_L('Ship Methods'));
      $.checkout_payments_button.setVisible(false);
      $.checkout_payments_button.setWidth(0);
    } else if (Alloy.CFG.payment_entry === 'default') {
      buttonWidth = Alloy.CFG.collect_billing_address ? 108 : 130;
      if (Alloy.CFG.collect_billing_address) {
        $.checkout_billing_address_button.setWidth(buttonWidth);
        $.checkout_billing_address_button.setVisible(true);
        $.checkout_address_button.setTitle(_L('Shipping'));
        $.checkout_shipping_methods_button.setTitle(_L('Ship Methods'));
        $.checkout_shipping_methods_button.setWidth(110);
      } else {
        $.checkout_billing_address_button.setWidth(0);
        $.checkout_billing_address_button.setVisible(false);
        $.checkout_address_button.setTitle(_L('Address'));
        $.checkout_shipping_methods_button.setTitle(_L('Shipping'));
        $.checkout_shipping_methods_button.setWidth(buttonWidth);
      }
      $.checkout_payments_button.setWidth(buttonWidth);
    }

    $.checkout_cart_button.setWidth(buttonWidth);
    $.checkout_address_button.setWidth(buttonWidth);
    $.checkout_confirmation_button.setWidth(buttonWidth);

    if ($.checkout_cart_button.getTitle().length > tabTextLength || $.checkout_address_button.getTitle().length > tabTextLength || $.checkout_billing_address_button.getTitle().length > tabTextLength || $.checkout_shipping_methods_button.getTitle().length > tabTextLength || $.checkout_payments_button.getTitle().length > tabTextLength || $.checkout_confirmation_button.getTitle().length > tabTextLength) {
      var tabFont = Alloy.Styles.detailLabelFontSmall;
      if (EAUtils.isLatinBasedLanguage()) {
        tabFont = Alloy.Styles.lineItemLabelFontSmallest;
      }
      $.checkout_cart_button.setFont(tabFont);
      $.checkout_address_button.setFont(tabFont);
      $.checkout_billing_address_button.setFont(tabFont);
      $.checkout_shipping_methods_button.setFont(tabFont);
      $.checkout_payments_button.setFont(tabFont);
      $.checkout_confirmation_button.setFont(tabFont);
    }
  });




  $.listenTo(Alloy.eventDispatcher, 'shippingOption:differentStorePickupAddress', differentStorePickupSelected);
  $.listenTo(Alloy.eventDispatcher, 'shippingOption:shipToCurrentStore', addressOrCurrentStorePickupSelected);
  $.listenTo(Alloy.eventDispatcher, 'shippingOption:shipToAddress', addressOrCurrentStorePickupSelected);
  $.listenTo(Alloy.eventDispatcher, 'differentStorePickupAddress:reconfirmAddress', onBasketChangeWhileDifferentStoreShippingAddressSet);
  $.listenTo(Alloy.eventDispatcher, 'cart_cleared', updateTabStates);





  $.checkout_cart_button.addEventListener('click', onCheckoutCartClick);

  $.checkout_address_button.addEventListener('click', onAddressClick);

  $.checkout_billing_address_button.addEventListener('click', onBillingClick);

  $.checkout_shipping_methods_button.addEventListener('click', onShippingClick);

  $.checkout_payments_button.addEventListener('click', onPaymentsClick);

  $.checkout_confirmation_button.addEventListener('click', onConfirmationClick);

  $.back_button.addEventListener('click', returnToShopping);





  $.listenTo(currentBasket, 'change:product_items reset:product_items change:shipments reset:shipments change:enable_checkout saved_products:downloaded', function () {
    updateTabStates();
  });


  $.listenTo(currentCustomer, 'change', function () {
    updateTabStates();
  });




  exports.deinit = deinit;
  exports.updateTabStates = updateTabStates;









  function deinit() {
    logger.info('DEINIT called');

    $.stopListening();
    $.checkout_cart_button.removeEventListener('click', onCheckoutCartClick);
    $.checkout_address_button.removeEventListener('click', onAddressClick);
    $.checkout_billing_address_button.removeEventListener('click', onBillingClick);
    $.checkout_shipping_methods_button.removeEventListener('click', onShippingClick);
    $.checkout_payments_button.removeEventListener('click', onPaymentsClick);
    $.checkout_confirmation_button.removeEventListener('click', onConfirmationClick);
    $.back_button.removeEventListener('click', returnToShopping);
    $.destroy();
  }









  function canCheckout() {
    return currentBasket.getProductItems().length > 0 && currentBasket.canEnableCheckout();
  }







  function setTabsEnabledSelected(tabs) {

    _.each(tabs, function (tab) {
      if (!tab.getEnabled()) {
        tab.setEnabled(true);
      }
    });
    _.each(tabs, function (tab) {
      if (tab.getBackgroundColor() != activeBackgroundColor) {
        tab.setBackgroundColor(activeBackgroundColor);
      }
    });
    _.each(tabs, function (tab) {
      if (tab.getBorderColor() != activeBackgroundColor) {
        tab.setBorderColor(activeBackgroundColor);
      }
    });
  }







  function setTabsEnabledUnselected(tabs) {

    _.each(tabs, function (tab) {
      if (!tab.getEnabled()) {
        tab.setEnabled(true);
      }
    });
    _.each(tabs, function (tab) {
      if (tab.getBackgroundColor() != inactiveBackgroundColor) {
        tab.setBackgroundColor(inactiveBackgroundColor);
      }
    });
    _.each(tabs, function (tab) {
      if (tab.getBorderColor() != inactiveBorderColor) {
        tab.setBorderColor(inactiveBorderColor);
      }
    });
  }







  function setTabsDisabledUnselected(tabs) {

    _.each(tabs, function (tab) {
      if (tab.getEnabled()) {
        tab.setEnabled(false);
      }
    });
    _.each(tabs, function (tab) {
      if (tab.getBackgroundColor() != inactiveBackgroundColor) {
        tab.setBackgroundColor(inactiveBackgroundColor);
      }
    });
    _.each(tabs, function (tab) {
      if (tab.getBorderColor() != inactiveBorderColor) {
        tab.setBorderColor(inactiveBorderColor);
      }
    });
  }







  function updateTabStates() {
    if (!currentBasket.has('checkout_status')) {
      return;
    }

    var needsBillingAddress = Alloy.CFG.collect_billing_address ? !currentBasket.getBillingAddress() : false;

    switch (currentBasket.getCheckoutStatus()) {
      case 'cart':
        var enabledUnselected = [];
        var disabledUnselected = [$.checkout_payments_button, $.checkout_confirmation_button];
        setTabsEnabledSelected([$.checkout_cart_button]);

        currentBasket.getProductItems().length > 0 ? enabledUnselected.push($.checkout_address_button) : disabledUnselected.push($.checkout_address_button);

        currentBasket.getShippingAddress() && canCheckout() ? enabledUnselected.push($.checkout_billing_address_button) : disabledUnselected.push($.checkout_billing_address_button);

        currentBasket.getShippingAddress() && canCheckout() ? setTabsEnabledUnselected([$.checkout_billing_address_button]) : setTabsDisabledUnselected([$.checkout_billing_address_button]);
        if (currentBasket.getShippingAddress() && canCheckout()) {
          !needsBillingAddress ? enabledUnselected.push($.checkout_shipping_methods_button) : disabledUnselected.push($.checkout_shipping_methods_button);
        } else {
          disabledUnselected.push($.checkout_shipping_methods_button);
        }
        setTabsEnabledUnselected(enabledUnselected);
        setTabsDisabledUnselected(disabledUnselected);
        if (currentBasket.getShippingAddress() && currentBasket.getDifferentStorePickup()) {
          onBasketChangeWhileDifferentStoreShippingAddressSet();
        }
        break;
      case 'shippingAddress':
        if (currentBasket.canEnableCheckout()) {

          setTabsEnabledUnselected([$.checkout_cart_button]);
          setTabsEnabledSelected([$.checkout_address_button]);
          var enabledUnselected = [];
          var disabledUnselected = [$.checkout_payments_button, $.checkout_confirmation_button];

          currentBasket.getShippingAddress() && canCheckout ? enabledUnselected.push($.checkout_billing_address_button) : disabledUnselected.push($.checkout_billing_address_button);

          canCheckout() && currentBasket.getShippingAddress() && !needsBillingAddress ? enabledUnselected.push($.checkout_shipping_methods_button) : disabledUnselected.push($.checkout_shipping_methods_button);
          setTabsEnabledUnselected(enabledUnselected);
          setTabsDisabledUnselected(disabledUnselected);

          if (currentBasket.getShippingAddress() && currentBasket.getDifferentStorePickup()) {
            onBasketChangeWhileDifferentStoreShippingAddressSet();
          }
        }
        break;
      case 'askBillingAddress':
        if (currentBasket.canEnableCheckout()) {

          setTabsEnabledUnselected([$.checkout_cart_button]);
          setTabsEnabledSelected([$.checkout_address_button]);
          setTabsDisabledUnselected([$.checkout_billing_address_button, $.checkout_shipping_methods_button, $.checkout_payments_button, $.checkout_confirmation_button]);
        }
        break;
      case 'billingAddress':
        if (currentBasket.canEnableCheckout()) {

          setTabsEnabledUnselected([$.checkout_cart_button, $.checkout_address_button]);
          setTabsEnabledSelected([$.checkout_billing_address_button]);
          canCheckout() && currentBasket.getShippingAddress() && currentBasket.getBillingAddress() && !differentStoreSelectedForPickup ? setTabsEnabledUnselected([$.checkout_shipping_methods_button]) : setTabsDisabledUnselected([$.checkout_shipping_methods_button]);
          setTabsDisabledUnselected([$.checkout_payments_button, $.checkout_confirmation_button]);
        }
        break;
      case 'shippingMethod':
        if (currentBasket.canEnableCheckout()) {

          setTabsEnabledUnselected([$.checkout_cart_button]);
          canCheckout() ? setTabsEnabledUnselected([$.checkout_address_button, $.checkout_billing_address_button]) : setTabsDisabledUnselected([$.checkout_address_button, $.checkout_billing_address_button]);
          setTabsEnabledSelected([$.checkout_shipping_methods_button]);
          setTabsDisabledUnselected([$.checkout_payments_button, $.checkout_confirmation_button]);
        }
        break;
      case 'ask':
        if (currentBasket.canEnableCheckout()) {


          setTabsDisabledUnselected([$.checkout_cart_button, $.checkout_address_button, $.checkout_shipping_methods_button, $.checkout_confirmation_button]);
        }
        break;
      case 'payments':
        setTabsDisabledUnselected([$.checkout_cart_button, $.checkout_address_button, $.checkout_billing_address_button, $.checkout_shipping_methods_button, $.checkout_confirmation_button]);
        setTabsEnabledSelected([$.checkout_payments_button]);
        break;
      case 'confirmation':
        setTabsDisabledUnselected([$.checkout_address_button, $.checkout_billing_address_button, $.checkout_shipping_methods_button, $.checkout_payments_button]);
        setTabsEnabledUnselected([$.checkout_cart_button]);
        setTabsEnabledSelected([$.checkout_confirmation_button]);
        break;}

  }






  function differentStorePickupSelected() {
    differentStoreSelectedForPickup = true;
    updateTabStates();
  }






  function addressOrCurrentStorePickupSelected() {
    differentStoreSelectedForPickup = false;
    updateTabStates();
  }










  function onCheckoutCartClick(event) {
    Alloy.eventDispatcher.trigger('hideAuxillaryViews');
    event.cancelBubble = true;
    currentBasket.setCheckoutStatus('cart');
  }







  function onAddressClick(event) {
    event.cancelBubble = true;
    currentBasket.setCheckoutStatus('shippingAddress');
  }







  function onBillingClick(event) {
    event.cancelBubble = true;
    currentBasket.setCheckoutStatus('billingAddress');
  }







  function onShippingClick(event) {
    event.cancelBubble = true;
    currentBasket.setCheckoutStatus('shippingMethod');
  }







  function onPaymentsClick(event) {
    event.cancelBubble = true;
    currentBasket.setCheckoutStatus('payments');
  }







  function onConfirmationClick(event) {
    event.cancelBubble = true;
    currentBasket.setCheckoutStatus('confirmation');
  }






  function onBasketChangeWhileDifferentStoreShippingAddressSet() {
    var needsBillingAddress = Alloy.CFG.collect_billing_address ? !currentBasket.getBillingAddress() : false;
    if (Alloy.CFG.collect_billing_address) {
      if (needsBillingAddress) {
        setTabsDisabledUnselected([$.checkout_billing_address_button, $.checkout_shipping_methods_button, $.checkout_payments_button, $.checkout_confirmation_button]);
      } else {
        setTabsEnabledUnselected([$.checkout_billing_address_button]);
        setTabsDisabledUnselected([$.checkout_shipping_methods_button, $.checkout_payments_button, $.checkout_confirmation_button]);
      }
    } else {
      setTabsDisabledUnselected([$.checkout_shipping_methods_button, $.checkout_payments_button, $.checkout_confirmation_button]);
    }
  }









  _.extend($, exports);
}

module.exports = Controller;