var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'checkout/cart/index';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};




  $.basketProductLineItems = Alloy.createCollection('productItem');$.customerProductLineItems = Alloy.createCollection('productItem');


  $.__views.cart = Ti.UI.createView(
  { layout: "vertical", id: "cart" });

  $.__views.cart && $.addTopLevelView($.__views.cart);
  $.__views.cart_contents = Ti.UI.createView(
  { layout: "vertical", id: "cart_contents" });

  $.__views.cart.add($.__views.cart_contents);
  $.__views.selectorRow = Ti.UI.createView(
  { layout: "horizontal", width: "100%", height: 51, id: "selectorRow" });

  $.__views.cart_contents.add($.__views.selectorRow);
  $.__views.cart_button = Ti.UI.createButton(
  { left: 200, top: 12, width: 150, height: 27, color: Alloy.Styles.buttons.primary.color, font: Alloy.Styles.smallButtonFont, backgroundImage: Alloy.Styles.buttonLeftOnImage, titleid: "_Cart_Items__0", id: "cart_button", accessibilityValue: "cart_button" });

  $.__views.selectorRow.add($.__views.cart_button);
  $.__views.saved_products_button = Ti.UI.createButton(
  { top: 12, width: 150, height: 27, color: Alloy.Styles.buttons.primary.color, font: Alloy.Styles.smallButtonFont, backgroundImage: Alloy.Styles.buttonRightOffImage, titleid: "_Saved_Items__0", id: "saved_products_button", accessibilityValue: "saved_products_button" });

  $.__views.selectorRow.add($.__views.saved_products_button);
  $.__views.wish_list_button = Ti.UI.createButton(
  { left: 1, top: 12, width: 150, height: 27, color: Alloy.Styles.buttons.primary.color, font: Alloy.Styles.smallButtonFont, backgroundImage: Alloy.Styles.buttonRightOffImage, titleid: "_Wish_list__0", id: "wish_list_button", accessibilityValue: "wishlist_cart_button" });

  $.__views.selectorRow.add($.__views.wish_list_button);
  $.__views.contents_container = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.color.background.white, top: 0, height: "100%", id: "contents_container", layout: "absolute" });

  $.__views.cart_contents.add($.__views.contents_container);
  $.__views.pli_container = Ti.UI.createView(
  { top: 0, layout: "absolute", width: "100%", height: "100%", id: "pli_container" });

  $.__views.contents_container.add($.__views.pli_container);
  $.__views.cart_table = Ti.UI.createTableView(
  { top: 5, height: "100%", separatorStyle: "transparent", id: "cart_table" });

  $.__views.pli_container.add($.__views.cart_table);
  var __alloyId32 = Alloy.Collections['$.basketProductLineItems'] || $.basketProductLineItems;function __alloyId33(e) {if (e && e.fromAdapter) {return;}var opts = __alloyId33.opts || {};var models = __alloyId32.models;var len = models.length;var rows = [];for (var i = 0; i < len; i++) {var __alloyId29 = models[i];__alloyId29.__transform = transformPLI(__alloyId29);var __alloyId31 = Alloy.createController('checkout/cart/productItemRow', { $model: __alloyId29 });
      rows.push(__alloyId31.getViewEx({ recurse: true }));
    }$.__views.cart_table.setData(rows);};__alloyId32.on('fetch destroy change add remove reset', __alloyId33);$.__views.no_products_container = Ti.UI.createView(
  { height: "100%", width: "100%", layout: "absolute", top: 30, backgroundColor: Alloy.Styles.color.background.white, id: "no_products_container" });

  $.__views.pli_container.add($.__views.no_products_container);
  $.__views.no_products_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.tabFont, top: 50, textid: "_There_are_no_items_in_your_cart_", id: "no_products_label", accessibilityValue: "no_products_label" });

  $.__views.no_products_container.add($.__views.no_products_label);
  $.__views.saved_products_container = Ti.UI.createView(
  { top: 0, layout: "vertical", width: "100%", height: "100%", id: "saved_products_container", visible: true });

  $.__views.contents_container.add($.__views.saved_products_container);
  $.__views.saved_products_table = Ti.UI.createTableView(
  { top: 5, height: "100%", separatorStyle: "transparent", id: "saved_products_table" });

  $.__views.saved_products_container.add($.__views.saved_products_table);
  var __alloyId37 = Alloy.Collections['$.customerProductLineItems'] || $.customerProductLineItems;function __alloyId38(e) {if (e && e.fromAdapter) {return;}var opts = __alloyId38.opts || {};var models = __alloyId37.models;var len = models.length;var rows = [];for (var i = 0; i < len; i++) {var __alloyId34 = models[i];__alloyId34.__transform = transformPLI(__alloyId34);var __alloyId36 = Alloy.createController('checkout/cart/savedProductItemRow', { $model: __alloyId34 });
      rows.push(__alloyId36.getViewEx({ recurse: true }));
    }$.__views.saved_products_table.setData(rows);};__alloyId37.on('fetch destroy change add remove reset', __alloyId38);$.__views.no_saved_products_container = Ti.UI.createView(
  { height: "100%", width: "100%", layout: "absolute", top: 30, backgroundColor: Alloy.Styles.color.background.white, id: "no_saved_products_container" });

  $.__views.saved_products_container.add($.__views.no_saved_products_container);
  $.__views.no_saved_products_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.tabFont, top: 50, textid: "_There_are_no_saved_items_", id: "no_saved_products_label", accessibilityValue: "no_saved_products_label" });

  $.__views.no_saved_products_container.add($.__views.no_saved_products_label);
  $.__views.approaching_discount_container = Ti.UI.createView(
  { layout: "vertical", width: "100%", height: 60, id: "approaching_discount_container", visible: false });

  $.__views.cart_contents.add($.__views.approaching_discount_container);
  $.__views.approaching_discount_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: 40, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, backgroundImage: Alloy.Styles.accentUpperLowerBorderImage, top: 10, font: Alloy.Styles.productFont, id: "approaching_discount_label", accessibilityValue: "approaching_discount_label" });

  $.__views.approaching_discount_container.add($.__views.approaching_discount_label);
  if (true) {
    $.__views.more_menu_popover = Ti.UI.iPad.createPopover(
    { backgroundColor: Alloy.Styles.color.background.white, id: "more_menu_popover" });

    $.__views.more_menu_popover && $.addTopLevelView($.__views.more_menu_popover);
    $.__views.__alloyId39 = Ti.UI.createWindow(
    { tabBarHidden: 1, navBarHidden: 1, backgroundColor: Alloy.Styles.color.background.white, statusBarStyle: Ti.UI.iOS.StatusBar.LIGHT_CONTENT, id: "__alloyId39" });

    $.__views.menu_container = Ti.UI.createTableView(
    { backgroundColor: Alloy.Styles.color.background.white, rowSeparatorInsets: { left: 1, right: 1 }, tableSeparatorInsets: { left: 1, right: 1 }, top: 0, rowHeight: 50, height: "100%", width: "100%", separatorColor: Alloy.Styles.color.background.darkGray, id: "menu_container" });

    $.__views.__alloyId39.add($.__views.menu_container);
    handleMenuAction ? $.addListener($.__views.menu_container, 'click', handleMenuAction) : __defers['$.__views.menu_container!click!handleMenuAction'] = true;$.__views.menu_nav_window = Ti.UI.iOS.createNavigationWindow(
    { width: 250, window: $.__views.__alloyId39, id: "menu_nav_window" });

    $.__views.more_menu_popover.contentView = $.__views.menu_nav_window;}
  exports.destroy = function () {__alloyId32 && __alloyId32.off('fetch destroy change add remove reset', __alloyId33);__alloyId37 && __alloyId37.off('fetch destroy change add remove reset', __alloyId38);};




  _.extend($, $.__views);










  var toCurrency = require('EAUtils').toCurrency;
  var fetchImages = require('EAUtils').fetchImagesForProducts;
  var getVisibleViewFromContainerView = require('EAUtils').getVisibleViewFromContainerView;
  var analytics = require('analyticsBase');
  var logger = require('logging')('checkout:cart:index', getFullControllerPath($.__controllerPath));

  var oldPliHash;
  var currentBasket = Alloy.Models.basket;
  var currentCustomer = Alloy.Models.customer;

  var cCProductLists = currentCustomer.productLists;
  var wishListViewController;
  var wishListId = null;


  var approaching_promotions_timer;


  var matrix_in = Ti.UI.create2DMatrix();
  matrix_in = matrix_in.scale(0, 1);
  var matrix_out = Ti.UI.create2DMatrix();
  var shrink_down = Ti.UI.createAnimation({
    transform: matrix_in,
    duration: 150,
    autoreverse: false,
    repeat: 0 });

  var expand_out = Ti.UI.createAnimation({
    transform: matrix_out,
    duration: 150,
    autoreverse: false,
    repeat: 0 });


  var menuRowStyle = $.createStyle({
    classes: ['menu_row_style'],
    apiName: 'Ti.UI.TableRowView' });

  var menuRowLabelStyle = $.createStyle({
    classes: ['menu_row_label_style'],
    apiName: 'Ti.UI.Label' });

  var allMenuItems = [{
    id: 'add_button',
    accessibilityValue: 'add_button',
    text: _L('Add to Cart') },
  {
    id: 'save_later_button',
    accessibilityValue: 'save_later_button',
    text: _L('Save for Later') },
  {
    id: 'add_to_wish_list_button',
    accessibilityValue: 'add_to_wish_list_button',
    text: _L('Add to Wish List') },
  {
    id: 'override_button',
    accessibilityValue: 'override_button',
    text: _L('Override') }];

  var selectedIndexForPopoverMenu;
  var cartPopoverMenuWhiteList = ['save_later_button', 'add_to_wish_list_button', 'override_button'];
  var saveForLaterPopoverMenuWhiteList = ['add_button', 'add_to_wish_list_button'];




  $.listenTo(Alloy.eventDispatcher, 'associate_logout', function () {
    if (approaching_promotions_timer) {
      clearInterval(approaching_promotions_timer);
    }
  });

  $.listenTo(Alloy.eventDispatcher, 'hideAuxillaryViews', hideMenuPopover);
  $.listenTo(Alloy.eventDispatcher, 'app:dialog_displayed', hideMenuPopover);
  $.listenTo(Alloy.eventDispatcher, 'app:navigation', hideMenuPopover);




  $.cart_table.addEventListener('click', onCartTableClick);

  $.cart_table.addEventListener('doubletap', onCartTableDoubleClick);

  $.saved_products_table.addEventListener('click', onSavedProductsClick);

  $.cart_button.addEventListener('click', switchToSelectedProductListView);

  $.saved_products_button.addEventListener('click', switchToSelectedProductListView);

  $.wish_list_button.addEventListener('click', switchToSelectedProductListView);






  $.listenTo(currentBasket, 'basket_sync', function () {
    logger.info('Responding to basket_sync');
    initProductLineItemsCollection();
    displayApproachingPromotions();
  });

  $.listenTo(currentCustomer, 'change:saved_products reset:saved_products', function () {
    logger.info('Responding to change:saved_products or reset:saved_products');
    initSavedItemsCollection(false);
  });

  $.listenTo(currentBasket, 'change:approaching_order_promotions  change:approaching_shipping_promotions', function () {
    logger.info('Responding to change:approaching_*_promotions');
    displayApproachingPromotions();
  });

  $.listenTo(currentBasket, 'change:checkout_status', function () {
    if (currentBasket.getCheckoutStatus() == 'cart') {
      initProductLineItemsCollection();
      displayApproachingPromotions();
    }
  });


  $.listenTo(currentCustomer, 'change:login', function () {
    logger.info('Responding to customer change');
    updateCartLabel();
    determineSelectorRowVisibility();
    determineWishListVisibility();
  });

  $.listenTo(cCProductLists, 'reset', function () {
    wishListId = cCProductLists.getFirstWishListId();
  });




  exports.init = init;
  exports.deinit = deinit;
  exports.switchToWishListView = switchToWishListView;
  exports.switchToCartView = switchToCartView;
  exports.switchToSavedProductView = switchToSavedProductView;
  exports.deleteSavedItem = deleteSavedItem;
  exports.deleteItem = deleteItem;









  function init() {
    logger.info('Calling INIT');
    determineSelectorRowVisibility();
    toggle_cart_on(true);
    displayApproachingPromotions();
    updateCartLabel();
  }






  function deinit() {
    logger.info('Calling deinit, removing listeners');
    $.cart_table.removeEventListener('click', onCartTableClick);
    $.cart_table.removeEventListener('doubletap', onCartTableDoubleClick);
    $.saved_products_table.removeEventListener('click', onSavedProductsClick);
    $.cart_button.removeEventListener('click', switchToSelectedProductListView);
    $.saved_products_button.removeEventListener('click', switchToSelectedProductListView);
    $.wish_list_button.removeEventListener('click', switchToSelectedProductListView);
    $.menu_container.setData([]);
    $.menu_container.removeEventListener('click', handleMenuAction);
    hideMenuPopover();
    if (wishListViewController) {
      $.contents_container.remove(wishListViewController.getView());
      wishListViewController.deinit();
      wishListViewController = null;
    }
    deinitRows($.cart_table);
    deinitRows($.saved_products_table);
    $.stopListening();
    $.destroy();
  }











  function toggle_cart_on(state, event) {
    logger.info('toggle_cart_on ' + state);
    var buttonId;
    if (state) {
      buttonId = 'cart_button';
    } else {
      buttonId = event.source.id;
    }

    switch (buttonId) {
      case 'cart_button':

        switchToCartView();
        break;
      case 'saved_products_button':

        switchToSavedProductView();
        break;
      case 'wish_list_button':

        switchToWishListView();
        break;
      default:
        break;}

  }






  function initProductLineItemsCollection() {
    logger.info('Calling initProductLineItemsCollection');
    updateCartLabel();
    var plis = currentBasket.getProductItems();
    var newHash = Ti.Utils.sha256(JSON.stringify(plis));

    if (oldPliHash && newHash == oldPliHash) {
      return;
    }
    deinitRows($.cart_table);
    oldPliHash = newHash;
    if (plis && plis.length > 0) {
      logger.info('Calling initProductLineItemsCollection with data');

      $.basketProductLineItems.once('reset', function () {
        logger.info('Fetching images for plis');
        fetchImageForProducts(plis, $.cart_table);
      });

      $.basketProductLineItems.reset(plis);
    } else {

      logger.info('Calling initProductLineItemsCollection with no data');
      $.basketProductLineItems.reset([]);
    }
  }







  function initSavedItemsCollection(changeCartVisibility) {
    updateSavedProductsLabel();
    var plis = currentCustomer.getSavedProducts();
    deinitRows($.saved_products_table);
    if (plis && plis.length > 0) {
      logger.info('Calling initSavedItemsCollection with data');

      $.customerProductLineItems.once('reset', function () {
        logger.info('Fetching images for saved plis');
        if ($.saved_products_table.getSections().length > 0) {
          fetchImageForProducts(plis, $.saved_products_table);
        }
      });

      $.customerProductLineItems.reset(plis);
    } else {
      logger.info('Calling initSavedItemsCollection with no data');
      $.customerProductLineItems.reset([]);
    }
    var changeVisibility = true;
    if (changeCartVisibility != undefined) {
      changeVisibility = changeCartVisibility;
    }
    if (changeVisibility) {
      adjustSectionVisibility();
    }
  }







  function transformPLI(model) {
    logger.info('transform PLI');
    return {
      product_name: model.getProductName(),
      product_id: _L('Item# ') + model.getProductId(),
      quantity: _L('Quantity:') + ' ' + model.getQuantity(),
      unit_price: _L('List Price: ') + toCurrency(model.getBasePrice()),
      sub_total: toCurrency(model.getPrice()),
      row_id: 'id_' + model.getProductId(),
      override_price: model.getBasePriceOverride() ? _L('Override: ') + toCurrency(model.getBasePriceOverride()) : '' };

  }






  function switchToWishListView() {
    var visibleView = getVisibleViewFromContainerView($.contents_container);
    if (wishListViewController && visibleView !== wishListViewController.getView()) {
      $.cart_button.setBackgroundImage(Alloy.Styles.buttonLeftOffImage);
      $.saved_products_button.setBackgroundImage(Alloy.Styles.buttonMiddleOffImage);
      $.wish_list_button.setBackgroundImage(Alloy.Styles.buttonRightOnImage);
      flip(visibleView, wishListViewController.getView());
      $.trigger('cart:display', {
        savedItemsDisplayed: false,
        wishListItemsDisplayed: true });

    }
  }






  function switchToCartView() {
    var visibleView = getVisibleViewFromContainerView($.contents_container);
    if (visibleView !== $.pli_container) {
      $.cart_button.setBackgroundImage(Alloy.Styles.buttonLeftOnImage);
      if (Alloy.CFG.enable_wish_list) {
        $.saved_products_button.setBackgroundImage(Alloy.Styles.buttonMiddleOffImage);
        $.wish_list_button.setBackgroundImage(Alloy.Styles.buttonRightOffImage);
      } else {
        $.saved_products_button.setBackgroundImage(Alloy.Styles.buttonRightOffImage);
      }
      if (visibleView) {
        flip(visibleView, $.pli_container);
      } else {
        flip($.pli_container, $.pli_container);
      }
      $.trigger('cart:display', {
        savedItemsDisplayed: false,
        wishListItemsDisplayed: false });

    }
  }






  function switchToSavedProductView() {
    var visibleView = getVisibleViewFromContainerView($.contents_container);
    if (visibleView !== $.saved_products_container) {
      $.cart_button.setBackgroundImage(Alloy.Styles.buttonLeftOffImage);
      if (Alloy.CFG.enable_wish_list) {
        $.saved_products_button.setBackgroundImage(Alloy.Styles.buttonMiddleOnImage);
        $.wish_list_button.setBackgroundImage(Alloy.Styles.buttonRightOffImage);
      } else {
        $.saved_products_button.setBackgroundImage(Alloy.Styles.buttonRightOnImage);
      }
      flip(visibleView, $.saved_products_container);
      $.trigger('cart:display', {
        savedItemsDisplayed: true,
        wishListItemsDisplayed: false });

    }
  }







  function deleteSavedItem(index) {
    logger.info('delete saved item');
    var item = currentCustomer.getSavedProducts()[index];
    var promise = currentCustomer.removeSavedProduct(item);
    Alloy.Router.showActivityIndicator(promise);
    initSavedItemsCollection(false);
  }







  function deleteItem(index) {
    logger.info('delete item');
    var promise = currentBasket.removeItem(currentBasket.getProductItems()[index].getItemId());
    Alloy.Router.showActivityIndicator(promise);
  }







  function addToCartFromAnotherList(item) {
    var productId = item.getProductId();
    var productName = item.getProductName();
    var product = {
      product_id: productId,
      quantity: item.getQuantity() };

    var addProduct = currentBasket.addProduct(product, {
      c_employee_id: Alloy.Models.associate.getEmployeeId(),
      c_store_id: Alloy.CFG.store_id });


    Alloy.Router.showActivityIndicator(addProduct);
    addProduct.done(function () {
      currentCustomer.removeSavedProduct(item);
      initSavedItemsCollection();
      analytics.fireAnalyticsEvent({
        category: _L('Basket'),
        action: _L('Add To Basket'),
        label: productName + ' (' + productId + ')' });

    }).fail(function (failModel) {
      var fault = failModel ? failModel.get('fault') : null;
      var errorMsg = _L('Unable to add item' + (item.getQuantity() > 1 ? 's' : '') + ' to the cart');
      if (fault) {
        if (fault.type && fault.type === 'ProductItemNotAvailableException') {
          errorMsg = String.format(_L('Item \'%s\' is not available in quantity %d.'), productName, item.getQuantity());
        } else if (fault.message && fault.message != '') {
          errorMsg = fault.message;
        }
      }
      notify(errorMsg, {
        preventAutoClose: true });

    });
  }








  function addProductToWishList(productInfo, listId) {
    if (cCProductLists.getWishListCount() > 1) {
      Alloy.Dialog.showCustomDialog({
        controllerPath: 'product/components/productListSelectorDialog',
        continueEvent: 'productListSelectorDialog:continue',
        cancelEvent: 'productListSelectorDialog:dismiss',
        continueFunction: function (event) {
          if (event && event.listId) {
            var promise = cCProductLists.addItem(event.listId, productInfo);
            promise.done(function (model, opt) {
              notifyOnProductAddedToWishList(event.listId, model, opt);
            }).fail(function (model) {});
            Alloy.Router.showActivityIndicator(promise);
          }
        },
        options: {
          wishListCollection: cCProductLists.getAllWishLists() } });


    } else {
      var promise = cCProductLists.addItem(listId, productInfo);
      promise.done(function (model, opt) {
        notifyOnProductAddedToWishList(listId, model, opt);
      }).fail(function (model) {});
      Alloy.Router.showActivityIndicator(promise);
    }
  }









  function notifyOnProductAddedToWishList(listId, model, options) {
    var cPList = cCProductLists.where({
      id: listId });

    var itemText = model.getProductName();
    var wishListName = getWishListNameFromPListArray(cPList);
    var message = String.format(_L('%s added to Wish List.'), itemText);
    if (options.update) {
      message = String.format(_L('%s updated in Wish List.'), itemText);
      if (wishListName != '') {
        message = String.format(_L('%s updated in Wish List %s.'), itemText, wishListName);
      }
    } else if (wishListName != '') {
      message = String.format(_L('%s added to Wish List %s.'), itemText, wishListName);
    }
    notify(message);
  }








  function getWishListNameFromPListArray(cPList) {
    var wishListName = '';
    if (cPList && cPList.length > 0) {
      if (cPList[0].getName()) {
        wishListName = cPList[0].getName() == _L('Wish List Title') ? '' : cPList[0].getName();
      }
    }
    return wishListName;
  }








  function openProductOverrideDialog(item, index) {
    logger.info('openProductOverrideDialog');
    var product = Alloy.createModel('product');
    product.set('id', item.getProductId());
    var promise = product.fetchModel();
    Alloy.Router.showActivityIndicator(promise);
    promise.done(function () {
      var itemForProductOverride = {
        price_override: item.getPriceOverride(),
        manager_employee_id: item.getManagerEmployeeId(),
        product_name: item.getProductName(),
        image: item.getThumbnailUrl(),
        base_price: item.getBasePrice(),
        price_override_type: item.getPriceOverrideType(),
        price_override_reason_code: item.getPriceOverrideReasonCode(),
        price_override_value: item.getPriceOverrideValue(),
        product_id: item.getProductId(),
        item_id: item.getItemId() };

      Alloy.Dialog.showCustomDialog({
        controllerPath: 'checkout/cart/productPriceAdjustments',
        initOptions: {
          pli: itemForProductOverride,
          index: index },

        continueEvent: 'productOverride:dismiss' });


    }).fail(function () {
      notify(_L('Unable to open product override dialog.'), {
        preventAutoClose: true });

    });
  }






  function displayApproachingPromotions() {
    logger.info('displayApproachingPromotions');

    if (approaching_promotions_timer) {
      clearInterval(approaching_promotions_timer);
    }
    var plis = currentBasket ? currentBasket.getProductItems() : null;
    if (plis && plis.length > 0) {
      var aops = currentBasket.getApproachingOrderPromotions();
      var asps = currentBasket.getApproachingShippingPromotions();
      var orderPriceDescriptions = currentBasket.getOrderPriceDescriptions();
      var approaching_promotions = null;
      if (aops && asps) {
        approaching_promotions = aops.concat(asps);
      } else if (aops) {
        approaching_promotions = aops;
      } else if (asps) {
        approaching_promotions = asps;
      }

      if (approaching_promotions && approaching_promotions.length > 0 || orderPriceDescriptions && orderPriceDescriptions.length > 0) {
        var promotions_strings = [];
        for (var i = 0; i < approaching_promotions.length; i++) {
          promotions_strings.push(String.format(_L('%s to go for \'%s\''), toCurrency(approaching_promotions[i].amount_to_qualify), approaching_promotions[i].promotion_description));
        }
        for (var i = 0; i < orderPriceDescriptions.length; i++) {
          promotions_strings.push(String.format(_L('%s off for \'%s\''), toCurrency(Math.abs(orderPriceDescriptions[i].getPrice())), orderPriceDescriptions[i].get('item_text')));
        }

        if (Alloy.Models.customer.isLoggedIn()) {
          $.contents_container.setHeight(540);
        } else {
          $.contents_container.setHeight(591);
        }
        $.approaching_discount_container.show();
        $.approaching_discount_container.setHeight(60);

        var loop_counter = 0;
        loop_counter = showApproachingPromotion(promotions_strings, loop_counter);
        if (promotions_strings.length > 1) {
          approaching_promotions_timer = setInterval(function () {
            loop_counter = showApproachingPromotion(promotions_strings, loop_counter);
          }, 5 * 1000);
        }
      } else {
        clearApproachingPromotions();
      }
    } else {
      clearApproachingPromotions();
    }
  }






  function showApproachingPromotion(promotions_strings, loop_counter) {
    $.approaching_discount_label.setText(promotions_strings[loop_counter]);
    loop_counter++;
    if (loop_counter >= promotions_strings.length) {
      loop_counter = 0;
    }
    return loop_counter;
  }








  function updateWishListButtonTitle(count) {
    if (count < 0) {
      $.wish_list_button.setTitle(_L('Wish List'));
      return;
    }
    $.wish_list_button.setTitle(String.format(_L('Wish List: %d'), count));
  }







  function updateCartLabel() {
    logger.info('updateCartLabel');
    var total = calculateQuantity(currentBasket.getProductItems());
    $.cart_button.setTitle(String.format(_L('Cart Items: %d'), total));
    if (total == 0) {
      if (currentCustomer.isLoggedIn()) {
        $.pli_container.setTop(5);
      } else {
        $.pli_container.setTop(0);
      }
      $.cart_table.hide();
      $.cart_table.setHeight(0);
      $.no_products_container.show();
      $.no_products_container.setHeight('100%');
      $.no_products_container.setWidth('100%');
      $.no_products_container.setTop(0);
    } else {
      $.pli_container.setTop(0);
      $.cart_table.setHeight('100%');
      $.no_products_container.hide();
      $.no_products_container.setHeight(0);
      $.cart_table.show();
      displayApproachingPromotions();
    }
  }







  function updateSavedProductsLabel() {
    logger.info('updateSavedProductsLabel');
    var total = calculateQuantity(currentCustomer.getSavedProducts());
    $.saved_products_button.setTitle(String.format(_L('Saved Items: %d'), total));
    if (total == 0) {
      $.saved_products_table.hide();
      $.saved_products_table.setHeight(0);
      $.no_saved_products_container.show();
      $.no_saved_products_container.setHeight('100%');
      $.no_saved_products_container.setWidth('100%');
      $.no_saved_products_container.setTop(0);
    } else {
      $.no_saved_products_container.hide();
      $.no_saved_products_container.setHeight(0);
      $.saved_products_table.show();
      $.saved_products_table.setHeight('100%');
    }
  }






  function calculateQuantity(productItems) {
    var sum = 0;
    if (productItems.length > 0) {
      sum = _.reduce(productItems, function (memo, product) {
        return memo + product.getQuantity();
      }, 0);
    }
    return sum;
  }






  function adjustSectionVisibility() {
    $.no_products_container.hide();
    $.no_products_container.setHeight(0);
    $.cart_table.show();
    displayApproachingPromotions();
    toggle_cart_on(true);
  }








  function fetchImageForProducts(plis, table) {
    fetchImages(plis, table);
  }








  function flip(from, to) {
    if (!from && to) {
      to.show();
      return;
    }
    if (from && !to) {
      from.show();
      return;
    }
    if (from === to) {

      to.transform = matrix_in;
      to.show();
      to.animate(expand_out);
    } else {
      from.animate(shrink_down, function () {

        from.hide();
        from.transform = matrix_out;


        to.transform = matrix_in;
        to.show();
        to.animate(expand_out);
      });
    }
  }






  function clearApproachingPromotions() {
    if (approaching_promotions_timer) {
      clearInterval(approaching_promotions_timer);
    }
    $.approaching_discount_label.setText('');
    $.approaching_discount_container.hide();
    $.approaching_discount_container.setHeight(0);

    if (Alloy.Models.customer.isLoggedIn()) {
      $.contents_container.setHeight(600);
    } else {
      $.contents_container.setHeight(651);
    }
    $.cart_table.setHeight('100%');
  }






  function determineSelectorRowVisibility() {

    if (Alloy.Models.customer.isLoggedIn()) {
      $.selectorRow.setHeight(51);
      $.selectorRow.show();
      $.contents_container.setBackgroundColor(Alloy.Styles.color.background.medium);
    } else {
      $.selectorRow.hide();
      $.selectorRow.setHeight(0);
      $.saved_products_container.hide();
      $.contents_container.setBackgroundColor(Alloy.Styles.color.background.white);
    }
  }






  function determineWishListVisibility() {

    if (Alloy.Models.customer.isLoggedIn()) {
      if (Alloy.CFG.enable_wish_list && !wishListViewController) {
        wishListViewController = Alloy.createController('checkout/cart/wishList', {
          addToCartFromAnotherList: addToCartFromAnotherList,
          navigateToProduct: navigateToProduct,
          confirmDeleteItem: confirmDeleteItem,
          updateWishListButtonTitle: updateWishListButtonTitle,
          fetchImageForProducts: fetchImageForProducts,
          getWishListNameFromPListArray: getWishListNameFromPListArray });

        $.contents_container.add(wishListViewController.getView());
        wishListViewController.getView().hide();
      }
      if (Alloy.CFG.enable_wish_list) {
        $.saved_products_button.setBackgroundImage(Alloy.Styles.buttonMiddleOffImage);
        $.cart_button.setLeft(100);
        $.saved_products_button.setLeft(1);
        $.wish_list_button.applyProperties({
          visible: true,
          height: 27,
          width: 150,
          touchEnabled: true });

      } else {
        $.cart_button.setLeft(200);
        $.saved_products_button.setLeft(0);
        $.saved_products_button.setBackgroundImage(Alloy.Styles.buttonRightOffImage);
        $.wish_list_button.applyProperties({
          visible: false,
          height: 0,
          width: 0,
          touchEnabled: false });

      }
    } else {
      if (wishListViewController) {
        $.contents_container.remove(wishListViewController.getView());
        wishListViewController.deinit();
        wishListViewController = null;
      }
    }
  }










  function setupRemoveOverride(product_id, index) {
    var deferred = new _.Deferred();
    Alloy.Dialog.showConfirmationDialog({
      messageString: _L('A price override has been applied to this item. Saving the item will remove the override.'),
      okButtonString: _L('Confirm'),
      okFunction: function () {
        var override = {
          product_id: product_id,
          index: index,
          price_override_type: 'none',
          employee_id: Alloy.Models.associate.getEmployeeId(),
          employee_passcode: Alloy.Models.associate.getPasscode(),
          store_id: Alloy.CFG.store_id };


        if (isKioskManagerLoggedIn()) {
          override.manager_employee_id = getKioskManager().getEmployeeId();
          override.manager_employee_passcode = getKioskManager().getPasscode();
          override.manager_allowLOBO = getKioskManager().getPermissions().allowLOBO;
          override.kiosk_mode = isKioskMode();
        }

        currentBasket.setProductPriceOverride(override, {
          c_employee_id: Alloy.Models.associate.getEmployeeId() },
        {
          silent: true }).
        done(function () {

          deferred.resolve();
        }).fail(function () {

          deferred.reject();
        });
      },
      cancelFunction: function () {
        deferred.reject();
      } });

    return deferred.promise();
  }








  function saveItemForLater(item, index) {
    item.setMessage(null, {
      silent: true });


    var product = {
      product_id: item.getProductId(),
      quantity: item.getQuantity() };

    var deferred = new _.Deferred();
    currentCustomer.addSavedProduct(product, {
      c_employee_id: Alloy.Models.associate.getEmployeeId(),
      c_store_id: Alloy.CFG.store_id }).
    done(function () {
      currentBasket.removeItem(item.getItemId()).done(function () {
        initSavedItemsCollection();
        deferred.resolve();
      }).fail(function (failModel) {
        var fault = failModel ? failModel.get('fault') : null;
        var errorMsg = _L('Unable to delete item.');
        if (fault && fault.message && fault.message != '') {
          errorMsg = fault.message;
        }
        notify(errorMsg, {
          preventAutoClose: true });

        deferred.reject();
      });
    }).fail(function (failModel) {
      var fault = failModel ? failModel.get('fault') : null;
      var errorMsg = _L('Unable to save item.');
      if (fault && fault.message && fault.message != '') {
        errorMsg = fault.message;
      }
      notify(errorMsg, {
        preventAutoClose: true });

      deferred.reject();
    });

    Alloy.Router.showActivityIndicator(deferred);
  }








  function confirmDeleteItem(event, callback) {


    Alloy.Dialog.showConfirmationDialog({
      messageString: String.format(_L('Do you really want to delete this item?'), event.product_name),
      titleString: _L('Delete Item'),
      okButtonString: _L('Delete'),
      okFunction: function () {

        if (event.wishListId && event.wishListItemId) {
          callback(event.wishListId, event.wishListItemId);
        } else {
          callback(event.index);
        }
      } });

  }








  function navigateToProduct(product_id, replaceItem) {
    var config = {
      product_id: product_id };

    if (replaceItem) {
      config.replaceItem = replaceItem;
    }
    Alloy.Router.navigateToProduct(config);
  }









  function showPopoverMenu(sourceButton, whiteList, selectedIndex) {
    var menuItems = [];

    menuItems = allMenuItems.filter(function (row) {
      if (whiteList.indexOf(row.id) > -1) {

        switch (row.id) {
          case 'save_later_button':
            if (!Alloy.Models.customer.isLoggedIn()) {
              return false;
            }
            break;
          case 'add_to_wish_list_button':
            if (!Alloy.Models.customer.isLoggedIn()) {
              return false;
            }
            if (!Alloy.CFG.enable_wish_list) {
              return false;
            } else if (Alloy.CFG.enable_wish_list && Alloy.Models.customer.productLists.getWishListCount() < 1) {
              return false;
            }
            break;
          case 'override_button':
            if (!Alloy.CFG.overrides.product_price_overrides) {
              return false;
            } else if (isKioskMode() && !isKioskManagerLoggedIn()) {
              return false;
            }
            break;}


        return true;
      }
      return false;
    });

    if (menuItems.length > 0) {
      selectedIndexForPopoverMenu = selectedIndex;
      var menuRowViews = [];


      _.each(menuItems, function (row) {
        var cMenuRowView = Ti.UI.createTableViewRow(menuRowStyle);
        cMenuRowView.add(_.extend(Ti.UI.createLabel(menuRowLabelStyle), row));
        menuRowViews.push(cMenuRowView);
      });


      $.menu_nav_window.setHeight(50 * menuItems.length);
      $.menu_container.setData(menuRowViews);


      var sourceButtonPointRelativeToScreen = sourceButton.convertPointToView({
        x: sourceButton.rect.x,
        y: sourceButton.rect.y },
      $.cart.parent.parent);


      if (Ti.Platform.displayCaps.platformHeight - sourceButtonPointRelativeToScreen.y > 230) {
        $.more_menu_popover.setArrowDirection(Titanium.UI.iPad.POPOVER_ARROW_DIRECTION_UP);
      } else {
        $.more_menu_popover.setArrowDirection(Titanium.UI.iPad.POPOVER_ARROW_DIRECTION_DOWN);
      }

      $.more_menu_popover.show({
        view: sourceButton });

    } else {
      selectedIndexForPopoverMenu = null;
    }
  }






  function hideMenuPopover() {
    $.more_menu_popover.hide();
  }








  function handleMenuAction(event) {
    hideMenuPopover();
    var visibleView = getVisibleViewFromContainerView($.contents_container);
    switch (visibleView) {
      case $.saved_products_container:
        var item = currentCustomer.getSavedProducts()[selectedIndexForPopoverMenu];
        var productId = item.getProductId();
        switch (event.source.id) {
          case 'add_button':

            addToCartFromAnotherList(item);
            break;
          case 'add_to_wish_list_button':

            addProductToWishList({
              product_id: productId,
              quantity: item.getQuantity() },
            wishListId);
            break;}

        break;
      case $.pli_container:
        var item = currentBasket.getProductItems()[selectedIndexForPopoverMenu];
        var productId = item.getProductId();
        switch (event.source.id) {
          case 'save_later_button':

            if (!currentCustomer.getSavedProducts()) {
              currentBasket.setSavedProducts(new Backbone.Collection());
            }
            if (item.getPriceOverride() == 'true') {
              setupRemoveOverride(productId, selectedIndexForPopoverMenu).done(function () {
                saveItemForLater(item, selectedIndexForPopoverMenu);
              });
            } else {
              saveItemForLater(item, selectedIndexForPopoverMenu);
            }
            break;
          case 'add_to_wish_list_button':

            addProductToWishList({
              product_id: productId,
              quantity: item.getQuantity() },
            wishListId);
            break;
          case 'override_button':

            openProductOverrideDialog(item, selectedIndexForPopoverMenu);
            break;}

        break;}

  }






  function deinitRows(table) {

    if (table.getSections().length > 0) {
      _.each(table.getSections()[0].getRows(), function (row) {
        row.deinit();
      });
    }
  }









  function onCartTableClick(event) {
    var item = currentBasket.getProductItems()[event.index];
    var product_id = item.getProductId();
    var product_name = item.getProductName();

    if (event.source.id === 'delete_button') {
      confirmDeleteItem({
        index: event.index,
        product_name: product_name },
      deleteItem);
    } else if (event.source.id === 'more_menu_button') {

      showPopoverMenu(event.source, cartPopoverMenuWhiteList, event.index);
    } else if (event.source.id === 'override_button') {

      openProductOverrideDialog(item, event.index);
    } else if (event.source.id === 'edit_button') {

      var item_json = item.toJSON();
      item_json.index = event.index;
      navigateToProduct(item.get('product_id'), item_json);
    }
  }







  function switchToSelectedProductListView(event) {
    if (event.source === $.pli_container) {
      toggle_cart_on(true, event);
    } else {
      toggle_cart_on(false, event);
    }
  }







  function onCartTableDoubleClick(event) {

    var row = $.cart_table.getSections()[0].getRows()[event.index];
    if (row) {
      row.showOverrideButton.call(this);
    }
  }







  function onSavedProductsClick(event) {
    var item = currentCustomer.getSavedProducts()[event.index];
    var product_id = item.getProductId();
    var product_name = item.getProductName();
    if (event.source.id === 'add_button') {

      addToCartFromAnotherList(item);
    } else if (event.source.id === 'more_menu_button') {

      showPopoverMenu(event.source, saveForLaterPopoverMenuWhiteList, event.index);
    } else if (event.source.id === 'delete_button') {

      confirmDeleteItem({
        index: event.index,
        product_name: product_name },
      deleteSavedItem);
    } else if (event.source.id === 'product_name' || event.source.id === 'product_image') {

      navigateToProduct(product_id);
    }
  }






  function onCartTabClick() {
    toggle_cart_on(true);
  }






  function onSavedTabClick() {
    toggle_cart_on(false);
  }





  __defers['$.__views.menu_container!click!handleMenuAction'] && $.addListener($.__views.menu_container, 'click', handleMenuAction);



  _.extend($, exports);
}

module.exports = Controller;