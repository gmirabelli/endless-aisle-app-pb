var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'components/customerPopover';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.popover_window = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.color.background.transparent, id: "popover_window" });

  $.__views.popover_window && $.addTopLevelView($.__views.popover_window);
  $.__views.backdrop = Ti.UI.createView(
  { top: 0, left: 0, width: 1024, height: 768, opacity: 0.25, backgroundColor: Alloy.Styles.color.background.black, id: "backdrop" });

  $.__views.popover_window.add($.__views.backdrop);
  $.__views.popover_container = Ti.UI.createView(
  { layout: "vertical", height: 140, width: 250, backgroundColor: Alloy.Styles.color.background.white, left: 525, top: 75, borderWidth: 2, borderColor: Alloy.Styles.color.border.lightest, id: "popover_container" });

  $.__views.popover_window.add($.__views.popover_container);
  $.__views.profile_button = Ti.UI.createButton(
  { backgroundImage: Alloy.Styles.secondaryButtonImage, width: "90%", height: 50, top: 10, titleid: "_View_Customer_Profile", id: "profile_button", accessibilityValue: "profile_button" });

  $.__views.popover_container.add($.__views.profile_button);
  $.__views.logout_button = Ti.UI.createButton(
  { backgroundImage: Alloy.Styles.primaryButtonImage, color: Alloy.Styles.buttons.primary.color, width: "90%", height: 50, top: 10, titleid: "_Logout_Customer", id: "logout_button", accessibilityValue: "logout_button" });

  $.__views.popover_container.add($.__views.logout_button);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var customerAddress = Alloy.Models.customerAddress;
  var currentCustomer = Alloy.Models.customer;
  var showCustomerAddressAlert = require('EAUtils').showCustomerAddressAlert;
  var logger = require('logging')('components:customerPopover', getFullControllerPath($.__controllerPath));





  $.logout_button.addEventListener('click', onLogoutClick);


  $.profile_button.addEventListener('click', onProfileClick);


  $.backdrop.addEventListener('click', dismiss);




  exports.init = init;
  exports.deinit = deinit;









  function init() {}








  function deinit() {
    $.logout_button.removeEventListener('click', onLogoutClick);
    $.profile_button.removeEventListener('click', onProfileClick);
    $.backdrop.removeEventListener('click', dismiss);
    $.stopListening();
    $.destroy();
  }









  function dismiss() {
    logger.info('dismissing dialog');
    $.trigger('customerPopover:dismiss');
  }






  function onLogoutClick() {
    if (currentCustomer.isLoggedIn() && customerAddress.isCustomerAddressPage()) {
      showCustomerAddressAlert(true).done(function () {
        Alloy.Router.customerLogout();
      });
    } else {
      Alloy.Router.customerLogout();
    }
    dismiss();
  }






  function onProfileClick() {
    if (currentCustomer.isLoggedIn() && customerAddress.isCustomerAddressPage()) {
      showCustomerAddressAlert(true).done(function () {
        Alloy.Router.navigateToCustomer();
      });
    } else {
      Alloy.Router.navigateToCustomer();
    }
    dismiss();
  }









  _.extend($, exports);
}

module.exports = Controller;