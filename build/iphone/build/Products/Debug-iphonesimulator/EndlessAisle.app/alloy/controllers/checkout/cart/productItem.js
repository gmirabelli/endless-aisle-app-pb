var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'checkout/cart/productItem';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};




  $.pli = Alloy.createModel('productItem');


  $.__views.product_line_item_container = Ti.UI.createView(
  { layout: "horizontal", width: 470, height: 148, id: "product_line_item_container" });

  $.__views.product_line_item_container && $.addTopLevelView($.__views.product_line_item_container);
  $.__views.product_image_container = Ti.UI.createView(
  { width: 148, height: 148, top: 0, id: "product_image_container" });

  $.__views.product_line_item_container.add($.__views.product_image_container);
  $.__views.product_image = Ti.UI.createImageView(
  { height: "100%", top: 0, id: "product_image", accessibilityValue: "product_image" });

  $.__views.product_image_container.add($.__views.product_image);
  $.__views.product_container = Ti.UI.createView(
  { layout: "vertical", width: 302, left: 10, top: 0, id: "product_container" });

  $.__views.product_line_item_container.add($.__views.product_container);
  $.__views.product_name = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.productFont, left: 0, id: "product_name", accessibilityValue: "product_name" });

  $.__views.product_container.add($.__views.product_name);
  $.__views.product_details_container = Ti.UI.createView(
  { layout: "horizontal", width: 302, top: 5, height: 82, id: "product_details_container" });

  $.__views.product_container.add($.__views.product_details_container);
  $.__views.product_info_container = Ti.UI.createView(
  { layout: "vertical", width: 151, top: 0, id: "product_info_container" });

  $.__views.product_details_container.add($.__views.product_info_container);
  $.__views.__alloyId40 = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, font: Alloy.Styles.lineItemFont, accessibilityValue: "product_id", id: "__alloyId40" });

  $.__views.product_info_container.add($.__views.__alloyId40);
  $.__views.option_text = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, font: Alloy.Styles.lineItemFont, id: "option_text", accessibilityValue: "option_text" });

  $.__views.product_info_container.add($.__views.option_text);
  $.__views.variation_container = Ti.UI.createView(
  { layout: "vertical", height: Ti.UI.SIZE, id: "variation_container" });

  $.__views.product_info_container.add($.__views.variation_container);
  $.__views.__alloyId41 = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, font: Alloy.Styles.lineItemFont, accessibilityValue: "quantity", id: "__alloyId41" });

  $.__views.product_info_container.add($.__views.__alloyId41);
  if (!hasScaleDown()) {
    $.__views.product_price_container = Ti.UI.createView(
    { layout: "vertical", width: 151, right: 0, id: "product_price_container" });

    $.__views.product_details_container.add($.__views.product_price_container);
    $.__views.unit_price_container = Ti.UI.createView(
    { height: Ti.UI.SIZE, right: 0, id: "unit_price_container" });

    $.__views.product_price_container.add($.__views.unit_price_container);
    $.__views.unit_price_label = Ti.UI.createLabel(
    { width: 151, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT, font: Alloy.Styles.lineItemFont, right: 0, id: "unit_price_label", accessibilityValue: "unit_price_label" });

    $.__views.unit_price_container.add($.__views.unit_price_label);
    $.__views.sale_price_container = Ti.UI.createView(
    { height: Ti.UI.SIZE, right: 0, id: "sale_price_container" });

    $.__views.product_price_container.add($.__views.sale_price_container);
    $.__views.sale_price_label = Ti.UI.createLabel(
    { width: 151, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT, font: Alloy.Styles.lineItemFont, right: 0, id: "sale_price_label", accessibilityValue: "sale_price_label" });

    $.__views.sale_price_container.add($.__views.sale_price_label);
    $.__views.override_price_container = Ti.UI.createView(
    { height: Ti.UI.SIZE, right: 0, id: "override_price_container" });

    $.__views.product_price_container.add($.__views.override_price_container);
    $.__views.override_price_label = Ti.UI.createLabel(
    { width: 151, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT, font: Alloy.Styles.lineItemFont, right: 0, id: "override_price_label", accessibilityValue: "override_price_label" });

    $.__views.override_price_container.add($.__views.override_price_label);
    $.__views.option_price_container = Ti.UI.createView(
    { height: Ti.UI.SIZE, right: 0, id: "option_price_container" });

    $.__views.product_price_container.add($.__views.option_price_container);
    $.__views.option_price_label = Ti.UI.createLabel(
    { width: 151, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT, font: Alloy.Styles.lineItemFont, right: 0, id: "option_price_label", accessibilityValue: "option_price_label" });

    $.__views.option_price_container.add($.__views.option_price_label);
    $.__views.item_price_container = Ti.UI.createView(
    { height: Ti.UI.SIZE, right: 0, top: 10, id: "item_price_container" });

    $.__views.product_price_container.add($.__views.item_price_container);
    $.__views.item_price_label = Ti.UI.createLabel(
    { width: 151, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT, font: Alloy.Styles.lineItemFont, right: 0, id: "item_price_label", accessibilityValue: "item_price_label" });

    $.__views.item_price_container.add($.__views.item_price_label);
  }
  if (!hasScaleDown()) {
    $.__views.price_adjustments_container = Ti.UI.createScrollView(
    { layout: "vertical", width: "100%", height: 20, id: "price_adjustments_container" });

    $.__views.product_container.add($.__views.price_adjustments_container);
  }
  $.__views.message_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, font: Alloy.Styles.lineItemFont, id: "message_label" });

  $.__views.product_container.add($.__views.message_label);
  var __alloyId42 = function () {$['pli'].__transform = _.isFunction($['pli'].transform) ? $['pli'].transform() : $['pli'].toJSON();$.product_name.text = $['pli']['__transform']['product_name'];$.__alloyId40.text = $['pli']['__transform']['product_id'];$.__alloyId41.text = $['pli']['__transform']['quantity'];if (!hasScaleDown()) {$.unit_price_label.text = $['pli']['__transform']['unit_price'];$.override_price_label.text = $['pli']['__transform']['override_price'];$.item_price_label.text = $['pli']['__transform']['item_price'];}};$['pli'].on('fetch change destroy', __alloyId42);exports.destroy = function () {$['pli'] && $['pli'].off('fetch change destroy', __alloyId42);};




  _.extend($, $.__views);










  var toCurrency = require('EAUtils').toCurrency;
  var currencyCode;
  var strikeThroughListPrice = false;
  var strikeThroughSalePrice = false;
  var eaUtils = require('EAUtils');
  var logger = require('logging')('checkout:cart:productItem', getFullControllerPath($.__controllerPath));
  var args = $.args;




  exports.init = init;
  exports.deinit = deinit;














  function init(product, pli, newCurrencyCode, order) {
    logger.info('init');
    addPricingToProductListItem(product, pli);
    currencyCode = newCurrencyCode;
    $.pli.clear();
    $.pli.set(pli.toJSON());
    updateImage(product, pli);
    updateVariations(product);
    updateOptionText(pli);
    updateMessageLabel(pli);
    showPurchaseQuantity(pli);
    if (hasScaleDown()) {
      scaleDown();
      return;
    }
    updateSalePrice(product, order);
    updateOverridePrice(pli, product);
    updateDueToCoupon(pli, product);
    updateProductDiscounts(pli);
  }






  function deinit() {

    logger.info('DEINIT called');
    $.stopListening();
    removeAllChildren($.variation_container);
    if (!hasScaleDown()) {
      removeAllChildren($.price_adjustments_container);
    }
    $.destroy();
  }










  function updateOptionText(pli) {
    if (_.isFunction(pli.getOptionItems) && pli.getOptionItems().length > 0) {
      var optionItems = pli.getOptionItems();
      var option = optionItems[0];
      $.option_text.setText(option.getItemText());
      if (option.getPrice() != 0) {
        $.option_price_label.setText(_L('Option Price:') + ' ' + toCurrency(option.getPrice(), currencyCode));
      }
    } else {
      $.option_text.hide();
      $.option_price_container.hide();
      $.option_text.setHeight(0);
      $.option_price_container.setHeight(0);
    }
  }







  function updateVariations(product) {
    if (product) {
      var vvs = product.getVariationValues();

      for (var name in vvs) {
        var displayName = product.getVariationAttributeDisplayName(name);
        var value = product.getVariationAttributeByName(name);
        var label = Ti.UI.createLabel({
          textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
          left: 0,
          height: Titanium.UI.SIZE,
          font: Alloy.Styles.lineItemFont,
          color: Alloy.Styles.color.text.mediumdark,
          text: String.format(_L('%s: %s'), displayName, value) });

        $.variation_container.add(label);
      }
    }
  }








  function updateOverridePrice(pli, product) {
    if (_.isFunction(pli.getPriceOverride) && pli.getPriceOverride() === 'true') {

      if (product.getSalePrice()) {
        addPriceStrikeThrough($.sale_price_container, $.sale_price_label);
        strikeThroughSalePrice = true;
      } else {
        addPriceStrikeThrough($.unit_price_container, $.unit_price_label);
        strikeThroughListPrice = true;
      }
    } else {
      $.override_price_container.hide();
      $.override_price_container.setHeight(0);
    }
  }








  function addPriceStrikeThrough(container, label) {
    var width = 60;
    var len = label.getText().length;
    width += (len - 5) * 5;
    container.setWidth(width);
    eaUtils.strikeThrough(label, label.getText());
  }








  function updateSalePrice(product, order) {
    var salePrice = null;
    if (product) {
      salePrice = product.getSalePrice();
    }
    if (salePrice && (!order || order.justCreated)) {
      var listPrice = product.getListPrice();
      $.unit_price_label.setText(_L('List Price:') + ' ' + toCurrency(listPrice, currencyCode));
      $.sale_price_label.setText(_L('Sale Price:') + ' ' + toCurrency(salePrice, currencyCode));
      $.sale_price_label.setWidth(Ti.UI.SIZE);

      addPriceStrikeThrough($.unit_price_container, $.unit_price_label);
      strikeThroughListPrice = true;
    } else {
      $.unit_price_label.setText('');
      $.sale_price_label.setText('');
      $.unit_price_container.setHeight(0);
      $.unit_price_container.hide();
      $.sale_price_container.setHeight(0);
      $.sale_price_container.hide();
    }
  }







  function updateImage(product, pli) {
    var image;
    if (_.isFunction(pli.getThumbnailUrl)) {
      image = pli.getThumbnailUrl() ? pli.getThumbnailUrl() : product.getCartImage();
    } else {
      image = product.getCartImage();
    }
    if (image) {

      var imageView = $.product_image;
      if (!imageView.image) {
        Alloy.Globals.getImageViewImage(imageView, image);
      }
    }
  }






  $.pli.transform = function () {
    return {
      product_name: $.pli.getProductName(),
      product_id: _L('Item# ') + $.pli.getProductId(),
      quantity: _L('Quantity:') + ' ' + $.pli.getQuantity(),
      unit_price: _L('List Price:') + ' ' + toCurrency($.pli.getBasePrice(), currencyCode),
      sub_total: toCurrency($.pli.getPrice()),
      override_price: $.pli.getBasePriceOverride() ? _L('Override:') + ' ' + toCurrency($.pli.getBasePriceOverride(), currencyCode) : '',
      item_price: _L('Item Price:') + ' ' + toCurrency($.pli.getPrice(), currencyCode) };

  };








  function updateDueToCoupon(pli, product) {
    if (_.isFunction(pli.getPriceAdjustments) && pli.getPriceAdjustments().length > 0) {

      if (!strikeThroughListPrice) {
        addPriceStrikeThrough($.unit_price_container, $.unit_price_label);
      }

      if (pli.getPriceOverride() === 'true') {
        addPriceStrikeThrough($.override_price_container, $.override_price_label);
      }

      if (product.getSalePrice() && !strikeThroughSalePrice) {
        addPriceStrikeThrough($.sale_price_container, $.sale_price_label);
      }
    }
  }







  function showPurchaseQuantity(pli) {
    if (_.isFunction(pli.getPurchasedQuantity)) {

      $.product_info_container.add(Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Titanium.UI.SIZE,
        color: Alloy.Styles.color.text.mediumdark,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        left: 0,
        font: Alloy.Styles.lineItemFont,
        accessibilityLabel: 'purchased_quantity',
        text: String.format(_L('Purchased Quantity: %d'), pli.getPurchasedQuantity()) }));

      $.product_details_container.setHeight(85);
    }
  }







  function updateProductDiscounts(pli) {
    if (_.isFunction(pli.getPriceAdjustments) && pli.getPriceAdjustments().length > 0) {
      var discounts = pli.getPriceAdjustments();
      for (var d = 0; d < discounts.length; ++d) {
        var discount = discounts[d];
        var label = Ti.UI.createLabel({
          textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
          left: 0,
          font: Alloy.Styles.lineItemLabelFont,
          color: Alloy.Styles.color.text.mediumdark,
          height: Titanium.UI.SIZE,
          text: discount.getItemText() });

        $.price_adjustments_container.add(label);
      }
      $.product_line_item_container.setHeight($.product_line_item_container.getHeight() + 10);
    } else {
      $.price_adjustments_container.hide();
      $.price_adjustments_container.setHeight(0);
    }
  }







  function updateMessageLabel(pli) {
    if (_.isFunction(pli.hasMessage) && pli.hasMessage()) {
      $.message_label.setText(pli.getMessage());
      if (hasScaleDown()) {
        $.product_line_item_container.setHeight($.product_line_item_container.getHeight() + 20);
      } else {
        $.product_line_item_container.setHeight($.product_line_item_container.getHeight() + 10);
      }
    }
  }








  function addPricingToProductListItem(product, pli) {
    if (pli.get('_type') == 'customer_product_list_item') {
      if (!pli.has('price')) {
        pli.set({
          price: product.getPrice() },
        {
          silent: true });

      }
      if (!pli.has('base_price')) {
        var listPrice = product.getListPrice();
        pli.set({
          base_price: product.getPrices() && listPrice ? listPrice : product.getPrice() },
        {
          silent: true });

      }
    }
  }







  function hasScaleDown() {
    return _.isObject($.args) && !_.isEmpty(args) && $.args.scaleDown === true;
  }






  function scaleDown() {
    $.product_line_item_container.setWidth(400);
    $.product_image_container.setLeft(10);
    $.product_image_container.setHeight(115);
    $.product_image_container.setWidth(115);
    $.product_container.setWidth(260);
    $.product_details_container.setWidth('100%');
  }









  _.extend($, exports);
}

module.exports = Controller;