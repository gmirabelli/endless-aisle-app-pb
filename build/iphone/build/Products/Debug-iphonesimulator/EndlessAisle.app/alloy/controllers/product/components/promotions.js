var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
		delete obj[key];
	}
	return arg;
}

function Controller() {

	require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'product/components/promotions';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};







	$.__views.promotions = Ti.UI.createView(
	{ layout: "vertical", height: Ti.UI.SIZE, top: 0, id: "promotions" });

	$.__views.promotions && $.addTopLevelView($.__views.promotions);
	exports.destroy = function () {};




	_.extend($, $.__views);










	var logger = require('logging')('product:components:promotions', getFullControllerPath($.__controllerPath));





	exports.init = init;
	exports.deinit = deinit;










	function init(args) {
		logger.info('init called');

		$model = $model || args && args.model || Alloy.Models.product;

		render();
	}






	function render() {
		logger.info('render called');

		var promotions = $model.getProductPromotions();
		if (!promotions) {
			return;
		}

		_.each(promotions, function (promotion) {
			var html = '<meta name="viewport" content="width=300,user-scalable=0,initial-scale=0">' + '<style>' + Alloy.CFG.product.promotionWebViewCss + '</style>' + '<div>' + promotion.getCalloutMsg() + '</div>';

			var webView = Ti.UI.createWebView($.createStyle({
				html: html,
				classes: ['webview_container'],
				apiName: 'WebView' }));


			$.promotions.add(webView);
		});
	}






	function deinit() {
		logger.info('deinit called');
		$.stopListening();
		removeAllChildren($.promotions);
		$.destroy();
	}









	_.extend($, exports);
}

module.exports = Controller;