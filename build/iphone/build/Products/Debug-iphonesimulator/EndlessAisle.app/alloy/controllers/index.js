var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'index';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.index = Ti.UI.createWindow(
  { tabBarHidden: 1, navBarHidden: 1, backgroundColor: Alloy.Styles.color.background.black, statusBarStyle: Ti.UI.iOS.StatusBar.LIGHT_CONTENT, top: 0, id: "index" });

  $.__views.index && $.addTopLevelView($.__views.index);
  $.__views.background = Ti.UI.createImageView(
  { top: 0, width: 1024, height: 768, image: Alloy.Styles.splashScreenImage, id: "background" });

  $.__views.index.add($.__views.background);
  kioskLogin ? $.addListener($.__views.background, 'click', kioskLogin) : __defers['$.__views.background!click!kioskLogin'] = true;$.__views.kiosk_message = Ti.UI.createLabel(
  { width: 360, height: 200, color: Alloy.Styles.darkAccentColor, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, zIndex: 1, left: 580, top: 20, font: Alloy.Styles.bigTitleFont, visible: false, verticalAlign: Ti.UI.TEXT_VERTICAL_ALIGNMENT_CENTER, id: "kiosk_message" });

  $.__views.index.add($.__views.kiosk_message);
  kioskLogin ? $.addListener($.__views.kiosk_message, 'click', kioskLogin) : __defers['$.__views.kiosk_message!click!kioskLogin'] = true;$.__views.backdrop = Ti.UI.createView(
  { top: 0, left: 0, backgroundColor: Alloy.Styles.color.background.black, opacity: 0, layout: "absolute", id: "backdrop" });

  $.__views.index.add($.__views.backdrop);
  exports.destroy = function () {};




  _.extend($, $.__views);











  var animation = require('alloy/animation');
  require('reporter');
  var analytics = require('analyticsBase');
  var logger = require('logging')('application:index', getFullControllerPath($.__controllerPath));

  var validateAppSettings = require('Validations').validateAppSettings;
  var validateNetwork = require('Validations').validateNetwork;
  var validateStorefront = require('Validations').validateStorefront;
  var validateDevicesStartup = require('Validations').validateDevicesStartup;
  var loadConfigurations = require('appConfiguration').loadConfigurations;
  var showActivityIndicator = require('dialogUtils').showActivityIndicator;
  var countryConfig = require('config/countries').countryConfig;
  var geolocation = require('./geolocation');

  var notifyReadDelay = 250;
  var notifyDelayStartup = 10000;

  var appSettings = require('appSettings');
  var EAUtils = require('EAUtils');
  var storePasswordHelpers = require('storePasswordHelpers');



  Alloy.Platform = Ti.Platform.osname;

  var loading = false,
  appIndexController,
  refreshing = false;




  $.listenTo(Alloy.eventDispatcher, 'app:startup', init);

  $.listenTo(Alloy.eventDispatcher, 'app:login', doAppLogin);

  $.listenTo(Alloy.eventDispatcher, 'configurations:prelogin', onConfigsPreLogin);

  $.listenTo(Alloy.eventDispatcher, 'configurations:postlogin', onConfigsPostLogin);

  $.listenTo(Alloy.eventDispatcher, 'configurations:store_availability.max_distance_search', loadAllStores);

  $.listenTo(Alloy.eventDispatcher, 'configurations:store_availability.distance_unit', loadAllStores);

  $.listenTo(Alloy.eventDispatcher, 'associate_logout', onAssociateLogout);

  $.listenTo(Alloy.eventDispatcher, 'tests:run', onTestsRun);

  Ti.Network.addEventListener('change', networkDidChange);











  function init() {
    var deferred = new _.Deferred();

    if (loading) {
      deferred.reject();
      return deferred.promise();
    }
    loading = true;

    removeNotify();

    allowAppSleep(true);


    loadConfigurations(true).done(function () {

      if ($.appIndex) {
        switchToIndex().always(function () {
          doStartupValidations().done(function () {
            deferred.resolve();
          }).fail(function () {
            deferred.reject();
          });
        });
      } else {
        doStartupValidations().done(function () {
          deferred.resolve();
        }).fail(function () {
          deferred.reject();
        });
      }
    }).fail(function () {
      deferred.reject();
      failStartup(_L('Unable to load application settings.'));
    });

    return deferred.promise();
  }










  function doStartupValidations() {
    var deferred = new _.Deferred();
    logger.info('Doing startup');
    if (Alloy.CFG.languageSelected) {
      Ti.Locale.setLanguage(Alloy.CFG.languageSelected);
    }
    validateAppSettings().fail(function () {
      failStartup(_L('Unable to validate application settings.'));
      deferred.reject();
    }).done(function () {
      validateNetwork().fail(function () {
        failStartup(_L('Unable to connect to a network.'));
        deferred.reject();
      }).done(function () {
        validateStorefront().fail(function (responseText) {
          failStartup(_L('Unable to connect to storefront.'), responseText);
          deferred.reject();
        }).done(function () {

          welcomeMessage().always(function () {
            validateDevicesStartup().fail(function (response) {
              failStartup(response.faultDescription, response);
              deferred.reject();
            }).done(function () {

              loadModels(_L('Loading Countries...'), 'countriesStates').fail(function () {

                deferred.reject();
              }).done(function () {
                notify(_L('Initializing Application...'), {
                  timeout: notifyDelayStartup });

                startApplication();
                deferred.resolve();
              });
            });
          });
        });
      });
    });
    return deferred.promise();
  }







  function showKioskMessage(enable) {
    if (EAUtils.isSymbolBasedLanguage() || EAUtils.isLatinBasedLanguage()) {
      $.kiosk_message.setFont(Alloy.Styles.calloutFont);
    }

    $.kiosk_message.setText(_L('Touch Screen to Begin'));
    if (enable) {
      $.kiosk_message.show();
    } else {
      $.kiosk_message.hide();
    }
  }









  function failStartup(message, fault, loginOnly) {
    loading = false;
    removeNotify();
    var messageString = _L('Reason: ') + message;

    if (fault && fault.stores) {
      Alloy.Dialog.showCustomDialog({
        controllerPath: 'components/startupPopover',
        initOptions: {
          message: messageString,
          fault: fault },

        continueEvent: 'startupPopover:dismiss',
        continueFunction: function () {
          Alloy.eventDispatcher.trigger('app:startup');
        },
        cancelEvent: 'startupPopover:appConfiguration',
        cancelFunction: function () {
          showAppConfigurationDialog();
        } });

    } else {
      if (_.isString(fault)) {
        messageString += '\n\n' + _L('Message: ') + fault;
      }
      Alloy.Dialog.showConfirmationDialog({
        messageString: messageString,
        titleString: _L('Unable to start the application'),
        okButtonString: _L('Retry'),
        cancelButtonString: _L('Configuration'),
        cancelFunction: function () {
          showAppConfigurationDialog(loginOnly);
        },
        okFunction: function () {
          removeNotify();
          if (loginOnly) {
            Alloy.eventDispatcher.trigger('app:login');
          } else {
            Alloy.eventDispatcher.trigger('app:startup');
          }
        } });

    }
  }







  function showAppConfigurationDialog(isLogin) {
    Alloy.Dialog.showCustomDialog({
      controllerPath: 'support/components/appConfigurationDialog',
      continueEvent: 'appConfigurationDialog:dismiss',
      continueFunction: function () {
        if (isLogin) {
          Alloy.eventDispatcher.trigger('app:login');
        } else {
          Alloy.eventDispatcher.trigger('app:startup');
        }
      } });

  }







  function welcomeMessage() {
    var deferred = new _.Deferred();



    if (appSettings.dbExists() && Alloy.CFG.countrySelected && countryConfig[Alloy.CFG.countrySelected] && Alloy.CFG.store_id && Alloy.CFG.appCurrency) {
      deferred.resolve();
      return deferred.promise();
    }
    loading = false;
    removeNotify();
    Alloy.Dialog.showCustomDialog({
      controllerPath: 'components/welcomePopover',
      continueEvent: 'welcomePopover:dismiss',
      continueFunction: function () {
        deferred.resolve();
      } });

    return deferred.promise();
  }







  function presentLogin(event) {

    loadConfigurations().done(function () {
      var continueLogin = function () {
        if (loadStartupModules()) {
          Alloy.eventDispatcher.trigger('configurations:prelogin');
          removeNotify();
          if (isKioskMode()) {
            validateKioskCredentials();
          } else {
            if (!Alloy.CFG.country_configuration || !Alloy.CFG.country_configuration[Alloy.CFG.countrySelected] || !Alloy.CFG.country_configuration[Alloy.CFG.countrySelected].list_price_book) {
              failStartup(String.format(_L('Price book mapping missing from Endless Aisle Preferences for the value \'%s\'.'), Alloy.CFG.countrySelected));
            } else {
              presentAssociateLogin(event);
            }
          }
        }
      };
      if (Alloy.CFG.navigation.use_mega_menu) {

        loadModels(null, 'bootstrapModels', true).done(function () {
          continueLogin();
        });
      } else {
        continueLogin();
      }
    }).fail(function () {
      failStartup(_L('Unable to load application settings.'));
    });
  }







  function presentAssociateLogin(info) {
    var associate_login = Alloy.Dialog.showCustomDialog({
      controllerPath: 'associate/login',
      initOptions: info,
      ignoreHideAuxillary: true,
      fadeOut: true,
      continueEvent: 'login:dismiss',
      continueFunction: function (event) {
        $.backdrop.setOpacity(0);
        if (event) {
          var location = info ? info.location : null;
          var promise = postLogin(event.employee_id, location);
          showActivityIndicator(promise);
        }
      } });

    $.backdrop.animate({
      opacity: 0.5,
      duration: 500 },
    function () {
      animation.popIn(associate_login.getView());
    });
  }










  function postAuthorization(dialog) {
    var cfgSettings = Alloy.createModel('cfgSettings');
    var promise = cfgSettings.loadKioskServerConfigs(Alloy.CFG.store_id);
    promise.done(function () {
      var serverSettings = cfgSettings.getSettings();
      if (serverSettings) {
        appSettings.setSetting('kiosk_mode.username', serverSettings.kiosk_mode.username);
        appSettings.setSetting('kiosk_mode.password', serverSettings.kiosk_mode.password);
      }
      Alloy.CFG.kiosk_mode.associate_credentials_entered = true;

      storePasswordHelpers.checkStorePasswordWarning(true).always(function () {

        showKioskMessage(true);
      });
    });
    promise.fail(function (model) {
      dialog.showErrorMessage(_L('Unable to retrieve kiosk settings'));
    });

    return promise;
  }






  function validateKioskCredentials() {
    if (Alloy.CFG.kiosk_mode.associate_credentials_entered) {
      showKioskMessage(true);
    } else {
      Alloy.Dialog.showCustomDialog({
        controllerPath: 'associate/authorization',
        initOptions: {
          subTitleText: _L('Associate Login To Enable Kiosk'),
          successMessage: _L('Manager Credentials Accepted'),
          showCancelButton: false,
          postAuthorizationFunction: postAuthorization },

        continueEvent: 'authorization:dismiss',
        continueFunction: function (result) {
          if (result && !result.result) {
            failStartup(_L('Unable to login kiosk user.'), null, true);
          }
        } });

    }
  }






  function kioskLogin() {
    if (!$.kiosk_message.getVisible()) {
      return;
    }
    logger.info('kioskLogin');

    showKioskMessage(false);

    var deferred = new _.Deferred();
    showActivityIndicator(deferred);
    var loginInfo = {
      employee_id: Alloy.CFG.kiosk_mode.username,
      passcode: Alloy.CFG.kiosk_mode.password };

    Alloy.Models.associate.loginAssociate(loginInfo).done(function () {
      postLogin(Alloy.CFG.kiosk_mode.username).always(function () {
        deferred.resolve();
      });
    }).fail(function () {
      deferred.resolve();

      failStartup(_L('Unable to login kiosk user.'), null, true);
    });
  }









  function postLogin(employee_code, location) {
    var deferred = new _.Deferred();

    Alloy.Models.authorizationToken.fetchToken(Alloy.CFG.storefront_host).done(function (model) {
      Alloy.Models.basket.getBasket({
        c_eaEmployeeId: employee_code }).
      done(function () {

        loadConfigurations().done(function () {
          analytics.fireAnalyticsEvent({
            category: _L('Users'),
            action: _L('Associate Login'),
            label: employee_code });

          analytics.fireUserEvent({
            category: _L('Users'),
            action: _L('Associate Login'),
            userId: employee_code });

          analytics.fireAnalyticsEvent({
            category: _L('Store'),
            action: _L('Login'),
            label: Alloy.Models.storeInfo.getId() });


          switchToAppIndex().always(function () {
            setTimeout(function () {

              if (location && location.route != 'associate_login') {
                logger.info('Redirecting to ' + JSON.stringify(location));
                Alloy.Router.navigate(location);
              } else {
                Alloy.Router.navigateToHome();
              }
              Alloy.eventDispatcher.trigger('app:ready');
              deferred.resolve();
            }, 0);
          });


          Alloy.eventDispatcher.trigger('configurations:postlogin');
        }).fail(function () {
          deferred.resolve();
          failStartup(_L('Unable to load application settings.'), null, true);
        });
      }).fail(function () {
        deferred.resolve();
        failStartup(_L('Unable to start the application'), null, true);
      }).always(function () {
        deferred.resolve();
      });
    }).fail(function (fault) {
      deferred.resolve();
      if (fault && (fault.isBadLogin() || fault.isEmptyClientCredentials())) {
        handleAuthorizationTokenError(fault);
      } else {
        failStartup(_L('Unable to start the application'), null, true);
      }
    });
    return deferred.promise();
  }











  function loadModels(label, modelName, login) {
    var deferred = new _.Deferred();
    login = login || false;
    if (label) {
      notify(label, {
        timeout: notifyDelayStartup });

    }
    setTimeout(function () {
      logger.trace('Loading ' + modelName);
      require(modelName).done(function () {
        logger.trace('Done loading ' + modelName);
        deferred.resolve();
      }).fail(function (model) {
        var fault = null;
        if (model && model.has('fault')) {
          var faultAttrs = model.get('fault');
          fault = faultAttrs.message ? faultAttrs.message : null;
        }
        failStartup(String.format(_L('Unable to load \'%s\'.'), modelName), fault, login);
        deferred.reject();
      });
    }, notifyReadDelay);
    return deferred.promise();
  }






  function startApplication() {
    setTimeout(function () {
      loading = false;
      Alloy.TopController = $;
      Alloy.TopWindow = $.index;
      Alloy.eventDispatcher.trigger('app:login');

    }, notifyReadDelay);
  }






  function doAppLogin(event) {
    logger.info('doAppLogin - app:login fired');
    Alloy.Globals.resetCookies();
    allowAppSleep(true);
    if ($.appIndex) {

      switchToIndex().always(function () {
        presentLogin(event);
      });
    } else {

      presentLogin(event);
    }
  }







  function onTouchApp() {
    logger.info('*** touchstart on topwindow');
    Alloy.eventDispatcher.trigger('session:renew');
  }







  function switchToIndex() {
    logger.info('switchToIndex called');
    var deferred = new _.Deferred();

    Alloy.Dialog.setWindow($.index);
    $.index.open();
    $.appIndex.removeEventListener('touchstart', onTouchApp);

    animation.crossFade($.appIndex, $.index, 500, function () {
      logger.info('closing and deinit appIndex');
      if ($.appIndex) {
        $.appIndex.close();
      }
      if (appIndexController) {
        appIndexController.deinit();
      }
      $.appIndex = null;
      deferred.resolve();
    });
    return deferred.promise();
  }







  function switchToAppIndex() {
    logger.info('switchToAppIndex called');
    var deferred = new _.Deferred();
    allowAppSleep(false);

    logger.info('creating new appIndexController');
    appIndexController = Alloy.createController('appIndex');
    Alloy.AppWindow = $.appIndex = appIndexController.getView();

    Alloy.Dialog.setWindow($.appIndex);
    appIndexController.init();
    Alloy.Router = appIndexController.router;
    Alloy.ViewManager = appIndexController.viewManager;
    $.appIndex.open();
    $.appIndex.addEventListener('touchstart', onTouchApp);
    animation.crossFade($.index, $.appIndex, 100, function () {
      $.index.close();
      deferred.resolve();
    });
    return deferred.promise();
  }






  function networkDidChange() {
    if (Ti.Network.online) {
      removeNotify(_L('Network is unavailable'));
      notify(_L('Network is now available'));
    } else {
      notify(_L('Network is unavailable'), {
        preventAutoClose: true });

    }
  }









  function executeModelTests(testNames, junit_file_location, logFunc) {
    setTimeout(function () {
      var behave = require('behave');
      var testsToLoad = testNames || Alloy.CFG.active_tests;

      if (testsToLoad && testsToLoad.length > 0) {
        _.each(testsToLoad, function (testName) {
          try {
            var spec = require('spec/' + testName);
            spec.define();
          } catch (exception) {
            if (exception instanceof Error && exception.code === 'MODULE_NOT_FOUND') notify(testName + ' is not a test that can be executed.');else throw exception;
          }
        });
      }

      behave.run(junit_file_location, logFunc);
    }, 1000);
  }







  function onConfigsPreLogin() {
    logger.info('onConfigsPreLogin called');

    Alloy.Models.product = Alloy.Models.product || Alloy.createModel('product');
    Alloy.Models.productSearch = Alloy.Models.productSearch || Alloy.createModel('productSearch');
    Alloy.Models.category = Alloy.Models.category || Alloy.createModel('category');
    Alloy.Models.customer = Alloy.Models.customer || Alloy.createModel('customer');
    Alloy.Models.associate = Alloy.Models.associate || Alloy.createModel('associate');
    Alloy.Models.basket = Alloy.Models.basket || Alloy.createModel('baskets');
    Alloy.Models.customerAddress = Alloy.Models.customerAddress || Alloy.createModel('customerAddress');
    Alloy.Models.customerOrder = Alloy.Models.customerOrder || Alloy.createModel('baskets');
    Alloy.Collections.customerOrderHistory = Alloy.Collections.customerOrderHistory || Alloy.createCollection('customerOrderHistory');
    Alloy.Models.storeInfo = Alloy.Models.storeInfo || Alloy.createModel('store');
    Alloy.Models.authorizationToken = Alloy.Models.authorizationToken || Alloy.createModel('authorizationToken');


    Alloy.Collections.allStores = Alloy.Collections.allStores || Alloy.createCollection('store');


    Alloy.Collections.history = Alloy.Collections.history || Alloy.createCollection('history');
    Alloy.Collections.customer = Alloy.Collections.customer || Alloy.createCollection('customer');
  }







  function loadStartupModules() {
    if (Alloy.CFG.devices.payment_terminal_module && !Alloy.paymentTerminal) {

      Alloy.paymentTerminal = loadModule(Alloy.CFG.devices.payment_terminal_module);
      if (Alloy.paymentTerminal) {
        if (!_.isFunction(Alloy.paymentTerminal.supports)) {

          failStartup(String.format(_L('Unable to load \'%s\'.'), Alloy.CFG.devices.payment_terminal_module));

          Alloy.paymentTerminal = null;
          return false;
        }
      } else {
        return false;
      }
    }
    if (Alloy.CFG.enable_barcode_scanner && !Alloy.barcodeScanner) {

      Alloy.barcodeScanner = loadModule('barcodeScanner');
      if (!Alloy.barcodeScanner) {
        return false;
      }
    }
    if (Alloy.CFG.printer_availability && !Alloy.receiptPrinter) {

      Alloy.receiptPrinter = loadModule(Alloy.CFG.devices.printer_module);
      if (!Alloy.receiptPrinter) {
        return false;
      }
    }
    if (Alloy.CFG.payment_entry === 'web') {
      var safariDialog = loadModule('ti.safaridialog');
      if (safariDialog) {
        var isSupported = safariDialog.isSupported();
        logger.info('ti.safaridialog.isSupported() ' + isSupported);

        if (!isSupported) {
          failStartup(_L('This version of iOS is not supported.  Please upgrade to a supported version.'));
          return false;
        }
      } else {

        return false;
      }
    }
    return true;
  }








  function loadModule(moduleName) {
    var module = null;
    try {
      logger.info('loading module: "' + moduleName + '"');
      module = require(moduleName);
      if (!module) {
        failStartup(String.format(_L('Unable to load \'%s\'.'), moduleName));
      }
    } catch (ex) {
      logger.error('cannot load module: "' + moduleName + '", exception: ' + ex);
      failStartup(ex);
    }
    return module;
  }






  function onConfigsPostLogin() {
    logger.info('onConfigsPostLogin called');


    Alloy.Models.storeInfo.getStore(Alloy.CFG.store_id).done(function () {
      var collectingBillingAddress = Alloy.CFG.collect_billing_address || Alloy.CFG.devices.payment_terminal_module === 'webDevice';
      if (!collectingBillingAddress || Alloy.CFG.printer_availability || Alloy.CFG.store_availability.enabled) {

        loadAllStores();
      }
      if (Alloy.Models.storeInfo.getLatitude() == null && Alloy.Models.storeInfo.getLongitude() == null) {

        getCurrentLocation();
      }
    });
  }






  function loadAllStores() {
    if (Alloy.CFG.store_availability.enabled) {
      Alloy.Collections.allStores.reset([]);
      Alloy.Collections.allStores.getAllStores().done(function () {
        logger.info('retrieved allStores');
        logger.secureLog(JSON.stringify(Alloy.Collections.allStores.models, null, 2));
      }).fail(function () {
        logger.error('failure retrieving allStores');
      });
    }
  }






  function onAssociateLogout() {
    Alloy.eventDispatcher.trigger('configurations:unload');
    if (Alloy.CFG.store_availability.enabled) {
      Alloy.Collections.allStores.reset([]);
    }
  }







  function onTestsRun(event) {
    var behaveLogger = require('behaveLoggerExt');
    executeModelTests(event.tests, event.junit_file_location, behaveLogger.getLoggerFunction());
  }







  function handleAuthorizationTokenError(fault) {
    var title = _L('Unable to start the application');
    var message = _L('Unable to start the application');
    if (fault) {
      if (fault.isBadLogin()) {
        title = _L('Store password incorrect');
        message = _L('The password for the store is incorrect. Please contact your admininstrator to fix this problem.');
      } else if (fault.isEmptyClientCredentials()) {
        title = _L('Client Credentials Not Set Up');
        message = _L('The client credentials are not set up correctly. Please contact your admininstrator to fix this problem.');
      }
    }

    Alloy.Dialog.showConfirmationDialog({
      messageString: message,
      titleString: title,
      okButtonString: _L('Restart App'),
      hideCancel: true,
      okFunction: function () {
        removeNotify();
        restartAppAfterTokenFailure();
      } });

  }






  function restartAppAfterTokenFailure() {
    var deferred = new _.Deferred();
    showActivityIndicator(deferred);


    if (Alloy.Models.basket.getOrderNumber()) {
      logger.info('has order number');
      Alloy.Models.basket.simpleAbandonOrder().always(function () {
        doLogout(deferred);
      });
    } else {
      doLogout(deferred);
    }
  }







  function doLogout(deferred) {
    Alloy.Collections.history.reset([]);

    Alloy.Models.basket.clear();
    delete Alloy.Models.basket.shippingMethods;
    Alloy.Models.basket.trigger('basket_sync');
    Alloy.Models.associate.logout().always(function () {
      logger.info('associateLogout done()');
      Alloy.eventDispatcher.trigger('associate_logout');

      if (Alloy.Router) {
        Alloy.Router.navigateToHome();
      }
      Alloy.eventDispatcher.trigger('app:login', {});
      deferred.resolve();
    });
  }






  function getCurrentLocation() {
    if (!Alloy.CFG.enable_address_autocomplete) {
      return;
    }
    geolocation.getCurrentLocation(function (coordinates) {
      Alloy.CFG.latitudeOnStartup = coordinates.latitude;
      Alloy.CFG.longitudeOnStartup = coordinates.longitude;
    });
  }




  $.index.open();
  Alloy.Dialog.setWindow($.index);
  init().done(function () {
    if (Ti.App.deployType !== 'production' && Alloy.CFG.perform_tests) {
      executeModelTests(undefined, Alloy.CFG.junit_file_location);
    }
  });





  __defers['$.__views.background!click!kioskLogin'] && $.addListener($.__views.background, 'click', kioskLogin);__defers['$.__views.kiosk_message!click!kioskLogin'] && $.addListener($.__views.kiosk_message, 'click', kioskLogin);



  _.extend($, exports);
}

module.exports = Controller;