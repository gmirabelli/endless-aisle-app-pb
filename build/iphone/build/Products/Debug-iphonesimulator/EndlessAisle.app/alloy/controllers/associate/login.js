var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'associate/login';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.associate_login_window = Ti.UI.createView(
  { opacity: 0, id: "associate_login_window" });

  $.__views.associate_login_window && $.addTopLevelView($.__views.associate_login_window);
  $.__views.contents = Ti.UI.createView(
  { top: 22, width: 465, height: 600, borderWidth: 1, borderColor: Alloy.Styles.color.border.dark, layout: "vertical", backgroundColor: Alloy.Styles.color.background.white, id: "contents" });

  $.__views.associate_login_window.add($.__views.contents);
  $.__views.login_title = Ti.UI.createLabel(
  { width: 260, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.titleFont, top: 26, textid: "_Associate_Login", id: "login_title", accessibilityValue: "login_title" });

  $.__views.contents.add($.__views.login_title);
  $.__views.login_subtitle_label = Ti.UI.createLabel(
  { width: 260, height: 50, color: Alloy.Styles.color.text.medium, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.appFont, verticalAlign: Ti.UI.TEXT_VERTICAL_ALIGNMENT_TOP, bottom: 1, textid: "_Enter_your_credentials", id: "login_subtitle_label", accessibilityValue: "login_subtitle_label" });

  $.__views.contents.add($.__views.login_subtitle_label);
  $.__views.login_error_label = Ti.UI.createLabel(
  { width: 320, height: 0, color: Alloy.Styles.color.text.red, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.calloutCopyFont, visible: false, bottom: 10, textid: "_Login_attempt_failed__Re_enter_your_ID_and_password_", id: "login_error_label", accessibilityValue: "login_error_label" });

  $.__views.contents.add($.__views.login_error_label);
  $.__views.employee_code = Ti.UI.createTextField(
  { passwordMask: true, color: Alloy.Styles.color.text.dark, borderColor: Alloy.Styles.color.border.light, borderWidth: 1, keyboardType: Ti.UI.KEYBOARD_TYPE_NUMBERS_PUNCTUATION, returnKeyType: Ti.UI.RETURNKEY_GO, left: 32, bottom: 15, width: 400, font: Alloy.Styles.dialogFieldFont, padding: { left: 18 }, height: 55, hintText: L('_Associate_ID'), id: "employee_code", accessibilityLabel: "employee_code" });

  $.__views.contents.add($.__views.employee_code);
  $.__views.employee_pin = Ti.UI.createTextField(
  { passwordMask: true, color: Alloy.Styles.color.text.dark, borderColor: Alloy.Styles.color.border.light, borderWidth: 1, keyboardType: Ti.UI.KEYBOARD_TYPE_NUMBERS_PUNCTUATION, returnKeyType: Ti.UI.RETURNKEY_GO, left: 32, bottom: 0, width: 400, font: Alloy.Styles.dialogFieldFont, padding: { left: 18 }, height: 55, hintText: L('_Password'), id: "employee_pin", accessibilityLabel: "employee_pin" });

  $.__views.contents.add($.__views.employee_pin);
  $.__views.forgot_password_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.accentColor, textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT, textid: "_Forgot_Password", right: 36, font: Alloy.Styles.detailValueFont, top: 10, bottom: 10, id: "forgot_password_label", accessibilityValue: "forgot_password_label" });

  $.__views.contents.add($.__views.forgot_password_label);
  $.__views.login_button = Ti.UI.createButton(
  { height: 55, backgroundImage: Alloy.Styles.primaryButtonImage, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.bigButtonFont, color: Alloy.Styles.buttons.primary.color, left: 32, width: 400, bottom: 8, titleid: "_Login", id: "login_button", accessibilityValue: "login_button" });

  $.__views.contents.add($.__views.login_button);
  $.__views.storefront_view = Ti.UI.createView(
  { layout: "horizontal", height: 40, left: 80, id: "storefront_view" });

  $.__views.contents.add($.__views.storefront_view);
  $.__views.storefront_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.medium, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.detailValueFont, top: 0, bottom: 10, id: "storefront_label" });

  $.__views.storefront_view.add($.__views.storefront_label);
  $.__views.change_country_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.accentColor, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.detailValueFont, top: 0, bottom: 10, id: "change_country_label", accessibilityValue: "change_country_label" });

  $.__views.storefront_view.add($.__views.change_country_label);
  $.__views.country_selector = Ti.UI.createView(
  { top: 22, width: 465, height: 600, borderWidth: 1, borderColor: Alloy.Styles.color.border.dark, layout: "vertical", backgroundColor: Alloy.Styles.color.background.white, id: "country_selector" });

  $.__views.associate_login_window.add($.__views.country_selector);
  $.__views.country_label = Ti.UI.createLabel(
  { width: 260, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.titleFont, top: 26, textid: "_Select_Country_and_Language", id: "country_label", accessibilityValue: "country_label" });

  $.__views.country_selector.add($.__views.country_label);
  $.__views.country_view = Ti.UI.createView(
  { left: 32, width: 400, paddingLeft: 18, top: 60, height: 55, id: "country_view" });

  $.__views.country_selector.add($.__views.country_view);
  $.__views.country_dropdown_view = Alloy.createController('components/countryDropDown', { id: "country_dropdown_view", __parentSymbol: $.__views.country_view });
  $.__views.country_dropdown_view.setParent($.__views.country_view);
  $.__views.language_view = Ti.UI.createView(
  { left: 32, width: 400, paddingLeft: 18, height: 55, id: "language_view" });

  $.__views.country_selector.add($.__views.language_view);
  $.__views.language_dropdown_view = Alloy.createController('components/languageDropDown', { id: "language_dropdown_view", __parentSymbol: $.__views.language_view });
  $.__views.language_dropdown_view.setParent($.__views.language_view);
  $.__views.assoc_button_container = Ti.UI.createView(
  { layout: "horizontal", height: 63, top: 30, id: "assoc_button_container" });

  $.__views.country_selector.add($.__views.assoc_button_container);
  $.__views.country_cancel_button = Ti.UI.createButton(
  { height: 55, backgroundImage: Alloy.Styles.secondaryButtonImage, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.tabFont, color: Alloy.Styles.buttons.primary.color, left: 32, width: 195, bottom: 8, titleid: "_Cancel", id: "country_cancel_button", accessibilityValue: "country_cancel_button" });

  $.__views.assoc_button_container.add($.__views.country_cancel_button);
  $.__views.country_apply_button = Ti.UI.createButton(
  { height: 55, backgroundImage: Alloy.Styles.primaryButtonImage, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.tabFont, color: Alloy.Styles.buttons.primary.color, left: 10, width: 195, bottom: 8, titleid: "_Apply", id: "country_apply_button", accessibilityValue: "country_apply_button" });

  $.__views.assoc_button_container.add($.__views.country_apply_button);
  $.__views.associate = Ti.UI.createView(
  { top: 22, width: 465, height: 600, borderWidth: 1, borderColor: Alloy.Styles.color.border.dark, layout: "vertical", backgroundColor: Alloy.Styles.color.background.white, id: "associate" });

  $.__views.associate_login_window.add($.__views.associate);
  $.__views.assoc_title = Ti.UI.createLabel(
  { width: 260, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.titleFont, top: 26, textid: "_Change_Password", id: "assoc_title", accessibilityValue: "assoc_title" });

  $.__views.associate.add($.__views.assoc_title);
  $.__views.assoc_subtitle_label = Ti.UI.createLabel(
  { width: 260, height: 50, color: Alloy.Styles.color.text.medium, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.appFont, verticalAlign: Ti.UI.TEXT_VERTICAL_ALIGNMENT_TOP, bottom: 1, textid: "_Enter_Associate_ID", id: "assoc_subtitle_label", accessibilityValue: "assoc_subtitle_label" });

  $.__views.associate.add($.__views.assoc_subtitle_label);
  $.__views.assoc_error_label = Ti.UI.createLabel(
  { width: 320, height: 0, color: Alloy.Styles.color.text.red, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.calloutCopyFont, visible: false, bottom: 10, textid: "_Login_attempt_failed__Re_enter_your_ID_and_password_", id: "assoc_error_label", accessibilityValue: "password_reset_associate_error_label" });

  $.__views.associate.add($.__views.assoc_error_label);
  $.__views.assoc_code = Ti.UI.createTextField(
  { passwordMask: true, color: Alloy.Styles.color.text.dark, borderColor: Alloy.Styles.color.border.light, borderWidth: 1, keyboardType: Ti.UI.KEYBOARD_TYPE_NUMBERS_PUNCTUATION, returnKeyType: Ti.UI.RETURNKEY_GO, left: 32, bottom: 15, width: 400, font: Alloy.Styles.dialogFieldFont, padding: { left: 18 }, height: 55, hintText: L('_Associate_ID'), id: "assoc_code", accessibilityLabel: "password_reset_associate_id" });

  $.__views.associate.add($.__views.assoc_code);
  $.__views.assoc_button_container = Ti.UI.createView(
  { layout: "horizontal", height: 63, top: 30, id: "assoc_button_container" });

  $.__views.associate.add($.__views.assoc_button_container);
  $.__views.assoc_cancel_button = Ti.UI.createButton(
  { height: 55, backgroundImage: Alloy.Styles.secondaryButtonImage, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.tabFont, color: Alloy.Styles.buttons.primary.color, left: 32, width: 195, bottom: 8, titleid: "_Cancel", id: "assoc_cancel_button", accessibilityValue: "password_reset_associate_cancel" });

  $.__views.assoc_button_container.add($.__views.assoc_cancel_button);
  $.__views.assoc_button = Ti.UI.createButton(
  { height: 55, backgroundImage: Alloy.Styles.primaryButtonImage, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.tabFont, color: Alloy.Styles.buttons.primary.color, left: 10, width: 195, bottom: 8, titleid: "_Reset_Password", id: "assoc_button", accessibilityValue: "password_reset_associate_ok" });

  $.__views.assoc_button_container.add($.__views.assoc_button);
  $.__views.manager = Ti.UI.createView(
  { top: 22, width: 465, height: 600, borderWidth: 1, borderColor: Alloy.Styles.color.border.dark, layout: "vertical", backgroundColor: Alloy.Styles.color.background.white, id: "manager" });

  $.__views.associate_login_window.add($.__views.manager);
  $.__views.manager_view = Alloy.createController('associate/authorization', { id: "manager_view", __parentSymbol: $.__views.manager });
  $.__views.manager_view.setParent($.__views.manager);
  $.__views.new_password = Ti.UI.createView(
  { top: 22, width: 465, height: 600, borderWidth: 1, borderColor: Alloy.Styles.color.border.dark, layout: "vertical", backgroundColor: Alloy.Styles.color.background.white, id: "new_password" });

  $.__views.associate_login_window.add($.__views.new_password);
  $.__views.new_password_title = Ti.UI.createLabel(
  { width: 260, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.titleFont, top: 26, textid: "_Change_Password", id: "new_password_title", accessibilityValue: "new_password_title" });

  $.__views.new_password.add($.__views.new_password_title);
  $.__views.new_password_subtitle_label = Ti.UI.createLabel(
  { width: 260, height: 50, color: Alloy.Styles.color.text.medium, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.appFont, verticalAlign: Ti.UI.TEXT_VERTICAL_ALIGNMENT_TOP, bottom: 1, textid: "_Enter_New_Password", id: "new_password_subtitle_label", accessibilityValue: "new_password_subtitle_label" });

  $.__views.new_password.add($.__views.new_password_subtitle_label);
  $.__views.new_password_error_label = Ti.UI.createLabel(
  { width: 320, height: 0, color: Alloy.Styles.color.text.red, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.calloutCopyFont, visible: false, bottom: 10, textid: "_Login_attempt_failed__Re_enter_your_ID_and_password_", id: "new_password_error_label", accessibilityValue: "new_password_error_label" });

  $.__views.new_password.add($.__views.new_password_error_label);
  $.__views.password = Ti.UI.createTextField(
  { passwordMask: true, color: Alloy.Styles.color.text.dark, borderColor: Alloy.Styles.color.border.light, borderWidth: 1, keyboardType: Ti.UI.KEYBOARD_TYPE_NUMBERS_PUNCTUATION, returnKeyType: Ti.UI.RETURNKEY_GO, left: 32, bottom: 15, width: 400, font: Alloy.Styles.dialogFieldFont, padding: { left: 18 }, height: 55, hintText: L('_Enter_New_Password'), id: "password", accessibilityLabel: "reset_new_password" });

  $.__views.new_password.add($.__views.password);
  $.__views.password_verify = Ti.UI.createTextField(
  { passwordMask: true, color: Alloy.Styles.color.text.dark, borderColor: Alloy.Styles.color.border.light, borderWidth: 1, keyboardType: Ti.UI.KEYBOARD_TYPE_NUMBERS_PUNCTUATION, returnKeyType: Ti.UI.RETURNKEY_GO, left: 32, bottom: 15, width: 400, font: Alloy.Styles.dialogFieldFont, padding: { left: 18 }, height: 55, hintText: L('_Repeat_New_Password'), id: "password_verify", accessibilityLabel: "reset_new_password_verify" });

  $.__views.new_password.add($.__views.password_verify);
  $.__views.__alloyId12 = Ti.UI.createView(
  { layout: "horizontal", height: 63, id: "__alloyId12" });

  $.__views.new_password.add($.__views.__alloyId12);
  $.__views.new_password_cancel_button = Ti.UI.createButton(
  { height: 55, backgroundImage: Alloy.Styles.secondaryButtonImage, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.tabFont, color: Alloy.Styles.buttons.primary.color, left: 32, width: 195, bottom: 8, titleid: "_Cancel", id: "new_password_cancel_button", accessibilityValue: "new_password_cancel_button" });

  $.__views.__alloyId12.add($.__views.new_password_cancel_button);
  $.__views.new_password_button = Ti.UI.createButton(
  { height: 55, backgroundImage: Alloy.Styles.primaryButtonImage, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.tabFont, color: Alloy.Styles.buttons.primary.color, left: 10, width: 195, bottom: 8, titleid: "_Change_Password", id: "new_password_button", accessibilityValue: "new_password_button" });

  $.__views.__alloyId12.add($.__views.new_password_button);
  $.__views.success = Ti.UI.createView(
  { top: 22, width: 465, height: 600, borderWidth: 1, borderColor: Alloy.Styles.color.border.dark, layout: "vertical", backgroundColor: Alloy.Styles.color.background.white, id: "success" });

  $.__views.associate_login_window.add($.__views.success);
  $.__views.success_title = Ti.UI.createLabel(
  { width: 260, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.titleFont, top: 26, textid: "_Success", id: "success_title", accessibilityValue: "success_title" });

  $.__views.success.add($.__views.success_title);
  $.__views.success_subtitle_label = Ti.UI.createLabel(
  { width: 260, height: 50, color: Alloy.Styles.color.text.medium, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.appFont, verticalAlign: Ti.UI.TEXT_VERTICAL_ALIGNMENT_TOP, bottom: 1, textid: "_Password_Successfully_Changed", top: 83, id: "success_subtitle_label", accessibilityValue: "success_subtitle_label" });

  $.__views.success.add($.__views.success_subtitle_label);
  $.__views.success_ok_button = Ti.UI.createButton(
  { height: 55, backgroundImage: Alloy.Styles.primaryButtonImage, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.tabFont, color: Alloy.Styles.buttons.primary.color, left: 32, width: 400, bottom: 8, top: 80, titleid: "_OK", id: "success_ok_button", accessibilityValue: "success_ok_button" });

  $.__views.success.add($.__views.success_ok_button);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var appSettings = require('appSettings');
  var currentAssociate = Alloy.Models.associate;

  var showActivityIndicator = require('dialogUtils').showActivityIndicator;
  var logger = require('logging')('associate:login', getFullControllerPath($.__controllerPath));
  var countryConfig = require('config/countries').countryConfig;
  var languageConfig = require('config/countries').languageConfig;
  var EAUtils = require('EAUtils');
  var tempCountrySelected;
  var tempLanguageSelected;
  var tempLanguageOcapiLocale;
  var tempLanguageStorefrontLocale;
  var viewHeight = 362;
  var manager = null;
  var payload = null;
  var forgotPasswordTimer = null;


  var errorLabelLength = 60;
  var symbolErrorLabelLength = 27;
  var symbolButtonTextLength = 9;




  $.employee_code.addEventListener('return', doLogin);
  $.employee_pin.addEventListener('return', doLogin);


  $.login_button.addEventListener('click', doLogin);

  if (Alloy.CFG.show_forgot_password_link) {
    $.forgot_password_label.addEventListener('click', doForgotPassword);

    $.assoc_button.addEventListener('click', validateAssociateExists);
    $.new_password_button.addEventListener('click', changePassword);
    $.success_ok_button.addEventListener('click', resetLoginForm);

    $.assoc_cancel_button.addEventListener('click', resetLoginForm);
    $.new_password_cancel_button.addEventListener('click', resetLoginForm);
  }

  if (Alloy.CFG.login_change_country_link) {

    $.change_country_label.addEventListener('click', onChangeCountryClick);
    $.country_apply_button.addEventListener('click', onCountryApplyClick);
    $.country_cancel_button.addEventListener('click', resetLoginForm);
  }

  if (EAUtils.isSymbolBasedLanguage()) {
    $.success_subtitle_label.setFont(Alloy.Styles.detailValueFont);
  } else {
    $.success_subtitle_label.setFont(Alloy.Styles.appFont);
  }




  exports.deinit = deinit;
  exports.init = init;










  function init(options) {
    logger.info('INIT');
    $.change_country_label.setHeight(0);
    $.change_country_label.setVisible(Alloy.CFG.login_change_country_link);
    if (Alloy.CFG.login_change_country_link) {
      var countrySelected = Alloy.CFG.countrySelected;
      var displayName = String.format(_L('%s (Change)'), _L(countryConfig[countrySelected].displayName));
      $.storefront_label.setText(_L('Current Country: '));
      $.change_country_label.setText(displayName);
      $.change_country_label.setHeight(Ti.UI.SIZE);
      $.country_dropdown_view.init();
      $.listenTo($.country_dropdown_view, 'country:change', onCountryChange);
      $.country_dropdown_view.updateCountrySelectedItem(countryConfig[countrySelected].value);
      $.language_dropdown_view.init();
      $.language_dropdown_view.populateLanguages(countrySelected);
      $.language_dropdown_view.updateLanguageSelectedItem(countrySelected);
      $.listenTo($.language_dropdown_view, 'language:change', onLanguageChange);
    } else {
      $.storefront_view.setHeight(0);
      $.login_button.setBottom(20);
    }
    $.forgot_password_label.setVisible(Alloy.CFG.show_forgot_password_link);

    $.toolbar = Alloy.createController('components/nextPreviousToolbar');
    $.toolbar.setTextFields([$.employee_code, $.employee_pin]);
    if (options && options.employee_id) {
      $.employee_code.setValue(options.employee_id);
      $.employee_pin.setValue('');
    }

    $.associate.hide();
    $.associate.setHeight(0);
    $.manager.hide();
    $.manager.setHeight(0);
    $.new_password.hide();
    $.new_password.setHeight(0);
    $.success.hide();
    $.success.setHeight(0);
    hideCountrySelector();
    if (EAUtils.isSymbolBasedLanguage() && $.assoc_button.getTitle().length > symbolButtonTextLength) {
      $.assoc_button.setFont(Alloy.Styles.calloutCopyFont);
      $.assoc_cancel_button.setFont(Alloy.Styles.calloutCopyFont);
    } else {
      $.assoc_button.setFont(Alloy.Styles.tabFont);
      $.assoc_cancel_button.setFont(Alloy.Styles.tabFont);
    }
    if (EAUtils.isLatinBasedLanguage()) {
      $.assoc_button.setFont(Alloy.Styles.detailTextCalloutFont);
      $.assoc_cancel_button.setFont(Alloy.Styles.detailTextCalloutFont);
    }
  }






  function deinit() {
    logger.info('DEINIT');

    $.toolbar && $.toolbar.deinit();

    $.employee_code.removeEventListener('return', doLogin);
    $.employee_pin.removeEventListener('return', doLogin);


    $.login_button.removeEventListener('click', doLogin);

    if (Alloy.CFG.show_forgot_password_link) {
      $.forgot_password_label.removeEventListener('click', doForgotPassword);

      $.assoc_button.removeEventListener('click', validateAssociateExists);
      $.new_password_button.removeEventListener('click', changePassword);
      $.success_ok_button.removeEventListener('click', resetLoginForm);

      $.assoc_cancel_button.removeEventListener('click', resetLoginForm);
      $.new_password_cancel_button.removeEventListener('click', resetLoginForm);
    }
    if (Alloy.CFG.login_change_country_link) {
      $.change_country_label.removeEventListener('click', onChangeCountryClick);
      $.country_apply_button.removeEventListener('click', onCountryApplyClick);
      $.country_cancel_button.removeEventListener('click', resetLoginForm);
      $.country_dropdown_view.deinit();
      $.language_dropdown_view.deinit();
    }

    $.manager_view.deinit();

    $.stopListening();
    $.destroy();
  }










  function onCountryChange(event) {
    tempCountrySelected = event.selectedCountry;
    if ($.language_dropdown_view && countryConfig[tempCountrySelected]) {
      $.language_dropdown_view.populateLanguages(tempCountrySelected);
      $.language_dropdown_view.updateLanguageSelectedValue(tempLanguageSelected);
      $.country_apply_button.setEnabled(true);
    } else {
      Alloy.Dialog.showConfirmationDialog({
        messageString: String.format(_L('There is no corresponding key for the value \'%s\' in countryConfig'), tempCountrySelected),
        titleString: _L('Configuration Error'),
        okButtonString: _L('OK'),
        hideCancel: true,
        okFunction: function () {
          return false;
        } });

      $.language_dropdown_view.populateLanguages();
      $.country_apply_button.setEnabled(false);
    }
  }







  function onLanguageChange(event) {
    if (event) {
      tempLanguageSelected = event.selectedLanguage;
      tempLanguageOcapiLocale = event.ocapiLocale;
      tempLanguageStorefrontLocale = event.storefrontLocale;
      $.country_apply_button.setEnabled(true);
    } else {
      $.country_apply_button.setEnabled(false);
    }
  }






  function onCountryApplyClick() {

    var promise = EAUtils.updateLocaleGlobalVariables(tempCountrySelected);
    showActivityIndicator(promise);
    promise.done(function () {

      Ti.Locale.setLanguage(tempLanguageSelected);
      Alloy.eventDispatcher.trigger('countryChange:selected');


      appSettings.setSetting('languageSelected', tempLanguageSelected);
      appSettings.setSetting('ocapi.default_locale', tempLanguageOcapiLocale);
      appSettings.setSetting('storefront.locale_url', tempLanguageStorefrontLocale);
      updateLabels();
      appSettings.setSetting('countrySelected', tempCountrySelected);
      hideCountrySelector();
      var displayName = String.format(_L('%s (Change)'), _L(countryConfig[Alloy.CFG.countrySelected].displayName));
      $.storefront_label.setText(_L('Current Country: '));
      $.change_country_label.setText(displayName);

      resetLoginForm();
    });
  }






  function onChangeCountryClick() {
    $.country_dropdown_view.updateCountrySelectedItem(countryConfig[Alloy.CFG.countrySelected].value);
    $.language_dropdown_view.updateLanguageSelectedItem(Alloy.CFG.languageSelected);
    showCountrySelector();
  }







  function updateLabels() {
    $.login_title.setText(_L('Associate Login'));
    $.login_subtitle_label.setText(_L('Enter your credentials'));
    $.employee_code.setHintText(_L('Associate ID'));
    $.employee_pin.setHintText(_L('Password'));
    $.login_button.setTitle(_L('Login'));
    $.forgot_password_label.setText(_L('Forgot Password'));
    $.country_label.setText(_L('Select Country and Language'));
    $.country_cancel_button.setTitle(_L('Cancel'));
    $.country_apply_button.setTitle(_L('Apply'));
    $.assoc_title.setText(_L('Change Password'));
    $.assoc_subtitle_label.setText(_L('Enter Associate ID'));
    $.assoc_code.setHintText(_L('Associate ID'));
    $.assoc_cancel_button.setTitle(_L('Cancel'));
    $.assoc_button.setTitle(_L('Reset Password'));
    $.new_password_title.setText(_L('Change Password'));
    $.new_password_subtitle_label.setText(_L('Enter New Password'));
    $.password.setHintText(_L('Enter New Password'));
    $.password_verify.setHintText(_L('Repeat New Password'));
    $.new_password_cancel_button.setTitle(_L('Cancel'));
    $.new_password_button.setTitle(_L('Change Password'));
    $.success_title.setText(_L('Success'));
    $.success_subtitle_label.setText(_L('Password Successfully Changed'));
    $.success_ok_button.setTitle(_L('OK'));
    $.toolbar && $.toolbar.deinit();
    $.toolbar = Alloy.createController('components/nextPreviousToolbar');
    $.toolbar.setTextFields([$.employee_code, $.employee_pin]);
    $.manager_view.updateLabels();
  }






  function showCountrySelector() {
    $.country_selector.show();
    $.country_selector.setHeight(viewHeight);
    $.contents.hide();
    $.contents.setHeight(0);
  }






  function hideCountrySelector() {
    $.country_selector.hide();
    $.country_selector.setHeight(0);
    $.contents.show();
    $.contents.setHeight(Ti.UI.SIZE);
  }






  function doLogin() {
    logger.info('doLogin called');
    $.login_button.animate(Alloy.Animations.bounce);
    var employee_code = $.employee_code.getValue();
    var employee_pin = $.employee_pin.getValue();

    if (!employee_code || !employee_pin) {
      renderError(_L('You must provide employee code and pin.'));
      return;
    }
    var deferred = new _.Deferred();
    showActivityIndicator(deferred);
    currentAssociate.loginAssociate({
      employee_id: employee_code,
      passcode: employee_pin }).
    done(function () {

      if (!currentAssociate.getPermissions().allowLOBO) {
        renderError(_L('This associate does not have permission to log on'));
        currentAssociate.clear();
        return;
      }
      setTimeout(function () {
        $.employee_code.setValue('');
        $.employee_pin.setValue('');
        resetError($.login_error_label, $.login_subtitle_label);
      }, 200);


      $.employee_pin.blur();
      $.employee_code.blur();

      $.trigger('login:dismiss', {
        employee_id: employee_code });

    }).fail(function (data) {
      var failMsg = _L('Login attempt failed. Re-enter your ID and password.');
      if (data && data.faultDescription) {
        failMsg = data.faultDescription;
      } else if (currentAssociate.get('httpStatus') != 200 && currentAssociate.get('fault')) {
        failMsg = currentAssociate.get('fault').message;
      }
      $.employee_pin.setValue('');
      renderError(failMsg);
    }).always(function () {
      deferred.resolve();
    });
  }






  function showPasswordChangedSuccessfully() {
    $.success.setHeight(viewHeight);
    $.success.show();
    $.new_password.hide();
    $.new_password.setHeight(0);

    $.password.setValue('');
    $.password_verify.setValue('');
  }








  function validatePassword(newPassword) {

    return true;
  }






  function changePassword() {
    resetError();

    if ($.password.getValue() == '' || $.password_verify.getValue() == '') {
      renderError(_L('Both fields must have a password'));
      return;
    }

    if ($.password.getValue() != $.password_verify.getValue()) {
      renderError(_L('Passwords do not match'));
      return;
    }

    if (!validatePassword($.password.getValue())) {
      renderError(_L('Password does not meet minimum criteria'));
      return;
    }

    payload.new_password = $.password.getValue();

    if (manager) {
      var deferred = new _.Deferred();
      showActivityIndicator(deferred);
      manager.changePassword(payload).done(function (model) {
        if (model.get('httpStatus') == 200 && model.get('result')) {
          showPasswordChangedSuccessfully();
        } else if (model.get('httpStatus') == 200 && !model.get('result') && model.get('message')) {
          renderError(model.get('message'));
        } else if (model.get('httpStatus') == 200 && !model.get('result')) {
          renderError(_L('Error changing password'));
        }
      }).fail(function (model) {
        if (model.get('httpStatus') != 200 && model.get('fault')) {
          renderError(model.get('fault').message);
        } else {
          renderError(_L('Error changing password'));
        }
      }).always(function () {
        deferred.resolve();
      });
    }
  }






  function getAssociateId() {
    resetError();

    $.associate.setHeight(viewHeight);
    $.associate.show();
    $.manager.hide();
    $.manager.setHeight(0);

    forgotPasswordTimer = setTimeout(function () {
      resetLoginForm();
    }, 60000);
  }






  function getNewPassword() {
    resetError();

    $.new_password.setHeight(viewHeight);
    $.new_password.show();
    $.associate.hide();
    $.associate.setHeight(0);

    $.assoc_code.setValue('');
  }






  function validateAssociateExists() {
    if ($.assoc_code.getValue() == '') {
      renderError(_L('The associate ID is required'));
      return;
    }

    resetError();

    var deferred = new _.Deferred();
    showActivityIndicator(deferred);
    manager.validateAssociateExists({
      employee_id: 'A' + $.assoc_code.getValue(),
      store_id: Alloy.CFG.store_id }).
    done(function (model) {
      if (model.get('httpStatus') == 200 && !model.get('fault')) {
        payload = {
          employee_id: 'A' + $.assoc_code.value,
          store_id: Alloy.CFG.store_id };

        getNewPassword();
      } else if (model.get('httpStatus') != 200 && model.get('fault')) {
        renderError(model.get('fault').message);
      } else {
        renderError(model.get('fault').message);
      }
    }).fail(function (model) {
      if (model.get('httpStatus') != 200 && model.get('fault')) {
        renderError(model.get('fault').message);
      } else {
        renderError(model.get('message'));
      }
    }).always(function () {
      deferred.resolve();
    });
  }







  function doForgotPassword() {
    $.manager_view.getView('enter_manager_authorization').setHeight(viewHeight);
    $.manager.setHeight(viewHeight);
    $.manager.show();

    manager = Alloy.createModel('associate');
    $.manager_view.init({
      associate: manager,
      subTitleText: _L('Enter Manager Credentials Password'),
      submitFunction: function (data) {
        if (data && data.result) {
          getAssociateId();
        }
      },
      cancelFunction: function () {
        resetLoginForm();
      } });


    var authorizationView = $.manager_view.getView('enter_manager_authorization');
    authorizationView.setLeft(0);
    authorizationView.setTop(0);

    $.contents.hide();
    $.contents.setHeight(0);

    resetError();
  }






  function resetLoginForm() {
    if (forgotPasswordTimer) {
      clearTimeout(forgotPasswordTimer);
      forgotPasswordTimer = null;
    }

    if (manager && manager.isLoggedIn()) {
      manager.logout().fail(function (model) {
        if (model.get('httpStatus') != 200 && model.get('fault')) {
          logger.info(model.get('fault').message);
        } else {
          logger.info('Error logging out manager after password change');
        }
      });
    }
    manager = null;
    payload = null;
    $.employee_code.setValue('');
    $.employee_pin.setValue('');
    $.associate.hide();
    $.associate.setHeight(0);
    $.manager.hide();
    $.manager.setHeight(0);
    $.new_password.hide();
    $.new_password.setHeight(0);
    $.success.hide();
    $.success.setHeight(0);
    hideCountrySelector();
    $.assoc_code.setValue('');
    $.password.setValue('');
    $.password_verify.setValue('');
    $.manager_view.clearErrorMessage();
    resetError($.login_error_label, $.login_subtitle_label);
    resetError($.assoc_error_label, $.assoc_subtitle_label);
    resetError($.new_password_error_label, $.new_password_subtitle_label);
  }






  function renderError(text, error_label, subtitle_label) {
    logger.info('RENDER ERROR in associate/login');

    if (!error_label && !subtitle_label) {
      if ($.contents.getVisible()) {
        error_label = $.login_error_label;
        subtitle_label = $.login_subtitle_label;
      } else if ($.associate.getVisible()) {
        error_label = $.assoc_error_label;
        subtitle_label = $.assoc_subtitle_label;
      } else if ($.new_password.getVisible()) {
        error_label = $.new_password_error_label;
        subtitle_label = $.new_password_subtitle_label;
      } else {
        return;
      }
    }
    subtitle_label.hide();
    subtitle_label.setHeight(0);

    if (text.length > errorLabelLength || EAUtils.isSymbolBasedLanguage() && text.length > symbolErrorLabelLength) {
      error_label.setFont(Alloy.Styles.smallerCalloutCopyFont);
    } else {
      error_label.setFont(Alloy.Styles.calloutCopyFont);
    }
    error_label.setHeight(50);
    error_label.setText(text);
    error_label.show();
  }






  function resetError(error_label, subtitle_label) {
    if (!error_label && !subtitle_label) {
      if ($.contents.getVisible()) {
        error_label = $.login_error_label;
        subtitle_label = $.login_subtitle_label;
      } else if ($.associate.getVisible()) {
        error_label = $.assoc_error_label;
        subtitle_label = $.assoc_subtitle_label;
      } else if ($.new_password.getVisible()) {
        error_label = $.new_password_error_label;
        subtitle_label = $.new_password_subtitle_label;
      } else {
        return;
      }
    }
    error_label.setText('');
    error_label.setHeight(0);
    error_label.hide();
    subtitle_label.show();
    subtitle_label.setHeight(50);
  }









  _.extend($, exports);
}

module.exports = Controller;