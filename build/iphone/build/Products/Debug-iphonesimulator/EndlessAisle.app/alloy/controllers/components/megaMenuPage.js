var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
		delete obj[key];
	}
	return arg;
}

function Controller() {

	require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'components/megaMenuPage';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};




	$.subCategories = Alloy.createCollection('category');


	$.__views.menu_category_container = Ti.UI.createView(
	{ top: 10, width: Ti.UI.FILL, layout: "horizontal", id: "menu_category_container" });

	var __alloyId123 = Alloy.Collections['$.subCategories'] || $.subCategories;function __alloyId124(e) {if (e && e.fromAdapter) {return;}var opts = __alloyId124.opts || {};var models = __alloyId123.models;var len = models.length;var children = $.__views.menu_category_container.children;for (var d = children.length - 1; d >= 0; d--) {$.__views.menu_category_container.remove(children[d]);}for (var i = 0; i < len; i++) {var __alloyId115 = models[i];__alloyId115.__transform = _.isFunction(__alloyId115.transform) ? __alloyId115.transform() : __alloyId115.toJSON();var __alloyId116 = Ti.UI.createView(
			{ width: 204 });

			$.__views.menu_category_container.add(__alloyId116);
			var __alloyId117 = Ti.UI.createView(
			{ top: 0, height: 35, left: 0, width: Ti.UI.FILL, layout: "horizontal" });

			__alloyId116.add(__alloyId117);
			var __alloyId118 = Ti.UI.createLabel(
			{ width: Ti.UI.SIZE, height: 40, color: Alloy.Styles.megaMenu.menuPage.font, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, top: 0, left: 20, font: Alloy.Styles.megaMenu.menuPage.font, text: __alloyId115.__transform.name, categoryid: __alloyId115.__transform.id });

			__alloyId117.add(__alloyId118);
			var __alloyId120 = Ti.UI.createView(
			{ top: 50, layout: "horizontal", width: Ti.UI.SIZE });

			__alloyId116.add(__alloyId120);
			var __alloyId122 = Alloy.createController('components/megaMenuLeaf', { $model: __alloyId115, __parentSymbol: __alloyId120 });
			__alloyId122.setParent(__alloyId120);
		}};__alloyId123.on('fetch destroy change add remove reset', __alloyId124);$.__views.menu_category_container && $.addTopLevelView($.__views.menu_category_container);
	exports.destroy = function () {__alloyId123 && __alloyId123.off('fetch destroy change add remove reset', __alloyId124);};




	_.extend($, $.__views);










	var args = arguments[0] || {};

	$.subCategories.reset($model.getVisibleCategories());




	$.listenTo(Alloy.Models.productSearch, 'change:selected_refinements', function () {
		var selRefs = Alloy.Models.productSearch.getSelectedRefinements();
		var category_id = selRefs.cgid;
		_.each($.menu_category_container.children, function (el) {
			var child = el.children[0].children[0];
			if (child.categoryid == category_id) {
				child.setBackgroundImage(Alloy.Styles.megaMenuCategoryUnderline);
			} else {
				child.setBackgroundImage('');
			}
		});
	});









	_.extend($, exports);
}

module.exports = Controller;