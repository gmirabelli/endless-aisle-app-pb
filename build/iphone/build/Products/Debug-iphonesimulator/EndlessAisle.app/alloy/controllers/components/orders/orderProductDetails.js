var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'components/orders/orderProductDetails';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};




  $.productLineItems = Alloy.createCollection('productItem');


  $.__views.pli_table = Ti.UI.createTableView(
  { top: 0, bottom: 0, separatorStyle: "transparent", id: "pli_table" });

  var __alloyId131 = Alloy.Collections['$.productLineItems'] || $.productLineItems;function __alloyId132(e) {if (e && e.fromAdapter) {return;}var opts = __alloyId132.opts || {};var models = __alloyId131.models;var len = models.length;var rows = [];for (var i = 0; i < len; i++) {var __alloyId128 = models[i];__alloyId128.__transform = transformPLI(__alloyId128);var __alloyId130 = Alloy.createController('checkout/cart/productItemRow', { $model: __alloyId128, __parentSymbol: __parentSymbol });
      rows.push(__alloyId130.getViewEx({ recurse: true }));
    }$.__views.pli_table.setData(rows);};__alloyId131.on('fetch destroy change add remove reset', __alloyId132);$.__views.pli_table && $.addTopLevelView($.__views.pli_table);
  exports.destroy = function () {__alloyId131 && __alloyId131.off('fetch destroy change add remove reset', __alloyId132);};




  _.extend($, $.__views);










  var toCurrency = require('EAUtils').toCurrency;
  var fetchImages = require('EAUtils').fetchImagesForProducts;
  var logger = require('logging')('orders:orderProductDetails', getFullControllerPath($.__controllerPath));




  exports.deinit = deinit;
  exports.render = render;










  function render(order) {
    logger.info('render called');
    var plis = order.getProductItems();
    _.each(plis, function (row) {
      row.currencyCode = order.getCurrencyCode();
    });
    var deferred;
    $.productLineItems.once('reset', function () {
      disableSelectionOnRows();
    });


    deinitRows();

    if (plis && plis.length > 0) {
      logger.info('[order] Calling reset with data');
      $.productLineItems.once('reset', function () {
        logger.info('Fetching images for plis');
        deferred = fetchImageForProducts(plis, order);
      });
      $.productLineItems.reset(plis);
    } else {
      logger.info('[order] Calling reset with no data');
      $.productLineItems.reset([]);
      deferred = new _.Deferred();
      deferred = deferred.promise();
    }
    if (plis && plis.length > 3) {

      $.pli_table.setTouchEnabled(true);
      $.pli_table.setShowVerticalScrollIndicator(true);
    }

    return deferred;
  }






  function deinit() {
    logger.info('deinit called');
    deinitRows();
    $.stopListening();
    $.destroy();
  }










  function transformPLI(model) {
    logger.info('[confirmation] transform PLI');
    var option_string = '';
    if (model.getOptionItems()) {
      var options = model.getOptionItems();
      _.each(options, function (o) {
        option_string += String.format(_L('%s: %s'), o.getItemText(), toCurrency(o.getPrice())) + '\n';
      });
    }
    return {
      name: model.getProductName(),
      item_number: _L('Item# ') + model.getProductId(),
      color: '',
      size: '',
      price: toCurrency(model.getPrice()),
      quantity: _L('Quantity:') + ' ' + model.getQuantity(),
      options: option_string };

  }






  function deinitRows() {
    if ($.pli_table.getSections().length > 0) {
      _.each($.pli_table.getSections()[0].getRows(), function (row) {
        row.deinit();
      });
    }
  }







  function fetchImageForProducts(plis, order) {
    return fetchImages(plis, $.pli_table, true, order);
  }






  function disableSelectionOnRows() {
    if ($.pli_table.getSections() && $.pli_table.getSections().length > 0) {
      var children = $.pli_table.getSections()[0].getRows();

      _.each(children, function (child) {
        child.setSelectionStyle(Ti.UI.iOS.TableViewCellSelectionStyle.NONE);
      });
    }
  }









  _.extend($, exports);
}

module.exports = Controller;