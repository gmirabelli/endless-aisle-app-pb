var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'reports/datePickers';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.outterContainer = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.color.background.transparentBlack30Percent, id: "outterContainer" });

  $.__views.outterContainer && $.addTopLevelView($.__views.outterContainer);
  $.__views.container = Ti.UI.createView(
  { height: Ti.UI.SIZE, width: 730, backgroundColor: Alloy.Styles.color.background.white, layout: "vertical", id: "container" });

  $.__views.outterContainer.add($.__views.container);
  $.__views.picker_container = Ti.UI.createView(
  { top: 0, height: Ti.UI.SIZE, width: Ti.UI.FILL, backgroundColor: Alloy.Styles.color.background.white, layout: "horizontal", id: "picker_container" });

  $.__views.container.add($.__views.picker_container);
  $.__views.date_from_wrapper = Ti.UI.createView(
  { left: 30, height: Ti.UI.SIZE, width: Ti.UI.SIZE, layout: "vertical", backgroundColor: Alloy.Styles.color.background.white, top: 30, bottom: 10, viewShadowColor: Alloy.Styles.shadowGrayColor, viewShadowOffset: { x: 0, y: 0 }, viewShadowRadius: 2, id: "date_from_wrapper" });

  $.__views.picker_container.add($.__views.date_from_wrapper);
  $.__views.title_view_from = Ti.UI.createView(
  { top: 0, height: Ti.UI.SIZE, width: 320, backgroundColor: Alloy.Styles.color.background.white, layout: "vertical", bottom: 10, id: "title_view_from" });

  $.__views.date_from_wrapper.add($.__views.title_view_from);
  $.__views.title_from = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.accentColor, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, top: 15, font: Alloy.Styles.bigButtonFont, textid: "_From", id: "title_from", accessibilityValue: "title_from" });

  $.__views.title_view_from.add($.__views.title_from);
  $.__views.seperator_date_from = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.accentColor, width: 320, height: 1, top: 15, id: "seperator_date_from" });

  $.__views.title_view_from.add($.__views.seperator_date_from);
  $.__views.date_from = Ti.UI.createPicker(
  { top: 0, width: 320, format24: false, calendarViewShown: false, id: "date_from", accessibilityValue: "date_from", type: Titanium.UI.PICKER_TYPE_DATE, useSpinner: true });

  $.__views.date_from_wrapper.add($.__views.date_from);
  $.__views.date_to_wrapper = Ti.UI.createView(
  { left: 30, height: Ti.UI.SIZE, width: Ti.UI.SIZE, layout: "vertical", backgroundColor: Alloy.Styles.color.background.white, top: 30, bottom: 10, viewShadowColor: Alloy.Styles.shadowGrayColor, viewShadowOffset: { x: 0, y: 0 }, viewShadowRadius: 2, id: "date_to_wrapper" });

  $.__views.picker_container.add($.__views.date_to_wrapper);
  $.__views.title_view_to = Ti.UI.createView(
  { top: 0, height: Ti.UI.SIZE, width: 320, backgroundColor: Alloy.Styles.color.background.white, layout: "vertical", bottom: 10, id: "title_view_to" });

  $.__views.date_to_wrapper.add($.__views.title_view_to);
  $.__views.title_to = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.accentColor, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, top: 15, font: Alloy.Styles.bigButtonFont, textid: "_To", id: "title_to", accessibilityValue: "title_to" });

  $.__views.title_view_to.add($.__views.title_to);
  $.__views.seperator_date_to = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.accentColor, width: 320, height: 1, top: 15, id: "seperator_date_to" });

  $.__views.title_view_to.add($.__views.seperator_date_to);
  $.__views.date_to = Ti.UI.createPicker(
  { top: 0, width: 320, format24: false, calendarViewShown: false, id: "date_to", accessibilityValue: "date_to", type: Titanium.UI.PICKER_TYPE_DATE, useSpinner: true });

  $.__views.date_to_wrapper.add($.__views.date_to);
  $.__views.controls_wrapper = Ti.UI.createView(
  { top: 0, height: 70, width: "100%", layout: "horizontal", id: "controls_wrapper" });

  $.__views.container.add($.__views.controls_wrapper);
  $.__views.cancel = Ti.UI.createButton(
  { viewShadowColor: Alloy.Styles.shadowGrayColor, viewShadowOffset: { x: 0, y: 0 }, viewShadowRadius: 2, left: "23%", top: "20%", height: "50%", width: "15%", font: Alloy.Styles.bigButtonFont, color: Alloy.Styles.buttons.primary.color, backgroundColor: Alloy.Styles.accentColor, titleid: "_Cancel", id: "cancel", accessibilityValue: "date_cancel" });

  $.__views.controls_wrapper.add($.__views.cancel);
  $.__views.go = Ti.UI.createButton(
  { viewShadowColor: Alloy.Styles.shadowGrayColor, viewShadowOffset: { x: 0, y: 0 }, viewShadowRadius: 2, left: "23%", top: "20%", height: "50%", width: "15%", font: Alloy.Styles.bigButtonFont, color: Alloy.Styles.buttons.primary.color, backgroundColor: Alloy.Styles.accentColor, titleid: "_Apply", id: "go", accessibilityValue: "date_go" });

  $.__views.controls_wrapper.add($.__views.go);
  $.__views.date_range_msg_container = Ti.UI.createView(
  { height: Ti.UI.SIZE, width: Ti.UI.FILL, bottom: 10, id: "date_range_msg_container" });

  $.__views.container.add($.__views.date_range_msg_container);
  $.__views.date_range_msg = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.appFont, id: "date_range_msg", accessibilityValue: "date_range_msg" });

  $.__views.date_range_msg_container.add($.__views.date_range_msg);
  exports.destroy = function () {};




  _.extend($, $.__views);











  var args = arguments[0] || {};
  var model = args.model;
  var formatDate = require('EAUtils').formatDate;
  var customDateFrom = model.get('customDateFrom'),
  customDateTo = model.get('customDateTo'),
  today = new Date(),
  minDate = new Date(2013, 0, 1);


  var allowedDateRanger = 90 * 24 * 60 * 60 * 1000;

  var TIME_START_DAY = 'T00:00:00',
  TIME_END_DAY = 'T23:59:59';




  $.cancel.addEventListener('click', dismiss);
  $.go.addEventListener('click', setDateTimeFilters);




  exports.deinit = deinit;









  function deinit() {
    $.cancel.removeEventListener('click', dismiss);
    $.go.removeEventListener('click', setDateTimeFilters);
    $.stopListening();
    $.destroy();
  }











  function convertDateToCurrentTimezone(dateString) {
    return new Date(dateString).getTime() + 60 * new Date().getTimezoneOffset() * 1000;
  }









  function setDateTimeFilters() {
    if (verifyDateRange()) {
      model.set({
        dateFrom: formatDate($.date_from.value, TIME_START_DAY),
        customDateFrom: formatDate($.date_from.value, TIME_START_DAY),
        dateTo: formatDate($.date_to.value, TIME_END_DAY),
        customDateTo: formatDate($.date_to.value, TIME_END_DAY) });

      dismiss();
    }
  }






  function dismiss() {
    $.trigger('datePickers:dismiss');
  }








  function verifyDateRange() {
    var dateFrom = new Date(formatDate($.date_from.getValue(), TIME_START_DAY)),
    dateTo = new Date(formatDate($.date_to.getValue(), TIME_END_DAY));
    var range = dateTo.getTime() - dateFrom.getTime();

    if (dateTo.getTime() < dateFrom.getTime()) {
      $.date_range_msg.setText(_L('End date cannot be before start date.'));
      $.date_range_msg.setColor('red');
      $.date_to.setValue(today);
      return false;
    }

    if (range > allowedDateRanger) {
      $.date_range_msg.setText(_L('Maximum range of 90 days allowed.'));
      $.date_range_msg.setColor('red');
      $.date_from.setValue(new Date(dateTo.getTime() - allowedDateRanger + 24 * 60 * 60 * 1000));
      return false;
    }

    $.date_range_msg.setText(_L('Maximum range of 90 days allowed.'));
    $.date_range_msg.setColor(Alloy.Styles.color.text.dark);
    return true;
  }




  $.date_from.setMinDate(minDate);
  $.date_to.setMinDate(minDate);
  $.date_from.setMaxDate(today);
  $.date_to.setMaxDate(today);
  $.date_from.setValue(customDateFrom ? new Date(convertDateToCurrentTimezone(customDateFrom)) : new Date(today.getTime() - 24 * 60 * 60 * 1000));
  $.date_to.setValue(customDateTo ? new Date(customDateTo) : today);
  $.date_range_msg.setText(_L('Maximum range of 90 days allowed.'));









  _.extend($, exports);
}

module.exports = Controller;