var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
		delete obj[key];
	}
	return arg;
}

function Controller() {

	require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'components/radio_button';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};







	$.__views.radio_button = Ti.UI.createView(
	{ layout: "horizontal", height: Ti.UI.SIZE, id: "radio_button" });

	$.__views.radio_button && $.addTopLevelView($.__views.radio_button);
	$.__views.radio_button_image = Ti.UI.createImageView(
	{ image: Alloy.Styles.radioButtonOnImage, id: "radio_button_image", accessibilityValue: "radio_button_image" });

	$.__views.radio_button.add($.__views.radio_button_image);
	exports.destroy = function () {};




	_.extend($, $.__views);












	_.extend($, exports);
}

module.exports = Controller;