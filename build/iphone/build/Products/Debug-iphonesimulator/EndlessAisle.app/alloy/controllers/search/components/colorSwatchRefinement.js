var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'search/components/colorSwatchRefinement';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.colorSwatchRefinementHeader = Ti.UI.createView(
  { height: 50, layout: "absolute", backgroundColor: Alloy.Styles.color.background.light, id: "colorSwatchRefinementHeader" });

  $.__views.colorSwatchRefinementHeaderLabel = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.calloutCopyFont, left: 22, id: "colorSwatchRefinementHeaderLabel", accessibilityValue: "color_swatch_refinement_header_label" });

  $.__views.colorSwatchRefinementHeader.add($.__views.colorSwatchRefinementHeaderLabel);
  $.__views.colorSwatchRefinement = Ti.UI.createTableViewSection(
  { headerView: $.__views.colorSwatchRefinementHeader, id: "colorSwatchRefinement" });

  $.__views.colorSwatchRefinement && $.addTopLevelView($.__views.colorSwatchRefinement);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var listenerViews = [];
  var logger = require('logging')('search:components:colorSwatchRefinement', getFullControllerPath($.__controllerPath));

  var psr = Alloy.Models.productSearch;
  var refinement = $model;

  var visibleValues = 0;
  var onViewClick;




  exports.deinit = deinit;
  exports.init = init;
  exports.shouldBeVisible = shouldBeVisible;









  function init() {
    var values,
    rows = [],
    row,
    view,
    label;

    values = refinement.getValues();
    $.colorSwatchRefinementHeaderLabel.setText(refinement.getLabel());


    while ($.colorSwatchRefinement.rowCount > 0) {
      $.colorSwatchRefinement.remove($.colorSwatchRefinement.rows[0]);
    }

    row = Ti.UI.createTableViewRow({
      layout: 'horizontal',
      touchEnabled: false,
      allowsSelection: false,
      opacity: 0.95,
      selectionStyle: Ti.UI.iOS.TableViewCellSelectionStyle.NONE });



    visibleValues = 0;
    _.each(values, function (value) {

      var attribute_id = refinement.getAttributeId();
      var value_id = value.getValue();
      var hit_count = value.getHitCount();
      var value_key = value.getPresentationId() || 'default';
      var swatchProperty = Alloy.CFG.product_search.refinements.colorForPresentationID[value_key];
      if (!swatchProperty) {
        logger.error('Missing colorForPresentationID for value \'' + value_key.toLowerCase() + '\' skipping color refinement.');
        return;
      }
      var isRefinedBy = psr.isRefinedBy(attribute_id, value_id);

      if (hit_count == 0) {
        return;
      }
      visibleValues++;
      var outerView = Ti.UI.createView({
        width: 50,
        height: 45,
        borderWidth: 1,
        borderColor: isRefinedBy ? Alloy.Styles.color.border.darkest : Alloy.Styles.color.border.lighter,
        left: 10,
        top: 10,
        bottom: 10,
        layout: 'absolute',
        bubbleParent: false,
        opacity: 0.95,
        accessibilityLabel: 'border_' + value_id + '_' + isRefinedBy,
        backgroundColor: Alloy.Styles.color.background.light });

      view = Ti.UI.createView({
        width: 44,
        height: 39,
        left: 3,
        top: 3,
        bottom: 10,
        layout: 'absolute',
        bubbleParent: false,
        opacity: 0.95,
        accessibilityLabel: 'select_' + value_id });

      outerView.add(view);
      view[swatchProperty.key] = swatchProperty.value;
      row.add(outerView);

      onViewClick = function (event) {
        event.cancelBubble = true;
        handleSwatchClick({
          attribute_id: attribute_id,
          value_id: value_id });

      };
      view.addEventListener('click', onViewClick);
      listenerViews.push(view);
    });
    $.colorSwatchRefinement.add(row);
  }






  function deinit() {
    logger.info('DEINIT called');
    deinitExistingRows();
    $.stopListening();
    $.destroy();
  }









  function shouldBeVisible() {
    return visibleValues > 1;
  }







  function deinitExistingRows() {
    var view = null;
    removeAllChildren($.colorSwatchRefinement);
    _.each(listenerViews, function (view) {
      view.removeEventListener('click', onViewClick);
    });
  }









  function handleSwatchClick(event) {
    event.cancelBubble = true;
    logger.info('triggering refinement:select: ' + JSON.stringify(event));
    $.colorSwatchRefinement.fireEvent('refinement:toggle', event);
  }




  init();









  _.extend($, exports);
}

module.exports = Controller;