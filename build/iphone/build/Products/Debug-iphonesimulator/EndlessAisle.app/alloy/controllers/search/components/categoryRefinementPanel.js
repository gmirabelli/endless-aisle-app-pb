var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'search/components/categoryRefinementPanel';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.categoryRefinementPanel = Ti.UI.createView(
  { layout: "absolute", visible: false, top: 0, left: 450, bottom: 0, right: 0, width: 626, zIndex: 2, id: "categoryRefinementPanel" });

  $.__views.categoryRefinementPanel && $.addTopLevelView($.__views.categoryRefinementPanel);
  $.__views.__alloyId235 = Ti.UI.createView(
  { left: 0, top: 0, width: 8, backgroundImage: Alloy.Styles.rightShadowImage, zIndex: 4, height: "100%", id: "__alloyId235" });

  $.__views.categoryRefinementPanel.add($.__views.__alloyId235);
  $.__views.selectedCategoriesTableHeader = Ti.UI.createView(
  { height: 61, layout: "absolute", backgroundColor: Alloy.Styles.color.background.light, id: "selectedCategoriesTableHeader" });

  $.__views.selectedCategoriesTableHeaderLabel = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: 61, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.bigAccentFont, backgroundColor: Alloy.Styles.color.background.light, left: 22, textid: "_Product_Categories", id: "selectedCategoriesTableHeaderLabel", accessibilityValue: "product_categories" });

  $.__views.selectedCategoriesTableHeader.add($.__views.selectedCategoriesTableHeaderLabel);
  $.__views.selectedCategoriesTable = Ti.UI.createTableView(
  { left: 8, top: 0, width: 298, color: Alloy.Styles.color.text.dark, zIndex: 4, separatorStyle: Ti.UI.TABLE_VIEW_SEPARATOR_STYLE_NONE, headerView: $.__views.selectedCategoriesTableHeader, id: "selectedCategoriesTable" });

  $.__views.categoryRefinementPanel.add($.__views.selectedCategoriesTable);
  $.__views.__alloyId237 = Ti.UI.createView(
  { left: 306, top: 0, width: 8, backgroundImage: Alloy.Styles.leftShadowImage, zIndex: 4, height: "100%", id: "__alloyId237" });

  $.__views.categoryRefinementPanel.add($.__views.__alloyId237);
  $.__views.subcategoriesTableHeader = Ti.UI.createView(
  { layout: "horizontal", height: 61, font: Alloy.Styles.buttonFont, backgroundColor: Alloy.Styles.color.background.light, id: "subcategoriesTableHeader" });

  $.__views.subcategoriesTableHeaderLabel = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: 61, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.smallAccentFont, backgroundColor: Alloy.Styles.color.background.light, left: 13, textid: "_Subcategories_of", id: "subcategoriesTableHeaderLabel", accessibilityValue: "subcategoriesTableHeaderLabel" });

  $.__views.subcategoriesTableHeader.add($.__views.subcategoriesTableHeaderLabel);
  $.__views.subcategoriesTableHeaderCalloutLabel = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: 61, color: Alloy.Styles.accentColor, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.smallAccentFont, backgroundColor: Alloy.Styles.color.background.light, left: 4, textid: "_All_Departments", id: "subcategoriesTableHeaderCalloutLabel", accessibilityValue: "subcategoriesTableHeaderCalloutLabel" });

  $.__views.subcategoriesTableHeader.add($.__views.subcategoriesTableHeaderCalloutLabel);
  $.__views.subcategoriesTable = Ti.UI.createTableView(
  { left: 306, top: 0, width: 260, backgroundColor: Alloy.Styles.color.background.light, color: Alloy.Styles.color.text.dark, zIndex: 3, separatorStyle: Ti.UI.TABLE_VIEW_SEPARATOR_STYLE_NONE, headerView: $.__views.subcategoriesTableHeader, id: "subcategoriesTable" });

  $.__views.categoryRefinementPanel.add($.__views.subcategoriesTable);
  exports.destroy = function () {};




  _.extend($, $.__views);











  var eaUtils = require('EAUtils');
  var logger = require('logging')('search:components:categoryRefinementPanel', getFullControllerPath($.__controllerPath));
  var currentSearch = Alloy.Models.productSearch;




  $.selectedCategoriesTable.addEventListener('click', onCategoryClick);
  $.subcategoriesTable.addEventListener('click', onSubCategoryClick);




  $.listenTo(currentSearch, 'change:selected_refinements', render);
  $.listenTo(currentSearch, 'change:refinements', render);




  exports.deinit = deinit;









  function deinit() {
    logger.info('DEINIT called');
    $.selectedCategoriesTable.removeEventListener('click', onCategoryClick);
    $.subcategoriesTable.removeEventListener('click', onSubCategoryClick);
    $.stopListening();
    $.destroy();
  }






  function render() {
    logger.trace('rendering refinements ');

    var path = currentSearch.getCategoryPath(false);
    logger.info('Path: ' + JSON.stringify(path));
    var row, view, label, backgroundImage, fontColor, categoryTitle;
    var cats = [];
    row = Ti.UI.createTableViewRow({
      category_id: Alloy.CFG.root_category_id,
      height: 65,
      layout: 'absolute' });

    var isSearching = !(currentSearch.getQuery() == '');

    if (path && path.length > 0) {
      backgroundImage = Alloy.Styles.categoryRootImage;
      fontColor = Alloy.Styles.color.text.dark;
    } else {
      backgroundImage = Alloy.Styles.categoryRootEndImage;
      fontColor = Alloy.Styles.color.text.white;
      categoryTitle = isSearching ? _L('Search Results') : _L('All Departments');
    }
    label = Ti.UI.createLabel({
      text: isSearching ? _L('Search Results') : _L('All Departments'),
      font: Alloy.Styles.categoryRefinementLabelLarge,
      color: fontColor,
      left: 22 });

    view = Ti.UI.createView({
      backgroundImage: backgroundImage,
      top: 0,
      left: 0,
      height: '100%',
      width: '100%',
      layout: 'absolute' });

    view.add(label);
    row.add(view);
    cats.push(row);
    for (var i = 0, ii = path.length; i < ii; i++) {
      row = Ti.UI.createTableViewRow({
        category_id: path[i].getValue(),
        height: 65,
        layout: 'absolute' });

      if (i == ii - 1) {
        backgroundImage = Alloy.Styles.categoryPathEndImage;
        fontColor = Alloy.Styles.color.text.white;
      } else {
        backgroundImage = Alloy.Styles.categoryPathMiddleImage;
        fontColor = Alloy.Styles.color.text.dark;
      }
      view = Ti.UI.createView({
        backgroundImage: backgroundImage,
        top: 0,
        left: 0,
        height: '100%',
        width: '100%' });

      label = Ti.UI.createLabel({
        text: path[i].getLabel(),
        font: Alloy.Styles.categoryRefinementLabelLarge,
        color: fontColor,
        left: 22 });

      row.add(view);
      row.add(label);
      cats.push(row);
    }
    $.selectedCategoriesTable.setData(cats);

    cats = [];
    var lowest = currentSearch.getSubcategoriesContext();
    var lowestParent = lowest.parent;
    if (lowestParent) {
      $.subcategoriesTableHeaderCalloutLabel.setText(lowestParent.getLabel());
      $.subcategoriesTableHeaderLabel.setText(_L('Subcategories of'));
    } else {
      $.subcategoriesTableHeaderCalloutLabel.setText(categoryTitle);
      $.subcategoriesTableHeaderLabel.setText('');
    }

    var lowestSubcats = lowest.subcategories;

    var selectedCategoryID = currentSearch.getCategoryID();
    var fontColor, categoryRefinement, categoryID, hitCount;

    if (!_.isEmpty(lowestSubcats)) {
      for (var i = 0, ii = lowestSubcats.length; i < ii; i++) {
        fontColor = Alloy.Styles.color.text.dark;
        categoryRefinement = lowestSubcats[i];
        categoryID = categoryRefinement.getValue();
        hitCount = categoryRefinement.getHitCount();

        if (isSearching && hitCount == 0) {
          fontColor = Alloy.Styles.color.text.light;
        } else if (selectedCategoryID == categoryID) {
          fontColor = Alloy.Styles.accentColor;
        }

        row = Ti.UI.createTableViewRow({
          category_id: categoryID,
          enable_click: isSearching && hitCount == 0 ? false : true,
          accessibilityLabel: 'catg_' + categoryID,
          height: 50,
          layout: 'absolute',
          opacity: '1.0',
          selectedBackgroundColor: Alloy.Styles.accentColor });

        view = Ti.UI.createView({
          backgroundImage: Alloy.Styles.categoryOptionImage,
          opacity: '0.9',
          top: 0,
          left: 0,
          height: '100%',
          width: '100%' });

        label = Ti.UI.createLabel({
          text: categoryRefinement.getLabel(),
          left: 26,
          font: Alloy.Styles.categoryRefinementLabelSmall,
          color: fontColor,
          highlightedColor: Alloy.Styles.color.text.dark });


        view.add(label);
        row.add(view);
        cats.push(row);
      }
    } else {
      logger.error('Unable to obtain categories from search response.  Verify product_search.cgid_select_params config is setup properly.');
    }

    $.subcategoriesTable.setData(cats);
    logger.trace('rendering refinements end');
  }









  function onCategoryClick(event) {
    var category_id = event.rowData.category_id;
    var selRefs = currentSearch.getSelectedRefinements();

    if (category_id) {
      selRefs = {
        cgid: category_id };


      currentSearch.setSelectedRefinements(selRefs, {
        silent: true });

      eaUtils.doProductSearch({
        force: true });

    }
  }






  function onSubCategoryClick(event) {
    var category_id = event.rowData.category_id;
    var enable_click = event.rowData.enable_click;
    var selRefs = currentSearch.getSelectedRefinements();

    if (category_id && enable_click) {
      selRefs = {
        cgid: category_id };


      currentSearch.setSelectedRefinements(selRefs, {
        silent: true });

      eaUtils.doProductSearch({
        force: true });

    }
  }









  _.extend($, exports);
}

module.exports = Controller;