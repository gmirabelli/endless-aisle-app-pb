var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'support/components/header';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.dashboard_header = Ti.UI.createView(
  { layout: "horizontal", height: 40, top: 0, left: 0, backgroundColor: Alloy.Styles.color.background.dark, id: "dashboard_header" });

  $.__views.dashboard_header && $.addTopLevelView($.__views.dashboard_header);
  $.__views.button_container = Ti.UI.createView(
  { layout: "horizontal", width: "100%", left: 0, id: "button_container" });

  $.__views.dashboard_header.add($.__views.button_container);
  $.__views.dashboard_configuration_button = Ti.UI.createButton(
  { top: 0, width: "16.6%", height: 40, color: Alloy.Styles.color.text.dark, font: Alloy.Styles.detailLabelFont, style: Ti.UI.iOS.SystemButtonStyle.PLAIN, backgroundColor: Alloy.Styles.color.background.medium, borderWidth: 1, borderColor: Alloy.Styles.color.border.medium, titleid: "_Configuration", id: "dashboard_configuration_button", accessibilityValue: "dashboard_configuration_button" });

  $.__views.button_container.add($.__views.dashboard_configuration_button);
  $.__views.dashboard_settings_button = Ti.UI.createButton(
  { top: 0, width: "16.6%", height: 40, color: Alloy.Styles.color.text.dark, font: Alloy.Styles.detailLabelFont, style: Ti.UI.iOS.SystemButtonStyle.PLAIN, backgroundColor: Alloy.Styles.color.background.medium, borderWidth: 1, borderColor: Alloy.Styles.color.border.medium, titleid: "_App_Settings", id: "dashboard_settings_button", accessibilityValue: "dashboard_settings_button" });

  $.__views.button_container.add($.__views.dashboard_settings_button);
  $.__views.dashboard_payment_terminal_button = Ti.UI.createButton(
  { top: 0, width: "16.6%", height: 40, color: Alloy.Styles.color.text.dark, font: Alloy.Styles.detailLabelFont, style: Ti.UI.iOS.SystemButtonStyle.PLAIN, backgroundColor: Alloy.Styles.color.background.medium, borderWidth: 1, borderColor: Alloy.Styles.color.border.medium, titleid: "_Payment_Terminal_Tab", id: "dashboard_payment_terminal_button", accessibilityValue: "dashboard_payment_terminal_button" });

  $.__views.button_container.add($.__views.dashboard_payment_terminal_button);
  $.__views.dashboard_receipt_printer_button = Ti.UI.createButton(
  { top: 0, width: "16.6%", height: 40, color: Alloy.Styles.color.text.dark, font: Alloy.Styles.detailLabelFont, style: Ti.UI.iOS.SystemButtonStyle.PLAIN, backgroundColor: Alloy.Styles.color.background.medium, borderWidth: 1, borderColor: Alloy.Styles.color.border.medium, titleid: "_Receipt_Printer", id: "dashboard_receipt_printer_button", accessibilityValue: "dashboard_receipt_printer_button" });

  $.__views.button_container.add($.__views.dashboard_receipt_printer_button);
  $.__views.dashboard_test_button = Ti.UI.createButton(
  { top: 0, width: "16.6%", height: 40, color: Alloy.Styles.color.text.dark, font: Alloy.Styles.detailLabelFont, style: Ti.UI.iOS.SystemButtonStyle.PLAIN, backgroundColor: Alloy.Styles.color.background.medium, borderWidth: 1, borderColor: Alloy.Styles.color.border.medium, titleid: "_Test_Tab", id: "dashboard_test_button", accessibilityValue: "dashboard_test_button" });

  $.__views.button_container.add($.__views.dashboard_test_button);
  $.__views.dashboard_logs_button = Ti.UI.createButton(
  { top: 0, width: "16.6%", height: 40, color: Alloy.Styles.color.text.dark, font: Alloy.Styles.detailLabelFont, style: Ti.UI.iOS.SystemButtonStyle.PLAIN, backgroundColor: Alloy.Styles.color.background.medium, borderWidth: 1, borderColor: Alloy.Styles.color.border.medium, titleid: "_Logs", id: "dashboard_logs_button", accessibilityValue: "dashboard_logs_button" });

  $.__views.button_container.add($.__views.dashboard_logs_button);
  exports.destroy = function () {};




  _.extend($, $.__views);











  var tabTextLength = 16;

  var inactiveBackgroundColor = Alloy.Styles.tabs.backgroundColorDisabled;
  var activeBackgroundColor = Alloy.Styles.tabs.backgroundColorEnabled;
  var inactiveBorderColor = Alloy.Styles.color.border.darker;




  $.dashboard_configuration_button.addEventListener('click', onDashboardConfigClick);
  $.dashboard_settings_button.addEventListener('click', onDashboardSettingsClick);
  $.dashboard_payment_terminal_button.addEventListener('click', onPaymentTerminalClick);
  $.dashboard_receipt_printer_button.addEventListener('click', onReceiptPrinterClick);
  $.dashboard_test_button.addEventListener('click', onTestClick);
  $.dashboard_logs_button.addEventListener('click', onLogsClick);




  exports.deinit = deinit;
  exports.selectTab = selectTab;
  exports.showHeader = showHeader;
  exports.hideHeader = hideHeader;









  function deinit() {
    $.dashboard_configuration_button.removeEventListener('click', onDashboardConfigClick);
    $.dashboard_settings_button.removeEventListener('click', onDashboardSettingsClick);
    $.dashboard_payment_terminal_button.removeEventListener('click', onPaymentTerminalClick);
    $.dashboard_receipt_printer_button.removeEventListener('click', onReceiptPrinterClick);
    $.dashboard_test_button.removeEventListener('click', onTestClick);
    $.dashboard_logs_button.removeEventListener('click', onLogsClick);
    $.stopListening();
    $.destroy();
  }










  function selectTab(tab) {
    inactivateAllTabs();
    switch (tab) {
      case 'configurations':
        selectTabColor($.dashboard_configuration_button);
        break;
      case 'app_config':
        selectTabColor($.dashboard_settings_button);
        break;
      case 'payment_terminal':
        selectTabColor($.dashboard_payment_terminal_button);
        break;
      case 'receipt_printer':
        selectTabColor($.dashboard_receipt_printer_button);
        break;
      case 'test':
        selectTabColor($.dashboard_test_button);
        break;
      case 'logs':
        selectTabColor($.dashboard_logs_button);
        break;}

  }






  function inactivateAllTabs() {
    $.dashboard_configuration_button.setBackgroundColor(inactiveBackgroundColor);
    $.dashboard_settings_button.setBackgroundColor(inactiveBackgroundColor);
    $.dashboard_payment_terminal_button.setBackgroundColor(inactiveBackgroundColor);
    $.dashboard_receipt_printer_button.setBackgroundColor(inactiveBackgroundColor);
    $.dashboard_test_button.setBackgroundColor(inactiveBackgroundColor);
    $.dashboard_logs_button.setBackgroundColor(inactiveBackgroundColor);

    $.dashboard_configuration_button.setBorderColor(inactiveBorderColor);
    $.dashboard_settings_button.setBorderColor(inactiveBorderColor);
    $.dashboard_payment_terminal_button.setBorderColor(inactiveBorderColor);
    $.dashboard_receipt_printer_button.setBorderColor(inactiveBorderColor);
    $.dashboard_test_button.setBorderColor(inactiveBorderColor);
    $.dashboard_logs_button.setBorderColor(inactiveBorderColor);
  }







  function selectTabColor(tab) {
    tab.setBackgroundColor(activeBackgroundColor);
    tab.setBorderColor(activeBackgroundColor);
  }






  function showHeader() {
    $.button_container.setVisible(true);
    $.button_container.setLayout('horizontal');
    $.button_container.setWidth('100%');
    $.button_container.setHeight('100%');
    $.button_container.setLeft(0);
  }






  function hideHeader() {
    $.button_container.setVisible(false);
  }











  function onDashboardConfigClick(event) {
    event.cancelBubble = true;
    $.trigger('changeDashboardPage', {
      page: 'configurations' });

  }








  function onDashboardSettingsClick(event) {
    event.cancelBubble = true;
    $.trigger('changeDashboardPage', {
      page: 'app_config' });

  }








  function onPaymentTerminalClick(event) {
    event.cancelBubble = true;
    $.trigger('changeDashboardPage', {
      page: 'payment_terminal' });

  }








  function onReceiptPrinterClick(event) {
    event.cancelBubble = true;
    $.trigger('changeDashboardPage', {
      page: 'receipt_printer' });

  }








  function onTestClick(event) {
    event.cancelBubble = true;
    $.trigger('changeDashboardPage', {
      page: 'test' });

  }








  function onLogsClick(event) {
    event.cancelBubble = true;
    $.trigger('changeDashboardPage', {
      page: 'logs' });

  }





  if (!Alloy.CFG.devices.payment_terminal_module) {
    $.dashboard_payment_terminal_button.hide();
    $.dashboard_payment_terminal_button.setWidth(0);
  } else {
    var deviceModule = require(Alloy.CFG.devices.payment_terminal_module);
    if (!_.isFunction(deviceModule.getInfoView) && !_.isFunction(deviceModule.getConfigView)) {
      $.dashboard_payment_terminal_button.hide();
      $.dashboard_payment_terminal_button.setWidth(0);
    }
  }

  if (!Alloy.CFG.devices.printer_module || !Alloy.CFG.printer_availability) {
    $.dashboard_receipt_printer_button.hide();
    $.dashboard_receipt_printer_button.setWidth(0);
  }

  if ($.dashboard_configuration_button.getTitle().length > tabTextLength || $.dashboard_settings_button.getTitle().length > tabTextLength || $.dashboard_payment_terminal_button.getTitle().length > tabTextLength || $.dashboard_receipt_printer_button.getTitle().length > tabTextLength || $.dashboard_test_button.getTitle().length > tabTextLength || $.dashboard_logs_button.getTitle().length > tabTextLength) {
    $.dashboard_configuration_button.setFont(Alloy.Styles.lineItemLabelFont);
    $.dashboard_settings_button.setFont(Alloy.Styles.lineItemLabelFont);
    $.dashboard_payment_terminal_button.setFont(Alloy.Styles.lineItemLabelFont);
    $.dashboard_receipt_printer_button.setFont(Alloy.Styles.lineItemLabelFont);
    $.dashboard_test_button.setFont(Alloy.Styles.lineItemLabelFont);
    $.dashboard_logs_button.setFont(Alloy.Styles.lineItemLabelFont);
  }









  _.extend($, exports);
}

module.exports = Controller;