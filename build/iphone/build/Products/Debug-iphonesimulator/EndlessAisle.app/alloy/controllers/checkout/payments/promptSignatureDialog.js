var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
		delete obj[key];
	}
	return arg;
}

function Controller() {

	require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'checkout/payments/promptSignatureDialog';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};







	$.__views.payment_signature_window = Ti.UI.createView(
	{ backgroundColor: Alloy.Styles.color.background.transparent, id: "payment_signature_window" });

	$.__views.payment_signature_window && $.addTopLevelView($.__views.payment_signature_window);
	$.__views.backdrop = Ti.UI.createView(
	{ top: 0, left: 0, width: 1024, height: 768, id: "backdrop" });

	$.__views.payment_signature_window.add($.__views.backdrop);
	$.__views.contents = Ti.UI.createView(
	{ backgroundColor: Alloy.Styles.color.background.white, layout: "vertical", width: Ti.UI.SIZE, height: Ti.UI.SIZE, id: "contents" });

	$.__views.payment_signature_window.add($.__views.contents);
	$.__views.padding_container = Ti.UI.createView(
	{ layout: "vertical", width: Ti.UI.SIZE, height: Ti.UI.SIZE, left: 50, right: 50, id: "padding_container" });

	$.__views.contents.add($.__views.padding_container);
	$.__views.prompt_signature = Alloy.createController('checkout/payments/promptSignature', { id: "prompt_signature", __parentSymbol: $.__views.padding_container });
	$.__views.prompt_signature.setParent($.__views.padding_container);
	exports.destroy = function () {};




	_.extend($, $.__views);










	var logger = require('logging')('checkout:payments:promptSignatureDialog', getFullControllerPath($.__controllerPath));





	$.listenTo($.prompt_signature, 'promptSignature:accept_signature', onAcceptSignature);




	exports.init = init;
	exports.deinit = deinit;
	exports.dismiss = dismiss;









	function init(args) {
		logger.info('INIT called');
		$.prompt_signature.init();
	}






	function deinit() {
		logger.info('DEINIT called');
		$.prompt_signature.deinit();
		$.stopListening();
		$.destroy();
	}










	function onAcceptSignature(event) {
		logger.info('onAcceptSignature called');

		Alloy.sigBlob = event.image;
		$.trigger('promptSignatureDialog:accept_signature', {
			image: event.image });

	}






	function dismiss() {
		$.trigger('promptSignatureDialog:dismiss', {
			approved: true });

	}









	_.extend($, exports);
}

module.exports = Controller;