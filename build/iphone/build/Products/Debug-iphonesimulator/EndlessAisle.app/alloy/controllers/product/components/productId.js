var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
		delete obj[key];
	}
	return arg;
}

function Controller() {

	require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'product/components/productId';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};







	$.__views.pdp_product_id = Ti.UI.createView(
	{ layout: "horizontal", height: Ti.UI.SIZE, width: "100%", left: 0, id: "pdp_product_id" });

	$.__views.pdp_product_id && $.addTopLevelView($.__views.pdp_product_id);
	$.__views.product_id_label = Ti.UI.createLabel(
	{ width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailTextFont, textid: "_Item__", id: "product_id_label", accessibilityValue: "pdp_info_text" });

	$.__views.pdp_product_id.add($.__views.product_id_label);
	$.__views.product_id = Ti.UI.createLabel(
	{ width: 240, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailTextFont, left: 3, ellipsize: true, id: "product_id", accessibilityValue: "product_id" });

	$.__views.pdp_product_id.add($.__views.product_id);
	exports.destroy = function () {};




	_.extend($, $.__views);










	var logger = require('logging')('product:components:productId', getFullControllerPath($.__controllerPath));




	exports.init = init;
	exports.deinit = deinit;
	exports.setId = setId;









	function init() {
		logger.info('init called');
	}






	function deinit() {
		logger.info('deinit called');
		$.stopListening();
		$.destroy();
	}









	function setId(id) {
		logger.info('setId called');
		if (id) {
			$.product_id.setText(id);
		}
	}









	_.extend($, exports);
}

module.exports = Controller;