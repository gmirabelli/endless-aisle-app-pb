var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'search/index';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.index = Ti.UI.createView(
  { id: "index", layout: "absolute" });

  $.__views.index && $.addTopLevelView($.__views.index);
  $.__views.searchHeader = Alloy.createController('search/components/searchHeader', { id: "searchHeader", __parentSymbol: $.__views.index });
  $.__views.searchHeader.setParent($.__views.index);
  if (Alloy.CFG.navigation.use_mega_menu) {
    $.__views.megaMenu = Alloy.createController('components/megaMenu', { id: "megaMenu", __parentSymbol: $.__views.index });
    $.__views.megaMenu.setParent($.__views.index);
  }
  $.__views.contents = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.color.background.white, top: 105, bottom: 0, right: 0, left: 0, id: "contents" });

  $.__views.index.add($.__views.contents);
  $.__views.scrollableContainer = Ti.UI.createView(
  { showPagingControl: 0, id: "scrollableContainer" });

  $.__views.contents.add($.__views.scrollableContainer);
  var __alloyId246 = [];
  $.__views.scrollableHits = Ti.UI.createScrollableView(
  { showPagingControl: 0, views: __alloyId246, id: "scrollableHits" });

  $.__views.scrollableContainer.add($.__views.scrollableHits);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var animation = require('alloy/animation');
  var logger = require('logging')('search:index', getFullControllerPath($.__controllerPath));

  var currentSearch = Alloy.Models.productSearch;
  var categoryModel = Alloy.Models.category;

  var MAX_PAGES = 48;

  var MODE_CATEGORY = 'category';
  var MODE_PRODUCTS = 'products';

  var pagingControls = null;
  var PagingControl = require('PagingControl');

  var gridMode;

  var productGridPages = [];




  $.listenTo(Alloy.eventDispatcher, 'configurations:postlogin', onConfigurationsLoaded);




  $.scrollableContainer.addEventListener('product:select', handleProductSelection);
  $.scrollableContainer.addEventListener('product:zoom', handleProductZoomSelection);




  $.listenTo(currentSearch, 'change', onCurrentSearchChange);




  exports.init = init;
  exports.deinit = deinit;










  function init(query) {
    logger.trace('init start');

    var deferred = new _.Deferred();


    if (!query) {
      deferred.reject();
    } else if (query.category_id == Alloy.CFG.root_category_id && query.query == '') {

      if (gridMode == MODE_CATEGORY) {

        deferred.resolveWith(currentSearch, [currentSearch]);
      } else {
        gridMode = MODE_CATEGORY;
        logger.trace('init fetch before');
        currentSearch.emptySearch().done(function () {
          logger.trace('init fetch start');
          deferred.resolveWith(currentSearch, [currentSearch]);
          logger.trace('init fetch end');
        });
      }
    } else {

      gridMode = MODE_PRODUCTS;
      var psr = currentSearch.simpleClone();

      var sel_refinements = {};
      if (query.category_id) {
        sel_refinements.cgid = query.category_id;
        psr.set('selected_refinements', sel_refinements, {
          silent: true });

        delete query.category_id;
      } else {
        sel_refinements.cgid = Alloy.CFG.root_category_id;
      }
      query = _.extend({
        count: Alloy.CFG.product_search.search_products_returned },
      query);

      psr.set(query, {
        silent: true });


      logger.trace('init fetch before');
      psr.fetch({
        silent: true }).
      done(function () {
        logger.trace('init fetch start');
        var count = psr.getCount();
        if (count == 1) {


          var productId = psr.getHits()[0].getProductId();
          handleProductSelection({
            product_id: productId,
            variant_id: psr.getQuery() });

          deferred.rejectWith(psr, [psr]);
        } else if (count == 0) {


          var message,
          query = psr.getQuery();
          if (query) {
            message = String.format(_L('No results found for \'%s\'\nTry a new search or click the home button to browse catalog'), query);
          } else {
            message = _L('No results found\nTry a new search or click the home button to browse catalog');
          }
          setTimeout(function () {
            Alloy.Router.presentProductSearchDrawer({
              message: message });

          }, 300);

          deferred.rejectWith(psr, [psr]);
        } else {




          currentSearch.initialize();
          currentSearch.set(psr.toJSON());

          deferred.resolveWith(currentSearch, [currentSearch]);
        }
        logger.trace('init fetch end');
      }).fail(function () {
        setTimeout(function () {
          Alloy.Router.presentProductSearchDrawer({
            message: _L('Unable to search products.') });

        }, 300);
        deferred.reject();
      });
    }
    logger.trace('init end');
    return deferred.promise();
  }






  function deinit() {
    logger.info('DEINIT called');
    $.searchHeader.deinit();
    if (Alloy.CFG.navigation.use_mega_menu) {
      $.megaMenu.deinit();
    }


    $.scrollableContainer.removeEventListener('product:select', handleProductSelection);
    $.scrollableContainer.removeEventListener('product:zoom', handleProductZoomSelection);
    removeAllExistingHitsViews();
    removeAllChildren($.scrollableContainer);
    removeAllChildren($.contents);
    $.stopListening();
    $.destroy();
  }









  function renderCategoryGrid() {
    logger.trace('renderCategoryGrid start');

    var categoryGrid = Alloy.createController('search/components/categoryGrid', {
      id: Alloy.CFG.root_category_id,
      $model: categoryModel });

    var scrollingContainer = Ti.UI.createScrollView({
      showHorizontalIndicator: false,
      showVerticalIndicator: true,
      contentHeight: 'auto' });

    scrollingContainer.add(categoryGrid.getView());
    scrollingContainer.category_id = Alloy.CFG.root_category_id;

    updateView(scrollingContainer);


    $.categoryGrid = categoryGrid;

    logger.trace('renderCategoryGrid end');
  }






  function renderProductGrid() {
    logger.trace('render start search index');


    var total = currentSearch.getTotal(),
    count = currentSearch.getCount(),
    productGrid,
    query = currentSearch.getQuery(),
    scrollToResults = false;
    var perPage = Alloy.CFG.product_search.products_per_page || 8,
    left = total,
    offset = 0;

    if (total == 1 && !currentSearch.force) {

      return;
    } else if (count == 0) {


      var message;
      if (query) {
        message = String.format(_L('No results found for \'%s\'\nTry a new search or click the home button to browse catalog'), query);
      } else {
        message = _L('No results found\nTry a new search or click the home button to browse catalog');
      }
      setTimeout(function () {
        Alloy.Router.presentProductSearchDrawer({
          message: message });

      }, 300);
      return;
    }

    if (query && query.length > 0) {
      scrollToResults = true;
    }

    if (Alloy.Platform == 'iphone') {
      perPage = 4;
    }

    var adjustedOffset,
    gridViews = [],
    count = 0,
    batch;
    var pGridPages = [];
    while (left > 0 && offset < perPage * MAX_PAGES) {
      adjustedOffset = offset;
      while (adjustedOffset >= Alloy.CFG.product_search.search_products_returned) {
        adjustedOffset -= Alloy.CFG.product_search.search_products_returned;
      }

      batch = Math.floor(offset / Alloy.CFG.product_search.search_products_returned);

      productGrid = Alloy.createController('search/components/productGrid', {
        offset: adjustedOffset,
        $model: offset < Alloy.CFG.product_search.search_products_returned ? currentSearch : currentSearch.batchSearch(batch) });

      pGridPages.push(productGrid);
      gridViews.push(productGrid.getView());

      left -= perPage;
      offset += perPage;
    }
    updateView(gridViews);

    productGridPages = pGridPages;

    if ($.scrollableHits.getCurrentPage() == 0) {

      setTimeout(function () {
        ensureResultsExistForPage(1);
        setTimeout(function () {
          ensureResultsExistForPage(2);
        }, 0);
      }, 0);
    }
    logger.trace('render end search index');
  }






  function deinitProductGrids() {
    _.each(productGridPages, function (pGridController) {
      pGridController.deinit();
    });
    productGridPages = [];
  }










  function updateView(newView) {
    logger.info('updateView called current page ' + $.scrollableHits.getCurrentPage());
    removeAllExistingHitsViews();
    var newScrollable = Ti.UI.createScrollableView({
      showPagingControl: false,
      id: 'scrollableHits' });

    if (_.isArray(newView)) {
      newScrollable.setViews(newView);
    } else {
      newScrollable.addView(newView);
    }

    $.scrollableContainer.remove($.scrollableHits);
    $.scrollableHits = null;
    $.scrollableHits = newScrollable;
    $.scrollableHits.addEventListener('pagechanged', handlePageChangedEvent);

    $.scrollableContainer.add($.scrollableHits);

    showPagingControlIfNecessary($.scrollableHits);
  }






  function removeAllExistingHitsViews() {
    logger.trace('removeAllExistingHitsViews start');
    $.categoryGrid && $.categoryGrid.deinit();
    $.scrollableHits.setCurrentPage(0);

    _.each($.scrollableHits.views, function (view) {
      $.scrollableHits.removeView(view);
      view = null;
    });
    $.scrollableHits.views = [];

    if (pagingControls) {
      $.contents.remove(pagingControls.getView());
      pagingControls.deinit();
      pagingControls = null;
    }
    deinitProductGrids();
    logger.trace('removeAllExistingHitsViews end');
  }







  function showPagingControlIfNecessary(scrollableView) {
    if (scrollableView.views && scrollableView.views.length > 1) {
      pagingControls = new PagingControl(scrollableView);
      $.contents.add(pagingControls.getView());
    } else {
      scrollableView.showPagingControl = false;
      pagingControls = null;
    }
  }







  function ensureResultsExistForPage(page) {
    if (page < 1) {
      return;
    }
    var productGrid = $.scrollableHits.views[page];

    setTimeout(function () {
      if (productGrid && productGrid.loadPage) {
        productGrid.loadPage();
      }
    }, 0);
  }










  function handleCategorySelection(event) {
    logger.info('handleCategorySelection: ' + event.category_id);

    Alloy.Router.navigateToProductSearch({
      query: '',
      category_id: event.category_id });

  }










  function handleProductSelection(event) {
    logger.info('product selection');
    Alloy.Router.navigateToProduct({
      product_id: event.product_id,
      variant_id: event.variant_id });

  }







  function handleProductZoomSelection(event) {
    logger.trace('handleProductZoomSelection start');

    Alloy.Dialog.showCustomDialog({
      controllerPath: 'product/images/imageZoom',
      initOptions: {
        product_id: event.product_id,
        selected_variation_value: event.selected_variation_value },

      continueEvent: 'image_zoom:dismiss' });


    logger.trace('handleProductZoomSelection end');
  }







  function handlePageChangedEvent(event) {
    logger.trace('**handlePageChanged currentPage ' + event.currentPage);
    var productGrid = $.scrollableHits.views[event.currentPage];
    if (productGrid) {
      productGrid.renderPage();
    }

    ensureResultsExistForPage(event.currentPage + 2);
  }










  function onCurrentSearchChange(model) {

    if (!model.changed.query && !model.changed.selected_refinements) {
      return;
    }
    logger.info('received change:selected_refinements');
    var is_empty = currentSearch.isEmptySearch();
    if (is_empty) {
      categoryModel.attributes.id = Alloy.CFG.root_category_id;
      categoryModel.fetch().done(function () {
        renderCategoryGrid();
      });
    } else {
      renderProductGrid();
    }
  }






  function onConfigurationsLoaded() {
    if (Alloy.CFG.navigation.use_mega_menu) {
      $.megaMenu.init();
    }
  }









  _.extend($, exports);
}

module.exports = Controller;