var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'associate/storePassword';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.store_password_window = Ti.UI.createView(
  { layout: "absolute", backgroundColor: Alloy.Styles.color.background.transparent, id: "store_password_window" });

  $.__views.store_password_window && $.addTopLevelView($.__views.store_password_window);
  $.__views.backdrop = Ti.UI.createView(
  { top: 0, left: 0, width: 1024, height: 768, opacity: 0.55, backgroundColor: Alloy.Styles.color.background.black, id: "backdrop" });

  $.__views.store_password_window.add($.__views.backdrop);
  $.__views.manager = Ti.UI.createView(
  { top: 22, width: 465, height: Ti.UI.SIZE, layout: "vertical", backgroundColor: Alloy.Styles.color.background.white, id: "manager" });

  $.__views.store_password_window.add($.__views.manager);
  $.__views.manager_view = Alloy.createController('associate/authorization', { id: "manager_view", __parentSymbol: $.__views.manager });
  $.__views.manager_view.setParent($.__views.manager);
  $.__views.store_password = Ti.UI.createView(
  { top: 22, width: 465, height: Ti.UI.SIZE, layout: "vertical", backgroundColor: Alloy.Styles.color.background.white, id: "store_password" });

  $.__views.store_password_window.add($.__views.store_password);
  $.__views.store_password_title = Ti.UI.createLabel(
  { width: Ti.UI.FILL, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.titleFont, top: 26, left: 32, right: 32, textid: "_Change_Store_Password", id: "store_password_title", accessibilityValue: "store_password_title" });

  $.__views.store_password.add($.__views.store_password_title);
  $.__views.store_subtitle = Ti.UI.createLabel(
  { width: Ti.UI.FILL, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, left: 32, right: 32, verticalAlign: Ti.UI.TEXT_VERTICAL_ALIGNMENT_TOP, bottom: 1, id: "store_subtitle", accessibilityValue: "store_username" });

  $.__views.store_password.add($.__views.store_subtitle);
  $.__views.store_password_error_label = Ti.UI.createLabel(
  { width: Ti.UI.FILL, height: 0, color: Alloy.Styles.color.text.red, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, left: 32, right: 32, font: Alloy.Styles.calloutCopyFont, visible: false, verticalAlign: Ti.UI.TEXT_VERTICAL_ALIGNMENT_TOP, bottom: 20, id: "store_password_error_label", accessibilityValue: "store_password_error_label" });

  $.__views.store_password.add($.__views.store_password_error_label);
  $.__views.scroll_fields = Ti.UI.createScrollView(
  { layout: "vertical", height: 200, id: "scroll_fields" });

  $.__views.store_password.add($.__views.scroll_fields);
  $.__views.current_password = Ti.UI.createTextField(
  { left: 32, right: 32, passwordMask: true, color: Alloy.Styles.color.text.dark, borderColor: Alloy.Styles.color.border.light, borderWidth: 1, keyboardType: Ti.UI.KEYBOARD_TYPE_NUMBERS_PUNCTUATION, returnKeyType: Ti.UI.RETURNKEY_GO, bottom: 15, width: Ti.UI.FILL, font: Alloy.Styles.dialogFieldFont, padding: { left: 18 }, height: 55, hintText: L('_Current_Password'), id: "current_password", accessibilityLabel: "current_store_password" });

  $.__views.scroll_fields.add($.__views.current_password);
  $.__views.password = Ti.UI.createTextField(
  { left: 32, right: 32, passwordMask: true, color: Alloy.Styles.color.text.dark, borderColor: Alloy.Styles.color.border.light, borderWidth: 1, keyboardType: Ti.UI.KEYBOARD_TYPE_NUMBERS_PUNCTUATION, returnKeyType: Ti.UI.RETURNKEY_GO, bottom: 15, width: Ti.UI.FILL, font: Alloy.Styles.dialogFieldFont, padding: { left: 18 }, height: 55, hintText: L('_Enter_New_Password'), id: "password", accessibilityLabel: "new_store_password" });

  $.__views.scroll_fields.add($.__views.password);
  $.__views.password_verify = Ti.UI.createTextField(
  { left: 32, right: 32, passwordMask: true, color: Alloy.Styles.color.text.dark, borderColor: Alloy.Styles.color.border.light, borderWidth: 1, keyboardType: Ti.UI.KEYBOARD_TYPE_NUMBERS_PUNCTUATION, returnKeyType: Ti.UI.RETURNKEY_GO, bottom: 15, width: Ti.UI.FILL, font: Alloy.Styles.dialogFieldFont, padding: { left: 18 }, height: 55, hintText: L('_Repeat_New_Password'), id: "password_verify", accessibilityLabel: "new_store_password_verify" });

  $.__views.scroll_fields.add($.__views.password_verify);
  $.__views.button_view = Ti.UI.createView(
  { layout: "horizontal", top: 20, height: Ti.UI.SIZE, width: Ti.UI.SIZE, bottom: 20, id: "button_view" });

  $.__views.store_password.add($.__views.button_view);
  $.__views.store_password_cancel_button = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, backgroundImage: Alloy.Styles.buttons.secondary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.secondary.backgroundDisabledImage, color: Alloy.Styles.buttons.secondary.color, disabledColor: Alloy.Styles.buttons.secondary.disabledColor, height: 40, width: 190, titleid: "_Cancel", id: "store_password_cancel_button", accessibilityValue: "store_password_cancel_button" });

  $.__views.button_view.add($.__views.store_password_cancel_button);
  $.__views.change_store_password_button = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.buttons.primary.color, disabledColor: Alloy.Styles.buttons.primary.disabledColor, backgroundImage: Alloy.Styles.buttons.primary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.primary.backgroundDisabledImage, height: 40, width: 190, left: 20, titleid: "_Change_Password", id: "change_store_password_button", accessibilityValue: "change_store_password_button" });

  $.__views.button_view.add($.__views.change_store_password_button);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var currentAssociate = Alloy.Models.associate;

  var showActivityIndicator = require('dialogUtils').showActivityIndicator;
  var logger = require('logging')('associate:storePassword', getFullControllerPath($.__controllerPath));
  var EAUtils = require('EAUtils');
  var storePasswordHelpers = require('storePasswordHelpers');

  var viewHeight = 379;

  var args = arguments[0] || {};

  var isManager = args.isManager || false;


  var errorLabelLength = 60;
  var symbolErrorLabelLength = 27;
  var symbolButtonTextLength = 9;




  $.store_password_cancel_button.addEventListener('click', dismiss);
  $.change_store_password_button.addEventListener('click', changePassword);
  $.current_password.addEventListener('return', changePassword);
  $.password.addEventListener('return', changePassword);
  $.password_verify.addEventListener('return', changePassword);




  exports.deinit = deinit;
  exports.init = init;









  function init() {
    logger.info('INIT');


    var deferred = new _.Deferred();
    if (!Alloy.Models.storeUser) {
      deferred = storePasswordHelpers.checkStorePassword(true);
    } else {
      deferred.resolve();
    }

    deferred.done(function () {
      $.store_subtitle.setText(String.format(_L('Enter current and new password for \'%s\''), Alloy.Models.storeUser.getStoreUsername()));

      if (currentAssociate.hasAdminPrivileges() || isManager || getKioskManager() && getKioskManager().hasAdminPrivileges()) {
        displayStorePasswordChange();
      } else {
        displayManagerCredentials();
      }
    });
    return deferred.promise();
  }






  function deinit() {
    logger.info('DEINIT');

    $.toolbar && $.toolbar.deinit();

    $.store_password_cancel_button.removeEventListener('click', dismiss);
    $.change_store_password_button.removeEventListener('click', changePassword);
    $.current_password.removeEventListener('return', changePassword);
    $.password.removeEventListener('return', changePassword);
    $.password_verify.removeEventListener('return', changePassword);

    $.manager_view.deinit();

    $.stopListening();
    $.destroy();
  }









  function displayManagerCredentials() {
    $.store_password.hide();
    $.store_password.setHeight(0);

    var manager = Alloy.createModel('associate');
    $.manager_view.init({
      associate: manager,
      subTitleText: _L('Enter Manager Credentials Store'),
      successMessage: _L('Manager Credentials Accepted'),
      submitFunction: displayStorePasswordChange,
      cancelFunction: dismiss });


    $.manager.setHeight(viewHeight);

    var authorizationView = $.manager_view.getView('enter_manager_authorization');
    authorizationView.setHeight(Ti.UI.FILL);
    authorizationView.setWidth(Ti.UI.FILL);
    authorizationView.setLeft(0);
    authorizationView.setTop(0);

    $.manager.show();
  }






  function displayStorePasswordChange() {
    $.manager.hide();
    $.manager.setHeight(0);

    $.toolbar = Alloy.createController('components/nextPreviousToolbar');
    $.toolbar.setTextFields([$.current_password, $.password, $.password_verify]);

    $.store_password.setHeight(Ti.UI.SIZE);
    $.store_password.show();
  }








  function validatePassword(newPassword) {

    return true;
  }







  function renderError(text) {
    if (text.length > errorLabelLength || EAUtils.isSymbolBasedLanguage() && text.length > symbolErrorLabelLength) {
      $.store_password_error_label.setFont(Alloy.Styles.smallerCalloutCopyFont);
    } else {
      $.store_password_error_label.setFont(Alloy.Styles.calloutCopyFont);
    }
    $.store_password_error_label.setHeight(Ti.UI.SIZE);
    $.store_password_error_label.setText(text);
    $.store_password_error_label.show();
  }






  function changePassword() {
    $.current_password.blur();
    $.password.blur();
    $.password_verify.blur();

    var currentPassword = $.current_password.getValue();
    var newPassword = $.password.getValue();
    var verifyPassword = $.password_verify.getValue();

    if (newPassword == '' || verifyPassword == '' || currentPassword == '') {
      renderError(_L('All fields must have a password.'));
      clearFields();
      return;
    }

    if (newPassword != verifyPassword) {
      renderError(_L('Passwords do not match'));
      clearFields();
      return;
    }

    if (newPassword === currentPassword) {
      renderError(_L('New password cannot be the same as current password'));
      clearFields();
      return;
    }

    if (!validatePassword(newPassword)) {
      renderError(_L('Password does not meet minimum criteria'));
      clearFields();
      return;
    }

    var deferred = new _.Deferred();
    Alloy.Router.showActivityIndicator(deferred);
    Alloy.Models.storeUser.updateStorePassword(currentPassword, newPassword).done(function () {
      var customObjects = Alloy.createModel('customObjects');
      customObjects.updateStorePasswords(Alloy.Models.storeUser.getStoreUsername(), newPassword).done(function () {
        deferred.resolve();
        dismiss();
        notify(_L('Store password has been changed successfully.'));
      }).fail(function (error) {
        deferred.reject();
        clearFields();
        renderError(error ? error.message : _L('Unable to change the store password for all stores.'));
      });
    }).fail(function (error) {
      deferred.reject();
      clearFields();
      renderError(error ? error.message : _L('Unable to change the store password.'));
    });
  }






  function dismiss() {
    $.trigger('storePassword:dismiss');
  }






  function clearFields() {
    $.current_password.setValue('');
    $.password.setValue('');
    $.password_verify.setValue('');
  }









  _.extend($, exports);
}

module.exports = Controller;