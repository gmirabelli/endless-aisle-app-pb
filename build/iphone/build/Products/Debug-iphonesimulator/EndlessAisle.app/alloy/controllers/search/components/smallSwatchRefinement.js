var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'search/components/smallSwatchRefinement';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.smallSwatchRefinementHeader = Ti.UI.createView(
  { height: 50, layout: "absolute", backgroundColor: Alloy.Styles.color.background.light, id: "smallSwatchRefinementHeader" });

  $.__views.smallSwatchRefinementHeaderLabel = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.buttonFont, left: 22, id: "smallSwatchRefinementHeaderLabel", accessibilityValue: "small_swatch_refinement_label" });

  $.__views.smallSwatchRefinementHeader.add($.__views.smallSwatchRefinementHeaderLabel);
  $.__views.smallSwatchRefinement = Ti.UI.createTableViewSection(
  { headerView: $.__views.smallSwatchRefinementHeader, id: "smallSwatchRefinement" });

  $.__views.smallSwatchRefinement && $.addTopLevelView($.__views.smallSwatchRefinement);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var listenerViews = [];
  var logger = require('logging')('search:components:smallSwatchRefinement', getFullControllerPath($.__controllerPath));

  var psr = Alloy.Models.productSearch;
  var refinement = $model;

  var visibleValues = 0;
  var onViewClick;




  exports.init = init;
  exports.deinit = deinit;
  exports.shouldBeVisible = shouldBeVisible;








  function init() {
    var values,
    rows = [],
    row,
    view,
    label;

    values = refinement.getValues();
    $.smallSwatchRefinementHeaderLabel.setText(refinement.getLabel());


    while ($.smallSwatchRefinement.rowCount > 0) {
      $.smallSwatchRefinement.remove($.smallSwatchRefinement.rows[0]);
    }

    row = Ti.UI.createTableViewRow({
      layout: 'horizontal',
      touchEnabled: false,
      allowsSelection: false,
      selectionStyle: Ti.UI.iOS.TableViewCellSelectionStyle.NONE });

    visibleValues = 0;

    _.each(values, function (value) {
      var value_id = value.getValue();
      var attribute_id = refinement.getAttributeId();
      var hit_count = value.getHitCount();
      var isRefinedBy = psr.isRefinedBy(attribute_id, value_id);

      if (hit_count == 0) {
        return;
      }
      visibleValues++;

      view = Ti.UI.createView({
        width: 50,
        height: 45,
        backgroundColor: isRefinedBy ? Alloy.Styles.accentColor : Alloy.Styles.color.background.medium,
        left: 10,
        top: 10,
        bottom: 10,
        layout: 'absolute',
        isRefinedBy: isRefinedBy,
        accessibilityLabel: 'select_' + value_id });

      label = Ti.UI.createLabel({
        text: value.getLabel(),
        font: Alloy.Styles.lineItemLabelFont,
        color: isRefinedBy ? Alloy.Styles.color.text.white : Alloy.Styles.color.text.mediumdark });

      view.add(label);
      row.add(view);

      onViewClick = function (event) {
        event.cancelBubble = true;
        handleSwatchClick({
          attribute_id: attribute_id,
          value_id: value_id });

      };

      view.addEventListener('click', onViewClick);
      listenerViews.push(view);
    });
    $.smallSwatchRefinement.add(row);
  }






  function deinit() {
    logger.info('DEINIT called');
    deinitExistingRows();
    $.stopListening();
    $.destroy();
  }









  function shouldBeVisible() {
    return visibleValues > 1;
  }






  function deinitExistingRows() {
    var view = null;
    removeAllChildren($.smallSwatchRefinement);
    _.each(listenerViews, function (view) {
      view.removeEventListener('click', onViewClick);
    });
  }









  function handleSwatchClick(event) {
    event.cancelBubble = true;
    logger.info('triggering refinement:select: ' + JSON.stringify(event));
    $.smallSwatchRefinement.fireEvent('refinement:toggle', event);
  }




  init();









  _.extend($, exports);
}

module.exports = Controller;