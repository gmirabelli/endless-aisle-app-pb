var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
		delete obj[key];
	}
	return arg;
}

function Controller() {

	require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'reports/quickStats';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};







	$.__views.quick_stat_row = Ti.UI.createTableViewRow(
	{ height: 100, id: "quick_stat_row" });

	$.__views.quick_stat_row && $.addTopLevelView($.__views.quick_stat_row);
	$.__views.container = Ti.UI.createView(
	{ height: Ti.UI.FILL, width: Ti.UI.FILL, id: "container" });

	$.__views.quick_stat_row.add($.__views.container);
	$.__views.value = Ti.UI.createLabel(
	{ width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.accentColor, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, top: "20%", font: Alloy.Styles.quickStatsValueFont, shadowColor: Alloy.Styles.shadowGrayColor, shadowOffset: { x: 1, y: 0 }, shadowRadius: 2, id: "value", accessibilityValue: "quick_stats_value", text: $model.__transform.value });

	$.__views.container.add($.__views.value);
	$.__views.title = Ti.UI.createLabel(
	{ width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.black, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.quickStatsTitleFont, top: "55%", id: "title", accessibilityValue: "quick_stats_title", text: $model.__transform.title });

	$.__views.container.add($.__views.title);
	$.__views.__alloyId230 = Ti.UI.createView(
	{ backgroundColor: Alloy.Styles.accentColor, width: "30%", height: 1, bottom: 0, id: "__alloyId230" });

	$.__views.container.add($.__views.__alloyId230);
	exports.destroy = function () {};




	_.extend($, $.__views);












	_.extend($, exports);
}

module.exports = Controller;