var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'associate/authorization';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.auth_window = Ti.UI.createView(
  { zIndex: 101, backgroundColor: Alloy.Styles.color.background.transparent, id: "auth_window" });

  $.__views.auth_window && $.addTopLevelView($.__views.auth_window);
  $.__views.backdrop = Ti.UI.createView(
  { top: 0, left: 0, width: 1024, height: 768, opacity: 0.25, backgroundColor: Alloy.Styles.color.background.black, id: "backdrop" });

  $.__views.auth_window.add($.__views.backdrop);
  $.__views.enter_manager_authorization = Ti.UI.createView(
  { layout: "vertical", width: 465, left: 280, height: 340, top: 75, backgroundColor: Alloy.Styles.color.background.white, id: "enter_manager_authorization" });

  $.__views.auth_window.add($.__views.enter_manager_authorization);
  $.__views.inner_container = Ti.UI.createView(
  { height: "100%", width: "100%", layout: "fixed", id: "inner_container" });

  $.__views.enter_manager_authorization.add($.__views.inner_container);
  $.__views.top_container = Ti.UI.createView(
  { height: Ti.UI.SIZE, width: "100%", top: 0, layout: "vertical", id: "top_container" });

  $.__views.inner_container.add($.__views.top_container);
  $.__views.title = Ti.UI.createLabel(
  { width: Ti.UI.FILL, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, verticalAlign: Ti.UI.TEXT_VERTICAL_ALIGNMENT_TOP, left: 32, right: 32, font: Alloy.Styles.titleFont, top: 26, id: "title", accessibilityValue: "manager_auth_title" });

  $.__views.top_container.add($.__views.title);
  $.__views.subtitle = Ti.UI.createLabel(
  { width: Ti.UI.FILL, height: 0, color: Alloy.Styles.color.text.medium, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, verticalAlign: Ti.UI.TEXT_VERTICAL_ALIGNMENT_TOP, left: 32, right: 32, font: Alloy.Styles.appFont, visible: false, bottom: 1, id: "subtitle", accessibilityValue: "manager_auth_subtitle" });

  $.__views.top_container.add($.__views.subtitle);
  $.__views.error = Ti.UI.createLabel(
  { width: Ti.UI.FILL, height: 0, color: Alloy.Styles.color.text.red, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, verticalAlign: Ti.UI.TEXT_VERTICAL_ALIGNMENT_TOP, left: 32, right: 32, font: Alloy.Styles.calloutCopyFont, visible: false, bottom: 20, id: "error", accessibilityValue: "manager_auth_error" });

  $.__views.top_container.add($.__views.error);
  $.__views.content_view = Ti.UI.createView(
  { top: 10, height: Titanium.UI.SIZE, width: Titanium.UI.SIZE, layout: "vertical", id: "content_view" });

  $.__views.top_container.add($.__views.content_view);
  $.__views.manager_id = Ti.UI.createTextField(
  { color: Alloy.Styles.color.text.dark, font: Alloy.Styles.dialogFieldFont, left: 32, right: 32, width: Ti.UI.FILL, height: 55, borderColor: Alloy.Styles.color.border.light, passwordMask: true, padding: { left: 18 }, keyboardType: Ti.UI.KEYBOARD_TYPE_NUMBERS_PUNCTUATION, returnKeyType: Ti.UI.RETURNKEY_GO, top: 3, id: "manager_id", accessibilityLabel: "manager_id" });

  $.__views.content_view.add($.__views.manager_id);
  $.__views.manager_password = Ti.UI.createTextField(
  { color: Alloy.Styles.color.text.dark, font: Alloy.Styles.dialogFieldFont, left: 32, right: 32, width: Ti.UI.FILL, height: 55, borderColor: Alloy.Styles.color.border.light, passwordMask: true, padding: { left: 18 }, keyboardType: Ti.UI.KEYBOARD_TYPE_NUMBERS_PUNCTUATION, returnKeyType: Ti.UI.RETURNKEY_GO, top: 15, id: "manager_password", accessibilityLabel: "manager_password" });

  $.__views.content_view.add($.__views.manager_password);
  $.__views.buttons = Ti.UI.createView(
  { height: Titanium.UI.SIZE, width: Titanium.UI.SIZE, layout: "horizontal", bottom: 20, id: "buttons" });

  $.__views.inner_container.add($.__views.buttons);
  $.__views.cancel_button = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, backgroundImage: Alloy.Styles.buttons.secondary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.secondary.backgroundDisabledImage, color: Alloy.Styles.buttons.secondary.color, disabledColor: Alloy.Styles.buttons.secondary.disabledColor, height: 40, width: 190, id: "cancel_button", accessibilityValue: "cancel_button" });

  $.__views.buttons.add($.__views.cancel_button);
  $.__views.submit_button = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.buttons.primary.color, disabledColor: Alloy.Styles.buttons.primary.disabledColor, backgroundImage: Alloy.Styles.buttons.primary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.primary.backgroundDisabledImage, height: 40, width: 190, left: 20, id: "submit_button", accessibilityValue: "submit_button" });

  $.__views.buttons.add($.__views.submit_button);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var appSettings = require('appSettings');
  var dialogUtils = require('dialogUtils');
  var EAUtils = require('EAUtils');

  var checkForAdminPrivileges = true,
  showCancelButton = true;
  var postAuthorizationFunction, submitFunction, cancelFunction;
  var associate, initConfig, successMessage;


  var errorLabelLength = 60;
  var symbolErrorLabelLength = 22;




  $.manager_id.addEventListener('return', validateManager);
  $.manager_password.addEventListener('return', validateManager);




  exports.init = init;
  exports.deinit = deinit;
  exports.showErrorMessage = showErrorMessage;
  exports.clearErrorMessage = clearErrorMessage;
  exports.updateLabels = updateLabels;
























  function init(config) {
    $.toolbar = Alloy.createController('components/nextPreviousToolbar');
    $.toolbar.setTextFields([$.manager_id, $.manager_password]);

    $.submit_button.addEventListener('click', validateManager);
    $.cancel_button.addEventListener('click', onCancel);

    if (config) {
      if (config.hasOwnProperty('checkForAdminPrivileges')) {
        checkForAdminPrivileges = config.checkForAdminPrivileges;
      }
      if (config.hasOwnProperty('showCancelButton')) {
        showCancelButton = config.showCancelButton;
        if (!showCancelButton) {
          $.cancel_button.setLeft(0);
          $.cancel_button.setWidth(0);
          $.submit_button.setWidth(400);
          $.submit_button.setLeft(3);
          $.cancel_button.setVisible(false);
          $.cancel_button.removeEventListener('click', onCancel);
        }
      }
      if (config.hasOwnProperty('postAuthorizationFunction')) {
        postAuthorizationFunction = config.postAuthorizationFunction;
      }
      if (config.hasOwnProperty('submitFunction')) {
        submitFunction = config.submitFunction;
      }
      if (showCancelButton && config.hasOwnProperty('cancelFunction')) {
        cancelFunction = config.cancelFunction;
      }
      if (config.hasOwnProperty('associate')) {
        associate = config.associate;
      }
      if (config.hasOwnProperty('successMessage')) {
        successMessage = config.successMessage;
      }
    }

    initConfig = config;


    updateLabels();
  }






  function deinit() {
    logger.info('DEINIT called');
    $.submit_button.removeEventListener('click', validateManager);
    $.cancel_button.removeEventListener('click', dismiss);
    $.manager_id.removeEventListener('return', validateManager);
    $.manager_password.removeEventListener('return', validateManager);
    $.toolbar && $.toolbar.deinit();
    $.stopListening();
    $.destroy();
  }












  function showErrorMessage(message, resetFields) {

    if ($.subtitle.getText()) {
      $.subtitle.hide();
      $.subtitle.setHeight(0);
    }

    if (message.length > errorLabelLength || EAUtils.isSymbolBasedLanguage() && message.length > symbolErrorLabelLength) {
      $.error.setFont(Alloy.Styles.smallerCalloutCopyFont);
    } else {
      $.error.setFont(Alloy.Styles.calloutCopyFont);
    }

    $.error.setText(message);
    $.error.setHeight(Ti.UI.SIZE);
    $.error.show();
    if (resetFields) {
      $.manager_id.setValue('');
      $.manager_password.setValue('');
    }
  }






  function clearErrorMessage() {
    $.error.hide();
    $.error.setText('');
    $.error.setHeight(0);

    if ($.subtitle.getText()) {
      $.subtitle.setHeight(Ti.UI.SIZE);
      $.subtitle.show();
    }
  }







  function updateLabels() {
    if (initConfig && initConfig.hasOwnProperty('titleText')) {
      $.title.setText(initConfig.titleText);
    } else {
      $.title.setText(_L('Manager Authorization'));
    }
    if (initConfig && initConfig.hasOwnProperty('subTitleText')) {
      $.subtitle.setText(initConfig.subTitleText);
      $.subtitle.setHeight(Ti.UI.SIZE);
      $.subtitle.show();
    }
    if (initConfig && initConfig.hasOwnProperty('managerIdHintText')) {
      $.manager_id.setHintText(initConfig.managerIdHintText);
    } else {
      $.manager_id.setHintText(_L('Enter Manager ID'));
    }
    if (initConfig && initConfig.hasOwnProperty('managerPasswordHintText')) {
      $.manager_password.setHintText(initConfig.managerPasswordHintText);
    } else {
      $.manager_password.setHintText(_L('Enter Manager Password'));
    }
    if (initConfig && initConfig.hasOwnProperty('submitButtonText')) {
      $.submit_button.setTitle(initConfig.submitButtonText);
    } else {
      $.submit_button.setTitle(_L('Authorize'));
    }
    if (initConfig && showCancelButton && initConfig.hasOwnProperty('cancelButtonText')) {
      $.cancel_button.setTitle(initConfig.cancelButtonText);
    } else {
      $.cancel_button.setTitle(_L('Cancel'));
    }
  }









  function validateManager() {
    $.manager_password.blur();
    $.manager_id.blur();

    var employee_id = $.manager_id.getValue();
    var passcode = $.manager_password.getValue();
    if (!employee_id || !passcode) {
      showErrorMessage(_L('You must provide employee code and pin.'), false);
      return;
    }

    var deferred = new _.Deferred();
    dialogUtils.showActivityIndicator(deferred);
    var shouldLogout = false;
    var shouldResolve = true;


    if (!associate) {
      associate = Alloy.createModel('associate');
      shouldLogout = true;
    }
    associate.loginAssociate({
      employee_id: employee_id,
      passcode: passcode }).
    done(function (model) {
      var result = checkForAdminPrivileges ? associate.hasAdminPrivileges() : true;
      if (result) {
        notify(successMessage ? successMessage : _L('Associate Credentials Accepted'));

        $.manager_id.setValue('');
        $.manager_password.setValue('');

        var data = {
          result: result,
          associate: associate };

        if (postAuthorizationFunction) {
          shouldResolve = false;
          postAuthorizationFunction(this, deferred).done(function () {
            completionHandler(data, shouldLogout);
          }).always(function () {
            deferred.resolve();
          });
        } else {
          completionHandler(data, shouldLogout);
        }
      } else {
        showErrorMessage(_L('This manager does not have administrative privileges.'), true);
      }
    }).fail(function (model) {
      if (model instanceof Backbone.Model && model.has('httpStatus') && model.get('httpStatus') != 200 && model.has('fault')) {
        showErrorMessage(model.get('fault').message, false);
      } else if (model.hasOwnProperty('error') && model.hasOwnProperty('success') && model.success == false) {
        showErrorMessage(model.error, false);
      } else {
        showErrorMessage(_L('Error logging in associate'), false);
      }
      $.manager_password.setValue('');
    }).always(function () {
      if (shouldResolve) {
        deferred.resolve();
      }
    });
  }









  function completionHandler(data, shouldLogout) {
    dismiss(data);
    if (submitFunction) {
      submitFunction(data);
    }
    if (shouldLogout) {
      associate.logout();
    }
  }






  function onCancel() {
    dismiss();
    if (cancelFunction) {
      cancelFunction();
    }
    $.manager_id.setValue('');
    $.manager_password.setValue('');
  }






  function dismiss(data) {
    $.trigger('authorization:dismiss', data ? {
      result: data.result,
      associate: data.associate } :
    {
      result: false,
      associate: associate });

  }









  _.extend($, exports);
}

module.exports = Controller;