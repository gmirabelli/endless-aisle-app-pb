var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'checkout/components/shippingSummary';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};




  $.shipping_address = Alloy.createModel('shippingAddress');$.shippingMethod = Alloy.createModel('shippingMethod');


  $.__views.shipping_address_container = Ti.UI.createView(
  { layout: "vertical", height: Ti.UI.SIZE, left: 0, top: 0, id: "shipping_address_container" });

  $.__views.shipping_address_container && $.addTopLevelView($.__views.shipping_address_container);
  $.__views.shipping_details_label_container = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.color.background.medium, borderWidth: 1, borderColor: Alloy.Styles.color.border.darker, top: 0, left: 0, width: "100%", height: 30, layout: "horizontal", id: "shipping_details_label_container" });

  $.__views.shipping_address_container.add($.__views.shipping_details_label_container);
  $.__views.shipping_details_label = Ti.UI.createLabel(
  { width: "100%", height: 28, color: Alloy.Styles.accentColor, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, top: 2, font: Alloy.Styles.smallAccentFont, left: 23, textid: "_Shipping_Details", id: "shipping_details_label", accessibilityValue: "shipping_details_label" });

  $.__views.shipping_details_label_container.add($.__views.shipping_details_label);
  $.__views.address_display = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.sectionTitleFont, left: 20, top: 10, id: "address_display", accessibilityValue: "summary_address_display" });

  $.__views.shipping_address_container.add($.__views.address_display);
  $.__views.phone = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.sectionTitleFont, left: 20, top: 5, id: "phone", accessibilityValue: "phone" });

  $.__views.shipping_address_container.add($.__views.phone);
  $.__views.email_address_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.sectionTitleFont, left: 20, top: 5, id: "email_address_label", accessibilityValue: "email_address_label" });

  $.__views.shipping_address_container.add($.__views.email_address_label);
  $.__views.shipping_method_details_container = Ti.UI.createView(
  { layout: "horizontal", height: Ti.UI.SIZE, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 20, top: 10, id: "shipping_method_details_container" });

  $.__views.shipping_address_container.add($.__views.shipping_method_details_container);
  $.__views.method_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailLabelFont, textid: "_Method_", id: "method_label", accessibilityValue: "method_label" });

  $.__views.shipping_method_details_container.add($.__views.method_label);
  $.__views.shipping_type = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailLabelFont, left: 5, id: "shipping_type", accessibilityValue: "shipping_type" });

  $.__views.shipping_method_details_container.add($.__views.shipping_type);
  $.__views.giftMessage = Alloy.createController('components/orders/giftMessage', { id: "giftMessage", __parentSymbol: $.__views.shipping_address_container });
  $.__views.giftMessage.setParent($.__views.shipping_address_container);
  var __alloyId58 = function () {$['shipping_address'].__transform = _.isFunction($['shipping_address'].transform) ? $['shipping_address'].transform() : $['shipping_address'].toJSON();$.address_display.text = $['shipping_address']['__transform']['addressLabel'];$.phone.text = $['shipping_address']['__transform']['phone'];};$['shipping_address'].on('fetch change destroy', __alloyId58);var __alloyId59 = function () {$['shippingMethod'].__transform = _.isFunction($['shippingMethod'].transform) ? $['shippingMethod'].transform() : $['shippingMethod'].toJSON();$.shipping_type.text = $['shippingMethod']['__transform']['name'];};$['shippingMethod'].on('fetch change destroy', __alloyId59);exports.destroy = function () {$['shipping_address'] && $['shipping_address'].off('fetch change destroy', __alloyId58);$['shippingMethod'] && $['shippingMethod'].off('fetch change destroy', __alloyId59);};




  _.extend($, $.__views);










  var args = arguments[0] || {};

  $model = $model || args.shipping_address;

  var currentBasket = Alloy.Models.basket;
  var toCountryName = require('EAUtils').countryCodeToCountryName;
  var logger = require('logging')('checkout:components:shippingSummary', getFullControllerPath($.__controllerPath));

  var oldShippingAddress = null;
  var oldShippingMethod = null;





  $.listenTo(Alloy.eventDispatcher, 'order_just_created', function (order) {
    logger.info('Event fired: order_just_created');
    $.shippingMethod.clear();
    $.shippingMethod.set(order.getShippingMethod().toJSON());
    $.shipping_address.set(order.getShippingAddress().toJSON());
    $.email_address_label.setText(order.getCustomerEmail());

    $.phone.setHeight($.shipping_address.getPhone() && $.shipping_address.getPhone() != 'undefined' ? 15 : 0);
    $.giftMessage.init(order.getIsGift(), order.getGiftMessage());
  });




  $.listenTo(currentBasket, 'change:shipments reset:shipments basket_shipping_methods', function (type, model, something, options) {
    logger.info('Event fired: shipments');
    render(currentBasket);
  });

  $.listenTo(currentBasket, 'change:customer_info reset:customer_info', function (type, model, something, options) {
    logger.info('Event fired: customer_info');
    if (currentBasket.getCustomerInfo()) {
      $.email_address_label.setText(currentBasket.getCustomerEmail());
    }
  });

  $.listenTo(currentBasket, 'change:checkout_status', function () {
    var checkoutStatus = currentBasket.getCheckoutStatus();


    if (checkoutStatus != 'cart') {

      if (checkoutStatus == 'shippingAddress') {
        if (currentBasket.getShippingAddress()) {
          this.getView().show();
        } else {
          this.getView().hide();
        }
      } else {
        this.getView().show();
      }
    } else {
      this.getView().hide();
    }
  });




  exports.render = render;
  exports.deinit = deinit;










  function render(model) {
    if (model) {
      var shippingAddress = model.getShippingAddress();
      var newAddress = shippingAddress ? Ti.Utils.sha256(JSON.stringify(shippingAddress.toJSON())) : null;
      if (newAddress != oldShippingAddress) {
        logger.info('updating shipping address');
        $.shipping_address.clear();
        if (shippingAddress) {
          $.shipping_address.set(shippingAddress.toJSON());
        } else {
          $.email_address_label.setText('');
        }

        $.phone.setHeight($.shipping_address.getPhone() && $.shipping_address.getPhone() != 'undefined' ? 15 : 0);
        oldShippingAddress = newAddress;
      }
      var shippingMethod = model.getShippingMethod();
      var newMethod = shippingMethod ? Ti.Utils.sha256(JSON.stringify(shippingMethod.toJSON())) : null;
      if (newMethod != oldShippingMethod) {
        oldShippingMethod = newMethod;
        if (shippingMethod && shippingMethod.getName()) {
          $.shipping_type.setText(shippingMethod.getName());
        } else {
          $.shipping_type.setText(_L('No Value'));
        }
      }

      var isGift = model.getIsGift();
      $.giftMessage.init(isGift, model.getGiftMessage());

      $.email_address_label.setText(model.getCustomerEmail());
    }
  }






  function deinit() {
    logger.info('DEINIT called');
    $.giftMessage.deinit();

    $.stopListening();
    $.destroy();
  }





  $.shipping_address.transform = function () {
    var phone = $.shipping_address.getPhone();
    return {
      addressLabel: $.shipping_address.getAddressDisplay('shipping'),
      phone: phone && phone != 'undefined' ? phone : '' };

  };









  _.extend($, exports);
}

module.exports = Controller;