var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
		delete obj[key];
	}
	return arg;
}

function Controller() {

	require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'components/orders/giftMessage';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};







	$.__views.gift_message_container = Ti.UI.createView(
	{ layout: "horizontal", height: Ti.UI.SIZE, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 20, top: 5, visible: false, id: "gift_message_container" });

	$.__views.gift_message_container && $.addTopLevelView($.__views.gift_message_container);
	$.__views.gift_message_label = Ti.UI.createLabel(
	{ width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailLabelFont, textid: "_Gift_Message_", id: "gift_message_label", accessibilityValue: "gift_message_label" });

	$.__views.gift_message_container.add($.__views.gift_message_label);
	$.__views.gift_message = Ti.UI.createLabel(
	{ width: "50%", height: 20, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, ellipsize: true, font: Alloy.Styles.detailLabelFont, left: 5, id: "gift_message", accessibilityValue: "gift_message" });

	$.__views.gift_message_container.add($.__views.gift_message);
	exports.destroy = function () {};




	_.extend($, $.__views);










	exports.init = init;
	exports.deinit = deinit;











	function init(isGift, text) {
		if (isGift) {
			$.gift_message_container.show();
			if (text) {
				$.gift_message.setText(text);
			}
			$.gift_message_container.setHeight(Ti.UI.SIZE);
		} else {
			$.gift_message_container.hide();
			$.gift_message_container.setHeight(0);
		}
	}






	function deinit() {
		$.destroy();
	}









	_.extend($, exports);
}

module.exports = Controller;