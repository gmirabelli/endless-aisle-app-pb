var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'search/components/productGrid';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.productGrid = Ti.UI.createView(
  { layout: "absolute", backgroundColor: Alloy.Styles.color.background.white, id: "productGrid" });

  $.__views.productGrid && $.addTopLevelView($.__views.productGrid);
  $.__views.productGridContainer = Ti.UI.createView(
  { layout: "absolute", top: 0, id: "productGridContainer" });

  $.__views.productGrid.add($.__views.productGridContainer);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var args = arguments[0] || {};
  var logger = require('logging')('search:components:productGrid', getFullControllerPath($.__controllerPath));

  var $model = $model || args.product_search_result;
  var offset = args.offset || 0;
  var hasRendered = false;
  var allTiles = [];
  var layoutProperties = {
    aspect: {
      width: 217,
      height: 271 },


    spacing: {
      width: 31,
      height: 31 },


    min_padding: {
      width: 31,
      height: 31 },


    fixed_num_cols: {
      phone: {
        portrait: {
          width: 2,
          height: 2 },

        landscape: {
          width: 4,
          height: 1 } },


      tablet: {
        portrait: {
          width: 3,
          height: 3 },

        landscape: {
          width: 4,
          height: 2 } } },



    max_heights: {
      phone: {
        portrait: {
          width: 320,
          height: 314 },

        landscape: {
          width: 480,
          height: 154 } },


      tablet: {
        portrait: {
          width: 768,
          height: 858 },

        landscape: {
          width: 1024,
          height: 643 } } } };








  exports.deinit = deinit;









  function init() {
    logger.trace('init start ' + getPageNumber());
    var pageLoad = new _.Deferred();
    var promise = $model.fetch();

    promise.done(function () {
      pageLoad.resolve();
    });
    promise.fail(function (response, status, err) {
      logger.info('model fetch failed! \nmessage: ' + response.statusText + '\nerror: ' + err);
      pageLoad.reject();
    });

    logger.trace('init end ' + getPageNumber());
    return pageLoad.promise();
  }








  function render(options) {
    logger.trace('render start ' + getPageNumber());

    var hits = $model.getHits();
    var hitsLength = hits.length;
    if (hitsLength == 0) {
      var pageRender = new _.Deferred();
      var onModelChange = function () {
        $.stopListening($model, 'change', onModelChange);
        render({
          type: 'orient',
          portrait: false });

        pageRender.resolve();
      };
      $.listenTo($model, 'change', onModelChange);
      return pageRender.promise();
    }




    var aspect = layoutProperties.aspect,



    spacing = layoutProperties.spacing,



    min_padding = layoutProperties.min_padding,



    fixed_num_cols = layoutProperties.fixed_num_cols;


    var num_cols = Alloy.Platform == 'ipad' ? fixed_num_cols.tablet : fixed_num_cols.phone,
    row,
    col;
    num_cols = options.portrait ? num_cols.portrait.width : num_cols.landscape.width;


    var num_rows = Alloy.Platform == 'ipad' ? fixed_num_cols.tablet : fixed_num_cols.phone;
    num_rows = options.portrait ? num_rows.portrait.height : num_rows.landscape.height;


    var max_heights = layoutProperties.max_heights,
    my_max_heights = Alloy.Platform == 'ipad' ? max_heights.tablet : max_heights.phone;

    var parent_width = options.portrait ? my_max_heights.portrait.width : my_max_heights.landscape.width;
    var parent_height = options.portrait ? my_max_heights.portrait.height : my_max_heights.landscape.height;


    var total_width = num_cols * (aspect.width + spacing.width) - spacing.width;
    var total_height = num_rows * (aspect.height + spacing.height) - spacing.height;
    var bound_width = parent_width - 2 * min_padding.width;


    var scalar_width = 1,
    scalar_height = 1,
    scalar = 1,
    fit_height = true;

    if (total_width > bound_width) {
      scalar_width = bound_width / total_width;
    }
    var mapped_height = total_height * scalar_width;
    var bound_height = parent_height - 2 * min_padding.height;
    if (fit_height && mapped_height > bound_height) {
      scalar_height = bound_height / mapped_height;
      bound_width = bound_width * scalar_height;
    } else {

      bound_height = mapped_height;
    }
    scalar = scalar_height * scalar_width;
    var scaled_aspect = {
      width: aspect.width * scalar,
      height: aspect.height * scalar };


    var offsetX = (parent_width - bound_width) / 2;
    var offsetY = (parent_height - bound_height) / 2;

    var scroller_height = parent_height;
    if (bound_height > parent_height) {
      scroller_height = bound_height + 2 * min_padding.height;
      offsetY = min_padding.height;
    }

    var tileView,
    regionDef,
    hit,
    length,
    num_tiles = num_cols * num_rows;
    for (var i = 0, ii = hitsLength; i < ii; i++) {
      if (i > num_tiles - 1) {
        break;
      }

      length = hitsLength - offset;
      if (length > num_tiles) {
        length = num_tiles;
      }
      if (hitsLength >= num_tiles) {
        length = num_tiles;
      }

      hit = hits[i + offset];

      row = Math.floor(i / num_cols);
      col = i % num_cols;

      if (hit) {

        if ($.productGridContainer.children.length > i) {
          tileView = $.productGridContainer.children[i];
        } else {
          var tileController = Alloy.createController('search/components/productTile', {
            hit: hit,
            index: i });

          tileView = tileController.getView();
          allTiles.push(tileController);
        }


        tileView.left = offsetX + col * scalar * (aspect.width + spacing.width);
        tileView.top = offsetY + row * scalar * (aspect.height + spacing.height);




        if ($.productGridContainer.children.length <= i) {
          $.productGridContainer.add(tileView);
        }
      }
    }
    logger.trace('render end ' + getPageNumber());
    hasRendered = true;
  }






  function deinit() {
    logger.info('DEINIT called');
    $.productGrid.loadPage = null;
    $.productGrid.renderPage = null;
    _.each(allTiles, function (tileController) {
      tileController.deinit();
    });
    allTiles = [];
    $.stopListening();
    removeAllChildren($.productGridContainer);
    $.destroy();
  }









  function getPageNumber() {
    return Math.floor($model.getStart() / Alloy.CFG.product_search.search_products_returned);
  }







  function loadPage() {
    var hits = $model.getHits();
    if (!($model && hits && hits.length > 0)) {

      return init();
    }
  }







  function renderPage() {
    if (!hasRendered) {
      return render({
        type: 'orient',
        portrait: false });

    }
  }





  var hits = $model.getHits();
  if ($model && hits && hits.length > 0) {
    render({
      type: 'orient',
      portrait: false });

  }

  $.productGrid.loadPage = loadPage;

  $.productGrid.renderPage = renderPage;









  _.extend($, exports);
}

module.exports = Controller;