var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'customer/components/order';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};




  $.customerOrder = Alloy.createModel('baskets');$.productLineItems = Alloy.createCollection('productItem');


  $.__views.customer_order = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.color.background.transparent, zIndex: 101, id: "customer_order" });

  $.__views.customer_order && $.addTopLevelView($.__views.customer_order);
  $.__views.backdrop = Ti.UI.createView(
  { top: 0, left: 0, width: 1024, height: 768, opacity: 0.55, backgroundColor: Alloy.Styles.color.background.black, id: "backdrop" });

  $.__views.customer_order.add($.__views.backdrop);
  $.__views.customer_order_container = Ti.UI.createView(
  { width: 887, height: 697, left: 69, top: 47, backgroundColor: Alloy.Styles.color.background.white, id: "customer_order_container" });

  $.__views.customer_order.add($.__views.customer_order_container);
  $.__views.order_left_column = Ti.UI.createView(
  { layout: "vertical", width: 610, height: Ti.UI.FILL, left: 0, top: 0, id: "order_left_column" });

  $.__views.customer_order_container.add($.__views.order_left_column);
  $.__views.__alloyId160 = Ti.UI.createView(
  { backgroundImage: Alloy.Styles.upperLowerBorderImage, height: 40, width: "100%", backgroundColor: Alloy.Styles.color.background.medium, id: "__alloyId160" });

  $.__views.order_left_column.add($.__views.__alloyId160);
  $.__views.order_details_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.accentColor, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 12, top: 11, font: Alloy.Styles.smallAccentFont, textid: "_Order_Details", id: "order_details_label", accessibilityValue: "subheader" });

  $.__views.__alloyId160.add($.__views.order_details_label);
  $.__views.order_header = Alloy.createController('components/orders/orderHeader', { id: "order_header", __parentSymbol: $.__views.order_left_column });
  $.__views.order_header.setParent($.__views.order_left_column);
  $.__views.__alloyId161 = Ti.UI.createView(
  { width: "100%", backgroundColor: Alloy.Styles.color.background.dark, height: 1, left: 0, top: 10, id: "__alloyId161" });

  $.__views.order_left_column.add($.__views.__alloyId161);
  $.__views.order_product_details = Alloy.createController('components/orders/orderProductDetails', { id: "order_product_details", __parentSymbol: $.__views.order_left_column });
  $.__views.order_product_details.setParent($.__views.order_left_column);
  $.__views.vertical_separator = Ti.UI.createView(
  { width: 1, backgroundColor: Alloy.Styles.color.background.dark, height: "100%", top: 0, right: 276, id: "vertical_separator" });

  $.__views.customer_order_container.add($.__views.vertical_separator);
  $.__views.order_right_column = Ti.UI.createView(
  { layout: "vertical", backgroundColor: Alloy.Styles.color.background.white, width: 275, right: 0, height: Ti.UI.SIZE, borderWidth: 0, id: "order_right_column" });

  $.__views.customer_order_container.add($.__views.order_right_column);
  $.__views.subheader = Ti.UI.createLabel(
  { width: 340, height: 40, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, backgroundColor: Alloy.Styles.color.background.medium, font: Alloy.Styles.detailLabelFont, borderWidth: 1, borderColor: Alloy.Styles.color.border.medium, textid: "_Order_Summary", id: "subheader" });

  $.__views.order_right_column.add($.__views.subheader);
  $.__views.order_summary = Alloy.createController('checkout/components/orderTotalSummary', { id: "order_summary", __parentSymbol: $.__views.order_right_column });
  $.__views.order_summary.setParent($.__views.order_right_column);
  $.__views.shipping_details = Alloy.createController('checkout/components/shippingSummary', { id: "shipping_details", __parentSymbol: $.__views.order_right_column });
  $.__views.shipping_details.setParent($.__views.order_right_column);
  $.__views.payment_details_view = Alloy.createController('checkout/components/paymentSummary', { id: "payment_details_view", __parentSymbol: $.__views.order_right_column });
  $.__views.payment_details_view.setParent($.__views.order_right_column);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var toCurrency = require('EAUtils').toCurrency;
  var logger = require('logging')('customer:components:order', getFullControllerPath($.__controllerPath));
  var args = arguments[0] || $.args || {};




  $.customer_order.addEventListener('click', customerOrderClickEventHandler);

  $.backdrop.addEventListener('click', dismiss);




  exports.init = init;
  exports.deinit = deinit;










  function init() {
    logger.info('Calling INIT');
    var order = Alloy.Models.customerOrder;
    var deferred = new _.Deferred();
    $.order_header.render(order, false);


    $.shipping_details.render(order);


    $.payment_details_view.render(order);

    $.order_summary.render(order.toJSON(), false);

    $.order_product_details.render(order).always(function () {
      deferred.resolve();
    });
    return deferred.promise();
  }






  function deinit() {
    logger.info('DEINIT called');
    $.backdrop.removeEventListener('click', dismiss);
    $.customer_order.removeEventListener('click', customerOrderClickEventHandler);
    $.order_product_details.deinit();
    $.order_header.deinit();
    $.order_summary.deinit();
    $.shipping_details.deinit();
    $.payment_details_view.deinit();
    $.stopListening();
    $.destroy();
  }








  function customerOrderClickEventHandler(event) {
    logger.info('customer_order click event listener');
    event.cancelBubble = true;
    dismiss();
  }








  function dismiss() {
    logger.info('dimissing order history');
    $.trigger('order_history:dismiss');
  }









  _.extend($, exports);
}

module.exports = Controller;