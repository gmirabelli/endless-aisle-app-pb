var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'customerSearch/index';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};




  $.customers = Alloy.createCollection('customer');


  $.__views.searchResults = Ti.UI.createView(
  { top: 55, visible: false, id: "searchResults", layout: "vertical" });

  $.__views.searchResults && $.addTopLevelView($.__views.searchResults);
  $.__views.__alloyId194 = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.color.background.medium, width: "100%", height: 44, layout: "horizontal", id: "__alloyId194" });

  $.__views.searchResults.add($.__views.__alloyId194);
  $.__views.breadcrumbs_back_button = Ti.UI.createButton(
  { image: Alloy.Styles.backButtonImage, width: 44, height: 44, style: Ti.UI.iOS.SystemButtonStyle.PLAIN, borderWidth: 1, borderColor: Alloy.Styles.color.border.medium, id: "breadcrumbs_back_button", accessibilityValue: "breadcrumbs_back_button" });

  $.__views.__alloyId194.add($.__views.breadcrumbs_back_button);
  $.__views.__alloyId195 = Ti.UI.createView(
  { width: 1, backgroundColor: Alloy.Styles.color.background.dark, height: "100%", top: 0, left: 0, id: "__alloyId195" });

  $.__views.__alloyId194.add($.__views.__alloyId195);
  $.__views.search_results_count = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 20, font: Alloy.Styles.detailLabelFont, id: "search_results_count", accessibilityValue: "search_results_count" });

  $.__views.__alloyId194.add($.__views.search_results_count);
  $.__views.padding = Ti.UI.createView(
  { id: "padding", height: 5 });

  $.__views.searchResults.add($.__views.padding);
  $.__views.results_contents = Ti.UI.createView(
  { layout: "vertical", height: Ti.UI.SIZE, visible: false, id: "results_contents" });

  $.__views.searchResults.add($.__views.results_contents);
  $.__views.lookup_window = Ti.UI.createView(
  { layout: "vertical", width: 655, height: 170, visible: false, id: "lookup_window" });

  $.__views.results_contents.add($.__views.lookup_window);
  $.__views.customer_lookup_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, top: 30, font: Alloy.Styles.headlineFont, textid: "_Customer_Search_Label", id: "customer_lookup_label", accessibilityValue: "customer_lookup_label" });

  $.__views.lookup_window.add($.__views.customer_lookup_label);
  $.__views.search_form_container = Ti.UI.createView(
  { layout: "horizontal", height: 57, width: 653, visible: false, id: "search_form_container" });

  $.__views.lookup_window.add($.__views.search_form_container);
  $.__views.search_textfield = Ti.UI.createTextField(
  { borderColor: Alloy.Styles.color.border.dark, borderWidth: 1, color: Alloy.Styles.color.text.dark, left: 0, top: 0, bottom: 10, width: 455, font: Alloy.Styles.textFieldFont, padding: { left: 18 }, height: 55, autocorrect: false, clearButtonMode: Ti.UI.INPUT_BUTTONMODE_ALWAYS, keyboardType: Ti.UI.KEYBOARD_TYPE_EMAIL, autocapitalization: Ti.UI.TEXT_AUTOCAPITALIZATION_NONE, returnKeyType: Ti.UI.RETURNKEY_SEARCH, hintText: L('_Enter_First_and_Last_Name_or_Email'), id: "search_textfield", accessibilityLabel: "search_textfield" });

  $.__views.search_form_container.add($.__views.search_textfield);
  $.__views.padding = Ti.UI.createView(
  { id: "padding", width: 18 });

  $.__views.search_form_container.add($.__views.padding);
  $.__views.search_button = Ti.UI.createButton(
  { width: 156, right: 0, top: 0, height: 55, backgroundImage: Alloy.Styles.primaryButtonImage, font: Alloy.Styles.bigButtonFont, color: Alloy.Styles.buttons.primary.color, id: "search_button", titleid: "_Search", accessibilityValue: "search_customer_button" });

  $.__views.search_form_container.add($.__views.search_button);
  $.__views.search_results_container = Ti.UI.createView(
  { width: "100%", id: "search_results_container", visible: false });

  $.__views.lookup_window.add($.__views.search_results_container);
  $.__views.results_error = Ti.UI.createLabel(
  { width: "100%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.red, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.headlineFont, id: "results_error", accessibilityValue: "results_error" });

  $.__views.search_results_container.add($.__views.results_error);
  $.__views.__alloyId196 = Ti.UI.createView(
  { backgroundColor: "#eeeeee", width: "100%", height: 1, top: 0, id: "__alloyId196" });

  $.__views.results_contents.add($.__views.__alloyId196);
  $.__views.results_table_container = Ti.UI.createView(
  { layout: "vertical", height: 475, width: "100%", top: 0, id: "results_table_container" });

  $.__views.results_contents.add($.__views.results_table_container);
  $.__views.results_container = Ti.UI.createTableView(
  { separatorStyle: "transparent", layout: "vertical", id: "results_container" });

  $.__views.results_table_container.add($.__views.results_container);
  var __alloyId200 = Alloy.Collections['$.customers'] || $.customers;function __alloyId201(e) {if (e && e.fromAdapter) {return;}var opts = __alloyId201.opts || {};var models = __alloyId200.models;var len = models.length;var rows = [];for (var i = 0; i < len; i++) {var __alloyId197 = models[i];__alloyId197.__transform = transformCustomer(__alloyId197);var __alloyId199 = Alloy.createController('customer/components/customerResultRow', { $model: __alloyId197 });
      rows.push(__alloyId199.getViewEx({ recurse: true }));
    }$.__views.results_container.setData(rows);};__alloyId200.on('fetch destroy change add remove reset', __alloyId201);exports.destroy = function () {__alloyId200 && __alloyId200.off('fetch destroy change add remove reset', __alloyId201);};




  _.extend($, $.__views);










  var currentCustomers = Alloy.Collections.customer;
  var currentBasket = Alloy.Models.basket;
  var currentCustomer = Alloy.Models.customer;

  var returnToShoppingOrCart = require('EAUtils').returnToShoppingOrCart;
  var analytics = require('analyticsBase');
  var logger = require('logging')('customerSearch:index', getFullControllerPath($.__controllerPath));




  $.searchResults.addEventListener('postlayout', onResultsPostLayout);

  $.results_container.addEventListener('click', onResultContainerClick);

  $.breadcrumbs_back_button.addEventListener('singletap', returnToShoppingOrCart);

  $.search_textfield.addEventListener('return', handlePerformSearch);

  $.search_button.addEventListener('click', handlePerformSearch);


  $.listenTo(Alloy.eventDispatcher, 'hideAuxillaryViews', hideKeyboard);




  $.listenTo(currentCustomers, 'sync', render);
  $.listenTo(currentCustomers, 'reset:customers', initCustomersCollection);
  $.listenTo(currentCustomer, 'saved_products:downloaded', onSavedProducts);




  exports.init = init;
  exports.render = render;
  exports.deinit = deinit;











  function init(options) {
    logger.info('Calling INIT');
    $.results_error.setText('');
    $.search_results_container.setVisible(false);
    $.search_textfield.setValue(options.customer_query || '');
    if (options.customer_query) {
      return handlePerformSearch();
    } else {
      var deferred = new _.Deferred();
      deferred.resolve();
      return deferred.promise();
    }
  }






  function deinit() {
    logger.info('DEINIT called');
    $.searchResults.removeEventListener('postlayout', onResultsPostLayout);
    $.breadcrumbs_back_button.removeEventListener('singletap', returnToShoppingOrCart);
    $.search_textfield.removeEventListener('return', handlePerformSearch);
    $.search_button.removeEventListener('click', handlePerformSearch);
    $.results_container.removeEventListener('click', onResultContainerClick);
    $.stopListening();
    $.destroy();
  }






  function render() {
    logger.info('Calling RENDER');
    var customers = currentCustomers.getCustomers();
    if (customers.length == 0) {
      renderError();
      return;
    }
  }











  function sendSearchRequest(searchText, searchParams, deferred) {
    Alloy.Collections.customer.search({
      attrs: searchParams }).
    done(function () {
      handleCustomerSearchDone(deferred, searchText);
    }).fail(function (error) {
      renderError(deferred, error);
    });
  }








  function handleCustomerSearchDone(deferred, searchText) {
    renderResults();
    if (Alloy.Collections.customer.length > 0) {
      deferred.resolve();
    } else {
      deferred.reject({
        message: String.format(_L('Could not find any customers matching \'%s\'.'), searchText) });

    }
  }





  function renderResults() {
    logger.info('RENDER RESULTS');
    logger.secureLog(JSON.stringify(currentCustomers));
    var customers = currentCustomers.getCustomers();
    if (customers.length == 0) {
      renderError();
      return;
    }

    initCustomersCollection();

    $.results_error.setText('');
    $.search_results_container.setVisible(false);
    $.search_textfield.blur();
  }







  function renderError(deferred, error) {
    logger.info('RENDER ERROR');
    initCustomersCollection();
    var search_text = $.search_textfield.value || currentCustomers.search_text;
    var errorMessage = String.format(_L('Could not find any customers matching \'%s\'.'), search_text);
    $.results_error.setText(errorMessage);

    if (deferred && _.isFunction(deferred.reject)) {
      if (error) {
        var message = error.description ? error.description : error.message;
        $.results_error.setText(message);
        deferred.reject({
          message: message });

      } else {
        deferred.reject({
          message: errorMessage });

      }
    }
    $.search_results_container.setVisible(true);
  }






  function transformCustomer(customer) {
    logger.info('transformCustomer: ' + customer);
    var name = customer.getFirstName() + ' ' + customer.getLastName();
    var hasAnAddress = customer.getAddresses() && customer.getAddresses().length > 0;
    if (hasAnAddress) {
      var address = customer.getAddresses()[0];
      var address1 = address && address.address1 && address.address1 != 'null' ? address.address1 : '';
      var address2 = address && address.address2 && address.address2 != 'null' ? address.address2 : '';
      var city_state_zip = address && address.city && address.city != 'null' ? address.city : '';
      city_state_zip += address && address.state_code && address.state_code != 'null' ? ', ' + address.state_code : '';
      city_state_zip += address && address.postal_code && address.postal_code != 'null' ? ' ' + address.postal_code : '';
      var country = address && address.country_code && address.country_code != 'null' ? address.country_code : '';
    }
    return {
      name: name,
      address1: address1,
      address2: address2,
      city_state_zip: city_state_zip,
      country: country,
      email: customer.getEmail(),
      phone: customer.getPhone(),
      login: customer.getLogin() };

  }





  function hideKeyboard() {
    $.search_textfield.blur();
  }








  function profileButtonClickHandler(login_info) {
    logger.info('profileButtonClickHandler');
    var promise = currentCustomer.loginCustomer({
      login: login_info },
    currentBasket);
    Alloy.Router.showActivityIndicator(promise);
    promise.done(function () {
      logger.info('redirecting to customer page');
      Alloy.Router.navigateToCustomer();
    }).fail(function (error) {
      notify(error && error.responseText ? error.responseText : String.format(_L('Could not get \'%s\' profile information'), login_info), {
        preventAutoClose: true });

    });
  }





  function loginButtonClickHandler(login_info) {
    logger.info('loginButtonClickHandler');
    var promise = currentCustomer.loginCustomer({
      login: login_info },
    currentBasket);
    Alloy.Router.showActivityIndicator(promise);
    promise.done(function () {
      returnToShoppingOrCart();
      analytics.fireAnalyticsEvent({
        category: _L('Users'),
        action: _L('Customer Login') });

    }).fail(function (error) {
      notify(error && error.responseText ? error.responseText : String.format(_L('Could not get \'%s\' profile information'), login_info), {
        preventAutoClose: true });

    });
  }





  function onResultsPostLayout() {
    $.searchResults.setVisible(true);
    $.results_contents.setVisible(true);
    $.lookup_window.setVisible(true);
    $.search_form_container.setVisible(true);
  }






  function onResultContainerClick(event) {
    logger.info('results_container click handler');
    var unsorted_customers = currentCustomers.getCustomers();
    var customers = _.sortBy(_.sortBy(unsorted_customers, 'first_name'), 'last_name');

    var customer = customers[event.index];
    if (event.source.id === 'profile_button') {
      event.source.animate(Alloy.Animations.bounce);
      confirmCurrentCustomerLogout(profileButtonClickHandler, customer.getLogin());
    } else if (event.source.id === 'login_button') {
      event.source.animate(Alloy.Animations.bounce);
      confirmCurrentCustomerLogout(loginButtonClickHandler, customer.getLogin());
    }
  }








  function confirmCurrentCustomerLogout(callback, loginInfo) {
    if (currentCustomer.getLogin() == loginInfo) {
      Alloy.Dialog.showAlertDialog({
        messageString: String.format(_L('\'%s\' is already logged in.'), currentCustomer.getFullName()) });

      return;
    }
    if (currentCustomer.isLoggedIn()) {
      Alloy.Dialog.showConfirmationDialog({
        messageString: _L('By tapping on "Continue", the cart will be cleared and you will logout the current customer you are logged in as.'),
        titleString: _L('Confirm Current Customer Logout'),
        okButtonString: _L('Continue'),
        okFunction: function () {
          Alloy.Router.customerLogout({
            noHomeScreenNavigation: true }).
          done(function () {
            callback(loginInfo);
          });
        } });

    } else {
      callback(loginInfo);
    }
  }







  function handlePerformSearch() {
    logger.info('handlePerformSearch');
    var search_text = $.search_textfield.value.trim();
    var searchParams = {};
    if (!search_text) {
      $.results_error.setText(_L('You must provide either a first/last name or an email for a customer search!'));
      $.search_results_container.setVisible(true);
      return;
    }
    var deferred = new _.Deferred();
    Alloy.Router.showActivityIndicator(deferred);
    var customers = Alloy.Collections.customer;
    var isEmail = search_text.indexOf('@') >= 0;

    analytics.fireAnalyticsEvent({
      category: _L('Search'),
      action: _L('Customer Search'),
      label: search_text });


    if (isEmail || search_text == '*') {
      searchParams = {
        email: search_text,
        firstname: '',
        lastname: '' };

      sendSearchRequest(search_text, searchParams, deferred);
    } else {

      var spacePos = search_text.indexOf(' ');

      if (spacePos < 0) {
        searchParams = {
          email: '',
          firstname: '*',
          lastname: search_text };

        sendSearchRequest(search_text, searchParams, deferred);
      } else {
        searchParams = {
          firstname: search_text.substring(0, spacePos),
          lastname: search_text.substring(spacePos + 1),
          email: '' };

        sendSearchRequest(search_text, searchParams, deferred);
      }
    }
    return deferred.promise();
  }








  function onSavedProducts() {
    logger.info('Responding to currentBasket saved_products:downloaded');
    var saved_products = currentCustomer.getSavedProducts();
    if (saved_products && saved_products.length > 0) {
      var sum = 0;
      sum = _.reduce(saved_products, function (memo, product) {
        return memo + product.getQuantity();
      }, 0);
      if (sum == 1) {
        notify(_L('1 saved product retrieved from your cart'));
      } else {
        notify(String.format(_L('%d saved products retrieved from your cart'), sum));
      }
    }
  }






  function initCustomersCollection() {
    logger.info('initCustomersCollection');
    var unsorted_customers = currentCustomers.getCustomers();
    var customers = _.sortBy(_.sortBy(unsorted_customers, 'first_name'), 'last_name');

    if (customers && customers.length > 0) {
      logger.info('Calling reset with data');
      $.search_results_count.setText(String.format(customers.length == 1 ? _L('%d Customer') : _L('%d Customers'), customers.length));

      $.customers.once('reset', function () {
        var children = $.results_container.getSections()[0].getRows();
        var count = 0;
        _.each(children, function (child, index) {
          child.getChildren()[0].setBackgroundColor(index % 2 == 0 ? Alloy.Styles.color.background.white : Alloy.Styles.color.background.light);
        });
        $.results_container.scrollToTop();
      });
      $.customers.reset(customers);
    } else {
      logger.info('Calling reset with no data');
      $.search_results_count.setText(String.format(_L('%d Customers'), 0));
      $.customers.reset([]);
    }
  }









  _.extend($, exports);
}

module.exports = Controller;