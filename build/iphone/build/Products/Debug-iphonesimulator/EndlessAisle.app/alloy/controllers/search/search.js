var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'search/search';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.search = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.color.background.transparent, id: "search" });

  $.__views.search && $.addTopLevelView($.__views.search);
  $.__views.backdrop = Ti.UI.createView(
  { top: 0, left: 0, width: 1024, height: 768, backgroundColor: Alloy.Styles.color.background.transparentBlack30Percent, id: "backdrop" });

  $.__views.search.add($.__views.backdrop);
  $.__views.contents = Ti.UI.createView(
  { left: 94, right: 94, top: 76, layout: "vertical", height: 270, backgroundColor: Alloy.Styles.color.background.white, id: "contents" });

  $.__views.search.add($.__views.contents);
  $.__views.landing = Ti.UI.createView(
  { top: 20, layout: "vertical", height: Ti.UI.SIZE, id: "landing" });

  $.__views.contents.add($.__views.landing);
  $.__views.search_tab = Ti.UI.createView(
  { height: 65, borderColor: Alloy.Styles.color.border.searchViewGray, borderWidth: 1, width: "94%", left: "3%", layout: "horizontal", id: "search_tab" });

  $.__views.landing.add($.__views.search_tab);
  $.__views.__alloyId247 = Ti.UI.createView(
  { width: "24.9%", backgroundColor: Alloy.Styles.accentColor, id: "__alloyId247" });

  $.__views.search_tab.add($.__views.__alloyId247);
  $.__views.product_search = Ti.UI.createButton(
  { color: Alloy.Styles.color.text.searchViewGray, font: Alloy.Styles.unselectedTabFont, height: "100%", width: "100%", top: 0, backgroundColor: Alloy.Styles.color.background.white, titleid: "_Products_Tab", id: "product_search", accessibilityValue: "product_search" });

  $.__views.__alloyId247.add($.__views.product_search);
  handleTabSelect ? $.addListener($.__views.product_search, 'click', handleTabSelect) : __defers['$.__views.product_search!click!handleTabSelect'] = true;if (!isKioskMode()) {
    $.__views.__alloyId248 = Ti.UI.createView(
    { width: "24.9%", backgroundColor: Alloy.Styles.accentColor, id: "__alloyId248" });

    $.__views.search_tab.add($.__views.__alloyId248);
    $.__views.customer_search = Ti.UI.createButton(
    { color: Alloy.Styles.color.text.searchViewGray, font: Alloy.Styles.unselectedTabFont, height: "100%", width: "100%", top: 0, backgroundColor: Alloy.Styles.color.background.white, titleid: "_Customers_Tab", id: "customer_search", accessibilityValue: "customer_search" });

    $.__views.__alloyId248.add($.__views.customer_search);
    handleTabSelect ? $.addListener($.__views.customer_search, 'click', handleTabSelect) : __defers['$.__views.customer_search!click!handleTabSelect'] = true;}
  if (allowOrderSearchTab()) {
    $.__views.__alloyId249 = Ti.UI.createView(
    { width: "24.9%", backgroundColor: Alloy.Styles.accentColor, id: "__alloyId249" });

    $.__views.search_tab.add($.__views.__alloyId249);
    $.__views.order_search = Ti.UI.createButton(
    { color: Alloy.Styles.color.text.searchViewGray, font: Alloy.Styles.unselectedTabFont, height: "100%", width: "100%", top: 0, backgroundColor: Alloy.Styles.color.background.white, titleid: "_Orders_Tab", id: "order_search", accessibilityValue: "order_search" });

    $.__views.__alloyId249.add($.__views.order_search);
    handleTabSelect ? $.addListener($.__views.order_search, 'click', handleTabSelect) : __defers['$.__views.order_search!click!handleTabSelect'] = true;}
  $.__views.text_field_wrapper = Ti.UI.createView(
  { layout: "horizontal", width: "94%", height: 64, top: -1, left: "3%", borderColor: Alloy.Styles.color.border.searchViewGray, id: "text_field_wrapper" });

  $.__views.landing.add($.__views.text_field_wrapper);
  $.__views.search_field = Ti.UI.createTextField(
  { backgroundImage: Alloy.Styles.longMagnifierImage, backgroundColor: Alloy.Styles.color.background.white, color: Alloy.Styles.color.text.dark, left: 0, width: "100%", font: Alloy.Styles.dialogFieldFont, maxLength: 50, padding: { left: 90 }, returnKeyType: Ti.UI.RETURNKEY_SEARCH, clearButtonMode: Ti.UI.INPUT_BUTTONMODE_ALWAYS, keyboardType: Ti.UI.KEYBOARD_TYPE_EMAIL, autocapitalization: Ti.UI.TEXT_AUTOCAPITALIZATION_NONE, autocorrect: false, height: 64, hintText: L('_Enter_Search_Term_or_UPC_Code'), id: "search_field", accessibilityLabel: "search_field" });

  $.__views.text_field_wrapper.add($.__views.search_field);
  $.__views.search_msg_button_wrapper = Ti.UI.createView(
  { id: "search_msg_button_wrapper" });

  $.__views.contents.add($.__views.search_msg_button_wrapper);
  $.__views.search_button = Ti.UI.createButton(
  { width: "20%", right: "3%", top: 25, height: 55, backgroundImage: Alloy.Styles.primaryButtonImage, color: Alloy.Styles.buttons.primary.color, font: Alloy.Styles.bigButtonFont, titleid: "_Search", id: "search_button", accessibilityValue: "search_button" });

  $.__views.search_msg_button_wrapper.add($.__views.search_button);
  $.__views.message = Ti.UI.createLabel(
  { width: "70%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.red, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: "3%", top: 25, font: Alloy.Styles.calloutFontMedium, id: "message", accessibilityValue: "search_results_error" });

  $.__views.search_msg_button_wrapper.add($.__views.message);
  exports.destroy = function () {};




  _.extend($, $.__views);











  var args = arguments[0] || $.args || {};
  var logger = require('logging')('search:search', getFullControllerPath($.__controllerPath));
  var selectedTabId;
  var currentCustomer = Alloy.Models.customer;
  var customerAddress = Alloy.Models.customerAddress;
  var emptyTextFieldError = _L('You must provide either a search term or UPC code for a product search.');

  var selectedStyle = {
    color: Alloy.Styles.color.text.black,
    font: Alloy.Styles.tabFont,
    height: '88%' };


  var unSelectedStyle = {
    color: Alloy.Styles.color.text.searchViewGray,
    font: Alloy.Styles.unselectedTabFont,
    height: '100%' };






  $.search_button.addEventListener('click', handlePerformSearch);
  $.search_field.addEventListener('return', handlePerformSearch);


  $.backdrop.addEventListener('click', dismiss);




  exports.init = init;
  exports.deinit = deinit;
  exports.dismiss = dismiss;
  exports.setMessage = setMessage;
  exports.focusSearchField = focusSearchField;










  function init(args) {
    logger.info('Calling INIT');
    if (args && args.message) {
      $.message.setText(args.message);
    }
    if (args && args.query) {
      $.search_field.setValue(args.query);
    }
  }






  function deinit() {
    logger.info('Calling DEINIT');
    $.search_button.removeEventListener('click', handlePerformSearch);
    $.search_field.removeEventListener('return', handlePerformSearch);
    $.backdrop.removeEventListener('click', dismiss);

    if (Alloy.CFG.enable_barcode_scanner) {
      $.bar_code_scanner.removeEventListener('singletap', handleScannerClick);
    }

    if ($.product_search) {
      $.product_search.removeEventListener('click', handleTabSelect);
    }

    if ($.customer_search) {
      $.customer_search.removeEventListener('click', handleTabSelect);
    }

    if ($.order_search) {
      $.order_search.removeEventListener('click', handleTabSelect);
    }

    $.stopListening();
    $.destroy();
  }









  function focusSearchField() {
    $.search_field.focus();
  }






  function setSearchFieldHintText(tabId) {
    switch (tabId) {
      case 'product_search':
        $.search_field.setHintText(_L('Enter Search Term or UPC Code'));
        emptyTextFieldError = _L('You must provide either a search term or UPC code for a product search.');
        break;
      case 'customer_search':
        $.search_field.setHintText(_L('Enter First and Last Name or Email'));
        emptyTextFieldError = _L('You must provide either a first/last name or an email for a customer search.');
        break;
      case 'order_search':
        $.search_field.setHintText(_L('Enter Order ID or Customer Email'));
        emptyTextFieldError = _L('You must provide either an order ID or a customer email for an order search.');
        break;}

  }






  function hideShowScannerButton(tabId) {
    if (Alloy.CFG.enable_barcode_scanner) {
      if (tabId == 'product_search') {
        $.search_field.setWidth('85%');
        $.search_field.setBackgroundImage(Alloy.Styles.magnifierImage);
        $.bar_code_scanner.setWidth('10.9%');
        $.bar_code_scanner.show();
        $.bar_code_scanner.setTouchEnabled(true);
      } else {
        $.bar_code_scanner.hide();
        $.search_field.setBackgroundImage(Alloy.Styles.longMagnifierImage);
        $.bar_code_scanner.setTouchEnabled(false);
        $.bar_code_scanner.setWidth(0);
        $.search_field.setWidth('100%');
      }
    }
  }






  function setMessage(message) {
    $.message.setText(message);
  }






  function allowOrderSearchTab() {
    return !isKioskMode() || isKioskManagerLoggedIn();
  }








  function handlePerformSearch() {
    var search_phrase = $.search_field.getValue();

    if (search_phrase == '') {
      setMessage(emptyTextFieldError);
      return;
    }
    setMessage('');

    $.search_field.blur();

    switch (selectedTabId) {
      case 'product_search':
        Alloy.Router.navigateToProductSearch({
          query: search_phrase,
          category_id: Alloy.CFG.root_category_id });


        break;

      case 'customer_search':
        Alloy.Router.navigateToCustomerSearchResult({
          customer_query: search_phrase });


        break;

      case 'order_search':
        Alloy.Router.navigateToOrderSearchResult({
          searchPhrase: search_phrase });


        break;}

  }






  function handleTabSelect(evt) {
    _.each($.search_tab.getChildren(), function (view) {
      var children = view.getChildren();
      if (children && children.length > 0) {
        if (children[0] !== evt.source) {
          _.extend(children[0], unSelectedStyle);
        }
      }
    });
    _.extend(evt.source, selectedStyle);
    selectedTabId = evt.source.id;
    setSearchFieldHintText(selectedTabId);
    hideShowScannerButton(selectedTabId);
    setMessage('');
    $.search_field.setValue('');
  }






  function handleScannerClick() {
    Alloy.eventDispatcher.trigger('barcode:start_camera');
  }





  function dismiss() {
    $.search_field.blur();
    $.trigger('search:dismiss');
  }




  if (Alloy.CFG.enable_barcode_scanner) {
    $.bar_code_scanner = Ti.UI.createButton($.createStyle({
      classes: ['bar_code_scanner'],
      apiName: 'Button' }));

    $.bar_code_scanner.addEventListener('singletap', handleScannerClick);
    $.text_field_wrapper.add($.bar_code_scanner);
  }

  if (args.tabId) {
    _.extend($[args.tabId], selectedStyle);
    selectedTabId = args.tabId || 'product_search';
    setSearchFieldHintText(selectedTabId);
    hideShowScannerButton(selectedTabId);
  }





  __defers['$.__views.product_search!click!handleTabSelect'] && $.addListener($.__views.product_search, 'click', handleTabSelect);if (!isKioskMode()) {
    __defers['$.__views.customer_search!click!handleTabSelect'] && $.addListener($.__views.customer_search, 'click', handleTabSelect);}
  if (allowOrderSearchTab()) {
    __defers['$.__views.order_search!click!handleTabSelect'] && $.addListener($.__views.order_search, 'click', handleTabSelect);}




  _.extend($, exports);
}

module.exports = Controller;