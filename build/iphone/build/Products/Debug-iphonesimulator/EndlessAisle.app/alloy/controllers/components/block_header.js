var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
		delete obj[key];
	}
	return arg;
}

function Controller() {

	require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'components/block_header';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};







	$.__views.block_header = Ti.UI.createLabel(
	{ width: "100%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.headlineFont, top: 10, id: "block_header", accessibilityValue: "block_header" });

	$.__views.block_header && $.addTopLevelView($.__views.block_header);
	exports.destroy = function () {};




	_.extend($, $.__views);










	var args = arguments[0] || {};
	var customLabel = args.customLabel || {};




	exports.setCustomLabel = setCustomLabel;











	function setCustomLabel(label) {
		$.block_header.setText(label);
	}



	$.block_header.setText(customLabel);









	_.extend($, exports);
}

module.exports = Controller;