var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'appIndex';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};




  Alloy.Models.instance('customer');Alloy.Models.instance('associate');Alloy.Models.instance('baskets');Alloy.Models.instance('category');Alloy.Models.instance('product');Alloy.Models.instance('productSearch');


  $.__views.appIndex = Ti.UI.createWindow(
  { tabBarHidden: 1, navBarHidden: 1, backgroundColor: Alloy.Styles.color.background.white, statusBarStyle: Ti.UI.iOS.StatusBar.LIGHT_CONTENT, top: 0, left: 0, width: Ti.UI.FILL, height: Ti.UI.FILL, id: "appIndex" });

  $.__views.appIndex && $.addTopLevelView($.__views.appIndex);
  $.__views.header = Alloy.createController('components/header', { id: "header", __parentSymbol: $.__views.appIndex });
  $.__views.header.setParent($.__views.appIndex);
  $.__views.hamburger_menu = Alloy.createController('components/hamburgerMenu', { id: "hamburger_menu", __parentSymbol: $.__views.appIndex });
  $.__views.hamburger_menu.setParent($.__views.appIndex);
  $.__views.product_search_tab = Ti.UI.createView(
  { top: 20, zIndex: 0, id: "product_search_tab" });

  $.__views.appIndex.add($.__views.product_search_tab);
  $.__views.product_search = Alloy.createController('search/index', { id: "product_search", __parentSymbol: $.__views.product_search_tab });
  $.__views.product_search.setParent($.__views.product_search_tab);
  $.__views.product_detail_tab = Ti.UI.createView(
  { top: 20, zIndex: 0, id: "product_detail_tab", visible: false });

  $.__views.appIndex.add($.__views.product_detail_tab);
  $.__views.product_index = Alloy.createController('product/index', { id: "product_index", __parentSymbol: $.__views.product_detail_tab });
  $.__views.product_index.setParent($.__views.product_detail_tab);
  $.__views.customer_search_tab = Ti.UI.createView(
  { top: 20, zIndex: 0, id: "customer_search_tab", visible: false });

  $.__views.appIndex.add($.__views.customer_search_tab);
  $.__views.customer_results = Alloy.createController('customerSearch/index', { id: "customer_results", __parentSymbol: $.__views.customer_search_tab });
  $.__views.customer_results.setParent($.__views.customer_search_tab);
  $.__views.customer_tab = Ti.UI.createView(
  { top: 20, zIndex: 0, id: "customer_tab", visible: false });

  $.__views.appIndex.add($.__views.customer_tab);
  $.__views.customer_tabset = Alloy.createController('customer/index', { id: "customer_tabset", __parentSymbol: $.__views.customer_tab });
  $.__views.customer_tabset.setParent($.__views.customer_tab);
  $.__views.cart_tab = Ti.UI.createView(
  { top: 20, zIndex: 0, id: "cart_tab", visible: false });

  $.__views.appIndex.add($.__views.cart_tab);
  $.__views.cart = Alloy.createController('checkout/index', { id: "cart", __parentSymbol: $.__views.cart_tab });
  $.__views.cart.setParent($.__views.cart_tab);
  $.__views.order_tab = Ti.UI.createView(
  { top: 20, zIndex: 0, id: "order_tab", visible: false });

  $.__views.appIndex.add($.__views.order_tab);
  $.__views.order_results = Alloy.createController('customer/components/orderSearchResult', { id: "order_results", __parentSymbol: $.__views.order_tab });
  $.__views.order_results.setParent($.__views.order_tab);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var analytics = require('analyticsBase');
  var paymentTerminal = require(Alloy.CFG.devices.payment_terminal_module);
  var animation = require('alloy/animation');
  var logger = require('logging')('application:appIndex', getFullControllerPath($.__controllerPath));

  var showActivityIndicator = require('dialogUtils').showActivityIndicator;
  var customerAddress = Alloy.Models.customerAddress;
  var currentBasket = Alloy.Models.basket;
  var newCustomer = Alloy.Models.customer;
  var currentAssociate = Alloy.Models.associate;
  var eaUtils = require('EAUtils');
  var storePasswordHelpers = require('storePasswordHelpers');
  var paymentDeviceDialogInterval = null;
  var paymentDeviceConnected = false;
  var addPaymentDeviceTimer = true;
  var searchDialog = null;

  var tabs = {
    product_search_tab: $.product_search_tab,
    product_detail_tab: $.product_detail_tab,
    customer_search_tab: $.customer_search_tab,
    customer_tab: $.customer_tab,
    cart_tab: $.cart_tab,
    order_tab: $.order_tab };





  $.listenTo(Alloy.eventDispatcher, 'hideAuxillaryViews', Alloy.Dialog.dismissDialog);
  $.listenTo(Alloy.eventDispatcher, 'app:ready', onAppReady);




  exports.deinit = deinit;
  exports.init = init;
  exports.viewManager = new ViewManager();
  exports.router = new Router();









  function init() {
    logger.info('init called');
    $.header.init();
    $.cart.init();
  }






  function deinit() {
    logger.info('DEINIT called');

    Alloy.Models.product.clear({
      silent: true });

    Alloy.Models.productSearch.clear({
      silent: true });

    removePaymentDeviceDialogInterval();
    $.header.deinit();
    $.hamburger_menu.deinit();
    $.product_search.deinit();
    $.product_index.deinit();
    $.customer_results.deinit();
    $.customer_tabset.deinit();
    $.cart.deinit();
    $.order_results.deinit();
    $.stopListening();
    $.destroy();
  }











  function handleAppRoute(options) {
    var deferred = new _.Deferred();
    logger.info('handleAppRoute - ' + options.route);
    switch (options.route) {
      case 'home':
        Alloy.eventDispatcher.trigger('hideAuxillaryViews');
        showHomeScreen(options).done(function () {
          deferred.resolve();
        }).fail(function () {
          deferred.reject();
        });
        break;

      case 'product_search_result':
        if (options.switch_only) {
          Alloy.eventDispatcher.trigger('hideAuxillaryViews');
          recordHistoryEvent(options);
          bringViewToFront('product_search_tab');
          deferred.resolve();
        } else {
          showProductSearchScreen(options).done(function () {
            deferred.resolve();
          }).fail(function () {
            deferred.reject();
          });
        }
        break;

      case 'product_search':
        showSearchDialog(options);
        deferred.resolve();
        break;

      case 'product_detail':
        Alloy.eventDispatcher.trigger('hideAuxillaryViews');
        if (options.switch_only) {
          recordHistoryEvent(options);
          bringViewToFront('product_detail_tab');
          deferred.resolve();
        } else {
          showProductDetailScreen(options).done(function () {
            deferred.resolve();
          }).fail(function () {
            deferred.reject();
          });
        }
        break;

      case 'customer_search':
        showSearchDialog(options);
        deferred.resolve();
        break;

      case 'order_search':
        showSearchDialog(options);
        deferred.resolve();
        break;

      case 'create_account':
        showCreateAccountDialog(options);
        deferred.resolve();
        break;

      case 'customer_search_result':
        showCustomerSearchScreen(options).done(function () {
          deferred.resolve();
        }).fail(function () {
          deferred.reject();
        });
        break;

      case 'customer':
        Alloy.eventDispatcher.trigger('hideAuxillaryViews');
        if (Alloy.Models.customer.isLoggedIn()) {

          showCustomerDetailScreen(options);
          deferred.resolve();
        } else {
          logger.info('tried to access customer detail without being logged in');
          deferred.reject();
        }
        break;

      case 'customer_logout':

        if (!Alloy.Models.customer.isLoggedIn()) {
          deferred.resolve();
          break;
        }
        Alloy.eventDispatcher.trigger('hideAuxillaryViews');
        doCustomerLogout(options).done(function () {
          deferred.resolve();
        }).fail(function () {
          deferred.reject();
        });
        break;

      case 'change_store_password':
        storePasswordHelpers.showChangeStorePassword(options).done(function () {
          deferred.resolve();
        }).fail(function () {
          deferred.reject();
        });
        break;

      case 'associate_logout':
        addPaymentDeviceTimer = false;
        Alloy.eventDispatcher.trigger('hideAuxillaryViews');
        if (customerAddress) {
          customerAddress.setModifyingCurrentAddress(false);
          customerAddress.setCurrentPage(null);
        }


        handleAppRoute({
          noHomeScreenNavigation: true,
          route: 'customer_logout' }).
        done(function () {
          doAssociateLogout(options).done(function () {
            deferred.resolve();
            if (Alloy.CFG.use_log_to_file && (Ti.App.deployType == 'production' || Ti.App.deployType == 'test')) {
              swissArmyUtils.backupLogs();
              eaUtils.uploadLogsToServer();
            }
          }).fail(function () {
            deferred.reject();
          });
        }).fail(function () {
          deferred.reject();
        });
        break;

      case 'associate_login':
        showAssociateLoginDialog(options).done(function () {
          deferred.resolve();
        }).fail(function () {
          deferred.reject();
        });
        break;

      case 'cart':
        Alloy.eventDispatcher.trigger('hideAuxillaryViews');
        if (Alloy.CFG.devices.payment_terminal_module != 'webDevice' && Alloy.CFG.devices.verify_payment_terminal_connection_at_checkout) {

          checkPaymentDevice();
        }
        showCartScreen(options);
        deferred.resolve();
        break;

      case 'order_search_result':
        showOrderSearchScreen(options).done(function () {
          deferred.resolve();
        }).fail(function () {
          deferred.reject();
        });
        break;}

    return deferred.promise();
  }








  function showHomeScreen(options) {
    var deferred = new _.Deferred();


    if (!Alloy.Models.productSearch.isEmptySearch()) {
      Alloy.Router.showActivityIndicator(deferred);

      Alloy.Models.productSearch.setSelectedSortingOption(null, {
        silent: true });


      Alloy.Models.productSearch.emptySearch().done(function () {

        bringViewToFront('product_search_tab');
        deferred.resolve();
        recordHistoryEvent(options);
      }).fail(function (model) {

        var messageString = _L('Error in application');
        var fault = model.get('fault');
        if (fault && fault.message) {
          messageString = fault.message;
        }
        Alloy.Dialog.showConfirmationDialog({
          messageString: messageString,
          titleString: _L('Unable to start the application'),
          okButtonString: _L('OK'),
          hideCancel: true });

        deferred.reject();
      });
    } else {

      bringViewToFront('product_search_tab');
      deferred.resolve();
      recordHistoryEvent(options);
      analytics.fireScreenEvent({
        screen: _L('Home') });

    }
    return deferred.promise();
  }








  function showProductSearchScreen(options) {
    var deferred = new _.Deferred();
    Alloy.Router.showActivityIndicator(deferred);

    $.product_search.init(options).fail(function (options) {
      logger.info('standard search was rejected: ' + JSON.stringify(options));

      deferred.reject();
    }).done(function () {
      if (searchDialog) {
        searchDialog.dismiss();
      }
      Alloy.eventDispatcher.trigger('hideAuxillaryViews');
      logger.info('switching to search tab');
      deferred.resolve();
      bringViewToFront('product_search_tab');
      if (Alloy.Models.productSearch.getTotal() == 1) {

        options.single_hit = true;
      }
      options.selected_refinements = Alloy.Models.productSearch.getSelectedRefinements();

      recordHistoryEvent(options);
      analytics.fireScreenEvent({
        screen: _L('Product Search') });

    });
    return deferred.promise();
  }







  function showSearchDialog(event) {
    logger.info('showSearchDialog');


    if (searchDialog && event && event.message) {
      searchDialog.setMessage(event.message);
    } else {
      searchDialog = Alloy.Dialog.showCustomDialog({
        controllerPath: 'search/search',
        initOptions: event,
        continueEvent: 'search:dismiss',
        continueFunction: function () {
          logger.info('search dismiss called');
          $.stopListening(searchDialog);
          searchDialog = null;
        },
        options: {
          tabId: event.route } });



      if (event && event.message) {
        searchDialog.setMessage(event.message);
      }
      searchDialog.focusSearchField();

      $.product_search.init();
    }
  }








  function showProductDetailScreen(options) {
    logger.info('showProductDetailScreen');
    var deferred = new _.Deferred();
    Alloy.Router.showActivityIndicator(deferred);

    Alloy.Models.product.clear({
      silent: true });


    $.product_index.init({
      product_id: options.product_id,
      variant_id: options.variant_id,
      historyCursor: options.historyCursor,
      replaceItem: options.replaceItem }).
    fail(function () {
      logger.info('$.product_index.init() fail in appIndex');
      deferred.reject();
    }).done(function () {
      logger.info('$.product_index.init() done in appIndex');

      bringViewToFront('product_detail_tab');
      deferred.resolve();

      if (!options.replaceItem) {
        recordHistoryEvent(options);
        analytics.fireScreenEvent({
          screen: _L('Product Detail') });

      }
    });
    return deferred.promise();
  }







  function requireAssociateLoggedIn(location) {
    Alloy.Router.presentAssociateLogin({
      location: location });

  }









  function showOrderSearchScreen(options) {
    promise = $.order_results.init(options);
    promise.done(function (result) {

      if (!result || result && !result.singleOrder) {
        Alloy.eventDispatcher.trigger('hideAuxillaryViews');
        bringViewToFront('order_tab');
        recordHistoryEvent(options);
      } else if (result && result.singleOrder && searchDialog) {

        searchDialog.dismiss();
      }
      analytics.fireScreenEvent({
        screen: _L('Order Lookup') });

    }).fail(function (event) {
      if (searchDialog && event && event.message) {
        searchDialog.setMessage(event.message);
      }
    });
    return promise;
  }








  function showCustomerSearchScreen(options) {
    var promise = $.customer_results.init(options);

    promise.done(function () {

      Alloy.eventDispatcher.trigger('hideAuxillaryViews');
      bringViewToFront('customer_search_tab');
      recordHistoryEvent(options);
      analytics.fireScreenEvent({
        screen: _L('Customer Search') });

    }).fail(function (event) {
      if (searchDialog && event && event.message) {
        searchDialog.setMessage(event.message);
      }
    });
    return promise;
  }







  function showCustomerDetailScreen(options) {
    bringViewToFront('customer_tab');
    recordHistoryEvent(options);
    analytics.fireScreenEvent({
      screen: _L('Customer Details') });

    $.customer_tabset.init();
  }







  function showCustomerSearchDialog(event) {
    var searchDialog = Alloy.Dialog.showCustomDialog({
      controllerPath: 'customerSearch/search',
      continueEvent: 'customer_search:dismiss' });

    if (event.errorMessage) {
      searchDialog.showErrorMessage(event.errorMessage);
    }
    searchDialog.focusSearchField();
  }






  function showCreateAccountDialog() {
    var createDialog = Alloy.Dialog.showCustomDialog({
      controllerPath: 'checkout/confirmation/createAccount',
      continueEvent: 'createAccount:dismiss',
      continueFunction: function () {
        logger.info('createAccount dismiss called');
        $.stopListening(createDialog);
        createDialog = null;
      } });

    createDialog.focusFirstField();


    $.listenTo(createDialog, 'createAccount:create', function (customer_info, address, loginPreference) {
      var deferred = new _.Deferred();
      Alloy.Router.showActivityIndicator(deferred);
      newCustomer.register(customer_info, {
        c_employee_id: currentAssociate.getEmployeeId(),
        c_employee_passcode: currentAssociate.getPasscode(),
        c_store_id: Alloy.CFG.store_id }).
      done(function () {

        analytics.fireAnalyticsEvent({
          category: _L('Users'),
          action: _L('Create User'),
          label: customer_info.username });

        analytics.fireUserEvent({
          category: _L('Users'),
          action: _L('Create User'),
          userId: customer_info.username });



        if (loginPreference) {
          var promise = newCustomer.loginCustomer({
            login: customer_info.customer.login },
          currentBasket);
          promise.done(function () {
            createDialog.dismiss();
            notify(_L('Account successfully created.'));
          }).fail(function (model, options, params) {
            createDialog.showErrorMessage(model ? model.get('fault') : null);
          }).always(function () {
            deferred.resolve();
          });
        } else {

          currentAssociate.loginAssociate({
            employee_id: currentAssociate.getEmployeeId(),
            passcode: currentAssociate.getPasscode() }).
          done(function () {
            createDialog.dismiss();
            notify(_L('Account successfully created.'));
          }).fail(function (data) {
            var failMsg = _L('Unable to create account.');
            if (data && data.faultDescription) {
              failMsg = data.faultDescription;
            } else if (data.get('httpStatus') != 200 && data.get('fault')) {
              failMsg = data.get('fault').message;
            }
            createDialog.showErrorMessage(failMsg);
          }).always(function () {
            deferred.resolve();
          });
        }
        analytics.fireAnalyticsEvent({
          category: _L('Customer'),
          action: _L('Create New User') });

      }).fail(function (model, options, params) {
        createDialog.showErrorMessage(model ? model.get('fault') : null);
        deferred.resolve();
      });
    });
  }








  function doCustomerLogout(options) {
    logger.info('doCustomerLogout');
    var deferred = new _.Deferred();
    Alloy.Router.showActivityIndicator(deferred);

    abandonOrder().done(function () {

      Alloy.Models.customer.syncSavedProducts(currentBasket, {
        c_employee_id: Alloy.Models.associate.getEmployeeId() }).
      done(function () {
        Alloy.Collections.history.reset([]);
        if (!options.noHomeScreenNavigation) {
          showHomeScreen(options);
        }

        Alloy.sigBlob = null;
        Alloy.Models.customer.doLogout();
        Alloy.Globals.resetCookies();
        logger.info('Attempting to log in as associate again ... clearing customer');
        resetAssociate().done(function () {
          deferred.resolve();
        }).fail(function () {
          deferred.reject();
        });
      }).fail(function () {
        deferred.reject();
      });
    }).fail(function () {
      deferred.reject();
    });
    return deferred.promise();
  }








  function resetAssociate() {
    var deferred = new _.Deferred();
    var employee_code = Alloy.Models.associate.getEmployeeId();
    var employee_pin = Alloy.Models.associate.getPasscode();
    Alloy.Models.associate.loginAssociate({
      employee_id: employee_code,
      passcode: employee_pin }).
    done(function () {
      logger.info('Logged in as associate again ... success ... loading basket');

      Alloy.Models.basket.deleteBasket().done(function () {
        Alloy.Models.basket.clear();
        Alloy.Models.basket.getBasket({
          c_eaEmployeeId: employee_code }).
        done(function () {
          Alloy.Models.basket.trigger('basket_sync');
          Alloy.Models.customer.clear();
          Alloy.Models.customer.trigger('change:login');

          delete Alloy.Models.basket.shippingMethods;
          deferred.resolve();
        }).fail(function () {
          customerLogoutFailureCleanup(employee_code, deferred);
        });
      }).fail(function () {
        logger.info('Failed to delete the basket, clearing customer and associate');
        customerLogoutFailureCleanup(employee_code, deferred);
      });
    }).fail(function () {
      deferred.reject();
      logger.error('Failed to log in as associate again ... clearing customer & associate');
      Alloy.Dialog.showConfirmationDialog({
        messageString: _L('The customer needs to be logged out before you can continue. Please try again.'),
        titleString: _L('Unable to logout customer.'),
        okButtonString: _L('Retry'),
        hideCancel: false,
        okFunction: function () {
          removeNotify();
          var promise = resetAssociate();
          Alloy.Router.showActivityIndicator(promise);
        } });

    });
    return deferred.promise();
  }








  function customerLogoutFailureCleanup(employeeId, deferred) {
    logger.error('failed to get the basket, clearing customer and associate');
    Alloy.Models.associate.clear();
    Alloy.Models.customer.clear();
    Alloy.Models.basket.clear();
    Alloy.Models.basket.getBasket({
      c_eaEmployeeId: employeeId });

    deferred.reject();
  }








  function doAssociateLogout(options) {
    logger.info('doAssociateLogout');
    var deferred = new _.Deferred();
    if (!Alloy.Models.associate.isLoggedIn()) {
      deferred.resolve();
      return deferred.promise();
    }
    Alloy.Router.showActivityIndicator(deferred);
    removePaymentDeviceDialogInterval();
    logger.info('isLoggedIn()');

    Alloy.sigBlob = null;


    if (Alloy.Models.basket.getOrderNumber()) {
      abandonOrder().done(function () {
        logger.info('order abandoned');

        if (Alloy.Models.customer.isLoggedIn()) {
          logger.info('customer isLoggedIn()');
          Alloy.Models.customer.syncSavedProducts(Alloy.Models.basket, {
            c_employee_id: Alloy.Models.associate.getEmployeeId() }).
          always(function () {
            logger.info('basket sync\'d');
            Alloy.Models.customer.clear();
            associateLogout(options, deferred);
          });
        } else {
          logger.info('isLoggedIn()');
          associateLogout(options, deferred);
        }
      }).fail(function () {
        notify(_L('Could not cancel the order.'), {
          preventAutoClose: true });

      });
    } else {

      if (Alloy.Models.customer.isLoggedIn()) {
        logger.info('customer isLoggedIn()!');
        Alloy.Models.customer.syncSavedProducts(Alloy.Models.basket, {
          c_employee_id: Alloy.Models.associate.getEmployeeId() }).
        always(function () {
          Alloy.Models.customer.clear();
          associateLogout(options, deferred);
        });
      } else {
        logger.info('customer not isLoggedIn()!');
        associateLogout(options, deferred);
      }
    }
    return deferred.promise();
  }







  function abandonOrder() {
    var deferred = new _.Deferred();

    if (Alloy.Models.basket.getOrderNumber()) {
      logger.info('has order number');
      var orderNo = Alloy.Models.basket.getOrderNumber();



      setTimeout(function () {
        logger.info('appIndex: cancelServerTransaction for order: ' + orderNo);
        paymentTerminal.cancelServerTransaction({
          order_no: orderNo });

      }, 5000);

      Alloy.Models.basket.abandonOrder(Alloy.Models.associate.getEmployeeId(), Alloy.Models.associate.getPasscode(), Alloy.CFG.store_id).done(function () {
        logger.info('order abandoned');

        deferred.resolve();
      }).fail(function () {
        notify(_L('Could not cancel the order.'), {
          preventAutoClose: true });

        deferred.reject();
      });
    } else {
      deferred.resolve();
    }
    return deferred.promise();
  }








  function showAssociateLoginDialog(options) {
    var deferred = new _.Deferred();

    Alloy.sigBlob = null;
    logger.info('showAssociateLoginDialog');
    if (Alloy.Models.associate.isLoggedIn()) {

      logger.info('showAssociateLoginDialog - associate_login - already logged in ... logging out instead');
      doAssociateLogout(options).done(function () {
        deferred.resolve();
      }).fail(function () {
        deferred.reject();
      });
    } else {
      Alloy.eventDispatcher.trigger('app:login', options);
      deferred.resolve();
    }
    return deferred.promise();
  }







  function showCartScreen(options) {
    $.cart.render();

    recordHistoryEvent(options);
    bringViewToFront('cart_tab');
    if (options.switchToWishList) {

      $.cart.cart.switchToWishListView();
    } else if (options.switchToCart) {

      $.cart.cart.switchToCartView();
    }
  }








  function associateLogout(options, deferred) {
    logger.info('associateLogout');
    Alloy.Collections.history.reset([]);

    Alloy.Models.basket.deleteBasket().done(function () {
      Alloy.Models.basket.clear();
      delete Alloy.Models.basket.shippingMethods;
      Alloy.Models.basket.trigger('basket_sync');
      Alloy.Models.associate.logout().always(function () {
        logger.info('associateLogout done()');
        Alloy.eventDispatcher.trigger('associate_logout');

        showHomeScreen(options);
        Alloy.eventDispatcher.trigger('app:login', options);
        deferred.resolve();
      });
    }).fail(function () {
      logger.info('could not clean up the server basket');
      deferred.reject();
    });
  }






  function removeAllDrawers() {
    if ($.size_chart_drawer) {
      $.appIndex.remove($.size_chart_drawer);
    }
  }







  function bringViewToFront(tab_id) {
    Alloy.eventDispatcher.trigger('app:navigation', {
      view: tab_id });

    for (var t in tabs) {
      if (t != tab_id) {
        tabs[t].hide();
        tabs[t].setHeight(0);
        tabs[t].setWidth(0);
      } else {
        tabs[t].setHeight(Ti.UI.FILL);
        tabs[t].setWidth('100%');
        tabs[t].show();
      }
    }
  }







  function Router() {
    return {







      navigateToCart: function (info) {
        info = _.extend({
          route: 'cart' },
        info);
        return handleAppRoute(info);
      },








      navigateToCustomer: function (info) {
        info = _.extend({
          route: 'customer' },
        info);
        return handleAppRoute(info);
      },








      navigateToCustomerSearchResult: function (info) {
        info = _.extend({
          route: 'customer_search_result' },
        info);
        return handleAppRoute(info);
      },








      customerLogout: function (info) {
        info = _.extend({
          route: 'customer_logout' },
        info);
        return handleAppRoute(info);
      },








      presentChangeStorePasswordDrawer: function (info) {
        info = _.extend({
          route: 'change_store_password' },
        info);
        return handleAppRoute(info);
      },








      navigateToHome: function (info) {
        info = _.extend({
          route: 'home' },
        info);

        return handleAppRoute(info);
      },








      navigateToCategory: function (info) {
        info = _.extend({
          route: 'product_search_result',
          query: '' },
        info);

        return handleAppRoute(info);
      },








      navigateToProduct: function (info) {
        info = _.extend({
          route: 'product_detail' },
        info);

        return handleAppRoute(info);
      },








      navigateToProductSearch: function (info) {
        info = _.extend({
          route: 'product_search_result' },
        info);

        return handleAppRoute(info);
      },








      navigateToOrderSearchResult: function (info) {
        info = _.extend({
          route: 'order_search_result' },
        info);

        return handleAppRoute(info);
      },








      presentAssociateLogin: function (info) {
        info = _.extend({
          route: 'associate_login' },
        info);
        return handleAppRoute(info);
      },








      presentCustomerSearchDrawer: function (info) {
        info = _.extend({
          route: 'customer_search' },
        info);
        return handleAppRoute(info);
      },








      presentOrderSearchDrawer: function (info) {
        info = _.extend({
          route: 'order_search' },
        info);
        return handleAppRoute(info);
      },








      presentProductSearchDrawer: function (info) {
        info = _.extend({
          route: 'product_search' },
        info);
        return handleAppRoute(info);
      },








      presentPrinterChooserDrawer: function (info) {
        info = _.extend({
          route: 'printer_chooser' },
        info);
        return handleAppRoute(info);
      },








      associateLogout: function (info) {
        info = _.extend({
          route: 'associate_logout' },
        info);
        return handleAppRoute(info);
      },








      presentCreateAccountDrawer: function (info) {
        info = _.extend({
          route: 'create_account' },
        info);
        return handleAppRoute(info);
      },








      checkPaymentDevice: function (verified) {
        return checkPaymentDevice(verified);
      },







      paymentDeviceConnectionChecked: function (connected) {
        if (connected != paymentDeviceConnected) {
          paymentDeviceConnected = connected;
          $.header.updateDeviceStatus(false, connected);
          if ($.no_payment_terminal) {
            $.no_payment_terminal.updateDeviceStatus(connected);
          }
        }
      },






      startup: function () {
        Alloy.eventDispatcher.trigger('app:startup', {
          login: true });

      },







      showActivityIndicator: function (deferred) {
        showActivityIndicator(deferred);
      },






      showHideHamburgerMenu: function () {
        $.hamburger_menu.showHideHamburgerMenu();
      },






      hideHamburgerMenu: function () {
        $.hamburger_menu.hideHamburgerMenu();
      },








      navigate: function (info) {

        return handleAppRoute(info);
      } };

  }








  function checkPaymentDevice(verifiedNotConnected) {
    var deferred = new _.Deferred();
    var isVerifiedNotConnected = verifiedNotConnected || false;
    logger.info('checking payment device');
    if (isVerifiedNotConnected || !paymentTerminal.verifyDeviceConnection()) {
      createNoPaymentTerminal().done(function () {
        deferred.resolve();
      }).fail(function () {
        deferred.reject();
      });
    } else {
      addPaymentDeviceDialogInterval();
      deferred.resolve();
    }
    return deferred.promise();
  }







  function createNoPaymentTerminal() {
    var deferred = new _.Deferred();
    logger.info('creating no payment terminal popover');

    if (!$.no_payment_terminal) {
      $.no_payment_terminal = Alloy.Dialog.showCustomDialog({
        controllerPath: 'checkout/payments/noPaymentTerminal',
        continueEvent: 'noPaymentTerminal:dismiss',
        continueFunction: function () {
          $.no_payment_terminal = null;

          if (paymentDeviceConnected) {
            deferred.resolve();
          } else {
            deferred.reject();
          }
          addPaymentDeviceDialogInterval();
        } });

      removePaymentDeviceDialogInterval();
    } else {

      $.no_payment_terminal.once('noPaymentTerminal:dismiss', function () {

        if (paymentDeviceConnected) {
          deferred.resolve();
        } else {
          deferred.reject();
        }
      });
    }
    return deferred.promise();
  }






  function removePaymentDeviceDialogInterval() {
    if (paymentDeviceDialogInterval) {
      logger.info('removing payment device dialog interval');
      clearInterval(paymentDeviceDialogInterval);
      paymentDeviceDialogInterval = null;
    }
  }






  function addPaymentDeviceDialogInterval() {
    if (!addPaymentDeviceTimer && paymentDeviceDialogInterval) {
      return;
    }
    if (Alloy.CFG.devices.payment_terminal_module != 'webDevice' && Alloy.CFG.devices.check_device_dialog_interval > 0) {
      logger.info('adding payment device dialog interval');
      paymentDeviceDialogInterval = setInterval(checkPaymentDevice, Alloy.CFG.devices.check_device_dialog_interval);
    }
  }






  function onAppReady() {
    var deferredStore = new _.Deferred();


    if (!isKioskMode()) {
      logger.info('onAppReady calling checkStorePasswordWarning');
      deferredStore = storePasswordHelpers.checkStorePasswordWarning();
    } else {
      deferredStore.resolve();
    }

    deferredStore.always(function () {
      if (Alloy.CFG.devices.payment_terminal_module != 'webDevice' && (Alloy.CFG.devices.verify_payment_terminal_connection_at_login || Alloy.CFG.devices.check_device_dialog_interval > 0)) {
        addPaymentDeviceTimer = true;
        checkPaymentDevice();
      }
      $.header.initNotifications();
    });
  }







  function ViewManager() {

    return {
      isProductSearchViewVisible: function () {
        return $.product_search_tab.getVisible();
      },

      isProductDetailViewVisible: function () {
        return $.product_detail_tab.getVisible();
      },

      isCustomerSearchViewVisible: function () {
        return $.customer_search_tab.getVisible();
      },

      isCustomerViewVisible: function () {
        return $.customer_tab.getVisible();
      },

      isCartViewVisible: function () {
        return $.cart_tab.getVisible();
      },

      isOrderSearchViewVisible: function () {
        return $.order_tab.getVisible();
      } };

  }









  _.extend($, exports);
}

module.exports = Controller;