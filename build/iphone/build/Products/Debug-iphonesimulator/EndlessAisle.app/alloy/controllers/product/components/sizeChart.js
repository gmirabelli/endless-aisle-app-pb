var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
		delete obj[key];
	}
	return arg;
}

function Controller() {

	require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'product/components/sizeChart';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};







	$.__views.size_chart_window = Ti.UI.createView(
	{ backgroundColor: Alloy.Styles.color.background.transparent, id: "size_chart_window", accessibilityLabel: "size_chart_window" });

	$.__views.size_chart_window && $.addTopLevelView($.__views.size_chart_window);
	$.__views.backdrop = Ti.UI.createView(
	{ top: 0, left: 0, width: 1024, height: 768, opacity: 0.55, backgroundColor: Alloy.Styles.color.background.black, id: "backdrop" });

	$.__views.size_chart_window.add($.__views.backdrop);
	$.__views.size_chart = Ti.UI.createWebView(
	{ top: 75, left: 128, width: 768, backgroundColor: Alloy.Styles.color.background.white, height: 600, id: "size_chart", willHandleTouches: false });

	$.__views.size_chart_window.add($.__views.size_chart);
	exports.destroy = function () {};




	_.extend($, $.__views);










	var logger = require('logging')('product:components:sizeChart', getFullControllerPath($.__controllerPath));




	$.backdrop.addEventListener('click', dismiss);




	exports.init = init;
	exports.deinit = deinit;











	function init(args) {
		logger.info('init called');
		$.size_chart.setHtml('');
		var head = '<meta name="viewport" content="user-scalable=0,initial-scale=0">' + '<style>' + Alloy.CFG.webViewCssReset + (Alloy.CFG.product.descriptionWebViewCss || '') + '</style>';
		if (args.cssFile) {
			head += '<link rel="stylesheet" type="text/css" href="' + args.cssFile + '" />';
		}
		var html = head + '<div>' + args.body + '</div>';
		$.size_chart.setHtml(html);
	}






	function deinit() {
		logger.info('deinit called');
		$.backdrop.removeEventListener('click', dismiss);
	}









	function dismiss() {
		logger.info('dismiss called');
		$.trigger('sizeChart:dismiss');
	}









	_.extend($, exports);
}

module.exports = Controller;