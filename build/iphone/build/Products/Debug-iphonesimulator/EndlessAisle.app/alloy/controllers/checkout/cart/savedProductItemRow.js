var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
		delete obj[key];
	}
	return arg;
}

function Controller() {

	require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'checkout/cart/savedProductItemRow';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};







	$.__views.productItemRowTable = Ti.UI.createTableViewRow(
	{ layout: "vertical", selectionStyle: Ti.UI.iOS.TableViewCellSelectionStyle.NONE, top: 0, contains_product_image: true, product_id: $model.__transform.row_id, id: "productItemRowTable" });

	$.__views.productItemRowTable && $.addTopLevelView($.__views.productItemRowTable);
	$.__views.product_line_item_container = Ti.UI.createView(
	{ top: 20, height: 173, width: 650, layout: "horizontal", left: 32, backgroundImage: Alloy.Styles.customerBorderImage, id: "product_line_item_container", contains_product_image: true });

	$.__views.productItemRowTable.add($.__views.product_line_item_container);
	$.__views.product_item = Alloy.createController('checkout/cart/productItem', { id: "product_item", __parentSymbol: $.__views.product_line_item_container });
	$.__views.product_item.setParent($.__views.product_line_item_container);
	$.__views.button_container = Ti.UI.createView(
	{ layout: "vertical", left: 20, id: "button_container" });

	$.__views.product_line_item_container.add($.__views.button_container);
	$.__views.edit_delete_button_container = Ti.UI.createView(
	{ top: 10, left: 0, height: Ti.UI.SIZE, width: "100%", id: "edit_delete_button_container" });

	$.__views.button_container.add($.__views.edit_delete_button_container);
	$.__views.delete_button = Ti.UI.createButton(
	{ height: 55, width: 55, backgroundImage: Alloy.Styles.deleteButtonIcon, id: "delete_button", accessibilityLabel: "delete_button" });

	$.__views.edit_delete_button_container.add($.__views.delete_button);
	$.__views.add_button = Ti.UI.createButton(
	{ backgroundImage: Alloy.Styles.secondaryButtonImage, width: 140, height: 28, color: Alloy.Styles.buttons.secondary.color, font: Alloy.Styles.switchLabelFont, top: 9, titleid: "_Add_to_Cart", id: "add_button", accessibilityValue: "add_button" });

	$.__views.button_container.add($.__views.add_button);
	$.__views.more_menu_button = Ti.UI.createButton(
	{ backgroundImage: Alloy.Styles.secondaryButtonImage, width: 140, height: 28, color: Alloy.Styles.color.text.white, font: Alloy.Styles.threeDotsButtonFont, top: 9, title: "• • •", id: "more_menu_button", accessibilityValue: "menu_button" });

	$.__views.button_container.add($.__views.more_menu_button);
	exports.destroy = function () {};




	_.extend($, $.__views);










	var logger = require('logging')('checkout:cart:savedProductItemRow', getFullControllerPath($.__controllerPath));


	var buttonTextLength = 18;












	$.productItemRowTable.update = function (product, pli, showDivider) {
		$.product_item.init(product, pli);
		if (!showDivider) {
			$.product_line_item_container.setBackgroundImage(null);
		}
	};






	$.productItemRowTable.deinit = function () {
		logger.info('DEINIT called');
		$.product_item.deinit();
		$.destroy();
	};





	if (!Alloy.Models.customer.isLoggedIn() && Alloy.CFG.enable_wish_list) {
		$.button_container.remove($.more_menu_button);
	} else if (Alloy.Models.customer.isLoggedIn() && Alloy.CFG.enable_wish_list && Alloy.Models.customer.productLists.getWishListCount() < 1 || Alloy.Models.customer.isLoggedIn() && !Alloy.CFG.enable_wish_list) {

		$.button_container.remove($.more_menu_button);
	} else {

		$.button_container.remove($.add_button);
	}

	if ($.add_button && $.add_button.getTitle().length > buttonTextLength) {
		$.add_button.setFont(Alloy.Styles.detailInfoBoldFont);
	}









	_.extend($, exports);
}

module.exports = Controller;