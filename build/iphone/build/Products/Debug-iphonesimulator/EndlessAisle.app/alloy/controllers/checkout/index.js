var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'checkout/index';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.checkout_container = Ti.UI.createView(
  { layout: "horizontal", bottom: 0, top: 55, id: "checkout_container" });

  $.__views.checkout_container && $.addTopLevelView($.__views.checkout_container);
  $.__views.left_column = Ti.UI.createView(
  { layout: "vertical", width: 690, left: 0, height: "100%", top: 0, bottom: 0, id: "left_column" });

  $.__views.checkout_container.add($.__views.left_column);
  $.__views.header_container = Ti.UI.createView(
  { height: 40, id: "header_container" });

  $.__views.left_column.add($.__views.header_container);
  $.__views.header = Alloy.createController('checkout/components/header', { id: "header", __parentSymbol: $.__views.header_container });
  $.__views.header.setParent($.__views.header_container);
  $.__views.checkout_tab_group = Ti.UI.createView(
  { top: 0, bottom: 0, left: 0, width: "100%", id: "checkout_tab_group" });

  $.__views.left_column.add($.__views.checkout_tab_group);
  $.__views.cart_tab = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.tabs.backgroundColorEnabled, font: Alloy.Styles.tabFont, width: "100%", bottom: 0, id: "cart_tab" });

  $.__views.checkout_tab_group.add($.__views.cart_tab);
  $.__views.cart = Alloy.createController('checkout/cart/index', { id: "cart", __parentSymbol: $.__views.cart_tab });
  $.__views.cart.setParent($.__views.cart_tab);
  $.__views.address_tab = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.tabs.backgroundColorEnabled, font: Alloy.Styles.tabFont, width: "100%", bottom: 0, id: "address_tab" });

  $.__views.checkout_tab_group.add($.__views.address_tab);
  $.__views.shipping_address = Alloy.createController('checkout/address/index', { id: "shipping_address", addressType: "shipping", __parentSymbol: $.__views.address_tab });
  $.__views.shipping_address.setParent($.__views.address_tab);
  $.__views.billing_address_tab = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.tabs.backgroundColorEnabled, font: Alloy.Styles.tabFont, width: "100%", bottom: 0, id: "billing_address_tab" });

  $.__views.checkout_tab_group.add($.__views.billing_address_tab);
  $.__views.billing_address = Alloy.createController('checkout/address/index', { id: "billing_address", addressType: "billing", __parentSymbol: $.__views.billing_address_tab });
  $.__views.billing_address.setParent($.__views.billing_address_tab);
  $.__views.shipping_methods_tab = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.tabs.backgroundColorEnabled, font: Alloy.Styles.tabFont, width: "100%", bottom: 0, id: "shipping_methods_tab" });

  $.__views.checkout_tab_group.add($.__views.shipping_methods_tab);
  $.__views.shipping_method = Alloy.createController('checkout/shippingMethod/index', { id: "shipping_method", __parentSymbol: $.__views.shipping_methods_tab });
  $.__views.shipping_method.setParent($.__views.shipping_methods_tab);
  $.__views.payments_tab = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.tabs.backgroundColorEnabled, font: Alloy.Styles.tabFont, width: "100%", bottom: 0, id: "payments_tab" });

  $.__views.checkout_tab_group.add($.__views.payments_tab);
  $.__views.payments = Alloy.createController('checkout/payments/index', { id: "payments", __parentSymbol: $.__views.payments_tab });
  $.__views.payments.setParent($.__views.payments_tab);
  $.__views.confirmation_tab = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.tabs.backgroundColorEnabled, font: Alloy.Styles.tabFont, width: "100%", bottom: 0, id: "confirmation_tab" });

  $.__views.checkout_tab_group.add($.__views.confirmation_tab);
  $.__views.confirmation = Alloy.createController('checkout/confirmation/index', { id: "confirmation", __parentSymbol: $.__views.confirmation_tab });
  $.__views.confirmation.setParent($.__views.confirmation_tab);
  $.__views.divider = Ti.UI.createView(
  { top: 0, width: 4, backgroundImage: Alloy.Styles.leftShadowImage, zIndex: 3, height: "100%", backgroundColor: "transparent", id: "divider" });

  $.__views.checkout_container.add($.__views.divider);
  $.__views.right_column_container = Ti.UI.createView(
  { id: "right_column_container" });

  $.__views.checkout_container.add($.__views.right_column_container);
  $.__views.right_column = Ti.UI.createView(
  { layout: "vertical", width: 330, left: 0, top: 0, id: "right_column" });

  $.__views.right_column_container.add($.__views.right_column);
  $.__views.order_summary_label = Ti.UI.createLabel(
  { width: 340, height: 40, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, backgroundColor: Alloy.Styles.color.background.medium, font: Alloy.Styles.detailLabelFont, borderWidth: 1, borderColor: Alloy.Styles.color.border.medium, textid: "_Order_Summary", id: "order_summary_label" });

  $.__views.right_column.add($.__views.order_summary_label);
  $.__views.order_totals = Alloy.createController('checkout/components/orderTotalSummary', { id: "order_totals", __parentSymbol: $.__views.right_column });
  $.__views.order_totals.setParent($.__views.right_column);
  $.__views.promotions = Alloy.createController('checkout/components/promotionSummary', { id: "promotions", __parentSymbol: $.__views.right_column });
  $.__views.promotions.setParent($.__views.right_column);
  $.__views.basket_shipping_details = Alloy.createController('checkout/components/shippingSummary', { id: "basket_shipping_details", __parentSymbol: $.__views.right_column });
  $.__views.basket_shipping_details.setParent($.__views.right_column);
  $.__views.entered_payments = Alloy.createController('checkout/components/paymentSummary', { id: "entered_payments", __parentSymbol: $.__views.right_column });
  $.__views.entered_payments.setParent($.__views.right_column);
  $.__views.checkout_button = Ti.UI.createButton(
  { left: 24, width: 280, height: 42, backgroundImage: Alloy.Styles.primaryButtonImage, backgroundDisabledColor: Alloy.Styles.buttons.primary.color, color: Alloy.Styles.buttons.primary.color, top: 620, titleid: "_Checkout", id: "checkout_button", accessibilityValue: "checkout_button" });

  $.__views.right_column_container.add($.__views.checkout_button);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var safariDialog = null;
  var currentCustomer = Alloy.Models.customer;
  var currentBasket = Alloy.Models.basket;
  var currentAssociate = Alloy.Models.associate;
  var checkoutStates = Alloy.Models.basket.checkoutStates;
  var paymentTerminal = require(Alloy.CFG.devices.payment_terminal_module);
  var eaUtils = require('EAUtils');
  var analytics = require('analyticsBase');
  var webOrderDeferred = null;
  var processingWebOrder = false;
  var logger = require('logging')('checkout:index', getFullControllerPath($.__controllerPath));

  var customerIsLoggedIn = currentCustomer.hasCustomerNumber() ? true : false;
  var toCountryCode = eaUtils.countryNameToCountryCode;
  var getAddressNickname = eaUtils.getAddressNickname;
  var savedItemsDisplayed = false;
  var currentPage = 0;
  var wishListItemsDisplayed = false;
  var tokenHash = {};





  $.listenTo(Alloy.eventDispatcher, 'associate_logout', function () {
    abandonOrder();
  });


  $.listenTo(Alloy.eventDispatcher, 'cart_cleared', function () {
    render();
    updateCheckoutButton();
  });


  $.listenTo(Alloy.eventDispatcher, 'hideAuxillaryViews', function () {
    closeSafariDialog();
  });


  $.listenTo(Alloy.eventDispatcher, 'configurations:postlogin', function () {

    if (Alloy.CFG.payment_entry === 'web') {
      checkoutStates = ['cart', 'shippingAddress', 'askBillingAddress', 'billingAddress', 'shippingMethod', 'payThroughWeb', 'confirmation'];
    } else if (Alloy.CFG.payment_entry === 'default') {
      checkoutStates = Alloy.CFG.collect_billing_address ? ['cart', 'shippingAddress', 'askBillingAddress', 'billingAddress', 'shippingMethod', 'payments', 'confirmation'] : ['cart', 'shippingAddress', 'shippingMethod', 'payments', 'confirmation'];
    }
    currentBasket.setCheckoutStates(checkoutStates);
  });


  $.listenTo(Alloy.eventDispatcher, 'payment:check_signature', showCheckSignature);


  $.listenTo(Alloy.eventDispatcher, 'payment:prompt_signature', showPromptSignature);


  $.listenTo(Alloy.eventDispatcher, 'payment:hide_signature', hideSignature);


  $.listenTo(Alloy.eventDispatcher, 'order_just_created', handleOrderCompletion);



  $.listenTo(Alloy.eventDispatcher, 'appLaunch:browserUrl', function (event) {
    if (!processingWebOrder) {

      return;
    }

    var url = decodeURI(event.url);
    logger.info('Received external launch event: ' + url);

    var index = url.indexOf('?');
    if (index == -1) {
      return;
    }


    var paramObj = {};
    var params = url.substr(index + 1).split('&');
    _.each(params, function (param) {
      var parts = param.split('=');
      var key = parts[0];
      var value = '';
      if (parts.length > 1) {
        value = parts[1];
      }
      paramObj[key] = value;
    });


    var token = paramObj['token'];
    if (!token) {
      return;
    }


    var orderToken = paramObj['token'];
    var orderNo = null;

    if (orderToken && tokenHash[orderToken]) {
      orderNo = tokenHash[orderToken];
      delete tokenHash[orderToken];
    } else {
      logger.info('Order token does not match, ignoring invalid token.');
      return;
    }


    if (paramObj.hasOwnProperty('action') && paramObj['action'] == 'cancel' || paramObj['action'] == 'timeout' || paramObj['action'] == 'confirmCancelledOrder') {

      if (closeSafariDialog()) {
        if (paramObj['action'] == 'timeout') {
          notify(_L('Server call failed. Please try again.'), {
            preventAutoClose: true });

        }

        cancelWebOrder(orderNo, paramObj['action']).always(function () {
          webOrderDeferred.resolve();
        });
      }
    } else if (paramObj.hasOwnProperty('action') && paramObj['action'] == 'confirm') {

      closeSafariDialog();

      var b = currentBasket.clone();
      currentBasket.clear({
        silent: true });


      logger.info('fetching full basket');
      currentBasket.getBasket({
        c_eaEmployeeId: Alloy.Models.associate.getEmployeeId() }).
      done(function () {
        if (currentCustomer.isLoggedIn()) {
          currentCustomer.claimBasket(currentBasket).done(function () {
            finishFetchOrder(orderNo);
          }).fail(function () {
            deferred.reject();
          });
        } else {
          finishFetchOrder(orderNo);
        }
      }).always(function () {
        webOrderDeferred.resolve();
      });
    }
  });




  $.checkout_button.addEventListener('click', onCheckoutButtonClick);



  $.listenTo($.shipping_method, 'checkoutDisabled', function () {
    currentBasket.setCheckoutStatus('cart');
  });



  $.listenTo($.shipping_address, 'checkoutDisabled', function () {
    currentBasket.setCheckoutStatus('cart');
  });



  $.listenTo($.billing_address, 'checkoutDisabled', function () {
    currentBasket.setCheckoutStatus('cart');
  });


  $.listenTo($.shipping_method, 'shippingOverride', function (args) {
    Alloy.Dialog.showCustomDialog({
      controllerPath: 'checkout/shippingMethod/shippingOverrides',
      continueEvent: 'shippingOverride:dismiss',
      initOptions: {
        selectedOverride: args.selectedShippingOverride,
        selectedMethod: args.selectedShippingMethod } });


  });

  $.listenTo($.confirmation, 'orderAbandoned', handleOrderAbandoned);


  $.listenTo($.payments, 'orderAbandoned', handleOrderAbandoned);


  $.listenTo($.payments, 'payment:error', function (error) {
    Alloy.Dialog.showConfirmationDialog({
      messageString: error.message,
      titleString: error.title,
      hideCancel: true,
      okFunction: function () {

        dismissPaymentTerminal();
      } });

  });

  $.listenTo($.payments, 'payment:success', function (success) {
    Alloy.Dialog.showConfirmationDialog({
      messageString: success.message,
      titleString: success.title,
      hideCancel: true,
      okFunction: function () {
        if (success.completionFunction) {
          success.completionFunction();
        }
      } });

  });


  $.listenTo($.payments, 'payment:gc_balance', openGCBalanceModal);


  $.listenTo($.payments, 'payment:use_terminal', function (args) {

    Alloy.Router.checkPaymentDevice().done(function () {
      args = _.extend(args, {
        hideManualEntry: paymentMethodType == 'giftCard' ? true : false });

      $.payment_terminal = Alloy.Dialog.showCustomDialog({
        controllerPath: 'checkout/payments/paymentTerminal',
        continueEvent: 'payment_terminal:dismiss',
        initOptions: args,
        continueFunction: function () {
          $.payment_terminal = null;
          $.payments.stopPaymentListening();
        } });

    });

  });

  $.listenTo($.cart, 'cart:display', function (event) {
    savedItemsDisplayed = event.savedItemsDisplayed;
    wishListItemsDisplayed = event.wishListItemsDisplayed;
    updateCheckoutButton();
    render();
  });





  $.listenTo(currentBasket, 'change:checkout_status', function () {
    logger.info('Responding to change in checkoutStatus');

    updateCheckoutButton();

    if (currentBasket.getCheckoutStatus() == 'shippingAddress' && currentBasket.getLastCheckoutStatus() == 'cart') {
      moveFromCartToShippingAddress();
    } else if (currentBasket.getCheckoutStatus() == 'askBillingAddress' && currentBasket.getLastCheckoutStatus() == 'shippingAddress') {
      moveFromShippingToBillingAddress();
    } else {
      handlePageSelection({
        page: currentBasket.getCheckoutStatus() });

    }
  });



  $.listenTo(currentBasket, 'change:shipments reset:shipments', function (type, model, something, options) {
    logger.info('Responding to change in shipments or reset shipments');
    updateCheckoutButton();
  });


  $.listenTo(currentCustomer, 'change', function () {
    logger.info('Responding to customer change');
    var customerNowLoggedIn = currentCustomer.hasCustomerNumber() ? true : false;

    if (customerNowLoggedIn != customerIsLoggedIn) {
      currentBasket.setCheckoutStatus('cart');
    }
    customerIsLoggedIn = customerNowLoggedIn;
  });


  $.listenTo(currentBasket, 'change:product_items reset:product_items change:enable_checkout', function () {
    logger.info('Responding to change or reset product_items');

    updateCheckoutButton();
  });




  exports.init = init;
  exports.render = render;
  exports.deinit = deinit;









  function init() {
    logger.info('init called');
    $.shipping_address.init();
    $.billing_address.init();
    render();
  }






  function render() {
    logger.info('render called');
    currentBasket.setCheckoutStatus('cart');
  }






  function deinit() {
    logger.info('deinit called');
    if (safariDialog) {
      safariDialog.removeEventListener('open', onSafariOpen);
      safariDialog.removeEventListener('close', onSafariClose);
    }

    $.stopListening();
    $.checkout_button.removeEventListener('click', onCheckoutButtonClick);


    $.shipping_method.deinit();
    $.shipping_address.deinit();
    $.billing_address.deinit();
    $.header.deinit();
    $.cart.deinit();
    $.payments.deinit();
    $.confirmation.deinit();
    $.order_totals.deinit();
    $.promotions.deinit();
    $.basket_shipping_details.deinit();
    $.entered_payments.deinit();
    $.destroy();
  }









  function hideAllPages() {

    if (!currentBasket.getLastCheckoutStatus()) {
      for (var i = 0, ii = $.checkout_tab_group.children.length; i < ii; i++) {
        $.checkout_tab_group.children[i].hide();
      }
    }
  }







  function hideCurrentPage() {
    $.checkout_tab_group.children[currentPage].hide();
  }







  function displayCartPage() {
    var deferred = new _.Deferred();

    $.cart.init();


    hideCurrentPage();
    currentPage = 0;

    $.checkout_tab_group.children[0].show();


    updateCheckoutButton();

    currentBasket.trigger('change:shipments');
    currentBasket.trigger('change:payment_details');
    analytics.fireScreenEvent({
      screen: _L('Cart') });

    return deferred.promise();
  }







  function displayShippingAddressPage() {

    var promise = $.shipping_address.render(true);
    Alloy.Router.showActivityIndicator(promise);
    promise.done(function () {
      hideCurrentPage();
      currentPage = 1;
      $.checkout_tab_group.children[1].show();
      analytics.fireScreenEvent({
        screen: _L('Shipping Address') });

    }).fail(function () {
      currentBasket.setCheckoutStatus(currentBasket.getLastCheckoutStatus(), {
        silent: true });

    });
    return promise;
  }








  function moveFromShippingToBillingAddress() {
    logger.info('moveFromShippingToBillingAddress hasBillingAddress ' + currentBasket.hasBillingAddress() + ' getShipToStore ' + currentBasket.getShipToStore());



    if (currentBasket.hasBillingAddress()) {
      displayBillingAddressPage();
    } else if (currentBasket.getShipToStore() || currentBasket.getDifferentStorePickup()) {

      if (currentCustomer.isLoggedIn()) {
        displayBillingAddressPage();
        return;
      }
      Alloy.Dialog.showConfirmationDialog({
        messageString: _L('Is the billing person the same as the shipping person?'),
        titleString: _L('Same Person'),
        okButtonString: _L('Yes'),
        cancelButtonString: _L('No'),
        okFunction: function () {
          var shippingAddress = currentBasket.getShippingAddress();
          var newBillingAddress = {
            first_name: shippingAddress.getFirstName(),
            last_name: shippingAddress.getLastName(),
            address1: '',
            address2: '',
            city: '',
            state_code: '',
            postal_code: '',
            country_code: '',
            phone: shippingAddress.getPhone() };

          $.billing_address.setAddress(newBillingAddress, null, false, {
            c_employee_id: Alloy.Models.associate.getEmployeeId() }).
          done(function () {
            displayBillingAddressPage();
          }).fail(function () {
            currentBasket.setCheckoutStatus(currentBasket.getLastCheckoutStatus(), {
              silent: true });

          });
        },
        cancelFunction: function () {
          displayBillingAddressPage();
        } });

    } else {
      Alloy.Dialog.showConfirmationDialog({
        messageString: _L('Is the billing address the same as the shipping address?'),
        titleString: _L('Same Address'),
        okButtonString: _L('Yes'),
        cancelButtonString: _L('No'),
        okFunction: function () {
          var shippingAddress = currentBasket.getShippingAddress();
          logger.info('setting billing address to shipping address ');
          logger.secureLog(JSON.stringify(shippingAddress, null, 4));
          $.billing_address.setAddress(shippingAddress, null, false, {
            c_employee_id: Alloy.Models.associate.getEmployeeId() }).
          done(function () {

            currentBasket.setCheckoutStatus('shippingMethod');
          }).fail(function () {
            currentBasket.setCheckoutStatus(currentBasket.getLastCheckoutStatus(), {
              silent: true });

          });
        },
        cancelFunction: function () {
          displayBillingAddressPage();
        } });

    }
  }







  function displayBillingAddressPage() {
    var promise = $.billing_address.render();
    Alloy.Router.showActivityIndicator(promise);
    promise.done(function () {
      currentBasket.setCheckoutStatus('billingAddress');
      hideCurrentPage();
      currentPage = 2;
      $.checkout_tab_group.children[2].show();
      analytics.fireScreenEvent({
        screen: _L('Billing Address') });

    }).fail(function () {
      currentBasket.setCheckoutStatus(currentBasket.getLastCheckoutStatus(), {
        silent: true });

    });
    return promise;
  }







  function displayShippingMethodPage() {

    var promise = $.shipping_method.render();
    Alloy.Router.showActivityIndicator(promise);
    promise.done(function () {
      hideCurrentPage();
      currentPage = 3;
      $.checkout_tab_group.children[3].show();
      analytics.fireScreenEvent({
        screen: _L('Shipping Methods') });

    }).fail(function () {
      currentBasket.setCheckoutStatus(currentBasket.getLastCheckoutStatus(), {
        silent: true });

    });
    return promise;
  }







  function displayPaymentsPage() {
    var deferred = new _.Deferred();

    if (!currentBasket.getOrderNumber() || currentBasket.getOrderNumber() === 'null') {
      createOrder(deferred, initializeAndDisplayPaymentsPage);
    } else {
      initializeAndDisplayPaymentsPage(deferred);
    }
    return deferred.promise();
  }








  function initializeAndDisplayPaymentsPage(deferred, model) {
    $.payments.init().done(function () {
      $.payments.render();
    });
    hideCurrentPage();
    currentPage = 4;
    $.checkout_tab_group.children[4].show();

    Alloy.eventDispatcher.trigger('payments_active', {
      payments_active: true });

    analytics.fireScreenEvent({
      screen: _L('Payments') });

    deferred.resolve();
  }







  function setStoreBillingAddress() {
    var deferred = new _.Deferred();
    if (Alloy.CFG.collect_billing_address) {
      deferred.resolve();
    } else {
      var billingAddress = currentBasket.getBillingAddress();
      if (billingAddress == null) {

        var storeAddress = Alloy.Models.storeInfo.constructStoreAddress();
        $.billing_address.setAddress(storeAddress, null, false).done(function () {
          deferred.resolve();
        }).fail(function () {
          deferred.reject();
        });
      } else {
        deferred.resolve();
      }
    }
    return deferred.promise();
  }








  function createOrder(deferred, onSuccessClosure) {
    logger.info('createOrder called');

    var deferred1 = new _.Deferred();
    Alloy.Router.showActivityIndicator(deferred1);

    setStoreBillingAddress().done(function () {

      currentBasket.createOrder().done(function (model, params, options) {

        if (currentBasket.getOrderNo() && currentBasket.canEnableCheckout()) {
          onSuccessClosure(deferred, model);
        } else {
          hideCurrentPage();

          currentBasket.setCheckoutStatus('cart');
          deferred.resolve();
        }
      }).fail(function (model) {

        var message = model.getFaultMessage();
        if (!message) {
          message = _L('Unable to create order.');
        }
        notify(message, {
          preventAutoClose: true });

        hideCurrentPage();
        currentBasket.setCheckoutStatus('cart');
        deferred.reject();
      }).always(function () {
        deferred1.resolve();
      });
    }).fail(function () {
      currentBasket.setCheckoutStatus(currentBasket.getLastCheckoutStatus(), {
        silent: true });

      deferred1.resolve();
    });
  }







  function displayConfirmationPage() {
    var deferred = new _.Deferred();
    initializeAndDisplayConfirmationPage(Alloy.Models.order, deferred);
    return deferred.promise();
  }








  function initializeAndDisplayConfirmationPage(order, deferred) {

    hideCurrentPage();
    currentPage = 5;
    $.checkout_tab_group.children[5].show();

    $.confirmation.render(order);
    analytics.fireScreenEvent({
      screen: _L('Confirmation') });

    deferred.resolve();
  }







  function handlePageSelection(event) {

    hideAllPages();

    var promise;
    switch (event.page) {
      case 'cart':
        promise = displayCartPage();
        break;
      case 'shippingAddress':
        promise = displayShippingAddressPage();
        break;
      case 'billingAddress':
        promise = displayBillingAddressPage();
        break;
      case 'shippingMethod':
        promise = displayShippingMethodPage();
        break;
      case 'payThroughWeb':
        promise = payThroughWeb();
        break;
      case 'payments':
        promise = displayPaymentsPage();
        break;
      case 'confirmation':
        promise = displayConfirmationPage();
        break;}

    if (promise) {
      promise.done(function () {
        $.header.updateTabStates();

        verifyPaymentTerminalConnection(event.page);
      });
    }
  }






  function canCheckout() {
    return currentBasket.getProductItems().length > 0 && !savedItemsDisplayed && !wishListItemsDisplayed;
  }







  function createAccount(order) {
    var orderModel = Alloy.createModel('baskets', order.toJSON());
    var createDialog = Alloy.Dialog.showCustomDialog({
      controllerPath: 'checkout/confirmation/createAccount',
      initOptions: {
        order: order },

      continueEvent: 'createAccount:dismiss',
      continueFunction: function () {
        $.stopListening(createDialog, 'createAccount:create');
      } });

    createDialog.focusFirstField();


    $.listenTo(createDialog, 'createAccount:create', function (customer_info, address, saveAddress) {
      var newCustomer = Alloy.createModel('customer');
      if (saveAddress) {
        var nickname = getAddressNickname(address.getCity(), address.getStateCode());
        address.setAddressId(nickname, {
          silent: true });



        if (address.getStateCode() == 'null') {
          address.unset('state_code', {
            silent: true });

        }

        address.setCountryCode(toCountryCode(address.getCountryCode()), {
          silent: true });

        if (!address.getAddress2()) {
          address.setAddress2('', {
            silent: true });

        }
        if (!address.getPhone()) {
          address.setPhone('', {
            silent: true });

        }
      }

      var data = {
        c_employee_id: currentAssociate.getEmployeeId(),
        c_employee_passcode: currentAssociate.getPasscode(),
        c_store_id: Alloy.CFG.store_id,
        c_orderNo: orderModel.getOrderNumber() };

      if (saveAddress) {
        data.c_address = address.toJSON();
      }

      var deferred = new _.Deferred();
      Alloy.Router.showActivityIndicator(deferred);
      newCustomer.register(customer_info, data).done(function () {
        analytics.fireAnalyticsEvent({
          category: _L('Users'),
          action: _L('Create User'),
          label: customer_info.username });

        analytics.fireUserEvent({
          category: _L('Users'),
          action: _L('Create User'),
          userId: customer_info.username });

        currentAssociate.loginAssociate({
          employee_id: currentAssociate.getEmployeeId(),
          passcode: currentAssociate.getPasscode() }).
        done(function () {
          createDialog.dismiss();
          notify(_L('Account successfully created.'));
        }).fail(function (data) {
          var failMsg = _L('Unable to create account.');
          if (data && data.faultDescription) {
            failMsg = data.faultDescription;
          } else if (data && data.get('httpStatus') != 200 && data.get('fault')) {
            failMsg = data.get('fault').message;
          }
          createDialog.showErrorMessage(failMsg);
        }).always(function () {
          deferred.resolve();
        });
      }).fail(function (model, options, params) {
        createDialog.showErrorMessage(model ? model.get('fault') : null);
        deferred.resolve();
      });
    });
  }






  function abandonOrder() {
    logger.info('abandonOrder called');

    hideCurrentPage();

    Alloy.eventDispatcher.trigger('payments_active', {
      payments_active: false });

  }






  function moveFromCartToShippingAddress() {

    var promise = currentBasket.validateCartForCheckout({
      c_employee_id: Alloy.Models.associate.getEmployeeId() },
    {
      silent: true });

    Alloy.Router.showActivityIndicator(promise);
    promise.done(function () {

      if (currentBasket.canEnableCheckout()) {
        handlePageSelection({
          page: 'shippingAddress' });


      } else {
        currentBasket.setCheckoutStatus('cart');

      }
    }).fail(function () {
      currentBasket.setCheckoutStatus(currentBasket.getLastCheckoutStatus(), {
        silent: true });

      updateCheckoutButton();
    });
  }






  function updateCheckoutButton() {
    if (currentBasket.getCheckoutStatus() === undefined) {
      return;
    }

    if (currentBasket.getCheckoutStatus() == 'cart') {
      $.checkout_button.show();
      $.checkout_button.setEnabled(canCheckout());
    } else {
      $.checkout_button.hide();
    }
  }







  function payThroughWeb() {
    logger.info('payThroughWeb called');
    var deferred = new _.Deferred();
    createOrder(deferred, displayPayThroughWebModal);
    return deferred.promise();
  }








  function displayPayThroughWebModal(deferred, model) {
    logger.info('displayPayThroughWebModal called');
    var webOrder = Alloy.createModel('storeWebOrder');

    var token = Ti.Utils.sha256(currentAssociate.getFirstName() + currentAssociate.getLastName() + Date.now() + Ti.Platform.createUUID());

    tokenHash[token] = currentBasket.getOrderNo();

    webOrder.save({
      token: token,
      order_no: currentBasket.getOrderNo() }).
    done(function () {
      analytics.fireScreenEvent({
        screen: _L('Pay Through Web') });

      webOrderDeferred = new _.Deferred();
      Alloy.Router.showActivityIndicator(webOrderDeferred);
      var params = {
        token: token,
        appUrl: Alloy.CFG.app_url_scheme,
        timeout: Alloy.CFG.session_timeout,
        sessionCurrency: Alloy.CFG.appCurrency };

      params = _.extend(params, eaUtils.getCurrencyConfiguration());
      var urlEnd = eaUtils.buildRequestUrl('EACheckout-StartWebPayment', params);
      var url = eaUtils.buildStorefrontURL('https', urlEnd);
      openSafariDialog(url);
    }).fail(function () {
      notify(_L('Failed to create order. Please try again.'), {
        preventAutoClose: true });

    });
    deferred.resolve();
  }







  function openSafariDialog(url) {
    logger.info('openSafariDialog with url: ' + url);
    safariDialog = require('ti.safaridialog');
    if (safariDialog) {
      safariDialog.addEventListener('open', onSafariOpen);
      safariDialog.addEventListener('close', onSafariClose);

      safariDialog.open({
        url: url,
        entersReaderIfAvailable: false,
        tintColor: Alloy.Styles.payThroughWeb.navigationTint });

    } else {
      logger.error('ti.safaridialog is not supported');
    }
  }








  function onSafariClose(event) {
    logger.info('onSafariClose: ' + JSON.stringify(event));
    safariDialog.removeEventListener('close', onSafariClose);


    if (processingWebOrder) {


      cancelWebOrder(currentBasket.getOrderNo()).always(function () {
        webOrderDeferred.resolve();
      });
      processingWebOrder = false;
    }

    safariDialog = null;
  }







  function onSafariOpen(event) {
    logger.info('onSafariOpen: ' + JSON.stringify(event));
    safariDialog.removeEventListener('open', onSafariOpen);


    processingWebOrder = true;
  }







  function closeSafariDialog() {
    logger.info('closeSafariDialog called');
    var closing = false;
    if (safariDialog) {

      var opened = safariDialog.opened;
      logger.info('ti.safaridialog.opened ' + opened);

      if (opened) {


        processingWebOrder = false;

        logger.info('closeSafariDialog closing open dialog');
        safariDialog.close();
        closing = true;
      }
    }
    return closing;
  }







  function verifyPaymentTerminalConnection(page) {
    if (!Alloy.CFG.devices.verify_payment_terminal_connection_at_checkout || Alloy.CFG.devices.payment_terminal_module == 'webDevice') {
      return;
    }


    if (_.indexOf(['verifoneDevice', 'adyenDevice'], Alloy.CFG.devices.payment_terminal_module, false) != -1) {
      switch (page) {
        case 'shippingAddress':
        case 'billingAddress':
        case 'shippingMethod':
        case 'payments':
          Alloy.Router.checkPaymentDevice();
          break;}

    }
  }







  function showPaymentSignature(args) {
    if (!paymentTerminal.needsSignature) {
      _.each(args.getPaymentInstruments(), function (payment) {
        if (paymentTerminal.signaturePromptedViaDevice) {

          payment.c_eaRequireSignature = false;
        } else if (payment.c_eaIsContactless && (Alloy.CFG.payment.nfc_signature_threshold_amount > payment.amount || Alloy.CFG.payment.nfc_signature_threshold_amount == -1.0)) {
          payment.c_eaRequireSignature = false;
        } else if (!payment.c_eaIsContactless && (Alloy.CFG.payment.swipe_signature_threshold_amount > payment.amount || Alloy.CFG.payment.swipe_signature_threshold_amount == -1.0)) {
          payment.c_eaRequireSignature = false;
        }
      });
    }


    var paymentSignature = Alloy.Dialog.showCustomDialog({
      controllerPath: 'checkout/payments/paymentSignature',
      initOptions: args,
      continueEvent: 'signature:dismiss',
      continueFunction: function () {

        if (!customerIsLoggedIn && !isKioskMode()) {
          createAccount(args);
        }
        if (isKioskMode()) {
          Alloy.Kiosk.order_complete_timer = setTimeout(function () {
            Alloy.Kiosk = {};
            Alloy.Router.associateLogout();
          }, Alloy.CFG.kiosk_mode.order_complete_reset_delay);
        }
      } });

    paymentSignature.once('signature:accepted', function () {
      currentBasket.setCheckoutStatus('confirmation');
    });

    if (!paymentTerminal.needsSignature) {
      currentBasket.setCheckoutStatus('confirmation');
    }
  }






  function dismissPaymentTerminal() {
    if ($.payment_terminal) {
      $.payment_terminal.dismiss();
    }
  }






  function handleOrderAbandoned() {
    abandonOrder();
    currentBasket.setCheckoutStatus('cart');
    updateCheckoutButton();
  }







  function showCheckSignature(image) {
    logger.info('showCheckSignature ' + image);

    $.confirm_signature = Alloy.Dialog.showCustomDialog({
      controllerPath: 'checkout/payments/confirmSignature',
      initOptions: image,
      cancelEvent: 'confirmSignature:dismiss',
      cancelFunction: function (event) {
        $.confirm_signature = null;

        if (event && event.declined) {

          paymentTerminal.signatureDeclined({
            image: image });

        }
      },
      continueEvent: 'confirmSignature:accepted',
      continueFunction: function (event) {
        $.confirm_signature = null;
        if (event) {
          logger.info('signature accepted ' + event.image);

          paymentTerminal.signatureApproved({
            image: event.image });

        } else {

          paymentTerminal.signatureDeclined({
            image: image });

        }
      } });

  }






  function showPromptSignature() {
    logger.info('showPromptSignature called');

    $.prompt_signature_dialog = Alloy.Dialog.showCustomDialog({
      controllerPath: 'checkout/payments/promptSignatureDialog',
      cancelEvent: 'promptSignatureDialog:dismiss',
      cancelFunction: function (event) {
        $.prompt_signature_dialog = null;

        if (!event) {

          paymentTerminal.cancelPayment(true);
          setTimeout(function () {
            logger.info('cancelServerTransaction (showPromptSignature/cancelFunction)');
            paymentTerminal.cancelServerTransaction({
              order_no: Alloy.Models.basket.getOrderNo() });

          }, 5000);
        }
      },
      continueEvent: 'promptSignatureDialog:accept_signature',
      continueFunction: function (event) {
        $.prompt_signature_dialog = null;
        if (event) {
          logger.info('signature accepted ' + event.image);

          paymentTerminal.signatureCollected({
            image: event.image });

        } else {

          paymentTerminal.cancelPayment(true);
          setTimeout(function () {
            logger.info('cancelServerTransaction (showPromptSignature/continueFunction)');
            paymentTerminal.cancelServerTransaction({
              order_no: Alloy.Models.basket.getOrderNo() });

          }, 5000);
        }
      } });

  }






  function hideSignature() {
    logger.info('hideSignature called');
    if ($.confirm_signature) {
      $.confirm_signature.dismiss();
    }
    if ($.prompt_signature_dialog) {
      $.prompt_signature_dialog.dismiss();
    }
  }









  function cancelWebOrder(orderNo, action) {
    logger.info('cancelWebOrder called');
    var deferred = new _.Deferred();
    var retryFailure = function () {
      Alloy.Dialog.showConfirmationDialog({
        messageString: _L('The order needs to be cancelled before you can continue. Please try again.'),
        titleString: action == 'timeout' ? _L('The request timed out.') : _L('Unable to cancel order.'),
        okButtonString: _L('Retry'),
        hideCancel: true,
        okFunction: function () {
          removeNotify();
          var promise = cancelWebOrder(orderNo, action);
          Alloy.Router.showActivityIndicator(promise);
        } });

    };
    var orderDetails = Alloy.createModel('baskets');
    orderDetails.getOrder({
      order_no: orderNo }).
    done(function () {

      logger.info('order status ' + orderDetails.getStatus());
      if (orderDetails.isStatusCreated()) {
        currentBasket.abandonOrder(Alloy.Models.associate.getEmployeeId(), Alloy.Models.associate.getPasscode(), Alloy.CFG.store_id).done(function () {

          handleOrderAbandoned();
          deferred.resolve();
        }).fail(function () {
          deferred.reject();
          retryFailure();
        });
      } else if (action) {

        deferred.resolve();
        var message = '';
        logger.info('action ' + action);
        if (action == 'cancel') {
          message = _L('This transaction has already been cancelled.');
        } else if (action == 'confirmCancelledOrder') {
          message = _L('This transaction has been cancelled and payment cannot be applied.');
        }
        Alloy.Dialog.showAlertDialog({
          messageString: message });

      } else {
        logger.error('action ' + action + ' or order status incorrect');
        deferred.reject();
      }
    }).fail(function () {
      deferred.reject();
      retryFailure();
    });

    return deferred.promise();
  }







  function handleOrderCompletion(event) {

    dismissPaymentTerminal();
    showPaymentSignature(event);
  }







  function finishFetchOrder(orderNo) {
    currentBasket.trigger('change');
    currentBasket.trigger('basket_sync');

    var customerOrder = Alloy.createModel('baskets');
    logger.info('looking up order detail: ' + orderNo);
    customerOrder.getOrder({
      order_no: orderNo }).
    done(function () {

      Alloy.Models.order = this;
      Alloy.eventDispatcher.trigger('order_just_created', this);
    });
  }









  function onCheckoutButtonClick() {
    var index = _.indexOf(checkoutStates, currentBasket.getCheckoutStatus());
    if (index < checkoutStates.length - 1) {
      currentBasket.setCheckoutStatus(checkoutStates[index + 1]);
    } else {
      currentBasket.setCheckoutStatus(checkoutStates[0]);
    }
  }








  function openGCBalanceModal(args) {
    Alloy.Dialog.showCustomDialog({
      controllerPath: 'checkout/payments/gcBalanceDetails',
      initOptions: args,
      cancelEvent: 'gc_balance:dismiss',
      cancelFunction: function () {
        dismissPaymentTerminal();
      },
      continueEvent: 'gc_balance:continue',
      continueFunction: function () {
        $.payments.applyGCPayment({
          track_1: args.track_1,
          track_2: args.track_2,
          order_no: args.order_no },
        args.toApply);
      } });

  }









  _.extend($, exports);
}

module.exports = Controller;