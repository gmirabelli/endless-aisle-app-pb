var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'customer/components/orderSearchResult';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};




  $.orders = Alloy.createCollection('customerOrderHistory');


  $.__views.search_results = Ti.UI.createView(
  { top: 55, layout: "vertical", id: "search_results" });

  $.__views.search_results && $.addTopLevelView($.__views.search_results);
  $.__views.__alloyId162 = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.color.background.medium, width: "100%", height: 44, layout: "horizontal", id: "__alloyId162" });

  $.__views.search_results.add($.__views.__alloyId162);
  $.__views.breadcrumbs_back_button = Ti.UI.createButton(
  { image: Alloy.Styles.backButtonImage, width: 44, height: 44, style: Ti.UI.iOS.SystemButtonStyle.PLAIN, borderWidth: 1, borderColor: Alloy.Styles.color.border.medium, id: "breadcrumbs_back_button", accessibilityLabel: "breadcrumbs_back_button" });

  $.__views.__alloyId162.add($.__views.breadcrumbs_back_button);
  $.__views.__alloyId163 = Ti.UI.createView(
  { width: 1, backgroundColor: Alloy.Styles.color.background.dark, height: "100%", top: 0, left: 0, id: "__alloyId163" });

  $.__views.__alloyId162.add($.__views.__alloyId163);
  $.__views.search_results_count = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 20, font: Alloy.Styles.detailLabelFont, id: "search_results_count", accessibilityValue: "search_results_count" });

  $.__views.__alloyId162.add($.__views.search_results_count);
  $.__views.padding_one = Ti.UI.createView(
  { height: 5, id: "padding_one" });

  $.__views.search_results.add($.__views.padding_one);
  $.__views.results_contents = Ti.UI.createView(
  { layout: "vertical", height: Ti.UI.SIZE, id: "results_contents" });

  $.__views.search_results.add($.__views.results_contents);
  $.__views.lookup_window = Ti.UI.createView(
  { layout: "vertical", width: 655, height: 170, id: "lookup_window" });

  $.__views.results_contents.add($.__views.lookup_window);
  $.__views.order_lookup_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, top: 30, font: Alloy.Styles.headlineFont, textid: "_Order_Search", id: "order_lookup_label", accessibilityValue: "order_lookup_label" });

  $.__views.lookup_window.add($.__views.order_lookup_label);
  $.__views.search_form_container = Ti.UI.createView(
  { layout: "horizontal", height: 57, width: 653, id: "search_form_container" });

  $.__views.lookup_window.add($.__views.search_form_container);
  $.__views.search_textfield = Ti.UI.createTextField(
  { borderColor: Alloy.Styles.color.border.dark, borderWidth: 1, color: Alloy.Styles.color.text.dark, left: 0, top: 0, bottom: 10, width: 455, font: Alloy.Styles.textFieldFont, padding: { left: 18 }, height: 55, autocorrect: false, clearButtonMode: Ti.UI.INPUT_BUTTONMODE_ALWAYS, keyboardType: Ti.UI.KEYBOARD_TYPE_EMAIL, autocapitalization: Ti.UI.TEXT_AUTOCAPITALIZATION_NONE, returnKeyType: Ti.UI.RETURNKEY_SEARCH, id: "search_textfield", accessibilityLabel: "order_search_textfield" });

  $.__views.search_form_container.add($.__views.search_textfield);
  $.__views.padding_two = Ti.UI.createView(
  { width: 18, id: "padding_two" });

  $.__views.search_form_container.add($.__views.padding_two);
  $.__views.search_button = Ti.UI.createButton(
  { width: 156, right: 0, top: 0, height: 55, backgroundImage: Alloy.Styles.primaryButtonImage, font: Alloy.Styles.bigButtonFont, color: Alloy.Styles.buttons.primary.color, titleid: "_Search", id: "search_button", accessibilityLabel: "search_order_button" });

  $.__views.search_form_container.add($.__views.search_button);
  $.__views.search_results_container = Ti.UI.createView(
  { width: "100%", id: "search_results_container" });

  $.__views.lookup_window.add($.__views.search_results_container);
  $.__views.results_error = Ti.UI.createLabel(
  { width: "100%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.red, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.headlineFont, id: "results_error", accessibilityLabel: "results_error" });

  $.__views.search_results_container.add($.__views.results_error);
  $.__views.line_separator = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.color.background.separatorColor, width: "100%", height: 1, top: 0, id: "line_separator" });

  $.__views.results_contents.add($.__views.line_separator);
  $.__views.results_table_container = Ti.UI.createView(
  { layout: "vertical", height: 475, width: "65%", top: 0, id: "results_table_container" });

  $.__views.results_contents.add($.__views.results_table_container);
  $.__views.history_container = Ti.UI.createTableView(
  { id: "history_container", separatorStyle: "transparent" });

  $.__views.results_table_container.add($.__views.history_container);
  var __alloyId167 = Alloy.Collections['$.orders'] || $.orders;function __alloyId168(e) {if (e && e.fromAdapter) {return;}var opts = __alloyId168.opts || {};var models = filterOrders(__alloyId167);var len = models.length;var rows = [];for (var i = 0; i < len; i++) {var __alloyId164 = models[i];__alloyId164.__transform = transformOrder(__alloyId164);var __alloyId166 = Alloy.createController('customer/components/historyResultRow', { $model: __alloyId164, __parentSymbol: __parentSymbol });
      rows.push(__alloyId166.getViewEx({ recurse: true }));
    }$.__views.history_container.setData(rows);};__alloyId167.on('fetch destroy change add remove reset', __alloyId168);exports.destroy = function () {__alloyId167 && __alloyId167.off('fetch destroy change add remove reset', __alloyId168);};




  _.extend($, $.__views);










  var ordersCollection = Alloy.Collections.customerOrderHistory;
  var logger = require('logging')('customer:components:orderSearchResult', getFullControllerPath($.__controllerPath));
  var toCurrency = require('EAUtils').toCurrency;
  var returnToShoppingOrCart = require('EAUtils').returnToShoppingOrCart;
  var filteredOrders = [];




  $.history_container.addEventListener('singletap', handleOrderSelected);
  $.breadcrumbs_back_button.addEventListener('singletap', returnToShoppingOrCart);
  $.search_textfield.addEventListener('return', handleOrderSearch);
  $.search_button.addEventListener('click', handleOrderSearch);




  exports.init = init;
  exports.deinit = deinit;











  function init(options) {
    logger.info('Calling INIT');
    var deferred = new _.Deferred();
    $.orders.reset([]);
    hideErrorMessage();
    $.search_textfield.setValue(options.searchPhrase);
    $.search_results_count.setText(String.format(_L('%d Orders'), 0));
    var errorMsg = String.format(_L('Could not retrieve orders for id or email \'%s\'.'), options.searchPhrase);
    Alloy.Router.showActivityIndicator(deferred);
    ordersCollection.search({
      customerEmailOrOrderNo: options.searchPhrase }).
    done(function () {
      $.orders.once('reset', function () {
        disableSelectionOnRows();
      });
      filteredOrders = filterOrders(ordersCollection);
      $.orders.reset(filteredOrders);
      $.search_results_count.setText(String.format(_L('%d Orders'), filteredOrders.length));
      if (filteredOrders && filteredOrders.length == 0) {
        deferred.reject({
          message: errorMsg });

        showErrorMessage(errorMsg);
      } else if (filteredOrders && filteredOrders.length == 1) {
        hideErrorMessage();
        handleSingleOrder(filteredOrders[0].get('orderNo'), deferred);
      } else {
        hideErrorMessage();
        deferred.resolve();
      }
    }).fail(function (error) {
      if (error && error.message) {
        logger.info(error.message);
        showErrorMessage(error.message);
        deferred.reject({
          message: error.message });

      } else {
        logger.info(String.format('Could not retrieve orders for id or email \'%s\'.', options.searchPhrase));
        showErrorMessage(errorMsg);
        deferred.reject({
          message: errorMsg });

      }
    });

    return deferred.promise();
  }






  function deinit() {
    logger.info('Calling DESTROY');
    $.history_container.removeEventListener('singletap', handleOrderSelected);
    $.breadcrumbs_back_button.removeEventListener('singletap', returnToShoppingOrCart);
    $.search_textfield.removeEventListener('return', handleOrderSearch);
    $.search_button.removeEventListener('click', handleOrderSearch);
    $.stopListening();
    $.destroy();
  }










  function showOrderDetailsDialog(orderId) {
    var promise = Alloy.Models.customerOrder.getOrder({
      order_no: orderId });

    Alloy.Router.showActivityIndicator(promise);
    promise.done(function (evt) {
      if (!evt || evt && !evt.noResult) {
        Alloy.Dialog.showCustomDialog({
          controllerPath: 'customer/components/order',
          continueEvent: 'order_history:dismiss' });

      }
    }).fail(function () {
      notify(String.format(_L('Could not retrieve order \'%s\'.'), orderId), {
        preventAutoClose: true });

    });
    return promise;
  }







  function handleSingleOrder(searchPhrase, deferred) {
    var errorMsg = String.format(_L('Could not retrieve orders for id or email \'%s\'.'), searchPhrase);
    showOrderDetailsDialog(searchPhrase).done(function (evt) {
      if (evt && evt.noResult) {
        logger.info(String.format('Could not retrieve orders for id or email \'%s\'.', searchPhrase));
        deferred.reject({
          message: errorMsg });

        showErrorMessage(errorMsg);
      } else {
        hideErrorMessage();
        deferred.resolve({
          singleOrder: true });

        $.search_results_count.setText(String.format(_L('%d Order'), 1));
      }
    });
  }





  function disableSelectionOnRows() {
    logger.info('disableSelectionOnRows');
    if ($.history_container.getSections().length >= 1) {
      var children = $.history_container.getSections()[0].getRows();

      _.each(children, function (child) {
        child.setSelectionStyle(Ti.UI.iOS.TableViewCellSelectionStyle.NONE);
      });
    }
  }








  function filterOrders(collection) {
    logger.info('filterOrders' + JSON.stringify(collection.models[0]));
    var filter = collection.filter(function (model) {
      return model.get('status') !== 'FAILED' && model.get('status') !== 'CREATED';
    });
    return filter;
  }








  function transformOrder(model) {
    logger.info('transform Order');
    var imageUrl = model.get('imageURL').replace('https', 'http');
    return {
      imageUrl: encodeURI(imageUrl),
      creationDate: model.get('creationDate'),
      order_status: _L(model.get('status')),
      totalNetPrice: toCurrency(model.getTotalNetPrice(), model.getCurrencyCode()),
      orderNo: model.get('orderNo') };

  }







  function showErrorMessage(errorText) {
    $.results_error.setText(errorText);
    $.search_results_container.setVisible(true);
  }






  function hideErrorMessage() {
    $.results_error.setText('');
    $.search_results_container.setVisible(false);
  }








  function handleOrderSearch() {
    var searchTerm = $.search_textfield.getValue();
    if (searchTerm && searchTerm.length > 0) {
      init({
        searchPhrase: $.search_textfield.getValue() });

    } else {
      showErrorMessage(_L('You must provide either an order ID or a customer email for an order search.'));
    }
  }






  function handleOrderSelected(event) {
    if (filteredOrders && filteredOrders.length > 0) {
      var order = filteredOrders[event.index];
      if (order) {
        showOrderDetailsDialog(order.get('orderNo'));
      }
    }
  }




  $.search_textfield.setHintText(_L('Enter Order ID or Customer Email'));









  _.extend($, exports);
}

module.exports = Controller;