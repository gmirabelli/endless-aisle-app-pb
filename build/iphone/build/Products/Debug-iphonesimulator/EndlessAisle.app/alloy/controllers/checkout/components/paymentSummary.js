var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'checkout/components/paymentSummary';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};




  $.payments = Alloy.createCollection('payment');


  $.__views.entered_payments = Ti.UI.createView(
  { layout: "vertical", left: 0, top: 10, height: Ti.UI.SIZE, id: "entered_payments" });

  $.__views.entered_payments && $.addTopLevelView($.__views.entered_payments);
  $.__views.entered_payments_label_container = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.color.background.medium, borderWidth: 1, borderColor: Alloy.Styles.color.border.darker, top: 0, left: 0, width: "100%", height: 30, layout: "horizontal", id: "entered_payments_label_container" });

  $.__views.entered_payments.add($.__views.entered_payments_label_container);
  $.__views.entered_payments_label = Ti.UI.createLabel(
  { width: "100%", height: 28, color: Alloy.Styles.accentColor, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, top: 2, font: Alloy.Styles.smallAccentFont, left: 20, textid: "_Payments_Heading", id: "entered_payments_label", accessibilityValue: "entered_payments_label" });

  $.__views.entered_payments_label_container.add($.__views.entered_payments_label);
  $.__views.payment_details_container = Ti.UI.createView(
  { top: 10, layout: "vertical", height: Ti.UI.SIZE, id: "payment_details_container" });

  $.__views.entered_payments.add($.__views.payment_details_container);
  $.__views.payment_table = Ti.UI.createTableView(
  { touchEnabled: true, id: "payment_table", separatorStyle: "transparent" });

  $.__views.payment_details_container.add($.__views.payment_table);
  var __alloyId51 = Alloy.Collections['$.payments'] || $.payments;function __alloyId52(e) {if (e && e.fromAdapter) {return;}var opts = __alloyId52.opts || {};var models = __alloyId51.models;var len = models.length;var rows = [];for (var i = 0; i < len; i++) {var __alloyId48 = models[i];__alloyId48.__transform = transformPayments(__alloyId48);var __alloyId50 = Alloy.createController('checkout/components/paymentSummaryRow', { $model: __alloyId48, __parentSymbol: __parentSymbol });
      rows.push(__alloyId50.getViewEx({ recurse: true }));
    }$.__views.payment_table.setData(rows);};__alloyId51.on('fetch destroy change add remove reset', __alloyId52);exports.destroy = function () {__alloyId51 && __alloyId51.off('fetch destroy change add remove reset', __alloyId52);};




  _.extend($, $.__views);










  var currentBasket = Alloy.Models.basket;
  var toCurrency = require('EAUtils').toCurrency;
  var currencyCode;
  var logger = require('logging')('checkout:components:paymentSummary', getFullControllerPath($.__controllerPath));





  $.listenTo(Alloy.eventDispatcher, 'order_just_created', function (order) {
    logger.info('responding to order_just_created');
    render(order);
  });





  $.listenTo(currentBasket, 'change:payment_details reset:payment_details', function (type, model, something, options) {
    logger.info('responding to change:payment_details or reset:payment_details');
    var payments = currentBasket.getPaymentDetails();
    deinitRows();
    if (payments && payments.length > 0) {

      setCanDeletePayment(currentBasket, true);
      $.payments.reset(payments);
    } else {
      $.payments.reset([]);
    }
  });

  $.listenTo(currentBasket, 'change:checkout_status', function () {
    var checkoutStatus = currentBasket.getCheckoutStatus();


    if (checkoutStatus == 'payments' || checkoutStatus == 'confirmation' && Alloy.CFG.payment_entry != 'pos') {
      this.getView().show();
      this.getView().setHeight(289);
    } else {
      this.getView().hide();
      this.getView().setHeight(0);
    }
  });




  exports.render = render;
  exports.deinit = deinit;










  function render(model) {
    currencyCode = model.getCurrencyCode();
    setCanDeletePayment(model, false);
    deinitRows();
    $.payments.reset(model.getPaymentInstruments());
  }






  function deinit() {
    logger.info('DEINIT called');

    $.stopListening();
    deinitRows();
    $.destroy();
  }










  function transformPayments(model) {
    if (model.isCreditCard()) {
      return {
        payment: (model.getCreditCardType() ? model.getCreditCardType() + ' ' : '') + model.getLastFourDigits(),
        amount: '- ' + toCurrency(model.getAmountAuth(), currencyCode) };

    } else if (model.isGiftCard()) {
      return {
        payment: 'Gift Card ' + model.getLastFourDigits(),
        amount: '- ' + toCurrency(model.getAmountAuth(), currencyCode) };

    } else {
      return {
        payment: '',
        amount: '' };

    }
  }





  function deinitRows() {

    if ($.payment_table.getSections().length > 0) {
      _.each($.payment_table.getSections()[0].getRows(), function (row) {
        row.deinit();
      });
    }
  }








  function setCanDeletePayment(order, canDelete) {
    _.each(order.getPaymentDetails(), function (payment) {
      payment.can_delete = canDelete;
    });
  }









  _.extend($, exports);
}

module.exports = Controller;