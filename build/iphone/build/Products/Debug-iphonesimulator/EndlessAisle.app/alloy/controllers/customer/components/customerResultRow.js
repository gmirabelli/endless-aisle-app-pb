var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
		delete obj[key];
	}
	return arg;
}

function Controller() {

	require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'customer/components/customerResultRow';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};







	$.__views.customer_result_row = Ti.UI.createTableViewRow(
	{ selectionStyle: Ti.UI.iOS.TableViewCellSelectionStyle.NONE, id: "customer_result_row" });

	$.__views.customer_result_row && $.addTopLevelView($.__views.customer_result_row);
	$.__views.customer_result_row_container = Ti.UI.createView(
	{ layout: "horizontal", width: 889, height: 127, id: "customer_result_row_container" });

	$.__views.customer_result_row.add($.__views.customer_result_row_container);
	$.__views.name_address = Ti.UI.createView(
	{ left: 40, layout: "vertical", height: 127, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, width: 355, id: "name_address" });

	$.__views.customer_result_row_container.add($.__views.name_address);
	$.__views.name_label = Ti.UI.createLabel(
	{ width: 355, height: 20, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, top: 10, font: Alloy.Styles.calloutCopyFont, ellipsize: true, wordWrap: false, id: "name_label", text: $model.__transform.name, accessibilityValue: "name" });

	$.__views.name_address.add($.__views.name_label);
	$.__views.address_display = Ti.UI.createLabel(
	{ width: 355, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, ellipsize: true, wordWrap: false, id: "address_display", accessibilityValue: "address_display" });

	$.__views.name_address.add($.__views.address_display);
	$.__views.email_phone = Ti.UI.createView(
	{ layout: "vertical", height: 127, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, top: 0, width: "35%", id: "email_phone" });

	$.__views.customer_result_row_container.add($.__views.email_phone);
	$.__views.email_label = Ti.UI.createLabel(
	{ width: 355, height: 20, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, font: Alloy.Styles.detailValueFont, ellipsize: true, wordWrap: false, top: 20, id: "email_label", text: $model.__transform.email, accessibilityValue: "email" });

	$.__views.email_phone.add($.__views.email_label);
	$.__views.__alloyId141 = Ti.UI.createLabel(
	{ width: 355, height: 20, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, font: Alloy.Styles.detailValueFont, ellipsize: true, wordWrap: false, text: $model.__transform.phone, accessibilityValue: "phone", id: "__alloyId141" });

	$.__views.email_phone.add($.__views.__alloyId141);
	$.__views.button_view = Ti.UI.createView(
	{ layout: "vertical", height: 127, width: "20%", id: "button_view" });

	$.__views.customer_result_row_container.add($.__views.button_view);
	$.__views.profile_button = Ti.UI.createButton(
	{ backgroundImage: Alloy.Styles.secondaryButtonImage, color: Alloy.Styles.buttons.secondary.color, font: Alloy.Styles.buttonFont, width: 118, height: 35, top: 20, right: 40, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, titleid: "_Profile", id: "profile_button", accessibilityValue: "profile_button" });

	$.__views.button_view.add($.__views.profile_button);
	$.__views.login_button = Ti.UI.createButton(
	{ color: Alloy.Styles.buttons.primary.color, backgroundImage: Alloy.Styles.primaryButtonImage, font: Alloy.Styles.buttonFont, width: 118, height: 35, right: 40, top: 9, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, titleid: "_Shop_For", id: "login_button", accessibilityValue: "shop_for_button" });

	$.__views.button_view.add($.__views.login_button);
	exports.destroy = function () {};




	_.extend($, $.__views);










	var EAUtils = require('EAUtils');

	var symbolButtonTextLength = 5;




	var addresses = $model.getAddresses();
	var addressModel = Alloy.createModel('customerAddress', addresses[0]);

	var addressLabel = addressModel.getAddressDisplay('customer');
	$.address_display.setText(addressLabel);

	if (EAUtils.isSymbolBasedLanguage() && $.login_button.getTitle().length > symbolButtonTextLength) {
		$.login_button.setFont(Alloy.Styles.lineItemLabelFont);
		$.profile_button.setFont(Alloy.Styles.lineItemLabelFont);
		$.login_button.setWidth(150);
		$.profile_button.setWidth(150);
		$.button_view.setWidth('30%');
		$.email_phone.setWidth('25%');
	} else {
		$.login_button.setFont(Alloy.Styles.buttonFont);
		$.profile_button.setFont(Alloy.Styles.buttonFont);
		$.login_button.setWidth(118);
		$.profile_button.setWidth(118);
		$.button_view.setWidth('20%');
		$.email_phone.setWidth('35%');
	}









	_.extend($, exports);
}

module.exports = Controller;