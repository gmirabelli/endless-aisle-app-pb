var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'components/sessionTimeoutConfirmation';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.session_timeout_confirmation_window = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.color.background.transparent, id: "session_timeout_confirmation_window" });

  $.__views.session_timeout_confirmation_window && $.addTopLevelView($.__views.session_timeout_confirmation_window);
  $.__views.backdrop = Ti.UI.createView(
  { top: 0, left: 0, width: 1024, height: 768, opacity: 0.55, backgroundColor: Alloy.Styles.color.background.black, id: "backdrop" });

  $.__views.session_timeout_confirmation_window.add($.__views.backdrop);
  $.__views.session_timeout_confirmation_container = Ti.UI.createView(
  { layout: "vertical", width: "50%", height: Ti.UI.SIZE, backgroundColor: Alloy.Styles.color.background.white, id: "session_timeout_confirmation_container" });

  $.__views.session_timeout_confirmation_window.add($.__views.session_timeout_confirmation_container);
  $.__views.title = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.accentColor, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.titleFont, top: 20, id: "title", accessibilityValue: "kiosk_reset_title" });

  $.__views.session_timeout_confirmation_container.add($.__views.title);
  $.__views.message = Ti.UI.createLabel(
  { width: Ti.UI.FILL, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.messageFont, top: 50, left: 50, right: 50, id: "message", accessibilityValue: "kiosk_reset_message" });

  $.__views.session_timeout_confirmation_container.add($.__views.message);
  $.__views.session_timeout_button_container = Ti.UI.createView(
  { layout: "horizontal", height: Ti.UI.SIZE, width: Ti.UI.SIZE, top: 50, bottom: 20, id: "session_timeout_button_container" });

  $.__views.session_timeout_confirmation_container.add($.__views.session_timeout_button_container);
  $.__views.continue_button = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, backgroundImage: Alloy.Styles.buttons.secondary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.secondary.backgroundDisabledImage, color: Alloy.Styles.buttons.secondary.color, disabledColor: Alloy.Styles.buttons.secondary.disabledColor, height: 40, width: 200, titleid: "_Continue", id: "continue_button", accessibilityValue: "kiosk_reset_cancel_button" });

  $.__views.session_timeout_button_container.add($.__views.continue_button);
  $.__views.end_session_button = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.buttons.primary.color, disabledColor: Alloy.Styles.buttons.primary.disabledColor, backgroundImage: Alloy.Styles.buttons.primary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.primary.backgroundDisabledImage, height: 40, width: 200, left: 20, titleid: "_End_Session", id: "end_session_button", accessibilityValue: "kiosk_reset_ok_button" });

  $.__views.session_timeout_button_container.add($.__views.end_session_button);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var counter = 0;
  var timer = null;




  $.end_session_button.addEventListener('click', buttonHandler);
  $.continue_button.addEventListener('click', buttonHandler);




  exports.init = init;
  exports.deinit = deinit;
  exports.dismiss = dismiss;









  function init() {
    counter = Alloy.CFG.session_timeout_dialog_display_time / 1000;

    $.title.setText(_L('End Session'));
    $.message.setText(String.format(_L('End Session Message'), counter));

    timer = setInterval(function () {
      $.message.setText(String.format(_L('End Session Message'), --counter));
      if (counter == 0) {
        clearInterval(timer);
      }
    }, 1000);
  }






  function deinit() {
    $.end_session_button.removeEventListener('click', buttonHandler);
    $.continue_button.removeEventListener('click', buttonHandler);
    clearInterval(timer);
    timer = null;
    $.destroy();
  }









  function dismiss() {
    $.trigger('session_timeout_confirmation:end_session');
  }











  function buttonHandler(event) {
    clearInterval(timer);
    timer = null;
    if (event.source.id == $.end_session_button.id) {
      dismiss();
    } else if (event.source.id == $.continue_button.id) {
      $.trigger('session_timeout_confirmation:continue');
    }
  }









  _.extend($, exports);
}

module.exports = Controller;