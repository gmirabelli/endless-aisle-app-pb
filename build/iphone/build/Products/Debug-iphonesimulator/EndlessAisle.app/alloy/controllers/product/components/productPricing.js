var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'product/components/productPricing';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.pricing = Ti.UI.createView(
  { layout: "horizontal", height: Ti.UI.SIZE, top: 8, id: "pricing" });

  $.__views.pricing && $.addTopLevelView($.__views.pricing);
  $.__views.list_price_container = Ti.UI.createView(
  { height: Ti.UI.SIZE, width: 0, id: "list_price_container" });

  $.__views.pricing.add($.__views.list_price_container);
  $.__views.list_price = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, id: "list_price", accessibilityValue: "list_price" });

  $.__views.list_price_container.add($.__views.list_price);
  $.__views.sales_price = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, id: "sales_price", accessibilityValue: "sales_price" });

  $.__views.pricing.add($.__views.sales_price);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var eaUtils = require('EAUtils');
  var logger = require('logging')('product:components:productPricing', getFullControllerPath($.__controllerPath));




  exports.init = init;
  exports.deinit = deinit;
  exports.setPrice = setPrice;









  function init(args) {
    logger.info('init called');
    if (args && args.model) {
      $model = args.model;
      $.listenTo($model, 'change:selected_details', onSelectionsDetails);
      setPrice($model.getPrice());
    }
  }






  function deinit() {
    logger.info('deinit called');
    $.stopListening();
    $.destroy();
  }










  function onSelectionsDetails(model) {
    logger.info('onSelectionsDetails called');
    if ($model.isProductSelected()) {
      updateSalesPrice(model);
    }
  }







  function setPrice(new_price) {
    logger.info('setPrice called with ' + new_price);
    if (new_price) {
      $.sales_price.setText(eaUtils.toCurrency(new_price));
    }
  }







  function setListPrice(list_price) {
    logger.info('setListPrice called with ' + list_price);
    if (list_price) {
      $.list_price_container.setWidth(Ti.UI.SIZE);
      $.sales_price.setLeft(5);
      eaUtils.strikeThrough($.list_price, eaUtils.toCurrency(list_price));
    }
  }






  function clearListPrice() {
    logger.info('clearListPrice called');
    $.list_price_container.setWidth(0);
    $.sales_price.setLeft(0);
    $.list_price.setText('');
  }







  function updateSalesPrice(product) {
    logger.info('updateSalesPrice called with ' + JSON.stringify(product));
    var price = product.getPrice();
    var prices = product.getPrices();
    var promo_price = product.getPromoPrice();
    if (prices) {
      var listPrice = product.getListPrice();
      if (listPrice != price) {
        setListPrice(listPrice);
      } else {
        clearListPrice();
      }
    } else {
      logger.info('No price books retrieved');
    }
    if (promo_price && promo_price < price) {
      setPrice(promo_price);
      setListPrice(price);
    } else {

      setPrice(price);
    }
  }









  _.extend($, exports);
}

module.exports = Controller;