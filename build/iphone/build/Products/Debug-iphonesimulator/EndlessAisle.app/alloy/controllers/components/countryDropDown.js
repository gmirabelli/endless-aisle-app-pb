var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'components/countryDropDown';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.countrySelectionDropDown = Ti.UI.createView(
  { id: "countrySelectionDropDown" });

  $.__views.countrySelectionDropDown && $.addTopLevelView($.__views.countrySelectionDropDown);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var appSettings = require('appSettings');
  var logger = require('logging')('components:countryDropDown', getFullControllerPath($.__controllerPath));
  var countryConfig = require('config/countries').countryConfig;


  _.each(countryConfig, function (country) {
    country.localizedDisplayName = _L(country.displayName);
  });




  $.listenTo(Alloy.eventDispatcher, 'countryChange:selected', function () {
    _.each(countryConfig, function (country) {
      country.localizedDisplayName = _L(country.displayName);
    });
    initializeCountries();
  });




  exports.init = init;
  exports.deinit = deinit;
  exports.updateCountrySelectedItem = updateCountrySelectedItem;









  function init() {
    logger.info('init called');
    initializeCountries();
  }






  function deinit() {
    logger.info('deinit called');
    $.country_select.deinit();
    $.stopListening();
    $.destroy();
  }










  function updateCountrySelectedItem(selectedItem) {
    logger.info('updateCountrySelectedItem called');
    $.country_select.updateSelectedItem(selectedItem);
  }






  function initializeCountries() {
    logger.info('initializeCountries called');
    $.country_select = Alloy.createController('components/selectWidget', {
      valueField: 'value',
      textField: 'localizedDisplayName',
      values: countryConfig,
      messageWhenNoSelection: _L('Select Country'),
      selectListTitleStyle: {
        accessibilityValue: 'country_select_dropdown',
        width: Ti.UI.FILL,
        left: 10,
        color: Alloy.Styles.color.text.darkest,
        disabledColor: Alloy.Styles.color.text.light,
        disabledBackgroundColor: Alloy.Styles.color.background.light,
        font: Alloy.Styles.detailValueFont },

      selectListStyle: {
        width: Ti.UI.FILL,
        top: 0,
        color: Alloy.Styles.color.text.darkest,
        disabledColor: Alloy.Styles.color.text.light,
        disabledBackgroundColor: Alloy.Styles.color.background.light,
        font: Alloy.Styles.detailValueFont,
        selectedFont: Alloy.Styles.detailValueFont,
        unselectedFont: Alloy.Styles.detailValueFont } });



    $.countrySelectionDropDown.add($.country_select.getView());
    $.listenTo($.country_select, 'itemSelected', onCountrySelected);
  }










  function onCountrySelected(event) {
    $.trigger('country:change', {
      selectedCountry: event.item.value });

  }









  _.extend($, exports);
}

module.exports = Controller;