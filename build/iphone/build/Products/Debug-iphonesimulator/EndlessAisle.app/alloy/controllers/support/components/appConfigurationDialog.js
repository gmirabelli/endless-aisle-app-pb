var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
		delete obj[key];
	}
	return arg;
}

function Controller() {

	require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'support/components/appConfigurationDialog';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};







	$.__views.background_window = Ti.UI.createView(
	{ width: "100%", height: "100%", id: "background_window" });

	$.__views.background_window && $.addTopLevelView($.__views.background_window);
	$.__views.backdrop = Ti.UI.createView(
	{ width: "100%", height: "100%", opacity: 0.55, backgroundColor: Alloy.Styles.color.background.black, id: "backdrop" });

	$.__views.background_window.add($.__views.backdrop);
	$.__views.container = Ti.UI.createView(
	{ top: 20, layout: "vertical", width: "60%", height: "80%", backgroundColor: Alloy.Styles.color.background.white, id: "container" });

	$.__views.background_window.add($.__views.container);
	$.__views.title = Ti.UI.createLabel(
	{ width: Ti.UI.SIZE, height: "7%", color: Alloy.Styles.accentColor, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.titleFont, top: 0, textid: "_Configuration", id: "title", accessibilityValue: "title" });

	$.__views.container.add($.__views.title);
	$.__views.configs_wrapper = Ti.UI.createScrollView(
	{ top: 0, width: "100%", height: "80%", layout: "vertical", scrollingEnabled: true, showVerticalScrollIndicator: true, id: "configs_wrapper" });

	$.__views.container.add($.__views.configs_wrapper);
	$.__views.info_panel = Alloy.createController('support/components/buildInfoDataColumn', { id: "info_panel", __parentSymbol: $.__views.configs_wrapper });
	$.__views.info_panel.setParent($.__views.configs_wrapper);
	$.__views.buttons_wrapper = Ti.UI.createView(
	{ top: "1.5%", width: "80%", height: "10%", id: "buttons_wrapper" });

	$.__views.container.add($.__views.buttons_wrapper);
	$.__views.retry_button = Ti.UI.createButton(
	{ textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.buttons.primary.color, disabledColor: Alloy.Styles.buttons.primary.disabledColor, backgroundImage: Alloy.Styles.buttons.primary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.primary.backgroundDisabledImage, height: 40, width: "50%", titleid: "_Retry", id: "retry_button", accessibilityValue: "retry_button" });

	$.__views.buttons_wrapper.add($.__views.retry_button);
	exports.destroy = function () {};




	_.extend($, $.__views);











	var logger = require('logging')('components:appConfigurationDialog', getFullControllerPath($.__controllerPath));




	$.retry_button.addEventListener('click', dismiss);




	exports.init = init;
	exports.deinit = deinit;









	function init(args) {
		logger.info('init called');
	}






	function deinit() {
		logger.info('deinit called');
		$.retry_button.removeEventListener('click', dismiss);
	}









	function dismiss() {
		$.trigger('appConfigurationDialog:dismiss');
	}









	_.extend($, exports);
}

module.exports = Controller;