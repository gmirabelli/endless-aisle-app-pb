var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'support/testing';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.testing = Ti.UI.createView(
  { tabBarHidden: false, navBarHidden: false, id: "testing" });

  $.__views.testing && $.addTopLevelView($.__views.testing);
  $.__views.testing_main = Ti.UI.createView(
  { layout: "horizontal", width: "100%", height: "100%", id: "testing_main" });

  $.__views.testing.add($.__views.testing_main);
  $.__views.data_column = Ti.UI.createView(
  { layout: "vertical", width: "70%", height: "100%", id: "data_column" });

  $.__views.testing_main.add($.__views.data_column);
  $.__views.version_section = Ti.UI.createView(
  { top: 20, left: 20, height: Ti.UI.SIZE, layout: "vertical", id: "version_section" });

  $.__views.data_column.add($.__views.version_section);
  $.__views.results_title = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.accentColor, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailLabelFont, left: 0, textid: "_Testing_Results_", id: "results_title", accessibilityValue: "results_title" });

  $.__views.version_section.add($.__views.results_title);
  $.__views.test_output = Ti.UI.createTextArea(
  { borderWidth: 2, borderColor: Alloy.Styles.selectLists.optionsListBorderColor, borderRadius: 2, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, width: Ti.UI.FILL, right: 20, height: "85%", scrollable: true, editable: false, id: "test_output", accessibilityValue: "test_output" });

  $.__views.version_section.add($.__views.test_output);
  $.__views.clear_results = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.buttons.primary.color, disabledColor: Alloy.Styles.buttons.primary.disabledColor, backgroundImage: Alloy.Styles.buttons.primary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.primary.backgroundDisabledImage, height: 30, width: 220, font: Alloy.Styles.smallButtonFont, top: 15, bottom: 20, left: 0, titleid: "_Clear_Results", id: "clear_results", accessibilityValue: "clear_results" });

  $.__views.version_section.add($.__views.clear_results);
  clearOutput ? $.addListener($.__views.clear_results, 'click', clearOutput) : __defers['$.__views.clear_results!click!clearOutput'] = true;$.__views.vertical_border = Ti.UI.createView(
  { width: 2, backgroundColor: Alloy.Styles.color.border.darkest, id: "vertical_border" });

  $.__views.testing_main.add($.__views.vertical_border);
  $.__views.action_column = Ti.UI.createView(
  { layout: "vertical", width: "28%", height: "100%", id: "action_column" });

  $.__views.testing_main.add($.__views.action_column);
  $.__views.action_section = Ti.UI.createView(
  { top: 20, left: 20, height: Ti.UI.SIZE, layout: "vertical", id: "action_section" });

  $.__views.action_column.add($.__views.action_section);
  $.__views.action_title = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.accentColor, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailLabelFont, left: 0, textid: "_Actions_", id: "action_title", accessibilityValue: "action_title" });

  $.__views.action_section.add($.__views.action_title);
  $.__views.tests_text = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailValueFont, left: 4, top: 10, bottom: 10, textid: "_TestsText", id: "tests_text", accessibilityValue: "tests_text" });

  $.__views.action_section.add($.__views.tests_text);
  $.__views.model_tests_title = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailLabelFontSmall, left: 10, top: 5, textid: "_Model_Tests", id: "model_tests_title", accessibilityValue: "model_tests_title" });

  $.__views.action_section.add($.__views.model_tests_title);
  $.__views.test_names = Ti.UI.createTableView(
  { borderWidth: 2, borderColor: Alloy.Styles.selectLists.optionsListBorderColor, borderRadius: 2, width: 250, height: 300, scrollable: true, editable: false, rowHeight: 40, style: Ti.UI.iOS.TableViewStyle.PLAIN, id: "test_names" });

  $.__views.action_section.add($.__views.test_names);
  $.__views.email_section = Ti.UI.createView(
  { top: 20, left: 20, height: Ti.UI.SIZE, layout: "vertical", id: "email_section" });

  $.__views.action_column.add($.__views.email_section);
  $.__views.email_tests_text = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailValueFont, left: 4, top: 10, bottom: 10, textid: "_EmailTestsText", id: "email_tests_text", accessibilityValue: "email_tests_text" });

  $.__views.email_section.add($.__views.email_tests_text);
  $.__views.email_address = Ti.UI.createTextField(
  { top: 10, layout: "vertical", font: Alloy.Styles.detailValueFont, left: 20, height: Ti.UI.SIZE, horizontalWrap: true, id: "email_address", accessibilityLabel: "email_address" });

  $.__views.email_section.add($.__views.email_address);
  $.__views.email_tests = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.buttons.primary.color, disabledColor: Alloy.Styles.buttons.primary.disabledColor, backgroundImage: Alloy.Styles.buttons.primary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.primary.backgroundDisabledImage, height: 30, width: Ti.UI.FILL, font: Alloy.Styles.smallButtonFont, top: 10, bottom: 20, titleid: "_Email_Test_Results", id: "email_tests", accessibilityValue: "email_tests" });

  $.__views.email_section.add($.__views.email_tests);
  emailTests ? $.addListener($.__views.email_tests, 'click', emailTests) : __defers['$.__views.email_tests!click!emailTests'] = true;exports.destroy = function () {};




  _.extend($, $.__views);










  var behaveLogger = require('behaveLoggerExt');

  var args = arguments[0] || {};
  var testRunning = false;
  var emailLogs = require('EAUtils').emailLogs;




  $.listenTo(Alloy.eventDispatcher, 'tests:complete', function () {
    testRunning = false;
    $.clear_results.setEnabled(true);
  });




  exports.deinit = deinit;









  function deinit() {

    $.stopListening();
    $.test_names.removeEventListener('click', onTestNameClick);

    $.clear_results.removeEventListener('click', clearOutput);
    $.email_tests.removeEventListener('click', emailTests);
    $.destroy();
  }










  function logToTestOutput(str) {
    $.test_output.setValue($.test_output.value + str);
  }






  function initializeTestList() {
    var data = [];
    var tests = Alloy.CFG.support_dashboard_tests;
    for (var i in tests) {
      data.push({
        id: tests[i],
        title: tests[i] });

    }

    $.test_names.addEventListener('click', onTestNameClick);
    $.test_names.setData(data);
  }










  function onTestNameClick(event) {
    if (testRunning) {
      notify(_L('Test currently in progress'));
      return;
    }

    notify(_L('Starting test execution: ') + event.row.title);
    testRunning = true;
    $.clear_results.setEnabled(false);
    Alloy.eventDispatcher.trigger('tests:run', {
      tests: [event.row.id] });

  }






  function emailTests() {
    var log = $.test_output.value;
    if (log && log.length > 0) {
      var promise = emailLogs(log);
      Alloy.Router.showActivityIndicator(promise);
      notify(_L('Email of testing log has been sent.'));
    } else {
      notify(_L('Email of testing log was not sent.'));
    }
  }






  function clearOutput() {
    $.test_output.setValue('');

  }




  if (Alloy.CFG.admin_email != null) {
    $.email_address.setValue(Alloy.CFG.admin_email);
    $.email_address.setHeight(Ti.UI.SIZE);
  } else {
    $.email_section.setHeight(0);
    $.email_section.setVisible(false);
  }

  behaveLogger.setLoggerFunction(logToTestOutput);
  initializeTestList();





  __defers['$.__views.clear_results!click!clearOutput'] && $.addListener($.__views.clear_results, 'click', clearOutput);__defers['$.__views.email_tests!click!emailTests'] && $.addListener($.__views.email_tests, 'click', emailTests);



  _.extend($, exports);
}

module.exports = Controller;