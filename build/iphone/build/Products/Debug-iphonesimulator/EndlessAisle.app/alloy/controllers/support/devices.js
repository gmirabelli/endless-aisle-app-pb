var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'support/devices';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.devices = Ti.UI.createView(
  { title: "Support Dashboard - Devices", tabBarHidden: false, navBarHidden: false, id: "devices" });

  $.__views.devices && $.addTopLevelView($.__views.devices);
  $.__views.devices_main = Ti.UI.createView(
  { layout: "horizontal", width: "100%", height: "100%", id: "devices_main" });

  $.__views.devices.add($.__views.devices_main);
  $.__views.data_column = Ti.UI.createView(
  { layout: "vertical", width: "70%", height: "100%", id: "data_column" });

  $.__views.devices_main.add($.__views.data_column);
  $.__views.vertical_border = Ti.UI.createView(
  { width: 2, backgroundColor: Alloy.Styles.color.border.darkest, id: "vertical_border" });

  $.__views.devices_main.add($.__views.vertical_border);
  $.__views.action_column = Ti.UI.createView(
  { layout: "vertical", width: "28%", height: "100%", id: "action_column" });

  $.__views.devices_main.add($.__views.action_column);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var args = arguments[0] || {};

  var deviceType = args.device;
  var deviceModule;

  var logger = require('logging')('support:devices', getFullControllerPath($.__controllerPath));




  exports.deinit = deinit;









  function init(deviceType) {
    logger.info('INIT called');
    $.data_column.removeAllChildren();
    $.action_column.removeAllChildren();

    if (!deviceType) {
      logger.error('No device type defined.  Unable to render view for support dashboard.');
      return false;
    }


    var deviceModulePath = Alloy.CFG.devices[deviceType + '_module'];

    if (!deviceModulePath) {
      logger.error('No device configuration found for device type: ' + deviceType + '_module');
      return false;
    }

    deviceModule = require(deviceModulePath);

    var infoView = 'getInfoView' in deviceModule && deviceModule.getInfoView();
    var configView = 'getConfigView' in deviceModule && deviceModule.getConfigView();

    if (infoView) {
      $.data_column.add(infoView);
    }
    if (configView) {
      $.action_column.add(configView);
    }


    return !!(infoView || configView);
  }






  function deinit() {
    logger.info('DEINIT called');
    'deinit' in deviceModule && deviceModule.deinit();
    removeAllChildren($.data_column);
    removeAllChildren($.action_column);

    $.stopListening();
    $.destroy();
  }




  init(deviceType);









  _.extend($, exports);
}

module.exports = Controller;