var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'checkout/confirmation/printerChooserDialog';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.printer_chooser_window = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.color.background.transparent, id: "printer_chooser_window" });

  $.__views.printer_chooser_window && $.addTopLevelView($.__views.printer_chooser_window);
  $.__views.backdrop = Ti.UI.createView(
  { top: 0, left: 0, width: 1024, height: 768, opacity: 0.55, backgroundColor: Alloy.Styles.color.background.black, id: "backdrop" });

  $.__views.printer_chooser_window.add($.__views.backdrop);
  $.__views.print_container = Ti.UI.createView(
  { layout: "vertical", width: 280, height: 280, top: 170, backgroundColor: Alloy.Styles.color.background.white, id: "print_container" });

  $.__views.printer_chooser_window.add($.__views.print_container);
  $.__views.print_button = Ti.UI.createButton(
  { backgroundImage: Alloy.Styles.buttons.secondary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.secondary.backgroundDisabledImage, color: Alloy.Styles.buttons.primary.color, width: 128, height: 42, font: Alloy.Styles.smallButtonFont, top: 20, titleid: "_Print", id: "print_button", accessibilityValue: "printer_dialog_print_button" });

  $.__views.print_container.add($.__views.print_button);
  $.__views.printer_container = Ti.UI.createView(
  { left: 20, right: 20, id: "printer_container" });

  $.__views.print_container.add($.__views.printer_container);
  $.__views.printer_chooser = Alloy.createController('checkout/confirmation/printerChooser', { id: "printer_chooser", __parentSymbol: $.__views.printer_container });
  $.__views.printer_chooser.setParent($.__views.printer_container);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var receiptToPrint;
  var printerAvailability = Alloy.CFG.printer_availability;




  $.print_button.addEventListener('click', onPrintClick);


  $.backdrop.addEventListener('click', dismiss);


  $.listenTo($.printer_chooser, 'printer_chosen', onPrinterChosen);




  exports.init = init;
  exports.deinit = deinit;










  function init(receipt) {
    if (printerAvailability) {
      receiptToPrint = receipt;
      $.printer_chooser.init();
      $.print_button.setEnabled(false);
    } else {
      $.print_button.setVisible(false);
      $.print_button.setWidth(0);
      $.print_button.setHeight(0);
    }
  }






  function deinit() {
    $.print_button.removeEventListener('click', onPrintClick);
    $.backdrop.removeEventListener('click', dismiss);
    $.stopListening($.printer_chooser, 'printer_chosen', onPrinterChosen);
    $.printer_chooser.deinit();
    $.stopListening();
    $.destroy();
  }









  function dismiss() {
    Alloy.eventDispatcher.trigger('printerlist:stopdiscovery', {});
    $.trigger('printer_chooser:dismiss');
  }






  function onPrintClick() {
    Alloy.eventDispatcher.trigger('print:receipt', receiptToPrint);
    dismiss();
  }







  function onPrinterChosen(enabled) {
    $.print_button.setEnabled(enabled.enabled);
  }









  _.extend($, exports);
}

module.exports = Controller;