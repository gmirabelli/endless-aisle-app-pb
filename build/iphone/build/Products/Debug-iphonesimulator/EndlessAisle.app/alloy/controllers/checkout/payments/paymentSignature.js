var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'checkout/payments/paymentSignature';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};




  $.printerList = Alloy.createCollection('printer');


  $.__views.payment_signature_window = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.color.background.transparent, id: "payment_signature_window" });

  $.__views.payment_signature_window && $.addTopLevelView($.__views.payment_signature_window);
  $.__views.backdrop = Ti.UI.createView(
  { top: 0, left: 0, width: 1024, height: 768, opacity: 0.55, backgroundColor: Alloy.Styles.color.background.black, id: "backdrop" });

  $.__views.payment_signature_window.add($.__views.backdrop);
  $.__views.payment_container = Ti.UI.createView(
  { layout: "vertical", width: 911, left: 56, height: 371, top: 173, backgroundColor: Alloy.Styles.color.background.white, id: "payment_container" });

  $.__views.payment_signature_window.add($.__views.payment_container);
  $.__views.contents = Ti.UI.createView(
  { height: 576, layout: "vertical", backgroundColor: Alloy.Styles.color.background.white, id: "contents", visible: false });

  $.__views.payment_container.add($.__views.contents);
  $.__views.prompt_signature = Alloy.createController('checkout/payments/promptSignature', { id: "prompt_signature", __parentSymbol: $.__views.contents });
  $.__views.prompt_signature.setParent($.__views.contents);
  $.__views.receipt_question_container = Ti.UI.createView(
  { height: 576, layout: "horizontal", id: "receipt_question_container", visible: true });

  $.__views.payment_container.add($.__views.receipt_question_container);
  $.__views.payment_confirmation_container = Ti.UI.createView(
  { width: "33%", layout: "vertical", id: "payment_confirmation_container" });

  $.__views.receipt_question_container.add($.__views.payment_confirmation_container);
  $.__views.thank_you_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.accentColor, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.bigTitleFont, top: 34, textid: "_Thank_You_", id: "thank_you_label", accessibilityValue: "thank_you_label" });

  $.__views.payment_confirmation_container.add($.__views.thank_you_label);
  $.__views.payment_confirmation_line1 = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: Alloy.Styles.appFont, top: 43, textid: "_Payment_has_been_accepted_and_your_order_is_complete_", id: "payment_confirmation_line1", accessibilityValue: "payment_confirmation_line1" });

  $.__views.payment_confirmation_container.add($.__views.payment_confirmation_line1);
  $.__views.order_no = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.appFont, id: "order_no", accessibilityValue: "order_no" });

  $.__views.payment_confirmation_container.add($.__views.order_no);
  $.__views.divider = Ti.UI.createLabel(
  { width: 2, height: 300, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, backgroundColor: Alloy.Styles.color.background.medium, top: 35, left: 15, id: "divider", accessibilityValue: "divider" });

  $.__views.receipt_question_container.add($.__views.divider);
  $.__views.question_container = Ti.UI.createView(
  { layout: "vertical", id: "question_container" });

  $.__views.receipt_question_container.add($.__views.question_container);
  $.__views.receipt_question_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.calloutFont, top: 34, left: 34, right: 34, id: "receipt_question_label", accessibilityValue: "receipt_question_label" });

  $.__views.question_container.add($.__views.receipt_question_label);
  $.__views.button_container = Ti.UI.createView(
  { layout: "horizontal", top: 37, id: "button_container" });

  $.__views.question_container.add($.__views.button_container);
  $.__views.email_button_container = Ti.UI.createView(
  { width: Ti.UI.SIZE, id: "email_button_container" });

  $.__views.button_container.add($.__views.email_button_container);
  $.__views.email_button = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, backgroundImage: Alloy.Styles.buttons.secondary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.secondary.backgroundDisabledImage, color: Alloy.Styles.buttons.secondary.color, disabledColor: Alloy.Styles.buttons.secondary.disabledColor, height: 35, width: 128, top: 0, id: "email_button", accessibilityValue: "signature_email_button" });

  $.__views.email_button_container.add($.__views.email_button);
  $.__views.print_container = Ti.UI.createView(
  { layout: "vertical", left: 10, right: 20, width: Ti.UI.SIZE, id: "print_container" });

  $.__views.button_container.add($.__views.print_container);
  $.__views.print_button = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, backgroundImage: Alloy.Styles.buttons.secondary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.secondary.backgroundDisabledImage, color: Alloy.Styles.buttons.secondary.color, disabledColor: Alloy.Styles.buttons.secondary.disabledColor, height: 35, width: 128, left: 0, titleid: "_Print", id: "print_button", accessibilityValue: "signature_print_button" });

  $.__views.print_container.add($.__views.print_button);
  $.__views.printer_chooser = Alloy.createController('checkout/confirmation/printerChooser', { id: "printer_chooser", __parentSymbol: $.__views.print_container });
  $.__views.printer_chooser.setParent($.__views.print_container);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var eaUtils = require('EAUtils');
  var logger = require('logging')('checkout:payments:paymentSignature', getFullControllerPath($.__controllerPath));

  var currentBasket = Alloy.Models.basket;
  var storeInfo = Alloy.Models.storeInfo;
  var currentAssociate = Alloy.Models.associate;

  var order = arguments[0] ? arguments[0] : null;

  var collection = new Backbone.Collection();
  var printerAvailability = Alloy.CFG.printer_availability;





  $.email_button.addEventListener('click', onEmailClick);


  $.print_button.addEventListener('click', onPrintClick);


  $.listenTo($.printer_chooser, 'printer_chosen', onPrinterChosen);


  $.listenTo($.prompt_signature, 'promptSignature:accept_signature', onAcceptSignature);




  exports.init = init;
  exports.deinit = deinit;










  function init(args) {
    logger.info('INIT called');
    $.prompt_signature.init();

    order = Alloy.createModel('baskets', args.toJSON());

    if (order.doesPaymentRequireSignature()) {
      switchToSignatureDisplay(true);
    } else {
      initializePrinterChooser();
    }
    $.print_button.setEnabled(false);
    if (eaUtils.isSymbolBasedLanguage()) {
      $.payment_confirmation_line1.setFont(Alloy.Styles.detailTextFont);
    } else {
      $.payment_confirmation_line1.setFont(Alloy.Styles.appFont);
    }
  }






  function deinit() {
    logger.info('DEINIT called');
    $.prompt_signature.deinit();
    $.email_button.removeEventListener('click', onEmailClick);
    $.print_button.removeEventListener('click', onPrintClick);
    Alloy.eventDispatcher.trigger('printerlist:stopdiscovery', {});
    $.printer_chooser.deinit();
    $.stopListening();
    $.destroy();
  }









  function switchToSignatureDisplay(displaySignature) {
    if (displaySignature) {
      $.contents.setHeight(371);
      $.contents.show();
      $.receipt_question_container.hide();
      $.receipt_question_container.setHeight(0);
    } else {
      $.receipt_question_container.setHeight(371);
      $.receipt_question_container.show();
      $.contents.hide();
      $.contents.setHeight(0);
    }
  }






  function dismiss() {
    $.trigger('signature:dismiss');
  }






  function initializePrinterChooser() {

    if (!printerAvailability) {
      logger.info('Printer is not available');
      $.print_container.hide();
      $.print_container.setWidth(0);
      $.email_button_container.setWidth('100%');
      $.email_button.setTitle(_L('OK'));
      $.receipt_question_label.setText(_L('Emailing receipt'));

      currentBasket.emailOrder(order.getOrderNumber()).always(function () {
        $.receipt_question_label.setText(_L('Your receipt has been emailed'));
      }).fail(function () {
        notify(_L('Error sending email'), {
          preventAutoClose: true });

      });
    } else {
      logger.info('Printer is available');
      $.receipt_question_label.setText(_L('How would you like to receive your receipt?'));
      $.email_button.setTitle(_L('Email'));
      $.email_button_container.setWidth('49%');
      $.email_button.setRight(10);
      $.printer_chooser.init();
    }
    $.trigger('signature:accepted');

    switchToSignatureDisplay(false);
    $.order_no.setText(_L('Order No:') + ' ' + order.getOrderNumber());
  }










  function onAcceptSignature(event) {
    logger.info('onAcceptSignature called');

    Alloy.sigBlob = event.image;

    var orderNo = order.getOrderNumber();

    eaUtils.sendSignatureToServer(event.image, orderNo + '');

    initializePrinterChooser();
  }






  function onEmailClick() {
    if (!printerAvailability) {
      dismiss();
    } else {
      Alloy.eventDispatcher.trigger('printerlist:stopdiscovery', {});
      var promise = currentBasket.emailOrder(order.getOrderNumber());
      Alloy.Router.showActivityIndicator(promise);
      promise.done(function () {
        dismiss();
      }).fail(function () {
        notify(_L('Error sending email'), {
          preventAutoClose: true });

      });
    }
  }






  function onPrintClick() {
    var receipt = _.extend({}, order);
    receipt.set('employee_id', currentAssociate.getEmployeeId());
    receipt.set('employee_name', currentAssociate.getFirstName() + ' ' + currentAssociate.getLastName());
    receipt.set('store_name', storeInfo.getId());
    receipt.set('store_address', storeInfo.constructStoreAddress());

    var json = receipt.toJSON();

    Alloy.eventDispatcher.trigger('printerlist:stopdiscovery', {});
    Alloy.eventDispatcher.trigger('print:receipt', json);

    dismiss();
  }







  function onPrinterChosen(enabled) {
    $.print_button.setEnabled(enabled.enabled);
  }









  _.extend($, exports);
}

module.exports = Controller;