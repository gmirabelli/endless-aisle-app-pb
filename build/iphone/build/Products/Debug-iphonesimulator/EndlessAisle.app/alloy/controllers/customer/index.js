var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'customer/index';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};




  $.customerAddresses = Alloy.createCollection('customerAddress');Alloy.Models.instance('customer');


  $.__views.index = Ti.UI.createView(
  { top: 55, id: "index" });

  $.__views.index && $.addTopLevelView($.__views.index);
  $.__views.__alloyId174 = Ti.UI.createView(
  { layout: "horizontal", id: "__alloyId174" });

  $.__views.index.add($.__views.__alloyId174);
  $.__views.__alloyId175 = Ti.UI.createView(
  { layout: "vertical", width: 400, height: "100%", left: 0, id: "__alloyId175" });

  $.__views.__alloyId174.add($.__views.__alloyId175);
  $.__views.__alloyId176 = Ti.UI.createView(
  { height: Ti.UI.SIZE, id: "__alloyId176" });

  $.__views.__alloyId175.add($.__views.__alloyId176);
  $.__views.customer_index_header_label = Ti.UI.createLabel(
  { width: 260, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.tabFont, top: 10, left: 20, textid: "_Customer_Details", id: "customer_index_header_label", accessibilityValue: "customer_index_header_label" });

  $.__views.__alloyId176.add($.__views.customer_index_header_label);
  $.__views.__alloyId177 = Ti.UI.createView(
  { layout: "vertical", top: 0, left: 25, height: Ti.UI.SIZE, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, id: "__alloyId177" });

  $.__views.__alloyId175.add($.__views.__alloyId177);
  $.__views.profile_fullname_view = Ti.UI.createView(
  { layout: "vertical", height: Ti.UI.SIZE, verticalAlign: Ti.UI.TEXT_VERTICAL_ALIGNMENT_TOP, bottom: 10, top: 35, id: "profile_fullname_view" });

  $.__views.__alloyId177.add($.__views.profile_fullname_view);
  $.__views.profile_fullname = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.accentColor, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, font: Alloy.Styles.calloutFont, id: "profile_fullname", accessibilityValue: "profile_fullname" });

  $.__views.profile_fullname_view.add($.__views.profile_fullname);
  $.__views.__alloyId178 = Ti.UI.createView(
  { layout: "horizontal", height: 20, id: "__alloyId178" });

  $.__views.__alloyId177.add($.__views.__alloyId178);
  $.__views.primary_info_view_email_text = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, top: 0, font: Alloy.Styles.detailValueFont, autocorrect: false, textid: "_Email__", id: "primary_info_view_email_text", accessibilityValue: "primary_info_view_email_text" });

  $.__views.__alloyId178.add($.__views.primary_info_view_email_text);
  $.__views.profile_email = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, top: 0, font: Alloy.Styles.detailValueFont, autocorrect: false, id: "profile_email", accessibilityValue: "profile_email" });

  $.__views.__alloyId178.add($.__views.profile_email);
  $.__views.__alloyId179 = Ti.UI.createView(
  { layout: "horizontal", height: 20, id: "__alloyId179" });

  $.__views.__alloyId177.add($.__views.__alloyId179);
  $.__views.primary_info_view_login_text = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, top: 0, font: Alloy.Styles.detailValueFont, autocorrect: false, textid: "_Login__", id: "primary_info_view_login_text", accessibilityValue: "primary_info_view_login_text" });

  $.__views.__alloyId179.add($.__views.primary_info_view_login_text);
  $.__views.profile_login = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, top: 0, font: Alloy.Styles.detailValueFont, autocorrect: false, id: "profile_login", accessibilityValue: "profile_login" });

  $.__views.__alloyId179.add($.__views.profile_login);
  $.__views.__alloyId180 = Ti.UI.createView(
  { layout: "vertical", top: 20, height: 80, id: "__alloyId180" });

  $.__views.__alloyId177.add($.__views.__alloyId180);
  $.__views.__alloyId181 = Ti.UI.createView(
  { layout: "horizontal", height: 20, id: "__alloyId181" });

  $.__views.__alloyId180.add($.__views.__alloyId181);
  $.__views.profile_labelled_view_birthday_text = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, font: Alloy.Styles.detailValueFont, textid: "_Birthday__", id: "profile_labelled_view_birthday_text", accessibilityValue: "profile_labelled_view_birthday_text" });

  $.__views.__alloyId181.add($.__views.profile_labelled_view_birthday_text);
  $.__views.profile_birthday = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, font: Alloy.Styles.detailValueFont, id: "profile_birthday", accessibilityValue: "profile_birthday" });

  $.__views.__alloyId181.add($.__views.profile_birthday);
  $.__views.__alloyId182 = Ti.UI.createView(
  { layout: "horizontal", height: 20, id: "__alloyId182" });

  $.__views.__alloyId180.add($.__views.__alloyId182);
  $.__views.profile_labelled_view_last_visit_text = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, font: Alloy.Styles.detailValueFont, textid: "_Days_since_last_visit__", id: "profile_labelled_view_last_visit_text", accessibilityValue: "profile_labelled_view_last_visit_text" });

  $.__views.__alloyId182.add($.__views.profile_labelled_view_last_visit_text);
  $.__views.profile_last_visit = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, font: Alloy.Styles.detailValueFont, id: "profile_last_visit", accessibilityValue: "profile_last_visit" });

  $.__views.__alloyId182.add($.__views.profile_last_visit);
  $.__views.__alloyId183 = Ti.UI.createView(
  { layout: "horizontal", height: 20, id: "__alloyId183" });

  $.__views.__alloyId180.add($.__views.__alloyId183);
  $.__views.profile_labelled_view_last_login_text = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, font: Alloy.Styles.detailValueFont, textid: "_Days_since_last_login__", id: "profile_labelled_view_last_login_text", accessibilityValue: "profile_labelled_view_last_login_text" });

  $.__views.__alloyId183.add($.__views.profile_labelled_view_last_login_text);
  $.__views.profile_last_login = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, font: Alloy.Styles.detailValueFont, id: "profile_last_login", accessibilityValue: "profile_last_login" });

  $.__views.__alloyId183.add($.__views.profile_last_login);
  $.__views.__alloyId184 = Ti.UI.createView(
  { layout: "horizontal", height: 20, id: "__alloyId184" });

  $.__views.__alloyId180.add($.__views.__alloyId184);
  $.__views.profile_labelled_view_customer_text = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, font: Alloy.Styles.detailValueFont, textid: "_Customer_Number__", id: "profile_labelled_view_customer_text", accessibilityValue: "profile_labelled_view_customer_text" });

  $.__views.__alloyId184.add($.__views.profile_labelled_view_customer_text);
  $.__views.profile_customer_no = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 0, font: Alloy.Styles.detailValueFont, id: "profile_customer_no", accessibilityValue: "profile_customer_no" });

  $.__views.__alloyId184.add($.__views.profile_customer_no);
  $.__views.shop_label = Ti.UI.createButton(
  { height: 35, width: 260, backgroundImage: Alloy.Styles.primaryButtonImage, color: Alloy.Styles.buttons.primary.color, font: Alloy.Styles.buttonFont, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, top: 300, left: 35, id: "shop_label", titleid: "_Return_to_Shopping", accessibilityValue: "shop_label" });

  $.__views.__alloyId177.add($.__views.shop_label);
  $.__views.__alloyId185 = Ti.UI.createView(
  { backgroundColor: "#eeeeee", height: "100%", width: 2, id: "__alloyId185" });

  $.__views.__alloyId174.add($.__views.__alloyId185);
  $.__views.__alloyId186 = Ti.UI.createView(
  { layout: "vertical", width: 622, height: "100%", right: 0, id: "__alloyId186" });

  $.__views.__alloyId174.add($.__views.__alloyId186);
  $.__views.__alloyId187 = Ti.UI.createView(
  { layout: "horizontal", height: 44, top: 0, left: 0, width: "100%", backgroundColor: Alloy.Styles.color.background.medium, id: "__alloyId187" });

  $.__views.__alloyId186.add($.__views.__alloyId187);
  $.__views.newProfile_tab_button = Ti.UI.createButton(
  { backgroundImage: Alloy.Styles.customerBorderImage, width: 156, height: 48, color: Alloy.Styles.color.text.dark, font: Alloy.Styles.buttonFont, style: Ti.UI.iOS.SystemButtonStyle.PLAIN, backgroundColor: Alloy.Styles.color.background.white, titleid: "_Profile_Tab", id: "newProfile_tab_button", accessibilityValue: "profile_tab_button" });

  $.__views.__alloyId187.add($.__views.newProfile_tab_button);
  $.__views.addresses_tab_button = Ti.UI.createButton(
  { backgroundImage: Alloy.Styles.customerBorderImage, width: 156, height: 48, color: Alloy.Styles.color.text.dark, font: Alloy.Styles.buttonFont, style: Ti.UI.iOS.SystemButtonStyle.PLAIN, backgroundColor: Alloy.Styles.color.background.white, titleid: "_Addresses", id: "addresses_tab_button", accessibilityValue: "addresses_tab_button" });

  $.__views.__alloyId187.add($.__views.addresses_tab_button);
  $.__views.history_tab_button = Ti.UI.createButton(
  { backgroundImage: Alloy.Styles.customerBorderImage, width: 156, height: 48, color: Alloy.Styles.color.text.dark, font: Alloy.Styles.buttonFont, style: Ti.UI.iOS.SystemButtonStyle.PLAIN, backgroundColor: Alloy.Styles.color.background.white, titleid: "_History", id: "history_tab_button", accessibilityValue: "history_tab_button" });

  $.__views.__alloyId187.add($.__views.history_tab_button);
  $.__views.customer_tab_group = Ti.UI.createView(
  { top: 0, left: 0, width: "100%", height: "100%", id: "customer_tab_group" });

  $.__views.__alloyId186.add($.__views.customer_tab_group);
  $.__views.customer_overview_tab = Ti.UI.createView(
  { font: Alloy.Styles.tabFont, backgroundColor: Alloy.Styles.tabs.backgroundColorEnabled, visible: false, id: "customer_overview_tab" });

  $.__views.customer_tab_group.add($.__views.customer_overview_tab);
  $.__views.__alloyId188 = Ti.UI.createView(
  { id: "__alloyId188" });

  $.__views.customer_overview_tab.add($.__views.__alloyId188);
  $.__views.customer_addresses = Alloy.createController('customer/components/addresses', { id: "customer_addresses", __parentSymbol: $.__views.__alloyId188 });
  $.__views.customer_addresses.setParent($.__views.__alloyId188);
  $.__views.customer_profile_tab = Ti.UI.createView(
  { font: Alloy.Styles.tabFont, backgroundColor: Alloy.Styles.tabs.backgroundColorEnabled, visible: false, id: "customer_profile_tab" });

  $.__views.customer_tab_group.add($.__views.customer_profile_tab);
  $.__views.__alloyId189 = Ti.UI.createView(
  { id: "__alloyId189" });

  $.__views.customer_profile_tab.add($.__views.__alloyId189);
  $.__views.customer_profile = Alloy.createController('customer/components/profile', { id: "customer_profile", __parentSymbol: $.__views.__alloyId189 });
  $.__views.customer_profile.setParent($.__views.__alloyId189);
  $.__views.customer_history_tab = Ti.UI.createView(
  { font: Alloy.Styles.tabFont, backgroundColor: Alloy.Styles.tabs.backgroundColorEnabled, visible: false, id: "customer_history_tab" });

  $.__views.customer_tab_group.add($.__views.customer_history_tab);
  $.__views.__alloyId190 = Ti.UI.createView(
  { id: "__alloyId190" });

  $.__views.customer_history_tab.add($.__views.__alloyId190);
  $.__views.customer_history = Alloy.createController('customer/components/history', { id: "customer_history", __parentSymbol: $.__views.__alloyId190 });
  $.__views.customer_history.setParent($.__views.__alloyId190);
  $.__views.customer_edit_address_tab = Ti.UI.createView(
  { font: Alloy.Styles.tabFont, backgroundColor: Alloy.Styles.tabs.backgroundColorEnabled, visible: false, id: "customer_edit_address_tab" });

  $.__views.customer_tab_group.add($.__views.customer_edit_address_tab);
  $.__views.__alloyId191 = Ti.UI.createView(
  { id: "__alloyId191" });

  $.__views.customer_edit_address_tab.add($.__views.__alloyId191);
  $.__views.customer_address = Alloy.createController('customer/components/address', { id: "customer_address", __parentSymbol: $.__views.__alloyId191 });
  $.__views.customer_address.setParent($.__views.__alloyId191);
  $.__views.customer_edit_profile_tab = Ti.UI.createView(
  { font: Alloy.Styles.tabFont, backgroundColor: Alloy.Styles.tabs.backgroundColorEnabled, visible: false, id: "customer_edit_profile_tab" });

  $.__views.customer_tab_group.add($.__views.customer_edit_profile_tab);
  $.__views.__alloyId192 = Ti.UI.createView(
  { id: "__alloyId192" });

  $.__views.customer_edit_profile_tab.add($.__views.__alloyId192);
  $.__views.edit_customer_profile = Alloy.createController('customer/components/editProfile', { id: "edit_customer_profile", __parentSymbol: $.__views.__alloyId192 });
  $.__views.edit_customer_profile.setParent($.__views.__alloyId192);
  var __alloyId193 = function () {Alloy['Models']['customer'].__transform = _.isFunction(Alloy['Models']['customer'].transform) ? Alloy['Models']['customer'].transform() : Alloy['Models']['customer'].toJSON();$.profile_email.text = Alloy['Models']['customer']['__transform']['email'];$.profile_login.text = Alloy['Models']['customer']['__transform']['login'];$.profile_birthday.text = Alloy['Models']['customer']['__transform']['birthday'];$.profile_last_visit.text = Alloy['Models']['customer']['__transform']['last_visit'];$.profile_last_login.text = Alloy['Models']['customer']['__transform']['last_login'];$.profile_customer_no.text = Alloy['Models']['customer']['__transform']['customer_no'];};Alloy['Models']['customer'].on('fetch change destroy', __alloyId193);exports.destroy = function () {Alloy['Models']['customer'] && Alloy['Models']['customer'].off('fetch change destroy', __alloyId193);};




  _.extend($, $.__views);










  var currentCustomer = Alloy.Models.customer;
  var currentAssociate = Alloy.Models.associate;
  var returnToShopping = require('EAUtils').returnToShopping;
  var customerAddress = Alloy.Models.customerAddress;
  var currentPage;
  var warningMessage;
  var warningTitle;
  var showCustomerAddressAlert = require('EAUtils').showCustomerAddressAlert;
  var analytics = require('analyticsBase');
  var logger = require('logging')('customer:index', getFullControllerPath($.__controllerPath));
  var tabSequence = ['addresses', 'profile', 'history', 'address', 'editProfile'];





  $.index.addEventListener('changeCustomerPage', handlePageSelection);

  $.shop_label.addEventListener('click', onShopClick);

  $.addresses_tab_button.addEventListener('click', onAddressTabClick);

  $.newProfile_tab_button.addEventListener('click', onProfileTabClick);

  $.history_tab_button.addEventListener('click', onHistoryTabClick);

  $.customer_addresses.getView().addEventListener('route', route);
  $.customer_profile.getView().addEventListener('route', route);
  $.customer_history.getView().addEventListener('route', route);
  $.customer_address.getView().addEventListener('route', route);
  $.edit_customer_profile.getView().addEventListener('route', route);




  $.listenTo(currentAssociate, 'change', render);

  $.listenTo(currentCustomer, 'change', render);




  exports.init = init;
  exports.deinit = deinit;
  exports.render = render;










  function init(options) {
    logger.info('Calling INIT');
    options = options || {};
    var defaultPage = options.page || Alloy.CFG.defaultCustomerPage;

    $.index.fireEvent('changeCustomerPage', {
      page: defaultPage });

  }






  function render() {
    logger.info('Calling RENDER:\n');
    logger.secureLog(JSON.stringify(currentCustomer));

    var customer_no = currentCustomer.getCustomerNumber();

    if (customer_no) {
      logger.info('customer already logged in, showing profile');
      var customer_full_name;
      var addressForm = require(Alloy.CFG.addressform);
      var isLastNameFirst = _.isFunction(addressForm.isLastNameFirst) ? addressForm.isLastNameFirst() : false;
      if (isLastNameFirst === true) {
        customer_full_name = currentCustomer.getLastName() + ' ' + currentCustomer.getFirstName();
      } else {
        customer_full_name = currentCustomer.getFirstName() + ' ' + currentCustomer.getLastName();
      }
      $.profile_fullname.setText(customer_full_name);
    } else {
      $.profile_last_visit.setText('');
      $.profile_last_login.setText('');
    }
  }






  function deinit() {
    logger.info('DEINIT called');
    $.shop_label.removeEventListener('click', onShopClick);
    $.index.removeEventListener('changeCustomerPage', handlePageSelection);
    $.addresses_tab_button.removeEventListener('click', onAddressTabClick);
    $.newProfile_tab_button.removeEventListener('click', onProfileTabClick);
    $.history_tab_button.removeEventListener('click', onHistoryTabClick);
    $.customer_addresses.getView().removeEventListener('route', route);
    $.customer_profile.getView().removeEventListener('route', route);
    $.customer_history.getView().removeEventListener('route', route);
    $.customer_address.getView().removeEventListener('route', route);
    $.edit_customer_profile.getView().removeEventListener('route', route);
    $.customer_addresses.deinit();
    $.customer_profile.deinit();
    $.customer_history.deinit();
    $.customer_address.deinit();
    $.edit_customer_profile.deinit();
    $.stopListening();
    $.destroy();
  }








  function disableAllTabs() {

    $.history_tab_button.setBackgroundImage(Alloy.Styles.customerTabImage);
    $.addresses_tab_button.setBackgroundImage(Alloy.Styles.customerTabImage);
    $.newProfile_tab_button.setBackgroundImage(Alloy.Styles.customerTabImage);
  }










  function route(args) {
    logger.info('handling customer route event: ' + args);
    args.cancelBubble = true;

    if (args.page == 'order') {
      Alloy.Dialog.showCustomDialog({
        controllerPath: 'customer/components/order',
        continueEvent: 'order_history:dismiss' });

    } else {
      handlePageSelection(args);
    }
  }







  function handlePageSelection(event) {
    logger.info('Handle Page Selection ' + event);


    disableAllTabs();
    currentPage = customerAddress.getCurrentPage();
    if (!currentPage) {
      for (var i = 0, ii = $.customer_tab_group.children.length; i < ii; i++) {
        $.customer_tab_group.children[i].hide();
      }
    }

    switch (event.page) {
      case 'addresses':
        $.addresses_tab_button.setBackgroundImage('');
        if (!event.isCancel) {
          var deferredIndicator = new _.Deferred();
          Alloy.Router.showActivityIndicator(deferredIndicator);
          $.customer_addresses.init().always(function () {
            deferredIndicator.resolve();
          });
        }
        if (currentPage) {
          $.customer_tab_group.children[_.indexOf(tabSequence, currentPage)].hide();
        }
        $.customer_tab_group.children[0].show();
        customerAddress.setCurrentPage(event.page);
        analytics.fireScreenEvent({
          screen: _L('Customer Addresses') });

        break;
      case 'profile':
        $.newProfile_tab_button.setBackgroundImage('');
        $.customer_profile.init();
        if (currentPage) {
          $.customer_tab_group.children[_.indexOf(tabSequence, currentPage)].hide();
        }
        $.customer_tab_group.children[1].show();
        customerAddress.setCurrentPage(event.page);
        analytics.fireScreenEvent({
          screen: _L('Customer Profile') });

        break;
      case 'history':
        $.history_tab_button.setBackgroundImage('');

        var deferredIndicator = new _.Deferred();
        Alloy.Router.showActivityIndicator(deferredIndicator);
        $.customer_history.init().always(function () {
          deferredIndicator.resolve();
        });
        if (currentPage) {
          $.customer_tab_group.children[_.indexOf(tabSequence, currentPage)].hide();
        }
        $.customer_tab_group.children[2].show();
        customerAddress.setCurrentPage(event.page);
        analytics.fireScreenEvent({
          screen: _L('Customer History') });

        break;
      case 'address':
        $.addresses_tab_button.setBackgroundImage('');
        var address_id = event.address_id;
        var promise = $.customer_address.init({
          address_id: address_id });

        Alloy.Router.showActivityIndicator(promise);
        promise.done(function () {
          if (currentPage) {
            $.customer_tab_group.children[_.indexOf(tabSequence, currentPage)].hide();
          }
          $.customer_tab_group.children[3].show();
          customerAddress.setCurrentPage(event.page);
        });
        analytics.fireScreenEvent({
          screen: _L('Customer Address') });

        customerAddress.setCurrentPage(event.page);
        break;
      case 'editProfile':
        $.newProfile_tab_button.setBackgroundImage('');
        $.customer_profile.init();
        if (currentPage) {
          $.customer_tab_group.children[_.indexOf(tabSequence, currentPage)].hide();
        }
        $.customer_tab_group.children[4].show();
        customerAddress.setCurrentPage(event.page);
        analytics.fireScreenEvent({
          screen: _L('Customer Edit Profile') });

        break;}

  }






  function onShopClick(event) {
    logger.info('shop_label click listener');
    if (customerAddress.isCustomerAddressPage()) {
      showCustomerAddressAlert(true).done(function () {
        returnToShopping();
      });
    } else {
      event.cancelBubble = true;
      customerAddress.setCurrentPage(null);

      returnToShopping();
    }
  }






  function onAddressTabClick(event) {
    logger.info('addresses_tab_button click listener');
    event.cancelBubble = true;
    handlePageSelection({
      page: 'addresses' });

  }






  function onProfileTabClick(event) {
    logger.info('newProfile_tab_button click listener');
    Alloy.eventDispatcher.trigger('hideAuxillaryViews');
    if (customerAddress.isCustomerAddressPage()) {
      showCustomerAddressAlert(true).done(function () {
        event.cancelBubble = true;
        handlePageSelection({
          page: 'profile' });

      });
    } else {
      event.cancelBubble = true;
      handlePageSelection({
        page: 'profile' });

    }
  }






  function onHistoryTabClick(event) {
    logger.info('history_tab_button click listener');
    if (customerAddress.isCustomerAddressPage()) {
      showCustomerAddressAlert(true).done(function () {
        event.cancelBubble = true;
        handlePageSelection({
          page: 'history' });

      });
    } else {
      event.cancelBubble = true;
      handlePageSelection({
        page: 'history' });

    }
  }









  _.extend($, exports);
}

module.exports = Controller;