var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'search/components/attributesRefinementPanel';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.attributesRefinementPanel = Ti.UI.createView(
  { layout: "absolute", visible: false, top: 0, left: 450, bottom: 0, right: 0, zIndex: 2, id: "attributesRefinementPanel" });

  $.__views.attributesRefinementPanel && $.addTopLevelView($.__views.attributesRefinementPanel);
  $.__views.__alloyId231 = Ti.UI.createView(
  { left: 0, top: 0, width: 8, backgroundImage: Alloy.Styles.rightShadowImage, zIndex: 4, height: "100%", id: "__alloyId231" });

  $.__views.attributesRefinementPanel.add($.__views.__alloyId231);
  $.__views.refinementsTableHeader = Ti.UI.createView(
  { height: 50, width: "100%", layout: "absolute", backgroundColor: Alloy.Styles.color.background.light, id: "refinementsTableHeader" });

  $.__views.refinementsTableHeaderLabel = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: 50, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.bigAccentFont, left: 22, backgroundColor: Alloy.Styles.color.background.transparent, id: "refinementsTableHeaderLabel", accessibilityValue: "refinementsTableHeaderLabel", textid: "_Filter_Options" });

  $.__views.refinementsTableHeader.add($.__views.refinementsTableHeaderLabel);
  $.__views.refinementsTable = Ti.UI.createTableView(
  { top: 0, left: 8, width: 307, height: 643, backgroundColor: Alloy.Styles.color.background.light, color: Alloy.Styles.color.text.dark, zIndex: 4, opacity: 0.95, separatorStyle: Ti.UI.TABLE_VIEW_SEPARATOR_STYLE_NONE, headerView: $.__views.refinementsTableHeader, id: "refinementsTable" });

  $.__views.attributesRefinementPanel.add($.__views.refinementsTable);
  $.__views.__alloyId233 = Ti.UI.createView(
  { left: 315, top: 0, width: 8, backgroundImage: Alloy.Styles.leftShadowImage, zIndex: 4, height: "100%", id: "__alloyId233" });

  $.__views.attributesRefinementPanel.add($.__views.__alloyId233);
  $.__views.appliedFiltersTableHeader = Ti.UI.createView(
  { height: 60, width: "100%", layout: "absolute", backgroundColor: Alloy.Styles.color.background.light, id: "appliedFiltersTableHeader" });

  $.__views.appliedFiltersTableHeaderLabel = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: 60, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.smallAccentFont, left: 25, backgroundColor: Alloy.Styles.color.background.transparent, id: "appliedFiltersTableHeaderLabel", accessibilityValue: "appliedFiltersTableHeaderLabel", textid: "_Applied_Filters" });

  $.__views.appliedFiltersTableHeader.add($.__views.appliedFiltersTableHeaderLabel);
  $.__views.appliedFiltersTableHeaderButton = Ti.UI.createView(
  { right: 8, top: 13, width: 96, height: 32, color: Alloy.Styles.color.text.mediumdark, backgroundColor: Alloy.Styles.color.background.medium, borderColor: Alloy.Styles.color.border.dark, id: "appliedFiltersTableHeaderButton" });

  $.__views.appliedFiltersTableHeader.add($.__views.appliedFiltersTableHeaderButton);
  $.__views.appliedFiltersTableHeaderButtonLabel = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.mediumdark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.buttonFont, id: "appliedFiltersTableHeaderButtonLabel", accessibilityValue: "appliedFiltersTableHeaderButtonLabel", textid: "_Clear" });

  $.__views.appliedFiltersTableHeaderButton.add($.__views.appliedFiltersTableHeaderButtonLabel);
  $.__views.appliedFiltersTable = Ti.UI.createTableView(
  { top: 0, left: 306, width: 265, backgroundColor: Alloy.Styles.color.background.light, color: Alloy.Styles.color.text.dark, zIndex: 3, borderWidth: 1, borderColor: Alloy.Styles.color.border.lightest, rowHeight: 50, font: Alloy.Styles.buttonFont, separatorStyle: Ti.UI.TABLE_VIEW_SEPARATOR_STYLE_NONE, headerView: $.__views.appliedFiltersTableHeader, id: "appliedFiltersTable" });

  $.__views.attributesRefinementPanel.add($.__views.appliedFiltersTable);
  exports.destroy = function () {};




  _.extend($, $.__views);











  var headerTextLabel = 15;
  var symbolHeaderTextLabel = 8;

  var eaUtils = require('EAUtils');
  var logger = require('logging')('search:components:attributesRefinementPanel', getFullControllerPath($.__controllerPath));
  var currentSearch = Alloy.Models.productSearch;
  var refinementControllers = [];




  $.refinementsTable.addEventListener('refinement:toggle', onRefinementToggle);

  $.appliedFiltersTable.addEventListener('click', onFilterClick);

  $.appliedFiltersTableHeaderButton.addEventListener('click', onFilterHeaderClick);




  $.listenTo(currentSearch, 'change:refinements', initRefinementsUI);

  $.listenTo(currentSearch, 'change:selected_refinements', initSelectedRefinementsUI);




  exports.deinit = deinit;









  function init() {
    logger.info('init');

    initRefinementsUI();
    initSelectedRefinementsUI();
    if (eaUtils.isSymbolBasedLanguage() && $.appliedFiltersTableHeaderLabel.getText().length > symbolHeaderTextLabel) {
      $.appliedFiltersTableHeaderLabel.setFont(Alloy.Styles.smallestAccentFont);
    } else {
      if ($.appliedFiltersTableHeaderLabel.getText().length > headerTextLabel) {
        $.appliedFiltersTableHeaderLabel.setFont(Alloy.Styles.smallerAccentCalloutFont);
      } else {
        $.appliedFiltersTableHeaderLabel.setFont(Alloy.Styles.smallAccentFont);
      }
    }
  }






  function deinit() {
    logger.info('DEINIT called');
    $.refinementsTable.removeEventListener('refinement:toggle', onRefinementToggle);
    $.appliedFiltersTable.removeEventListener('click', onFilterClick);
    $.appliedFiltersTableHeaderButton.removeEventListener('click', onFilterHeaderClick);
    $.stopListening();
    deinitRefinementControllers();
    $.destroy();
  }












  function templateForRefinementAttribute(attributeId) {
    return Alloy.CFG.product_search.refinements.refinement_component[attributeId] || Alloy.CFG.product_search.refinements.refinement_component['default'];
  }






  function deinitRefinementControllers() {
    _.each(refinementControllers, function (refinementController) {
      refinementController.deinit && refinementController.deinit();
    });
    refinementControllers = [];
  }











  function onRefinementToggle(event) {
    event.cancelBubble = true;

    var attribute_id = event.attribute_id;
    var value_id = event.value_id;

    currentSearch.toggleRefinementValue(attribute_id, value_id, {
      silent: true });

    eaUtils.doProductSearch({
      force: true });

  }








  function onFilterClick(event) {
    event.cancelBubble = true;

    var attribute_id = event.rowData.attribute_id;
    var value_id = event.rowData.value_id;

    currentSearch.toggleRefinementValue(attribute_id, value_id, {
      silent: true });

    eaUtils.doProductSearch();
  }








  function onFilterHeaderClick(event) {
    event.cancelBubble = true;

    currentSearch.clearAllRefinementValues({
      silent: true });

    eaUtils.doProductSearch();
  }









  function initRefinementsUI() {
    logger.info('initRefinementsUI');
    var refinementSections = [];
    deinitRefinementControllers();

    var refinements = currentSearch.getRefinements(),
    attribute_id,
    template,
    controller,
    view,
    refinement;

    for (var i = 0, ii = refinements.length; i < ii; i++) {
      refinement = refinements[i];
      attribute_id = refinement.getAttributeId();


      if (attribute_id == 'cgid') {
        continue;
      }


      template = templateForRefinementAttribute(attribute_id);
      controller = Alloy.createController(template, {
        $model: refinement });

      refinementControllers.push(controller);
      view = controller.getView();

      if (view && controller.shouldBeVisible()) {
        refinementSections.push(view);
      }
    }


    $.refinementsTable.setData(refinementSections);
  }






  function initSelectedRefinementsUI() {
    logger.info('initSelectedRefinementsUI');
    var selectedRefinementsRows = [];

    var selectedRefinements = currentSearch.getSelectedRefinements();
    var keys = _.keys(selectedRefinements),
    key,
    value;
    var refinements = currentSearch.getRefinementsCollection();
    var refinement, values, value, value_id, value_ids, hit_count, row, label, view;

    var refinementValue = null;

    for (var k = 0, kk = keys.length; k < kk; k++) {
      key = keys[k];

      if (key == 'cgid') {
        continue;
      }

      value_id = selectedRefinements[key];
      refinement = refinements.where({ attribute_id: key })[0];
      if (!refinement) {
        return;
      }

      values = refinement.getValuesCollection();
      value_ids = value_id.split('|');

      for (var l = 0, ll = value_ids.length; l < ll; l++) {
        value_id = value_ids[l];
        value = values.where({ value: value_id })[0];
        if (!value) {
          logger.info('Could not find expected value! ' + value_id + ' in ' + JSON.stringify(values.toJSON()));
          continue;
        }
        hit_count = value.getHitCount();
        if (hit_count == 0) {
          continue;
        }

        refinementValue = currentSearch.getRefinementValue(key, value_id);

        row = Ti.UI.createTableViewRow({
          attribute_id: refinement.getAttributeId(),
          value_id: value_id,
          height: 50,
          layout: 'absolute',
          opacity: '1.0',
          selectedBackgroundColor: Alloy.Styles.accentColor });

        view = Ti.UI.createView({
          backgroundImage: Alloy.Styles.categoryOptionImage,
          opacity: '0.9',
          top: 0,
          left: 0,
          height: '100%',
          width: '100%' });

        label = Ti.UI.createLabel({
          text: String.format(_L('%s: %s'), refinement.getLabel(), refinementValue.getLabel()),
          left: 26,
          font: Alloy.Styles.detailLabelFont,
          color: Alloy.Styles.color.text.dark,
          highlightedColor: Alloy.Styles.color.text.dark });

        view.add(label);
        row.add(view);
        view = Ti.UI.createView({
          backgroundImage: Alloy.Styles.refinementClearImage,
          right: 12,
          top: 18,
          height: 16,
          width: 16,
          accessibilityLabel: 'clear_' + refinementValue.getLabel() });

        row.add(view);

        selectedRefinementsRows.push(row);
      }
    }


    $.appliedFiltersTable.setData(selectedRefinementsRows);
  }




  init();









  _.extend($, exports);
}

module.exports = Controller;