var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
		delete obj[key];
	}
	return arg;
}

function Controller() {

	require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'checkout/address/unavailableProductItemRow';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};







	$.__views.product_item_row = Ti.UI.createTableViewRow(
	{ layout: "vertical", selectionStyle: Ti.UI.iOS.TableViewCellSelectionStyle.NONE, height: Ti.UI.SIZE, id: "product_item_row" });

	$.__views.product_item_row && $.addTopLevelView($.__views.product_item_row);
	$.__views.product_line_item_container = Ti.UI.createView(
	{ top: 10, height: 210, width: 400, id: "product_line_item_container" });

	$.__views.product_item_row.add($.__views.product_line_item_container);
	$.__views.stock_availability_label = Ti.UI.createLabel(
	{ width: "100%", height: 25, color: Alloy.Styles.color.text.red, textAlign: "center", top: 0, font: Alloy.Styles.appBoldFont, id: "stock_availability_label", text: $model.__transform.stock_availability_message });

	$.__views.product_line_item_container.add($.__views.stock_availability_label);
	$.__views.item_container = Ti.UI.createView(
	{ height: Ti.UI.SIZE, width: "100%", layout: "horizontal", left: 0, top: 30, id: "item_container" });

	$.__views.product_line_item_container.add($.__views.item_container);
	$.__views.product_item = Alloy.createController('checkout/cart/productItem', { id: "product_item", scaleDown: true, __parentSymbol: $.__views.item_container });
	$.__views.product_item.setParent($.__views.item_container);
	$.__views.edit_delete_button_container = Ti.UI.createView(
	{ bottom: 10, right: 5, height: 58, width: 70, id: "edit_delete_button_container" });

	$.__views.product_line_item_container.add($.__views.edit_delete_button_container);
	$.__views.delete_button = Ti.UI.createButton(
	{ height: 55, width: 55, backgroundImage: Alloy.Styles.deleteButtonIcon, right: 10, id: "delete_button", accessibilityValue: "delete_button" });

	$.__views.edit_delete_button_container.add($.__views.delete_button);
	exports.destroy = function () {};




	_.extend($, $.__views);










	var logger = require('logging')('checkout:address:unavailableProductItemRow', getFullControllerPath($.__controllerPath));









	$.product_item_row.deinit = function () {
		logger.info('DEINIT called');
		$.product_item.deinit();
		$.product_item_row.update = null;
		$.stopListening();
		$.destroy();
	};









	$.product_item_row.update = function (product, pli, showDivider) {
		logger.info('update called');
		$.product_item.init(product, pli);
		if (_.isFunction(pli.hasMessage) && pli.hasMessage()) {
			$.product_line_item_container.setHeight($.product_line_item_container.getHeight() + 10);
		}
	};









	_.extend($, exports);
}

module.exports = Controller;