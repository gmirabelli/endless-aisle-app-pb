var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'product/components/productListSelectorDialog';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};




  $.currentProductList = Alloy.createCollection('productLists');


  $.__views.backdrop = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.color.background.transparentBlack30Percent, id: "backdrop" });

  $.__views.backdrop && $.addTopLevelView($.__views.backdrop);
  $.__views.main_container = Ti.UI.createView(
  { height: 400, width: 500, backgroundColor: Alloy.Styles.color.background.white, layout: "vertical", id: "main_container" });

  $.__views.backdrop.add($.__views.main_container);
  $.__views.title_container = Ti.UI.createView(
  { top: 0, height: 60, width: 320, backgroundColor: Alloy.Styles.color.background.white, layout: "vertical", id: "title_container" });

  $.__views.main_container.add($.__views.title_container);
  $.__views.title = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.accentColor, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, top: 15, font: Alloy.Styles.bigButtonFont, textid: "_Select_Wish_List", id: "title", accessibilityValue: "title" });

  $.__views.title_container.add($.__views.title);
  $.__views.separator_upper = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.accentColor, width: Ti.UI.FILL, height: 1, top: 0, id: "separator_upper" });

  $.__views.main_container.add($.__views.separator_upper);
  $.__views.list_view_container = Ti.UI.createView(
  { top: 0, height: 270, id: "list_view_container" });

  $.__views.main_container.add($.__views.list_view_container);
  $.__views.list_view = Ti.UI.createTableView(
  { backgroundColor: "white", id: "list_view" });

  $.__views.list_view_container.add($.__views.list_view);
  var __alloyId208 = Alloy.Collections['$.currentProductList'] || $.currentProductList;function __alloyId209(e) {if (e && e.fromAdapter) {return;}var opts = __alloyId209.opts || {};var models = __alloyId208.models;var len = models.length;var rows = [];for (var i = 0; i < len; i++) {var __alloyId202 = models[i];__alloyId202.__transform = transformPLI(__alloyId202);var __alloyId204 = Ti.UI.createTableViewRow(
      { height: 50, backgroundColor: "white", selectedBackgroundColor: Alloy.Styles.color.background.white, backgroundSelectedColor: Alloy.Styles.color.background.white, listId: __alloyId202.__transform.listId });

      rows.push(__alloyId204);
      var __alloyId205 = Ti.UI.createView(
      {});

      __alloyId204.add(__alloyId205);
      var __alloyId206 = Ti.UI.createLabel(
      { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: 10, right: 40, text: __alloyId202.__transform.listName });

      __alloyId205.add(__alloyId206);
      var __alloyId207 = Ti.UI.createImageView(
      { right: 10, height: 30, width: 30, image: Alloy.Styles.checkBoxImageWhite, visible: false, accessibilityValue: "check_box_checked" });

      __alloyId205.add(__alloyId207);
    }$.__views.list_view.setData(rows);};__alloyId208.on('fetch destroy change add remove reset', __alloyId209);onListSelect ? $.addListener($.__views.list_view, 'click', onListSelect) : __defers['$.__views.list_view!click!onListSelect'] = true;$.__views.separator_lower = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.accentColor, width: Ti.UI.FILL, height: 1, top: 0, id: "separator_lower" });

  $.__views.main_container.add($.__views.separator_lower);
  $.__views.controls_wrapper = Ti.UI.createView(
  { top: 15, layout: "horizontal", height: Ti.UI.SIZE, width: Ti.UI.SIZE, bottom: 15, id: "controls_wrapper" });

  $.__views.main_container.add($.__views.controls_wrapper);
  $.__views.cancel = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, backgroundImage: Alloy.Styles.buttons.secondary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.secondary.backgroundDisabledImage, color: Alloy.Styles.buttons.secondary.color, disabledColor: Alloy.Styles.buttons.secondary.disabledColor, height: 40, width: 200, titleid: "_Cancel", id: "cancel", accessibilityValue: "cancel" });

  $.__views.controls_wrapper.add($.__views.cancel);
  dismiss ? $.addListener($.__views.cancel, 'click', dismiss) : __defers['$.__views.cancel!click!dismiss'] = true;$.__views.apply = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.buttons.primary.color, disabledColor: Alloy.Styles.buttons.primary.disabledColor, backgroundImage: Alloy.Styles.buttons.primary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.primary.backgroundDisabledImage, height: 40, width: 200, titleid: "_Apply", left: 20, id: "apply", accessibilityValue: "apply" });

  $.__views.controls_wrapper.add($.__views.apply);
  dismiss ? $.addListener($.__views.apply, 'click', dismiss) : __defers['$.__views.apply!click!dismiss'] = true;exports.destroy = function () {__alloyId208 && __alloyId208.off('fetch destroy change add remove reset', __alloyId209);};




  _.extend($, $.__views);









  var args = arguments[0] || {};
  var collection = args.wishListCollection;
  var currentlySelectedListId;




  exports.deinit = deinit;









  function deinit() {
    $.cancel.removeEventListener('click', dismiss);
    $.apply.removeEventListener('click', dismiss);
    $.list_view.removeEventListener('click', onListSelect);
    $.stopListening();
    $.destroy();
  }










  function onListSelect(event) {
    if (event.row) {
      var row = event.row;
      setSelectedRowBGColor(row);
      currentlySelectedListId = row.listId;
    }
  }







  function dismiss(event) {
    if (event.source.id === 'apply') {
      $.trigger('productListSelectorDialog:continue', {
        listId: currentlySelectedListId });

    } else {
      $.trigger('productListSelectorDialog:dismiss');
    }
  }







  function setSelectedRowBGColor(row) {

    _.each($.list_view.getSections()[0].getRows(), function (cRow) {
      var checkBox = _.find(cRow.getChildren()[0].getChildren(), function (child) {
        return child.getApiName() == 'Ti.UI.ImageView';
      });
      if (cRow.listId == row.listId) {
        if (checkBox) {

          checkBox.setVisible(true);
        }
        row.setBackgroundColor(Alloy.Styles.selectLists.selectedOptionBackgroundColor);
      } else {
        if (checkBox) {

          checkBox.setVisible(false);
        }
        cRow.setBackgroundColor(Alloy.Styles.color.background.white);
      }
    });
  }







  function transformPLI(model) {
    return {
      listId: model.getId(),
      listName: model.getName() };

  }




  $.currentProductList.reset(collection);





  __defers['$.__views.list_view!click!onListSelect'] && $.addListener($.__views.list_view, 'click', onListSelect);__defers['$.__views.cancel!click!dismiss'] && $.addListener($.__views.cancel, 'click', dismiss);__defers['$.__views.apply!click!dismiss'] && $.addListener($.__views.apply, 'click', dismiss);



  _.extend($, exports);
}

module.exports = Controller;