var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'components/dropdown';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.container = Ti.UI.createView(
  { borderWidth: Alloy.Styles.selectLists.borderWidth, borderColor: Alloy.Styles.selectLists.borderColor, id: "container" });

  $.__views.container && $.addTopLevelView($.__views.container);
  $.__views.menuBox = Ti.UI.createTableView(
  { id: "menuBox" });

  $.__views.container.add($.__views.menuBox);
  handleMenuBoxClick ? $.addListener($.__views.menuBox, 'click', handleMenuBoxClick) : __defers['$.__views.menuBox!click!handleMenuBoxClick'] = true;exports.destroy = function () {};




  _.extend($, $.__views);










  var logger = require('logging')('components:dropdown', getFullControllerPath($.__controllerPath));

  var args = arguments[0] || {};
  var items = args.items;
  var textField = args.textField;
  var valueField = args.valueField;
  var selectableField = args.selectableField;
  var style = args.style;




  exports.deinit = deinit;









  function init() {
    $.menuBox.setSeparatorStyle(Ti.UI.TABLE_VIEW_SEPARATOR_STYLE_NONE);
    $.menuBox.setTableSeparatorInsets({
      left: 0,
      right: 0 });

    $.menuBox.setRowHeight(Alloy.Styles.selectLists.height);


    var rows = [];
    var filteredItems = filterItems(items),
    selectedIndex = -1;
    _.each(filteredItems, function (item, index) {
      var itemJSON = transformItem(item);

      if (itemJSON.selected) {
        itemJSON.font = style && style.selectedFont ? style.selectedFont : Alloy.Styles.selectLists.font;
        selectedIndex = index;
      } else {
        itemJSON.font = style && style.unselectedFont ? style.unselectedFont : Alloy.Styles.selectLists.selectedOptionFont;
      }
      if (itemJSON.selectable && itemJSON.enabled && itemJSON.selected) {

        itemJSON.backgroundColor = style && style.selectedOptionBackgroundColor ? style.selectedOptionBackgroundColor : Alloy.Styles.selectLists.selectedOptionBackgroundColor;
        itemJSON.color = style && style.selectedOptionColor ? style.selectedOptionColor : Alloy.Styles.selectLists.selectedOptionColor;
      } else if (itemJSON.selectable && itemJSON.enabled && !itemJSON.selected) {

        itemJSON.backgroundColor = style && style.backgroundColor ? style.backgroundColor : Alloy.Styles.selectLists.backgroundColor;
        itemJSON.color = style && style.color ? style.color : Alloy.Styles.selectLists.color;
      } else if (!itemJSON.enabled && itemJSON.selected) {

        itemJSON.backgroundColor = style && style.selectedOptionBackgroundColor ? style.selectedOptionBackgroundColor : Alloy.Styles.selectLists.selectedOptionBackgroundColor;
        itemJSON.color = style && style.selecetdOptionColor ? style.selecetdOptionColor : Alloy.Styles.selectLists.selectedOptionColor;
      } else if (!itemJSON.enabled && !itemJSON.selected) {

        itemJSON.backgroundColor = style && style.disabledBackgroundColor ? style.disabledBackgroundColor : Alloy.Styles.selectLists.disabledBackgroundColor;
        itemJSON.color = style && style.disabledColor ? style.disabledColor : Alloy.Styles.selectLists.disabledColor;
      } else if (!itemJSON.selectable && !itemJSON.selected) {

        itemJSON.backgroundColor = style && style.disabledBackgroundColor ? style.disabledBackgroundColor : Alloy.Styles.selectLists.disabledBackgroundColor;
        itemJSON.color = style && style.disabledColor ? style.disabledColor : Alloy.Styles.selectLists.disabledColor;
      } else {

        itemJSON.backgroundColor = style && style.selectedOptionBackgroundColor ? style.selectedOptionBackgroundColor : Alloy.Styles.selectLists.selectedOptionBackgroundColor;
        itemJSON.color = style && style.selectedOptionColor ? style.selectedOptionColor : Alloy.Styles.selectLists.selectedOptionColor;
      }
      itemJSON.selectedBackgroundColor = style && style.selectedBackgroundColor ? style.selectedBackgroundColor : Alloy.Styles.accentColor;
      rows.push(itemJSON);
    });
    $.menuBox.setData(rows);
    if (selectedIndex > -1) {
      $.menuBox.scrollToIndex(selectedIndex - 1);
    }
  }






  function deinit() {
    logger.info('DEINIT called');

    $.menuBox.removeEventListener('click', handleMenuBoxClick);
    $.stopListening();
    $.destroy();
  }










  function filterItems(items) {
    return items.models;
  }







  function transformItem(item) {
    var itemJSON = item.toJSON();

    itemJSON.title = '' + itemJSON[textField];
    itemJSON.selectable = selectableField ? itemJSON[selectableField] : true;
    itemJSON.enabled = itemJSON.hasOwnProperty('enabled') ? itemJSON.enabled : true;
    itemJSON.item_id = '' + itemJSON[valueField];
    itemJSON.accessibilityValue = itemJSON[valueField];
    if (item.hasOwnProperty('selected')) {
      itemJSON.selected = item.selected;
    }

    return itemJSON;
  }










  function handleMenuBoxClick(event) {
    logger.info('handleMenuBoxClick');
    var itemId = event.rowData.item_id;


    var matchingItems = items.filter(function (item) {
      var itemJSON = item.toJSON();
      return itemJSON[valueField] == itemId;
    });
    var item = matchingItems.length ? matchingItems[0] : null;
    $.trigger('itemSelected', item.toJSON());
  }




  init();

  if (style && style.border_color) {
    $.container.setBorderColor(style.border_color);
  }





  __defers['$.__views.menuBox!click!handleMenuBoxClick'] && $.addListener($.__views.menuBox, 'click', handleMenuBoxClick);



  _.extend($, exports);
}

module.exports = Controller;