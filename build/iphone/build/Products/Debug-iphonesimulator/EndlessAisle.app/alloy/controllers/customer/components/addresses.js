var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'customer/components/addresses';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};




  $.customerAddresses = Alloy.createCollection('customerAddress');


  $.__views.addresses_contents = Ti.UI.createView(
  { layout: "vertical", height: Ti.UI.SIZE, width: "100%", left: 0, id: "addresses_contents" });

  $.__views.addresses_contents && $.addTopLevelView($.__views.addresses_contents);
  $.__views.new_address_row = Ti.UI.createView(
  { height: 104, id: "new_address_row" });

  $.__views.addresses_contents.add($.__views.new_address_row);
  $.__views.new_address_button = Ti.UI.createButton(
  { width: 351, height: 42, backgroundImage: Alloy.Styles.primaryButtonImage, color: Alloy.Styles.buttons.primary.color, font: Alloy.Styles.buttonFont, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, top: 30, bottom: 30, titleid: "_Add_New_Address", id: "new_address_button", accessibilityValue: "new_address_button" });

  $.__views.new_address_row.add($.__views.new_address_button);
  $.__views.__alloyId135 = Ti.UI.createView(
  { height: 1, width: "100%", backgroundColor: "#dddddd", id: "__alloyId135" });

  $.__views.addresses_contents.add($.__views.__alloyId135);
  $.__views.no_addresses = Ti.UI.createLabel(
  { width: 0, height: 0, color: Alloy.Styles.color.text.dark, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.tabFont, visible: false, id: "no_addresses" });

  $.__views.addresses_contents.add($.__views.no_addresses);
  $.__views.addresses_container = Ti.UI.createView(
  { height: 750, id: "addresses_container" });

  $.__views.addresses_contents.add($.__views.addresses_container);
  $.__views.address_list = Ti.UI.createScrollView(
  { bottom: 200, layout: "vertical", contentHeight: "auto", showVerticalScrollIndicator: "true", showHorizontalScrollIndicator: "true", id: "address_list", dataTransform: "transformAddress" });

  $.__views.addresses_container.add($.__views.address_list);
  var __alloyId139 = Alloy.Collections['$.customerAddresses'] || $.customerAddresses;function __alloyId140(e) {if (e && e.fromAdapter) {return;}var opts = __alloyId140.opts || {};var models = __alloyId139.models;var len = models.length;var children = $.__views.address_list.children;for (var d = children.length - 1; d >= 0; d--) {$.__views.address_list.remove(children[d]);}for (var i = 0; i < len; i++) {var __alloyId136 = models[i];__alloyId136.__transform = transformAddress(__alloyId136);var __alloyId138 = Alloy.createController('customer/components/addressTile', { $model: __alloyId136, __parentSymbol: $.__views.address_list });
      __alloyId138.setParent($.__views.address_list);
    }};__alloyId139.on('fetch destroy change add remove reset', __alloyId140);exports.destroy = function () {__alloyId139 && __alloyId139.off('fetch destroy change add remove reset', __alloyId140);};




  _.extend($, $.__views);










  var currentCustomer = Alloy.Models.customer;
  var customerAddresses = Alloy.Models.customerAddress;
  var toCountryName = require('EAUtils').countryCodeToCountryName;
  var logger = require('logging')('customer:components:addresses', getFullControllerPath($.__controllerPath));




  $.new_address_button.addEventListener('click', onNewAddressButtonClick);
  $.address_list.addEventListener('click', onAddressListClick);




  $.listenTo(currentCustomer, 'customer:clear', function () {
    logger.info('currentCustomer customer:clear event listener');
    $.customerAddresses.reset();
  });




  exports.init = init;
  exports.deinit = deinit;










  function init() {
    logger.info('Calling INIT');
    var deferred = new _.Deferred();
    currentCustomer.addresses.fetchAddresses(currentCustomer.getCustomerId()).done(function () {
      if (!currentCustomer.hasAddresses()) {
        logger.info('no address');
        $.addresses_contents.setTop(0);
        $.addresses_container.setWidth(0);
        $.addresses_container.setHeight(0);
        $.addresses_container.setVisible(false);
        $.new_address_row.setTop(0);

        $.no_addresses.setWidth('100%');
        $.no_addresses.setTop(70);
        $.no_addresses.setLeft(150);
        $.no_addresses.setHeight(50);
        $.no_addresses.setVisible(true);
        $.no_addresses.setText(_L('There are no saved addresses.'));
      } else {
        $.no_addresses.setWidth(0);
        $.no_addresses.setTop(0);
        $.no_addresses.setHeight(0);
        $.no_addresses.setVisible(false);

        $.new_address_row.setTop(0);
        $.addresses_container.setVisible(true);
        $.addresses_container.setWidth('100%');
        $.addresses_container.setHeight(750);

        $.customerAddresses.reset(currentCustomer.addresses.getAddressesOfType('customer'));
      }
      deferred.resolve();
    }).fail(function () {
      logger.info('cannot retrieve addresses');
      deferred.reject();
    });
    return deferred.promise();
  }






  function deinit() {
    logger.info('DEINIT Called');
    $.new_address_button.removeEventListener('click', onNewAddressButtonClick);
    $.address_list.removeEventListener('click', onAddressListClick);
    $.stopListening();
    $.destroy();
  }









  function showDeleteAddressPopover(address_name) {
    Alloy.Dialog.showConfirmationDialog({
      messageString: String.format(_L('Do you really want to delete this address?'), address_name),
      titleString: _L('Delete Address'),
      okButtonString: _L('Delete'),
      okFunction: function () {
        var promise = currentCustomer.addresses.deleteAddress(address_name, currentCustomer.getCustomerId());
        Alloy.Router.showActivityIndicator(promise);
        promise.done(function () {
          notify(String.format(_L('Address \'%s\' successfully deleted.'), address_name));
          $.addresses_contents.fireEvent('route', {
            page: 'addresses' });

        }).fail(function () {
          notify(String.format(_L('Could not delete address \'%s\'.'), address_name), {
            preventAutoClose: true });

        });
      } });

  }







  function transformAddress(model) {
    logger.info('transform address');
    var country_code = model.getCountryCode().toUpperCase();
    var country_name = toCountryName(country_code);

    var city_state_zip = model.getStateCode() ? model.getCity() + ', ' + model.getStateCode() + ' ' + model.getPostalCode() : model.getCity() + ' ' + (model.getPostalCode() ? model.getPostalCode() : '');
    return {
      full_name: model.getFullName(),
      address_name: model.getAddressId(),
      address1: model.getAddress1(),
      address2: model.getAddress2(),
      city: model.getCity(),
      state_code: model.getStateCode(),
      postal_code: model.getPostalCode(),
      city_state_zip: model.getStateCode() ? model.getCity() + ', ' + model.getStateCode() + ' ' + model.getPostalCode() : model.getCity() + ' ' + (model.getPostalCode() ? model.getPostalCode() : ''),
      country: country_name };

  }










  function onNewAddressButtonClick(event) {
    logger.info('new_address_button click event listener');
    event.cancelBubble = true;
    $.addresses_contents.fireEvent('route', {
      page: 'address' });

  }







  function onAddressListClick(event) {
    if (event.source.id == 'address_edit_button') {
      $.addresses_contents.fireEvent('route', {
        page: 'address',
        address_id: event.source.address_id });

    } else if (event.source.id == 'address_delete_button') {
      showDeleteAddressPopover(event.source.address_name);
    }
  }









  _.extend($, exports);
}

module.exports = Controller;