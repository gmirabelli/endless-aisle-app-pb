var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'product/components/detailHeader';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.pdp_header = Ti.UI.createView(
  { layout: "vertical", height: Ti.UI.SIZE, left: 7, id: "pdp_header" });

  $.__views.pdp_header && $.addTopLevelView($.__views.pdp_header);
  $.__views.product_name = Ti.UI.createLabel(
  { width: "100%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.featuredProductFont, top: 20, id: "product_name", accessibilityValue: "product_name" });

  $.__views.pdp_header.add($.__views.product_name);
  $.__views.pdp_info_container = Ti.UI.createView(
  { layout: "absolute", height: Ti.UI.SIZE, width: "100%", top: 10, id: "pdp_info_container" });

  $.__views.pdp_header.add($.__views.pdp_info_container);
  $.__views.product_id = Alloy.createController('product/components/productId', { color: Alloy.Styles.color.text.darkest, font: Alloy.Styles.detailTextFont, width: 240, left: 3, ellipsize: true, height: Ti.UI.SIZE, id: "product_id", __parentSymbol: $.__views.pdp_info_container });
  $.__views.product_id.setParent($.__views.pdp_info_container);
  $.__views.pdp_ratings = Ti.UI.createView(
  { layout: "horizontal", height: Ti.UI.SIZE, width: Ti.UI.SIZE, left: 320, id: "pdp_ratings" });

  $.__views.pdp_info_container.add($.__views.pdp_ratings);
  $.__views.pdp_rating_label = Ti.UI.createLabel(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, color: Alloy.Styles.color.text.darkest, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: Alloy.Styles.detailTextFont, textid: "_Product_Rating_", id: "pdp_rating_label", accessibilityValue: "pdp_rating_label" });

  $.__views.pdp_ratings.add($.__views.pdp_rating_label);
  $.__views.stars = Ti.UI.createView(
  { width: Ti.UI.SIZE, height: Ti.UI.SIZE, layout: "horizontal", id: "stars" });

  $.__views.pdp_ratings.add($.__views.stars);
  $.__views.pricing_container = Ti.UI.createView(
  { layout: "absolute", height: Ti.UI.SIZE, width: "100%", id: "pricing_container" });

  $.__views.pdp_header.add($.__views.pricing_container);
  $.__views.pricing = Alloy.createController('product/components/productPricing', { id: "pricing", __parentSymbol: $.__views.pricing_container });
  $.__views.pricing.setParent($.__views.pricing_container);
  exports.destroy = function () {};




  _.extend($, $.__views);










  $.currentProduct = Alloy.Models.product;
  var logger = require('logging')('product:components:detailHeader', getFullControllerPath($.__controllerPath));





  exports.init = init;
  exports.render = render;
  exports.deinit = deinit;
  exports.setProductID = setProductID;
  exports.setPrice = setPrice;









  function init() {
    logger.info('init called');
    $.pricing.init({
      model: $.currentProduct });

    $.product_id.init();
    $.render();
  }






  function render() {
    logger.info('render called');
    var id = $.currentProduct.getId();
    setProductID();
    $.product_name.setText($.currentProduct.getName());

    $.pdp_ratings.setVisible(Alloy.CFG.product.ratings.max_rating > 0);

    var rating = Alloy.CFG.product.ratings.attribute_name ? $.currentProduct.get(Alloy.CFG.product.ratings.attribute_name) : 0;
    rating = rating || 0;

    var fraction = rating % 1;

    if (fraction) {
      rating = Math.floor(rating);
    }


    for (var i = 0; i < Alloy.CFG.product.ratings.max_rating; i++) {
      var imagepath = null;
      if (rating > i) {
        imgpath = Alloy.CFG.product.ratings.starFull;
      } else if (fraction) {
        imgpath = Alloy.CFG.product.ratings.starHalf;
        fraction = 0;
      } else {
        imgpath = Alloy.CFG.product.ratings.starNone;
      }


      var imageView = Ti.UI.createImageView({
        image: imgpath,
        accessibilityValue: 'rating_stars',
        width: 20,
        height: 20 });

      Alloy.Globals.getImageViewImage(imageView, imgpath);
      $.stars.add(imageView);
    }
  }






  function deinit() {
    logger.info('deinit called');
    $.stopListening();
    $.pricing.deinit();
    $.product_id.deinit();
    removeAllChildren($.stars);
    $.destroy();
  }










  function setProductID(new_id) {
    logger.info('setProductID called with ' + new_id);
    $.product_id.setId(new_id || _L('n/a'));
  }







  function setPrice(new_price) {
    logger.info('setPrice called with ' + new_price);
    $.pricing.setPrice(new_price);
  }









  _.extend($, exports);
}

module.exports = Controller;