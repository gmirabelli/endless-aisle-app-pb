var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'checkout/payments/paymentTerminal';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.payment_terminal_window = Ti.UI.createView(
  { backgroundColor: Alloy.Styles.color.background.transparent, id: "payment_terminal_window" });

  $.__views.payment_terminal_window && $.addTopLevelView($.__views.payment_terminal_window);
  $.__views.backdrop = Ti.UI.createView(
  { top: 0, left: 0, width: 1024, height: 768, opacity: 0.55, backgroundColor: Alloy.Styles.color.background.black, id: "backdrop" });

  $.__views.payment_terminal_window.add($.__views.backdrop);
  $.__views.payment_terminal_container = Ti.UI.createView(
  { layout: "vertical", width: "50%", height: Ti.UI.SIZE, backgroundColor: Alloy.Styles.color.background.white, id: "payment_terminal_container" });

  $.__views.payment_terminal_window.add($.__views.payment_terminal_container);
  $.__views.payment_image = Ti.UI.createImageView(
  { image: "images/checkout/paymentTerminal.png", height: 40, width: 40, top: 20, id: "payment_image", accessibilityLabel: "payment_image" });

  $.__views.payment_terminal_container.add($.__views.payment_image);
  $.__views.payment_terminal_label = Ti.UI.createLabel(
  { width: "90%", height: Ti.UI.SIZE, color: Alloy.Styles.color.text.medium, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, top: 20, font: Alloy.Styles.tabFont, textid: "_Please_process_payment_through_the_payment_terminal_", id: "payment_terminal_label", accessibilityValue: "payment_terminal_label" });

  $.__views.payment_terminal_container.add($.__views.payment_terminal_label);
  $.__views.button_container = Ti.UI.createView(
  { layout: "horizontal", height: Ti.UI.SIZE, width: Ti.UI.SIZE, top: 40, bottom: 20, id: "button_container" });

  $.__views.payment_terminal_container.add($.__views.button_container);
  $.__views.cancel_button = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, backgroundImage: Alloy.Styles.buttons.secondary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.secondary.backgroundDisabledImage, color: Alloy.Styles.buttons.secondary.color, disabledColor: Alloy.Styles.buttons.secondary.disabledColor, height: 40, width: 200, titleid: "_Cancel", id: "cancel_button", accessibilityValue: "payment_cancel_button" });

  $.__views.button_container.add($.__views.cancel_button);
  $.__views.manual_entry_button = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.buttons.primary.color, disabledColor: Alloy.Styles.buttons.primary.disabledColor, backgroundImage: Alloy.Styles.buttons.primary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.primary.backgroundDisabledImage, height: 40, width: 200, titleid: "_Manual_Entry", left: 10, id: "manual_entry_button", accessibilityValue: "manual_entry_button" });

  $.__views.button_container.add($.__views.manual_entry_button);
  $.__views.retry_button = Ti.UI.createButton(
  { textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, color: Alloy.Styles.buttons.primary.color, disabledColor: Alloy.Styles.buttons.primary.disabledColor, backgroundImage: Alloy.Styles.buttons.primary.backgroundImage, backgroundDisabledImage: Alloy.Styles.buttons.primary.backgroundDisabledImage, height: 40, width: 0, titleid: "_Retry", left: 10, visible: false, id: "retry_button", accessibilityValue: "retry_button" });

  $.__views.button_container.add($.__views.retry_button);
  exports.destroy = function () {};




  _.extend($, $.__views);










  var paymentTerminal = require(Alloy.CFG.devices.payment_terminal_module);
  var logger = require('logging')('checkout:payments:paymentTerminal', getFullControllerPath($.__controllerPath));

  var currentOptions = null;
  var retryData = null;






  $.cancel_button.addEventListener('click', onCancelClick);

  $.manual_entry_button.addEventListener('click', onManualEntryClick);

  $.retry_button.addEventListener('click', onRetryClick);

  $.listenTo(Alloy.eventDispatcher, 'payment_terminal:manual_card_data_on', onManualCardOn);

  $.listenTo(Alloy.eventDispatcher, 'payment_terminal:dismiss', dismiss);

  $.listenTo(Alloy.eventDispatcher, 'payment_terminal:disable_cancel', disableCancelButton);



  Ti.App.addEventListener('payment_terminal:enable_retry', enableRetry);




  exports.init = init;
  exports.deinit = deinit;
  exports.dismiss = dismiss;










  function init(args) {
    retryData = null;
    $.manual_entry_button.setEnabled(true);

    if (paymentTerminal.supports('solicited')) {
      args.options = {
        solicited: true };

    }

    currentOptions = args;

    logger.info('cancelling pending request(s)');
    paymentTerminal.cancelPayment();



    logger.info('requesting standard payment (not manual)');
    paymentTerminal.acceptPayment(args);

    if (!paymentTerminal.supports('payment_terminal_cancel')) {
      $.cancel_button.hide();
      $.cancel_button.setWidth(0);
    }

    if (!args.hideManualEntry && paymentTerminal.supports('manual_entry')) {
      $.manual_entry_button.show();
    } else {
      $.manual_entry_button.hide();
      $.manual_entry_button.setWidth(0);
    }
  }






  function deinit() {
    retryData = null;
    $.cancel_button.removeEventListener('click', onCancelClick);
    $.manual_entry_button.removeEventListener('click', onManualEntryClick);
    $.stopListening(Alloy.eventDispatcher, 'payment_terminal:manual_card_data_on', onManualCardOn);
    $.stopListening(Alloy.eventDispatcher, 'payment_terminal:dismiss', dismiss);
    $.stopListening(Alloy.eventDispatcher, 'payment:payment_listeners_stopped', paymentListenersStopped);

    $.stopListening(Alloy.eventDispatcher, 'payment_terminal:disable_cancel', disableCancelButton);


    Ti.App.removeEventListener('payment_terminal:enable_retry', enableRetry);
    $.stopListening();
    $.destroy();
  }









  function dismiss() {
    $.trigger('payment_terminal:dismiss');
  }







  function enableRetry(event) {
    logger.info('paymentTerminal: enabling retry');
    retryData = event.payment_data;
    $.payment_terminal_label.setText(event.message);
    $.cancel_button.setEnabled(true);
    $.retry_button.setVisible(true);
    $.retry_button.setWidth($.cancel_button.getWidth());
    if (paymentTerminal.supports('manual_entry')) {
      $.manual_entry_button.hide();
      $.manual_entry_button.setWidth(0);
    }
  }









  function onCancelClick() {
    $.cancel_button.setEnabled(false);

    $.listenTo(Alloy.eventDispatcher, 'payment:payment_listeners_stopped', paymentListenersStopped);

    logger.info('paymentTerminal: cancel clicked');
    Alloy.eventDispatcher.trigger('payment:stop_payment_listening');

    logger.info('paymentTerminal: cancelPayment on payment terminal');
    paymentTerminal.cancelPayment(true);

    var orderNo = Alloy.Models.basket.getOrderNo();
    retryData = null;

    setTimeout(function () {
      logger.info('paymentTerminal: cancelServerTransaction');
      paymentTerminal.cancelServerTransaction({
        order_no: orderNo });

    }, 5000);
  }






  function paymentListenersStopped() {
    $.stopListening(Alloy.eventDispatcher, 'payment:payment_listeners_stopped', paymentListenersStopped);
    dismiss();
  }






  function disableCancelButton() {
    $.cancel_button.setEnabled(false);
  }






  function onManualEntryClick() {
    $.manual_entry_button.setEnabled(false);
    paymentTerminal.cancelPayment({
      sendEvent: 0 });


    setTimeout(function () {
      paymentTerminal.acceptPayment({
        amount: currentOptions.amount,
        options: {
          manual: true } });


    }, 3000);

  }






  function onManualCardOn() {
    $.manual_entry_button.setTitle(_L('Manual Entry (on)'));
  }






  function onRetryClick() {
    if (retryData) {
      logger.info('Retrying payment approved event: ' + JSON.stringify(retryData));
      var eventType = Alloy.CFG.devices.payment_terminal_module == 'adyenDevice' ? 'payment:cc_approved' : 'payment:credit_card_data';
      Alloy.eventDispatcher.trigger(eventType, retryData);
      $.retry_button.setVisible(false);
      $.retry_button.setWidth(0);
      $.payment_terminal_label.setText(_L('Retrying attempt to complete order'));
      retryData = null;
    }
  }









  _.extend($, exports);
}

module.exports = Controller;