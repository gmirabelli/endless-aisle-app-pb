

module.exports = function (loggingCategory, loggingComponent) {

    var logging = {};

    var categoryString = loggingCategory;
    var component = loggingComponent;

    logging.log = function (cat, message) {
        var logCats = getLoggableCategories();
        var loggableCategories = logCats.loggableCategories;
        var loggableSubcategories = logCats.loggableSubcategories;
        var category, subCategoryArray;
        var shouldLogSubcategory = false;
        var logStamp;

        var index = cat.indexOf(':');
        if (index != -1) {
            category = cat.substring(0, index);
            subCategoryArray = cat.substring(index + 1).split(':');
        } else {
            category = cat;
        }

        if (!message) {
            message = category;
            category = 'default';
        }

        var shouldLog = !subCategoryArray && loggableCategories.indexOf(category) > -1 || loggableCategories.indexOf('all') > -1 || category == 'error' || loggableCategories.indexOf('memory') > -1;

        if (!shouldLog && subCategoryArray) {
            if (loggableCategories.indexOf(category) != -1) {
                shouldLog = shouldLogSubcategory = true;
            } else {
                var subCatArr = loggableSubcategories[category];

                if (subCatArr) {
                    for (var idx = 0; idx < subCatArr.length; idx++) {
                        var directoryLevel = null;

                        if (subCategoryArray) {
                            directoryLevel = subCategoryArray.indexOf(subCatArr[idx][0]);
                        }

                        if (subCatArr[idx].length == 1 && subCategoryArray && directoryLevel > -1 && directoryLevel < subCategoryArray.length - 1) {
                            shouldLog = shouldLogSubcategory = true;
                            break;
                        } else {
                            if (_.isEqual(subCatArr[idx], subCategoryArray)) {
                                shouldLog = shouldLogSubcategory = true;
                                break;
                            }
                        }
                    }
                }

                if (category == 'checkout' && subCategoryArray && subCategoryArray.indexOf('payments') > -1) {
                    shouldLog = shouldLogSubcategory = true;
                }
            }
        }

        if (shouldLog) {
            if (typeof moment === 'function') {
                logStamp = '[' + moment().format('HH:mm:ss.SSS') + ']';
            } else {
                logStamp = '';
            }
            logStamp += ' [' + category + (subCategoryArray ? ':' + this.arrayToString(subCategoryArray) : '') + ']' + (component ? ' ' + component : '');
            if (category == 'error') {
                Ti.API.error(logStamp, message);
            } else if (category == 'trace') {
                var elapsed,
                    now = new Date().getTime();
                if (lastTrace > 0) {
                    elapsed = now - lastTrace;
                } else {
                    elapsed = '---';
                }

                lastTrace = now;

                Ti.API.trace(logStamp, 'elapsed: ' + elapsed, message);
            } else if (loggableCategories.indexOf('all') > -1 || !subCategoryArray && loggableCategories.indexOf(category) > -1 || shouldLogSubcategory) {
                if (Ti.App.deployType === 'production') {
                    swissArmyUtils.eaLog(logStamp + ' ' + message);
                } else {
                    Ti.API.debug(logStamp, message);
                }
            }
            if (loggableCategories != Alloy.CFG.loggableCategories) {
                supportLog(logStamp + ' ' + message);
            }
            if (loggableCategories.indexOf('memory') > -1) {
                var memChange,
                    nowMem = Ti.Platform.availableMemory;
                if (lastMemory > 0) {
                    memChange = lastMemory - nowMem;
                } else {
                    memChange = '---';
                }
                lastMemory = nowMem;
                Ti.API.trace(logStamp, 'Available Mem: ' + nowMem + ' memChange: ' + memChange);
            }
        }
    };

    logging.secureLog = function (text, cat) {
        if (Ti.App.deployType !== 'production') {
            if (cat) {
                logging.log(cat, text);
            } else {
                logging.info(text);
            }
        }
    };

    logging.info = function (message) {
        this.log(categoryString, message);
    };

    logging.trace = function (message) {
        this.log('trace', message);
    };

    logging.error = function (message) {
        this.log('error', message);
    };

    logging.arrayToString = function (arr) {
        var result = '';
        _.each(arr, function (str) {
            result += str + ':';
        });
        return result.substring(0, result.length - 1);
    };

    return logging;
};