

var logger = require('logging')('application:timers', 'app/lib/timers');

(function () {
    var appSessionTimer = null;
    var sessionDialogTimer = null;

    var serverSessionTimer = null;

    Alloy.eventDispatcher.listenTo(Alloy.eventDispatcher, 'configurations:postlogin', onConfigsPostLogin);
    Alloy.eventDispatcher.listenTo(Alloy.eventDispatcher, 'configurations:unload', onConfigsUnload);

    Alloy.eventDispatcher.listenTo(Alloy.eventDispatcher, 'session:renew', renewSessionTimer);

    function associateTimeout() {
        logger.info('Client timeout occurred for id ' + appSessionTimer);
        var currentAssociate = Alloy.Models.associate;
        if (currentAssociate && currentAssociate.isLoggedIn()) {
            clearInterval(appSessionTimer);
            appSessionTimer = null;
            Alloy.Router.hideHamburgerMenu();
            var sessionConfirmDialog = Alloy.Dialog.showCustomDialog({
                controllerPath: 'components/sessionTimeoutConfirmation',
                continueEvent: 'session_timeout_confirmation:continue',
                continueFunction: function () {
                    startAppSessionTimer();
                },
                cancelEvent: 'session_timeout_confirmation:end_session',
                cancelFunction: function () {
                    Alloy.Router.associateLogout();
                }
            });
            sessionDialogTimer = setTimeout(function () {
                sessionConfirmDialog.dismiss();
            }, Alloy.CFG.session_timeout_dialog_display_time);
        }
    }

    function sessionRenewTimeout() {
        logger.info('Server Session Keep-Alive PING');
        require('Validations').validateDevices(true);
    }

    function renewSessionTimer() {
        var currentAssociate = Alloy.Models.associate;
        if (appSessionTimer && currentAssociate && currentAssociate.isLoggedIn()) {
            startAppSessionTimer();
        }
    }

    function startAppSessionTimer() {
        logger.info('Starting Client Session Timeout for ' + Alloy.CFG.session_timeout);
        stopAppSessionTimer();
        appSessionTimer = setInterval(associateTimeout, Alloy.CFG.session_timeout);
        logger.info('Client Session new timer id ' + appSessionTimer);
    }

    function stopAppSessionTimer() {
        logger.info('Stopping Client Session Timeout');
        if (appSessionTimer) {
            logger.info('Client Session clear timer id ' + appSessionTimer);
            clearInterval(appSessionTimer);
            appSessionTimer = null;
        }
        if (sessionDialogTimer) {
            clearTimeout(sessionDialogTimer);
            sessionDialogTimer = null;
        }
    }

    function startServerSessionKeepAlive() {
        logger.info('Starting Server Session Keep-Alive PING for ' + Alloy.CFG.session_keep_alive);
        if (Alloy.CFG.session_keep_alive) {
            serverSessionTimer = setInterval(sessionRenewTimeout, Alloy.CFG.session_keep_alive);
        }
    }

    function stopServerSessionKeepAlive() {
        logger.info('Stopping Server Session Keep-Alive PING');
        if (serverSessionTimer) {
            clearInterval(serverSessionTimer);
            serverSessionTimer = null;
        }
    }

    function onConfigsPostLogin() {
        startServerSessionKeepAlive();
        startAppSessionTimer();
    }

    function onConfigsUnload() {
        stopServerSessionKeepAlive();
        stopAppSessionTimer();
    }
})();