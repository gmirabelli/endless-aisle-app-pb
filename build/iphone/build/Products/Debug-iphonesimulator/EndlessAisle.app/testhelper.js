

exports.equals = equals;
exports.isTrue = isTrue;
exports.isFalse = isFalse;
exports.isUndefined = isUndefined;
exports.isNotUndefined = isNotUndefined;
exports.isNull = isNull;
exports.isNotNull = isNotNull;
exports.failure = failure;
exports.functionsDefined = functionsDefined;
exports.testDynamicNumberFunctions = testDynamicNumberFunctions;
exports.testDynamicIntegerFunctions = testDynamicIntegerFunctions;
exports.testDynamicStringFunctions = testDynamicStringFunctions;
exports.testDynamicDateFunctions = testDynamicDateFunctions;
exports.testDynamicBooleanFunctions = testDynamicBooleanFunctions;
exports.testDynamicObjectFunctions = testDynamicObjectFunctions;
exports.testDynamicArrayFunctions = testDynamicArrayFunctions;
exports.testDynamicMapFunctions = testDynamicMapFunctions;
exports.runDynamicMethodTestsForChildObject = runDynamicMethodTestsForChildObject;
exports.compareAddresses = compareAddresses;
exports.newCategoryModel = newCategoryModel;
exports.newContentModel = newContentModel;
exports.newProductModel = newProductModel;
exports.newProductSearchModel = newProductSearchModel;
exports.newBasketModel = newBasketModel;
exports.newAssociateModel = newAssociateModel;
exports.newCustomerModel = newCustomerModel;
exports.newCustomerSearchModel = newCustomerSearchModel;
exports.newCustomerAddressModel = newCustomerAddressModel;
exports.newCustomerOrderModel = newCustomerOrderModel;
exports.newStoreCountriesModel = newStoreCountriesModel;
exports.newStoreStatesModel = newStoreStatesModel;
exports.newCFGSettingsModel = newCFGSettingsModel;
exports.newProductCollection = newProductCollection;
exports.newStoreCollection = newStoreCollection;
exports.newProductItemCollection = newProductItemCollection;

function equals(actual, expected) {
    expect(actual).toBe(expected);
}

function isTrue(actual) {
    expect(actual).toBe(true);
}

function isFalse(actual) {
    expect(actual).toBe(false);
}

function isUndefined(actual) {
    expect(actual).toBe(undefined);
}

function isNotUndefined(actual) {
    expect(actual).notToBe(undefined);
}

function isNull(actual) {
    expect(actual).toBe(null);
}

function isNotNull(actual) {
    expect(actual).notToBe(null);
}

function failure(error, expectationText) {
    if (error && _.isFunction(error.toJSON)) {
        error = error.toJSON();
    }
    if (error === undefined || error === null) {
        expect('A deferred failure occurred').toBe('Success');
    } else if (error.hasOwnProperty('fault')) {
        var fault = error.fault;
        if (fault.hasOwnProperty('type') && fault.hasOwnProperty('message')) {
            expect(fault.type + ': ' + fault.message).toBe('Success');
        }
    } else if (error.hasOwnProperty('type') && error.hasOwnProperty('message')) {
        expect(error.type + ': ' + error.message).toBe('Success');
    } else if (typeof error === 'string') {
        expect(error).toBe(expectationText || 'Success');
    }
}

function functionsDefined() {
    _.each(arguments, function (func) {
        expect(func).notToBe(undefined);
    });
}

function testDynamicNumberFunctions(obj, property) {
    var methodRoot = _.capitalize(_.camelize(property));
    var hasFunc = obj['has' + methodRoot];
    var getFunc = obj['get' + methodRoot];
    var setFunc = obj['set' + methodRoot];

    this.functionsDefined(hasFunc, getFunc, setFunc);

    if (hasFunc.call(obj)) {
        this.equals(hasFunc.call(obj), true);
        this.equals(getFunc.call(obj), obj.get(property));
    }

    var prevVal = obj.get(property);
    var model = setFunc.call(obj, 501.1);
    var modelGetFunc = model['get' + methodRoot];
    this.equals(modelGetFunc.call(model), 501.1);
    if (prevVal !== undefined) {
        setFunc.call(obj, prevVal);
    }
}

function testDynamicIntegerFunctions(obj, property) {
    var methodRoot = _.capitalize(_.camelize(property));
    var hasFunc = obj['has' + methodRoot];
    var getFunc = obj['get' + methodRoot];
    var setFunc = obj['set' + methodRoot];

    this.functionsDefined(hasFunc, getFunc, setFunc);

    if (hasFunc.call(obj)) {
        this.equals(hasFunc.call(obj), true);
        this.equals(getFunc.call(obj), obj.get(property));
    }

    var prevVal = obj.get(property);
    var model = setFunc.call(obj, 500);
    var modelGetFunc = model['get' + methodRoot];
    this.equals(modelGetFunc.call(model), 500);
    if (prevVal !== undefined) {
        setFunc.call(obj, prevVal);
    }
}

function testDynamicStringFunctions(obj, property) {
    var methodRoot = _.capitalize(_.camelize(property));
    var hasFunc = obj['has' + methodRoot];
    var getFunc = obj['get' + methodRoot];
    var setFunc = obj['set' + methodRoot];

    this.functionsDefined(hasFunc, getFunc, setFunc);

    if (hasFunc.call(obj)) {
        this.equals(hasFunc.call(obj), true);
        this.equals(getFunc.call(obj), obj.get(property));
        var prevVal = obj.get(property);
        var testString = 'testing function set' + methodRoot + ' on ' + property == 'id' ? prevVal : obj.get('id');
        var model = setFunc.call(obj, testString);
        var modelGetFunc = model['get' + methodRoot];
        this.equals(modelGetFunc.call(model), testString);
        if (prevVal !== undefined) {
            setFunc.call(obj, prevVal);
        }
    }
}

function testDynamicDateFunctions(obj, property) {
    var methodRoot = _.capitalize(_.camelize(property));
    var hasFunc = obj['has' + methodRoot];
    var getFunc = obj['get' + methodRoot];
    var setFunc = obj['set' + methodRoot];

    this.functionsDefined(hasFunc, getFunc, setFunc);

    if (hasFunc.call(obj)) {
        this.equals(hasFunc.call(obj), true);
        this.equals(getFunc.call(obj), obj.get(property));
    }

    var prevVal = obj.get(property);

    var modelGetFunc = model['get' + methodRoot];

    if (prevVal !== undefined) {
        setFunc.call(obj, prevVal);
    }
}

function testDynamicBooleanFunctions(obj, property) {
    var methodRoot = _.capitalize(_.camelize(property));
    var hasFunc = obj['has' + methodRoot];
    var isFunc = obj['is' + methodRoot];
    var setFunc = obj['setIs' + methodRoot];

    this.functionsDefined(hasFunc, isFunc, setFunc);

    if (hasFunc.call(obj)) {
        this.equals(hasFunc.call(obj), true);
        this.equals(isFunc.call(obj), obj.get(property));
    }

    var boolVal = obj.get(property);
    var model = setFunc.call(obj, !boolVal);
    var modelIsFunc = model['is' + methodRoot];
    this.equals(modelIsFunc.call(model), !boolVal);
    if (boolVal !== undefined) {
        setFunc.call(obj, boolVal);
    }
}

function testDynamicObjectFunctions(obj, property) {
    var methodRoot = _.capitalize(_.camelize(property));
    var hasFunc = obj['has' + methodRoot];
    var getFunc = obj['get' + methodRoot];
    var setFunc = obj['set' + methodRoot];

    this.functionsDefined(hasFunc, getFunc, setFunc);

    if (hasFunc.call(obj)) {
        this.equals(hasFunc.call(obj), true);
        this.equals(getFunc.call(obj), obj.get(property));
    }

    var prevVal = obj.get(property);
    var objToSet = {
        id: 'test property',
        test: 'another property'
    };
    var model = setFunc.call(obj, objToSet);
    var modelGetFunc = model['get' + methodRoot];
    this.equals(JSON.stringify(modelGetFunc.call(model)).replace(/\"/g, "'"), JSON.stringify(objToSet).replace(/\"/g, "'"));
    if (prevVal !== undefined) {
        setFunc.call(obj, prevVal);
    }
}

function testDynamicArrayFunctions(obj, property) {
    var methodRoot = _.capitalize(_.camelize(property));
    var hasFunc = obj['has' + methodRoot];
    var getFunc = obj['get' + methodRoot];
    var getCollFunc = obj['get' + methodRoot + 'Collection'];
    var setFunc = obj['set' + methodRoot];

    this.functionsDefined(hasFunc, getFunc, getCollFunc, setFunc);

    var val = obj.get(property);
    var array = getFunc.call(obj);
    var coll = getCollFunc.call(obj);
    if (hasFunc.call(obj)) {
        this.equals(hasFunc.call(obj), true);
        this.equals(coll, val);
    }

    this.equals(_.isArray(array), true);
    this.equals(coll.models.length, array.length);
    var model = setFunc.call(obj, []);
    var modelGetFunc = model['get' + methodRoot];
    this.equals(modelGetFunc.call(model).length, 0);
    if (val !== undefined) {
        setFunc.call(obj, val);
    }
}

function testDynamicMapFunctions(obj, property) {}

function runDynamicMethodTestsForChildObject(metadataObj, obj) {
    var metaprops = metadataObj.properties;
    if (metaprops === undefined) {
        return;
    }
    var properties = _.keys(metaprops);
    _.each(properties, function (property) {
        var classDesc = metaprops[property];
        if (classDesc.type == 'string') {
            console.log('test ' + classDesc.type + ' property: ' + property);
            exports.testDynamicStringFunctions(obj, property);
        } else if (classDesc.type == 'integer') {
            console.log('test ' + classDesc.type + ' property: ' + property);
            exports.testDynamicIntegerFunctions(obj, property);
        } else if (classDesc.type == 'number') {
            console.log('test ' + classDesc.type + ' property: ' + property);
            exports.testDynamicNumberFunctions(obj, property);
        } else if (classDesc.type == 'object' && classDesc.format == 'map') {
            console.log('test ' + classDesc.type + '/map property: ' + property);
            exports.testDynamicMapFunctions(obj, property);
        } else if (classDesc.type == 'object') {
            console.log('test ' + classDesc.type + ' property: ' + property);
            exports.testDynamicObjectFunctions(obj, property);
            exports.runDynamicMethodTestsForChildObject(metaprops[property], obj.get(property));
        } else if (classDesc.type == 'boolean') {
            console.log('test ' + classDesc.type + ' property: ' + property);
            exports.testDynamicBooleanFunctions(obj, property);
        } else if (classDesc.type == 'array') {
            console.log('test ' + classDesc.type + ' property: ' + property);
            exports.testDynamicArrayFunctions(obj, property);
        }
    });
}

function compareAddresses(address1, address2) {
    this.equals(address1.getAddress1(), address2.getAddress1());
    this.equals(address1.getAddress2(), address2.getAddress2());
    this.equals(address1.getCity(), address2.getCity());
    this.equals(address1.getCountryCode(), address2.getCountryCode());
    this.equals(address1.getStateCode(), address2.getStateCode());
    this.equals(address1.getPostalCode(), address2.getPostalCode());
    this.equals(address1.getFirstName(), address2.getFirstName());
    this.equals(address1.getLastName(), address2.getLastName());
    this.equals(address1.getPhone(), address2.getPhone());
}

function newCategoryModel(id) {
    return newModel('category', {
        id: id
    });
}

function newContentModel(id) {
    return newModel('content', {
        id: id
    });
}

function newProductModel(id) {
    return newModel('product', {
        id: id
    });
}

function newProductSearchModel(json) {
    return newModel('productSearch', json);
}

function newBasketModel() {
    return newModel('baskets');
}

function newAssociateModel() {
    return newModel('associate');
}

function newCustomerModel() {
    return newModel('customer');
}

function newCustomerSearchModel() {
    return newModel('customerSearch');
}

function newCustomerAddressModel() {
    return newModel('customerAddress');
}

function newCustomerOrderModel() {
    return newModel('customerOrder');
}

function newStoreCountriesModel() {
    return newModel('storeCountries');
}

function newStoreStatesModel() {
    return newModel('storeStates');
}

function newCFGSettingsModel() {
    return newModel('cfgSettings');
}

function newModel(type, json) {
    return Alloy.createModel(type, json);
}

function newCollection(type, data) {
    return Alloy.createCollection(type, data);
}

function newProductCollection() {
    return newCollection('product');
}

function newStoreCollection() {
    return newCollection('store');
}

function newProductItemCollection() {
    return newCollection('productItem');
}